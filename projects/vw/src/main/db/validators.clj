(ns db.validators
  (:require
   [schema.core :as s]))


(def validate s/validate)

(def UserData$
  {s/Any s/Any})

(def UserData$$
  {:_id s/Str
   :_rev s/Str
   :social_id s/Str

   ;:create_time (s/pred pos? 'pos?) ;; create_time 없는 유저도 있다.

   :event s/Any

   :user {:money s/Int
          :last-logout-time s/Int
          ;:avatar-url s/Str
          :gender s/Any
          :last-access-time s/Int
          :name s/Str
          :locale s/Any
          :email s/Any
          s/Any s/Any}

   :game {:slotmachine
          {:bet-amount (s/enum 200)
           ;:freespin-count s/Int
           s/Any s/Any}
          s/Any s/Any} ;; blackjack이 있는 경우도 있다.
   s/Any s/Any ;; conflict 결과가 저장된 경우가 있음.. (cloudant에서 발생시킴)
   }
  )
