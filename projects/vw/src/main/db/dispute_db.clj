(ns db.dispute-db
  (:require
    [clojure.tools.logging :refer :all]
    [slingshot.slingshot :refer [throw+ try+]]

    [db.impl.protocols :as impl]
    [db.impl.cloudant :as cloudant]
    [vw.read-config :as config]
    [db.validators :as v]
    [db.db-const :as CONST]
    [clojure.data.json :as json]

    [vw.err :as err]
    [vita.shared.const.err-code :as ERR]
    ))

;; "DB 구조
;;{:_id ":54c4af25-98a7-4ac0-ba55-04d560ba4dcd",
;; :_rev "491-4b84ab074643dd7cc0eb9dc33f23aa23",
;; :payment_id "none",
;; :time "1234"
;; :changed_fields "actions,dispute"
; }
; }"
;;
;; "
;; ************************
;; DB의 키워드중에 '-'는 반드시 '_' 로 처리. view로 검색할 때 '-'는 처리 되지 않는다.
;; ex) {:social-id :social-id}-> {:social_id :social-id}

;; DB에 _id의 value가 아닌 일반 Field의 value는 keyword가 사라진다.
;; ex)  {:_id :id :social_id :value} -> DB에는 {_id :id :social_id  value} 로 저장
;; ************************
;; "

(defonce db*
         (cloudant/->Cloudant (:dispute config/db-info*)))


;;;
(defn- put%
  ([contents validator]
   (put% contents validator CONST/RETRY_COUNT))

  ([contents validator try-count]
   (when-not (pos? try-count)
     (err/in-error ERR/DB_RETRY_FAIL))

   (try+
     (-> db*
         (impl/insert
           (if validator (v/validate validator contents) contents)))
     (catch Object _
       (put% contents validator (dec try-count))))))



(defn- get%
  ([sid validator]
   (get% sid :payment_id validator CONST/RETRY_COUNT))

  ([sid view-name validator]
   (get% sid view-name validator CONST/RETRY_COUNT))

  ([sid view-name validator try-count]
   (when-not (pos? try-count)
     (err/in-error ERR/DB_RETRY_FAIL))

   (try+
     (let [doc (-> db*
                   (impl/query ["views" view-name {:key sid}])
                   (first))]
       (if validator (v/validate validator doc) doc))
     (catch Object _
       (get% sid validator (dec try-count))))))



(defn my-print [& args]
  (warn (apply str args)))


;;; Basic API.
(defn put$
  ([contents]           (put$ contents nil))
  ([contents validator] (put% contents validator)))


(defn get$
  ([sid]           (get$ sid nil))
  ([sid validator] (get% sid validator)))


(defn insert
  [data]
  (my-print "==== dispute-db/insert ====")
  (my-print "type==> " (type data))
  (my-print "data==> " data)
  (put$ data))


;(get$ "483897378407825")
;{:_id "672c0dcb0fba8dcab6353becf3c60447", :request-id "c97158116ddd421a8da481aba2ec920a", :_rev "1-b1117e82cabedf92618b7ca692599f85", :payout-foreign-exchange-rate 1, :application {:name "test-vita", :namespace "test-vita", :id "755329927844736"}, :refundable-amount {:currency "USD", :amount "3.00"}, :actions [{:type "charge", :status "completed", :currency "USD", :amount "3.00", :time-created "2014-07-11T06:11:37+0000", :time-updated "2014-07-11T06:11:37+0000"}], :id "483897378407825", :items [{:type "IN_APP_PURCHASE", :product "http://test.sonaclo.com/products/og/product/0", :quantity 1}], :created-time "2014-07-11T06:11:37+0000", :user {:id "100008385018381", :name "Jennifer Amhchejahcha Narayananberg"}, :country "KR", :test 1}
