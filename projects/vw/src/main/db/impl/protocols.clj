(ns db.impl.protocols)


(defprotocol IDatabase
  (insert [this data])
  (delete [this data])
  (update [this data])
  (query [this query]))
