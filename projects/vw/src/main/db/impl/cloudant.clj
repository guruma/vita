(ns db.impl.cloudant
  (:require
   [db.impl.protocols :as impl]
   [com.ashafa.clutch :as clutch]
   ))



(defrecord Cloudant [db]
  impl/IDatabase
  (insert [this data]
    (clutch/put-document db data))

  (delete [this data]
    (clutch/delete-document db data))

  (update [this data]
    (clutch/update-document db data))

  (query [this query]
    (let [[design-document view-key query-params-map] query]
      (->> (clutch/get-view db design-document view-key query-params-map)
           (map :value)))))
