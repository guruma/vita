(ns vw.handler
  (:require
   [compojure.core :refer :all]
   [compojure.handler :as handler]
   [compojure.route :as route]
   [clojure.java.io :as io]
   [clojure.data.json :as json]
   [ring.middleware.edn :as edn]
   [ring.util.response :as res]
   [hiccup.page :as h :refer [html5]]
   [ring.middleware.json :as json-md]
   [clj-http.client :as http]
   [clojure.tools.logging :refer :all]

   [vw.products :as prod]
   [vw.realtime-update :as rtu]
   [vw.keyword-util :as kwd]
   [vita.config.macros :as config]
   ))


(defn my-print [& args]
  (warn (apply str args)))


(def index-page
  (io/resource "public/main.html"))


(defn respond-facebook-product [product-id]
  (my-print "\n\n============= facebook =============")
    (my-print "respond-facebook-product" product-id)
    (let [res (html5 (prod/og-product product-id))]
      (my-print res)
      res))


(defn- process-payment-object [payment-object]
  (rtu/write-payment-object payment-object))


(defn- respond-payments-object [payments-rtu]
  (my-print "==== respond-payments-object ====")
  (my-print payments-rtu)
  (if-let [payments-object (rtu/fetch-payments-object payments-rtu)]
    (do
      (doseq [payment-object payments-object]
        (my-print "====> payment-object <====")
        (my-print "::: " payment-object)
        (process-payment-object payment-object))
      (res/response "DB Write. O.K."))
    "Invalid Payment Object"))


;; {:object "payments", :entry [{:id "472111316252398", :time 1404354457, :changed-fields ["actions"]}]}
(defn- respond-realtime-update [req]
  (my-print "**** respond-realtime-update ****")
  ;;   (my-print req)
  (let [body (slurp (:body req))]
    (if (rtu/verify-realtime-update (assoc req :body body))
      (let [rtu (-> body
                    json/read-json
                    kwd/clojure-style-keywordize)]
        (if (rtu/payments-object? rtu)
          (respond-payments-object rtu)
          "Invalid Payments Object"))
      "Invalid RTU")))


(defn- respond-subscription [params]
  (my-print "**** respond-subscription ****")
  (let [params (kwd/clojure-style-keywordize params)]
;;     (my-print params)
    (if (rtu/subscribe? params)
      (do
        ; challenge 값을 되돌려준다.
        (my-print "hub.challenge: " (:hub.challenge params))
        (:hub.challenge params))
      "Invalid Subscription.")))


(defroutes app-routes
  (GET  "/vita-login/" [] index-page)
  (POST "/vita-login/" [] index-page)

  ; RTU subscription
  (GET  "/payment/realtime-update"    req (respond-subscription (:params req)))

  ; RTU(RealTime Update). 1. 비동기 결제. 2. 환불, 지불 거절, 지불 거절 취소. 3. 이의 제기
  (POST "/payment/realtime-update"    req (respond-realtime-update req))

  ; 페북에서 상품 정보를 가져오는 요청
  (GET  "/products/og/product/:product-id" [product-id] (respond-facebook-product product-id))


  ; 로그인
  (route/resources "/vita-login/")

  ; 에러
  (route/not-found "Page not found!"))


(def app
  (handler/site (-> app-routes
                    edn/wrap-edn-params)))

;; realtime update
;; :entry: [{:id "511634608967335", :time 1405230725, :changed-fields ["actions"]}]

;;
;; (defn- respond-payments-object [params]
;;   (my-print "==== respond-payments-object ====")
;;   (my-print ":entry: " (:entry params))
;;   (if-let [po (rtu/get-payments-object params)]
;;     (do
;;       (my-print "====> payments-object <====")
;;       (my-print po)
;;       (rtu/write-payments-object po)
;;       (res/response "O.K."))
;;     "Invalid Payment Object"))

;; 비동기 업데이트
;;
;; 결제 관련 실시간 업데이트(RealTime Update. RTU) :
;;   1. 결제가 생성되거나 변경될 때마다(이의제기(Dispute)나 환불(Refund)) 페북에서 우리 서버로 전송된다.
;;   2. 앱 대시보드에 RTU를 받을 엔드포인트(URL)을 /payment/realtime-update 으로 구독했다.
;;   3. 만약 페북이 서버에 대한 실시간 업데이트에 실패하면 페북은 즉시 재시도하고,
;;      다음 24 시간 동안 빈도를 줄이면서 몇 차례 더 업데이트를 시도한다.
;;   4. 매 요청마다 X-Hub-Signature HTTP 헤더로 SHA1 서명을 보낸다.
;;      이것으로 요청의 페이로드의 무결성을 확인할 수 있다.
;;   5. Payments 객체의 경우 actions와 disputes 필드를 받아보기 한다.
;;      5.1. actions : 결제 비동기 완료, 페북 환불 지급, 사용자 지불 거절시
;;      5.2. disputes : 사용자 이의 제기
;;
;;   6. RTU 요청의 데이타 예제 :
;;      {:object "payments", :entry [{:id "472111316252398", :time 1404354457, :changed-fields ["actions"]}]}
;;   7. 이 요청을 받은 후 id로 다시 페북에 payment object(payment의 상세정보)를 가져와야 한다.
;;   8. payment object는 다음과 같은 형태이다.
;; {
;;    "id": "3603105474213890",
;;    "user": {
;;       "name": "Daniel Schultz",
;;       "id": "221159"
;;    },
;;    "application": {
;;       "name": "Friend Smash",
;;       "namespace": "friendsmashsample",
;;       "id": "241431489326925"
;;    },
;;    "actions": [
;;       {
;;          "type": "charge",
;;          "status": "completed",
;;          "currency": "USD",
;;          "amount": "0.99",
;;          "time_created": "2013-03-22T21:18:54+0000",
;;          "time_updated": "2013-03-22T21:18:55+0000"
;;       },
;;       {
;;          "type": "refund",
;;          "status": "completed",
;;          "currency": "USD",
;;          "amount": "0.99",
;;          "time_created": "2013-03-23T21:18:54+0000",
;;          "time_updated": "2013-03-23T21:18:55+0000"
;;       }
;;    ],
;;    "refundable_amount": {
;;       "currency": "USD",
;;       "amount": "0.00"
;;    },
;;    "items": [
;;       {
;;          "type": "IN_APP_PURCHASE",
;;          "product": "http://www.friendsmash.com/og/friend_smash_bomb.html",
;;          "quantity": 1
;;       }
;;    ],
;;    "country": "US",
;;    "created_time": "2013-03-22T21:18:54+0000",
;;    "payout_foreign_exchange_rate": 1,
;; }

;;   여기서 눈여겨 볼 것은...
;;   actions 필드의 type 속성이다.
;;   type 속성은  charge, refund, chargeback, chargeback_reversal 등이 있다.
;;
;;
;; 환불 :
;;   1. 페북에서 직접 환불하는 경우도 있는데, 이때도 RTU를 보내준다.
;;   2. 페북에서 개발자가 환불해야 하는 경우에 대해 RTU를 보내준다.
;;
;; 지불 거절
;;   1. 사용자가 결제 수단 공급자(카드회사 등)에 직접 연락하여 지불 중단 등 이의 제기할 때 발생.
;;   2. 사유는 무단 사용, 이중 청구, 게임 머니 미수령등 다양하다.
;;
;; 지불 거절 취소
;;   1. 사용자가 은행에 지불 거절을 신청했으나, 은행에서 정당하지 않은 신청이라 판단한 경우.
;;


;;
;; payment object
;;
;;
;; #_(
;; {:request-id 46a3fdab1fe345269907aaf433153e6e,
;;  :payout-foreign-exchange-rate 1,
;;  :application {:name test-vita,
;;                :namespace test-vita,
;;                :id 755329927844736},
;;  :refundable-amount {:currency USD,
;;                      :amount 3.00},
;;  :actions [{:type charge,
;;             :status completed,
;;             :currency USD,
;;             :amount 3.00,
;;             :time-created 2014-07-03T02:27:35+0000,
;;             :time-updated 2014-07-03T02:27:37+0000}],
;;  :id 472111316252398,
;;  :items [{:type IN_APP_PURCHASE,
;;           :product http://test.sonaclo.com/products/og/product/0,
;;           :quantity 1}],
;;  :created-time 2014-07-03T02:27:35+0000,
;;  :user {:id 100008385018381,
;;         :name Jennifer Amhchejahcha Narayananberg},
;;  :country KR,
;;  :test 1}
;; )

