(ns vw.verifier
  "=======================

   모듈 책임 : 페이스북에서 받은 결재 정보를 검증한다.
   모듈 이름 : vw.verifier
   연관 모듈 : vita.config.macros
   모듈 작성자 : guruma@weebinar.com

   모듈 정의 :
     1. 페이스북의 암호화된 정보를 풀 수 있어야 한다.
     2. 페이스북의 결제 정보의 유효함을 검증 할 수 있어야 한다.

   ========================"
  (:require
   ; 연관 모듈
   [vita.config.macros :as config]

   ; 구현에 필요한 라이브러리
;;    [sonaclo.shared.util :refer [not-nil?]]
;;    [vw.log :refer [my-print]]
   [clojure.tools.logging :refer :all]
   [vw.keyword-util :as kwd]
   [ring.util.codec :as codec]
   [pandect.core :as pan]
   [clojure.data.json :as json]))


(defn my-print [& args]
  (warn (apply str args)))


(def app-info (config/config:get-in [:api :facebook]))


(defn verify-realtime-update [req]
  (let [hub-signature (:x-hub-signature (kwd/clojure-style-keywordize (:headers req)))
        payload (:body req)
        expected-sig (str "sha1=" (pan/sha1-hmac payload (:app-secret-code app-info)))]
    (my-print "==== verify-realtime-update ====")
;;     (my-print payload)
;;     (my-print expected-sig)
;;     (my-print hub-signature)
    (= hub-signature expected-sig)))
