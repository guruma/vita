(ns vw.log
  (:require [clojure.tools.logging :as log]))
;;             [clj-logging-config.log4j :as log4j]))

;; (log4j/set-logger! :level :warn
;;              :pattern "%d %-5p %c: %m%n"
;;              :out (org.apache.log4j.DailyRollingFileAppender.))

;; (defmacro dbg [& arg]
;;   `(type (first '~arg)))

;; (defmacro prnt [args]
;;   `(let [a# (map #(str ">>>" %) '~args)]
;;      (map #(str %1 " --> " %2) a# (list ~@args))))

;; (defmacro debug [& args]
;;   `(if (string? (first '~args))
;;      (log/debug (clojure.string/join (list ~@args)))
;;      (log/debug (clojure.string/join ", " (prnt ~args)))))

(defn debug [s]
  (log/warn s))

(defn my-print [& args]
  (debug (apply str args)))

;; (log/debug "ffff")

;; (log/info "kkkk")
;; (info "kkkk")
;; (println "fff")
