(ns vw.db-updator
  "=======================

   모듈 책임 : 결제 처리 정보를 DB에 저장하고 업데이트한다.
   모듈 이름 : vw.db-updator
   연관 모듈 : 없음
   모듈 작성자 : ?

   모듈 정의 :
     1. 결제 처리 정보를 DB에 로그로 기록한다.
     2. 결제 처리 정보에 따라 해당 유저의 chips를 업데이트한다.

   ========================"
  (:require [db.dispute-db :as dispute-db]
            [clojure.tools.logging :refer :all]))


(defn my-print [& args]
  (warn (apply str args)))


(defn update-dispute [info]
  (dispute-db/insert info))


(defn update-db-dispute [info]
  (my-print "==== update-db-dispute ====")
  (my-print "::: " info)
  (try
    (update-dispute info)
    ;; 예외 처리
    (catch Exception e
      (my-print "==== update-db-dispute exception ====")
      (my-print e)
      ;TODO : log db에 기록해야 함.
      )))

;; (update-db-dispute {:payment-id "1235" :time "5555" :changed-fields "actions"})
