(ns vw.keyword-util
  "=======================

   모듈 책임 : 페북에서 받은 데이타를 clojure 소스에서 처리하기 좋은 map으로 바꾼다.
   모듈 이름 : vw.keyword
   연관 모듈 :
   모듈 작성자 : guruma@weebinar.com

   모듈 정의 :
     1. 페북에서 온 데이타를 str을 keyword로 바꾼다.
     2. 키워드 이름중 언더바('_')를 하이픈('-')으로 바꾼다.
     3. 키워드 이름중 하이픈('-')를 언더바('_') 바꾼다.

   ========================"
  )


(defn- hyphen->underbar [kw]
  (keyword (.replace (name kw) "-" "_")))


(defn- underbar->hyphen [kw]
  (keyword (.replace (name kw) "_" "-")))


(defn underbar-keywordize-keys [m]
  (let [f (fn [[k v]] [(hyphen->underbar k) v])]
    (clojure.walk/postwalk
     (fn [x] (if (map? x) (into {} (map f x)) x))
     m)))


(defn hyphen-keywordize-keys [m]
  (let [f (fn [[k v]] [(underbar->hyphen k) v])]
    (clojure.walk/postwalk
     (fn [x] (if (map? x) (into {} (map f x)) x))
     m)))


(defn clojure-style-keywordize [m]
  (hyphen-keywordize-keys (clojure.walk/keywordize-keys m)))


;; test codes

;; (= (hyphen->underbar :a-b) :a_b)
;; (= (hyphen->underbar :a----b) :a____b)
;; (= (hyphen->underbar :a-_-_b) :a____b)
;; (= (hyphen->underbar :a_b) :a_b)
;; (= (hyphen->underbar :ab) :ab)
;; (= (hyphen->underbar :a-) :a_)
;; (= (hyphen->underbar :-a) :_a)

;; (= (clojure-style-keywordize {"a_b" {"c_d" 1}}) {:a-b {:c-d 1}})



