(ns vw.realtime-update
  "=======================

   모듈 책임 : 결제 오브젝트를 검증하고 DB에 기록한다.
   모듈 이름 : vw.realtime-update
   연관 모듈 : vw.products vw.db-updator
   모듈 작성자 : guruma@weebinar.com

   모듈 정의 :
     1. 결제 오브젝트의 무결성을 검증한다.
     2. 결제 오브젝트의 유효성을 검증한다.
     3. 결제 오브젝트를 DB에 기록한다.

   ========================"
  (:require
   ; 연관 모듈
   [vw.products :refer [get-product]]
   [vw.verifier :as fb-verif]
   [vw.db-updator :as db]
   [vita.config.macros :as config]
   [vw.keyword-util :as kwd]

   ; 구현에 필요한 라이브러리
   [clojure.tools.logging :refer :all]
   [clj-http.client :as http]
   [ring.util.codec :as codec]
   [pandect.core :as pan]
   [clojure.data.json :as json]))


(defn my-print [& args]
  (apply println args))
;;   (warn (apply str args)))


(def app-info (config/config:get-in [:api :facebook]))


(def app-access-token (str (:appId app-info) "|" (:app-secret-code app-info)))


(defn- fb-uri [path]
  (str "https://graph.facebook.com" path))

;; (defn- fb-uri [& path]
;;   (str "https://graph.facebook.com" path))


(defn verify-realtime-update [req]
  (fb-verif/verify-realtime-update req))


(defn write-payment-object [payment-object]
  (my-print "==== write-payment-object ====")
  (db/update-db-dispute payment-object))


(defn subscribe?
  "checks a request for checking facebook RealTime Callback subscrtiption"
  [params]
  (and (= "subscribe" (:hub.mode params))
       ; hub.verify-token은 앱 대쉬보드의 Canvas Payments의 Realtime Updates에 있는 Verification Token에서 설정한다.
       (= "5757" (:hub.verify-token params))))


(defn payments-object?
  "checks a request via facebook RealTime Callback"
  [params]
  (= "payments" (:object params)))


(defn fetch-payments-object[params]
  (my-print "==== fetch-payments-object ====")
  (let [entry (:entry params)]
    (my-print entry)
    (try
      (doall
      (for [{:keys [id time changed-fields]} entry]
        (let [url (str (fb-uri "/") id "/")
              po (-> (http/get url {:query-params {"access_token" app-access-token}})
                     :body
                     json/read-json
                     kwd/clojure-style-keywordize
                     )]
          (my-print "==== payments-object ====")
;;           (my-print url)
          (my-print ":::1: " po)
          po)))
          (catch Exception e
            (my-print "==== Exception : fetch-payments-object ====")
            (my-print "error: "(.getMessage e))))))



;; test

;; (time
;;  (dotimes [n 100]
;;    (fetch-payments-object
;;     {:entry [{:id "511634608967335", :time 1405230725, :changed-fields ["actions"]}]})))
;; ;; "Elapsed time: 39463.394 msecs"

;; (def po
;;   (fetch-payments-object
;;    {:entry [{:id "511634608967335", :time 1405230725, :changed-fields ["actions"]}]}))

;; po

;; (write-payment-object (first po))


