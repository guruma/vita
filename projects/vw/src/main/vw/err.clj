(ns vw.err
  (:require
    [slingshot.slingshot :refer [throw+]]))


(defmacro in-error
  ([error-code]
   `(throw+ {:type :in-error :err ~error-code}))
  ([error-code message]
   `(throw+ {:type :in-error :err ~error-code} ~message))
  ([error-code fmt arg & args]
   `(throw+ {:type :in-error :err ~error-code} ~fmt ~arg ~@args)))


(defmacro sys-error
  ([error-code]
   `(throw+ {:type :sys-error :err ~error-code}))
  ([error-code message]
   `(throw+ {:type :sys-error :err ~error-code} ~message))
  ([error-code fmt arg & args]
   `(throw+ {:type :sys-error :err ~error-code} ~fmt ~arg ~@args)))


(defn in-error? [x]
  (= (:type x) :in-error))


(defn sys-error? [x]
  (= (:type x) :sys-error))