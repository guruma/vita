(ns vita.config.loader
  (:require
   [clojure.java.io :as io]
   [environ.core :as env]
   [clojure.tools.reader.edn :as edn]))


(defn- file-exist?
  [fpath]
  (-> fpath
      (io/as-file)
      (.exists)))


(defn- get-config
  [config-fname]
  (-> config-fname
      (slurp)
      (edn/read-string)))


(defn load-config
  "인자로 받은 env-config 환경 변수에 있는 파일을 읽어들인다."
  [env-config]
  (let [fpath (env/env env-config)]
    (when-not (some? fpath)
      (throw (Exception. "fpath not found" env-config)))

    (when-not (file-exist? fpath)
      (throw (Exception. "file not exiest")))

    (get-config fpath)))

;; (load-config :vw-config)
;; (env/env :vw-config)

(defn- keywordize [s]
  (-> (clojure.string/lower-case s)
      (clojure.string/replace "_" "-")
      (clojure.string/replace "." "-")
      (keyword)))

(defn- sanitize [k]
  (let [s (keywordize (name k))]
    (if-not (= k s) (println "Warning: environ key " k " has been corrected to " s))
    s))

(defn- read-env-file []
  (let [env-file (io/file ".lein-env")]
    (if (.exists env-file)
      (into {} (for [[k v] (read-string (slurp env-file))]
                 [(sanitize k) v])))))

(defn- read-system-env []
  (->> (System/getenv)
       (map (fn [[k v]] [(keywordize k) v]))
       (into {})))

(defn- read-system-props []
  (->> (System/getProperties)
       (map (fn [[k v]] [(keywordize k) v]))
       (into {})))
