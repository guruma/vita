(ns vita.config.macros
  (:require
   [vita.config.loader :as config]))


(def config*
  "환경변수 VA_CONFIG를 읽어들인 값이 온다.(edn형식)"
  (config/load-config :vw-config))


(defmacro config:get-in
  "config의 값을 읽어옴."
  [get-v]
  (get-in config* get-v))


(defmacro DEBUG
  "config의 :debug값이 true인 경우에만 실행됨."
  [& body]
  (when (get config* :debug)
    `(do
       ~@body)))


(defmacro LEVEL
  "config의 :level에 따른 분기를 타도록 도와줌.."
  [& body]
  (let [level
        (get config* :level)

        cond-dic
        (->> `~body
             (partition 2)
             (reduce (fn [acc [f s]] (assoc acc f s)) {}))]
    (get cond-dic level)))
