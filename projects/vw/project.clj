(defproject vw "0.1.0-SNAPSHOT"
  :min-lein-version "2.3.4"
  :dependencies
  [[org.clojure/clojure "1.6.0"]
   [org.clojure/core.async "0.1.303.0-886421-alpha"]
   [ring/ring-core "1.3.0"]
   ;[info.sunng/ring-jetty9-adapter "0.7.1"]
   [ring/ring-json "0.3.1"]
   [fogus/ring-edn "0.2.0"]
   [javax.servlet/servlet-api "2.5"] 
   [environ "0.5.0"]
   [compojure "1.1.8"]
   [pandect "0.3.3"]
   [clj-http "0.9.2"]
   
   [org.clojure/data.json "0.2.5"]
   [hiccup "1.0.5"]
   
   
   ;; Log.
   [org.clojure/tools.logging "0.3.0"]
   [org.slf4j/slf4j-log4j12 "1.7.7"]
   [clj-logging-config "1.9.10"]
   [log4j/apache-log4j-extras "1.2.17"]
   [log4j/log4j "1.2.17" :exclusions [javax.mail/mail
                                      javax.jms/jms
                                      com.sun.jdmk/jmxtools
                                      com.sun.jmx/jmxri]]

   ;; schema
   [prismatic/schema "0.2.4"]

   ;; try catch
   [slingshot "0.10.3"]

   ;; DB.
   [paddleguru/clutch "0.4.0"]
   [com.cemerick/url "0.1.1"]]


  :plugins [[lein-ring "0.8.11"]
            [lein-environ "0.5.0"]
            [lein-midje "3.1.3"]]

  :ring {:handler vw.handler/app}

  :clean-targets [:target-path "resources/public/js/game/"]
  :source-paths ["src/main"]
  :profiles
  {:dev {:env {:vw-config "../../dist/config/vw.edn"}
         :jvm-opts ["-XX:-OmitStackTraceInFastThrow"]}

   :proxy {
         :env {:vw-config "../../dist/config/proxy-vw.edn"}
         :ring {:handler vw.handler/app
                :ssl? true
                :stacktraces? false
                :auto-reload? false
                :ssl-port 443
                :keystore "../../dist/certs/sonaclo.keystore"
                :key-password "gasanfreenote"
                :want-client-auth true
                :need-client-auth true}
           :jvm-opts ["-XX:-OmitStackTraceInFastThrow"]}
   :qa {:env {:vw-config "../../dist/config/qa1-vw.edn"}
        :ring {:handler vw.handler/app
               :ssl? true
               :stacktraces? false
               :auto-reload? false
               :ssl-port 443
               :keystore "../../dist/certs/sonaclo.keystore"
               :key-password "gasanfreenote"
               :want-client-auth true
               :need-client-auth true}
         :jvm-opts ["-XX:-OmitStackTraceInFastThrow"]}

   :live {
           :env {:vw-config "../../dist/config/live1-vw.edn"}
           :ring {:handler vw.handler/app
                  :ssl? true
                  :stacktraces? false
                  :auto-reload? false
                  :ssl-port 443
                  :keystore "../../dist/certs/sonaclo.keystore"
                  :key-password "gasanfreenote"
                  :want-client-auth true
                  :need-client-auth true
                  }
           }
   }

  :aliases {"proxy-vw" ["with-profile" "proxy" "ring" "server"]}
  )
