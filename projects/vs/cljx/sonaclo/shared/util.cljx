(ns sonaclo.shared.util
  (:require
   #+clj [clojure.stacktrace :as stacktrace]
   [clojure.walk]))

#+clj
(defmacro assert*
  "Evaluates expr and throws an exception if it doesn't evaluate to logical true.
   Returns true if it does."
  {:added "1.0"}
  ([x]
     (when *assert*
       `(try
          (when-not ~x
            (throw (new AssertionError (str "Assert failed: " (pr-str '~x)))))
          true
          (catch AssertionError ~'e
                 (stacktrace/print-stack-trace ~'e)
                 (throw ~'e) ))))
	([x message]
     (when *assert*
       `(try
          (when-not ~x
            (throw (new AssertionError (str "Assert failed: " ~message "\n" (pr-str '~x)))))
          true
          (catch AssertionError ~'e
                 (stacktrace/print-stack-trace ~'e)
                 (throw ~'e) )))))
#+clj
(defmacro assert-validator
	[& body]
	(if *assert*
  	`(fn [~'v_]
	     (and ~@body)
			 true)
		`(constantly true) ))

(def not-nil?
  (complement nil?))


(defmacro when-let* [bindings & body]
  (when (not (even? (count bindings)))
    (throw (IllegalArgumentException.
             "when-let* requires an even number of forms in binding vector")))

  (let [when-lets (reduce (fn [sexpr bind]
                            (let [form (first bind)
                                  tst (second bind)]
                              (conj sexpr `(when-let [~form ~tst]))))
                          ()
                          (partition 2 bindings))
        body (cons 'do body)]
    `(->> ~body ~@when-lets) ))

(defn dissoc-in
  "Dissociates an entry from a nested associative structure returning a new
  nested structure. keys is a sequence of keys. Any empty maps that result
  will not be present in the new structure.

  (dissoc-in [{:a 2 :b 9} {:a 9 :b 5}] [1 :a])
  ; => [{:b 9, :a 2} {:b 5}]
  "
  [m [k & ks :as keys]]
  (if ks
    (if-let [next-map (get m k)]
      (let [new-map (dissoc-in next-map ks)]
        (if (seq new-map)
          (assoc m k new-map)
          (dissoc m k)))
      m)
    (dissoc m k)))


(defn list->map
  "input (:key value :key1 value1"
  [list]
  (apply hash-map list))


(defn remove-vector
  "remove elem in coll

  ```clojure
  >> (remove-vector [1 2 3] 0)
  ;=> [2 3]
  "
  [coll pos]
  (if (neg? pos)
    coll
    (vec (concat (subvec coll 0 pos) (subvec coll (inc pos))))))


(defn in?
  "true if seq contains elm

   ```clojure
   >> (in? [:a :b] :a)
   ;=> true
   ```
  "
  [seq elm]

  (some #(= elm %) seq))


(defn find-key-by-value
  "
  ```clojure
  >> (find-key-by-value {:a 1 :b 2 :c 2} 2)
  => (:b :c)
  ```
  "
  [map val]
  (for [[k v] map :when (= v val)]
    k))


(defn probility
  "확율이다.
  input parm=> 40이면 40% 확율이 된다."
  [probility]
  (-> (rand-int 100)
      (< probility)))


(defn combine
  "Merge maps, recursively merging nested maps whose keys collide.
   ref: https://groups.google.com/forum/#!topic/clojure/UdFLYjLvNRs

  ```clojure
  >> (combine {:foo {:bar :baz}} {:foo {:x :y}})
  ;=> {:foo {:x :y, :bar :baz}}
  ```
 "

  ([m1 m2]
     (reduce (fn [m1 [k2 v2]]
               (if-let [v1 (get m1 k2)]
                 (if (and (map? v1) (map? v2))
                   (assoc m1 k2 (combine v1 v2))
                   (assoc m1 k2 v2))
                 (assoc m1 k2 v2)))
             m1 m2))
  ([m1 m2 & more]
     (apply combine (combine m1 m2) more)))


(defn f-zipmap
  "
  ```clojure
  >> (f-zipmap inc [1 2 3])
  => {1 2, 2 3, 3 4}
  ```
  "

  [f col]

  (->> col
       (reduce (fn [acc x] (assoc acc x (f x))) {})))


(defn update-values-in
  "Update values at ks in map by f. use wildcard keyword (:*).

   (update-values-in m [:team :* :age] + 3)"

  [m ks f & args]
  (let [pre-ks (take-while #(not= :* %) ks)
        cnt (count pre-ks)
        iter-ks (vec (keys (get-in m pre-ks)))]
    (loop [m m [k & rks] iter-ks]
      (if (nil? k)
        m
        (recur (update-in m
                          (assoc ks cnt k)
                          #(apply f (cons % args)))
               rks)))))

(defn- hyphen->underbar [kw]
  (keyword (.replace (name kw) "-" "_")))


(defn- underbar->hyphen [kw]
  (keyword (.replace (name kw) "_" "-")))


(defn underbar-keywordize-keys [m]
  (let [f (fn [[k v]] [(hyphen->underbar k) v])]
    (clojure.walk/postwalk
      (fn [x] (if (map? x) (into {} (map f x)) x))
      m)))


(defn hyphen-keywordize-keys [m]
  (let [f (fn [[k v]] [(underbar->hyphen k) v])]
    (clojure.walk/postwalk
      (fn [x] (if (map? x) (into {} (map f x)) x))
      m)))


(defn clojure-style-keywordize [m]
  (hyphen-keywordize-keys (clojure.walk/keywordize-keys m)))


(defn- index-exclude
  "Take all indices execpted ex"
  [r ex]
  (remove ex (range r)))

(defn dissoc-vec
  "Vector 삭제한다.
  ex (dissoc-vec [1 2 3] 1 2) : '(1)"
  [vector & remove-indexes]
  (map vector (index-exclude (count vector) (set remove-indexes))))
