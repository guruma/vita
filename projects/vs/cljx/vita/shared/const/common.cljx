(ns vita.shared.const.common)

(def ^:const TEXAS :texas)
(def ^:const OMAHA :omaha)
(def ^:const OMAHAHL :omahahl)
(def ^:const SLOTMACHINE :slotmachine)
(def ^:const MAIN_LOBBY :main-lobby)
(def ^:const COMMON :common)
