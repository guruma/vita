(ns assert-test
  (:require [sonaclo.shared.util :refer :all]))

(def a* (atom {:x {:y {:z 10 :k 20}}}))

(set-validator! a* (assert-validator
										 (cond
											 (= 20 (get-in v_ [:x :y :k]))
                       (assert* (not (nil? (get-in v_ [:x :y :z]))) "must not nil") )))

(defn fn-c [a]
  (swap! a update-in [:x :y :z] + 20)
  (swap! a assoc-in [:x :y :z] nil))

(defn fn-b [a]
  (fn-c a))

(defn fn-a [a]
  (fn-b a))


(fn-a a*)

(assert* (= 1 2) "not same")

