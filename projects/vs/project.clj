(defproject vs "0.1.0-SNAPSHOT"
  :dependencies [[org.clojure/clojure "1.6.0"]]

  :plugins [[com.keminglabs/cljx "0.4.0"]]
  :source-paths ["test"]
  :cljx {:builds [{:source-paths ["cljx"]
                   :rules :clj
                   :output-path "../va/src/main/"
                   }

                  {:source-paths ["cljx"]
                   :rules :clj
                   :output-path "../vw/src/main/"
                   }

                  {:source-paths ["cljx"]
                   :rules :cljs
                   :output-path "../vc/src/main/"
                   }

                  ]
         }
  )
