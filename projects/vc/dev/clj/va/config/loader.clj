(ns va.config.loader
  (:require
   [clojure.java.io :as io]
   [environ.core :as env]
   [clojure.tools.reader.edn :as edn]))


(defn- file-exist?
  [fpath]
  (-> fpath
      (io/as-file)
      (.exists)))


(defn- get-config
  [config-fname]
  (-> config-fname
      (slurp)
      (edn/read-string)))


(defn load-config
  "인자로 받은 env-config 환경 변수에 있는 파일을 읽어들인다."
  [env-config]
  (let [fpath (env/env env-config)]
    (when-not (some? fpath)
      (throw (Exception. "fpath not found" env-config)))

    (when-not (file-exist? fpath)
      (throw (Exception. "file not exiest")))

    (get-config fpath)))
