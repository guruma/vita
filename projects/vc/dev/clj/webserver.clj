(ns webserver
  (:require
   [clojure.java.io :as io]
   [compojure.core :refer [GET POST defroutes]]
   [compojure.route :as route]
   [compojure.handler :as handler]
   [mccool]
   ))


(defroutes router
  (GET "/" [] (io/resource "public/client.html"))

  (GET "/mccool"
       [] (mccool/render-index))
  (GET "/mccool/:page"
       [page] (mccool/get-page page))
  (POST "/mccool/:page"
        {params :params} (mccool/post-page (:page params) params))
  (POST "/mccool/:page/:action"
        {params :params} (mccool/post-page-action (:page params) (:action params) params))
  (route/resources "/")
  (route/not-found "Page not found"))


(def app
  (-> (handler/site router)))
