(ns repl
  (:require
   [cemerick.piggieback]
   [weasel.repl.websocket]
   [ring.adapter.jetty]
   [webserver]
   ))


(defn run! []
  (defonce ^:private server
    (ring.adapter.jetty/run-jetty #'webserver/app {:port 8888 :join? false}))
  server)


(defn cljs! []
  (cemerick.piggieback/cljs-repl
   :repl-env (weasel.repl.websocket/repl-env
              :ip "0.0.0.0" :port 9001)))
