(ns mccool
  (:import [java.net.URL])
  (:require
   [clostache.parser :as mustache]
   [cemerick.url :as url]
   [com.ashafa.clutch :as clutch]
   [va.config.macros :as cm]))

(defn render-index []
  (mustache/render-resource "mccool/index.mustache" {:name "Donald"}))

(defn get-page [page]
  (condp = page
    "db"
    (mustache/render-resource (str "mccool/db.mustache") (:db cm/config*))

    ;; else.
    (mustache/render-resource (str "mccool/" page ".mustache"))))


(defn post-page [page params]
  (str page params (:db cm/config*))
  )


(declare dispatch)

(defn post-page-action [page action params]
  (dispatch page action params)
  )


(defmulti dispatch (fn [page action params] [page action]))

(defmethod dispatch ["db" "clear-db"]
  [page action _]
  (let [{{con-game :game
          con-rank :rank
          con-log :log}
         :connect username :username password :password} (:db cm/config*)

         userinfo {:username username :password password}]

    (assert (not= username "vita"))

    (let [game-db (merge (url/url con-game) userinfo)
          rank-db (merge (url/url con-rank) userinfo)
          log-db (merge (url/url con-log) userinfo)]

    ;; delete all database.
    (doseq [db [game-db rank-db log-db]]
      (let [c (clutch/couch db)]
        (clutch/delete-database c)))

    ;; game-db
    (clutch/create! (clutch/couch game-db))
    (clutch/save-view game-db "views"
                      [:javascript {:social_id {:map "function(doc) { emit(doc.social_id, doc); }"}
                                    :email {:map "function(doc) { emit(doc.user.email, doc); }"}

                                    :friend_rank {:map "function(doc) { emit(doc.social_id, [doc.social_id, doc.user.money]); }"}
                                    :invitee {:map "function(doc) { if (!doc.invitee) return; for (var invitee_id in doc.invitee) { emit(invitee_id, doc); }}"}
                                    :sid_name {:map "function(doc) { emit(doc.social_id, [doc.user.name]); }"}

                                    }])

    ;; log-db
    (clutch/create! (clutch/couch log-db))
    (clutch/save-view log-db "views"
                      [:javascript {:social_id {:map "function(doc) {\n   emit(doc.social_id, doc);\n}" }
                                    :sid&log_type {:map "function(doc) {\n   emit([doc.social_id, doc.log_type], doc);\n}"}
                                    :log_type {:map "function(doc) {\n   emit([doc.log_type], doc);\n}" }}])


    ;; rank-db
    (clutch/create! (clutch/couch rank-db))


    ;; example put.
    (clutch/put-document
     game-db {:_id (keyword (str (java.util.UUID/randomUUID)))
              :social_id "im social id"
              :user {:money 0}
              :event "im evetn type"
              :create_time (System/currentTimeMillis)
              :game {:slotmachine {:bet-amount 200
                                   :freespin-cnt 0}}} )
    "DONE"
    )))
