(ns vita.slotmachine.test-counter
  (:require-macros
   [clojure.test.check.clojure-test :refer (defspec)]
   [clojure.test.check.properties :as prop]
   [cemerick.cljs.test :refer (deftest is)]
   )
  (:require
   [clojure.test.check :as sc]
   [clojure.test.check.clojure-test.runtime :as ct]

   ;; [cemerick.cljs.test :as t]
   [clojure.test.check.generators :as gen]
   [clojure.test.check.properties :as prop]
   [vita.slotmachine.util.counter :as util.counter]))


(deftest test-hello1

  (is (= 1 1))
  (is (= (last (util.counter/count-range 0 6000)) 6000))

  )

(defspec first-element-is-min-after-sorting 100
  (prop/for-all [v (gen/vector gen/int)]
                (= v v)))
