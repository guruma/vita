(ns milk.time
  (:require-macros
   [cljs.core.async.macros :as m :refer [go]])
  (:require
   [cljs.core.async :refer [<! >! chan close! sliding-buffer put! alts! take! timeout pub sub unsub]]))


(def ^:private current-time* (atom 0))
(def ^:private delta-time* (atom 0))
(def ^:private delta-time-publisher* (chan))
(def ^:private delta-time-publication* (pub delta-time-publisher* #(:topic %)))



(defn subscribe-delta-time []
  (let [ch (chan 10)]
    (sub delta-time-publication* :time ch)
    ch))


(defn unsubscribe-delta-time [ch]
  (unsub delta-time-publication* :time ch)
  (close! ch))


(defn ^:milk init! []
  (reset! current-time* (.now js/Date)))


(defn ^:milk update! []
  (let [new-t (.now js/Date)
        old-t @current-time*
        dt (- new-t old-t)]
    (go
      (>! delta-time-publisher* {:topic :time :dt dt}))
    (reset! delta-time* dt)
    (reset! current-time* new-t)))


;; ========
;; publics.

(defn current-time [] @current-time*)
(defn delta-time   [] @delta-time*)







;;------------------------
;; channel publish example

;; ; publisher is just a normal channel
;; (def publisher (chan))

;; ; publication is a thing we subscribe to
;; (def publication
;;   (pub publisher #(:topic %)))

;; ; define a bunch of subscribers
;; (def subscriber-one (chan))
;; (def subscriber-two (chan))
;; (def subscriber-three (chan))

;; ; subscribe
;; (sub publication :account-created subscriber-one)
;; (sub publication :account-created subscriber-two)
;; (sub publication :user-logged-in  subscriber-two)
;; (sub publication :change-page     subscriber-three)
