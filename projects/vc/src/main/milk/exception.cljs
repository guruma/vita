(ns milk.exception
  (:require-macros [vita.logger :as vcl])
  (:require [milk.event :as event]))


(defn err-handle [& {:keys [ga info err]}]
  (vcl/ERROR "ERROR!  ga = %o, info = %s, err = %o"
             (if ga (aget ga "name"))
             info
             err))

  ;; 문제있는 컴포넌트를 가지고 있는 가톰을 폭파시킨다...
  ;; (if ga
  ;;   (event/del-gatom ga))
