(ns vita.net.last-packet)



(def last-packets* (atom {})) ;; { adjust-cmd : packet}


(defn- adjust-cmd
  "Qjoin, Rjoin -> join"
  [cmd]
  (->> cmd
       name
       rest
       (apply str)))



(defn get-last-packet [p]
  (when-let [cmd (:cmd p)]
    (let [k (adjust-cmd cmd)]
      (get @last-packets* k))))


(defn store [data]

  (when-let [cmd (:cmd data)]
    (let [adjusted-cmd (adjust-cmd cmd)]
      (swap! last-packets* assoc adjusted-cmd data)))

  data)
