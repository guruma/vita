(ns vita.net.core
  (:require-macros [vita.logger :as vcl])
  (:require
   [milk.network.websocket :as ws]
   [vita.net.last-packet :as last-packet]
   [vita.net.util :as net.util]
   [vita.url]
   ))


(def COMMON_SOCK_INFO [:common vita.url/APP_GAME])


(def GAME_SOCK_INFO [:game vita.url/APP_GAME])


;; 연결은 1회에 한한다.
(def first?* (atom true))


(declare send)

(defn- retry [p]
  (when-let [data (last-packet/get-last-packet p)]
    (vcl/DEBUG "==== retry packet ====" data)
    (send data)
    nil))



;;-----------------
;; public



(defn connect [sock-info]
  (if @first?*
    (do (reset! first?* false)
        (ws/connect (net.util/adjust-sock-info sock-info))
        :init)
    :connected))



(defn close [sock-info]
  (ws/close (net.util/adjust-sock-info sock-info)))



(defn send
  ([data]
     (send COMMON_SOCK_INFO data))

  ([sock-info data]

     (let [data' (->> (net.util/with-scene data sock-info)
                      (net.util/with-tk)
                      (last-packet/store))]

       ;; 원소켓 정책으로 일반 씬 패킷은 GAME 소켓으로 단일화됨
       (ws/send GAME_SOCK_INFO data'))))



(defn dequeue [sock-info]

  (when-let [p (->> (net.util/adjust-sock-info sock-info)
                    (ws/dequeue))]
    (cond

     (net.util/ignore? p)
     nil

     (net.util/retry? p)
     (retry p)

     (net.util/valid? p sock-info)
     p

     :else
     (do (vcl/WARN "unknown packet check!" p)
         p))))
