(ns vita.net.util)


(defn with-tk [data]
  (assoc data :tk (vita.my/get-token)))


(defn with-scene [data sock-info]
  (if-not (:scene data)
    (assoc data :scene (first sock-info))
    data))


(defn adjust-sock-info [sock-info]
  [:game (second sock-info)])


(defn ignore? [p]
  (= :Recho (get-in p [:val :cmd])))


(defn retry? [p]
  (= -2002 (or (get-in p [:val :data :err])
               (get-in p [:val :err]))))


(defn valid? [p sock-info]
  (or (= :common (get-in p [:val :scene]))
      (= (get-in p [:val :scene]) (first sock-info))))
