(ns vita.dialog-manager
  (:require-macros [milk.macro :refer [letc]])
  (:require [milk.gatom :as gatom]
            [milk.event :as event]
            [milk.screen.canvas]
            [milk.component.box-collider :as component.box-collider]))


(defn adjust-box-z [ga base-z]
  (letc ga [box-collider :box-collider]
        (when box-collider
          (let [z (aget box-collider :z)]
            (aset box-collider :z (+ z base-z)))))
  ga)


(defn adjust-behaviour-order [ga base-z]
  (doseq [comp (gatom/get-behaviours ga)]
    (let [z (or (aget comp :order) 0)]
      (aset comp :order (+ z base-z))))
  ga)


(defprotocol IDialogManager
  (show [this element])
  (hide [this])
  (hide-all [this]))


(defrecord DialogManager [stack]
  IDialogManager
  (show [this element]
    (let [make-fn (:make-fn element)
          [screen-w screen-h] (milk.screen.canvas/get-wh)
          base-z (* (inc (count stack)) 100)
          blocker (event/new-gatom "box"
                                   [(component.box-collider/box-collider :w screen-w :h screen-h :z base-z)])
          element' (assoc element
                     :gatoms
                     (conj (mapv #(-> %
                                      (adjust-box-z base-z)
                                      (adjust-behaviour-order base-z))
                                 (make-fn))
                           blocker))]
      (when-let [show-fn (:show-fn element)]
        (show-fn))
      (DialogManager. (conj stack element'))))

  (hide [this]
    (if (empty? stack)
      this

      (let [peeked (peek stack)
            poped (pop stack)
            gatoms (:gatoms peeked)]

        (doseq [ga gatoms]
          (event/del-gatom! ga))
        (DialogManager. poped))))

  (hide-all [this]
    (if (empty? stack)
      this
      (let [peeked (peek stack)
            hide-fn (:hide-fn peeked)]
        (hide-fn)
        (DialogManager. [])))))
