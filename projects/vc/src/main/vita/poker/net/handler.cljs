(ns vita.poker.net.handler
  (:require-macros
   [vita.config.macros :as vcm]
   [cljs.core.async.macros :as m :refer [go]]
   [vita.logger :as vcl])
  (:require
   [cljs.core.async :as async :refer [<! >! chan close! sliding-buffer put! alts! take! timeout]]
   [milk.event :as event]

   [vita.poker.room :as r]
   [sonaclo.util :as util]
   [vita.shared.const.err-code :as ERR]

   [vita.poker.manager.betmanager :as bm]
   [vita.poker.manager.usermanager :as um]
   [vita.poker.manager.cardmanager :as cm]

   [vita.poker.manager.tablemanager :as tm]
   [vita.poker.manager.roommanager :as rm]
   [vita.poker.manager.lobbymanager :as lm]
   [vita.poker.manager.buttonmanager :as btnm]
   [vita.poker.manager.chatmanager :as chatm]
   [vita.scene.common.bar.bar :as barm]
   [vita.scene.common.dialog.buy_result :as buy_result]
   [vita.resource.sound :as sound]
   [vita.app :as app]
   [vita.my :as my]

   [vita.poker.net.sender :as sender]
   [vita.poker.const.const :as CONST]
   [vita.poker.const.offset :as offset :refer [offset-map*]]
   [vita.poker.const.pos :refer [pos-map*] :as const.pos]
   [vita.poker.net.check]
   [vita.poker.animation.move-card]
   [vita.poker.animation.flip-card]
   [vita.poker.net.core]
   [vita.net.core :as net]
   [sdk.facebook.core :as fb]
   ))



(defn room->gatom

  "서버에서 보내온 room 정보를 토대로 gatom 을 생성한다."

  [{:keys [pot round-pot users dealer-user-sid community-cards]}]

  ;; 유저 기본 뷰
  (doseq [[_ u] (r/users)]
    (let [seat (:seat u)]
      (if (and seat (not= seat :none))
        (um/new-user seat u))))

  ;; pot
  (let [pot' (- pot round-pot)]
    (if (and pot' (< 0 pot'))
      (tm/new-pot pot')))

  ;; 게임 중인 뷰에 한해서 그린다.
  (when (r/is-playing-room?)

    ;; community cards
    (doseq [[idx c] (util/with-idx  community-cards)]
      (let [pos (map + (pos-map* :cmm-cards)
                     (map #(* idx %)
                          (offset-map* :cmm-inteval)))]
        (cm/new-card :card c :pos pos)))

    ;; dealer mark
    (if dealer-user-sid
      (tm/new-dealer-mark dealer-user-sid))

    (doseq [[_ u] (r/users)]

      (let [seat (:seat u)]
        (when (and seat (not= seat :none))

          ;; cardpair
          (let [cards (:cards u)]
            (doseq [idx (range (count cards))]
              (tm/new-cardpair-on (:seat u) idx)))

          ;; chip
          (let [bet (:bet u)]
            (if (and bet (< 0 bet))
              (bm/update-bet (:seat u)))))
        ))
    ))

(defn common-cmd? [cmd]
  (contains? #{:Rpayment :Rcatalog} cmd))


(defn lobby-cmd? [cmd]
  (contains? #{:Raccount
               :Btimeout
               :Bfreecoin
               :Rroomlist
               :Rfreecoin
               :Rjoinroom
               :Rquit}
             cmd))


(defn game-cmd? [cmd]
  (contains? #{:Bbet
               :Rbet
               :Bpreflop
               :Bturn
               :Briver
               :Bflop
               :Bbetend
               :Rquit}
             cmd))


(defn valid-room-id? [p]
  (let [room-id (get-in p [:data :room-id])]

    (if (or (nil? room-id) (= (r/room-id) room-id))
      true)))


(defmulti handle

  "server 데이터 -> cmd 디스패치"

  (fn [p]
    (let [cmd (:cmd p)]
      ;; (vcl/DEBUG "==================== RECEIVE ======================")
      ;; (vcl/DEBUG (str cmd m))

      (cond

       (common-cmd? cmd)
       cmd

       (not (valid-room-id? p))
       :wrong-room-id

       ;; 유저가 로비에 있을때는 로비관련 패킷만 접수한다.
       (and (r/user-not-in-room?) (not (lobby-cmd? cmd)))
       :in-lobby-wrong-packet

       :else
       cmd))))


(defmethod handle :in-lobby-wrong-packet [a]
  (vcl/WARN "In lobby - but wrong packet =" a))


(defmethod handle :wrong-room-id [a]
  (vcl/WARN "unmatched room-id, %s" a))


(defmethod handle :Btimeout [_]
  (app/dialog:show {:make-fn #(vita.scene.common.dialog.timeout/dialog)}))


(defmethod handle :Bfreecoin [{{:keys [event-type money]} :data}]
  (when (and event-type money)
    (app/dialog:show {:make-fn #(vita.scene.common.dialog.freecoin/dialog event-type money (fn [] (sender/v-send :freecoin)))})))


(defmethod handle :Rpayment [{{:keys [status account]} :data}]
  (if (= status "completed")
    (let [{:keys [user game]} account]
      (my/set-user user)
      (my/set-game game)
      (barm/update-money)
      (buy_result/show (:money user)))))


(defn send-payment-result-to-game-server [result]
  (vcl/INFO "====== send-payment-result-to-game-server ======")
  (vcl/INFO result)
  (net/send {:cmd :Qpayment :data result}))


(defmethod handle :Rcatalog [{product-info :data}]
  (fb/invoke-payment-dialog product-info send-payment-result-to-game-server))


(defmethod handle :Raccount [{{:keys [account]} :data}]
  (let [{:keys [user game]} account]
    (my/set-user user)
    (my/set-game game)
    (barm/update-money)))


(def first-roomlist? (atom true))

(defmethod handle :Rroomlist [{{:keys [roomlist cur-page max-page err]} :data}]
  (vita.poker.net.check/timer-end)

  (cond

   ;; 에러 패킷 처리
   (and err (not= err ERR/ERR_NONE))
   (app/dialog:show {:make-fn #(vita.scene.common.dialog.err/dialog err (fn []))})

   ;; 룸에 있을때는 룸리스트 패킷을 무시한다.
   (r/in-room?)
   (vcl/WARN "wrong packet, you should be poker lobby")

   :else ;; 룸리스트는 벡터 타입이여야 한다
   (let [roomlist (vec roomlist)]
     (lm/page-btn-check cur-page max-page)
     (r/set-cur-page cur-page)
     (r/set-max-page max-page)
     (r/set-roomlist roomlist)
     (lm/new-page roomlist)

     (when @first-roomlist?
       ;; 블락해제
       (app/dialog:hide)
       (reset! first-roomlist? false))
     )))


(defmethod handle :Rjoinroom [{{:keys [room]} :data err :err}]

  ;; 입력 블락 해제
  (vita.poker.net.core/block-off)

  (cond

   ;; 에러 패킷 처리
   (not= err ERR/ERR_NONE)
   (app/dialog:show {:make-fn #(vita.scene.common.dialog.err/dialog err (fn [] (sender/v-send :roomlist (r/get-cur-page))))})

   ;; 룸에 있을때는 Rjoinroom 패킷 무시
   (r/in-room?)
   (vcl/WARN "un valid Rjoinroom packet, you already in room")

   :else
   (do
     (r/set-room room)
     (r/enter-room)

     (lm/clear-all) ; 로비 클리어

     (rm/room-init) ; 룸 초기화

     (sound/pause :bgm-main)

     (room->gatom room))))


(defmethod handle :Bjoinroom [{{:keys [user]} :data}]

  (r/add-user user)

  (chatm/chat (util/ellipsis (:name user) CONST/NAME_LENGTH) " joined")
  )


(defmethod handle :Rseat [{{:keys [seat]} :data err :err}]

  (if (not= err ERR/ERR_NONE)

    (app/dialog:show {:make-fn #(vita.scene.common.dialog.err/dialog err (fn []))})

    ;; TODO(kep)
    (do (sender/v-send :account)
        (rm/seat-room seat))))


(defmethod handle :Bseat [{{:keys [seat user]} :data}]

  (event/tag-message! :discard-render :animation)

  ;; 유저 정보 저장
  (r/add-user user)

  (chatm/chat (util/ellipsis (:name user) CONST/NAME_LENGTH) " seated")

  ;; 유저 gatom 생성.
  (um/new-user seat user))


(defmethod handle :Rstandup [{err :err}]
  (if (not= err ERR/ERR_NONE)
    (app/dialog:show {:make-fn #(vita.scene.common.dialog.err/dialog err (fn []))})))


(defmethod handle :Bstandup [{{:keys [pot seat sid]} :data}]

  (let [quit-cards (r/get-from-user :cards sid)
        quit-name (r/get-from-user :name sid)

        timer-seat (tm/get-timer-seat)]
    (if (= timer-seat seat)
      (tm/timer-stop))

    ;; 자기 자신일때 처리
    (when (= sid (r/get-sid))
      (let [sid (r/get-sid)
            my-cards (r/get-from-user :cards sid)
            my-seat (r/get-from-user :seat sid)]
        ;; 움직이는 애니메이션 제거
        (event/tag-message! :discard-render :animation)
        ;; 룸에서 자신 제거
        (r/set-room (update-in (r/get-room) [:users] #(dissoc % sid)))
        ;; 오픈되어있는 내카드 있으면 끄기
        (cm/del-card-by-seat my-seat)
        ;; 기타 룸 버튼 및 설정 처리
        (rm/standup-room)

        ;; TODO(kep)
        (sender/v-send :account)))

    (chatm/chat (util/ellipsis quit-name CONST/NAME_LENGTH) " standup")
    (r/reset-pot pot)
    ;; WARN 위 처리 로직 이후에 remove-user 를 해야함. 위에서 old-room 정보를 사용함 (관련이슈 VT-301)
    (r/remove-user sid)
    (rm/someone-quit-room seat quit-cards)))


(defmethod handle :Bquit [{{sid :sid} :data}]

  (let [quit-seat (r/get-from-user :seat sid)
        quit-cards (r/get-from-user :cards sid)
        quit-name (r/get-from-user :name sid)]

    (chatm/chat (util/ellipsis quit-name CONST/NAME_LENGTH) " quit")

    (r/remove-user sid)

    (let [timer-seat (tm/get-timer-seat)]
      (if (= timer-seat quit-seat)
        (tm/timer-stop)))

    (rm/someone-quit-room quit-seat quit-cards)))


(defmethod handle :Rquit [p]

  ;; 애니메이션 끄기
  (event/tag-message! :discard-render :animation)

  ;; rotate 원위치
  (const.pos/recover-rotate)

  ;; 응답 여부 관계없이 클라이언트는 나간다.
  (rm/clear-room)

  (lm/lobby-init)

  (sound/play :bgm-main)

  (chatm/clear-all)

  ;; 입력 블락 해제
  (vita.poker.net.core/block-off)

  ;; account 갱신 요청
  (sender/v-send :account)
  ;; roomlist 갱신 요청
  (sender/v-send :roomlist (r/get-cur-page)))


;;------------------------------------------------
;; 본격적인 게임 진행 관련 프로토콜
;;------------------------------------------------

(defmethod handle :Bstart [{{:keys [users dealer-sid]} :data}]

  ;; 지난 잔상 및 가톰 말끔히 정리
  (event/tag-message! :discard-render :animation)
  (tm/game-clear)

  ;; 룸 데이터 갱신
  (r/set-users users)
  (r/reset-in-room :dealer-user-sid dealer-sid)

  ;; 게임 시작
  (tm/game-start dealer-sid users))


(defmethod handle :Bpreflop [{{:keys [users]} :data}]

  (r/set-users users)

  (let [sorted-seat (r/sorted-seat-by-dealer)
        sorted-seat-cards (map #(vector % (:cards (r/get-user-by-seat %)))
                               sorted-seat)]

    (vcl/NOTICE sorted-seat-cards)
    (go
      (loop [seat-cards sorted-seat-cards idx 0]

        (when (seq seat-cards)

          ;; 한장씩 배포
          (doseq [[seat cards] seat-cards]
            (when (r/in-room?)
              (when-let [card (first cards)]
                ;; 카드 이동
                (<! (vita.poker.animation.move-card/user-card-hand-out seat idx))

                (when (r/in-room?)
                  (if (= (r/get-my-seat) seat)
                    ;; 내 카드는 카드 오픈
                    (vita.poker.animation.flip-card/flip-user-card seat
                                                                   idx
                                                                   #(cm/new-card
                                                                     :card card
                                                                     :seat seat
                                                                     :pos (const.pos/get-card-pos seat idx)))
                    ;; 다른사람 카드는 뒤집어진 카드그림
                    (tm/new-cardpair-on seat idx))
                  ))))

          (recur (keep (fn [[s cs]] (if (seq (rest cs)) [s (rest cs)]))
                       seat-cards)
                 (inc idx)))))
    ))


(defn cmm-card-open

  ([community-cards idx]
     (let [ch (chan 1)]
       (go
         (let [card (get community-cards idx)
               interval (offset-map* :cmm-inteval)]
           (<! (vita.poker.animation.move-card/cmm-card-hand-out idx interval))
           (vita.poker.animation.flip-card/flip-cmm-card idx
                                                    interval
                                                    #(cm/new-card :card card
                                                                  :pos (const.pos/get-cmm-card-pos idx interval))))
         (close! ch))
       ch))

  ([community-cards]
     (let [ch (chan 1)]
       (go
         (doseq [[_ idx] (map vector community-cards (iterate inc 0))]
           (when (r/in-room?)
             (<! (cmm-card-open community-cards idx))))
         (close! ch))
       ch)))




(defmethod handle :Bflop [{{:keys [community-cards]} :data}]

  (r/reset-in-room :community-cards community-cards)

  ;; 커뮤니티 카드 3장 연속 오픈 애니메이션
  (cmm-card-open community-cards))


(defmethod handle :Bturn [{{:keys [community-cards]} :data}]

  (r/reset-in-room :community-cards community-cards)

  ;; 커뮤니티 네번째 카드 오픈
  (cmm-card-open community-cards 3))


(defmethod handle :Briver [{{:keys [community-cards]} :data}]

  (r/reset-in-room :community-cards community-cards)

  ;; 커뮤니티 다섯번째 카드 오픈
  (cmm-card-open community-cards 4))


(defmethod handle :Rbet [{err :err}]
  (if (not= err ERR/ERR_NONE)
    (vcl/ERROR nil "ERROR :: Rbet, err = %o" err)))


(defmethod handle :Bbet [{{:keys [pot
                                  round-bet
                                  raise-bet
                                  cur-user-select-list
                                  cur-user-sid
                                  last-user
                                  last-user-sid]} :data}]

  (if pot
    (r/reset-pot pot))

  (if round-bet
    (r/reset-in-room :round-bet round-bet))

  (if raise-bet
    (r/reset-in-room :raise-bet raise-bet))

  (tm/game-bet)

  ;; last-user 베팅 애니메이션 on
  (when last-user-sid
    (let [old-bet (get-in (r/users) [last-user-sid :bet])
          new-bet (last-user :bet)
          d-bet (- new-bet old-bet)]

      (r/reset-user last-user-sid last-user)
      (tm/bet-action last-user d-bet)))

  ;; cur-user 유저 타이머 on
  (when cur-user-sid
    (let [cur-user-seat (r/get-from-user :seat cur-user-sid)]
      (um/update-user :state nil cur-user-seat)
      (tm/timer-start cur-user-seat))

    ;; 자신의 차례이면 게임버튼 활성화
    (when (= cur-user-sid (r/get-sid))

      (r/update-raise-bet :min)

      (let [k (btnm/get-checked)
            s (set cur-user-select-list)]

        ;; 자동 체크 버튼 켜진게 있으면 해당 로직 실행
        (cond
         (and (= k :chk-btn-call)
              (contains? s :call))
         (do (btnm/game-play-btns-off)
             (js/setTimeout #(sender/v-send :call) 1000))

         (and (= k :chk-btn-raise)
              (contains? s :raise))
         (do (btnm/game-play-btns-off)
             (js/setTimeout #(sender/v-send :raise) 1000))

         (and (= k :chk-btn-fold)
              (contains? s :fold))
         (do (btnm/game-play-btns-off)
             (js/setTimeout #(sender/v-send :fold) 1000))

         (and (= k :chk-btn-check)
              (contains? s :check))
         (do (btnm/game-play-btns-off)
             (js/setTimeout #(sender/v-send :check) 1000))

         ;; 유저 선택
         :else (do
                 (btnm/game-setting-btns-on)
                 (btnm/game-play-btns-on cur-user-select-list))

         )))))


(defmethod handle :Bbetend [{{:keys [pot users]} :data}]
  (r/set-users users)
  (r/reset-pot pot)

  (tm/game-betend users))


(defmethod handle :Bend [{{:keys [community-cards winner players room overage]} :data}]

  (btnm/game-play-btns-off)
  (tm/timer-stop)

  ;; 남은 커뮤니티 카드 모두 오픈
  (doseq [[idx card] (map vector (iterate inc 0) community-cards)]
    (if (not (cm/exist-card? card))
      (cmm-card-open community-cards idx)))

  (let [old-room (r/get-room)]

    ;; 게임 끝난 상태의 (초기화된) 룸 정보로 갱신
    (r/set-room room)

    ;; 약간 시간차를 두고 게임 종료처리
    (js/setTimeout #(when (r/in-room?)
                      ;; 과거 룸정보로 종료처리 뷰를 보여줌
                      (tm/game-end winner players old-room overage))
                   2000))

  ;; 자동 리필로 인해 계정 돈이 바뀔 수 있음으로, 게임 끝날때마다 account 정보 갱신한다.
  ;; TODO 리팩토링
  ;; 패킷 낭비임.
  ;; TODO(kep)
  (js/setTimeout #(sender/v-send :account)
                   3000))


(defmethod handle :default [a]
  ;; TODO 에러 핸들링 처리
  (vcm/DEBUG "ERROR: unknown handle type!!!" a))
