(ns vita.poker.net.sender
  (:require-macros [vita.config.macros :as vcm]
                   [vita.logger :as vcl])
  (:require
   [milk.event :as event]
   [milk.component.box-collider :as comp.box-collider]
   [vita.poker.room :as r]
   [vita.poker.const.pos :as const.pos]
   [vita.app :as app]
   [vita.my :as my]
   [vita.scene.common.dialog.buy-in :as dialog.buy-in]
   [vita.resource.sound :as sound]
   [vita.net.core :as vita.network]
   [vita.poker.net.check]
   [vita.poker.net.core]
   ))


;; 너무 잦은 통신을 방지하기 위해 마지막 send 시간을 저장해 놓았다가 보낸다.
(def last-time (atom 0))

(defn is-valid-time? []
  #_(let [lt @last-time
        now (.getTime (js/Date.))]
    (if (<= 1000 (- now lt))  ;; 버튼간 통신 속도는 1.5 초 정도 시간텀을 둔다.
      (do (reset! last-time now)
          true)
      false))
  ;; todo 안쓰임....
  true)



(defn sock-init [s]
  (def sock s))


(defn -send [data]
  (vita.network/send sock data))


;;--------------------------------------------------------
;; 서버 연결 통신
;;--------------------------------------------------------

(defmulti v-send (fn [k & a] k))


;; (defmethod v-send :login
;;   [_]
;;   (-send {:cmd :Qlogin :user-id (r/get-user-id)}))


(defmethod v-send :payment
  [_]
  (-send {:cmd :Qpayment :data {} :scene :common}))


(defmethod v-send :account
  [_]
  (-send {:cmd :Qaccount :data {}}))


(defmethod v-send :roomlist
  ([_]
     (vita.poker.manager.roommanager/clear-room)
     (-send {:cmd :Qroomlist :data {}}))
  ([_ page]
     (vita.poker.manager.roommanager/clear-room)
     (-send {:cmd :Qroomlist :data {:page page}})))


(defmethod v-send :freecoin
  [_]
  (-send {:cmd :Qfreecoin :data {}}))


(defmethod v-send :join
  [_ room-id]
  (when (is-valid-time?)
    ;; 화면 입력 블락.
    (vita.poker.net.core/block-on)
    (-send {:cmd :Qjoinroom :data {:room-id room-id}})))


(defn send-seat [seat buy-in auto-refill]
  (let [state (r/get-from-user :state (r/get-sid))]
    (if (or (nil? state) (= :join state))
      (-send {:cmd :Qseat
              :data {:seat seat
                     :buy-in buy-in
                     :auto-refill auto-refill
                     :room-id (r/get-from-room :room-id)}})
      (vcl/DEBUG "can't send seat! state =" state))))


(defmethod v-send :seat
  [_ seat]
  (let [money (my/get-money)
        bb (r/get-from-room :bb)
        on-click #(send-seat seat %1 %2)]

    (app/dialog:show {:make-fn #(dialog.buy-in/dialog on-click money bb)})))


(defmethod v-send :standup
  [_]
  (if (is-valid-time?)
    (let [seat (r/get-from-user :seat (r/get-sid))]
      (when (some? seat)
        (-send {:cmd :Qstandup :data {:room-id (r/get-from-room :room-id)}})))))


(defmethod v-send :quit
  [_]
  ;; 화면 입력 블락.
  (vita.poker.net.core/block-on)
  ;; Qquit 보내기
  (-send {:cmd :Qquit :data {:room-id (r/get-from-room :room-id)}}))



;;--------------------------------------------------------
;; 게임 관련 통신
;;--------------------------------------------------------

(defmethod v-send :bet
  [_]
  (let [round-bet (r/get-from-room :round-bet)]

    (-send {:cmd :Qbet
            :data {
                   :room-id (r/get-from-room :room-id)
                   :select :bet
                   :bet round-bet}})))


(defmethod v-send :call
  [_]
  (let [sid (r/get-sid)
        round-bet (r/get-from-room :round-bet)
        my-bet (r/get-from-user :bet sid)
        my-money (r/get-from-user :money sid)
        call (- round-bet my-bet)]

    (-send {:cmd :Qbet
            :data
            {:room-id (r/get-from-room :room-id)
             :sid sid
             :select :call
             :bet (if (< my-money call)
                    my-money
                    call)}})))


(defmethod v-send :raise
  [_]
  (let [sid (r/get-sid)
        raise-bet (r/get-raise-bet)
        my-bet (r/get-from-user :bet sid)
        my-money (r/get-from-user :money sid)]

    (-send {:cmd :Qbet
            :data {
                   :room-id (r/get-from-room :room-id)
                   :sid sid
                   :select :raise
                   :bet raise-bet}
            })))


(defmethod v-send :check
  [_]

  (-send {:cmd :Qbet
          :data {:room-id (r/get-from-room :room-id)
                 :select :check}}))


(defmethod v-send :fold
  [_]

  (-send {:cmd :Qbet
          :data {
                 :room-id (r/get-from-room :room-id)
                 :select :fold}}))


;;-------------------
;; test 용 패킷

(defmethod v-send :echo
  [_]
  (vita.network/send {:cmd :Qecho :data {}}))
