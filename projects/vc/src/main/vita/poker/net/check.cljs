(ns vita.poker.net.check
  (:require-macros
   [cljs.core.async.macros :as m :refer [go]]
   [vita.logger :as vcl])
  (:require
   [cljs.core.async :as async :refer [<! >! chan close! sliding-buffer put! alts! take! timeout]]))


;;----------------
;; roomlist 패킷이 안올때를 대비해서 임시로 재시도 하는 로직

(def retry-cnt 5)
(def retry-time 3000)
(def lp-time (atom nil))


(defn timer-end []
  (vcl/INFO "timer end!!!!!!!!!!!")
  (reset! lp-time nil))


(defn timer-init []
  (vcl/INFO "timer init")
  (reset! lp-time (.now js/Date)))


(defn timer-start [callback]
  (go
    (vcl/INFO "!!!!!!! timer start")
    (loop [i retry-cnt]
      (vcl/INFO "loop")
      (<! (timeout retry-time))

      (if (<= i 0)
        (timer-end)
        (when @lp-time
          (callback)
          (timer-init)
          (recur (dec i)))))))
