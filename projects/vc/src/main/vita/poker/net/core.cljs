(ns vita.poker.net.core
  (:require [milk.event :as event]
            [milk.screen.canvas]
            [vita.poker.behaviour.ui-text :refer [ui-text]]
            [vita.scene.common.behaviour.loading :refer [loading]]
            [milk.component.box-collider :as comp.box-collider]))


(def block-ga* (atom []))


(defn block-on []

  (when-not (seq @block-ga*)
    ;; 입력 막기
    (let [[screen-w screen-h] (milk.screen.canvas/get-wh)
          ga (event/new-gatom "box"
                              [(comp.box-collider/box-collider :w screen-w
                                                               :h screen-h
                                                               :z 99)]
                              :pos [0 0])
          text-ga (event/new-gatom "wait"
                                   [(loading :back-color :#000000
                                             :alpha 0.2)]
                                   :pos [640 450])
          ]

      (swap! block-ga* conj ga)
      (swap! block-ga* conj text-ga)
      )))


(defn block-off []
  (let [gs @block-ga*]
    (doseq [ga gs]
      (event/del-gatom ga))
    (reset! block-ga* [])))
