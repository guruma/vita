(ns vita.poker.manager.roommanager
  (:require
   [milk.event :as event]

   [vita.poker.room :as r]
   [vita.poker.behaviour.ui-image :refer [ui-image]]
   [vita.poker.manager.manager :as mm]
   [vita.poker.manager.cardmanager :as cm]
   [vita.poker.manager.betmanager :as bm]
   [vita.poker.manager.usermanager :as um]
   [vita.poker.manager.chatmanager :as chtm]

   [vita.poker.const.const :as const]
   [vita.poker.const.pos :as const.pos]
   [vita.poker.manager.tablemanager :as tm]
   [vita.poker.manager.buttonmanager :as btnm]

   [vita.resource.image :as image]
   [vita.scene.common.bar.bar :as bar]

   [vita.poker.behaviour.ui-chip :refer [ui-chip]]
   [vita.poker.behaviour.ui-card :refer [ui-card]]
   ))


(def gs* (atom {}))


(defn clear-all []
  (mm/clear-all gs*))


(defn room-init []

  ;; 배경
  (mm/g-add gs* :background
            :ga (event/new-gatom "background"
                                 [(ui-image :img (image/load :poker-back) :order -1)]
                                 :pos [0 0]))

  ;; 룸 퇴실 버튼 초기화
  (btnm/room-out-btn-init)

  ;; 빈자리 버튼 초기화
  (btnm/init-emptyseat)

  ;; pot 현황 정보
  (tm/pot-info-window-on)

  ;; TODO(jdj) 기능 완성
  (chtm/chat-init)

  ;; WARN:: 룸에서 칩, 딜러표시, 카드페어, 턴표시 위치 테스트 용도임 - 지우지말것.
  ;; (event/new-gatom "aaa"
  ;;                  [(ui-image :img-k :d)]
  ;;                  :pos (const.pos/get-dealer-mark-pos :seat0))
  ;; (event/new-gatom "aaa"
  ;;                  [(ui-image :img-k :d)]
  ;;                  :pos (const.pos/get-dealer-mark-pos :seat1))
  ;; (event/new-gatom "aaa"
  ;;                  [(ui-image :img-k :d)]
  ;;                  :pos (const.pos/get-dealer-mark-pos :seat2))
  ;; (event/new-gatom "aaa"
  ;;                  [(ui-image :img-k :d)]
  ;;                  :pos (const.pos/get-dealer-mark-pos :seat3))
  ;; (event/new-gatom "aaa"
  ;;                  [(ui-image :img-k :d)]
  ;;                  :pos (const.pos/get-dealer-mark-pos :seat4))

  ;; (event/new-gatom "aaa"
  ;;                  [(ui-card :card :card-back-r)]
  ;;                  :pos (const.pos/get-cardpair-pos :seat0))
  ;; (event/new-gatom "aaa"
  ;;                  [(ui-card :card :card-back-l)]
  ;;                  :pos (const.pos/get-cardpair-pos :seat1))
  ;; (event/new-gatom "aaa"
  ;;                  [(ui-card :card :card-back-s)]
  ;;                  :pos (const.pos/get-cardpair-pos :seat2))
  ;; (event/new-gatom "aaa"
  ;;                  [(ui-card :card :card-back-r)]
  ;;                  :pos (const.pos/get-cardpair-pos :seat3))
  ;; (event/new-gatom "aaa"
  ;;                  [(ui-card :card :card-back-l)]
  ;;                  :pos (const.pos/get-cardpair-pos :seat4))

  ;; (event/new-gatom "aaa"
  ;;                  [(ui-chip :bet 100)]
  ;;                  :pos (const.pos/get-chip-pos :seat0))
  ;; (event/new-gatom "aaa"
  ;;                  [(ui-chip :bet 100)]
  ;;                  :pos (const.pos/get-chip-pos :seat1))
  ;; (event/new-gatom "aaa"
  ;;                  [(ui-chip :bet 100)]
  ;;                  :pos (const.pos/get-chip-pos :seat2))
  ;; (event/new-gatom "aaa"
  ;;                  [(ui-chip :bet 100)]
  ;;                  :pos (const.pos/get-chip-pos :seat3))
  ;; (event/new-gatom "aaa"
  ;;                  [(ui-chip :bet 100)]
  ;;                  :pos (const.pos/get-chip-pos :seat4))

  ;; (event/new-gatom "aaa"
  ;;                  [(ui-image :img-k :myturn)]
  ;;                  :pos (const.pos/get-myturn-pos :seat0))
  ;; (event/new-gatom "aaa"
  ;;                  [(ui-image :img-k :myturn)]
  ;;                  :pos (const.pos/get-myturn-pos :seat1))
  ;; (event/new-gatom "aaa"
  ;;                  [(ui-image :img-k :myturn)]
  ;;                  :pos (const.pos/get-myturn-pos :seat2))
  ;; (event/new-gatom "aaa"
  ;;                  [(ui-image :img-k :myturn)]
  ;;                  :pos (const.pos/get-myturn-pos :seat3))
  ;; (event/new-gatom "aaa"
  ;;                  [(ui-image :img-k :myturn)]
  ;;                  :pos (const.pos/get-myturn-pos :seat4))

  )


(defn seat-room [seat]

  ;; 앉기 성공 -> 모든 '앉기' 버튼 off
  (btnm/clear-emptyseat)

  ;; buychips 버튼 비활성화. VT-400
  (bar/btn-bar-buychips-off)

  ;; 게임관련 버튼 및 창 활성화
  (btnm/game-play-btns-init)
  (btnm/game-setting-btns-init)
  (btnm/chk-btn-init)

  (tm/bet-info-window :init)

  ;; 회전 적용
  (const.pos/rotate-seat seat)
  (um/rotate-seat)
  (bm/rotate-seat)
  (tm/rotate-seat)
  )


(defn standup-room []

  ;; 회전 원위치
  (const.pos/recover-rotate)
  (um/rotate-seat)
  (bm/rotate-seat)
  (tm/rotate-seat)

  ;; buychips 버튼 활성화. VT-400
  (bar/btn-bar-buychips-on)

  ;; 게임버튼 다 끄기.
  (btnm/game-play-btns-clear)
  (btnm/clear-all-chk)
  (btnm/game-setting-btns-clear)
  (tm/bet-info-window :clear)

  ;; 빈자리 버튼 초기화
  (btnm/init-emptyseat)
  )


(defn someone-quit-room [quit-seat quit-cards]

  (tm/reset-pot)
  (um/quit-user quit-seat)
  (tm/user-cardpair-off quit-seat)

  ;; 유저 베팅 금액이 있으면 pot 으로 옮긴다.
  (bm/bet-to-pot quit-seat tm/add-pot)

  (doseq [c quit-cards]
    (cm/del-card c))

  (let [my-seat (r/get-from-user :seat (r/get-sid))]
    (if (some #(= % my-seat) [nil :none]) ;; 서있는 상태라면 빈자리버튼 생성
      (btnm/new-emptyseat-btn quit-seat)))
  )


(defn clear-room []

  ;; buychips 버튼 활성화. VT-400
  (bar/btn-bar-buychips-on)

  (r/clear-room)
  (r/quit-room)

  (clear-all)

  (tm/clear-all)

  (tm/bet-info-window :clear)

  (cm/clear-all)

  (um/clear-all)

  (bm/clear-all)

  (btnm/clear-all)

  (chtm/clear-all)

  )
