(ns vita.poker.manager.betmanager
  (:require-macros
   [cljs.core.async.macros :as m :refer [go]]
   [vita.logger :as vcl])
  (:require
   [cljs.core.async :as async :refer [<! >! chan close! sliding-buffer put! alts! take! timeout]]

   [milk.event :as event]

   [vita.poker.room :as r]
   [sonaclo.util :as util]
   [vita.poker.manager.manager :as mm]
   [vita.poker.const.pos :as const.pos :refer [pos-map*]]
   [vita.poker.const.const :as const.const]

   [vita.poker.behaviour.ui-chip :refer [ui-chip]]
   [vita.poker.behaviour.movedirect :refer [movedirect]]

   [vita.poker.animation.move-chip]

   ))


(def gs* (atom {}))


(defn clear-all []
  (mm/clear-all gs*))


(defn get-all []
  @gs*)


(defn del-bet [k]
  (mm/g-del gs* k))


(defn update-bet
  "룸 정보 조회 -> bet 값을 최신값으로 갱신"

  [seat]

  (when (r/in-room?)
    (let [bet (r/get-from-user-by-seat seat :bet)]
      (if-let [m (mm/g-get gs* seat)]

        ;; 이미 존재하면 값만 갱신
        (let [a (:bet m)]
          (reset! a bet))

        ;; 없으면, 칩 가톰 생성후 저장
        (let [a (atom bet)
              ga (event/new-gatom "chip"
                                  [(ui-chip :bet a)]
                                  :pos (const.pos/get-chip-pos seat))]
          (mm/g-add gs* seat
                    :ga ga
                    :bet a))))))


(defn bet-to-pot
  "user bet chip -> move to pot"
  [seat add-pot]
  (let [{:keys [ga bet]} (mm/g-get gs* seat)]
    (when (and ga bet)
      (event/add-component! ga (movedirect :from (const.pos/get-chip-pos seat)
                                           :to (pos-map* :pot)
                                           :duration const.const/CHIP_MOVE_DUR
                                           :end-callback
                                           #(do (add-pot @bet)
                                                (event/del-gatom ga)))))))

(defn all-to-pot
  "all user bet chip -> move to pot"
  [add-pot]
  (doseq [[seat _] @gs*]
    (bet-to-pot seat add-pot)))


(defn pot-to-user
  "money in pot -> move to user"
  [money seat change-pot change-user-money]
  (change-pot (- money))

  (go
    (<! (vita.poker.animation.move-chip/move-chip money
                                             (pos-map* :pot)
                                             (const.pos/get-seat-pos seat)
                                             const.const/CHIP_MOVE_DUR))
    (when (r/in-room?)
      (change-user-money seat money))))


(defn return-overage
  "all overage money in pot -> move to users"
  [overage change-pot change-user-money old-room]

  (when-let [[money sid] overage]
    (let [seat (get-in old-room [:users sid :seat])]
      (pot-to-user money seat change-pot change-user-money))))


(defn rotate-seat
  "user gatom reposition"
  []
  (mm/rotate-seat gs* identity const.pos/get-chip-pos))
