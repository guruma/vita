(ns vita.poker.manager.manager
  (:require
   [milk.gatom :as gatom]
   [milk.event :as event]
   [milk.component.box-collider :refer [box-collider]]
   [vita.scene.common.behaviour.ui-button :refer [ui-button]]
   [vita.poker.behaviour.reposition :refer [reposition]]
   [vita.poker.behaviour.ui-checkbox :refer [ui-checkbox]]
   [vita.poker.behaviour.ui-change-text :refer [ui-change-text]]

   [vita.resource.image :as image]
   [vita.poker.const.btn :refer [btn-map*]]
   [vita.poker.const.pos :as const.pos :refer [pos-map*]]
   [vita.poker.const.const :as const.const]
   ))


(defn g-get [gs* k]
  (@gs* k))


(defn get-ga [gs* k]
  (:ga (g-get gs* k)))


(defn exist? [gs* k]
  (not (nil? (g-get gs* k))))


(defn g-del [gs* k]
  (if-let [m (g-get gs* k)]
    (event/del-gatom (:ga m))
    (swap! gs* dissoc k)))


(defn g-add [gs* k & args]
  (if (exist? gs* k)
    (g-del gs* k))

  (let [m (apply hash-map args)]
    (swap! gs* assoc k m)))


(defn clear-all [gs*]
  (doseq [[k m] @gs*]
    (g-del gs* k))
  (reset! gs* {}))


(defn rotate-seat
  "seat 을 기준으로 관련있는 가톰의 pos 를 다시 계산한다."
  [gs* make-k make-pos]
  (doseq [seat const.const/SEAT_LIST]
    (let [ga (get (g-get gs* (make-k seat)) :ga)]
      (when ga
        (event/add-component! ga (reposition (make-pos seat)))))))


;;-----------------------
;; modify function

(defn btn-on [gs* k]
  (let [ga (:ga (g-get gs* k))
        comp (gatom/get-b ga :ui-button)]
    (event/send-message! comp :enable-on)))


(defn btn-on? [gs* k]
  (let [ga (:ga (g-get gs* k))]
    (when ga
      (let [comp (gatom/get-b ga :ui-button)]
        (aget comp :enabled?)))))


(defn btn-off [gs* k]
  (let [ga (:ga (g-get gs* k))
        comp (gatom/get-b ga :ui-button)]
    (event/send-message! comp :enable-off)))


(defn check [gs* k]
  (if-let [chk (:check (g-get gs* k))]
    (reset! chk true)))


(defn uncheck [gs* k]
  (if-let [chk (:check (g-get gs* k))]
    (reset! chk false)))


(defn checked? [gs* k]
  (let [chk (:check (g-get gs* k))]
    (boolean @chk)))


;;-----------------------
;; complex helper

(defn new-btn [gs* k & {:keys [button-info pos on-click enable order] :or {}}]

  (let [ga (event/new-gatom (name k)
                            [(ui-button :button-info button-info
                                        :on-click on-click
                                        :enable enable
                                        :order order)]
                            :pos pos)]

      (g-add gs* k :ga ga)))


(defn new-btn-with-k [gs* k & {:keys [on-click enable order] :or {}}]
  (let [button-info (btn-map* k)
        pos (pos-map* k)]
    (new-btn gs* k
             :button-info button-info
             :pos pos
             :on-click on-click
             :enable enable
             :order order)))


(defn new-text-btn [gs* k & {:keys [button-info pos on-click enable text-f text-style text-offset] :or {}}]
  (let [txt-comp (ui-change-text :text-f text-f
                                 :style text-style
                                 :offset text-offset
                                 :enable enable)

        btn-comp (ui-button :button-info button-info
                            :on-click on-click
                            :enable enable)

        enable-on (aget btn-comp :enable-on)
        enable-off (aget btn-comp :enable-off)]

    (aset btn-comp
          :enable-on
          #(do (enable-on) (event/send-message! txt-comp :enable-on)))
    (aset btn-comp
          :enable-off
          #(do (enable-off) (event/send-message! txt-comp :enable-off)))


    (let [ga (event/new-gatom (name k)
                              [btn-comp txt-comp]
                              :pos pos)]
      (g-add gs* k :ga ga))))


(defn new-chk-btn [gs* k & {:keys [chk-img unchk-img pos whz on-check enable] :or {}}]

  (let [[w h z] whz
        enabled?* (atom enable)
        checked?* (atom false)
        table {:normal  [     0  0 w h]
               :over    [     w  0 w h]
               :down    [(* 2 w) 0 w h]
               :disable [(* 3 w) 0 w h]}
        ga (event/new-gatom (name k)
                            [(ui-checkbox :on-check on-check
                                          :enabled?* enabled?*
                                          :checked?* checked?*
                                          :chk-img chk-img
                                          :img unchk-img
                                          :table table)
                             (box-collider :w w :h h :z z)]
                            :pos pos)]

    (g-add gs* k :ga ga :check checked?* :enable enabled?*)))
