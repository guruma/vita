(ns vita.poker.manager.buttonmanager
  (:require-macros [vita.logger :as vcl])
  (:require

   [vita.poker.manager.manager :as mm]
   [vita.poker.room :as r]
   [sonaclo.util :as util]
   [vita.poker.net.sender :as sender]
   [vita.poker.const.btn :refer [btn-map*]]
   [vita.poker.const.pos :as const.pos :refer [pos-map*]]
   [vita.poker.const.offset :refer [offset-map*]]
   [vita.poker.const.const :as const]
   [vita.resource.image :as image]

   [vita.my :as my]
   ))

;;----------------------
;; emptyseat & autoseat buttons

(def seat-gs* (atom {}))


(defn -make-emptyseat-k [seat]
  (util/keyword+ :btn-empty seat))


(defn new-emptyseat-btn [seat]

  (let [k (-make-emptyseat-k seat)
        button-info (btn-map* :btn-empty-sit)
        pos (const.pos/get-seat-pos seat)
        on-click #(sender/v-send :seat seat)]

    (mm/new-btn seat-gs* k
                :button-info button-info
                :pos pos
                :on-click on-click
                :enable true)))


(defn get-random-seat []
  (let [seats (keys (r/get-seat-user))
        empty-seats (reduce (fn [acc s] (disj acc s)) (set const/SEAT_LIST) seats)]
    (if (seq empty-seats)
      (rand-nth (vec empty-seats)))))


(defn init-autoseat []
  (mm/new-btn seat-gs* :autoseat
              :button-info (btn-map* :btn-auto-seat)
              :pos (pos-map* :btn-auto-seat)
              :on-click #(if-let [seat (get-random-seat)]
                           (sender/v-send :seat seat))))


(defn init-emptyseat []

  (init-autoseat)

  ;; 게임 테이블 의자 - 앉기 버튼
  (let [fill-seats (set (keys (r/get-seat-user)))]
    (doseq [s const/SEAT_LIST]
      (if-not (contains? fill-seats s)
        (new-emptyseat-btn s)))))


(defn clear-emptyseat []
  (mm/clear-all seat-gs*))



;;---------------------------
;; play auto check button

(def auto-chk-gs* (atom {}))

(def chk-btns [:chk-btn-call :chk-btn-raise :chk-btn-check :chk-btn-fold])


(defn uncheck-all []
  (doseq [k chk-btns]
    (mm/uncheck auto-chk-gs* k)))


(defn get-checked []
  (if-let [[k _] (first
                  (filter (fn [[_ b]] (true? b))
                          (map (fn [k] [k (mm/checked? auto-chk-gs* k)]) chk-btns)))]
    k))


(defn do-check [k]
  (uncheck-all) ; 4 버튼 중 하나만 켜져야 되기 때문에 다른 체크 모두 off
  (mm/check auto-chk-gs* k))


(defn clear-all-chk []
  (mm/clear-all auto-chk-gs*))


(defn new-chk-btn-with-k [k]
  (let [whz [20 20 10]
        pos (pos-map* k)]
    (mm/new-chk-btn auto-chk-gs* k
                    :chk-img (image/load :check-p)
                    :unchk-img (image/load :check-n)
                    :pos pos
                    :whz whz
                    :on-check #(do-check k)
                    :enable true)))


(defn chk-btn-init []
  (doseq [k chk-btns]
    (new-chk-btn-with-k k)))



;;----------------------------------------
;; game play buttons


(def play-btn-gs* (atom {}))


(def game-play-btns [:btn-bet :btn-call :btn-raise :btn-check :btn-fold])


(defn game-play-btns-clear []
  (mm/clear-all play-btn-gs*))


(defn game-play-btns-on
  ([]
     (doseq [k game-play-btns]
       (mm/btn-on play-btn-gs* k)))

  ([select-lst]
     (doseq [k select-lst]
       (mm/btn-on play-btn-gs* (util/keyword+ :btn- k)))))


(defn btn-on? [k]
  (mm/btn-on? play-btn-gs* k))


(defn game-play-btns-off []
  (doseq [k game-play-btns]
    (mm/btn-off play-btn-gs* k)))


(defn game-play-btns-init []

  ;; call 버튼은 버튼 내에 숫자 표시가 되는 버튼 형식이다.
  (let [callback #(do (game-play-btns-off) (sender/v-send :call))
        text-f #(let [money (min (- (r/get-from-room :round-bet)
                                    (r/get-from-user :bet (r/get-sid)))
                                 (r/get-from-user :money (r/get-sid)))]
                  (if (zero? money)
                    "0"
                    (util/number-abbreviation money)))
        text-style {:font "23pt Arial" :color :#000000 :align :right}
        text-offset (offset-map* :call-text)]

    (mm/new-text-btn play-btn-gs* :btn-call
                     :button-info (btn-map* :btn-call)
                     :pos (pos-map* :btn-call)
                     :on-click callback
                     :enable false
                     :text-f text-f
                     :text-style text-style
                     :text-offset text-offset
                     ))


  (mm/new-btn-with-k play-btn-gs* :btn-raise
                  :on-click #(do (game-play-btns-off) (sender/v-send :raise))
                  :enable false)

  (mm/new-btn-with-k play-btn-gs* :btn-check
                  :on-click #(do (game-play-btns-off) (sender/v-send :check))
                  :enable false)

  (mm/new-btn-with-k play-btn-gs* :btn-fold
                  :on-click #(do (game-play-btns-off) (sender/v-send :fold))
                  :enable false)
  )


;;----------------------------------------
;; bet setting buttons

(def setting-btn-gs* (atom {}))


(def game-setting-btns [:btn-minus :btn-plus :btn-min :btn-half :btn-pot :btn-all-in])


(defn clear-setting-btns []
  (mm/clear-all setting-btn-gs*))


(defn game-setting-btns-init []

  ;; 중앙 하단, 게임 설정 버튼
  (mm/new-btn-with-k setting-btn-gs* :btn-minus
                  :on-click #(r/dec-raise-bet)
                  :enable false)

  (mm/new-btn-with-k setting-btn-gs* :btn-plus
                  :on-click #(r/inc-raise-bet)
                  :enable false)

  (mm/new-btn-with-k setting-btn-gs* :btn-min
                  :on-click #(r/update-raise-bet :min)
                  :enable false)

  (mm/new-btn-with-k setting-btn-gs* :btn-half
                  :on-click #(r/update-raise-bet :half)
                  :enable false)

  (mm/new-btn-with-k setting-btn-gs* :btn-pot
                  :on-click #(r/update-raise-bet :pot)
                  :enable false)

  (mm/new-btn-with-k setting-btn-gs* :btn-all-in
                  :on-click #(r/all-in-raise-bet)
                  :enable false)
  )


(defn game-setting-btns-on []
  (doseq [k game-setting-btns]
    (mm/btn-on setting-btn-gs* k)))

(defn game-setting-btns-off []
  (doseq [k game-setting-btns]
    (mm/btn-off setting-btn-gs* k)))

(defn game-setting-btns-clear []
  (doseq [k game-setting-btns]
    (mm/g-del setting-btn-gs* k)))



;;----------------------------------------
;; 룸 기타 버튼들

(def out-btn-gs* (atom {}))


(declare all-btn-unenable)
(defn room-out-btn-init []
  ;; 좌 상단, 퇴실버튼
  (mm/new-btn-with-k out-btn-gs* :btn-stand-up
                  :on-click #(sender/v-send :standup)
                  :enable true)

  (mm/new-btn-with-k out-btn-gs* :btn-to-lobby
                  :on-click #(do (all-btn-unenable)
                                 (sender/v-send :quit))
                  :enable true)
  )


;;------------------------------------------
;; all clear

(defn all-btn-unenable []
  (doseq [k (keys @seat-gs*)]
    (mm/btn-off seat-gs* k))
  (doseq [k (keys @play-btn-gs*)]
    (mm/btn-off play-btn-gs* k))
  (doseq [k (keys @out-btn-gs*)]
    (mm/btn-off out-btn-gs* k)))



(defn clear-all []
  (mm/clear-all out-btn-gs*)
  (mm/clear-all setting-btn-gs*)
  (mm/clear-all play-btn-gs*)
  (mm/clear-all auto-chk-gs*)
  (mm/clear-all seat-gs*))
