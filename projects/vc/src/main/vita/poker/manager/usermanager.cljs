(ns vita.poker.manager.usermanager
  (:require
   [milk.event :as event]
   [milk.gatom :as gatom]

   [vita.poker.room :as r]
   [sonaclo.util :as util]
   [vita.poker.manager.manager :as mm]
   [vita.poker.const.pos :as const.pos]
   [vita.poker.const.const :as CONST]

   [vita.poker.behaviour.ui-user :refer [ui-user]]
   [vita.poker.behaviour.movedirect :refer [movedirect]]
   ))


(def gs* (atom {}))


(defn clear-all []
  (mm/clear-all gs*))


(def user-state-offset [-35 -35])


(defn -make-user-ga-k [seat]
  (util/keyword+ "user-ga" seat))


(defn new-user [seat user]

  (let [sid (atom (:sid user))
        rname (atom (util/ellipsis (r/get-from-user :name @sid) CONST/NAME_LENGTH))
        state (atom (r/get-from-user :state @sid))
        money (atom (r/get-from-user :money @sid))
        bet (atom (r/get-from-user :bet @sid))
        user-ga (event/new-gatom "user"
                                 [(ui-user (:avatar-url user)
                                           rname
                                           state
                                           user-state-offset
                                           money
                                           bet)]
                                 :pos (const.pos/get-seat-pos seat))]

    ;; 속성을 atom 변수로 만들어 ga 에 넘겨주는 방법으로 값을 공유한다.
    (mm/g-add gs* (-make-user-ga-k seat)
              :ga user-ga
              :sid sid
              :name name
              :state state
              :money money
              :bet bet)))


(defn rotate-seat []
  (mm/rotate-seat gs* -make-user-ga-k const.pos/get-seat-pos))


(defn update-user
  "user 의 k (특정키워드) 값을 새정보로 갱신"

  ([k seat]
     (let [new-v (r/get-from-user-by-seat seat k)]
       (update-user k new-v seat)))

  ([k v seat]
     (let [m (mm/g-get gs* (-make-user-ga-k seat))
           a (get m k)]
       (when a
         (reset! a v)
         ))))


(defn apply-update-user
  [k update-fn seat]
  (let [m (mm/g-get gs* (-make-user-ga-k seat))
        a (get m k)]
    (when a
      (swap! a update-fn))))


(defn quit-user [seat]
  (mm/g-del gs* (-make-user-ga-k seat)))
