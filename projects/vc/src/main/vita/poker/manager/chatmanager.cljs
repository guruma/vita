(ns vita.poker.manager.chatmanager
  (:require
   [milk.event :as event]

   [vita.poker.manager.manager :as mm]

   [vita.scene.common.behaviour.ui-button :refer [ui-button]]
   [vita.poker.behaviour.ui-image :refer [ui-image]]
   [vita.poker.behaviour.ui-chat :refer [ui-chat]]

   [vita.resource.image :as image]
   [sonaclo.util :as util]
   ))


(def pos-map*
  {:dealer-chat [220 815]
   :chat-back [220 825]
   :chat-write [220 935]
   :chat-btn [453 935]
   })


(def gs* (atom {}))


(declare clear-chat)

(defn clear-all []
  (clear-chat)
  (mm/clear-all gs*))


(def chat* (atom []))


(defn clear-chat []
  (reset! chat* []))


(defn add-chat* [s]
  (swap! chat* #(if (<= 5 (count %))
                  (conj (vec (rest %)) s)
                  (conj % s))))


(defn chat [& args]
  (add-chat* (apply str args)))


(defn chat-view-init []
  (let [chat-back-img (image/load :chat-back)
        chat-dealer-tab-img (image/load :chat-dealer)
        chat-text-write-img (image/load :chat-textbox)

        back-ga (event/new-gatom "chat-back"
                                 [(ui-image :img chat-back-img)]
                                 :pos (pos-map* :chat-back))
        m-ga (event/new-gatom "chat-mark"
                              [(ui-image :img chat-dealer-tab-img)]
                              :pos (pos-map* :dealer-chat))

        s-ga (event/new-gatom "chat-text"
                              [(ui-chat chat*)]
                              :pos (pos-map* :chat-back))
        ;; write-ga (event/new-gatom "chat-write"
        ;;                           [(ui-image :img chat-text-write-img)]
        ;;                           :pos (pos-map* :chat-write))
        ]

    (mm/g-add gs* :chat-mark :ga m-ga)
    (mm/g-add gs* :chat-back :ga back-ga)
    ;; (mm/g-add gs* :chat-write :ga write-ga)
    ))


(defn chat-btn-init []
  (let [button-info {:img-k :chat-enter :table (util/button-table [80 20])}


        ;; btn (util/make-btn  20 20 6)
        ;; {:keys [img-k table box]} btn
        ;; {:keys [w h z]} box
        ;; enable* (atom true)

        chat-btn (event/new-gatom "chat-button"
                                  [(ui-button :button-info button-info
                                              :on-click #())]
                                  :pos (pos-map* :chat-btn))]
    (mm/g-add gs* :chat-btn
              :ga chat-btn)))


(defn chat-init []
  (chat-view-init)
  ;;(chat-btn-init)
  )
