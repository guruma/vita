(ns vita.poker.manager.lobbymanager
  (:require-macros [cljs.core.async.macros :as m :refer [go]]
                   [vita.logger :as vcl])
  (:require
   [milk.event :as event]
   [milk.component.box-collider :refer [box-collider]]
   [vita.helper]

   [vita.poker.behaviour.ui-image :refer [ui-image]]
   [vita.poker.behaviour.ui-text :refer [ui-text]]
   [vita.poker.manager.manager :as mm]
   [vita.poker.room :as r]
   [vita.poker.net.sender :as sender]
   [vita.poker.const.btn :refer [btn-map*]]
   [vita.poker.const.pos :refer [pos-map*]]
   [vita.poker.const.offset :refer [offset-map*]]

   [vita.resource.image :as image]
   [sonaclo.util :as util]
   ))


(def lobby-gs* (atom {}))


(defn btn-enable-on [k]
  (mm/btn-on lobby-gs* k))


(defn btn-enable-off [k]
  (mm/btn-off lobby-gs* k))


;;----------------------------------
;; lobby

(declare clear-popup)

(defn lobby-init [] 

  (mm/g-add lobby-gs* :background
            :ga (event/new-gatom "backgroud"
                                 [(ui-image :img (image/load :lo-back) :order -1)]
                                 :pos [0 0]))

  (mm/new-btn-with-k lobby-gs* :btn-lo-left
                  :on-click #(let [next-page (r/get-left-page)]
                               (when (not= next-page (r/get-cur-page))
                                 (clear-popup)
                                 (sender/v-send :roomlist next-page)))
                  :enable true)

  (mm/new-btn-with-k lobby-gs* :btn-lo-right
                  :on-click #(let [next-page (r/get-right-page)]
                               (when (not= next-page (r/get-cur-page))
                                 (clear-popup)
                                 (sender/v-send :roomlist next-page)))
                  :enable true)

  (mm/new-btn-with-k lobby-gs* :btn-lo-playnow
                  :on-click #(if-let [room-id (r/get-random-room-id)]
                               (sender/v-send :join room-id))
                  :enable true)

  (mm/new-btn-with-k lobby-gs* :btn-lo-backb
                  :on-click #(vita.helper/load-scene! :main-lobby)
                  :enable true)
  )



(defn page-btn-check [cur-page max-page]
  (btn-enable-on :btn-lo-left)
  (btn-enable-on :btn-lo-right)
  (if (= cur-page 1)
    (btn-enable-off :btn-lo-left))
  (if (<= max-page cur-page)
    (btn-enable-off :btn-lo-right)))


;;-----------------------------------------
;; 페이지 단위

(def page-gs* (atom {}))


(defn add-page-btn [k & {:keys [button-info pos on-click enable order] :or {}}]
  (mm/new-btn page-gs* k :button-info button-info :pos pos :on-click on-click :enable enable :order order))


(defn add-page-ga [k & {:keys [ga] :or {}}]
  (mm/g-add page-gs* k :ga ga))


(defn clear-page []
  (mm/clear-all page-gs*))


(declare make-room-info)
(declare popup)


(defn new-page [roomlist]
  (clear-page)
  (dorun (map-indexed #(make-room-info %1 %2) roomlist)))


(defn avgpot [bb]
  (let [b (* bb 20)
        s (* bb 5)
        i (rand-int (- b s))
        d (+ s i)]
    (* 100 (.round js/Math (/ d 100)))))


(defn make-room-info [idx room]

  (let [room-num (util/keyword+ :room idx)
        {:keys [room-id users sb bb]} room]

    ;; 테이블 입장 버튼 (한 테이블 전체가 버튼)
    (add-page-btn (util/keyword+ :table idx)
                  :button-info (btn-map* :btn-lo-table)
                  :pos (pos-map* room-num)
                  :on-click #(sender/v-send :join room-id)
                  :enable true)

    ;; 방 정보
    (add-page-ga (util/keyword+ :label idx)
                 :ga (event/new-gatom "label"
                                      [(ui-text :text (str (util/money-with-abbreviation sb) " / " (util/money-with-abbreviation bb))
                                                :style {:color :#FFFF00})
                                       (ui-text :text "Avg.POT"
                                                :style {:color :#FFFFFF}
                                                :offset {:dy 20})
                                       (ui-text :text (str " " (util/money-with-abbreviation (avgpot bb)))
                                                :style {:color :#05FFFF}
                                                :offset {:dx 70 :dy 20})]
                                      :pos (map + (pos-map* room-num)
                                                (offset-map* :room-sbbb))))


    ;; 돋보기 버튼
    (add-page-btn (util/keyword+ :btn-lo-see idx)
                  :button-info (btn-map* :btn-lo-see)
                  :pos (map + (pos-map* room-num) (offset-map* :room-to-see))
                  :on-click #(popup room-num)
                  :enable true)


    ;; 미니 유저 그림
    (doseq [{:keys [name seat avatar-url]} (vals users)]
      (let [avatar (if (and avatar-url (not= avatar-url ""))
                     (try (util/load-image avatar-url)
                        (catch js/Error err nil)))

            twth [40 40]
            ga (event/new-gatom "miniuser"
                                [(ui-image :img (or avatar (image/load :lo-player))
                                           :frame [0 0 nil nil]
                                           :twth twth)]
                                :pos (map + (pos-map* room-num)
                                          (offset-map* (util/keyword+ :room-mini- seat))))]
        (add-page-ga (util/keyword+ :player idx seat)
                     :ga ga)))
    ))



;;--------------------------------------
;; 팝업 뷰

(def popup-gs* (atom {}))


(defn add-popup-ga [k & {:keys [ga]}]
  (mm/g-add popup-gs* k :ga ga))


(defn add-popup-btn [k & {:keys [button-info pos on-click enable order] :or {}}]
  (mm/new-btn popup-gs* k
              :button-info button-info
              :pos pos
              :on-click on-click
              :enable enable
              :order order))


(defn clear-popup []
  (mm/clear-all popup-gs*))


(defn popup [room-num]

  (clear-popup)

  (let [popup-back-img (image/load :lo-join-back)
        popup-table-img (image/load :lo-table)
        popup-player-img (image/load :lo-player)

        idx (js/parseInt (last (name room-num)))
        room (get (r/get-roomlist) idx)
        {:keys [room-id users]} room
        base-pos (pos-map* room-num)]

    ;; 배경 패널
    (add-popup-ga "popup-back"
                  :ga (event/new-gatom "popup-back"
                                       [(ui-image :img popup-back-img)
                                        (box-collider :w 450 :h 280)]
                                       :pos (map + base-pos
                                                 (offset-map* :room-join-back))))

    ;; 배경 테이블 패널
    (add-popup-ga "popup-table"
                  :ga (event/new-gatom "popup-table"
                                       [(ui-image :img popup-table-img)]
                                       :pos (map + base-pos
                                                 (offset-map* :room-join-table))))


    ;; 조인 버튼
    (add-popup-btn :btn-lo-join
                   :button-info (btn-map* :btn-lo-join)
                   :pos (map + (pos-map* room-num)
                             (offset-map* :room-to-join))
                   :on-click #(sender/v-send :join room-id)
                   :enable true)


    ;; 창 닫기 버튼
    (add-popup-btn :btn-join-back-cancel
                   :button-info (btn-map* :btn-join-cancel)
                   :pos (map + (pos-map* room-num)
                             (offset-map* :btn-join-back-cancel))
                   :on-click #(clear-popup)
                   :enable true)

    ;; 유저 표시
    (let [idx (js/parseInt (last (name room-num)))]
      (doseq [u (:users ((r/get-roomlist) idx))]
        (let [avatar (if-let [url (:avatar-url u)]
                       (try (util/load-image url)
                            (catch js/Error err nil)))

              twth (if avatar [40 40] nil)]

          (add-popup-ga (str "mini-user" (:seat u) (:name u))
                        :ga (event/new-gatom "mini-user"
                                             [(ui-image :img (or avatar
                                                                 popup-player-img)
                                                        :frame [0 0 nil nil]
                                                        :twth twth)
                                              (ui-text :text (str (:name u))
                                                       :style {:font "13pt Arial"
                                                               :color :#FFFFFF}
                                                       :offset {:dx 0 :dy 50})
                                              ]
                                             :pos (map + base-pos
                                                       (offset-map* (util/keyword+
                                                                     :room-mini-
                                                                     (:seat u))))
                                             )))))

    ))


(defn clear-all []
  (mm/clear-all lobby-gs*)
  (mm/clear-all popup-gs*)
  (mm/clear-all page-gs*))
