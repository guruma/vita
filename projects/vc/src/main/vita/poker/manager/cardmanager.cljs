(ns vita.poker.manager.cardmanager
  (:require-macros
   [cljs.core.async.macros :as m :refer [go]])
  (:require
   [cljs.core.async :as async :refer [<! >! chan close! sliding-buffer put! alts! take! timeout]]
   [milk.event :as event]
   [vita.poker.behaviour.ui-card :refer [ui-card]]
   [vita.poker.behaviour.movedirect :refer [movedirect]]

   [vita.poker.manager.manager :as mm]
   [vita.poker.room :as r]
   ))


(def gs* (atom {}))


(defn clear-all []
  (mm/clear-all gs*))


(defn new-card [& {:keys [card pos seat] :or {}}]

  (when (r/in-room?)
    (let [transparency (atom 1.0)]
      (mm/g-add gs* card
                :transparency transparency
                :seat seat
                :pos pos
                :ga (event/new-gatom card
                                     [(ui-card :card card :transparency transparency)]
                                     :pos pos)))))

(defn exist-card? [card]
  (mm/g-get gs* card))


(defn del-card [card]
  (mm/g-del gs* card))


(defn del-card-by-seat [seat]
  (doseq [[card m] @gs*]
    (if (= seat (:seat m))
      (del-card card))))


(defn move-card [card & {:keys [from to delta duration] :or {}}]

  (when (r/in-room?)
    (when-let [m (mm/g-get gs* card)]
      (let [ga (:ga m)]

        (event/add-component! ga
                              (movedirect :from from
                                          :to to
                                          :delta delta
                                          :duration duration
                                          :end-callback #()))))))

(defn throw-all-cards
  "게임 종료 후, 카드 회수하는 함수"
  []
  (let [data @gs*]
    (doseq [[card m] data]
      (let [{:keys [pos]} m]
        (del-card card)
        ))))


(defn transparentize [card alpha]
  (when-let [m (mm/g-get gs* card)]
    (let [t (:transparency m)]
      (reset! t alpha))))


(defn all-cards []
  (keys @gs*))
