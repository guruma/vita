(ns vita.poker.manager.tablemanager
  (:require-macros
   [cljs.core.async.macros :as m :refer [go]]
   [vita.logger :as vcl])
  (:require
   [cljs.core.async :as async :refer [<! >! chan close! sliding-buffer put! alts! take! timeout]]

   [milk.event :as event]

   [vita.poker.behaviour.ui-image :refer [ui-image]]
   [vita.poker.behaviour.ui-chip :refer [ui-chip]]
   [vita.poker.behaviour.ui-card :refer [ui-card]]
   [vita.poker.behaviour.ui-text :refer [ui-text]]
   [vita.poker.behaviour.ui-result :refer [ui-result]]
   [vita.poker.behaviour.movedirect :refer [movedirect]]
   [vita.poker.behaviour.ui-cardpair :refer [ui-cardpair]]
   [vita.poker.behaviour.timer :refer [timer]]
   [vita.poker.behaviour.ui-bet-info :refer [ui-bet-info]]
   [vita.poker.behaviour.ui-pot-info :refer [ui-pot-info]]
   [vita.poker.behaviour.reposition :refer [reposition]]
   [vita.poker.behaviour.ui-large-number :refer [ui-large-number]]

   [vita.poker.manager.manager :as mm]
   [vita.poker.manager.cardmanager :as cm]
   [vita.poker.manager.betmanager :as bm]
   [vita.poker.manager.usermanager :as um]
   [vita.poker.manager.buttonmanager :as btnm]

   [vita.poker.room :as r]

   [vita.poker.net.sender :as sender]
   [vita.poker.const.pos :as const.pos :refer [pos-map*]]
   [vita.poker.const.btn :refer [btn-map*]]
   [vita.poker.const.offset :as offset :refer [offset-map*]]
   [vita.poker.const.const :as CONST_POKER]

   [vita.resource.sound :as snd]
   [vita.resource.image :as image]
   [sonaclo.util :as util]

   [vita.poker.animation.move-chip]
   [vita.poker.animation.move-card]
   [vita.poker.animation.flip-card]
   [vita.poker.animation.winani]
   ))


(def gs* (atom {}))


(def score* (atom []))
(def winner-marks* (atom []))


(defn clear-score* []
  (doseq [ga @score*]
    (event/del-gatom ga))
  (reset! score* []))


(defn clear-winner-marks* []
  (doseq [ga @winner-marks*]
    (event/del-gatom ga))
  (reset! winner-marks* []))


(defn clear-all []
  ;; 룸 가톰 모두 삭제
  (mm/clear-all gs*)

  ;; 종료처리에 사용하는 임시 가톰들도 삭제
  (clear-score*)
  (clear-winner-marks*)
  )


;;-----------------------------------------------
;; pot info window

(def pot-info* (atom 0))

(defn add-pot-info [v]
  (swap! pot-info* #(+ % v)))

(defn reset-pot-info [v]
  (reset! pot-info* v))

(defn pot-info-window-on []
  (reset-pot-info 0)
  (let [ga (event/new-gatom "pot-info"
                            [(ui-pot-info pot-info*)]
                            :pos (pos-map* :pot-info))]
    (mm/g-add gs* :pot-info
              :ga ga)))

(defn pot-info-window-off []
  (mm/g-del gs* :pot-info))



;-----------------------------------------------------------------------
;; 베팅 현황 정보창

(def bet-info* (atom nil))
(defn bet-info-window [cmd]

  (case cmd
    :init (if (nil? @bet-info*) ; 중복생성 방지
            (let [ga (event/new-gatom "bet-info-window"
                                      [(ui-bet-info :offset
                                                    (offset-map* :bet-info-text)
                                                    :on-raise?
                                                    #(btnm/btn-on? :btn-raise))]
                                      :pos (pos-map* :bet-info-window))]
              ;; 실제로 버튼은 아니지만;;; 게임 버튼과 함께 다니기 때문에 여기서 처리;;
              (reset! bet-info* ga)))
    :clear (do (event/del-gatom @bet-info*)
             (reset! bet-info* nil))
    nil))


;;-----------------------------------------------------------------------
;; 유저 카드

(defn new-user-card
  "뒤집기 애니메이션 + 카드 뷰 on"

  [seat idx card]

  (let [callback #(cm/new-card :card card :pos (const.pos/get-card-pos seat idx) :seat seat)]

    ;; 카드 뒤집기 애니메이션 -> 끝나면 새 카드 생성
    (vita.poker.animation.flip-card/flip-user-card seat idx callback)
    ))


(defn all-usercards-open
  "마지막에 모든 유저카드 오픈"

  [players]

  (let [sid (r/get-sid)]
    (doseq [[sid' m] players]
      (when (not= sid sid')
        (let [seat (r/get-from-user :seat sid')]
          (doseq [[idx c] (util/with-idx (:personal-cards m))]
            (new-user-card seat idx c)))))))


;;-----------------------------------------------------------------------
;; cardpair


(defn- cardpair-key [seat idx]
  (util/keyword+ seat "cardpair" idx))


(defn new-cardpair-on [seat idx]
  "유저 옆에 'cardpair' 이미지 on"
  (let [rotated-pos (const.pos/get-cardpair-pos seat)
        rotated-seat (const.pos/apply-rotate seat)
        card-k (cond
                (contains? #{:seat0 :seat3} rotated-seat)
                :card-back-r
                (contains? #{:seat1 :seat4} rotated-seat)
                :card-back-l
                :else
                :card-back-s)]
    (let [k (cardpair-key seat idx)
          ga (event/new-gatom "cardpair"
                              [(ui-image :img-k card-k)]
                              :pos (map + [(* idx 5) 0] rotated-pos))]
      (mm/g-add gs* k
                :ga ga))))


(defn user-cardpair-on []
  "모든 유저의 'cardpair' 이미지 on"
  (doseq [[_ u] (r/get-from-room :users)]
    (doseq [idx (range (count (:cards u)))]
      (new-cardpair-on (:seat u) idx))))


(defn user-cardpair-off [seat]
  (doseq [k (map #(util/keyword+ seat "cardpair" %) (range 4))]
    (mm/g-del gs* k)))


(defn throw-all-cardpair []
  (doseq [seat CONST_POKER/SEAT_LIST]
    (user-cardpair-off seat)))


(defn rotate-cardpair []
  (throw-all-cardpair)
  (user-cardpair-on))



;; ----------------------------------------------------------------
;; pot gatom value 포인터

(def pot* (atom 0))

(defn get-pot []
  @pot*)

(defn new-pot [pot]
  (when (r/in-room?)
    (reset! pot* pot)
    (if-not (mm/exist? gs* :pot)
      (mm/g-add gs* :pot
                :ga (event/new-gatom "pot"
                                     [(ui-chip :bet pot*)]
                                     :pos (pos-map* :pot))))))

(defn reset-pot []
  (when (r/in-room?)
    (let [ga (:ga (mm/g-get gs* :pot))
          pot (r/get-from-room :pot)]
      (if ga
        (reset! pot* pot)
        (new-pot pot)))))


(defn add-pot [bet]
  (when (r/in-room?)
    (if (nil? (mm/g-get gs* :pot))
      (new-pot @pot*))
    (swap! pot* #(+ % bet))))


;; -----------------------------------------------------------------
;; dealer mark

(defn new-dealer-mark [dealer-sid]
  (let [seat (r/get-from-user :seat dealer-sid)
        dealer-mark-img (image/load :d)
        ga (event/new-gatom "dealer-mark"
                            [(ui-image :img dealer-mark-img)]
                            :pos (const.pos/get-dealer-mark-pos seat))]

    (mm/g-add gs* :dealer-mark
              :ga ga
              :seat seat)))

(defn rotate-dealermark []
  (let [m (mm/g-get gs* :dealer-mark)
        seat (:seat m)
        ga (:ga m)]
    (when ga
      (event/add-component! ga (reposition (const.pos/get-dealer-mark-pos seat))))))





;;------------------------------------------------------------------
;; myturn

(defn myturn-on [seat]
  (mm/g-add gs* :myturn
            :seat seat
            :ga (event/new-gatom :myturn
                                 [(ui-image :img (image/load :myturn)
                                            :order 0)]
                                 :pos (const.pos/get-myturn-pos seat))))


(defn myturn-off []
  (mm/g-del gs* :myturn))


(defn rotate-myturn []
  (let [m (mm/g-get gs* :myturn)
        seat (:seat m)
        ga (:ga m)]
    (when ga
      (event/add-component! ga (reposition (const.pos/get-myturn-pos seat))))))


;; -----------------------------------------------------------------
;; timer

(defn timer-start [seat]
  (myturn-on seat)
  (let [ga (event/new-gatom "timer"
                            [(timer :duration CONST_POKER/USER_TIMER_DUR
                                    :offset (offset-map* :user-timer)
                                    :end-callback #())]
                            :pos (const.pos/get-seat-pos seat))]

    (mm/g-add gs* :timer :ga ga :seat seat)
    ))


(defn get-timer-seat []
  (:seat (mm/g-get gs* :timer)))


(defn timer-stop []
  (myturn-off)
  (mm/g-del gs* :timer))


(defn rotate-timer []
  (let [m (mm/g-get gs* :timer)
        seat (:seat m)
        ga (:ga m)]
    (when ga
      (event/add-component! ga (reposition (const.pos/get-seat-pos seat))))))


;;------------------------------------------------------------------
;; winner mark

(defn winner-mark-clear []
  (doseq [ga @winner-marks*]
    (event/del-gatom ga))
  (reset! winner-marks* []))


(defn winner-mark [seat]
  ;; winner 표시
  (when (r/in-room?)
    (let [seatpos (const.pos/get-seat-pos seat)

          w-ga (event/new-gatom "winner-mark"
                                [(ui-image :img (image/load :player-winner))]
                                :pos (map + seatpos
                                          (offset-map* :winner-mark)))]
      (swap! winner-marks* conj w-ga)

      (vita.poker.animation.winani/winani seat #()))))


;;------------------------------------------------------------------
;; betting

(defn bet-action [user bet]
  "유저 베팅 애니메이션"
  (let [seat (:seat user)
        state (:state user)]

    ;; 유저 상태 업데이트
    (um/update-user :state seat)
    (snd/play (:state (r/get-user-by-seat seat)))
    (um/update-user :bet seat)


    ;; fold 하면 카드 수거한다.
    (when (= state :fold)
      (user-cardpair-off seat)
      (cm/del-card-by-seat seat)
      (vita.poker.animation.move-card/user-card-discard seat))

    ;; 추가 베팅이 있을때 칩 애니메이션 발생
    (when (< 0 bet)
      (add-pot-info bet)
      (go
        (<! (vita.poker.animation.move-chip/move-chip bet
                                                 (const.pos/get-seat-pos seat)
                                                 (const.pos/get-chip-pos seat)
                                                 CONST_POKER/CHIP_MOVE_DUR))
        (when (r/in-room?)
          (bm/update-bet seat)
          (um/update-user :money seat))))

    ))


(defn sb-bb-bet-action []
  (go
    (let [sb-user (r/sb-user)
          sb-money (r/get-from-room :sb)

          sb-user' (assoc sb-user
                     :bet sb-money
                     :state :small)

          bb-user (r/bb-user)
          bb-money (r/get-from-room :bb)

          bb-user' (assoc bb-user
                     :bet bb-money
                     :state :big)
          ]

      (when (r/in-room?)
        (r/reset-user (:sid sb-user) sb-user')
        (bet-action sb-user' sb-money)
        (<! (timeout 1000)))

      (when (r/in-room?)
        (r/reset-user (:sid bb-user) bb-user')
        (bet-action bb-user bb-money))

      )))


(defn betend-action []
  "유저 베팅 돈을 pot 으로 모은다."

  ;; 유저의 bet 값 업데이트
  (doseq [[_ u] (r/users)]
    (um/update-user :bet (:seat u)))

  ;; 유저가 베팅한 칩들을 pot 으로 옮긴다.
  (doseq [[seat m] (bm/get-all)]
    (when-let [{:keys [ga bet]} m]
      (when (and bet (< 0 @bet))
        (bm/del-bet seat)
        (go
          (<! (vita.poker.animation.move-chip/move-chip bet
                                                        (const.pos/get-chip-pos seat)
                                                        (pos-map* :pot)
                                                        CONST_POKER/CHIP_MOVE_DUR))
          (when (r/in-room?)
            (reset-pot))))))


  ;;(js/setTimeout #(snd/play :chip-stack) CONST_POKER/CHIP_MOVE_DUR)

  ;; 베팅 흔적 삭제
  (bm/clear-all)

  ;; 베팅시 계속 더해줬지만, 안전함을 위해 베팅 종료시마다 룸에 pot 정보로 갱신
  (reset-pot-info (r/get-from-room :pot))
  )


;;-------------------------------------------------------------------
;; 회전 적용

(defn rotate-seat []
  (rotate-cardpair)
  (rotate-timer)
  (rotate-dealermark)
  (rotate-myturn)
  )


;;-------------------------------------------------------------------
;; 게임

(defn game-start [dealer-sid users]

  (r/set-dealer-sid dealer-sid)

  ;; 딜러 마크 표시 on
  (new-dealer-mark dealer-sid)

  ;; pot 정보창 표시
  (pot-info-window-on)

  ;; 유저들 state 갱신 (게임이 시작되면 바뀐다)
  (doseq [[_ u] users]
    (um/update-user :state (:seat u))
    (um/update-user :money (:seat u)))

  ;; sb 베팅 시작
  (sb-bb-bet-action))



(defn move-winner-money [win-sid win-money old-room]
  (let [ch (chan 1)]
    (go
      (let [seat (get-in old-room [:users win-sid :seat])]
        (bm/pot-to-user win-money
                        seat
                        #(do (add-pot %) (add-pot-info %))
                        #(um/apply-update-user :money
                                               (fn [v]
                                                 (+ v win-money))
                                               seat))
        (close! ch)))
    ch))


(defn show-card-result [seat result]
  (mm/g-add gs* (util/keyword+ seat "result")
            :ga (event/new-gatom "show-card-result"
                                 [(ui-result result)]
                                 :pos (const.pos/get-result-pos seat))))

(defn del-card-result [seat]
  (mm/g-del gs* (util/keyword+ seat "result")))


(defn show-winner-hands [sid money players old-room]
  (let [ch (chan 1)]
    (go
      (let [result (get-in players [sid :score :category])
            best-hands (get-in players [sid :score :cards])
            seat (get-in old-room [:users sid :seat])]

        ;; 내 카드 들어올리기
        (doseq [c best-hands]
          (cm/move-card c :delta [0 -15] :duration CONST_POKER/CARD_MOVE_DUR))

        ;; result 족보 표시
        (show-card-result seat result)

        ;; winner 마크표시
        (winner-mark seat)

        ;; 획득 돈 표시
        (let [ga (event/new-gatom "big-n"
                                  [(ui-large-number money)]
                                  :pos (map + (offset-map* :win-money) (const.pos/get-seat-pos seat)))]
          (swap! score* conj ga))

        (<! (timeout 2000))

        ;; 카드 다시 내리기
        (doseq [c best-hands]
          (cm/move-card c :delta [0 15] :duration CONST_POKER/CARD_MOVE_DUR))

        (<! (timeout 1000))

        )
      (close! ch))))


(defn game-clear []
  (clear-all)
  (bm/clear-all)
  (cm/clear-all))


(defn game-end [winner players old-room overage]

  (let [;; winner 와 players 에  high, low 정보가 섞여있기 때문에 분리한다.
        high-winner (:high winner)
        high-players (into {} (map (fn [[k v]] [k (update-in v [:score] #(:high %))])
                                   players))

        low-winner (:low winner)
        low-players (into {} (map (fn [[k v]] [k (update-in v [:score] #(:low %))])
                                  players))

        ;; 남은 유저 베팅 머니 정보
        -users (:users old-room)
        bets (apply + (map :bet (vals -users)))

        ;; 게임 카드 모두 오픈되고 정상종료인지, 중간에 fold 나 튕김으로 끝난건지 확인
        -play-user-cnt (count (keep #(if (r/is-playing-user? (:state %))
                                       true)
                                    (vals -users)))
        is-normal-end? (< 1 -play-user-cnt)

        ]

    (go
      ;; 유저의 상태 업데이트
      (doseq [[_ u] (:users old-room)]
        (um/update-user :state :none (:seat u)))

      ;; 중간에 게임끝나는 경우, 유저의 남아있는 베팅돈을 pot 으로 모음
      (when (and (r/in-room?) bets)
        (bm/all-to-pot add-pot)
        (<! (timeout 500)))

      ;; overage 있으면 유저에게 돌려줌
      (when (and (r/in-room?) (seq overage))
        (mm/g-add gs* "overage"
                  :ga (event/new-gatom "overage"
                                       [(ui-text :text "OVERAGE"
                                                 :style CONST_POKER/OVERAGE_STYLE
                                                 :offset {:dx -50 :dy 20}
                                                 )]
                                       ;align text style dx dy order
                                       :pos (pos-map* :pot)))
        (bm/return-overage overage
                           #(do (add-pot %) (add-pot-info %))
                           (fn [seat money] (um/apply-update-user :money
                                                                  #(+ % money)
                                                                  seat))
                           old-room)

        (<! (timeout 1000))
        (mm/g-del gs* "overage")
        (<! (timeout 1000))
        )

      (when (and (r/in-room?) is-normal-end?)
        ;; 유저 cardpair 이미지 삭제
        (doseq [[_ u] (:users old-room)]
          (user-cardpair-off (:seat u)))
        ;; 다른 유저들 카드 오픈 (중간에 끝났을 경우는 카드를 안보여준다.)
        (all-usercards-open players)
        (<! (timeout 1500)))

      ;; high 뷰를 보여준다.
      (when (and is-normal-end? (r/in-room?))
        (doseq [[win-sid win-money] high-winner]
          (<! (show-winner-hands win-sid win-money high-players old-room))))

      ;; 획득 돈 분배
      (when (r/in-room?)
        (doseq [[win-sid win-money] high-winner]
          (<! (move-winner-money win-sid win-money old-room)))

        ;; 효과 지우기
        (doseq [[win-sid _] high-winner]
          (del-card-result (get-in old-room [:users win-sid :seat])))
        (winner-mark-clear)
        (clear-score*))

      ;; low 뷰를 보여준다.
      (when (and is-normal-end? (r/in-room?))
        (doseq [[win-sid win-money] low-winner]
          (<! (show-winner-hands win-sid win-money low-players old-room))))

      ;; 획득 돈 분배
      (when (r/in-room?)
        (doseq [[win-sid win-money] low-winner]
          (move-winner-money win-sid win-money old-room))

        ;; 효과 지우기
        (doseq [[win-sid _] high-winner]
          (del-card-result (get-in old-room [:users win-sid :seat])))
        (winner-mark-clear)
        (clear-score*))

      (reset-pot)

      (when (r/in-room?)
        ;; 카드 삭제
        (cm/throw-all-cards)
        (throw-all-cardpair))

      ;; 완전 종료처리.
      (when (r/in-room?)
        (game-clear))

      )))


(defn game-bet []

  ;; 타이머 정지
  (timer-stop)

  ;; 게임버튼 off
  (btnm/game-play-btns-off))


(defn game-betend [users]

  ;; 유저들 정보 갱신
  (doseq [[_ u] users]
    (um/update-user :state (:seat u)))

  ;; 베팅 종료 처리
  (betend-action))
