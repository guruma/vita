(ns vita.poker.doc.protocol)

;;---------------------------------------
;; 프로토콜
;;---------------------------------------

;; < 기본 표현 방식 >
;; cmd Q 클라이언트가 서버에 요청
;;     R 요청 응답
;;     B 서버 브로드캐스팅 푸시

;; {:cmd :Qlogin
;;  ... }
;; {:cmd :Rlogin
;;  :token token9
;;  ... }
; 로그인을 통해
; 유저 정보를 이미 받은 상태(페이스북 아이디, 토큰 포함)

;;---------------------------------------
;; 방 리스트 요청하기
;;---------------------------------------

{:cmd :Qroomlist
 :page 1}

{:cmd :Rroomlist
 :err 0
 :roomlist [{:room-id :r1
             :sbbb [50 100]
             :users [{:seat :seat1 :name :aaa}]}]
 :page 1}

;;---------------------------------------
;; 방에 조인하기
;;---------------------------------------
{:cmd :Qjoin
 :token :token9}

{:cmd :Rjoin
 :err nil ; or true
 :room {}} ;room 정보

{:cmd :Bjoin
 :user {}} ; 유저정보

;;---------------------------------------
;; 방에서 나가기 ( TO Lobby Btn )
;;---------------------------------------
{:cmd :Qquit
 :token :token1}

{:cmd :Rquit
 :err 0}

{:cmd :Bquit
 :users {}
 :token :token1
 :pot 2400} ; 나간 유저의 베팅머니는 pot 으로 옮겨진다.

;;---------------------------------------
;; 테이블에 앉기
;;---------------------------------------
{:cmd :Qseat
 :token :token9
 :seat :seat1}

{:cmd :Rseat
 :err nil ; or errcode
 :seat :seat1
 }

{:cmd :Bseat
 :token :token2
 :seat :seat3
 :user {}}

;;---------------------------------------
;; 의자에서 일어나기
;;---------------------------------------
{:cmd :Qstandup
 :token :token2
}

{:cmd :Rstandup
 :err 0}

{:cmd :Bstandup
 :token :token2 ; 나간 유저
 :users {} ; 나간 이후 유저 정보
 :pot 2500} ; 유저가 나간 후, pot 변경된 값

;;---------------------------------------
;; 기타 정보 조회
;;---------------------------------------
{:cmd :Qget
 :ks [:users :token1]} ; nil 이면 room 통채로 조회요청

{:cmd :Rget
 :data {}}

;;---------------------------------------
;; 서버 게임 푸시
;;---------------------------------------

{:cmd :Bstart
 :dealer-token :token1
 :users {} }

{:cmd :Bpreflop ; 게임시작, 유저카드 배포
 :users {} ; 유저들 정보 (카드 2 장씩 추가됨)
 }

{:cmd :Qbet
 :token :token1
 :select :bet
 :bet 1000}

{:cmd :Rbet
 :err nil}

{:cmd :Bbet
 :pot 100
 :last-user-token :token1 ; or nil
 :last-user {}; or nil
 :cur-user-token :token2
 :cur-user-select-list [:call :raise :fold]
 :round-bet 400 ; 현재 라운드 베팅금액
 }

{:cmd :Bbetend
 :pot 100
 :users {}} ; 유저정보갱신

{:cmd :Bflop
 :community-cards [:spade-2 :heart-Q :dimond-7]}

;; bet... betend...

{:cmd :Bturn
 :community-cards [:spade-2 :heart-Q :dimond-7 :dimond-6]}

;; bet... betend...

{:cmd :Briver
 :community-cards [:spade-2 :heart-Q :dimond-7 :dimond-6 :heart-A]}


; 게임 끝 밑 정산
{:cmd :Bend

 ;; 승자 정보
 :winner {:high {:token2 20000
                 :token5 10000}
          :low {:token1 12000}} ;; :low [:token3]

 ;; 종료 처리 이후 룸 정보
 :room {}

 :community-cards [],

 :overage nil

 ;; 유저들 핸드 결과
 :players {:token2 {:personal-cards [:diamond-J :spade-5]
                    :score {:high {:category :one-pair,
                                   :cards [:diamond-A :heart-A :spade-K :diamond-J :spade-8]}
                            :low {:category nil}}}}}
