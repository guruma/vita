(ns vita.poker.const.offset)


(def offset-map*
  {
   ;;------------------------
   ;; 룸 리스트 관련 이미지 오프셋
   :room-sbbb [120 70]
   :room-to-see [70 57]
   :room-to-join [97 207]
   :room-join-back [-61 -20]
   :room-join-table [0 0]
   :btn-join-back-cancel [350 0]

   :room-mini-seat0 [273 17]
   :room-mini-seat1 [273 122]
   :room-mini-seat2 [145 122]
   :room-mini-seat3 [17 122]
   :room-mini-seat4 [17 17]


   ;;-------------------------
   ;; 게임 룸 내 이미지 오프셋

   :cmm-inteval [73 0]
   :card-interval [30 0]
   :r-card-user [-170 -40]
   :l-card-user [130 -40]
   :user-timer [84 9]
   :winner-mark [-26 -35]
   :win-money [5 5]
   :call-text [110 35]
   :bet-info-text [312 6]
   })
