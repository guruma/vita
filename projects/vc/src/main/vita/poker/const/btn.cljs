(ns vita.poker.const.btn
  (:require [sonaclo.util :as util]
            [vita.resource.image :as image]))


(def btn-map*

  ;;-----------------------------------------------------
  ;;    make-btn 인자 순서  <IMG> <W> <H> <Z>
  ;;-----------------------------------------------------

  {
   :btn-lo-playnow {:img-k :lo-playnow :table (util/button-table [520 34])}
   :btn-lo-backb {:img-k :to-lobby :table (util/button-table [520 34])}

   ;;--------------------------------------
   ;; lobby btn

   :btn-lo-table {:img-k nil :table (util/button-table [1280 170])}
   :btn-lo-join {:img-k :lo-join :table (util/button-table [544 44])}
   :btn-lo-left {:img-k :lo-left :table (util/button-table [200 84])}
   :btn-lo-right {:img-k :lo-right :table (util/button-table [200 84])}
   :btn-lo-see {:img-k :lo-see :table (util/button-table [148 37])}
   :btn-join-cancel {:img-k :join-cancel :table (util/button-table [116 29])}

   ;;---------------------------------------
   ;; room btn

   ;; 좌 상단, 퇴실 버튼
   :btn-stand-up {:img-k :stand-up :table (util/button-table [520 34])}
   :btn-to-lobby {:img-k :lo-backb :table (util/button-table [520 34])}
   ;; 빈자리 자동 앉기 버튼
   :btn-auto-seat {:img-k :auto-seat :table (util/button-table [968 63])}
   ;; 테이블 빈자리 앉기 버튼
   :btn-empty-sit {:img-k :click-here :table (util/button-table [376 130])}
   ;; 채팅
   :btn-chat {:img-k :chat-enter :table (util/button-table [80 20])}

   ;;---------------------------------------
   ;; game btn

   ;; 우 하단, 게임 진행 버튼
   :btn-bet {:img-k :btn-bet :table (util/button-table [496 80])}
   :btn-call {:img-k :btn-call :table (util/button-table [504 82])}
   :btn-raise {:img-k :btn-raise :table (util/button-table [504 82])}
   :btn-check {:img-k :btn-check :table (util/button-table [504 52])}
   :btn-fold {:img-k :btn-fold :table (util/button-table [504 52])}

   ;; 중앙 하단, 게임 설정 버튼
   :btn-minus {:img-k :btn-minus :table (util/button-table [208 96])}
   :btn-plus {:img-k :btn-plus :table (util/button-table [208 96])}
   :btn-min {:img-k :btn-min :table (util/button-table [240 44])}
   :btn-half {:img-k :btn-half :table (util/button-table [240 44])}
   :btn-pot {:img-k :btn-pot :table (util/button-table [240 44])}
   :btn-all-in {:img-k :btn-all-in :table (util/button-table [788 44])}
   })
