(ns vita.poker.const.const)


;; 텍사스 포커의 테이블 자리는 5 자리이다.
(def ^:const SEAT_LIST [:seat0 :seat1 :seat2 :seat3 :seat4])

(def ^:const CARD_RESULT_RANK {:royal-straight-flush 9
                              :straight-flush 8
                              :four-of-a-kind 7
                              :full-house 6
                              :flush      5
                              :straight 4
                              :three-of-a-kind 3
                              :two-pair 2
                              :one-pair 1
                              :high-card 0})

(def ^:const NAME_LENGTH 17)


;;---------------
;; sec time const

(def ^:const CHIP_MOVE_DUR 200)

(def ^:const USER_TIMER_DUR 15000)

(def ^:const CARD_MOVE_DUR 250)

(def ^:const CARD_MOVE_DUR_2 100)


;;---------------
;; style const

(def PLAY_USER_STATES #{:bet :raise :check :call :wait :small :big :all-in})

(def DEFAULT_STYLE {:font "13pt Arial"
                    :color :#FFFFFF
                    :align :left})


(def BAR_NAME_STYLE {:color :#FFFFFF})

(def OVERAGE_STYLE {:color :#FFFF00
                    :font "15pt Arial"
                    :align :center})
