(ns vita.poker.const.pos
  (:require [vita.poker.const.offset :refer [offset-map*]]
            [sonaclo.util :as util]))


(def pos-map*
 {
  :room-dealer [644 272]

  :pot-info [566 286]
  :cmm-cards [465 332]
  :pot [625 440]

  ;;---------------------
  ;; 버튼 위치
  ;;---------------------

  ;; 로비 관련 버튼
  :btn-lo-backb [220 70]
  :btn-lo-playnow [220 109]
  :btn-lo-join [580 700]
  :btn-lo-left [220 396]
  :btn-lo-right [1010 396]
  :btn-lo-sees [[290 187] [802 187] [544 396] [289 599] [801 599]]

  :room0 [220 130]
  :room1 [732 130]
  :room2 [475 336]
  :room3 [220 542]
  :room4 [732 542]

  ;; 자동 앉기 버튼
  :btn-auto-seat [519 859]

  ;; 게임 진행 버튼
  :btn-bet [933 700]
  :btn-call [933 815]
  :btn-raise [804 815]
  :btn-check [933 903]
  :btn-fold [804 903]

  :chk-btn-call [938 820]
  :chk-btn-raise [809 820]
  :chk-btn-check [938 908]
  :chk-btn-fold [809 908]

  ;; 게임 설정 버튼
  :btn-minus [480 858]
  :btn-plus [745 858]
  :btn-min [540 858]
  :btn-half [608 858]
  :btn-pot [676 858]
  :btn-all-in [540 910]

  ;; 게임 퇴시 버튼
  :btn-to-lobby [222 70]
  :btn-stand-up [222 110]

  ;; 설정 버튼
  :btn-window-bigger [695 20]
  :btn-window-smaller [695 20]
  :btn-music [730 20]
  :btn-sound [765 20]
  :btn-information [800 20]

  ;; 승리금액 large number
  :large-number [644 750]

  ;; raise 빨간 숫자 표시
  :bet-info-window [480 815]
  })


(defn get-cmm-card-pos [idx interval]
  (map + (pos-map* :cmm-cards) (map #(* idx %) interval)))

;;-----------------------------------
;; rotated seat

(def center-seat-offset [0 60])

(def seated* (atom nil))

(def rotated* (atom {:seat0 :seat0
                     :seat1 :seat1
                     :seat2 :seat2
                     :seat3 :seat3
                     :seat4 :seat4}))

(def seat-pos {:seat4 [220 222]
               :seat3 [220 518]
               :seat2 [593 647]
               :seat1 [966 518]
               :seat0 [966 222]})


(defn rotate-seat [center-seat]
  (reset! seated* center-seat)
  (let [seats [:seat0 :seat1 :seat2 :seat3 :seat4]

        idx (util/indexof seats center-seat)
        rotated (util/rotate-list seats (- 2 idx))]

    (reset! rotated* (into {} (map vector seats rotated)))))


(defn recover-rotate []
  (rotate-seat :seat2)
  ;; 반드시 밑에서 reset 해야함. rotate-seat 내부에서 덮어씜
  (reset! seated* nil))


(defn apply-rotate [seat]
  (get @rotated* seat))


;;-----------------------------------
;; myturn pos

(defn get-myturn-pos [seat]
  (get {:seat0 [860 270]
        :seat1 [825 435]
        :seat2 [570 450]
        :seat3 [320 435]
        :seat4 [280 270]}
       (apply-rotate seat)))


;;-----------------------------------
;; get user-seat-pos

(defn get-seat-pos [seat]
  (get seat-pos (apply-rotate seat)))


;;-----------------------------------
;; card-pos by seat

(def user-card-pos {:seat0 [800 205]
                    :seat1 [800 498]
                    :seat2 [593 522]
                    :seat3 [380 498]
                    :seat4 [380 205]})


(defn get-card-pos [seat idx]
  (let [seat-offset (user-card-pos (apply-rotate seat))]
    (map + seat-offset
         (map #(* idx %) (offset-map* :card-interval)))))


;;-----------------------------------
;; result-view by seat

(def user-result-pos {:seat0 [800 262]
                      :seat1 [800 554]
                      :seat2 [514 527]
                      :seat3 [220 554]
                      :seat4 [220 262]})


(defn get-result-pos [seat]
  (user-result-pos (apply-rotate seat)))


;;-----------------------------------
;; chip-pos by seat

(def user-chip-pos {:seat0 [928 346]
                    :seat1 [905 449]
                    :seat2 [590 480]
                    :seat3 [336 448]
                    :seat4 [318 346]})


(defn get-chip-pos [seat]
  (user-chip-pos (apply-rotate seat)))


;;-----------------------------------
;; dealer-mark-pos by seat

(def user-dealer-mark-pos {:seat0 [882 286]
                           :seat1 [832 498]
                           :seat2 [666 498]
                           :seat3 [421 498]
                           :seat4 [361 290]})


(defn get-dealer-mark-pos [seat]
  (user-dealer-mark-pos (apply-rotate seat)))


;;-----------------------------------
;; cardpair-pos by seat

(def cardpair-pos {:seat0 [910 310]
                   :seat1 [870 480]
                   :seat2 [635 490]
                   :seat3 [370 480]
                   :seat4 [335 315]})

(defn get-cardpair-pos [seat]
  (get cardpair-pos (apply-rotate seat)))
