(ns vita.poker.animation.flip-card
  (:require-macros
   [cljs.core.async.macros :as m :refer [go]]
   [vita.logger :as vcl])
  (:require
   [cljs.core.async :as async :refer [<! >! chan close! sliding-buffer put! alts! take! timeout]]
   [milk.event :as event]

   [vita.resource.sound :as sound]
   [vita.resource.image :as image]

   [vita.poker.behaviour.ui-card :refer [ui-card]]
   [vita.poker.behaviour.reposition :refer [reposition]]
   [vita.poker.behaviour.animation :refer [animation]]

   [vita.poker.const.const :as CONST]
   [vita.poker.const.pos :as const.pos]
   [vita.poker.room :as r]

   [vita.poker.animation.util :as animation.util]
   ))

(def flip-duration 200)


(defn flip-card [pos callback]
  (let [ch (chan 1)
        img-k :cardreversion
        frame [0 8 70 97]
        animation-idx* (atom 0)]

    (go
      (sound/play :card-open)
      (<! (animation.util/anima img-k frame pos flip-duration 1))
      (callback)
      (close! ch))

    ch))


(defn flip-user-card [seat idx callback]
  (let [ch (chan 1)
        pos (const.pos/get-card-pos seat idx)]
    (go
      (<! (flip-card pos callback))
      (close! ch))
    ch))


(defn flip-cmm-card [idx interval callback]
  (let [ch (chan 1)
        pos (const.pos/get-cmm-card-pos idx interval)]
    (go
      (<! (flip-card pos callback))
      (close! ch))
    ch))
