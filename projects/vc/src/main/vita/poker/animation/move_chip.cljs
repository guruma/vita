(ns vita.poker.animation.move-chip
  (:require-macros
   [cljs.core.async.macros :as m :refer [go]]
   [vita.logger :as vcl])
  (:require
   [cljs.core.async :as async :refer [<! >! chan close! sliding-buffer put! alts! take! timeout]]
   [vita.resource.sound :as sound]
   [vita.poker.const.pos :as const.pos]
   [vita.poker.animation.util :as animation.util]
   [vita.poker.behaviour.ui-chip :refer [ui-chip]]
   ))


(def move-duration 100)


(defn move-chip [bet from to duration]
  (let [ch (chan 1)
        img-comp (ui-chip :bet bet)
        duration move-duration]

    (go
      (<! (animation.util/move from to img-comp duration))
      (sound/play :chip-return-1)
      (close! ch))
    ch))
