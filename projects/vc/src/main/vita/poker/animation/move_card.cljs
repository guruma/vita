(ns vita.poker.animation.move-card
  (:require-macros
   [cljs.core.async.macros :as m :refer [go]]
   [vita.logger :as vcl])
  (:require
   [cljs.core.async :as async :refer [<! >! chan close! sliding-buffer put! alts! take! timeout]]
   [vita.resource.sound :as sound]
   [vita.poker.const.pos :as const.pos]
   [vita.poker.animation.util :as animation.util]
   [vita.poker.behaviour.ui-image :refer [ui-image]]
   ))


(def move-duration 200)


(defn user-card-hand-out [seat idx]
  (let [ch (chan 1)
        img-comp (ui-image :img-k :card-back-s)
        duration move-duration
        start-pos (const.pos/pos-map* :room-dealer)
        end-pos (const.pos/get-seat-pos seat)]

    (go
      ;;(sound/play :card-deal)
      (<! (animation.util/move start-pos end-pos img-comp duration))
      (close! ch))
    ch))


(defn user-card-discard [seat]
  (let [ch (chan 1)
        img-comp (ui-image :img-k :card-back-s)
        duration move-duration
        start-pos (const.pos/get-seat-pos seat)
        end-pos (const.pos/pos-map* :room-dealer)
        ]

    (go
      (sound/play :card-deal)
      (<! (animation.util/move start-pos end-pos img-comp duration))
      (close! ch))
    ch))


(defn cmm-card-hand-out [idx interval]
  (let [ch (chan 1)
        img-comp (ui-image :img-k :card-back-s)
        duration move-duration
        start-pos (const.pos/pos-map* :room-dealer)
        end-pos (const.pos/get-cmm-card-pos idx interval)]
    (go
      ;;(sound/play :card-deal)
      (<! (animation.util/move start-pos end-pos img-comp duration))
      (close! ch))
    ch))
