(ns vita.poker.animation.util
  (:require-macros
   [cljs.core.async.macros :as m :refer [go]]
   [vita.logger :as vcl])
  (:require
   [cljs.core.async :as async :refer [<! >! chan close! sliding-buffer put! alts! take! timeout]]
   [milk.event :as event]
   [milk.gatom :as gatom]
   [milk.time]

   [vita.resource.sound :as sound]
   [vita.resource.image :as image]

   [vita.poker.const.pos :as const.pos]
   [vita.poker.behaviour.animation :refer [animation]]
   [vita.poker.behaviour.ui-image :refer [ui-image]]
   [vita.poker.behaviour.reposition :refer [reposition]]
   ))



(defn find-idx [dur acc-dur i f w]
  (let [ratio (/ acc-dur dur)]
    (.round js/Math (* ratio f))))


(defn linear [cur-dur dur start end]
  (+ start
     (* cur-dur (/ (- end start) dur))))


(defn move [start-pos end-pos img-comp duration]
  (let [ch (chan 1)
        ga (event/new-gatom "move-ani"
                            [img-comp]
                            :pos start-pos)

        dt-sub-ch (milk.time/subscribe-delta-time)]

    (gatom/add-tag ga :animation)

    (go
      (loop [cur-dur 0]
        (when (<= cur-dur duration)

          (let [dt (:dt (<! dt-sub-ch))
                [sx sy] start-pos
                [ex ey] end-pos

                new-pos [(linear cur-dur duration sx ex)
                         (linear cur-dur duration sy ey)]]

            (event/add-component! ga (reposition new-pos))

            (recur (+ cur-dur dt)))))

      ;; 루프 종료 처리
      (event/del-gatom ga)
      (milk.time/unsubscribe-delta-time dt-sub-ch)
      (close! ch))
    ch))


(defn anima [img-k frame pos duration repeat]
  (let [ch (chan 1)
        dt 25
        [i f w h] frame
        animation-idx* (atom 0)
        ga (event/new-gatom "animation"
                            [(animation :img (image/load img-k)
                                        :frame frame
                                        :idx* animation-idx*)]
                            :pos pos)

        dt-sub-ch (milk.time/subscribe-delta-time)]

    (gatom/add-tag ga :animation)

    (go
      (loop [cur-dur 0 repeat-cnt 1]

        (if (<= duration cur-dur)

          ;; 반복횟수가 다됬으면 끝내고, 아니면 다시 시작.
          (if (<= repeat repeat-cnt)
            nil
            (recur 0 (inc repeat-cnt)))

          (let [dt (:dt (<! dt-sub-ch))
                cur-idx (find-idx duration cur-dur i f w)]
            (reset! animation-idx* cur-idx)
            (recur (+ cur-dur dt) repeat-cnt))))

      (milk.time/unsubscribe-delta-time dt-sub-ch)
      (event/del-gatom ga)
      (close! ch))
    ch))
