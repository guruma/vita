(ns vita.poker.animation.winani
  (:require-macros
   [cljs.core.async.macros :as m :refer [go]]
   [vita.logger :as vcl])
  (:require
   [cljs.core.async :as async :refer [<! >! chan close! sliding-buffer put! alts! take! timeout]]
   [milk.event :as event]

   [vita.resource.sound :as sound]
   [vita.resource.image :as image]

   [vita.poker.const.pos :as const.pos]
   [vita.poker.room :as r]

   [vita.poker.animation.util :as animation.util]
   ))



(defn winani [seat callback]
  (when (r/in-room?)
    (let [ch (chan 1)
          img-k :winani
          frame [0 1 171 171]
          pos (map + [-50 -100] (const.pos/get-seat-pos seat))
          duration 100
          repeat 15]

      (go
        (<! (animation.util/anima img-k frame pos duration repeat))
        (callback)
        (close! ch))

      ch)))
