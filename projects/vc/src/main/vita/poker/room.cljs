(ns vita.poker.room
  "room"
  (:require [sonaclo.util :as util]
            [vita.poker.const.const :as const]))


;;--------------------------------------
;; 로비 (룸 리스트) 관련 정보
;;--------------------------------------

(def roomlist* (atom {:cur-page 1
                      :max-page 1}))

;; 로비용 정보. 현재 로비 몇페이지 인지 저장한다.
(defn set-cur-page [page] (swap! roomlist* assoc :cur-page page))
(defn get-cur-page [] (@roomlist* :cur-page))

(defn set-max-page [page] (swap! roomlist* assoc :max-page page))
(defn get-max-page [] (@roomlist* :max-page))


(defn get-left-page []
  (max 1 (dec (get-cur-page))))

(defn get-right-page []
  (min (get-max-page) (inc (get-cur-page))))

(defn set-roomlist [roomlist]
  (swap! roomlist* assoc :roomlist roomlist))

(defn get-roomlist []
  (@roomlist* :roomlist))

(defn get-random-room-id []
  (let [rlst (:roomlist @roomlist*)]
    (if (seq rlst)
      (:room-id (rand-nth rlst)))))


;;--------------------------------------
;; 내 관련 정보
;;--------------------------------------

(def *db* (js-obj))

(defn _set [k v]
  (aset *db* k v))
(defn _get [k]
  (aget *db* k))


(defn set-user-id [user-id] (_set :user-id user-id))
(defn get-user-id [] (_get :user-id))


(defn set-sid [sid] (_set :sid sid))
(defn get-sid [] (_get :sid))


(defn set-dealer-sid [sid] (_set :dealer-sid sid))
(defn get-dealer-sid [] (_get :dealer-sid))


(declare get-from-user)
(declare get-from-room)

(_set :round-bet 0)
(_set :raise-bet 0)

(defn get-raise-bet [] (_get :raise-bet))
(defn set-raise-bet
  "유저의 새 raise 금액 세팅"
  [new-raise]
  (let [money (get-from-user :money (get-sid))]
    (let [new-raise' (-> (max new-raise (get-from-room :raise-bet)) ;; 앞선 사람의 raise 금액 이상.
                         (min money))]  ;; 소지한 돈 이하
      (_set :raise-bet new-raise')
      new-raise')))


(defn min-raise-bet
  "유저 기본 레이즈 베팅값 : 마지막 베팅금액 + 마지막 레이즈금액 - 현재 내 베팅금액"
  []
  (+ (get-from-room :round-bet)
     (get-from-room :raise-bet)
     (- (get-from-user :bet (get-sid)))))


(defn update-raise-bet
  [cmd]
  (let [min-raise (min-raise-bet)]
    (case cmd
      :min (set-raise-bet min-raise)

      :half (set-raise-bet (max min-raise
                                (.round js/Math (/ (get-from-room :pot) 2))))

      :pot (set-raise-bet (max min-raise (get-from-room :pot)))

      nil)))


(defn dec-raise-bet []
  (set-raise-bet (- (get-raise-bet) (get-from-room :bb))))


(defn inc-raise-bet []
  (set-raise-bet (+ (get-raise-bet) (get-from-room :bb))))


(defn all-in-raise-bet []
  (set-raise-bet (get-from-user :money (get-sid))))


(defn more-than-money? [bet]
  (>= bet (get-from-user :money (get-sid))))


;;--------------------------------------
;; 룸
;;--------------------------------------

(def in-room?* (atom false))


(defn enter-room []
  (reset! in-room?* true))

(defn quit-room []
  (reset! in-room?* false))

(defn in-room? []
  @in-room?*)


(def room* (atom nil))


(defn room-id [] (:room-id @room*))


(defn set-room [room]
  (reset! room* room))


(defn update-room [room]
  (if @room*
    (set-room room)))


(defn reset-in-room [k v]
  (swap! room* assoc k v))


(defn set-users [users]
  (reset-in-room :users users))


(defn reset-user [user-key user]
  (swap! room* assoc-in [:users user-key] user))


(defn add-user [user]
  (let [user-key (:sid user)]
    (swap! room* assoc-in [:users user-key] user)))


(defn remove-user [user-key]
  (swap! room* update-in [:users] dissoc user-key))


(defn reset-pot [pot]
  (reset-in-room :pot pot))


(defn clear-room []
  (set-room nil))


(defn user-not-in-room? []
  (nil? @room*))


(defn init! []
  (reset! in-room?* false)
  (reset! room* nil))

;;--------------------------------------
;; 룸 조회 함수
;;--------------------------------------

;; 룸 조회
(defn get-room []
  @room*)


(defn get-from-room [k]
  (get @room* k))


(defn get-pot []
  (get-from-room :pot))


(defn users []
  (:users @room*))


(defn get-user [user-key]
  (get (users) user-key))


(defn get-seat-user []
  (reduce (fn [m [_ u]] (assoc m (:seat u) u)) {} (users)))


(defn get-user-by-seat [seat]
  (get (get-seat-user) seat))


(defn get-from-user-by-seat [seat k]
  (get-in (get-seat-user) [seat k]))


(defn get-cards-by-seat [seat]
  (get-from-user-by-seat seat :cards))


(defn get-card [seat idx]
  (get (get-from-user-by-seat seat :cards) idx))


(defn get-bet-by-seat [seat]
  (get-from-user-by-seat seat :bet))


(defn get-from-user [k user-key]
  (get-in @room* [:users user-key k]))


(defn get-my-seat []
  (get-from-user :seat (get-sid)))


(defn get-my-cards []
  (get-from-user :cards (get-sid)))


(defn get-community-cards []
  (get @room* :community-cards))


(defn is-my-seat? [seat]
  (= seat (get-from-user :seat (get-sid))))


(defn is-playing-room? []
  (not= :wait (get-from-room :state)))


(defn is-playing-user? [state]
  (contains? const/PLAY_USER_STATES state))


(defn sorted-seat-by-dealer []
  (let [seat-user (get-seat-user)
        dealer-seat (get-from-user :seat (get-dealer-sid))
        seats (sort (keep (fn [[s u]] (if (is-playing-user? (:state u)) s))
                          seat-user))
        d-idx (util/indexof seats dealer-seat)
        [s1 s2] (split-at (+ 1 d-idx) seats)]

    (concat s2 s1)))

(defn sb-user []
  (let [sorted-seat (sorted-seat-by-dealer)
        sb-seat (first sorted-seat)]
    (get-user-by-seat sb-seat)))

(defn bb-user []
  (let [sorted-seat (sorted-seat-by-dealer)
        bb-seat (second sorted-seat)]
    (get-user-by-seat bb-seat)))
