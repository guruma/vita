(ns vita.poker.behaviour.ui-pot-info
  (:require-macros [milk.macro :refer [def-behaviour letc]])
  (:require [js.canvas :as js.canvas]
            [milk.gatom :as gatom]
            [vita.resource.image :as image]
            [sonaclo.util :as util]))


(defn draw-number [ctx money [x y]]
  (-> ctx
      (js.canvas/save)
      (js.canvas/font-style "15pt Arial")
      (js.canvas/text-align :center)
      (js.canvas/fill-style :#FFFFFF)
      (js.canvas/text {:text money :x x :y y})
      (js.canvas/restore)
      ))


(def-behaviour ui-pot-info [info]
  [
   img (image/load :pot-back)
   money (atom (or @info 0))
   money' (atom (util/money-with-abbreviation @info))
   ]

  :render (fn [ga ctx]
            (let [new-money (or @info 0)]
              (when (not= new-money @money)
                (reset! money new-money)
                (reset! money' (util/money-with-abbreviation new-money)))

              (letc ga [transform :transform]
                    (let [[x y] (aget transform :pos)]

                      ;; 바탕 그리기
                      (js.canvas/draw-image ctx img x y)

                      ;; 숫자 표시
                      (draw-number ctx @money' [(+ x 75) (+ y 25)]))

                    ))))
