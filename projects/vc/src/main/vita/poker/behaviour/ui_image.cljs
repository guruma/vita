(ns vita.poker.behaviour.ui-image
  (:require-macros [milk.macro :refer [def-behaviour letc]])
  (:require [js.canvas :as js.canvas]
            [milk.gatom :as gatom]
            [vita.resource.image :as image]))



(def-behaviour ui-image [& {:keys [img img-k frame order twth] :or {}}]
  [[sx sy w h] (or frame [nil nil nil nil])
   [tw th] (or twth [w h])
   img* (or img (image/load img-k))
   ]

  :order (or order 0)

  :render (fn [ga ctx]
            (letc ga [transform :transform]
                  (let [[tx ty] (aget transform :pos)]
                    (if sx
                      (let [[w' h'] (image/img-w-h img*)
                            w (or w w')
                            h (or h h')]
                        (js.canvas/draw-image ctx img* {:sx sx :sy sy
                                                       :sw w :sh h
                                                       :dx tx :dy ty
                                                       :dw tw :dh th}))

                      (js.canvas/draw-image ctx img* tx ty))))))
