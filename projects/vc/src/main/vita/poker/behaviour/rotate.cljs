(ns vita.poker.behaviour.rotate
  (:require-macros [milk.macro :refer [def-behaviour letc]])
  (:require [js.canvas :as js.canvas]
            [milk.gatom :as gatom]
            [vita.resource.image :as image]))



(def-behaviour rotate [& {:keys [src deg-spd] :or {}}]
  [img* src
   acc-duration* (atom 0)]

  :render (fn [ga ctx]
            (letc ga [transform :transform]
                  (when-let [[tx ty] (aget transform :pos)]
                    (swap! acc-duration* #(+ deg-spd %))
                    (when-let [[w h] (image/img-w-h img*)]
                      (let [dx (+ tx (/ w 2))
                            dy (+ ty (/ h 2))]
                        (js.canvas/translate ctx dx dy)
                        (js.canvas/rotate ctx @acc-duration*)
                        (js.canvas/draw-image ctx img*
                                              (- (/ w 2))
                                              (- (/ h 2)))
                        (js.canvas/rotate ctx (- @acc-duration*))
                        (js.canvas/translate ctx (- dx) (- dy)))
                      )))))
