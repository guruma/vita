(ns vita.poker.behaviour.ui-cardpair
  (:require-macros [milk.macro :refer [def-behaviour letc]])
  (:require [js.canvas :as js.canvas]
            [milk.gatom :as gatom]
            [vita.resource.image :as image]
            [vita.poker.const.pos :as pos]
            [sonaclo.util :as util]))


(def-behaviour ui-cardpair [seat card-cnt]
  [img* (case seat
          (:seat0 :seat3) (image/load :card-back-r)
          :seat2 (image/load :card-back-s)
          (:seat1 :seat4) (image/load :card-back-l)
          (image/load :test))]

  :render (fn [ga ctx]
            (letc ga [transform :transform]
                  (let [[tx ty] (aget transform :pos)]
                    ;; 소지한 장수만큼 페어카드를 엎어놓는다.
                    (reduce (fn [acc _] (do
                                          (js.canvas/draw-image ctx img* (+ tx acc) ty)
                                          (+ acc 5)))
                            0
                            (range card-cnt)))))
  )
