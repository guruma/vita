(ns vita.poker.behaviour.ui-change-text
  (:require-macros [milk.macro :refer [def-behaviour letc]])
  (:require [js.canvas :as js.canvas]
            [js.canvas.util]
            [milk.gatom :as gatom]
            [vita.poker.const.const :as const]))


(def-behaviour ui-change-text [& {:keys [text-f style offset order enable] :or {}}]

  [style (merge const/DEFAULT-STYLE style)
   font (:font style)
   color (:color style)
   align (:align style)

   [dx dy] (or offset [0 0])
   ]

  :order (or order 0)

  :enabled? (if (nil? enable) true enable)

  :enable-on
  (fn []
    (aset self :enabled? true))

  :enable-off
  (fn []
    (aset self :enabled? false))

  :render (fn [ga ctx]
            (when (aget self :enabled?)
              (letc ga [transform :transform]
                    (let [[tx ty] (aget transform :pos)]
                      (-> ctx
                          (js.canvas/save)
                          (js.canvas.util/set-style :font font :color color :align align)
                          (js.canvas/text {:text (text-f)
                                           :x (+ tx dx)
                                           :y (+ ty dy)})
                          (js.canvas/restore)
                          ))))))
