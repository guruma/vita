(ns vita.poker.behaviour.timer
  (:require-macros [milk.macro :refer [def-behaviour letc]])
  (:require [milk.gatom :as gatom]
            [milk.event :as event]
            [js.canvas :as js.canvas]
            [sonaclo.util :as util]
            [vita.resource.image :as image]
            ))


(def-behaviour timer [& {:keys [duration offset end-callback] :or {}}]

  [timer-on (image/load :time-graph)
   timer-off (image/load :time-back)
   wh (atom (image/img-w-h timer-on))
   acc-dur* (atom 0)]


  :update
  (fn [ga dt]
    (swap! acc-dur* + dt)

    (when (>= @acc-dur* duration)
      (end-callback)
      (event/del-gatom! ga)))


  :render
  (fn [ga ctx]
    (letc ga [transform :transform]
            (let [[tx ty] (util/merge-pos (aget transform :pos) offset)]

              (js.canvas/draw-image ctx timer-off tx ty)

              (if-not @wh
                (reset! wh (image/img-w-h timer-on)))

              (when-let [[w h] @wh]
                (let [ratio (/ @acc-dur* duration)
                      l' (min (dec h) (* ratio h))
                      h' (max 1 (- h l'))]
                  (js.canvas/draw-image ctx timer-on
                                        {:sx 0 :sy l' :sw w :sh h'
                                         :dx tx :dy (+ ty l') :dw w :dh h'})))

              )))

  ;; :on-destroy
  ;; (fn [ga]
  ;;   (when end-callback
  ;;     (end-callback ga)))

  )
