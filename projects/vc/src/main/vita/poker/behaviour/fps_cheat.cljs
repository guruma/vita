(ns vita.poker.behaviour.fps-cheat
  (:require-macros [milk.macro :refer [def-behaviour letc]])
  (:require [milk.gatom :as gatom]
            [milk.input.keyboard :as input.keyboard]
            [vita.const.flag :as FLAG]
            ))


(def-behaviour fps-cheat []

  []

  :order -1

  :update
  (fn [ga dt]
    (when (input.keyboard/is-keyup? "p") ;; 'p' key 키보드
      (swap! FLAG/FPS not)))

  )
