(ns vita.poker.behaviour.ui-chip
  (:require-macros [milk.macro :refer [def-behaviour letc]])
  (:require [js.canvas :as js.canvas]
            [milk.gatom :as gatom]
            [vita.resource.image :as image]
            [sonaclo.util :as util]))


(def chip-map*
  {10 [:1kd :1km :1ku]
   50 [:5kd :5km :5ku]
   100 [:10kd :10km :10ku]
   500 [:50kd :50km :50ku]
   1000 [:100kd :100km :100ku]
   })

;; 칩 단위
(def chip-units (keys chip-map*))

;; 칩사이의 거리
(def small-interval [0 -2])

;; 서로 다른 단위의 칩들 사이 거리  ex) 50칩 -- 10칩
(def big-interval [25 0])


(defn draw-img [ctx img [x y]]
  (js.canvas/draw-image ctx img x y))


(defn money->chips [money]
  (util/number-divs=>div-quots money chip-units))


(defn calc-pos [pos ntimes delta-pos]
  (util/merge-pos pos (util/multiply-pos ntimes delta-pos)))


(defn draw-chips [ctx unit cnt pos]
  (let [[down-img m-img top-img] (chip-map* unit)
        last (dec cnt)]

    (doseq [i (range cnt)]
      (condp = i
        ;; 맨 아래 칩 그리기
        0 (draw-img ctx (image/load down-img) pos)

        ;; 맨 위의 칩 그리기
        ;;(dec cnt) (draw-img ctx (image/load top-img) (calc-pos pos (dec cnt) small-interval))

        ;; 중간 칩들 그리기
        (draw-img ctx (image/load m-img) (calc-pos pos i small-interval))))))


#_(defn draw-all-chips [ctx chips pos]
  (let [f (fn [interval [unit cnt]]
            (if (< 0 cnt)
              (let [pos' (util/merge-pos pos interval)]
                (do (draw-chips ctx unit cnt pos')
                    (util/merge-pos interval big-interval)))
              interval))]

    (reduce f [0 0] chips)))


(defn draw-all-chips [ctx chips pos]

    (loop [cs chips dummys* 0 chips* 0]

      ;; 아직 (최대갯수)가 안넘었으면서, 그려질 칩이 남아있을 경우.
      (when (seq cs)

        (cond

         (and (<= 3 dummys*) (<= 6 chips*))
         nil

         ;; 한단 칩 높이가 꽉차면 옆으로 옮긴다.
         (or (and (= dummys* 0) (<= 10 chips*))
             (and (= dummys* 1) (<= 7 chips*))
             (and (= dummys* 2) (<= 9 chips*))
             (and (= dummys* 3) (<= 6 chips*)))
         (recur cs (inc dummys*) 0)

         :else
         (let [[unit cnt] (first cs)
               [down-img m-img top-img] (chip-map* unit)]

           (if (= cnt 0)

             ;; 칩 갯수가 없으면 패스
             (recur (rest cs) dummys* chips*)
             ;; 칩 그리기

             (let [pos' (util/merge-pos pos
                                        (map #(* dummys* %) big-interval)
                                        (map #(* chips* %) small-interval))]
               (if (= chips* 0)
                 ;; 맨 아래칸은 아래칩 이미지 적용
                 (draw-img ctx (image/load down-img) pos')
                 ;; 그 외는 중간칩 이미지 적용
                 (draw-img ctx (image/load m-img) pos'))

               (recur (cons [unit (dec cnt)] (rest cs)) dummys* (inc chips*)))))

         ))))


(defn draw-number [ctx money [x y]]
  (-> ctx
      (js.canvas/save)
      (js.canvas/font-style "12pt Arial")
      (js.canvas/fill-style :#FFFFFF)
      (js.canvas/text {:text money :x (+ x 5) :y (+ 40 y)})
      (js.canvas/restore)
      ))


(def-behaviour ui-chip [& {:keys [bet] :or {}}]

  [
   get-bet (if (= (type bet) (type 1))
                    (fn [] bet)
                    (fn [] @bet))
   money* (atom (get-bet))
   money** (atom (util/money-with-abbreviation @money*))
   chips* (atom (money->chips @money*))
   ]

  :order 1

  :render (fn [ga ctx]
            (letc ga [transform :transform]
                  (let [pos (aget transform :pos)
                        money (get-bet)]

                    (when (not= money @money*)
                      (reset! money* money)
                      (reset! money** (util/money-with-abbreviation money))
                      (let [chips' (money->chips money)]
                        (reset! chips* chips')))

                    (when (and money (< 0 money))

                      ;; 칩 그리기
                      (draw-all-chips ctx @chips* pos)

                      ;; 숫자 표시
                      (draw-number ctx @money** pos))

                    ))))
