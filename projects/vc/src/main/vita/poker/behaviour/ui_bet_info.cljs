(ns vita.poker.behaviour.ui-bet-info
  (:require-macros [milk.macro :refer [def-behaviour letc]])
  (:require [js.canvas :as js.canvas]
            [milk.gatom :as gatom]
            [sonaclo.util :as util]
            [vita.poker.room :as r]
            [vita.resource.image :as image]))


;;--------------------------------
;; 빨간 숫자 설정

(def red-w 18)
(def red-h 32)

(let [w red-w h red-h]
  (def num-map*
    {
     "0" [0 0 w h]
     "1" [w 0 w h]
     "2" [(* w 2) 0 w h]
     "3" [(* w 3) 0 w h]
     "4" [(* w 4) 0 w h]
     "5" [(* w 5) 0 w h]
     "6" [(* w 6) 0 w h]
     "7" [(* w 7) 0 w h]
     "8" [(* w 8) 0 w h]
     "9" [(* w 9) 0 w h]
     "," [(* w 10) 0 w h]
     "k" [(* w 11) 0 w h]
     "m" [(* w 12) 0 w h]
     "+" [(* w 13) 0 w h]
     "$" [(* w 14) 0 w h]
     "x" [(* w 15) 0 w h]
     }))


(defn draw-red-number [ctx red-img [x y] num]
  (let [[sx sy w h] (num-map* num)]
    (js.canvas/draw-image ctx red-img {:sx sx :sy sy :sw w :sh h
                                       :dx x :dy (- y 3) :dw w :dh h})
    [(+ x w) y]))



;;--------------------------------


(def-behaviour ui-bet-info [&{:keys [offset on-raise?] :or {}}]

  [
   raise-non-img (image/load :raise-w-non)
   raise-img (image/load :raise-w)
   red-img (image/load :df-s)
   ]

  :render (fn [ga ctx]
            (letc ga [transform :transform]
                  (let [[x y] (aget transform :pos)
                        money (r/get-raise-bet)
                        num-list (util/money-with-comma money)
                        delta (* red-w (count num-list))]

                    ;; 바탕 그리기
                    (if (on-raise?)
                      (do (js.canvas/draw-image ctx raise-img x y)
                          ;; 숫자 그리기
                          (reduce #(draw-red-number ctx red-img %1 %2)
                                  (map + [x y] offset [(- delta) 0])
                                  num-list))

                      (js.canvas/draw-image ctx raise-non-img x y))

                    ))))
