(ns vita.poker.behaviour.ui-text
  (:require-macros [milk.macro :refer [def-behaviour letc]])
  (:require [js.canvas :as js.canvas]
            [js.canvas.util]
            [milk.gatom :as gatom]
            [vita.poker.const.const :as const]))


(def-behaviour ui-text [& {:keys [text style offset order] :or {}}]

  [style (merge const/DEFAULT-STYLE style)
   font (:font style)
   color (:color style)
   align (:align style)

   dx (or (:dx offset) 0)
   dy (or (:dy offset) 0)]


  :order (or order 50)


  :render (fn [ga ctx]
            (letc ga [transform :transform]
                  (let [[tx ty] (aget transform :pos)]
                    (-> ctx
                        (js.canvas/save)
                        (js.canvas.util/set-style :font font :color color :align align)
                        (js.canvas/text {:text (if (= (type text) (type ""))
                                                 text
                                                 @text)
                                         :x (+ tx dx)
                                         :y (+ ty dy)})
                        (js.canvas/restore)
                        )))))
