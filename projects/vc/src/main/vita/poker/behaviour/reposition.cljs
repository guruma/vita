(ns vita.poker.behaviour.reposition
  (:require-macros [milk.macro :refer [def-behaviour letc]])
  (:require [milk.gatom :as gatom]
            [milk.event :as event]))


(def-behaviour reposition [new-pos]

  []

  :on-awake
  (fn [ga]
    (letc ga [transform :transform]

          ;; pos 변경
          (aset transform :pos new-pos)

          ;; 사멸
          (event/destroy-component! ga self))))
