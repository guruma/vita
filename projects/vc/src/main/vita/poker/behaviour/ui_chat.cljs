(ns vita.poker.behaviour.ui-chat
  (:require-macros [milk.macro :refer [def-behaviour letc]])
  (:require [js.canvas :as js.canvas]
            [milk.gatom :as gatom]))



(def chat-interval 20)


(def-behaviour ui-chat [chat]
  [
   font "13pt Arial"
   color :#FFFFFF
   ]

  :order 50

  :render (fn [ga ctx]
            (letc ga [transform :transform]
                  (let [[tx ty] (aget transform :pos)]

                    (js.canvas/save ctx)
                    (js.canvas/font-style ctx font)
                    (js.canvas/fill-style ctx color)
                    (do
                      (dorun (map-indexed
                              (fn [idx s]
                                (js.canvas/text ctx {:text s
                                                     :x (+ tx 10)
                                                     :y (+ ty 30 (* chat-interval idx))}))
                              @chat))
                      ctx)
                    (js.canvas/restore ctx)
                    ))))
