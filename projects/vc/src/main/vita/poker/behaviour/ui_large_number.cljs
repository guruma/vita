(ns vita.poker.behaviour.ui-large-number
  (:require-macros [milk.macro :refer [def-behaviour letc]])
  (:require [js.canvas :as js.canvas]
            [milk.gatom :as gatom]
            [milk.event :as event]
            [vita.resource.image :as image]
            [sonaclo.util :as util]))


(def duration 1000)


(def-behaviour ui-large-number [money]

  [
   ;;acc-dur* (atom 0)
   numstr (str "+" (util/money-with-abbreviation money))
   ]


  :order 100


  ;; :update
  ;; (fn [ga dt]
  ;;   (swap! acc-dur* + dt)

  ;;   (if (>= @acc-dur* duration)
  ;;     (event/destroy-component! ga self)))


  :render (fn [ga ctx]
            (letc ga [transform :transform]
                  (let [[tx ty] (aget transform :pos)]
                    (-> ctx
                        (js.canvas/save)
                        (js.canvas/font-style "22pt Arial")
                        (js.canvas/fill-style :#FFFF00)
                        (js.canvas/text-align :center)
                        (js.canvas/text {:text numstr :x (+ tx 30) :y (+ ty 140)})
                        (js.canvas/restore)
                        )))))
