(ns vita.poker.behaviour.ui-test-text
  (:require-macros [milk.macro :refer [def-behaviour letc]])
  (:require [js.canvas :as js.canvas]
            [milk.gatom :as gatom]))


(def-behaviour ui-test-text [& {:keys [f dx dy] :or {}}]
  []

  :order 100

  :render (fn [ga ctx]
            (letc ga [transform :transform]
                  (let [[tx ty] (aget transform :pos)]
                    (-> ctx
                        (js.canvas/save)
                        (js.canvas/font-style "13pt Arial")
                        (js.canvas/fill-style :#FF0000)
                        (js.canvas/text {:text (f)
                                         :x (+ tx dx)
                                         :y (+ ty dy)})
                        (js.canvas/restore)
                        )))))
