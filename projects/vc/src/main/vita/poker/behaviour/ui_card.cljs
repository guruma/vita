(ns vita.poker.behaviour.ui-card
  (:require-macros [milk.macro :refer [def-behaviour letc]])
  (:require [js.canvas :as js.canvas]
            [milk.gatom :as gatom]
            [sonaclo.util :as util]
            [vita.resource.image :as image]))


(def-behaviour ui-card [& {:keys [card transparency] :or {}}]
  [img* (image/load card)]
  :transparency (if transparency transparency (atom 1.0))
  ;:order 5

  :render (fn [ga ctx]
            (letc ga [transform :transform]
                  (let [[tx ty] (aget transform :pos)
                        transparency (aget self :transparency)]

                    (if (and img* tx ty)
                      (-> ctx
                          (js.canvas/save)
                          (js.canvas/alpha @transparency)
                          (js.canvas/draw-image img* tx ty)
                          (js.canvas/restore))
                      )))))
