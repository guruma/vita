(ns vita.poker.behaviour.animation
  (:require-macros [milk.macro :refer [def-behaviour letc]])
  (:require [milk.gatom :as gatom]
            [milk.event :as event]
            [js.canvas :as js.canvas]))


;; (defn find-idx [dur acc-dur i f w]
;;   (let [ratio (/ acc-dur dur)]
;;     (.round js/Math (* ratio f))))


(def-behaviour animation [& {:keys [img frame offset idx* repeat order] :or {}}]

  [
   [i f w h] frame
   [dx dy] (or offset [0 0])
   ;;idx (atom 0)
   ;;acc-duration* (atom 0)
   repeat* (atom (or repeat 1))
   ]


  :order (or order 1)


  ;; :update
  ;; (fn [ga dt]
  ;;   (swap! acc-duration* + dt)

  ;;   (if (>= @acc-duration* duration)

  ;;     (let [r @repeat*]
  ;;       (cond (< 0 r) (do (reset! acc-duration* 0)
  ;;                         (swap! repeat* dec))
  ;;             :else (event/destroy-component! ga self)))

  ;;     (reset! idx (find-idx duration @acc-duration* i f w))))


  :render
  (fn [ga ctx]
    (letc ga [transform :transform]
            (let [[tx ty] (aget transform :pos)]
              (js.canvas/draw-image ctx img {:sx (* @idx* w) :sy 0 :sw w :sh h
                                             :dx (+ tx dx) :dy (+ ty dy)
                                             :dw w :dh h}))))


  ;; :on-destroy
  ;; (fn [ga]
  ;;   (when end-callback
  ;;     (end-callback ga)))
  )
