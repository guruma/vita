(ns vita.poker.behaviour.receiver
  (:require-macros [milk.macro :refer [def-behaviour letc]]
                   [vita.logger :as vcl])
  (:require [milk.gatom :as gatom]
            [milk.event :as event]
            [sonaclo.util]
            [vita.net.core :as vita.network]
            ))


(def-behaviour receiver [& {:keys [dequeue on-awake on-receive on-error on-close sock] :or {}}]
  [acc-dt* (atom 0)]

  :on-awake
  on-awake

  :update
  #(when-let [p (dequeue)]
     (let [{:keys [type val]} p]
       (case type
         ;;:open (on-open)
         :receive (on-receive val)
         :error (on-error)
         :close (on-close)
         (vcl/ERROR "unknown net protocol =" type)))
      )

  :on-destroy
  #_(do (vcl/DEBUG "===== POKER socket close!! =====")
       (vita.network/close sock))
  )
