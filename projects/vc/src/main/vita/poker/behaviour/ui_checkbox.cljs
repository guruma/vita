(ns vita.poker.behaviour.ui-checkbox
  (:require-macros [milk.macro :refer [def-behaviour letc]])
  (:require [milk.gatom :as gatom]
            [js.canvas :as js.canvas]))



(defn- get-button-state [hover? down?]
  (cond
   down? :down
   hover? :over
   :else :normal))


(def-behaviour ui-checkbox [& {:keys [on-check enabled?* checked?* chk-img img table order] :or {}}]

  [button-wh (subvec (:normal table) 2)
   hover?* (atom false)
   down?* (atom false)]


  :order (or order 10)


  :on-mouse-up
  #(when (and @enabled?* @hover?*)
     (if @checked?*
       (reset! checked?* false)
       (do (reset! checked?* true)
           (if on-check (on-check)))))


  :on-mouse-enter
  #(when @enabled?*
     (reset! hover?* true))


  :on-mouse-exit
  #(when @enabled?*
     (reset! hover?* false))


  :on-mouse-drag
  (fn [ga down?]
    (when @enabled?*
      (reset! down?* down?)))


  :render
  (fn [ga ctx]
    (let [button-state (if @enabled?*
                         (get-button-state @hover?* @down?*)
                         :disable)]

      (letc ga [transform :transform]
            (let [[tx ty] (aget transform :pos)
                  [bw bh] button-wh
                  [sx sy w h] (button-state table)]

              (js.canvas/draw-image ctx img {:sx sx :sy sy :sw w :sh h
                                             :dx tx :dy ty :dw bw :dh bh})

              (if @checked?*
                (js.canvas/draw-image ctx chk-img tx ty))))))

  )
