(ns vita.poker.behaviour.ui-user
  (:require-macros [milk.macro :refer [def-behaviour letc]])
  (:require [js.canvas :as js.canvas]
            [vita.resource.image :as image]
            [sonaclo.util :as util]
            [milk.gatom :as gatom]))


(def-behaviour ui-user [url u-name state state-offset money bet]

  [player-img* (image/load :player)
   player-back* (image/load :player-back)
   img* (if (seq url) (try (util/load-image url)
                           (catch js/Error err
                             nil)))
   wh (atom nil)

   money' (atom @money)
   money'' (atom (util/money-with-abbreviation @money))]

  :render (fn [ga ctx]
            (letc ga [transform :transform]
                  (let [[tx ty] (aget transform :pos)
                        [ux uy] (map + [tx ty] state-offset)]

                    (js.canvas/draw-image ctx player-back* tx ty)
                    (js.canvas/draw-image ctx player-img* (+ 9 tx) (+ 9 ty))

                    (when img*
                      (if-not @wh
                        (reset! wh (image/img-w-h img*)))
                      (if-let [[w h] @wh]
                        (js.canvas/draw-image ctx img* {:sx 0 :sy 0
                                                        :sw w :sh h
                                                        :dx (+ 10 tx) :dy (+ 10 ty)
                                                        :dw 74 :dh 74})))

                    (when-let [state-img (image/load @state)]
                      (js.canvas/draw-image ctx state-img (+ 9 ux) (+ 9 uy)))

                    (let [new-money @money]
                      (when (not= new-money @money')
                        (reset! money' new-money)
                        (reset! money'' (util/money-with-abbreviation new-money))))

                    (-> ctx
                        (js.canvas/save)
                        (js.canvas/font-style "11pt Arial")
                        (js.canvas/text-align :center)
                        (js.canvas/fill-style :#87ceeb)
                        (js.canvas/text {:text (name @u-name)
                                         :x (+ tx 48) :y (+ ty 103)})
                        ;;(js.canvas/text {:text @bet
                        ;;                 :x (+ tx 10) :y (+ ty 40)})
                        (js.canvas/fill-style :#ffffff)

                        (js.canvas/text {:text @money''
                                         :x (+ tx 48) :y (+ ty 121)})

                        (js.canvas/restore))))))
