(ns vita.poker.behaviour.ui-result
  (:require-macros [milk.macro :refer [def-behaviour letc]])
  (:require [js.canvas :as js.canvas]
            [vita.resource.image :as image]
            [milk.gatom :as gatom]))


(def-behaviour ui-result [result]

  [img (image/load result)]

  :order 10

  :render (fn [ga ctx]
            (letc ga [transform :transform]
                  (when img
                    (let [[tx ty] (aget transform :pos)]
                      (js.canvas/draw-image ctx img tx ty))))))
