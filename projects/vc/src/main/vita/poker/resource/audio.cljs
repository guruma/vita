(ns vita.poker.resource.audio)


(def audio-paths
  {
   :card-open "sound/card/card-open.mp3"
   :card-deal "sound/card/card-deal.mp3"

   :chip-return-1 "sound/card/chip-return-1.mp3"
   :chip-return-2 "sound/card/chip-return-2.mp3"
   :chip-stack "sound/card/chip-stack.mp3"

   :call "sound/card/voice-call.mp3"
   :check "sound/card/voice-check.mp3"
   :fold "sound/card/voice-fold.mp3"
   :raise "sound/card/voice-raise.mp3"
   })
