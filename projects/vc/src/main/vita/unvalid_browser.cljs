(ns vita.unvalid-browser
  (:require
   [jayq.core]
   [js.canvas :as cv]
   [goog.userAgent]
   ))


;; TODO(kep) goog 라이브러리 사용. http://docs.closure-library.googlecode.com/git/namespace_goog_userAgent.html

(defn supported-browser? []
  (let [ua (.toLowerCase (.getUserAgentString goog.userAgent))

        ;; ie 11 버전부터는 MSIE 라는 문구를 더이상 지원하지 않는다.
        ;; http://msdn.microsoft.com/en-us/library/ie/bg182625(v=vs.110).aspx
        ;; rv:11 이라는 문구로 대체함.

        ;; windows8 에서 ie11 브라우저에 '도구' -> '호환성 보기 설정' 에
        ;; 해당 사이트를 추가하지 않으면,
        ;; msie 10 으로 출력되는 버그가 있어서,
        ;; 허용하기로 변경 (실제로 ie 10 버전에서 게임 하는데 큰 문제 없었음)
        check-list ["msie 10"
                    "rv:11"
                    "chrome"
                    "safari"
                    "firefox"
                    "opera"
                    "windows"
                    "macintosh"]
        mybws (set (keep #(if (not= -1 (.indexOf ua %)) %) check-list))]

    (.log js/console "browser check : " ua)

    (cond

     ;; ie 11, chrome, firfox, opera, safari-linux
     ;; supported list
     (some #(contains? mybws %) ["msie 10" "rv:11" "chrome" "firefox" "opera"])
     true

     ;; windows safari is not surpported!
     (contains? mybws "safari")
     (if (contains? mybws "windows")
       false
       true)

     :else
     false)))


(defn unvalid-warn [msg]
  (let [canvas (-> (jayq.core/$ :#milk-canvas) (.get 0))
        w (.-width canvas)
        h (.-height canvas)
        ctx (-> canvas (cv/get-context :2d))]

    (-> ctx
        (cv/fill-style :#333333)
        (cv/fill-rect {:x 0 :y 0 :w w :h h})
        (cv/fill-style :#777777)
        (cv/text-align :center)
        (cv/font-style "35pt Arial")
        (cv/text {:text msg :x 640 :y 480})
        (cv/font-style "20pt Arial")
        (cv/text {:text "Please use these - IE 11, Chrome, Safari, Firefox, Opera"
                  :x 640 :y 530}))

    (.log js/console msg)))
