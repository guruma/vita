(ns vita.const.locale
  (:require [vita.shared.const.err-code :as ERR]
            ))



;; 나라 인덱스
(def KOREA 0)
(def AMERICA 1)



(def text-map
  {
   ERR/DB_RETRY_FAIL ["서버 데이터 오류입니다"
                      "Server data error"
                      ]
   ERR/EMPTY_ACCOUNT ["계정이 비었습니다"
                      "Empty account!"
                      ]
   ERR/DELETED_ROOM ["해당 룸이 존재하지 않습니다"
                     "Room not exist!"
                     ]
   ERR/NOT_ENOUGH_ACCOUNT_MONEY ["돈이 부족합니다"
                                 "Not enough account money"
                                 ]
   ERR/DUPLICATE_SEAT ["앉을 수 없는 의자입니다"
                       "Duplicate seat!"
                       ]
   ERR/ALREADY_INVITE ["이미 초대하셨습니다"
                       "Already send invite message"
                       ]
   ERR/ALREADY_DEL_INVITEE ["초대받은 유저를 삭제할 수 없습니다"
                            "Already deleted user!"
                            ]

   ERR/SOCKET_CLOSE ["서버와의 연결이 끊겼습니다"
                     "Disconnected!"
                     ]
   ERR/SOCKET_ERROR ["서버와 접속이 원할하지 않습니다"
                     "Socket error!"
                     ]

   })


(defn get-text [code]
  ;; TODO(jdj) 현재 나라는 미국만...
  (get-in text-map [code AMERICA] "Unknown error!"))
