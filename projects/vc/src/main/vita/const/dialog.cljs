(ns vita.const.dialog
  (:require [vita.shared.const.err-code :as ERR]))



;;--------------------
;; BUY-IN dialog

(def UI_BUYIN_BACK
  {:img-k :buyin-back
   :pos [373 280]
   :w 550
   :h 330})


(defn- item-table [n [w h]]
  (->> (iterate (partial + w) 0)
       (take n)
       (mapv (fn [x] [x 0 w h]))))


(defn- button-table [[tw h]]
  (let [w (/ tw 4)
        [normal over down disable] (item-table 4 [w h])]
    {:normal normal :over over :down down :disable disable}))


(def BTN_OK_INFO
  {:img-k :btn-ok :table (button-table [348 30]) :w 87 :h 30})


(def BTN_CANCEL_INFO
  {:img-k :btn-cancel :table (button-table [348 30])})


(def BTN_AUTO_REFILL
  {:img-k :check-n :chk-img-k :check-p :table (button-table [80 20])})


(def PRG_BUY_IN
  {:rect [430 452 420 20]})


(def LBL_MY_CHIP
  {:pos [540 420]})


(def BUY_IN_DIALOG_AUTO_REFILL_BTN
  {:pos [390 575]})


(def BUY_IN_DIALOG_OK_BTN
  {:pos [440 530]})


(def BUY_IN_DIALOG_CANCEL_BTN
  {:pos [740 530]})


(def BTN_CLICK_TO_SIT_INFO
  {:img-k :click-to-sit :table (button-table [968 63]) :pos [515 500]})


(def BTN_X_CLOSE_INFO
  {:img-k :close :table (button-table [116 29]) :pos [862 315]})


(def BTN_-_INFO
  {:img-k nil :table (button-table [120 30]) :pos [404 448]})


(def BTN_+_INFO
  {:img-k nil :table (button-table [120 30]) :pos [843 448]})


;;--------------------
;; TIMEOUT dialog

(def TIMEOUT_STYLE {:color :#FFFF00 :font "20pt Arial"})

(def TIMEOUT_DIALOG_TEXT_1 "SESSION TIMEOUT")

(def TIMEOUT_DIALOG_TEXT_2 "PLEASE RELOGIN")

(def TIMEOUT_BACK_POS [415 280])

(def TIMEOUT_OK_BTN_POS [600 470])

(def TIMEOUT_DIALOG_TEXT_OFFSET_1 {:dx 30 :dy 100})

(def TIMEOUT_DIALOG_TEXT_OFFSET_2 {:dx 30 :dy 150})



;;--------------------
;; ERROR dialog

(def ERR_TEXT_STYLE {:color :#FFFF00 :font "20pt Arial" :align :center})

(def ERR_TEXT_STYLE2 {:color :#FFFF00 :font "12pt Arial" :align :center})

(def ERR_DIALOG_POS [415 280])

(def ERR_DIALOG_OK_BTN_POS [600 470])

(def ERR_DIALOG_TEXT_OFFSET_1 {:dx 225 :dy 100})

(def ERR_DIALOG_TEXT_OFFSET_2 {:dx 225 :dy 130})


;;--------------------
;; FREECOIN dialog

(def BTN_FREECOIN_OK_INFO
  {:img-k :gift-ok :table (button-table [1524 47]) :w 381 :h 47})
