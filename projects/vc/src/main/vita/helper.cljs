(ns vita.helper
  (:require-macros
   [cljs.core.async.macros :as m :refer [go]]
   [vita.config.macros :as vcm]
   [vita.logger :as vcl])
  (:require
   [cljs.core.async :as async :refer [<! >! chan close! sliding-buffer put! alts! take! timeout]]
   [milk.event :as event]
   [sonaclo.util]
   [vita.my]
   [sdk.facebook.core :as fb]

   [vita.scene.loading.core]
   [vita.const :as GAME]
   [vita.net.core :as vita.network]))


(defn get-sock [scene-name]
  [scene-name (get GAME/URL_DIC scene-name)])


(defn check-connection-from-dequeue
  "dequeue 를 시도해서 서버 접속됬는지 확인한다"
  [sock]
  (let [result-ch (chan 1)]
    (go
      (loop [cnt 0]
        (if (< 100 cnt)
          (close! result-ch)
          (do (<! (timeout 500))
              (let [p (vita.network/dequeue sock)]
                (if (= :open (:type p))
                  (do (vcl/DEBUG "pppppppppp-> " p)
                      (vita.my/set-is-loginned? false)
                      (>! result-ch p)
                      (close! result-ch))
                  (recur (inc cnt))))))))
    result-ch))



(defn get-connect [sock]
  (let [result-ch (chan 1)]
    (go
      (loop [try-num 0]

        (if (< 30 try-num)

          ;; connection 시도가 초과하면 실패
          (close! result-ch)

          ;; connection 맺기 시도.
          (let [connect-info (vita.network/connect sock)]

            (if (= :connected connect-info)
              ;; 이미 연결 상태
              (>! result-ch true)

              (do
                ;; pulling을 걸어주고,
                (if-let [packet (<! (check-connection-from-dequeue sock))]

                  ;; 소켓이 열렸으면,
                  (case (:type packet)
                    :open (do (>! result-ch true)
                              (close! result-ch))
                    (recur (inc try-num)))

                  ;; 닫혀있으면 재시도
                  (recur (inc try-num)))))))))
    result-ch))



(defn connection-helper [[prg-loading lbl-loading] scene-name resource-loaded-ch resource-load-start]

  (go

    ;; 로긴 토큰이 없으면, fb sdk 로직이 끝나서 세팅 될때까지 대기
    (when-not (vita.my/get-login-token)
      (aset lbl-loading :text "waiting for login data from ...")
      (<! vita.my/login-token-ch*))


    ;; 앱 서버 접속 시작
    (let [sock (get-sock scene-name)
          ldl-bar (chan)
          is-connected? (<! (get-connect sock))]

      (if is-connected?

        ;; 연결 성공
        (go (resource-load-start) ;; 리소스 다운 시작
            (<! resource-loaded-ch) ;; 리소스 다운 완료 확인 후,
            (event/load-scene! scene-name)) ;; 씬 로딩 시작

        ;; 연결 실패
        (aset lbl-loading :text "fail to connect, please retry")))))


(defn loading->scene [scene-name]
  (vita.scene.loading.core/load

   scene-name

   (fn [[prg-loading lbl-loading] resource-loaded-ch resource-load-start]
       (connection-helper [prg-loading lbl-loading] scene-name resource-loaded-ch resource-load-start))))



(defn load-scene! [scene-name]
  (event/load-scene! :loading scene-name))
