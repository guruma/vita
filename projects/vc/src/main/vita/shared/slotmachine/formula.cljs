(ns vita.shared.slotmachine.formula
  "formula"

        
  (:require [js.math])
  )


(def ^:private pay-table*
  {:gold7      [  0    0    3  35 100   0]
   :red7       [  0    0    3  35 100 350]
   :blue7      [  0    0    3  30  90 300]
   :bar3       [  0    0    1  18  54 180]
   :bar2       [  0    0    1  15  45 150]
   :bar1       [  0    0    1  12  36 120]
   :lemon      [  0    0    0  10  30 100]
   :apple      [  0    0    0   9  27  90]
   :cherry     [  0    0    0   8  24  80]
   :watermelon [  0    0    0   7  21  70]
   :grape      [  0    0    0   6  18  60]})


(def pay-lines*
  [[1 1 1 1 1]
   [0 0 0 0 0]
   [2 2 2 2 2]
   [0 1 2 1 0]
   [2 1 0 1 2]

   [1 0 0 0 1]
   [1 2 2 2 1]
   [0 0 1 2 2]
   [2 2 1 0 0]
   [1 0 1 2 1]

   [1 2 1 0 1]
   [0 1 1 1 0]
   [2 1 1 1 2]
   [0 1 0 1 0]
   [2 1 2 1 2]

   [1 1 0 1 1]
   [1 1 2 1 1]
   [0 0 2 0 0]
   [2 2 0 2 2]
   [0 2 2 2 0]])


(defn get-paytable
  ([minimum-bet]
     (get-paytable minimum-bet pay-table*))
  ([minimum-bet pay-table]
     (->> pay-table
          (reduce-kv (fn [acc k v]
                       (assoc acc k (mapv (partial * minimum-bet) v)))
                     {}))))


(defn jackpot? [bingo]
  (->> (vals bingo)
       (some (fn [[item n]] (and (= item :gold7) (= n 5))))
       boolean))


(defn calc-payout-amount
  " ;; @ex bingo {8 [:apple 4 '([0 0] [1 0] [2 0] [3 0])]
   ;;            3 [:blue7 3 '([0 1] [1 1] [2 1])]}"

  [bingo per-line-bet]
  (if (empty? bingo)
    0
    (->> (vals bingo)
         (map (fn [[item n]] (get-in (get-paytable per-line-bet) [item n])))
         (reduce +))))


(defn- get-bingo-info
  "- triples {vector of vector} : triple in the frame.
   - line {vector of int} : line to be calculated against triples.
   - return {int | nil} : count of bingo in the line against triples,
                         or nil
   calculate the count of bingo in tne line against the triples.
   usage : (bingo-line triples [1 1 2 1 2])"
  [triples line]


  (let [positions (map-indexed vector line)
        items (map #(get-in triples %) positions)
        first-item (first items)
        n (->> items
               (concat [first-item])
               (partition 2 1)
               (take-while (partial apply =))
               (count))]

    (when (>= n 2)

      (cond
       ;; question 일때,
       (= :question first-item)
       nil

       ;; 과일이 2개일때.
       (and (first-item #{:apple :lemon :grape :watermelon :cherry}) (= n 2))
       nil

       :else
       [first-item n (take n positions)]))))


(defn bingo-lines
  "- triples {vector of vector} : triples in the frame.
   - lines {vector of vector} : lines to be calculated against triples. ( frames.cljs에 정의된 lines* ).
   - return {map} : map of line number and bingo count in the triples.
                  {8 [:watermelon 3 ([0 0] [1 0] [2 1])], 13 [:apple 3 ([0 2] [1 1] [2 2])]} "
  [triples lines]

  (->> pay-lines*
       (take lines)
       (keep-indexed (fn [i line]
                       (when-let [bingo-info (get-bingo-info triples line)]
                         [i bingo-info])))
       (into {})))


(defn sync-triples [triples backs]
  (let [qm-idxs (for [[i triple] (map-indexed vector triples)
                      [j item] (map-indexed vector triple)
                      :when (= item :question)]
                  [i j])]
    (loop [acc triples
           [f & rst] qm-idxs]
      (if (empty? f)
        acc
        (recur (assoc-in acc f (get backs f)) rst)))))


(defn calc-total-earned-chip [triples-with-qms qm-backs bet-line-count per-line-bet]
  (let [triple (sync-triples triples-with-qms qm-backs)
        bingo  (bingo-lines triple bet-line-count)
        amount (calc-payout-amount bingo per-line-bet)
        gold7-count (count (filter #{:gold7} (flatten triple)))]
           (* amount (js.math/pow 2 gold7-count))
                                                    
    ))

;;;;;;;;;;;; This file autogenerated from cljx/vita/shared/slotmachine/formula.cljx
