(ns vita.asset.loader
  (:require-macros
   [cljs.core.async.macros :as m :refer [go go-loop]])
  (:require
   [cljs.core.async :as async :refer [<! >! chan close! sliding-buffer put! alts! take! timeout]]
   [vita.resource.image :as image]
   [vita.resource.sound :as sound])
  )


#_(defn async-preload [pre-asset-infos callback]
  (let [len (count pre-asset-infos)]

    (if (zero? len)
      (callback nil [0 0])

      (let [c (chan)
            put-fn (fn [name]
                     (fn [res] (put! c (assoc res :name name))))]

        (go (doseq [asset-info pre-asset-infos]
              (let [{:keys [name type src]} asset-info
                    put-fn' (put-fn name)]
                (case type
                  :image (load-image src
                                     :on-load put-fn'
                                     :on-error put-fn')
                  :audio (load-audio src
                                     :on-load put-fn'
                                     :on-error put-fn')
                  nil
                  ))))

        (let [load-fn (fn [x cur-idx]
                        (callback x [cur-idx len]))]
          (go-loop [idx 0
                    info (<! c)]
            (if (>= idx len)
              (close! c)

              (do (load-fn info (inc idx))
                  (let [{:keys [name]} info]
                    (swap! asset-dic* assoc name info))
                  (recur (inc idx) (<! c))))))))))


(defn async-preload [scene-name on-load-callback end-callback]

  (go (image/preload scene-name on-load-callback end-callback))
  (go (sound/preload))
  (go (sound/preload-bgm scene-name))
  )
