(ns vita.app
  (:require [vita.dialog-manager :as dialog-manager]))


(def ^:private dialog-manager*
  (atom (dialog-manager/DialogManager. [])))


(defn dialog:show [element]
  (-> dialog-manager*
      (swap! (fn [this]
               (dialog-manager/show this element)))))


(defn dialog:hide []
  (-> dialog-manager*
      (swap! (fn [this]
               (dialog-manager/hide this)))))
