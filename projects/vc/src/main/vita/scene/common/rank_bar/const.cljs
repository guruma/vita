(ns vita.scene.common.rank-bar.const)


;; 기본 위치
(def height 810)
(def tab-pos [220 (- height 20)])


;; 왼쪽 위치
(def left 205)
(def left-edge-pos [left height])
(def left-page-btn [left (+ 25 height)])
(def left-page-end-btn [left (+ 70 height)])


;; 오른쪽 위치
(def right 1050)
(def right-edge-pos [right height])
(def right-page-btn [right (+ 25 height)])
(def right-page-end-btn [right (+ 70 height)])


;; 선물주기 버튼 오프셋
(def gift-btn-offset [10 115])


;; 사람 1명당 길이
(def person-interval 104)


;;----------------
;; helper

(defn- item-table [n [w h]]
  (->> (iterate (partial + w) 0)
       (take n)
       (mapv (fn [x] [x 0 w h]))))


(defn- button-table [[tw h]]
  (let [w (/ tw 4)
        [normal over down disable] (item-table 4 [w h])]
    {:normal normal :over over :down down :disable disable}))


(def BTN_LEFT_PAGE_INFO
  {:img-k :mlob-side-left-page :table (button-table [68 41])})

(def BTN_LEFT_PAGE_END_INFO
  {:img-k :mlob-side-left-page-end :table (button-table [68 41])})

(def BTN_RIGHT_PAGE_INFO
  {:img-k :mlob-side-right-page :table (button-table [68 41])})

(def BTN_RIGHT_PAGE_END_INFO
  {:img-k :mlob-side-right-page-end :table (button-table [68 41])})


(def BTN_INVITE_INFO
  {:img-k :mlob-invite :table (button-table [304 36])})
