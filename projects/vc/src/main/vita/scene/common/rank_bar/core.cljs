(ns vita.scene.common.rank-bar.core
  (:require-macros [vita.logger :as vcl])
  (:require
   [milk.gatom :as gatom]
   [milk.event :as event :refer [new-gatom]]

   [vita.scene.common.behaviour.ui-image :refer [ui-image]]
   [vita.scene.common.behaviour.ui-ani-images :refer [ui-ani-images]]
   [vita.scene.common.behaviour.ui-button :refer [ui-button]]
   [vita.scene.common.behaviour.ui-event :refer [ui-event]]
   [vita.scene.common.behaviour.ui-label :refer [ui-label]]

   [vita.scene.common.rank-bar.const :as CONST]

   [vita.resource.image :as image]
   [sonaclo.util :as util]
   [vita.my :as my]
   ))


;;---------------------
;; 페이지 번호 정보

(def max-page* (atom 1))
(defn get-max-page [] @max-page*)
(defn set-max-page [page] (reset! max-page* page))

(def cur-page* (atom 1))
(defn get-cur-page [] @cur-page*)
(defn set-cur-page [page] (reset! cur-page* (min @max-page* page)))


;;---------------------
;; send 함수를 받아서 초기화 한다.

(def send-fn* (atom nil))

(defn- init-send-fn [f] (reset! send-fn* f))
(defn- send-fn [& args]

  (when-let [f @send-fn*]
    (apply f args)))


;;---------------------
;; 탭, 관련 cmd 정보

(def tab-cmds {:friendlist :Qfriendlist
              :ranklist :Qranklist})

(def cur-tab* (atom :friendlist))

(defn get-page-cmd []
  (tab-cmds @cur-tab*))

(defn change-tab []
  ;; TODO...
  )



;;---------------------
;; 하단 리스트 초기화 로직


(def btn-gs* (atom {}))


(defn- send-visible-on-message [ga]
  (let [comp (gatom/get-b ga :ui-button)]
    (event/send-message! comp :visible-on)))


(defn- send-visible-off-message [ga]
  (let [comp (gatom/get-b ga :ui-button)]
    (event/send-message! comp :visible-off)))


(defn page-btn-check [cur-page max-page]
  (let [{:keys [l-ga l-end-ga r-ga r-end-ga]} @btn-gs*]
    (if (<= cur-page 1)
      (do (send-visible-off-message l-ga)
          (send-visible-off-message l-end-ga))
      (do (send-visible-on-message l-ga)
          (send-visible-on-message l-end-ga)))
    (if (<= max-page cur-page)
      (do (send-visible-off-message r-ga)
          (send-visible-off-message r-end-ga))
      (do (send-visible-on-message r-ga)
          (send-visible-on-message r-end-ga)))))


(defn init [send]

  (set-cur-page 1)

  (init-send-fn send)

  (event/new-gatom "tab"
                   [(ui-image :img (image/load :mlob-tab-friend-ranking))]
                   :pos CONST/tab-pos)

  ;; 왼쪽 마감처리
  (event/new-gatom "left"
                   [(ui-image :img (image/load :mlob-side-left-edge))]
                  :pos CONST/left-edge-pos)

  ;; 오른쪽 마감처리
  (event/new-gatom "right"
                   [(ui-image :img (image/load :mlob-side-right-edge))]
                   :pos CONST/right-edge-pos)

  ;; 좌우 페이지 버튼
  (let [l-ga (event/new-gatom "left-page-btn"
                              [(ui-button :button-info CONST/BTN_LEFT_PAGE_INFO
                                          :on-click
                                          #(when (not= 1 (get-cur-page))
                                             (send-fn (get-page-cmd)
                                                      {:page (dec (get-cur-page))}))
                                          )]
                              :pos CONST/left-page-btn)

        l-end-ga (event/new-gatom "left-page-end-btn"
                                  [(ui-button :button-info CONST/BTN_LEFT_PAGE_END_INFO
                                              :on-click
                                              #(when (not= 1 (get-cur-page))
                                                 (send-fn (get-page-cmd)
                                                          {:page 1}))
                                              )]
                                  :pos CONST/left-page-end-btn)
        r-ga (event/new-gatom "right-page-btn"
                              [(ui-button :button-info CONST/BTN_RIGHT_PAGE_INFO
                                          :on-click
                                          #(when (< (get-cur-page) (get-max-page))
                                             (send-fn (get-page-cmd)
                                                      {:page (inc (get-cur-page))}))
                                          )]
                              :pos CONST/right-page-btn)

        r-end-ga (event/new-gatom "right-page-end-btn"
                                  [(ui-button :button-info CONST/BTN_RIGHT_PAGE_END_INFO
                                              :on-click
                                              #(when (< (get-cur-page) (get-max-page))
                                                 (send-fn (get-page-cmd)
                                                          {:page (get-max-page)}))
                                              )]
                                  :pos CONST/right-page-end-btn)]


    (swap! btn-gs* assoc
           :l-ga l-ga
           :l-end-ga l-end-ga
           :r-ga r-ga
           :r-end-ga r-end-ga))



  ;; 랭크 리스트 사람들어갈 자리 세팅
  (doseq [[x i] (map vector (range 220 1000 CONST/person-interval) (cycle [true false]))]
    (event/new-gatom "p1"
                     [(ui-image :img (if i
                                       (image/load :mlob-player-02)
                                       (image/load :mlob-player-01)))]
                     :pos [x CONST/height]))
  )
