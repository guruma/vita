(ns vita.scene.common.rank-bar.util
  (:require
   [sonaclo.util :as util]
   [milk.event :as event :refer [new-gatom]]
   [vita.scene.common.behaviour.ui-label :refer [ui-label]]
   [vita.scene.common.behaviour.ui-image :refer [ui-image]]

   [vita.scene.common.rank-bar.const :as CONST]))


;;-----------------
;; helper 함수

(defn person-ga [x name avatar-url]
  (let [ga (event/new-gatom "person"
                            [(ui-image :img (util/load-image avatar-url) :dx 28 :dy 28)
                             (ui-label :text name
                                       :style {:color :#FFFFFF :font "10pt Arial"}
                                       :dx 52
                                       :dy 100
                                       :align :center)]
                            :pos [x CONST/height])]
    ga))
