(ns vita.scene.common.rank-bar.ranklist
  (:require
   [milk.event :as event :refer [new-gatom]]))

;;-----------------------------
;; game rank list
;; TODO(jdj) 친구 전체 리스트는 릴리즈 이후로....
;;-----------------------------

(def rank-gs* (atom []))

(defn add-rank-ga [ga]
  (swap! rank-gs* conj ga))

(defn clear-rank-gs []
  (doseq [ga @rank-gs*]
    (event/del-gatom ga))
  (reset! rank-gs* []))


(defn set-rank-list
  [rank-list]
  (doseq [{:keys [name money sid avatar-url]} rank-list]
    ;; .... TODO
    ;; 릴리즈 이후....


    ))
