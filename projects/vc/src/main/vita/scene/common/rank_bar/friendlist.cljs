(ns vita.scene.common.rank-bar.friendlist
  (:require
   [milk.event :as event :refer [new-gatom]]
   [sonaclo.util :as util]
   [sdk.facebook.core :as fb]
   [vita.my :as my]
   [vita.scene.common.rank-bar.core :as rank-bar.core]
   [vita.scene.common.behaviour.ui-button :refer [ui-button]]
   [vita.scene.common.behaviour.ui-label :refer [ui-label]]
   [vita.scene.common.rank-bar.const :as CONST]
   [vita.scene.common.rank-bar.util :as rank-bar.util]))


;;-----------------------------
;; my friend list 관련
;;-----------------------------

(def me* (atom nil))

(defn clean-me []
  (when-let [ga @me*]
    (event/del-gatom ga))
  (reset! me* nil))


(def friend-gs* (atom []))

(defn add-friend-ga [ga]
  (swap! friend-gs* conj ga))

(defn clean-friend-gs []
  (doseq [ga @friend-gs*]
    (event/del-gatom ga))
  (reset! friend-gs* []))



(defn make-friend [x sid name money avatar-url registered?]
  (let [ga (rank-bar.util/person-ga x name avatar-url)]

    (add-friend-ga ga)

    (if registered?

      ;; 등록된 친구의 경우 - 머니 정보 + 선물보내기 버튼
      (do
        (event/add-component! ga (ui-label :text (util/money-with-comma money)
                                           :style {:color :#FFFF00 :font "9pt Arial"}
                                           :dx 52
                                           :dy 120
                                           :align :center))
        ;; TODO(jdj) 선물보내기 버튼. 릴리즈 이후로....
        #_(let [gift-ga (event/new-gatom "btn"
                                       [(ui-button :button-info CONST/SEND_GIFT_BTN_INFO
                                                   :on-click
                                                   (fn [] (.log js/console "gift click")))]
                                       :pos (map + [x height] gift-btn-offset))]
          (add-friend-ga gift-ga)))

      ;; 등록 안된 친구의 경우.
      (let [btn-ga (event/new-gatom "btn-invite"
                                    [(ui-button :button-info CONST/BTN_INVITE_INFO
                                                :on-click (fn []
                                                            (fb/friends-request [sid] "Try your best luck!"
                                                                              #(rank-bar.core/send-fn
                                                                                :Qinvitefriend ;; TODO(jdj)
                                                                                {:invitee (first %)})))
                                                )]
                                    :pos [(+ 15 x) (+ 105 CONST/height)])]
        (add-friend-ga btn-ga)))))



(defn make-me []
  (let [ga (rank-bar.util/person-ga 220 (my/get-name) (my/get-avatar-url))]
    (event/add-component! ga (ui-label :text (util/money-with-comma (my/get-money))
                                       :style {:color :#FFFF00 :font "9pt Arial"}
                                       :dx 52
                                       :dy 120
                                       :align :center))
    (reset! me* ga)))


(defn set-friend-list
  "내 친구 리스트 뷰잉을 초기화 한다.
  friends = [{:name 'aaa'
               :sid 'EFEFEFSDFD'
               :money 1230000
               :avatar-url ''
               :registered? true}
              ...]
"
  [friends]
  (clean-friend-gs)

  (clean-me)
  (make-me)

  ;; 친구 정보 세팅
  (doseq [[friend pos-x] (map vector friends (range (+ 220 CONST/person-interval)
                                                    1000
                                                    CONST/person-interval))]
    (let [{:keys [name sid money avatar-url registered?]} friend]
      (make-friend pos-x sid name money avatar-url registered?))))
