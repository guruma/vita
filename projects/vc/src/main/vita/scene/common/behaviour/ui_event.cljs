(ns vita.scene.common.behaviour.ui-event
  (:require-macros [milk.macro :refer [def-behaviour letc]])
  (:require [milk.gatom :as gatom]
            [js.canvas :as js.canvas]
            ))


(def-behaviour ui-event [& {:keys [on-click] :or {}}]
  [hover?* (atom false)
   down?* (atom false)]

  :enabled? true

  :on-mouse-up
  #(when (and (aget self :enabled?) @hover?*)
     (when on-click
       (on-click self)))

  :on-mouse-enter
  #(when (aget self :enabled?)
     (reset! hover?* true))

  :on-mouse-exit
  #(when (aget self :enabled?)
     (reset! hover?* false))

  :on-mouse-drag
  (fn [ga down?]
    (when (aget self :enabled?)
      (reset! down?* down?))))
