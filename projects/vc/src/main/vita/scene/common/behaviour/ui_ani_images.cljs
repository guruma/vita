(ns vita.scene.common.behaviour.ui-ani-images
  (:require-macros [milk.macro :refer [def-behaviour letc]])
  (:require [js.canvas :as js.canvas]
            [vita.resource.image :as image]
            [milk.gatom :as gatom]))


(def-behaviour ui-ani-images [& {:keys [img-ks duration order] :or {}}]
  [n-img-dic (->> img-ks
                  (map image/load)
                  (map-indexed vector)
                  (into {}))
   max-n (count img-ks)
   cur-n* (atom 0)
   interval (quot duration max-n)
   acc-duration* (atom 0)]


  :order
  (or order 0)

  :update
  (fn [ga dt]
    (swap! acc-duration* + dt)

    (if (>= @acc-duration* duration)
      (swap! acc-duration* - duration)
      (reset! cur-n* (quot @acc-duration* interval))))

  :render
  (fn [ga ctx]
    (letc ga [transform :transform]
          (let [[tx ty] (aget transform :pos)
                img (n-img-dic @cur-n*)]
            (js.canvas/draw-image ctx img tx ty)))))
