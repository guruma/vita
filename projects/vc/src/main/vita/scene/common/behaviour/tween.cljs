(ns vita.scene.common.behaviour.tween
  (:require-macros [milk.macro :refer [def-behaviour letc]])
  (:require [milk.gatom :as gatom]
            [milk.event :as event]))


(defn linear [t b c d]
  (+ (* c (/ t d)) b))


(defn easing-out-expo [t b c d]
  (+ (* c (inc (- (.pow js/Math 2 (* -10 (/ t d))))))
     b))


(def-behaviour tween [& {:keys [from to duration end-callback] :or {}}]

  [[fx fy] from
   [ex ey] to
   acc-duration* (atom 0)]

  :on-awake
  (fn [ga]
    (letc ga [transform :transform]
          (aset transform :pos [fx fy])))

  :update
  (fn [ga dt]
    (swap! acc-duration* + dt)

    (if (>= @acc-duration* duration)
      (event/destroy-component! ga self)

      (letc ga [transform :transform]
            (let [next-p [(linear @acc-duration* fx (- ex fx) duration)
                          (easing-out-expo @acc-duration* fy (- ey fy) duration)]]
              (aset transform :pos next-p)))))

  :on-destroy
  (fn [ga]
    (when end-callback
      (end-callback ga))))
