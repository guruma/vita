(ns vita.scene.common.behaviour.ui-progress
  (:require-macros [milk.macro :refer [def-behaviour letc]])
  (:require [milk.gatom :as gatom]
            [js.canvas :as js.canvas]
            [milk.component.box-collider :as component.box-collider]
            [milk.event :as event]
            [milk.input.mouse :as input.mouse]
            [vita.resource.image :as image]))


(defn range-in [min x max]
  (cond (<= x min) min
        (>= x max) max
        :else x))


(def-behaviour ui-progress [{:keys [wh on-progress progress* order pointer-img-k draw-bar?] :or {draw-bar? true}}]
  [[progress-w progress-h] wh
   hover?* (atom false)
   down?* (atom false)
   pointer-img (image/load pointer-img-k)

   progress* (or progress* (atom 1.0))
   ]

  :order
  (or order 0)

  :enabled?
  true

  :on-mouse-enter
  #(when (aget self :enabled?)
     (reset! hover?* true))

  :on-mouse-exit
  #(when (aget self :enabled?)
     (reset! hover?* false))

  :on-mouse-drag
  (fn [ga down?]
    (when (aget self :enabled?)
      (letc ga [transform :transform]
            (let [[tx ty] (aget transform :pos)
                  [px py] (input.mouse/mouse-pos)
                  progress (range-in 0 (/ (- px tx) progress-w) 1)]
              (when on-progress
                (on-progress progress))
              (reset! progress* progress)))
      (reset! down?* down?)))

  :render
  (fn [ga ctx]
    (letc ga [transform :transform]
          (let [[tx ty] (aget transform :pos)]

            (when draw-bar?
              (js.canvas/fill-rect ctx {:x tx
                                        :y ty
                                        :w (* @progress* progress-w)
                                        :h progress-h}))


            (when pointer-img
              (let [[iw ih] (image/img-w-h pointer-img)]
                (js.canvas/draw-image ctx
                                      pointer-img
                                      (+ tx (* @progress* progress-w) (- (/ iw 2)))
                                      ty)))
            ))))
