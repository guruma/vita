(ns vita.scene.common.behaviour.image-animator
  (:require-macros [milk.macro :refer [def-behaviour letc]])
  (:require [js.canvas :as js.canvas]
            [milk.gatom :as gatom]
            [milk.event :as event]))


(def-behaviour image-animator [ani-info]

  [{:keys [target from to duration end-callback]} ani-info
   rate (/ (- to from) duration)
   acc-duration* (atom 0)]

  :on-awake
  (fn [ga]
    (aset target :idx from))

  :update
  (fn [ga dt]
    (swap! acc-duration* + dt)

    (aset target :idx (int (* @acc-duration* rate)))

    (if (>= @acc-duration* duration)
      (event/destroy-component! ga self)))

  :on-destroy
  (fn [ga]
    (when end-callback
      (end-callback ga))))
