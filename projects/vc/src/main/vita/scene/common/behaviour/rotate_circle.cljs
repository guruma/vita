(ns vita.scene.common.behaviour.rotate-circle
  (:require-macros [milk.macro :refer [def-behaviour letc]])
  (:require [js.canvas :as js.canvas]
            [milk.gatom :as gatom]
            [vita.resource.image :as image]))


(def-behaviour rotate-circle []
  [r 20
   deg-spd 0.1
   acc-duration* (atom 0)
   two-pi (* (.-PI js/Math) 2)
   delta-deg 0.2]

  :update
  (fn [ga dt]
    (swap! acc-duration* #(+ deg-spd %)))

  :render
  (fn [ga ctx]
    (letc ga [transform :transform]
          (when-let [[tx ty] (aget transform :pos)]
            (-> ctx
                (js.canvas/save)
                (js.canvas/circle-line {:x tx :y ty :r r
                                        :color :#696969
                                        :width 5
                                        :s-deg @acc-duration*
                                        :e-deg (+ two-pi @acc-duration* delta-deg)})
                (js.canvas/restore))
            ))))
