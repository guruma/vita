(ns vita.scene.common.behaviour.ui-label
  (:require-macros [milk.macro :refer [def-behaviour letc]])
  (:require [philos.cljs.debug :refer-macros [dbg clog break]]
            [milk.gatom :as gatom]
            [js.canvas :as js.canvas]))

(defn draw-line-through
  "text에 가로 취소선을 그린다.
   #ctx {canvas context
   #x {int} text의 x 좌표
   #y {int} text의 y 좌표
   #line-color {str} 취소선의 색깔
   #line-width {int} 취소선의 굵기"
  [ctx [x y] text {:keys [line-color line-width] :or {line-color "#D64300" line-width 1}}]
;  (dbg [line-color line-width] :o :if line-color)
  (let [text-width (.-width (.measureText ctx text))
        y'         (- y 6)]
    (-> ctx
        (js.canvas/save)
        (js.canvas/stroke-style line-color)
        (js.canvas/stroke-width line-width)
        (js.canvas/begin-path)
        (js.canvas/move-to (- x 3) y')
        (js.canvas/line-to (+ x 3 text-width) y')
        (js.canvas/stroke)
        (js.canvas/restore) ))
  ctx)

(def-behaviour ui-label [& {:keys [text order dx dy style align] :or {}}]
  [{:keys [font color line-through]} style
   {:keys [line-color line-width]} line-through
   font (or font "20pt Arial")
   color (or color :#F000F0)
   align (or align :left)]

  :text text
  :order (or order 0)

  :render
  (fn [ga ctx]
    (letc ga [transform :transform]
          (let [[tx ty] (aget transform :pos)
                tx'     (if dx (+ tx dx) tx)
                ty'     (if dy (+ ty dy) ty)
                text    (aget self :text)]
             (-> ctx
                (js.canvas/save)
                (js.canvas/text-align align)
                (js.canvas/font-style font)
                (js.canvas/fill-style color)
                (js.canvas/text {:x tx'
                                 :y ty'
                                 :text text})
                (#(if line-through 
                    (draw-line-through % [tx' ty'] text {:line-color line-color :line-width line-width})
                    %))
                (js.canvas/restore))))))
