(ns vita.scene.common.behaviour.ui-button
  (:require-macros [milk.macro :refer [def-behaviour letc]])
  (:require [milk.gatom :as gatom]
            [js.canvas :as js.canvas]
            [milk.component.box-collider :as component.box-collider]
            [milk.event :as event]
            [sonaclo.util :as util]
            [vita.resource.sound :as snd]
            [vita.resource.image :as image]
            [jayq.core :as jq :refer [$]]
            ))


(defn rect-in [[x y w h] [px py]]
  (and (< x px (+ x w))
       (< y py (+ y h))))


(defn- get-button-state [hover? down?]
  (cond
   down? :down
   hover? :over
   :else :normal))


(def-behaviour ui-button [& {:keys [button-info on-click order enable visible] :or {}}]
  [{:keys [img img-k table]} button-info

   img (or img (image/load img-k))

   ;; button state.
   hover?* (atom false)
   down?* (atom false)]

  :order (or order 0)

  :enabled? (if (nil? enable) true enable)

  :visible? (if (nil? visible) true visible)

  :on-awake
  (fn [ga]
    (letc ga [box-collider :box-collider]
          (when-not box-collider
            (let [[_ _ w h] (:normal table)]
              (event/add-component! ga (component.box-collider/box-collider :w w :h h))))))

  :on-mouse-up
  #(do (if @down?* (reset! down?* false))
       (when (and (aget self :enabled?) @hover?* on-click)
         (snd/play :click-1)
         (on-click self)))

  :on-mouse-enter
  #(do (if-not @hover?* (reset! hover?* true))
       (when (aget self :enabled?)
         (-> ($ :#milk-canvas)
             (jq/css "cursor" "pointer"))))

  :on-mouse-exit
  #(do (if @hover?* (reset! hover?* false))
       (when (aget self :enabled?)
         (-> ($ :#milk-canvas)
             (jq/css "cursor" "auto"))))

  :on-mouse-drag
  (fn [ga down?]
    (when (aget self :enabled?)
      (reset! down?* down?)))

  :enable-on
  (fn []
    (aset self :enabled? true))

  :enable-off
  (fn []
    (aset self :enabled? false))

  :visible-on
  (fn []
    (aset self :visible? true))

  :visible-off
  (fn []
    (aset self :visible? false))

  :render
  (fn [ga ctx]
    (when (aget self :visible?)
      (let [button-state (if (aget self :enabled?)
                           (get-button-state @hover?* @down?*)
                           :disable)]

        (letc ga [transform :transform]
              (let [[tx ty] (aget transform :pos)
                    [sx sy w h] (button-state table)]
                (js.canvas/draw-image ctx img {:sx sx :sy sy :sw w :sh h
                                               :dx tx :dy ty :dw w :dh h})))))))
