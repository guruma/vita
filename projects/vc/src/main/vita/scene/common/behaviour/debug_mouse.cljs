(ns vita.scene.common.behaviour.debug-mouse
  (:require-macros [milk.macro :refer [def-behaviour]])
  (:require [js.canvas :as js.canvas]
            [milk.gatom :as gatom]
            [milk.input.mouse :as input.mouse]))


(def-behaviour debug-mouse []

  [mouse-pressed?* (atom false)
   mouse-pos* (atom [0 0])]

  :order 100

  :update
  (fn [ga dt]
    (let [pressed (input.mouse/get-mouse-button 0)]
      (reset! mouse-pressed?* pressed)
      (when pressed
        (reset! mouse-pos* (input.mouse/mouse-pos)))))

  :render
  (fn [ga ctx]
    (when @mouse-pressed?*
      (let [[mx my] @mouse-pos*]
        (-> ctx
            (js.canvas/save)
            (js.canvas/fill-style :#00FF00)
            (js.canvas/circle {:x mx :y my :r 10})
            (js.canvas/fill)
            (js.canvas/restore))))))
