(ns vita.scene.common.behaviour.ui-animation-progress
  (:require-macros [milk.macro :refer [def-behaviour letc]])
  (:require [milk.gatom :as gatom]
            [js.canvas :as js.canvas]
            [milk.component.box-collider :as component.box-collider]
            [milk.event :as event]
            [sonaclo.util :as util]
            ))


;; (defn get-image-dic [info]
;;   (reduce-kv (fn [acc k v]
;;                (assoc acc k (util.image/load-image v)))
;;              {}
;;              info))



(def-behaviour ui-animation-progress [image-dic]

  [ww 716]

  :progress
  0

  :enabled?
  true

  :render
  (fn [ga ctx]
    (letc ga [transform :transform]
          (let [[tx ty] (aget transform :pos)
                {:keys [start middle end light]} image-dic
                progress-w (* (aget self :progress) ww)]

            (js.canvas/draw-image ctx start tx ty)
            (js.canvas/draw-image ctx middle {:sx 0
                                              :sy 0
                                              :sw 1
                                              :sh 18
                                              :dx (+ tx 9)
                                              :dy ty
                                              :dw (max 0 (+ progress-w (- 27)))
                                              :dh 18})

            (js.canvas/draw-image ctx end
                                  (+ tx 9 (max 0 (+ progress-w (- 27))))
                                  ty)

            ;; 31/ 32
            (js.canvas/draw-image ctx light
                                  (+ tx (max 0 (+ progress-w (- 31))))
                                  (+ ty (- 7)))

            )))
  )
