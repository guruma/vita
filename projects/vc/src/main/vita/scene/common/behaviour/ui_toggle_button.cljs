(ns vita.scene.common.behaviour.ui-toggle-button
  (:require-macros [milk.macro :refer [def-behaviour letc]])
  (:require [milk.gatom :as gatom]
            [js.canvas :as js.canvas]
            [milk.component.box-collider :as component.box-collider]
            [milk.event :as event]
            [vita.resource.image :as image]
            [vita.resource.sound :as snd]
            [jayq.core :as jq :refer [$]]
            ))


(defn rect-in [[x y w h] [px py]]
  (and (< x px (+ x w))
       (< y py (+ y h))))


(defn- get-button-state [hover? down?]
  (cond
   down? :down
   hover? :over
   :else :normal))


(def-behaviour ui-toggle-button [& {:keys [img img-k button-info on-click downed? enable] :or {}}]
  [{:keys [button-wh table]} button-info

   img (or img (image/load img-k))

   ;; button state.
   hover?* (atom false)
   down?* (atom false)
   downed?* (atom false)]

  :enabled? (and enable true)

  :on-awake
  (fn [ga]
    (when downed?
      (reset! downed?* downed?))
    (letc ga [box-collider :box-collider]
          (when-not box-collider
            (let [[_ _ w h] (:normal table)]
              (event/add-component! ga (component.box-collider/box-collider :w w :h h))))))

  :on-mouse-up
  #(when (and (aget self :enabled?) @hover?*)
     (if @downed?*
       (when on-click
         (on-click self false))
       (when on-click
         (on-click self true)))
     (reset! downed?* (not @downed?*)))

  :on-mouse-enter
  #(when (aget self :enabled?)
     (reset! hover?* true)
     (-> ($ :#milk-canvas)
         (jq/css "cursor" "pointer")))

  :on-mouse-exit
  #(when (aget self :enabled?)
     (reset! hover?* false)
     (-> ($ :#milk-canvas)
         (jq/css "cursor" "auto")))

  :on-mouse-drag
  (fn [ga down?]
    (when (aget self :enabled?)
      (reset! down?* down?)))

  :render
  (fn [ga ctx]
    (let [button-state (if (aget self :enabled?)
                         (get-button-state @hover?* (or @downed?* @down?*))
                         :disable)]

      (letc ga [transform :transform]
            (let [[tx ty] (aget transform :pos)
                  [sx sy w h] (button-state table)]
              (js.canvas/draw-image ctx img {:sx sx :sy sy :sw w :sh h
                                             :dx tx :dy ty :dw w :dh h}))))))
