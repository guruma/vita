(ns vita.scene.common.behaviour.backboard
  (:require-macros [milk.macro :refer [def-behaviour letc]])
  (:require [js.canvas :as js.canvas]
            [milk.gatom :as gatom]
            [milk.screen.canvas]))


(def-behaviour backboard [& {:keys [order] :or {}}]
  [[screen-w screen-h] (milk.screen.canvas/get-wh)]

  :order (or order 0)

  :render
  (fn [ga ctx]
    (-> ctx
        (js.canvas/save)
        (js.canvas/alpha 0.8)
        (js.canvas/fill-rect {:x 0 :y 0 :w screen-w :h screen-h})
        (js.canvas/restore))))
