(ns vita.scene.common.behaviour.ui-draw
  (:require-macros [milk.macro :refer [def-behaviour letc]])
  (:require [js.canvas :as js.canvas]
            [milk.gatom :as gatom]))


(def-behaviour ui-draw [& {:keys [x y w h color alpha order] :or {}}]
  []

  :order
  (or order 0)

  :render
  (fn [ga ctx]
    (letc ga [transform :transform]
          (let [[tx ty] (aget transform :pos)]
            (-> ctx
                (js.canvas/save)
                (js.canvas/fill-style color)
                (js.canvas/alpha alpha)
                (js.canvas/fill-rect {:x x :y y :w w :h h})
                (js.canvas/restore)))))
  )
