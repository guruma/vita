(ns vita.scene.common.behaviour.ui-image
  (:require-macros [milk.macro :refer [def-behaviour letc]])
  (:require [js.canvas :as js.canvas]
            [sonaclo.util :as util]
            [vita.resource.image :as image]
            [milk.gatom :as gatom]))


(def-behaviour ui-image [& {:keys [img img-k image path order dx dy scale] :or {}}]

  [img* (or image img (image/load img-k) (util/load-image path))]

  :order
  (or order 0)

  :render
  (fn [ga ctx]
    (letc ga [transform :transform]
          (let [[x y] (aget transform :pos)]

            (if-let [[scale-x scale-y] scale]
              (-> ctx
                  (js.canvas/save)
                  (js.canvas/translate (+ dx x) (+ dy y))
                  (js.canvas/scale scale-x scale-y)
                  (js.canvas/draw-image img* 0 0)
                  (js.canvas/restore))
              (js.canvas/draw-image ctx img* (+ dx x) (+ dy y))))))
  )
