(ns vita.scene.common.behaviour.loading
  (:require-macros [milk.macro :refer [def-behaviour letc]])
  (:require [js.canvas :as js.canvas]
            [milk.gatom :as gatom]
            [milk.screen.canvas]
            [vita.resource.image :as image]))


(defn- item-table [n [w h]]
  (->> (iterate (partial + w) 0)
       (take n)
       (mapv (fn [x] [x 0 w h]))))


(def-behaviour loading [& {:keys [back-color alpha] :or {}}]
  [img (image/load :loading-mini)
   table (item-table 8 [(/ 296 8)  39])
   [screen-w screen-h] (milk.screen.canvas/get-wh)
   idx* (atom 0)
   acc* (atom 0)

   alpha (or alpha 0.8)
   back-color (or back-color :#000000)
   ]

  :order 9999

  :update
  (fn [ga dt]
    (swap! acc* + dt)
    (if (>= @acc* 800)
      (swap! acc* rem 800))

    (reset! idx* (quot @acc* 100))
    )


  :render
  (fn [ga ctx]
    (let [[sx sy w h] (nth table @idx*)
          dw (* w 2)
          dh (* h 2)]
      (-> ctx
          (js.canvas/save)
          (js.canvas/alpha alpha)
          (js.canvas/fill-style back-color)
          (js.canvas/fill-rect {:x 0 :y 0 :w screen-w :h screen-h})
          (js.canvas/restore)
          (js.canvas/draw-image img {:sx sx :sy sy :sw w :sh h
                                     :dx (- (/ screen-w 2)
                                            (/ dw 2))
                                     :dy (- (/ screen-h 2)
                                            (/ dh 2))
                                     :dw dw :dh dh})))))
