(ns vita.scene.common.behaviour.ui-multi-image
  (:require-macros [milk.macro :refer [def-behaviour letc]])
  (:require [js.canvas :as js.canvas]
            [milk.gatom :as gatom]
            [vita.resource.image :as image]))


(def-behaviour ui-multi-image [& {:keys [info n] :or {}}]
  [{:keys [img-k src table]} info
   img (image/load img-k)
   idx* (or n (atom 0))]

  :set-n
  (fn [n] (reset! idx* n))

  :render
  (fn [ga ctx]
    (when (.-complete img)
      (letc ga [transform :transform]
            (let [[tx ty] (aget transform :pos)
                  [sx sy w h] (nth table @idx*)]
              (js.canvas/draw-image ctx img {:sx sx :sy sy :sw w :sh h
                                             :dx tx :dy ty :dw w :dh h}))))))
