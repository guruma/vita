(ns vita.scene.common.behaviour.key-cheat
  (:require-macros [milk.macro :refer [def-behaviour letc]])
  (:require [milk.gatom :as gatom]
            [milk.input.keyboard :as input.keyboard]))


(def-behaviour key-cheat [& key-callbacks]

  [cbs* (apply hash-map key-callbacks)]

  :order -1

  :update
  (fn [ga dt]
    (doseq [keycode (input.keyboard/get-keyups)]
      (if-let [callback (get cbs* keycode)]
        (callback)))))
