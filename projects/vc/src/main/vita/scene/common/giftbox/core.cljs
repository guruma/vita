(ns vita.scene.common.giftbox.core
  (:require-macros [vita.logger :as vcl])
  (:require [milk.event :as event :refer [new-gatom]]
            [milk.component.box-collider :as component.box-collider]

            [vita.scene.common.behaviour.ui-image :refer [ui-image]]
            [vita.scene.common.behaviour.ui-label :refer [ui-label]]
            [vita.scene.common.behaviour.ui-button :refer [ui-button]]
            [vita.scene.common.behaviour.ui-toggle-button :refer [ui-toggle-button]]
            [vita.resource.image :as image]

            [sonaclo.util :as util]

            [vita.scene.main-lobby.network :as network]
            [vita.shared.const.main-lobby :as PROTOCOL]

            [vita.scene.common.giftbox.accept :as accept]
            [vita.scene.common.giftbox.send :as send]

            [vita.helper]
            [vita.app :as app]
            [vita.scene.common.behaviour.loading :refer [loading]]

            [vita.my :as my]
            [sonaclo.shared.util :as shared.util]

            [philos.cljs.debug :refer-macros [dbg clog break]]
            [vita.scene.common.giftbox.count :as count]
            ))

(def common-ui* (atom []))
(defn- add-common-ui [ga]
  (swap! common-ui* conj ga))
(defn- clear-common-ui []
  (doseq [ga @common-ui*]
    (event/del-gatom ga))
  (reset! common-ui* []))

(def gift-info-data* (atom {}))

(defn- clear-all-ui []
  (clear-common-ui)
  (accept/clear-accept-ui)
  (send/clear-send-ui))

(declare set-all-ui)
(defn accept-callback [idx]
  (clear-all-ui)
  (let [gift (:gift (nth (:gifts (:acceptable @gift-info-data*)) idx))
        freespin (second (:value gift))]
    (my/add-freespin-count freespin)
    (swap! gift-info-data* #(update-in % [:acceptable :gifts] (fn [gift-vec]
                                                                (shared.util/remove-vector gift-vec idx))))
    (count/set_gift_count (dec (count/get_gift_count))) ;gift-count는 login할때만 업데이트 하므로, 메인로비에서 count 숫자 바꾸려면 이렇게 해야 한다.
    )
  (set-all-ui :accept))

(defn send-callback []
  (clear-all-ui)
  (let [gift (first (:gifts (:sendable @gift-info-data*)))
        freespin (second (:reward gift))]
    (my/add-freespin-count freespin)
    (swap! gift-info-data* #(assoc-in % [:sendable :gifts] nil)))
  (set-all-ui :send))

(defn q_gift_info []
  (network/send PROTOCOL/Q_GIFT_INFO {:sid (vita.my/get-social-id)})
  (app/dialog:show {:make-fn (fn [] [(new-gatom "" [(loading)] :pos [400 400])])}))

(defn r_gift_info [data]
  ;data -> {:sendable {:gifts ({:description "You and friends will get Free Spins +10 times!", :_id "7d08112879e389ea788a4a56cc851b48", :_rev "1-3eeb32ba3b28cdb770e30742d997903c", :facebook-msg "Free Spins 10 times gift to you!", :end-date [2014 10 31], :value ["freespin" 10], :reward ["freespin" 10], :title "Free Spins Gift", :sub-title "Free Spins +10 times", :gift-id "gift-7cba6afb-10c5-489d-aef5-e1b3d26c5b38", :period "daily", :start-date [2014 8 20]})}, :acceptable {:gifts ({:_id "2c23e0327d09b54eda6717fd27b6cfd4", :_rev "1-f13d6f62192a57989c3795b20f20a039", :gift {:description "You and friends will get Free Spins +10 times!", :_id "7d08112879e389ea788a4a56cc851b48", :_rev "1-3eeb32ba3b28cdb770e30742d997903c", :facebook-msg "Free Spins 10 times gift to you!", :end-date [2014 10 31], :value ["freespin" 10], :reward ["freespin" 10], :title "Free Spins Gift", :sub-title "Free Spins +10 times", :gift-id "gift-7cba6afb-10c5-489d-aef5-e1b3d26c5b38", :period "daily", :start-date [2014 8 20]}, :social-id "261363797394639", :sender "Manmyung Kim"})}}}

  (reset! gift-info-data* data)
  (set-all-ui :accept))

(defn set-all-ui [child_view]
  (app/dialog:hide)

  ;; 전체 화면 입력 블락
  (add-common-ui
    (let [[screen-w screen-h] (milk.screen.canvas/get-wh)]
      (event/new-gatom "box"
                       [(component.box-collider/box-collider :w screen-w
                                                             :h screen-h
                                                             :z 99)]
                       :pos [0 0])))

  (add-common-ui
    (new-gatom "giftbox-back"
               [(ui-image :img (image/load :giftbox-back))]
               :pos [256 100]))

  ;;우측 상단의 x 버튼
  (add-common-ui
    (let [btn-info (util/make-btn :giftbox-x-btn 34 34 200)
          {:keys [w h]} (get btn-info :box)]
      (new-gatom "giftbox-x-btn"
                 [(ui-button :button-info btn-info
                             :on-click #(do
                                         (clear-all-ui)
                                         (reset! gift-info-data* {}))
                             :enable true)
                  (component.box-collider/box-collider :w w :h h :z 100)]
                 :pos [951 137])))

  ;;change to accecpt
  (add-common-ui
    (let [btn-info (util/make-btn nil 189 44 200)
          {:keys [w h]} (get btn-info :box)]
      (new-gatom "change-to-accept"
                 [(ui-button :button-info btn-info
                             :on-click #(do
                                         (accept/clear-accept-ui)
                                         (send/clear-send-ui)
                                         (accept/set-accept-ui (:acceptable @gift-info-data*) accept-callback)))
                  (component.box-collider/box-collider :w w :h h :z 100)]
                 :pos [564 135] )))

  ;;change to send
  (add-common-ui
    (let [btn-info (util/make-btn nil 189 44 200)
          {:keys [w h]} (get btn-info :box)]
      (new-gatom "change-to-send"
                 [(ui-button :button-info btn-info
                             :on-click #(do
                                         (accept/clear-accept-ui)
                                         (send/clear-send-ui)
                                         (send/set-send-ui (:sendable @gift-info-data*) send-callback)))
                  (component.box-collider/box-collider :w w :h h :z 100)]
                 :pos [(+ 564 189) 135] )))

  (let [freespin-count (my/get-freespin-count)]
    (when (> freespin-count 0)
      (add-common-ui
        (new-gatom (str "label3")
                   [(ui-label :text (str "+ FREE SPIN : " freespin-count)
                              :style {:color :#FFFFFF :font "24px Arial"}
                              :dx 0
                              :dy 0)]
                   :pos [455 673]))

      (add-common-ui
        (let [btn-info (util/make-btn :giftbox-go 63 48 200)
              {:keys [w h]} (get btn-info :box)]
          (new-gatom "giftbox-go"
                     [(ui-button :button-info btn-info
                                 :on-click #(vita.helper/load-scene! :slotmachine)
                                 :enable true)
                      (component.box-collider/box-collider :w w :h h :z 100)]
                     :pos [776 640] )))))

  (when (= :accept child_view)
    (accept/set-accept-ui (:acceptable @gift-info-data*) accept-callback))

  (when (= :send child_view)
    (send/set-send-ui (:sendable @gift-info-data*) send-callback)))