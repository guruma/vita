(ns vita.scene.common.giftbox.accept
  (:require-macros [vita.logger :as vcl])
  (:require [milk.event :as event :refer [new-gatom]]
            [milk.component.box-collider :as component.box-collider]

            [vita.scene.common.behaviour.ui-image :refer [ui-image]]
            [vita.scene.common.behaviour.ui-label :refer [ui-label]]
            [vita.scene.common.behaviour.ui-button :refer [ui-button]]
            [vita.scene.common.behaviour.ui-toggle-button :refer [ui-toggle-button]]
            [vita.resource.image :as image]

            [sonaclo.util :as util]

            [vita.scene.main-lobby.network :as network]
            [vita.shared.const.main-lobby :as PROTOCOL]

            [vita.scene.common.giftbox.send :as send]
            ))

(def accept-ui* (atom []))
(defn- add-accept-ui [ga]
  (swap! accept-ui* conj ga))
(defn clear-accept-ui []
  (doseq [ga @accept-ui*]
    (event/del-gatom ga))
  (reset! accept-ui* []))

(defn set-accept-ui [acceptable accept-callback]
  ;acceptable -> {:gifts ({:_id "2c23e0327d09b54eda6717fd27b6cfd4", :_rev "1-f13d6f62192a57989c3795b20f20a039", :gift {:description "You and friends will get Free Spins +10 times!", :_id "7d08112879e389ea788a4a56cc851b48", :_rev "1-3eeb32ba3b28cdb770e30742d997903c", :facebook-msg "Free Spins 10 times gift to you!", :end-date [2014 10 31], :value ["freespin" 10], :reward ["freespin" 10], :title "Free Spins Gift", :sub-title "Free Spins +10 times", :gift-id "gift-7cba6afb-10c5-489d-aef5-e1b3d26c5b38", :period "daily", :start-date [2014 8 20]}, :social-id "261363797394639", :sender "Manmyung Kim"})}
  (add-accept-ui
    (new-gatom "giftbox-tab01"
               [(ui-image :img (image/load :giftbox-tab01))]
               :pos [564 135]))

  (if (empty? (:gifts acceptable))
    (add-accept-ui
       (new-gatom (str "label-empty")
                  [(ui-label
                     :align :center
                     :text "Empty Gifts!"
                     :style {:color :#696969 :font "30px Arial"})]
                  :pos [640 400]))
    (let [gifts (:gifts acceptable)]
      (doseq [idx (range (count gifts))]
        (let [gift (nth gifts idx)
              dy (* 75 idx)]
          (add-accept-ui
            (new-gatom "giftbox-mback"
                       [(ui-image :img (image/load :giftbox-mback))]
                       :pos [297 (+ dy 181)]))

          (add-accept-ui
            (new-gatom "giftbox-freespin"
                       [(ui-image :img (image/load :giftbox-freespin))]
                       :pos [300 (+ dy 184)]))

          (add-accept-ui
            (new-gatom (str "label1")
                       [(ui-label :text (:sender gift)
                                  :style {:color :#000000 :font "16px Arial"})]
                       :pos [375 (+ dy 208)]))

          (add-accept-ui
            (new-gatom (str "label2")
                       [(ui-label :text (:description (:gift gift))
                                  :style {:color :#000000 :font "18px Arial"})]
                       :pos [375 (+ dy 240)]))

          (add-accept-ui
            (let [btn-info (util/make-btn :giftbox-accept 117 52 200)
                  {:keys [w h]} (get btn-info :box)]
              (new-gatom "giftbox-accept"
                         [(ui-button :button-info btn-info
                                     :on-click #(do
                                                 (network/send PROTOCOL/Q_GIFT_ACCEPT {:sid (vita.my/get-social-id) :gift (:gift gift)})
                                                 (accept-callback idx))
                                     :enable true)
                          (component.box-collider/box-collider :w w :h h :z 100)]
                         :pos [859 (+ dy 193)] ))))))))