(ns vita.scene.common.giftbox.count
  (:require-macros [vita.logger :as vcl])
  (:require [milk.event :as event :refer [new-gatom]]

            [vita.scene.common.behaviour.ui-image :refer [ui-image]]
            [vita.scene.common.behaviour.ui-label :refer [ui-label]]

            [vita.scene.main-lobby.network :as network]
            [vita.shared.const.main-lobby :as PROTOCOL]
            ))

(def gift-count* (atom 0))
(def gift-count-ga* (atom nil))
(defn- set-gift-count-ga [ga]
  (reset! gift-count-ga* ga))
(defn- clear-gift-count-ga []
  (when-let [ga @gift-count-ga*]
    (event/del-gatom ga))
  (reset! gift-count-ga* nil))

(defn q_gift_count []
  (network/send PROTOCOL/Q_GIFT_COUNT {:sid (vita.my/get-social-id)}))

(defn view_gift_count []
  (when (> @gift-count* 0)
    (set-gift-count-ga
      (new-gatom "gift_accept_num"
                 [(ui-image :img-k :gift-count-01)
                  (ui-image :img-k :gift-count-02 :scale [11 1] :dx 8)
                  (ui-image :img-k :gift-count-03 :dx (+ 8 11))
                  (ui-label :text (str @gift-count*)
                            :style {:color :#FFFFFF :font "12px Arial"}
                            :dx 10 :dy 13)]
                 :pos [275 755]))))

(defn set_gift_count [count]
  (reset! gift-count* count)
  (clear-gift-count-ga)
  (view_gift_count))

(defn get_gift_count []
  @gift-count*)