(ns vita.scene.common.giftbox.send
  (:require-macros [vita.logger :as vcl])
  (:require [milk.event :as event :refer [new-gatom]]
            [milk.component.box-collider :as component.box-collider]

            [vita.scene.common.behaviour.ui-image :refer [ui-image]]
            [vita.scene.common.behaviour.ui-label :refer [ui-label]]
            [vita.scene.common.behaviour.ui-button :refer [ui-button]]
            [vita.scene.common.behaviour.ui-toggle-button :refer [ui-toggle-button]]
            [vita.resource.image :as image]

            [sonaclo.util :as util]

            [vita.app :as app]
            [vita.scene.main-lobby.network :as network]
            [vita.shared.const.main-lobby :as PROTOCOL]

            [vita.scene.common.rank-bar.const :as CONST]

            [sdk.facebook.core :as fb]
            ))

(def send-common-ui* (atom []))
(defn- add-send-common-ui [ga]
  (swap! send-common-ui* conj ga))
(defn- clear-send-common-ui []
  (doseq [ga @send-common-ui*]
    (event/del-gatom ga))
  (reset! send-common-ui* []))

(def send-friendlist-ui* (atom []))
(defn- add-send-friendlist-ui [ga]
  (swap! send-friendlist-ui* conj ga))
(defn- clear-send-friendlist-ui []
  (doseq [ga @send-friendlist-ui*]
    (event/del-gatom ga))
  (reset! send-friendlist-ui* []))

(def downed-idx* (atom #{}))
(def friendlist-page* (atom 0))

(def page-per-num 5)
(defn- max-friendlist-page* []
  (int (/ (count (vita.my/get-friend-list)) page-per-num)))
(defn- min-friendlist-page* []
  0)

(defn clear-send-ui []
  (clear-send-common-ui)
  (clear-send-friendlist-ui)
  (reset! downed-idx* #{})
  (reset! friendlist-page* 0))

(defn- send-gift [gift send-callback]
  (let [friend-list (vita.my/get-friend-list)
        selected-friend-list (keep-indexed #(if (contains? @downed-idx* %1) %2) friend-list)]
    (when-not (empty? selected-friend-list)
      (let [new-invitee (map #(% "id") selected-friend-list)]
          (fb/friends-request new-invitee
                             (:facebook-msg gift)
                             #(do
                               (vcl/DEBUG "send fb request: " %)
                               (network/send PROTOCOL/Q_GIFT_SEND {:sid (vita.my/get-social-id) :new-invitees % :gift gift})
                               (send-callback)))))))

(defn- set-send-friendlist-ui [enable]
  (clear-send-friendlist-ui)
  (let [friend-list (vita.my/get-friend-list)]
    (when-not (empty? friend-list)
      (let [friend-list-cur-page (nth (partition-all page-per-num friend-list) @friendlist-page*)]
        (doseq [idx-in-page (range (count friend-list-cur-page))] ;friend-list-cur-page 의 수가 5보다 작은 경우도 있기 때문에 range 5는 문제가 생김
          (let [btn-info (util/make-btn :giftbox-send-friendsback 260 75 200)
                {:keys [w h]} (get btn-info :box)
                friend (nth friend-list-cur-page idx-in-page)
                friend-idx (+ idx-in-page (* page-per-num @friendlist-page*))]
            (add-send-friendlist-ui
              (new-gatom (str "giftbox-send-friendsback" friend-idx)
                         [(ui-toggle-button :img (image/load (:img-k btn-info))
                                            :button-info btn-info
                                            :on-click (fn [ga down?]
                                                        (if down?
                                                          (swap! downed-idx* conj friend-idx)
                                                          (swap! downed-idx* disj friend-idx)
                                                          ))
                                            :downed? (contains? @downed-idx* friend-idx)
                                            :enable enable)
                          (component.box-collider/box-collider :w w :h h :z 100)]
                         :pos [297
                               (+ 215 (* 75 idx-in-page))
                               ]))
            (add-send-friendlist-ui
              (new-gatom (str "label" friend-idx)
                         [(ui-image :img (util/load-image
                                           (((friend "picture") "data") "url")
                                           ) :dx 0 :dy 0)
                          (ui-label :text (friend "name")
                                    :style {:color :#000000 :font "14px Arial"}
                                    :dx 65
                                    :dy 10)
                          #_(ui-label :text (if (friend "is_invitable")
                                            "Invite!"
                                            "Send!")
                                    :style {:color :#3232FF :font "16px Arial"}
                                    :dx 65
                                    :dy 50)]
                         :pos [309
                               (+ 226 (* 75 idx-in-page))
                               ]))))))))

(defn set-send-ui [sendable send-callback]
  ;sendable -> {:gifts ({:description "You and friends will get Free Spins +10 times!", :_id "7d08112879e389ea788a4a56cc851b48", :_rev "1-3eeb32ba3b28cdb770e30742d997903c", :facebook-msg "Free Spins 10 times gift to you!", :end-date [2014 10 31], :value ["freespin" 10], :reward ["freespin" 10], :title "Free Spins Gift", :sub-title "Free Spins +10 times", :gift-id "gift-7cba6afb-10c5-489d-aef5-e1b3d26c5b38", :period "daily", :start-date [2014 8 20]})}
  (let [enable (not= nil (seq (:gifts sendable)))]
    (add-send-common-ui
      (new-gatom "giftbox-tab02"
                 [(ui-image :img (image/load :giftbox-tab02))]
                 :pos [564 135]))

    (if enable
      (do
        (add-send-common-ui
          (new-gatom "giftbox-sendable-description"
                     [(ui-label :text (:title (first (:gifts sendable)))
                                :align :center
                                :style {:color :#FFFFFF :font "30px Arial"}
                                :dx 0
                                :dy 0)
                      (ui-label :text (:description (first (:gifts sendable)))
                                :align :center
                                :style {:color :#FFFFFF :font "19px Arial"}
                                :dx 0
                                :dy 50)]
                     :pos [770 300]))

        (add-send-common-ui
          (let [btn-info (util/make-btn :giftbox-send-button 240 100 200)
                {:keys [w h]} (get btn-info :box)]
            (new-gatom "giftbox-send-button"
                       [(ui-button :button-info btn-info
                                   :on-click #(send-gift (first (:gifts sendable)) send-callback)
                                   :enable true)
                        (component.box-collider/box-collider :w w :h h :z 100)]
                       :pos [652 483] ))))
      (add-send-common-ui
        (new-gatom "giftbox-once-a-day"
                   [(ui-label :text "Sending is only once a day!"
                              :align :center
                              :style {:color :#696969 :font "28px Arial"})]
                   :pos [770 400])))

    (add-send-common-ui
      (new-gatom "giftbox-send-listback"
                 [(ui-image :img (image/load :giftbox-send-listback))]
                 :pos [296 180]))

    (add-send-common-ui
      (new-gatom "giftbox-send-allfriends"
                 [(ui-image :img (image/load :giftbox-send-allfriends))]
                 :pos [296 180]))

    (add-send-common-ui
      (let [[_ _ w h] (:normal (:table CONST/BTN_LEFT_PAGE_INFO))]
        (event/new-gatom "left-page-btn"
                         [(ui-button :button-info CONST/BTN_LEFT_PAGE_INFO
                                     :on-click
                                     #(when (> @friendlist-page* (min-friendlist-page*))
                                       (swap! friendlist-page* dec)
                                       (set-send-friendlist-ui enable)))
                          (component.box-collider/box-collider :w w :h h :z 100)]
                         :pos [500 178])))

    (add-send-common-ui
      (let [[_ _ w h] (:normal (:table CONST/BTN_RIGHT_PAGE_INFO))]
        (event/new-gatom "right-page-btn"
                         [(ui-button :button-info CONST/BTN_RIGHT_PAGE_INFO
                                     :on-click
                                     #(when (< @friendlist-page* (max-friendlist-page*))
                                       (swap! friendlist-page* inc)
                                       (set-send-friendlist-ui enable)))
                          (component.box-collider/box-collider :w w :h h :z 100)]
                         :pos [530 178])))

    (set-send-friendlist-ui enable)))