(ns vita.scene.common.dialog.util
  (:require-macros [vita.config.macros :as vcm]))


(defn page-reload []
  (vcm/LEVEL
   :live (aset js/window.top "location" "https://apps.facebook.com/wpluscasino/")
   :qa (aset js/window.top "location" "https://apps.facebook.com/qa-vita/")
   :internal (.reload js/window.location)))
