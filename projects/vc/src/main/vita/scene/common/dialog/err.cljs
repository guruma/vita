(ns vita.scene.common.dialog.err
  (:require [milk.event :as event :refer [new-gatom]]
            [milk.component.box-collider :as component.box-collider]

            [vita.scene.common.behaviour.ui-image :refer [ui-image]]
            [vita.resource.image :as image]
            [vita.app :as app]
            [vita.const.dialog :as CONST_DIALOG]
            [vita.shared.const.err-code :as ERR]

            [vita.poker.behaviour.ui-text :refer [ui-text]]
            [vita.scene.common.behaviour.ui-button :refer [ui-button]]

            [vita.const.locale :as locale]
            ))


;;------------------
;; err handler util

(def err-callbacks*
  {ERR/DELETED_ROOM (fn [])
   ERR/NOT_ENOUGH_ACCOUNT_MONEY (fn [])
   ERR/ALREADY_INVITE (fn [])
   ERR/ALREADY_DEL_INVITEE (fn [])
   })


(defn- get-err-callback
  "각 에러코드에 맞는 콜백함수를 찾는다"
  [err-code default-callback]
  (get err-callbacks* err-code default-callback))


;;-----------------
;; dialog

(defn dialog [err callback]

  [
   (new-gatom "err popup"

              [(ui-image :img (image/load :dialog-back))

               (ui-text :text (locale/get-text err)
                        :offset CONST_DIALOG/ERR_DIALOG_TEXT_OFFSET_1
                        :style CONST_DIALOG/ERR_TEXT_STYLE
                        )

               (ui-text :text (name err)
                        :offset CONST_DIALOG/ERR_DIALOG_TEXT_OFFSET_2
                        :style CONST_DIALOG/ERR_TEXT_STYLE2
                        )]

              :pos CONST_DIALOG/ERR_DIALOG_POS)


   (let [{:keys [w h]} CONST_DIALOG/BTN_OK_INFO]
     (new-gatom "btn-ok"
                [(ui-button :button-info CONST_DIALOG/BTN_OK_INFO
                            :on-click #(do (app/dialog:hide)
                                           (if callback
                                             (callback))))

                 (component.box-collider/box-collider :w w
                                                      :h h)]

                :pos CONST_DIALOG/ERR_DIALOG_OK_BTN_POS))

   ])
