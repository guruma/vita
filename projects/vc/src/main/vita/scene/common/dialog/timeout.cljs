(ns vita.scene.common.dialog.timeout
  (:require [milk.event :as event :refer [new-gatom]]
            [milk.component.box-collider :as component.box-collider]

            [vita.scene.common.behaviour.ui-image :refer [ui-image]]
            [vita.scene.common.dialog.util :as dialog.util]
            [vita.resource.image :as image]
            [vita.const.dialog :as CONST_DIALOG]

            [vita.poker.behaviour.ui-text :refer [ui-text]]
            [vita.scene.common.behaviour.ui-button :refer [ui-button]]
            ))


(defn dialog []

  [
   (new-gatom "timeout-dialog-back"

              [(ui-image :img (image/load :lo-join-back))

               (ui-text :text CONST_DIALOG/TIMEOUT_DIALOG_TEXT_1
                        :offset CONST_DIALOG/TIMEOUT_DIALOG_TEXT_OFFSET_1
                        :style CONST_DIALOG/TIMEOUT_STYLE)

               (ui-text :text CONST_DIALOG/TIMEOUT_DIALOG_TEXT_2
                        :offset CONST_DIALOG/TIMEOUT_DIALOG_TEXT_OFFSET_2
                        :style CONST_DIALOG/TIMEOUT_STYLE)]

              :pos CONST_DIALOG/TIMEOUT_BACK_POS)


   (let [{:keys [w h]} CONST_DIALOG/BTN_OK_INFO]
     (new-gatom "btn-ok"
                [(ui-button :button-info CONST_DIALOG/BTN_OK_INFO
                            :on-click dialog.util/page-reload)

                 (component.box-collider/box-collider :w w :h h)]

                :pos CONST_DIALOG/TIMEOUT_OK_BTN_POS))

   ]
  )
