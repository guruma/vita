(ns vita.scene.common.dialog.blue
  (:require [milk.event :as event :refer [new-gatom]]
            [milk.component.box-collider :as component.box-collider]

            [vita.scene.common.behaviour.ui-image :refer [ui-image]]
            [vita.scene.common.behaviour.ui-label :refer [ui-label]]
            [vita.scene.common.behaviour.ui-button :refer [ui-button]]
            [vita.resource.image :as image]
            [vita.app :as app]
            [vita.my]
            [vita.shared.const.events :as CONST_EVENT]

            [sonaclo.util :as util]
            ))


;; helper

(defn- item-table [n [w h]]
  (->> (iterate (partial + w) 0)
       (take n)
       (mapv (fn [x] [x 0 w h]))))


(defn- button-table [[tw h]]
  (let [w (/ tw 4)
        [normal over down disable] (item-table 4 [w h])]
    {:normal normal :over over :down down :disable disable}))


(def blue-up-h 7)

(defn dialog [& {:keys [m-h texts callback]}]

  (let [make-comp (fn [[text style] i]
                    (ui-label :text text :style style :align (:align style)
                              :dx 207 :dy (+ blue-up-h 30 (* i 40))))

        text-comps (map make-comp (partition 2 texts) (iterate inc 0))]

    [
     (new-gatom "blue-background"
                (concat
                 [(ui-image :img-k :blue-window-up)
                  (ui-image :img-k :blue-window-m :scale [1 m-h] :dy blue-up-h)
                  (ui-image :img-k :blue-window-down :dy (+ blue-up-h m-h))
                  ]
                 text-comps)
                :pos [430 310])

     (let [button-info {:img-k :blue-btn-ok :table (button-table [248 27])}]
       (new-gatom "btn-ok"
                  [(ui-button :button-info button-info
                              :on-click (fn []
                                          (app/dialog:hide)
                                          (callback))
                              )
                   (component.box-collider/box-collider :w 62 :h 27)]
                  :pos [607 460]))
     ]
    ))
