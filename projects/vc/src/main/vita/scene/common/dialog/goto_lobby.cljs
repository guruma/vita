(ns vita.scene.common.dialog.goto-lobby
  (:require [milk.event :as event :refer [new-gatom]]
            [milk.component.box-collider :as component.box-collider]

            [vita.scene.common.behaviour.ui-image :refer [ui-image]]
            [vita.scene.common.behaviour.ui-label :refer [ui-label]]
            [vita.scene.common.behaviour.ui-event :refer [ui-event]]
            [vita.scene.common.behaviour.backboard :refer [backboard]]
            [vita.app :as app]

            ))


(def order 150)


(defn dialog [text callback]

  [
   (new-gatom "lbl-goto-lobby"
              [(ui-event :on-click #(do (app/dialog:hide)
                                        (callback)))
               (backboard)
               (ui-label :text text :dx 500 :dy 500)
               (component.box-collider/box-collider :w 3000 :h 3000)
               ]
              :pos [0 0])
   ]
  )
