(ns vita.scene.common.dialog.buy-chips
  (:require [philos.cljs.debug :refer-macros [dbg clog break]]
            [milk.event :as event :refer [new-gatom]]
            [milk.component.box-collider :as component.box-collider]

            [ajax.core :refer [POST]]
            [sdk.facebook.core :as fb]

            [vita.my]
            [vita.scene.common.behaviour.ui-image :refer [ui-image]]
            [vita.scene.common.behaviour.ui-label :refer [ui-label]]
            [vita.scene.common.behaviour.ui-event :refer [ui-event]]
            [vita.scene.common.behaviour.backboard :refer [backboard]]
            [vita.scene.common.behaviour.ui-button :refer [ui-button]]
            [vita.scene.common.dialog.freecoin]


            [vita.resource.image :as image]
            [vita.const.dialog :as CONST_DIALOG]
            [vita.app :as app]
            [vita.net.core :as net]
            [sonaclo.util :as util]
            ))



(defn- receive-catalog [res code]
  (.log js/console (str res)))


(defn- get-catalog-from-server []
  (POST "/products/catalog"
        {:params {}
         :handler #(receive-catalog % :success)
         :error-handler #(receive-catalog % :fail)}))



(defn send-selected-product [product-id-selected]
  (.log js/console "==== send-selected-product ====")
  (let [token (vita.my/get-token)]
    (.log js/console "==== send-selected-product 1 ====")
  (net/send {:cmd :Qcatalog :data {:product-id-selected product-id-selected
                                   :token token}})))


(def catalog*
  [{:text {:chips "750,000  Chips"     :original "$3"    :discount "$3"}}
   {:text {:chips "3,600,000  Chips"   :original "$14"   :discount "$9"}}
   {:text {:chips "9,500,000  Chips"   :original "$38"   :discount "$19"}}
   {:text {:chips "27,300,000  Chips"  :original "$110"  :discount "$39"}}
   {:text {:chips "64,900,000  Chips"  :original "$258"  :discount "$59"}}
   {:text {:chips "198,000,000  Chips" :original "$802"  :discount "$99"}}
   {:text {:chips "497,500,000  Chips" :original "$2,000" :discount "$199"}}
   {:text {:chips "897,000,000  Chips" :original "$3,588" :discount "$299"}} ])


(defn buy-chips-row [row pos product]
   [(new-gatom (str "lbl-chips-" row)
                [(ui-label :text (:chips product)
                           :style {:color :#FFA500}
                           :dx 0
                           :dy 40)]
                :pos pos)

     (new-gatom (str "lbl-original-cash-" row)
                [(ui-label :text (:original product)
                           :style {:color :#FFA500 :font "20px Arial"
                                   :line-through {:line-color "rgba(208, 67, 48, 0.9)" :line-width 3}}
                           :dx 350                           ;  "#d64330"
                           :dy 40)]
                :pos pos)

     (new-gatom (str "lbl-discount-cash-" row)
                 [(ui-label :text (:discount product)
                            :style {:color :#FFA500}
                            :dx 500
                            :dy 40)]
                 :pos pos)

      (let [btn-info (util/make-btn :buychips-buy-btn 63 48 200)
            {:keys [w h]} (get btn-info :box)]
        (new-gatom (str "buy-chips-buy-btn-" row)
                   [(ui-button :button-info btn-info
                               :on-click #(do
                                            (app/dialog:hide)
                                            (clog row)
                                            (send-selected-product row))
                                :enable true)
                     (component.box-collider/box-collider :w w :h h)]
                    :pos [920 (+ 223 (* row 60))] ))

      (new-gatom (str "buychips-line-" (inc row))
                 [(ui-image :img (image/load :buychips-line))]
                 :pos [(pos 0)
                       (+ (pos 1) 60)] )])

(defn dialog []
  (concat
    ;; buychips dialog background image
    [(new-gatom "buychips-back"
                [;(backboard)
                 (ui-image :img (image/load :buychips-back))]
                :pos [218 89])

     ;; buychips dialog 우측 상단의 x 버튼
     (let [btn-info (util/make-btn :buychips-x-btn 34 34 200)
           {:keys [w h]} (get btn-info :box)]
       (new-gatom ":buy-chips-x-btn"
                  [(ui-button :button-info btn-info
                              :on-click #(do
                                           (app/dialog:hide)
                                         #_(send-selected-product 0))
                              :enable true)
                   (component.box-collider/box-collider :w w :h h)]
                  :pos [990 127]))

      (new-gatom "buychips-line1"
                 [(ui-image :img (image/load :buychips-line))]
                :pos [290 215])]

     (buy-chips-row 0 [290 215] (get-in catalog* [0 :text]))
     (buy-chips-row 1 [290 275] (get-in catalog* [1 :text]))
     (buy-chips-row 2 [290 335] (get-in catalog* [2 :text]))
     (buy-chips-row 3 [290 395] (get-in catalog* [3 :text]))
     (buy-chips-row 4 [290 455] (get-in catalog* [4 :text]))
     (buy-chips-row 5 [290 515] (get-in catalog* [5 :text]))
     (buy-chips-row 6 [290 575] (get-in catalog* [6 :text]))
     (buy-chips-row 7 [290 635] (get-in catalog* [7 :text])) ))
