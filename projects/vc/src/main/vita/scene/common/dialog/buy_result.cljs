(ns vita.scene.common.dialog.buy_result
  (:require [vita.app :as app]
            [vita.scene.common.dialog.blue :as blue]
            [sonaclo.util :as util]
            ))

(defn show [money]
  (app/dialog:show {:make-fn
                     (fn [] (blue/dialog :m-h 180
                                         :texts [""
                                                 {}
                                                 "Thank you!"
                                                 {:color :#FFFF00 :font "20pt Arial" :align :center}
                                                 (str "Your money is " (util/money-with-comma money))
                                                 {:color :#FFFF00 :font "25pt Arial" :align :center}]
                                         :callback (fn [] )))}))