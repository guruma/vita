(ns vita.scene.common.dialog.buy-in
  (:require [milk.event :as event :refer [new-gatom]]
            [milk.component.box-collider :as component.box-collider]

            [vita.scene.common.behaviour.ui-image :refer [ui-image]]
            [vita.scene.common.behaviour.ui-button :refer [ui-button]]
            [vita.scene.common.behaviour.ui-event :refer [ui-event]]
            [vita.scene.common.behaviour.ui-progress :refer [ui-progress]]
            [vita.scene.common.behaviour.ui-label :refer [ui-label]]
            [vita.app :as app]
            [vita.resource.image :as image]

            [vita.poker.behaviour.ui-checkbox :refer [ui-checkbox]]
            [vita.const.dialog :as CONST_DIALOG]

            [sonaclo.util :as util]
            ))


;; 나중에 입력될 ratio 비율에 따른 값을 내놓는 함수를 만든다.
;; input  => 100 200
;; output => function :: input  => ratio (0 ~ 1.0)
;;                       output => 100 ~ 200 사이의 값
(defn make-ratio-f [low high unit]
  (let [d (- high low)]
    (fn [ratio]
      (+ low
         (* unit (js/Math.floor (/ (* d ratio) unit)))))))


(defn dialog [on-click money bb]
  (let [;; 입장 최소 금액.
        ;; 그보다 낮은 금액은 의자에 앉을 수 없다.
        l-limit (* bb 20)
        seat-enable? (<= l-limit money)

        ;; 입장 최대 금액.
        h-limit (* bb 200)
        h-limit' (if (< h-limit money) h-limit money)

        ;; 진행바의 비율에 따라 금액을 결정해주는 함수
        ratio-f (make-ratio-f l-limit h-limit' 100)

        lbl-mychip (ui-label :text (cond (not seat-enable?)
                                         (str "you need "
                                              (util/money-with-comma h-limit))

                                         (< h-limit money)
                                         (util/money-with-comma h-limit)

                                         :else
                                         (util/money-with-comma money))
                             :style {:color :#FFFF00 :font "22pt Arial"}
                             :align :center
                             :dx 90
                             :dy -10)

        auto-check (atom true)
        [_ _ w h] (get-in CONST_DIALOG/BTN_OK_INFO [:table :normal])
        [_ _ pw ph] (:rect CONST_DIALOG/PRG_BUY_IN)
        progress* (atom 1.0)
        progress (ui-progress {:wh [pw ph]
                               :progress* progress*
                               :pointer-img-k :buyin-a
                               :draw-bar? false
                               :on-progress #(if seat-enable?
                                               (aset lbl-mychip
                                                     :text (util/money-with-comma (ratio-f %))))})]

    [
     ;; 뒷배경
     (let [{:keys [img-k pos w h]} CONST_DIALOG/UI_BUYIN_BACK]
       (new-gatom "bg-buyin-back"
                  [(ui-image :img (image/load img-k))]
                  :pos pos))

     ;; click-to-sit 버튼
     (let [[_ _ w h] (get-in CONST_DIALOG/BTN_CLICK_TO_SIT_INFO [:table :normal])]
       (new-gatom "btn-click-to-sit"
                  [(ui-button :button-info CONST_DIALOG/BTN_CLICK_TO_SIT_INFO
                              :on-click #(if seat-enable?
                                           (let [ratio @progress*]
                                             (on-click (ratio-f ratio) @auto-check)
                                             (app/dialog:hide)))
                              :enable (if seat-enable? true false))
                   (component.box-collider/box-collider :w w :h h)]
                  :pos (:pos CONST_DIALOG/BTN_CLICK_TO_SIT_INFO)))

     ;; x (close) 버튼
     (let [[_ _ w h] (get-in CONST_DIALOG/BTN_X_CLOSE_INFO [:table :normal])]
       (new-gatom "btn-cancel"
                  [(ui-button :button-info CONST_DIALOG/BTN_X_CLOSE_INFO
                              :on-click #(app/dialog:hide))
                   (component.box-collider/box-collider :w w :h h)]
                  :pos (:pos CONST_DIALOG/BTN_X_CLOSE_INFO)))

     ;; auto refill 체크 버튼
     (let [{:keys [img-k chk-img-k table]} CONST_DIALOG/BTN_AUTO_REFILL
           [_ _ w h] (:normal table)]
       (new-gatom "btn-auto-refill"
                  [(ui-checkbox :on-click #()
                                :enabled?* (atom true)
                                :checked?* auto-check
                                :chk-img (image/load chk-img-k)
                                :img (image/load img-k)
                                :table table)
                   (component.box-collider/box-collider :w w :h h)]
                  :pos (:pos CONST_DIALOG/BUY_IN_DIALOG_AUTO_REFILL_BTN)))

     ;; - 버튼
     (let [[_ _ w h] (get-in CONST_DIALOG/BTN_-_INFO [:table :normal])]
       (new-gatom "btn-minus"
                  [(ui-button :button-info CONST_DIALOG/BTN_-_INFO
                              :on-click #(when seat-enable?
                                           (swap! progress* (fn [r] (max 0.0 (- r 0.1))))
                                           (aset lbl-mychip
                                                 :text (util/money-with-comma (ratio-f @progress*)))))
                   (component.box-collider/box-collider :w w :h h)]
                  :pos (:pos CONST_DIALOG/BTN_-_INFO)))

     ;; + 버튼
     (let [[_ _ w h] (get-in CONST_DIALOG/BTN_+_INFO [:table :normal])]
       (new-gatom "btn-plus"
                  [(ui-button :button-info CONST_DIALOG/BTN_+_INFO
                              :on-click #(when seat-enable?
                                           (swap! progress* (fn [r] (min 1.0 (+ r 0.1))))
                                           (aset lbl-mychip
                                                 :text (util/money-with-comma (ratio-f @progress*)))))
                   (component.box-collider/box-collider :w w :h h)]
                  :pos (:pos CONST_DIALOG/BTN_+_INFO)))

     ;; min money
     (new-gatom "buy-in min money"
                [(ui-label :text (util/money-with-comma l-limit)
                           :style {:color :FFFFFF :font "10pt Arial"}
                           :align :left)]
                :pos [414 445])

     ;; max money
     (new-gatom "buy-in max money"
                [(ui-label :text (util/money-with-comma h-limit')
                           :style {:color :FFFFFF :font "10pt Arial"}
                           :align :right)]
                :pos [862 445])

     ;; 입장 머니 조절바
     (let [[x y w h] (:rect CONST_DIALOG/PRG_BUY_IN)]
       (new-gatom "prg-chip"
                  [progress
                   (component.box-collider/box-collider :w w :h h)]
                  :pos [x y]))

     ;; 입장 머니 라벨 텍스트
     (new-gatom "lbl-mychip"
                [lbl-mychip]
                :pos (:pos CONST_DIALOG/LBL_MY_CHIP))
     ]

    ))
