(ns vita.scene.common.dialog.freecoin
  (:require [milk.event :as event :refer [new-gatom]]
            [milk.component.box-collider :as component.box-collider]

            [vita.scene.common.behaviour.ui-image :refer [ui-image]]
            [vita.resource.image :as image]
            [vita.app :as app]
            [vita.my]
            [vita.shared.const.events :as CONST_EVENT]

            [vita.scene.common.behaviour.ui-button :refer [ui-button]]
            [vita.const.dialog :as CONST_DIALOG]
            [sonaclo.util :as util]
            ))



(defn dialog [event-id money callback]

  [
   (new-gatom "freecoin"
              [(ui-image :img (cond
                               (= event-id CONST_EVENT/EVENT_REGISTER)
                               (image/load :freecoin-start)
                               (= event-id CONST_EVENT/EVENT_DAILY)
                               (image/load :freecoin-day)
                               :else
                               (image/load :test)))]
              :pos [256 100])


   (let [{:keys [w h]} CONST_DIALOG/BTN_FREECOIN_OK_INFO]
     (new-gatom "btn-ok"
                [(ui-button :button-info CONST_DIALOG/BTN_FREECOIN_OK_INFO
                            :on-click #(do
                                         ;; (TODO) 머니 획득 ...
                                         ;; 일단은 그냥 돈 업데이트
                                         (vita.my/add-money money)
                                         (app/dialog:hide)
                                         (callback)))
                 (component.box-collider/box-collider :w w :h h)]
                :pos [461 661]))
   ]
  )
