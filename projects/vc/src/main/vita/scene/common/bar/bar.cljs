(ns vita.scene.common.bar.bar
  (:require-macros
   [vita.logger :as vcl])
  (:require
   [jayq.core]

   [milk.event :as event]
   [milk.gatom :as gatom]
   [milk.component.box-collider :refer [box-collider]]

   [vita.my :as my]
   [sonaclo.util :as util]
   [vita.resource.image :as image]
   [vita.scene.common.dialog.buy-chips]
   [vita.app :as app]

   [vita.scene.common.behaviour.fps :refer [fps]]

   [vita.poker.net.sender :as poker.sender]
   [vita.poker.room :as poker.room]
   [vita.poker.manager.manager :as mm]
   [vita.poker.behaviour.ui-image :refer [ui-image]]
   [vita.poker.behaviour.ui-text :refer [ui-text]]
   [vita.poker.behaviour.fps-cheat :refer [fps-cheat]]
   [vita.scene.common.behaviour.key-cheat :refer [key-cheat]]

   [vita.poker.const.const :as const]
   [vita.shared.const.err-code :as ERR]
   [vita.scene.common.dialog.blue]
   [vita.helper]))


;;----------------
;; BAR const

(def BAR_DEFAULT_ORDER 50)

(def now-scene* (atom :main-lobby))

(def BAR_BACK_POS [0 0])

(def BAR_OFFSET_MAP {:player [225 5]
                 :name [280 22]
                 :money [470 47]
                 :btn-bar-buychips [477 0]
                 :bar-logo [547 0]
                 :btn-bar-max [856 7]
                 :btn-bar-musicon [906 7]
                 :btn-bar-musicoff [906 7]
                 :btn-bar-help [956 7]
                 :btn-bar-short [1006 7]
                 })

(def BAR_BTN_INFOS
  {:btn-bar-musicon {:img-k :bar-musicon
                     :table (util/button-table [184 46])}
   :btn-bar-musicoff {:img-k :bar-musicoff
                      :table (util/button-table [184 46])}
   :btn-bar-buychips {:img-k :bar-buychips
                      :table (util/button-table [260 60])}
   :btn-bar-max {:img-k :bar-max
                 :table (util/button-table [184 46])}
   :btn-bar-help {:img-k :bar-help
                  :table (util/button-table [184 46])}
   :btn-bar-short {:img-k :bar-short
                   :table (util/button-table [184 46])}
   :btn-short {:img-k :bar-shortgame
               :table (util/button-table [376 24])}
   })


(defn get-pos-with-offset [k]
  (map + BAR_BACK_POS (BAR_OFFSET_MAP k)))


;;----------------
;; gatoms basket

(def gs* (atom {}))


(defn clear-all []
  (mm/clear-all gs*))


;;-----------------------
;; music on & off 버튼

(def last-sound-off* (atom nil))
(def last-sound-on* (atom nil))
(def music-btn* (atom true))


(declare music-btn-on)

(defn music-btn-off []
  (reset! music-btn* false)

  (if-let [sound-off @last-sound-off*]
    (sound-off))

  ;; on 버튼 지우고, off 버튼 생성
  (mm/g-del gs* :btn-bar-musicon)
  (mm/new-btn gs* :btn-bar-musicoff
              :button-info (BAR_BTN_INFOS :btn-bar-musicoff)
              :pos (get-pos-with-offset :btn-bar-musicoff)
              :on-click music-btn-on
              :enable true
              :order BAR_DEFAULT_ORDER))

(defn music-btn-on []
  (reset! music-btn* true)

  (if-let [sound-on @last-sound-on*]
    (sound-on))

  ;; off 버튼 지우고, on 버튼 생성
  (mm/g-del gs* :btn-bar-musicoff)
  (mm/new-btn gs* :btn-bar-musicon
              :button-info (BAR_BTN_INFOS :btn-bar-musicon)
              :pos (get-pos-with-offset :btn-bar-musicon)
              :on-click music-btn-off
              :enable true
              :order BAR_DEFAULT_ORDER))


;;-------------------------------------------------
;; user info (name, money, avatar-profile-image)


(defn update-name []
  (mm/g-add gs* :name
            :ga (event/new-gatom "bar-name"
                                 [(ui-text :text (str (my/get-name))
                                           :style const/BAR-NAME-STYLE)]
                                 :pos (get-pos-with-offset :name))))

(def money* (atom 0))


(defn update-money []
  (reset! money* (util/with-comma (my/get-money))))


(defn update-avatar []
  (let [avt-url (my/get-avatar-url)]
    (cond
     (empty? avt-url)
     (vcl/WARN "my avatar-url is empty," avt-url)

     :else
     (try
       (mm/g-add gs* :bar-avatar
                 :ga (event/new-gatom "bar-avatar"
                                      [(ui-image
                                        :img (util/load-image avt-url)
                                        :order BAR_DEFAULT_ORDER)]
                                      :pos (get-pos-with-offset :player)))
       (catch js/Error err
         (vcl/ERROR "bar-avatar image load ERROR!! err = %o, avt-url = %s" err avt-url))))))


(defn bar-player-init []
  (mm/g-add gs* :bar-avatar
            :ga (event/new-gatom "bar-avatar"
                                 [(ui-image
                                   :img (image/load :bar-player)
                                   :order BAR_DEFAULT_ORDER)]
                                 :pos (get-pos-with-offset :player)))

  (mm/g-add gs* :money
            :ga (event/new-gatom "money"
                                 [(ui-text :text money*
                                           :style {:color :#FFFF00
                                                   :align :right})]
                                 :pos (get-pos-with-offset :money)))


  ;; 계정 정보 변경시 콜백 등록
  (vita.my/add-callback #(update-name))
  (vita.my/add-callback #(update-money))
  (vita.my/add-callback #(update-avatar))

  (update-name)
  (update-avatar)
  (update-money)
  )



;;---------------------
;; 우 상단 - 바로가기 메뉴

(def short-view* (atom false))


(defn short-view []
  (swap! short-view* not)

  (if-not @short-view*

    (doseq [k [:short-main-lobby :short-slotmachine
               :short-texas :short-omaha :short-omahahl]]
      (mm/g-del gs* k))

    (do
      (mm/new-text-btn gs* :short-main-lobby
                       :button-info (BAR_BTN_INFOS :btn-short)
                       :pos [965 60];;(get-pos-with-offset :btn-short-mainlobby)
                       :on-click #(when (not= :main-lobby @now-scene*)
                                    (short-view)
                                    (if (poker.room/in-room?) (poker.sender/v-send :quit))
                                    (vita.helper/load-scene! :main-lobby))
                       :enable true
                       :text-f (fn [] "main lobby")
                       :text-offset [5 15]
                       )

      (mm/new-text-btn gs* :short-slotmachine
                       :button-info (BAR_BTN_INFOS :btn-short)
                       :pos [965 84];;(get-pos-with-offset :btn-short-mainlobby)
                       :on-click #(when (not= :slotmachine @now-scene*)
                                    (short-view)
                                    (if (poker.room/in-room?) (poker.sender/v-send :quit))
                                    (vita.helper/load-scene! :slotmachine))
                       :enable true
                       :text-f (fn [] "slotmachine")
                       :text-offset [5 15]
                       )

      (mm/new-text-btn gs* :short-texas
                       :button-info (BAR_BTN_INFOS :btn-short)
                       :pos [965 108];;(get-pos-with-offset :btn-short-mainlobby)
                       :on-click #(when (not= :texas @now-scene*)
                                    (short-view)
                                    (if (poker.room/in-room?) (poker.sender/v-send :quit))
                                    (vita.helper/load-scene! :texas))
                       :enable true
                       :text-f (fn [] "texas")
                       :text-offset [5 15]
                       )

      (mm/new-text-btn gs* :short-omaha
                       :button-info (BAR_BTN_INFOS :btn-short)
                       :pos [965 132];;(get-pos-with-offset :btn-short-mainlobby)
                       :on-click #(when (not= :omaha @now-scene*)
                                    (short-view)
                                    (if (poker.room/in-room?) (poker.sender/v-send :quit))
                                    (vita.helper/load-scene! :omaha))
                       :enable true
                       :text-f (fn [] "omaha")
                       :text-offset [5 15]
                       )

      (mm/new-text-btn gs* :short-omahahl
                       :button-info (BAR_BTN_INFOS :btn-short)
                       :pos [965 156];;(get-pos-with-offset :btn-short-mainlobby)
                       :on-click #(when (not= :omahahl @now-scene*)
                                    (short-view)
                                    (if (poker.room/in-room?) (poker.sender/v-send :quit))
                                    (vita.helper/load-scene! :omahahl))
                       :enable true
                       :text-f (fn [] "omaha HL")
                       :text-offset [5 15]
                       )
      )))




(defn enter-full-screen []
  ;; (.log js/console "09999999")
  ;; (let [doc (jayq.core/$ :html)

  ;;       __ (.log js/console doc)
  ;;       _ (.log js/console "==========")


  ;;       _ (.log js/console (get doc "webkitRequestFullScreen"))
  ;;       _ (.log js/console "----------")

  ;;       req (or (get doc "requestFullScreen")
  ;;               (get doc "webkitRequestFullScreen")
  ;;               (get doc "mozRequestFullScreen")
  ;;               (get doc "msRequestFullScreen"))]

  ;;   (.log js/console 999999999)
  ;;   (.log js/console req)
  ;;   (when req
  ;;     (.log js/console "run")
  ;;     (let [a (req (.get doc 0))]
  ;;       (.log js/console "a   " a)))
  )


(defn exit-full-screen []
  )

;;===================================================================
;; 상단 메뉴 초기화

(defn bar-init [& {:keys [scene sound-on sound-off sock-close] :or {}}]

  (reset! now-scene* scene)

  ;; fps test
  (mm/g-add gs* :fps
            :ga (event/new-gatom "FPS"
                                 [(fps)]
                                 :pos [10 50]))

  ;; fps cheat key
  (mm/g-add gs* :fps-cheat
            :ga (event/new-gatom "fps-cheat"
                                 [(fps-cheat)]
                                 :pos [0 0]))

  ;; TODO(jdj) 테스트 코드 삭제
  (mm/g-add gs* :bar-key-cheat
            :ga (event/new-gatom "bar-key-cheat"
                                 [(key-cheat
                                   ;; 다이얼로그 테스트 용도...
                                   "i" #(app/dialog:show {:make-fn (fn [] (vita.scene.common.dialog.err/dialog ERR/ALREADY_INVITE (fn [])))}))]
                                 :pos [0 0]))



  ;;----------------------------
  ;; 상단 관련 가톰들 생성

  (mm/g-add gs* :bar-back
            :ga (event/new-gatom "bar-back"
                                 [(ui-image :img (image/load :bar-back)
                                            :order BAR_DEFAULT_ORDER)]
                                 :pos BAR_BACK_POS))

  (let [logo-imgs {:texas (image/load :logo-texas)
                   :omaha (image/load :logo-omaha)
                   :omahahl (image/load :logo-omahahl)
                   :logo (image/load :logo-default)
                   }
        logo-img (logo-imgs scene (logo-imgs :logo))]

    (mm/g-add gs* :bar-logo
              :ga (event/new-gatom "bar-logo"
                                   [(ui-image :img logo-img :order BAR_DEFAULT_ORDER)]
                                   :pos (get-pos-with-offset :bar-logo))))

  (mm/new-btn gs* :btn-bar-buychips
              :button-info (BAR_BTN_INFOS :btn-bar-buychips)
              :pos (get-pos-with-offset :btn-bar-buychips)
              :on-click #(app/dialog:show {:make-fn vita.scene.common.dialog.buy-chips/dialog})
              :enable true
              :order BAR_DEFAULT_ORDER)

  ;; (mm/new-btn gs* :btn-bar-max
  ;;             :button-info (BAR_BTN_INFOS :btn-bar-max)
  ;;             :pos (get-pos-with-offset :btn-bar-max)
  ;;             :on-click #(enter-full-screen)
  ;;             :enable true
  ;;             :order BAR_DEFAULT_ORDER)

  (mm/new-btn gs* :btn-bar-help
              :button-info (BAR_BTN_INFOS :btn-bar-help)
              :pos (get-pos-with-offset :btn-bar-help)
              :on-click #(app/dialog:show {:make-fn (fn [] (vita.scene.common.dialog.blue/dialog :m-h 180
                                                                                                 :texts [""
                                                                                                         {}
                                                                                                         "Coming soon"
                                                                                                         {:color :#FFFF00 :font "25pt Arial" :align :center}]
                                                                                                 :callback (fn [] )))})
              :enable true
              :order BAR_DEFAULT_ORDER)

  (mm/new-btn gs* :btn-bar-short
              :button-info (BAR_BTN_INFOS :btn-bar-short)
              :pos (get-pos-with-offset :btn-bar-short)
              :on-click #(short-view)
              :enable true
              :order BAR_DEFAULT_ORDER)

  ;; 이전 scene 에서 사운드 끄기
  (if-let [last-sound-off @last-sound-off*]
    (last-sound-off))

  ;; 사운드 함수 변경
  (reset! last-sound-on* sound-on)
  (reset! last-sound-off* sound-off)

  ;; 사운드 버튼 세팅
  (if @music-btn*
    (music-btn-on)
    (music-btn-off))

  ;; 유저 정보 표시
  (bar-player-init)
  )

;VT-400
(defn btn-bar-buychips-off []
    (mm/btn-off gs* :btn-bar-buychips))
(defn btn-bar-buychips-on []
    (mm/btn-on gs* :btn-bar-buychips))
