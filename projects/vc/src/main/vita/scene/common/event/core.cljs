(ns vita.scene.common.event.core
  (:require-macros
   [vita.logger :as vcl]
   [cljs.core.async.macros :as m :refer [go]])
  (:require
   [cljs.core.async :as async :refer [<! >! chan]]

   [milk.event :as event]
   [milk.component.box-collider :as component.box-collider]

   [vita.app :as app]
   [vita.url :as url]
   [vita.net.core :as net]
   [vita.scene.common.dialog.freecoin]
   [vita.shared.const.events :as CONST_EVENT]))



(def -ch (chan 1))

(defn reset-ch []
  (def -ch (chan 1)))

(defn unblock []
  (go
    (>! -ch true))
  )



(defn run [events]

  (reset-ch)

  ;; 전체 화면 입력 블락
  (let [[screen-w screen-h] (milk.screen.canvas/get-wh)
        ga (event/new-gatom "box"
                            [(component.box-collider/box-collider :w screen-w
                                                                  :h screen-h
                                                                  :z 99)]
                            :pos [0 0])]

    ;; events 처리
    (go
      (doseq [{:keys [id uid info]} events]

        (cond
         ;; 이벤트 다이얼로그 띄우기
         (contains? #{CONST_EVENT/EVENT_REGISTER CONST_EVENT/EVENT_DAILY} id)
         (let [{:keys [money]} info]
           (app/dialog:show {:make-fn #(vita.scene.common.dialog.freecoin/dialog id
                                                                                 money
                                                                                 (fn [] (net/send [:main-lobby
                                                                                                   url/APP_GAME]
                                                                                                  {:cmd :Qevent
                                                                                                   :data {:uid uid}})))}))

         (= id CONST_EVENT/EVENT_INVITE_FRIEND)
         ;; TODO 임시 다이얼로그
         (let [{:keys [money invitee]} info
               f #(vita.scene.common.dialog.blue/dialog :m-h 180
                                                        :texts [(str "You are now connected with " invitee "!")
                                                                {:color :#FFFF00 :font "15pt Arial" :align :center}
                                                                "Close this message to redeem"
                                                                {:color :#FFFF00 :font "15pt Arial" :align :center}
                                                                "$1,000,000"
                                                                {:color :#FFFF00 :font "25pt Arial" :align :center}]
                                                        :callback (fn []
                                                                    (.log js/console "AAAAAAAAA")
                                                                    (net/send [:main-lobby
                                                                               url/APP_GAME]
                                                                              {:cmd :Qevent
                                                                               :data {:uid uid}})))]
           (app/dialog:show {:make-fn f}))


         :else
         (vcl/WARN "undefined event id = %s, uid = %s, info = %s" id uid info))

        (<! -ch)
        ))

    ;; 전체 화면 입력 블락 해제
    (event/del-gatom ga)))
