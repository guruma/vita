(ns vita.scene.slotmachine.resource.audio)


(def audio-paths
  {
   :bingo-1 "sound/slotmachine/bingo-1.mp3"
   :bingo-2 "sound/slotmachine/bingo-2.mp3"
   :bingo-3 "sound/slotmachine/bingo-3.mp3"
   :chip-collect "sound/slotmachine/chip-collect.mp3"

   :reel-spin-1 "sound/slotmachine/reel-spin-1.mp3"
   :reel-spin-2 "sound/slotmachine/reel-spin-2.mp3"
   :reel-spin-3 "sound/slotmachine/reel-spin-3.mp3"
   :reel-spin-4 "sound/slotmachine/reel-spin-4.mp3"
   :reel-stop-1 "sound/slotmachine/reel-stop-1.mp3"
   :reel-stop-2 "sound/slotmachine/reel-stop-2.mp3"
   :reel-stop-3 "sound/slotmachine/reel-stop-3.mp3"
   :reel-stop-4 "sound/slotmachine/reel-stop-4.mp3"
   })
