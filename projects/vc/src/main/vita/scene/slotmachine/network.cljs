(ns vita.scene.slotmachine.network
  "클라이언트 (웹)소켓 관리."
  (:require [vita.net.core :as vita.network]
            [vita.scene.slotmachine.const :as CONST]))


(defn connect []
  #_(vita.network/connect CONST/SOCKET))


(defn send [cmd data]
  (->> {:cmd cmd :data data}
       (vita.network/send CONST/SOCKET)))


(defn close []
  (vita.network/close CONST/SOCKET))


(defn dequeue []
  (vita.network/dequeue CONST/SOCKET))
