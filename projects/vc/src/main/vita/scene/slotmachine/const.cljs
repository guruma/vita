(ns vita.scene.slotmachine.const
  "클라이언트 전용 상수."
  (:require
   [vita.url :as URL]
   [vita.resource.image :as image]
   [vita.shared.const.common :as COMMON]))


;;; Images
;; Background.
(def BG_BACKGROUND_INFO
  "배경 이미지"
  {:img-k :slot-main-back :pos [0 0]})


(def BG_JACKPOT_INFO
  "잭팟 이미지"
  {:img-k :jackpot :pos [595 68]})


;; Items
(defn- item-table [n [w h]]
  (->> (iterate (partial + w) 0)
       (take n)
       (mapv (fn [x] [x 0 w h]))))


(def M_REEL_ITEM_INFO_DIC
  "릴 아이템 이미지 정보"

  {:blue7      :icon-7b
   :red7       :icon-7y
   :gold7      :icon-7g
   :bar1       :icon-bar
   :bar2       :icon-dbar
   :bar3       :icon-tbar
   :apple      :icon-apple
   :cherry     :icon-cherry
   :grape      :icon-grape
   :lemon      :icon-lemon
   :watermelon :icon-water})


;; Frame
(def FRAME-RECT [200 254 840 450])      ; 프레임 영역.

(def FRAME-REEL-HEIGHT
  (let [[_ y _ h] FRAME-RECT]
    (+ y h)))

(def FRAME_REEL_ITEM_HEIGHT 140)

(def FRAME-REEL-HEIGHT-WITH-ITEM
  (let [[_ y _ _] FRAME-RECT]
    (- y FRAME_REEL_ITEM_HEIGHT)))


(def FRAME_REEL_POSITIONS
  "프레임의 릴 각각의 시작지점."
  [[231 254]
   [399 254]
   [566 254]
   [734 254]
   [901 254]])


;; Multiplier
(def MULTIPLIER-RECT
  "배수판 영역."
  [555 255 200 55])

(def MULTIPLIER-SPINNING-TIME
  "배수판 회전 시간(millisec)."
  0)

(def REEL-SPINNING-TIME
  "릴 회전 시간(millisec)."
  300)

(def QM-REVERTING-TIME
  "question mark 회전 시간(millisec)."
  2600)


;;; ============
;;; IMAGES.

;; Buttons.
(defn- button-table [[tw h]]
  (let [w (/ tw 4)
        [normal over down disable] (item-table 4 [w h])]
    {:normal normal :over over :down down :disable disable}))

(def UI_BTN_SPIN          {:img-k :spin        :table (button-table [432 110]) :pos [945 829]})
(def UI_BTN_FREESPIN      {:img-k :freespin    :table (button-table [956 116]) :pos [820 826] :visible false})
(def UI_BTN_FREESPIN_STOP {:img-k :freespin-stop :table (button-table [956 116]) :pos [820 826] :visible false})
(def UI_BTN_LINE          {:img-k :lineplus    :table (button-table [400 24]) :pos [641 915]})
(def UI_BTN_BET           {:img-k :slot-bet    :table (button-table [400 24]) :pos [535 915]})
(def UI_BTN_TOLOBBY       {:img-k :slot-lobby  :table (button-table [520 34]) :pos [221 70]})
(def UI_BTN_PAYTABLE      {:img-k :paytable    :table (button-table [520 34]) :pos [930 70]})
(def UI_BTN_AUTO          {:img-k :slot-auto   :table (button-table [432 110]) :pos [830 829]})
(def UI_BTN_MAX_BET       {:img-k :slot-maxbet :table (button-table [336 60]) :pos [731 879]})


;; Bet Line Buttons.
;; Line Nums
(def LINE-NUM-WH [26 26])
(def BET_LINE_NUM_IMG_TABLE (item-table 2 LINE-NUM-WH))
(def BET_LINE_ANIMATE_TIME 500)         ; (millisec.)
(def BET_LINE_NUM_INFO_DIC
  {4  {:img-k :bln4  :table BET_LINE_NUM_IMG_TABLE :pos [196 254]}
   2  {:img-k :bln2  :table BET_LINE_NUM_IMG_TABLE :pos [196 300]}
   8  {:img-k :bln8  :table BET_LINE_NUM_IMG_TABLE :pos [196 348]}
   10 {:img-k :bln10 :table BET_LINE_NUM_IMG_TABLE :pos [196 395]}
   6  {:img-k :bln6  :table BET_LINE_NUM_IMG_TABLE :pos [196 443]}
   1  {:img-k :bln1  :table BET_LINE_NUM_IMG_TABLE :pos [196 491]}
   7  {:img-k :bln7  :table BET_LINE_NUM_IMG_TABLE :pos [196 538]}
   9  {:img-k :bln9  :table BET_LINE_NUM_IMG_TABLE :pos [196 586]}
   3  {:img-k :bln3  :table BET_LINE_NUM_IMG_TABLE :pos [196 633]}
   5  {:img-k :bln5  :table BET_LINE_NUM_IMG_TABLE :pos [196 681]}

   12 {:img-k :bln12 :table BET_LINE_NUM_IMG_TABLE :pos [1059 254]}
   18 {:img-k :bln18 :table BET_LINE_NUM_IMG_TABLE :pos [1059 300]}
   14 {:img-k :bln14 :table BET_LINE_NUM_IMG_TABLE :pos [1059 348]}
   20 {:img-k :bln20 :table BET_LINE_NUM_IMG_TABLE :pos [1059 395]}
   11 {:img-k :bln11 :table BET_LINE_NUM_IMG_TABLE :pos [1059 443]}
   16 {:img-k :bln16 :table BET_LINE_NUM_IMG_TABLE :pos [1059 491]}
   17 {:img-k :bln17 :table BET_LINE_NUM_IMG_TABLE :pos [1059 538]}
   13 {:img-k :bln13 :table BET_LINE_NUM_IMG_TABLE :pos [1059 586]}
   19 {:img-k :bln19 :table BET_LINE_NUM_IMG_TABLE :pos [1059 633]}
   15 {:img-k :bln15 :table BET_LINE_NUM_IMG_TABLE :pos [1059 681]}
   })


;; Bet Lines.
(def BET_LINE_IMG_TABLE (item-table 2 [832 456]))
(def BET_LINE_IMG_POS [220 254])
(def BET_LINE_INFO_DIC
  {1  {:img-k :bl1  :table BET_LINE_IMG_TABLE :pos BET_LINE_IMG_POS}
   2  {:img-k :bl2  :table BET_LINE_IMG_TABLE :pos BET_LINE_IMG_POS}
   3  {:img-k :bl3  :table BET_LINE_IMG_TABLE :pos BET_LINE_IMG_POS}
   4  {:img-k :bl4  :table BET_LINE_IMG_TABLE :pos BET_LINE_IMG_POS}
   5  {:img-k :bl5  :table BET_LINE_IMG_TABLE :pos BET_LINE_IMG_POS}
   6  {:img-k :bl6  :table BET_LINE_IMG_TABLE :pos BET_LINE_IMG_POS}
   7  {:img-k :bl7  :table BET_LINE_IMG_TABLE :pos BET_LINE_IMG_POS}
   8  {:img-k :bl8  :table BET_LINE_IMG_TABLE :pos BET_LINE_IMG_POS}
   9  {:img-k :bl9  :table BET_LINE_IMG_TABLE :pos BET_LINE_IMG_POS}
   10 {:img-k :bl10 :table BET_LINE_IMG_TABLE :pos BET_LINE_IMG_POS}
   11 {:img-k :bl11 :table BET_LINE_IMG_TABLE :pos BET_LINE_IMG_POS}
   12 {:img-k :bl12 :table BET_LINE_IMG_TABLE :pos BET_LINE_IMG_POS}
   13 {:img-k :bl13 :table BET_LINE_IMG_TABLE :pos BET_LINE_IMG_POS}
   14 {:img-k :bl14 :table BET_LINE_IMG_TABLE :pos BET_LINE_IMG_POS}
   15 {:img-k :bl15 :table BET_LINE_IMG_TABLE :pos BET_LINE_IMG_POS}
   16 {:img-k :bl16 :table BET_LINE_IMG_TABLE :pos BET_LINE_IMG_POS}
   17 {:img-k :bl17 :table BET_LINE_IMG_TABLE :pos BET_LINE_IMG_POS}
   18 {:img-k :bl18 :table BET_LINE_IMG_TABLE :pos BET_LINE_IMG_POS}
   19 {:img-k :bl19 :table BET_LINE_IMG_TABLE :pos BET_LINE_IMG_POS}
   20 {:img-k :bl20 :table BET_LINE_IMG_TABLE :pos BET_LINE_IMG_POS}})


;; Nums.
(defn- nums-table [n [tw h]]
  (let [w (/ tw n)]
    (->> (iterate (partial + w) 0)
         (take n)
         (mapv (fn [x] [x 0 w h])))))


(def NUMS_BIG_LARGE_IMG_INFO  {:img-k :big-large    :table (nums-table 15 [900 60])})
(def NUMS_RED_SMALL_IMG_INFO  {:img-k :df-s         :table (nums-table 16 [288 32])})
(def NUMS_RED_LARGE_IMG_INFO  {:img-k :df           :table (nums-table 16 [448 50])})
(def NUMS_BLUE_LARGE_IMG_INFO {:img-k :freespin-num :table (nums-table 10 [420 77])})


(def UI_NUM_LINE_VIEW (assoc NUMS_RED_SMALL_IMG_INFO :pos [685 884]))
(def UI_NUM_BET_VIEW (assoc NUMS_RED_SMALL_IMG_INFO :pos [610 884]))
(def UI_NUM_TOTAL_BET (assoc NUMS_RED_SMALL_IMG_INFO :pos [791 838]))
(def UI_NUM_JACKPOT (assoc NUMS_RED_LARGE_IMG_INFO :pos [600 95] :direct :mid))
(def UI_NUM_CHIP (assoc NUMS_RED_SMALL_IMG_INFO :pos [495 873]))
(def UI_MONEY_PAYOUT (assoc NUMS_BIG_LARGE_IMG_INFO :pos [581 756]))
(def UI_NUM_FREESPIN (assoc NUMS_BLUE_LARGE_IMG_INFO :pos [846 847] :direct :mid :visible false))


;; Payout.
(def UI_PAYOUT_TEXT_INFO
  {:goodluck {:img-k :goodluck :pos [553 803]}
   :youwon   {:img-k :youwon   :pos [575 803]}
   :oops     {:img-k :oops     :pos [600 803]}
   :freespin {:img-k :freespin :pos [553 803]}})

(def PAYOUT_FLASH_INFO {:img-k :flash-horz :table (item-table 2 [365 115])})


(def M_PAYOUT_FLASH_INFO
  {:left {:img-ks [:light-left-01
                   :light-left-02
                   :light-left-03
                   :light-left-04]
          :pos [126 175]}
   :right {:img-ks [:light-right-01
                    :light-right-02
                    :light-right-03
                    :light-right-04]
           :pos [1084 175]}
   })


;; Reel Flash.
(def REEL_FLASH_INFO {:img-k :flash-vert :table (item-table 2 [170 460])})

(def M_REEL_FLASH_INFO
  {:on  :light-w
   :off :light-b})

(def REEL_FLASH_POSES
  [[231 206]
   [399 206]
   [566 206]
   [734 206]
   [901 206]])


;; Multiplier.
(def MULTIPLIER_DIC
  {:0 :mult0
   :1 :mult1
   :2 :mult2
   :3 :mult3
   :4 :mult4
   :5 :mult5
   :6 :mult6
   :7 :mult7
   :8 :mult8
   :9 :mult9
   :x :multx})


;;
(def MAX_PER_LINE_BET_COUNT 5)
(def MAX_BET_LINE_COUNT 20)

;; network
(def SOCKET [COMMON/SLOTMACHINE URL/APP_GAME])


;; paytable
(def UI_BTN_PAY_TABLE_CLOSE {:img-k :pay-close :table (button-table [120 30]) :pos [988 174]})
(def BG_PAY_TABLE {:img-k :pay-back :pos [214 127]})

(def PAY_TABLE_POSES
  {
   :gold7 [255 314]
   :red7 [386 314]
   :blue7 [517 314]
   :bar3 [648 314]
   :bar2 [779 314]
   :bar1 [910 314]
   :lemon [255 507]
   :apple [386 507]
   :cherry [517 507]
   :watermelon [648 507]
   :grape [779 507]
   :symbol [910 507]
   })
