(ns vita.scene.slotmachine.behaviour.reel-flash
  (:require-macros [milk.macro :refer [def-behaviour letc]])
  (:require [milk.gatom :as gatom]
            [js.canvas :as js.canvas]
            [vita.resource.image :as image]
            ))


(def-behaviour reel-flash [info]
  [{:keys [img img-k table]} info
   img (or img (image/load img-k))
   acc-dt* (atom 0)]

  :update
  (fn [ga dt]
    (swap! acc-dt* + dt)
    (when (>= @acc-dt* 500)
      (reset! acc-dt* 0)))

  :render
  (fn [ga ctx]
    (letc ga [transform :transform]
          (let [[tx ty] (aget transform :pos)
                [sx sy w h] (nth table (if (< @acc-dt* 250) 0 1))]
            (js.canvas/draw-image ctx img {:sx sx :sy sy :sw w :sh h
                                           :dx tx :dy ty :dw w :dh h})))))
