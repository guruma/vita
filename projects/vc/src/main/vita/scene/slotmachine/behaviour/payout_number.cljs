(ns vita.scene.slotmachine.behaviour.payout-number
  (:require-macros [milk.macro :refer [def-behaviour letc]])
  (:require [milk.gatom :as gatom]
            [js.canvas :as js.canvas]

            [milk.event :as event]

            [vita.scene.slotmachine.util.image :as util]
            [vita.resource.image :as image]

            [vita.scene.slotmachine.behaviour.payout-number-updater :refer [payout-number-updater]]
            ))


(def-behaviour payout-number [info number]
  [{:keys [img img-k table]} info
   img (or img (image/load img-k))
   ga* (atom nil)]

  :on-awake
  #(reset! ga* %)

  :on-destroy
  #(reset! ga* nil)

  :set-payout
  (fn [n callback]
    (if (or (zero? n)
            (= n (aget self :number)))
      (callback)
      (let [cur-num (aget self :number)]
        (event/add-component! @ga* (payout-number-updater self [cur-num n] callback)))))

  :number
  (or number 0)

  :render
  (fn [ga ctx]
    (letc ga [transform :transform]
          (let [[tx ty] (aget transform :pos)]
            (let [digits (util/->money (aget self :number))
                  digits-count (count digits)]
              (doseq [[n i] digits]
                (let [[sx sy w h] (nth table n)]
                  (js.canvas/draw-image ctx img {:sx sx :sy sy :sw w :sh h
                                                 :dx (+ tx
                                                        (- (* i w))
                                                        (* (/ digits-count 2) w))
                                                 :dy ty
                                                 :dw w
                                                 :dh h}))))))))
