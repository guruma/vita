(ns vita.scene.slotmachine.behaviour.networker
  (:require-macros [milk.macro :refer [def-behaviour letc]]
                   [vita.config.macros :as vcm]
                   [vita.logger :as vcl])

  (:require
   [milk.gatom :as gatom]
   [vita.scene.slotmachine.network :as network]
   [milk.event :as event :refer [new-gatom]]

   ;; - dialog
   [vita.helper]
   [vita.app :as app]
   [vita.scene.slotmachine.ui.ui-manager :as ui-manager]
   [vita.shared.const.err-code :as ERR]
   [vita.scene.common.dialog.err :as dialog.err]
   [vita.scene.common.dialog.util :as dialog.util]

   ;; protocol
   [vita.scene.slotmachine.net.r-join-slotmachine :as protocol.join-slotmachine]
   [vita.scene.slotmachine.net.r-spin-slotmachine :as protocol.spin-slotmachine]
   [vita.scene.slotmachine.net.r-timeout :as protocol.timeout]
   [vita.scene.slotmachine.net.r-freecoin :as protocol.freecoin]
   [vita.scene.slotmachine.net.r-payment :as protocol.payment]
   [vita.scene.slotmachine.net.r-catalog :as protocol.catalog]
   [vita.shared.const.slotmachine :as PROTOCOL]))


(def ^:private protocol-dic*
  (atom {PROTOCOL/R_JOIN_SLOTMACHINE protocol.join-slotmachine/join-slotmachine
         PROTOCOL/R_SPIN_SLOTMACHINE protocol.spin-slotmachine/spin-slotmachine
         PROTOCOL/B_TIMEOUT protocol.timeout/timeout
         PROTOCOL/B_FREECOIN protocol.freecoin/freecoin
         PROTOCOL/R_PAYMENT protocol.payment/Rpayment
         PROTOCOL/R_CATALOG protocol.catalog/Rcatalog}))


(defn- get-protocol [k]
  (get @protocol-dic* k (fn [& args] (vcl/ERROR "undefined cmd= %s in SLOTMACHINE" k))))




(def-behaviour networker []
  []

  :on-awake
  #(->> {:bet-amount 200}
        (network/send PROTOCOL/Q_JOIN_SLOTMACHINE))

  :update
  (fn []
    (when-let [p (network/dequeue)]

      (case (:type p)

        :receive
        (let [packet (:val p)
              {:keys [err cmd data]} packet
              protocol (get-protocol cmd)]

          (if (= err ERR/ERR_NONE)
            (protocol data)
            (do
              ;; TODO(jdj) 리팩...
              ;; 에러코드를 한곳에서 처리하기 위해서 여기서 다이얼로그를 띄우도록 구조가 잡혀있는데,
              ;; Rjoin 을 받기전에 미니로딩으로 가려져있는 상태에서 에러코드가 오면, 미니로딩때문에 에러 다이얼로그 창이 잘 안보인다.
              ;; 그렇다고,
              ;; err 코드가 있는 패킷을 넘겨서 처리 시키면,
              ;; err 코드를 한곳에서 감지해서 처리하지 못하고 흩어진다
              ;; 아마도 미니로딩 구조를 변경해야 할듯.
              (when (= cmd :Rjoin-slotmachine)
                (app/dialog:hide))

              (let [default-callback #(vita.helper/load-scene! :main-lobby)
                    err-callback (dialog.err/get-err-callback err default-callback)
                    callback #(do (ui-manager/clear! ui-manager/ui*)
                                  (err-callback))]
                (app/dialog:show {:make-fn #(dialog.err/dialog err callback)})))))

        :close
        (app/dialog:show {:make-fn #(dialog.err/dialog ERR/SOCKET_CLOSE dialog.util/page-reload)})

        :error
        (app/dialog:show {:make-fn #(dialog.err/dialog ERR/SOCKET_ERROR dialog.util/page-reload)})

        nil))))
