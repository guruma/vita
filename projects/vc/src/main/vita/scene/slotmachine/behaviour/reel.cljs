(ns vita.scene.slotmachine.behaviour.reel
  "프레임에서 릴 하나를 담당."
  (:require-macros [milk.macro :refer [def-behaviour letc]])
  (:require [milk.gatom :as gatom]
            [milk.event :as event]
            [vita.scene.slotmachine.util.image :as util]

            [js.canvas :as js.canvas]
            [vita.scene.slotmachine.const :as CONST]
            [clojure.set :as set]))


;; (def REEL_ITEM_INFO_DIC
;;   "릴 아이템 이미지 정보"
;;   {:blue7      {:src "img/slotmachine/n-7blue.png" :table (item-table 11 [120 120])}
;;    :red7       {:src "img/slotmachine/n-7red.png" :table (item-table 11 [120 120])}
;;    :gold7      {:src "img/slotmachine/n-7gold1.png" :table (item-table 4 [120 120])}
;;    :apple      {:src "img/slotmachine/n-apple.png" :table (item-table 8 [120 120])}
;;    :bar1       {:src "img/slotmachine/n-bar1.png" :table (item-table 4 [120 120])}
;;    :bar2       {:src "img/slotmachine/n-bar2.png" :table (item-table 4 [120 120])}
;;    :bar3       {:src "img/slotmachine/n-bar3.png" :table (item-table 4 [120 120])}
;;    :cherry     {:src "img/slotmachine/n-cherry.png" :table (item-table 8 [120 120])}
;;    :grape      {:src "img/slotmachine/n-grape.png" :table (item-table 8 [120 120])}
;;    :lemon      {:src "img/slotmachine/n-lemon.png" :table (item-table 8 [120 120])}
;;    :watermelon {:src "img/slotmachine/n-watermelon.png" :table (item-table 8 [120 120])}
;;    :question   {:src "img/slotmachine/n-question1.png" :table (item-table 4 [120 120])}})


;; (def R_REEL_ITEM_INFO_DIC
;;   {:blue7      {:src "img/slotmachine/r-7blue.png" :table (item-table 5 [140 140])}
;;    :red7       {:src "img/slotmachine/r-7red.png" :table (item-table 5 [140 140])}
;;    :gold7      {:src "img/slotmachine/r-7gold.png" :table (item-table 5 [140 140])}
;;    :apple      {:src "img/slotmachine/r-apple.png" :table (item-table 5 [140 140])}
;;    :bar1       {:src "img/slotmachine/r-bar1.png" :table (item-table 5 [140 140])}
;;    :bar2       {:src "img/slotmachine/r-bar2.png" :table (item-table 5 [140 140])}
;;    :bar3       {:src "img/slotmachine/r-bar3.png" :table (item-table 5 [140 140])}
;;    :cherry     {:src "img/slotmachine/r-cherry.png" :table (item-table 5 [140 140])}
;;    :grape      {:src "img/slotmachine/r-grape.png" :table (item-table 5 [140 140])}
;;    :lemon      {:src "img/slotmachine/r-lemon.png" :table (item-table 5 [140 140])}
;;    :watermelon {:src "img/slotmachine/r-watermelon.png" :table (item-table 5 [140 140])}
;;    :question   {:src "img/slotmachine/n-question2.png" :table (item-table 10 [140 140])}})


;; (def ^:private reel-item-img-dic*
;;   "릴 아이템 앞면->뒷면 이미지 정보."
;;   (->> CONST/REEL_ITEM_INFO_DIC
;;        (reduce-kv #(assoc %1 %2 {:img (util/load-image (:src %3))
;;                                  :table (:table %3)})
;;                   {})))


;; (def ^:private r-reel-item-img-dic*
;;   "릴 아이템 뒷면->앞면 이미지 정보."
;;   (->> CONST/R_REEL_ITEM_INFO_DIC
;;        (reduce-kv #(assoc %1 %2 {:img (util/load-image (:src %3))
;;                                  :table (:table %3)})
;;                   {})))


;; (defn- mult2 [dt] (Math/round (* dt 2)))


;; (defn- spin-reel-item [reel-item spin-amount]
;;   (let [{:keys [sym pos]} reel-item
;;         [px py] pos
;;         y' (+ py spin-amount)
;;         r (rem y' CONST/FRAME-REEL-HEIGHT)]
;;     {:sym sym
;;      :pos [px r]
;;      :dirty (>= y' CONST/FRAME-REEL-HEIGHT)}))


;; (defn- get-syms [reel-items]
;;   (map :sym reel-items))


;; (defn- get-rand-syms
;;   [coll]
;;   (->> (set coll)
;;        (set/difference #{:blue7 :red7 :gold7 :apple :bar1 :bar2 :bar3 :cherry :grape :lemon :watermelon})
;;        (shuffle)))


;; (defn- adjust-reel-items [reel-items]

;;   (if-not (some :dirty reel-items)

;;     reel-items

;;     (loop [acc [] [f & rst] reel-items r (get-rand-syms (get-syms reel-items))]
;;       (cond (not f)          acc
;;             (not (:dirty f)) (recur (conj acc f) rst r)
;;             :else            (recur (conj acc {:sym (first r)
;;                                                :pos (let [[px py] (:pos f)]
;;                                                       [px (+ py CONST/FRAME-REEL-HEIGHT-WITH-ITEM)])
;;                                                :dirty false})
;;                                     rst
;;                                     (rest r))))))


;; (defn- default-items [[reel-x reel-y]]
;;   [{:sym :bar2
;;     :pos [reel-x (- reel-y CONST/FRAME_REEL_ITEM_HEIGHT)]}
;;    {:sym :apple
;;     :pos [reel-x reel-y]}
;;    {:sym :watermelon
;;     :pos [reel-x (+ reel-y CONST/FRAME_REEL_ITEM_HEIGHT)]}
;;    {:sym :cherry
;;     :pos [reel-x (+ reel-y CONST/FRAME_REEL_ITEM_HEIGHT CONST/FRAME_REEL_ITEM_HEIGHT)]}])


;; (defn spin! [reel]
;;   ((aget reel :spin!)))


;; (defn stop! [reel triple]
;;   ((aget reel :stop!) triple))


;; (defn qm-reverting-effect! [reel dt]
;;   (aset reel :revert-acc (+ (aget reel :revert-acc) dt)))

;; ;; backs {[0 1] :red7, [4 2] :lemon}

;; (def-behaviour reel [pos]
;;   [[reel-x reel-y] pos
;;    items* (atom (default-items pos))]

;;   :spin?
;;   false

;;   :triple
;;   []

;;   :revert-q
;;   :bar2

;;   :revert-acc
;;   0

;;   :spin!
;;   (fn []
;;     (aset self :revert-acc 0)
;;     (aset self :spin? true))


;;   :stop!
;;   (fn [triple]
;;     (aset self :spin? false)
;;     (aset self :triple triple)

;;     (let [[t1 t2 t3] triple]
;;       (swap! items* assoc-in [0 :pos] [reel-x (- reel-y CONST/FRAME_REEL_ITEM_HEIGHT)])
;;       (swap! items* assoc-in [1] {:sym t1 :pos [reel-x reel-y]})
;;       (swap! items* assoc-in [2] {:sym t2 :pos [reel-x (+ reel-y CONST/FRAME_REEL_ITEM_HEIGHT)]})
;;       (swap! items* assoc-in [3] {:sym t3 :pos [reel-x (+ reel-y CONST/FRAME_REEL_ITEM_HEIGHT CONST/FRAME_REEL_ITEM_HEIGHT)]})))


;;   :update
;;   (fn [ga dt]

;;     (when (aget self :spin?)
;;       ;; TODO(kep) 여기 필히 고칠것.
;;       (swap! items* (fn [reel-items]
;;                       (->> reel-items
;;                            (mapv #(spin-reel-item % (mult2 dt)))
;;                            adjust-reel-items)))))

;;   :render
;;   (fn [ga ctx]
;;     (js.canvas/save ctx)
;;     (js.canvas/begin-path ctx)
;;     (js.canvas/rect ctx CONST/FRAME-RECT)
;;     (js.canvas/clip ctx)

;;     (doseq [{:keys [sym pos]} @items*]
;;       (if (or (= (aget self :revert-acc) 0)
;;               (not= sym :question))

;;         (let [{:keys [img table]} (get reel-item-img-dic* sym)
;;               [px py] pos
;;               [sx sy w h] (nth table 0)]
;;           (js.canvas/draw-image ctx img {:sx sx :sy sy :sw w :sh h
;;                                          :dx px :dy py :dw w :dh h}))



;;         (let [acc (aget self :revert-acc)
;;               k (quot acc (/ CONST/QM-REVERTING-TIME 18))
;;               [sym n [ax ay]] (cond (< k 4) [:question k [0 0]]
;;                                     (< k 13) [:question (- k 4) [-10 -10]]
;;                                     (< k 18) [(aget self :revert-q) (- k 13) [-10 -10]]
;;                                     :else [(aget self :revert-q) 0 [0 0]])
;;               {:keys [img table]} (if (or (<= k 3) (<= 18 k))
;;                                     (get reel-item-img-dic* sym)
;;                                     (get r-reel-item-img-dic* sym))
;;               [px py] pos
;;               [sx sy w h] (nth table n)]
;;           (js.canvas/draw-image ctx img {:sx sx :sy sy :sw w :sh h
;;                                          :dx (+ px ax) :dy (+ py ay) :dw w :dh h}))
;;         ))

;;     (js.canvas/restore ctx)))
