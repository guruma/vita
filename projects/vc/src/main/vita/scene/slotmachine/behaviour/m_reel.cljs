(ns vita.scene.slotmachine.behaviour.m-reel
  "프레임에서 릴 하나를 담당."
  (:require-macros [milk.macro :refer [def-behaviour letc]])
  (:require [milk.gatom :as gatom]
            [milk.event :as event]
            [vita.resource.image :as image]

            [js.canvas :as js.canvas]
            [vita.scene.slotmachine.const :as CONST]
            [clojure.set :as set]))


(def ^:private reel-item-img-dic*
  "릴 아이템 앞면->뒷면 이미지 정보."
  (->> CONST/M_REEL_ITEM_INFO_DIC
       (reduce-kv #(assoc %1 %2 %3)
                  {})))


(defn- mult2 [dt] (Math/round (* dt 2)))


(defn- spin-reel-item [reel-item spin-amount]
  (let [{:keys [sym pos]} reel-item
        [px py] pos
        y' (+ py spin-amount)
        r (rem y' CONST/FRAME-REEL-HEIGHT)]
    {:sym sym
     :pos [px r]
     :dirty (>= y' CONST/FRAME-REEL-HEIGHT)}))


(defn- get-syms [reel-items]
  (map :sym reel-items))


(defn- get-rand-syms
  [coll]
  (->> (set coll)
       (set/difference #{:blue7 :red7 :gold7 :apple :bar1 :bar2 :bar3 :cherry :grape :lemon :watermelon})
       (shuffle)))


(defn- adjust-reel-items [reel-items]

  (if-not (some :dirty reel-items)

    reel-items

    (loop [acc [] [f & rst] reel-items r (get-rand-syms (get-syms reel-items))]
      (cond (not f)          acc
            (not (:dirty f)) (recur (conj acc f) rst r)
            :else            (recur (conj acc {:sym (first r)
                                               :pos (let [[px py] (:pos f)]
                                                      [px (+ py CONST/FRAME-REEL-HEIGHT-WITH-ITEM)])
                                               :dirty false})
                                    rst
                                    (rest r))))))


(defn- default-items [[reel-x reel-y]]
  [{:sym :bar2
    :pos [reel-x (- reel-y CONST/FRAME_REEL_ITEM_HEIGHT)]}
   {:sym :apple
    :pos [reel-x reel-y]}
   {:sym :watermelon
    :pos [reel-x (+ reel-y CONST/FRAME_REEL_ITEM_HEIGHT)]}
   {:sym :cherry
    :pos [reel-x (+ reel-y CONST/FRAME_REEL_ITEM_HEIGHT CONST/FRAME_REEL_ITEM_HEIGHT)]}])


(defn spin! [reel]
  ((aget reel :spin!)))


(defn stop! [reel triple]
  ((aget reel :stop!) triple))


(defn qm-reverting-effect! [reel dt]
  (aset reel :revert-acc
        (+ (aget reel :revert-acc) dt)))

;; backs {[0 1] :red7, [4 2] :lemon}

(def-behaviour m-reel [pos]
  [[reel-x reel-y] pos
   items* (atom (default-items pos))]

  :spin?
  false

  :triple
  []

  :revert-q
  :bar2

  :revert-acc
  0

  :spin!
  (fn []
    (aset self :revert-acc 0)
    (aset self :spin? true))


  :stop!
  (fn [triple]
    (aset self :spin? false)
    (aset self :triple triple)

    (let [[t1 t2 t3] triple]
      (swap! items* assoc-in [0 :pos] [reel-x (- reel-y CONST/FRAME_REEL_ITEM_HEIGHT)])
      (swap! items* assoc-in [1] {:sym t1 :pos [reel-x reel-y]})
      (swap! items* assoc-in [2] {:sym t2 :pos [reel-x (+ reel-y CONST/FRAME_REEL_ITEM_HEIGHT)]})
      (swap! items* assoc-in [3] {:sym t3 :pos [reel-x (+ reel-y CONST/FRAME_REEL_ITEM_HEIGHT CONST/FRAME_REEL_ITEM_HEIGHT)]})))


  :update
  (fn [ga dt]

    (when (aget self :spin?)
      ;; TODO(kep) 여기 필히 고칠것.
      (swap! items* (fn [reel-items]
                      (->> reel-items
                           (mapv #(spin-reel-item % (mult2 dt)))
                           adjust-reel-items)))))

  :render
  (fn [ga ctx]
    (js.canvas/save ctx)
    (js.canvas/begin-path ctx)
    (js.canvas/rect ctx CONST/FRAME-RECT)
    (js.canvas/clip ctx)

    (doseq [{:keys [sym pos]} @items*]
      (let [img (image/load (get reel-item-img-dic* sym))
            [px py] pos]
        (js.canvas/draw-image ctx img px py)))
    (js.canvas/restore ctx)))
