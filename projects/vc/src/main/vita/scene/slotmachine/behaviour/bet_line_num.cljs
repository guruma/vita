(ns vita.scene.slotmachine.behaviour.bet-line-num
  (:require-macros [milk.macro :refer [def-behaviour letc]])
  (:require [milk.gatom :as gatom]
            [js.canvas :as js.canvas]

            [milk.event :as event]
            [milk.component.box-collider :as component.box-collider]
            [vita.resource.image :as image]))


(def-behaviour bet-line-num [line-num info fn-on-enter fn-on-exit]
  [{:keys [img img-k table]} info
   img (or img (image/load img-k))]

  :on-awake
  (fn [ga]
    (letc ga [box-collider :box-collider]
          (when-not box-collider
            (let [[_ _ w h] (nth table 0)]
              (event/add-component! ga (component.box-collider/box-collider :w w :h h))))))

  :on?
  false

  :on-mouse-enter
  #(fn-on-enter)

  :on-mouse-exit
  #(fn-on-exit)

  :render
  (fn [ga ctx]
    (letc ga [transform :transform]
          (let [[tx ty] (aget transform :pos)
                on-off (if (aget self :on?) 1 0)
                [sx sy w h] (nth table on-off)]
            (js.canvas/draw-image ctx img {:sx sx :sy sy :sw w :sh h
                                           :dx tx :dy ty :dw w :dh h})))))
