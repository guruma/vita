(ns vita.scene.slotmachine.behaviour.m-reel-flash
  (:require-macros [milk.macro :refer [def-behaviour letc]])
  (:require [milk.gatom :as gatom]
            [js.canvas :as js.canvas]
            ))


(def-behaviour m-reel-flash [info]
  [{:keys [on off]} info]

  :on?
  false

  :render
  (fn [ga ctx]
    (letc ga [transform :transform]
          (let [[tx ty] (aget transform :pos)]
            (if (aget self :on?)
              (js.canvas/draw-image ctx on tx ty)
              (js.canvas/draw-image ctx off tx ty))))))
