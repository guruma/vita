(ns vita.scene.slotmachine.behaviour.m-payout-flash
  (:require-macros [milk.macro :refer [def-behaviour letc]])
  (:require [milk.gatom :as gatom]
            [js.canvas :as js.canvas]
            [vita.scene.slotmachine.const :as CONST]
            [vita.resource.image :as image]
            ))



(def-behaviour m-payout-flash [info]
  [acc-dt* (atom 0)
   l-imgs (map image/load (get-in CONST/M_PAYOUT_FLASH_INFO [:left :img-ks]))
   r-imgs (map image/load (get-in CONST/M_PAYOUT_FLASH_INFO [:right :img-ks]))
   l-count (count l-imgs)
   r-count (count r-imgs)
   l-pos (get-in CONST/M_PAYOUT_FLASH_INFO [:left :pos])
   r-pos (get-in CONST/M_PAYOUT_FLASH_INFO [:right :pos])]


  :on?
  false

  :update
  (fn [ga dt]
    (if (aget self :on?)
      (swap! acc-dt* + (* 10 dt))
      (swap! acc-dt* + dt)
      )
    (when (>= @acc-dt* 4000)
      (reset! acc-dt* 0)))

  :render
  (fn [ga ctx]
    (let [
          l-img (nth l-imgs (quot @acc-dt* 1000))
          r-img (nth r-imgs (quot @acc-dt* 1000))
          [lx ly] l-pos
          [rx ry] r-pos]
      (js.canvas/draw-image ctx l-img lx ly)
      (js.canvas/draw-image ctx r-img rx ry))))
