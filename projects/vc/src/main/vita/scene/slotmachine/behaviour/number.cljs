(ns vita.scene.slotmachine.behaviour.number
  (:require-macros [milk.macro :refer [def-behaviour letc]])
  (:require [milk.gatom :as gatom]
            [js.canvas :as js.canvas]
            [vita.scene.slotmachine.util.image :as util]
            [vita.resource.image :as image]))


(def-behaviour number [info number]
  [{:keys [img img-k table direct visible]} info
   img (or img (image/load img-k))
   direct (or direct :left)]

  :number
  (or number 0)

  :visible (if (nil? visible) true visible)

  :render
  (fn [ga ctx]
    (when (and (aget self :visible)
               (not (neg? (aget self :number))))
      (letc ga [transform :transform]
            (let [[tx ty] (aget transform :pos)
                  digits (util/->digit (aget self :number))
                  count-digits (count digits)]

              (case direct
                :left
                (doseq [[n i] digits]
                  (let [[sx sy w h] (nth table n)]
                    (js.canvas/draw-image ctx img {:sx sx :sy sy :sw w :sh h
                                                   :dx (+ tx
                                                          (- (* i w)))
                                                   :dy ty
                                                   :dw w
                                                   :dh h})))

                :mid
                (doseq [[n i] digits]
                  (let [[sx sy w h] (nth table n)]
                    (js.canvas/draw-image ctx img {:sx sx :sy sy :sw w :sh h
                                                   :dx (+ tx
                                                          (- (* i w))
                                                          (* (/ count-digits 2) w))
                                                   :dy ty
                                                   :dw w
                                                   :dh h})))
                nil))))))
