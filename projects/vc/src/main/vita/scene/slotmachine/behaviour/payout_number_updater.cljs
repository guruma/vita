(ns vita.scene.slotmachine.behaviour.payout-number-updater
  (:require-macros [milk.macro :refer [def-behaviour letc]])
  (:require [milk.gatom :as gatom]
            [js.canvas :as js.canvas]

            [milk.event :as event]
            [vita.scene.slotmachine.counter :as counter]
            ))


(def TTTT 30)

(def-behaviour payout-number-updater [target [num next-num] callback]
  [target target
   counters (counter/count-range num next-num)
   acc* (atom 0)]

  :update
  (fn [ga dt]
    (swap! acc* + dt)

    (let [t @acc*]
      (if (< t (* TTTT (count counters)))
        (aset target :number (nth counters (quot t TTTT)))
        (do (aset target :number (last counters))
            (event/destroy-component! ga self)
            (when callback
              (callback)))))))
