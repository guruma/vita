(ns vita.scene.slotmachine.behaviour.paytable

  (:require-macros [milk.macro :refer [def-behaviour letc]])
  (:require [js.canvas :as js.canvas]
            [vita.resource.image :as image]
            [milk.gatom :as gatom]))


(defn dic-map [f m]
  (into {} (for [[k v] m] [k (f v)])))


(defn get-paytable-info [paytable]
  (->> (-> paytable
           (assoc-in [:gold7 5] :JackPot))
       (dic-map (fn [v]
                  (->> v
                       (map-indexed vector)
                       (remove (fn [[i v]] (= v 0)))
                       (reverse)
                       (vec))))))

(defn- add-seperator [n]
  (->> n
       (str)
       (reverse)
       (partition-all 3)
       (interpose \,)
       (flatten)
       (reverse)))


(def-behaviour paytable [{:keys [img paytable paytable-poses]}]
  [font-size 14
   paytable-info (get-paytable-info paytable)
   paytable-info (assoc paytable-info :symbol (:red7 paytable-info))]

  :render
  (fn [ga ctx]
    (letc ga [transform :transform]
          (let [[tx ty] (aget transform :pos)]
            (js.canvas/draw-image ctx img tx ty)
            )
          )


    (doseq [[k infos] paytable-info]
      (let [[sx sy] (get paytable-poses k)]

        (doseq [[idx [count chip]] (map-indexed vector infos)]
          (let [x sx
                y (+ sy (* (+ font-size 10) idx))]
            (-> ctx
                (js.canvas/save)
                (js.canvas/font-style (str font-size "pt Arial"))
                (js.canvas/fill-style :#EB7610)
                (js.canvas/text {:x x :y y :text count})

                (js.canvas/fill-style :#FFFFFF)
                (js.canvas/text {:x (+ x font-size):y y :text (cond
                                                               (number? chip)
                                                               (str "$" (apply str (add-seperator chip)))

                                                               :else
                                                               (name chip)
                                                                )})
                (js.canvas/restore))))))))
