(ns vita.scene.slotmachine.behaviour.multiplier
  (:require-macros [milk.macro :refer [def-behaviour letc]])
  (:require [milk.gatom :as gatom]
            [vita.scene.slotmachine.const :as CONST]

            [vita.resource.image :as image]
            [js.canvas :as js.canvas]
            [js.math :refer [round]]
            ))

;; TODO(kep) 리펙토링.


(defn mult2 [dt] (round (* dt 2)))


(defn cyclex
  "> (cyclex [:a :b :c :d :e] 2)
   [:c :d :e :a :b]"
  ([[fst & rst]]
     (conj (vec rst) fst))
  ([coll n]
     (->> (cycle coll)
          (drop n)
          (take (count coll))
          (vec))))


(def ^:private multipliers*
  (atom [{:dt-sum 0
          :offset-btm 0
          :idx-btm 0
          :spin-fn mult2
          :items (shuffle (range 10))}

         {:dt-sum 0
          :offset-btm 0
          :idx-btm 0
          :spin-fn mult2
          :items (shuffle (range 10))}]))


(defn spin-multiplier [multiplier dt]
  (let [{:keys [dt-sum idx-btm items spin-fn]} multiplier
        dt-sum' (+ dt-sum (spin-fn dt))
        idx-btm' (quot dt-sum' 55)
        offset-btm' (rem dt-sum' 55)
        items' (cyclex items (- idx-btm' idx-btm))]
    {:dt-sum dt-sum'
     :idx-btm idx-btm'
     :offset-btm offset-btm'
     :spin-fn spin-fn
     :items items'}))


(defn- spin-multipliers [dt]
  (swap! multipliers* update-in [0] spin-multiplier dt)
  (swap! multipliers* update-in [1] spin-multiplier dt))


(defn- coord->xy [i j]
  (let [x (if (zero? i) 615 670)
        y (+ 255
             (* 55 (dec j))
             (get-in @multipliers* [i :offset-btm]))]
    [x y]))


(defn- get-columns []
  (let [c0 (take 2 (get-in @multipliers* [0 :items]))
        c1 (take 2 (get-in @multipliers* [1 :items]))]
    (concat (map-indexed (fn [i v] [i 0 v]) c0)
            (map-indexed (fn [i v] [i 1 v]) c1))))


(def-behaviour multiplier []
  [img-dic (->> CONST/MULTIPLIER_DIC
                (reduce-kv #(assoc %1 %2 (image/load %3)) {}))]

  :number
  1

  :spin?
  false

  :update
  (fn [ga dt]
    (when (aget self :spin?)
      (spin-multipliers dt)))

  :orign-render
  ;; TODO(kep) x에대한 그림 그리는 것을 밖으로 빼자.
  (fn [ga ctx]
    (js.canvas/save ctx)
    (js.canvas/begin-path ctx)
    (js.canvas/rect ctx CONST/MULTIPLIER-RECT)
    (js.canvas/clip ctx)

    (if-not (aget self :spin?)
      (let [num (aget self :number)
            n0 (keyword (str (quot num 10)))
            n1 (keyword (str (rem num 10)))]
        (-> ctx
            (js.canvas/draw-image (:x img-dic) 555 255)
            (js.canvas/draw-image (n0 img-dic) 615 255)
            (js.canvas/draw-image (n1 img-dic) 670 255)))

      (do (js.canvas/draw-image ctx (:x img-dic) 555 255)
          (doseq [[i j item] (get-columns)]
            (let [[x y] (coord->xy i j)
                  k (keyword (str item))]
              (js.canvas/draw-image ctx (k img-dic) x y)))))
    (js.canvas/restore ctx)))
