(ns vita.scene.slotmachine.behaviour.timer
  (:require-macros [milk.macro :refer [def-behaviour letc]])
  (:require [milk.gatom :as gatom]
            [milk.event :as event]))


(def-behaviour timer [& {:keys [duration interval end-callback]}]
  [acc-duration* (atom 0)]

  :update
  (fn [ga dt]

    (swap! acc-duration* + dt)

    (when interval
      (interval dt))

    (when (>= @acc-duration* duration)
      (event/destroy-component! ga self)))

  :on-destroy
  #(when end-callback
     (end-callback %)))
