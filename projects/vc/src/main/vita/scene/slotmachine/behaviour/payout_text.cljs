(ns vita.scene.slotmachine.behaviour.payout-text
  (:require-macros [milk.macro :refer [def-behaviour letc]])
  (:require [milk.gatom :as gatom]
            [js.canvas :as js.canvas]
            [vita.scene.slotmachine.util.image :as util]
            [vita.resource.image :as image]
            ))


(def-behaviour payout-text [text-info text num-info num-pos]
  [text-dic (reduce-kv (fn [acc k v]
                         (assoc acc k {:img (if-let [img (:img v)]
                                              img
                                              (image/load (:img-k v))) :pos (:pos v)}))
                       {} text-info)
   {:keys [img img-k table]} num-info
   [nx ny] num-pos
   num-img (or img (image/load img-k))]

  :text
  text

  :number
  0

  :origin-render
  (fn [ga ctx]
    (when-not (= (aget self :text) :none)
      (let [{:keys [img pos]} (get text-dic (aget self :text))
            [px py] pos]
        (js.canvas/draw-image ctx img px py))

      (when (= (aget self :text) :freespin)
        (doseq [[n i] (util/->digit (aget self :number))]
          (let [[sx sy w h] (nth table n)]
            (js.canvas/draw-image ctx num-img {:sx sx :sy sy :sw w :sh h
                                               :dx (- nx (* i w)) :dy ny :dw w :dh h})))))))
