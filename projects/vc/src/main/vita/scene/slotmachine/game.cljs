(ns vita.scene.slotmachine.game
  "게임 총 관리자."
  (:require-macros [vita.config.macros :as vcm]
                   [vita.logger :as vcl])
  (:require [vita.scene.slotmachine.const :as CONST]
            [milk.event :as event :refer [new-gatom]]

            [vita.scene.slotmachine.data :as data]
            [vita.shared.slotmachine.formula :as fm]

            ;; - ui.
            [vita.scene.slotmachine.ui.ui-manager :as ui-manager]
            [vita.scene.slotmachine.ui.reel-manager :as reel-manager]
            [vita.scene.slotmachine.ui.time-manager :as time-manager]
            [vita.scene.slotmachine.ui.line-manager :as line-manager]
            [vita.helper]

            [vita.resource.sound :as sound]
            [vita.app :as app]
            ))


;; :state
;;  - :normal       => :spinnning
;;  - :spinning     => :waiting-multiplier => :spinning
;;                  => :waiting-calc       => :qm-reverting
;;                                         => :multiplying
;;  - :qm-reverting => :waiting-calc       => :multiplying
;;                                         => :waiting-multiplier => :multiplying
;;  - :multiplying  => :waiting-calc       => :completed
;;  - :completed    => :normal

(declare set-state!)
(declare waiting-multiplying)
(declare waiting-calc)


(defn init-my-info [data]
  (reset! ui-manager/spinning?* false)
  (swap! data/my* merge data/DEFAULT_MY_DATA data)
  (ui-manager/update-ui! :chip :bet :line :jackpot :freespin))


(defn spin-slotmachine! [data]

  (swap! data/my* merge data)

  (let [{:keys [backs]} data]

    (ui-manager/update-ui! :jackpot)

    (reel-manager/set!-backs (:reel-manager @ui-manager/ui*) backs))

  (time-manager/start-timer! (:time-manager @ui-manager/ui*) 1000
                             :end-callback #(set-state! :spinning)))


;; game state!
(defn get-score-multiple []
  (try
    (let [bingo (fm/bingo-lines (data/get-triples) (:bet-line-count @data/my*))
          score (fm/calc-payout-amount bingo (:per-line-bet @data/my*))
          multiple (Math/pow 2 (data/get-frame-gold7-count))]
      [score multiple])
    (catch :default e
      (vcl/ERROR "get-score-multiple error , err = %o" e))))


(defmulti set-state! (fn [id] id))


(defmethod set-state! :normal
  [id]
  (ui-manager/update-ui! :normal!))


(defmethod set-state! :spinning
  [id]
  (let [reel-manager (:reel-manager @ui-manager/ui*)
        multiplier (:multiplier @ui-manager/ui*)]

    (if (reel-manager/spinning? reel-manager)
      (let [[idx triple] (reel-manager/stop-reel reel-manager)]
        (sound/play :reel-stop-1)

        (data/sync!-triple idx triple)

        (ui-manager/update-ui! :reel-flash)

        (if (data/same-gold7-count?)
          (do (aset multiplier :spin? false)
              (time-manager/start-timer! (:time-manager @ui-manager/ui*)  300 :end-callback #(set-state! :spinning)))

          (do (aset multiplier :spin? true)
              (data/update-gold7-count)
              (waiting-multiplying (data/get-gold7-count) #(set-state! :spinning)))))

      (let [[score multiple] (get-score-multiple)]

        (waiting-calc score #(let [{:keys [triples backs]} @data/my*
                                   result (fm/sync-triples triples backs)]
                               (if (some #{:question} (flatten triples))
                                 (set-state! :qm-reverting)
                                 (set-state! :multiplying))))))))


(defmethod set-state! :multiplying
  [id]
  (let [[score multiple] (get-score-multiple)]
    (if (> multiple 1)
      (waiting-calc (* score multiple) #(set-state! :completed))
      (set-state! :completed))))


(defmethod set-state! :qm-reverting
  [id]
  ;;  - :qm-reverting => :waiting-calc       => :multiplying
  ;;                                         => :waiting-multiplier => :multiplying

  (time-manager/start-timer! (:time-manager @ui-manager/ui*)

                             CONST/QM-REVERTING-TIME
                             :interval (fn [dt]
                                         (let [reel-manager (:reel-manager @ui-manager/ui*)]
                                           (reel-manager/qm-reverting-effect reel-manager dt)))

                             :end-callback (fn []
                                             (data/sync-my-triples)
                                             (ui-manager/update-ui! :reel-flash)

                                             (let [[score multiple] (get-score-multiple)]
                                               (waiting-calc score (fn []
                                                                     (if (data/same-gold7-count?)
                                                                       (set-state! :multiplying)
                                                                       (do (data/update-gold7-count)
                                                                           (waiting-multiplying (data/get-gold7-count) #(set-state! :multiplying))))))))))


(defmethod set-state! :completed
  [id]

  (ui-manager/update-ui! :chip :complete!)
  (time-manager/start-timer! (:time-manager @ui-manager/ui*)
                             1000
                             :end-callback #(set-state! :normal)))



;; game state helper.
(defn- waiting-calc [n callback]
  (try
    (let [bingos (fm/bingo-lines (data/get-triples) (:bet-line-count @data/my*))]
      (doseq [[bingo v] bingos]
        (line-manager/set-bingo (:line-manager @ui-manager/ui*) (inc bingo) true)))

    (if (pos? n)
      (aset (:payout-text @ui-manager/ui*) :text :youwon))

    (when (and (pos? n)
               (not= n (aget (:money-payout @ui-manager/ui*) :number)))
      (vcl/DEBUG n (aget (:money-payout @ui-manager/ui*) :number))
      (sound/play :chip-collect))


    ((aget (:money-payout @ui-manager/ui*) :set-payout) n  callback)

    (catch :default e
      (vcl/ERROR "waiting-calc error, err = %o" e))))


(defn- waiting-multiplying [n callback]

  (let [multiplier (:multiplier @ui-manager/ui*)]
    (aset multiplier :spin? true)

    (time-manager/start-timer! (:time-manager @ui-manager/ui*)
                               100
                               :end-callback #(do (aset multiplier :number (Math/pow 2 n))
                                                  (aset multiplier :spin? false)
                                                  (time-manager/start-timer! (:time-manager @ui-manager/ui*)  300 :end-callback callback)))))
