(ns vita.scene.slotmachine.util.image
  (:require [sonaclo.util :as util]))


(def ^:private num-table*
  {\0 0 \1 1 \2 2 \3 3 \4 4 \5 5 \6 6 \7 7 \8 8 \9 9 \, 10 \$ 14})


(defn- add-seperator [n]
  (->> n
       str
       reverse
       (partition-all 3)
       (interpose \,)
       flatten))


(defn ->digit [n]
  (->> n
       (add-seperator)
       (map-indexed (fn [i n] [(get num-table* n) i]))))


(defn ->money [n]
  (->> [\$]
       (concat (add-seperator n))
       (map-indexed (fn [i n] [(get num-table* n) i]))))


(defn load-image [path]
  (util/load-image path))
