(ns vita.scene.slotmachine.data
  "게임 상에서 사용할 데이터 모음."
  (:require [vita.shared.slotmachine.formula :as fm]))


(def ^:const DEFAULT_MY_DATA
  {:begin-per-line-bet 200
   :auto-mode? false
   :freespin-mode? false
   :chip 0
   :bet-line-count 20
   :per-line-bet 200
   :jackpot 9999
   :freespin-count 0
   :backs nil
   :triples nil
   })


(def my*
  "현재 유저의 정보를 기록."
  (atom {}))


(defn sub-my-freespin-count
  "freespin시, freespin 횟수를 차감한다."
  []
  (swap! my* update-in [:freespin-count] dec))


(defn freespin-mode [k]
  (swap! my* assoc :freespin-mode? (if (= :on k) true false)))


(defn sub-my-chip
  "spin시, 배팅 금액을 차감한다."
  []
  (swap! my* (fn [my]
               (merge-with - my {:chip (* (:bet-line-count my)
                                          (:per-line-bet my))}))))


;; frame internal.
(def frame*
  "ui frame의 갱신 정보를 담고 있다."
  (atom {:triples []
         :gold7-count 0}))


(defn get-triples
  []
  (:triples @frame*))


(defn get-gold7-count
  []
  (:gold7-count @frame*))


(defn get-frame-gold7-count
  "frame triples에서 gold7를 얻어온다."
  []

  (->> (get-triples)
       (flatten)
       (filter #{:gold7})
       (count)))


(defn get-triple-idxs
  [triples key]

  (for [[i t] (map-indexed vector (get-triples))
        :when (some #(and (= % key) %) t)]
    i))


(defn get-frame-gold7-idxs
  "frame triples에서 gold7 reel idx를 얻어온다."
  []
  (-> (get-triples)
      (get-triple-idxs :gold7)))


(defn ^:boolean triples-has-key?
  [triples key]
  (-> (get-triple-idxs triples key)
      (empty?)
      (not)))


(defn sync-my-triples []
  (swap! frame*
         assoc :triples
         (let [m @my*]
           (fm/sync-triples (:triples m) (:backs m)))))

(defn sync!-triple [idx triple]
  (swap! frame*
         update-in [:triples]
         (fn [triples]
           (assoc-in triples [idx] triple))))


(defn reset!-triples []
  (swap! frame* assoc :triples []))


(defn reset!-gold7-count []
  (swap! frame* assoc :gold7-count 0))


(defn ^:boolean same-gold7-count? []
  (= (get-gold7-count) (get-frame-gold7-count)))


(defn update-gold7-count []
  (swap! frame* assoc :gold7-count (get-frame-gold7-count)))


;;
(defn get-score-multiple []
  (let [bingo (fm/bingo-lines (get-triples) (:bet-line-count @my*))
        score (fm/calc-payout-amount bingo (:per-line-bet @my*))
        multiple (Math/pow 2 (get-frame-gold7-count))]
    [score multiple]))
