(ns vita.scene.slotmachine.net.r-freecoin
  (:require [vita.my]
            [vita.app :as app]

            [vita.scene.slotmachine.network :as network]
            [vita.shared.const.slotmachine :as PROTOCOL]
            ))


(defn freecoin
  [{:keys [event-type money]}]

  (when (and event-type money)
    (let [f (fn [] (vita.scene.common.dialog.freecoin/dialog event-type
                                               money
                                               #(network/send PROTOCOL/Q_FREECOIN {})))]

      (app/dialog:show {:make-fn f}))))
