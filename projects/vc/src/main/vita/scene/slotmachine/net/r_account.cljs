(ns vita.scene.slotmachine.net.r-account
  (:require
   [vita.my :as my]
   [vita.app :as app]
   [vita.scene.common.bar.bar :as barm]
   ))


(defn Raccount [{{:keys [account]} :data}]
  (let [{:keys [user game]} account]
    (my/set-user user)
    (my/set-game game)
    (barm/update-money)))
