(ns vita.scene.slotmachine.net.r-payment
  (:require-macros [vita.logger :as vcl])
  (:require
   [vita.scene.slotmachine.network :as network]
   [vita.shared.const.slotmachine :as PROTOCOL]
   [vita.my :as my]
   [vita.scene.common.bar.bar :as barm]
   [vita.scene.common.dialog.buy_result :as buy_result]
   ))


(defn Rpayment [{:keys [status account]}]
  (vcl/INFO (str "R-payment result: " status))
  (vcl/INFO (str "R-payment result: " account))
  (if (= status "completed")
    (let [{:keys [user game]} account]
      (my/set-user user)
      (my/set-game game)
      (barm/update-money)
      (buy_result/show (:money user)))))
