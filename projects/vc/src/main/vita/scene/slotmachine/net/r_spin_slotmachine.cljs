(ns vita.scene.slotmachine.net.r-spin-slotmachine
  "슬롯머신 돌리기."
  (:require [vita.scene.slotmachine.game :as game]))


(defn spin-slotmachine
  "
  # 입력:
  {:cmd PROTOCOL/SPIN_SLOTMACHINE
   :data {:line
          :bet-amount
          :chip
          :freespin-cnt}}

  # 출력:
  {:cmd PROTOCOL/SPIN_SLOTMACHINE
   :err PROTOCOL/ERR_NONE
   :data {:triples
          :chip
          :jackpot
          :freespin-cnt
          :backs
          :triples}}
"
  [data]
  (game/spin-slotmachine! data))
