(ns vita.scene.slotmachine.net.r-timeout
  (:require
   [vita.my]
   [vita.app :as app]
   ))


(defn timeout
  [& _]
  (app/dialog:show {:make-fn #(vita.scene.common.dialog.timeout/dialog)}))
