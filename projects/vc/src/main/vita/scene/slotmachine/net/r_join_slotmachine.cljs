(ns vita.scene.slotmachine.net.r-join-slotmachine
  "슬롯머신 게임 접속."
  (:require [vita.scene.slotmachine.game :as game]
            [vita.app :as app]
            ))


(defn join-slotmachine
  "
  # 입력:
  {:cmd PROTOCOL/JOIN_SLOTMACHINE
   :data {:bet-amount}}

  # 출력:
  {:cmd PROTOCOL/JOIN_SLOTMACHINE
   :err PROTOCOL/ERR_NONE
   :data {:jackpot
          :chip
          :begin-per-line-bet}}
"
  [data]
  (app/dialog:hide)
  (game/init-my-info data))
