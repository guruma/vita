(ns vita.scene.slotmachine.counter
  "payout 숫자 갱신 효과를 위한 카운터")

;; <counter 변화 패턴>
;;
;; 0 --> 987
;; ----------------------------------------------------------------
;; 100 200 300 400 500 600 700 800         : 8회   (n-1) * 100
;; 810 820 830 840 850 860 870 880 890 900
;; 910 920 930 940 950 960 970             : 17회  (n-1+10) * 10
;; 971 972 973 974 975 976 977 978 979 980
;; 981 982 983 984 985 986 987             : 17회  (n+10) * 1

;; 첫쨰자리    수: (n-1) * 100
;; 둘쨰자리    수: (n-1+10) * 10
;; 마지막 자리 수: (n+10) * 1

;; 987 --> 1415
;; ------------------------------------------------------------------
;; 1087 1187 1287 1387                      :4회  (14 - 9 + 1)
;; 10   11   12   13

;; 1397 1407                                :2회  (11 - 8 + 1)
;;   9   10

;; 1408 1409 1410 1411 1412 1413 1414 1415  :8회  (15 - 7)
;;    8    9   10   11   12   13   14   15

(def ^:private char-table*
  {\0 0 \1 1 \2 2 \3 3 \4 4 \5 5 \6 6 \7 7 \8 8 \9 9})


(defn- char->int [ch]
  (char-table* ch))


;; <sample>
;; num    => 789
;; return => {2 7, 1 8, 0 9}
(defn- split-int
  "#num {int} number to split
   #return {map} {place digit, ...}
                  place 자리수 (10^place)"
  [num]
  (let [chars (seq (str num))           ; (\7 \8 \9)
        ints (map char->int chars)      ; (7 8 9)
        length (count ints)]            ; 3
    (zipmap (range 0 length)
            (reverse ints) )))


(defn- to-int [n]
  (if (nil? n) 0 n))


(defn calc-diff
  "#start  {int} start number
  #end    {int} end number
  #return {vec} [[digit place] ...]"
  [start end]
  (let [[min' max']  (map split-int [start end])
        max-len   (count max')
        rev-range (reverse (range 0 max-len))
        borrowed  #(if (= -1 (get (last %) 0)) 0 10)]
    (if (>= start end)
      ()
      (reduce (fn [acc i]
                (let [digit
                      (cond
                       (= i (dec max-len) 0) ; 가장 높은 자리수가 첫쨰 자리일 때
                       (- (max' i) (to-int (min' i)))

                       (= i (dec max-len)) ; 가장 높은 자리수일 때
                       (- (max' i) (inc (to-int (min' i))))

                       (= i 0)          ; 가장 낮은 자리수일 때
                       (- (+ (max' i) (borrowed acc))
                          (to-int (min' i)))

                       :else            ; 기타
                       (- (+ (max' i) (borrowed acc))
                          (inc (to-int (min' i))) ))]
                  (conj acc [digit (int (Math/pow 10 i))]) ))
              []
              rev-range) )))


;; <sample>
;; [start end] => [0 789]
;; diff-seq => [[6 100] [17 10] [19 1]]
;; deltas   => (100 100 100 100 100 100
;;              10 10 10 10 10 10 10 10 10 10 10 10 10 10 10 10 10
;;              1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1)
;; return   => (100 200 300 400 500 600 610 620 630 640 650 660 670 680 690 700
;;              710 720 730 740 750 760 770 771 772 773 774 775 776 777 778 779
;;              780 781 782 783 784 785 786 787 788 789)
(defn count-range
  [start end]
  (if (>= start end)
    nil

    (let [diff-seq (calc-diff start end) ;  [[6 100] [17 10] [19 1]]
          deltas   (mapcat (fn [[digit place]]
                             (repeat digit place))
                           diff-seq)]
      (reductions + (concat [start] deltas)))))
