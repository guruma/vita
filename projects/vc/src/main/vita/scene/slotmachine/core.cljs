(ns vita.scene.slotmachine.core
  "slotmachien main scene."
  (:require-macros
   [vita.config.macros :as cm])
  (:require
   [milk.event :as event :refer [new-gatom]]

   [vita.app :as app]
   [vita.scene.slotmachine.resource.audio]

   ;; debug.
   [vita.scene.common.behaviour.fps :refer [fps]]

   [vita.scene.common.behaviour.loading :refer [loading]]

   ;; ui.
   [vita.scene.slotmachine.ui.ui-manager :as ui-manager]

   [vita.scene.common.bar.bar :as bar]
   [vita.resource.sound :as sound]

   ;; network
   [vita.scene.slotmachine.behaviour.networker :refer [networker]]))


;; main.
(defn scene []
  (app/dialog:show {:make-fn (fn [] [(new-gatom "" [(loading)] :pos [400 400])])})

  ;; todo .....testcode
  (sound/add-loads vita.scene.slotmachine.resource.audio/audio-paths)



  (bar/bar-init :scene :slotmachine
                :sound-on #(sound/sound-on (fn [] true) :bgm-slotmachine)
                :sound-off sound/sound-off
                :sock-close #(do
                               (vita.scene.slotmachine.network/close)))
  (cm/DEBUG
   (event/new-gatom "fps" [(fps)] :pos [100 50]))
  (ui-manager/init-ui!)
  (event/new-gatom "networker" [(networker)]))
