(ns vita.scene.slotmachine.ui.reel-flash-manager
  "빙고시 릴을 반짝이게 하기 위한 용도."
  (:require [milk.event :refer [new-gatom del-gatom]]
            [vita.scene.slotmachine.behaviour.reel-flash :refer [reel-flash]]
            [vita.scene.slotmachine.behaviour.m-reel-flash :refer [m-reel-flash]]

            [vita.scene.slotmachine.const :as CONST]

            [vita.resource.image :as image]))



(defn new-reel-flash-manager []
  (let [reel-img-info (reduce-kv (fn [acc k v]
                                   (assoc acc k (image/load v)))
                                 {}
                                 CONST/M_REEL_FLASH_INFO)
        reel-flash-dic {0 (m-reel-flash reel-img-info)
                        1 (m-reel-flash reel-img-info)
                        2 (m-reel-flash reel-img-info)
                        3 (m-reel-flash reel-img-info)
                        4 (m-reel-flash reel-img-info)}]
    (doseq [[k v] reel-flash-dic]
      (new-gatom "reel-flash"
                 [v]
                 :pos (get CONST/REEL_FLASH_POSES k)))
    (atom reel-flash-dic)))


(defn clear! [mgr]
  (reset! mgr nil))


(defn reel-flash-on [mgr reel-nums]
  (doseq [reel-num reel-nums]
    (when-let [flash (get @mgr reel-num)]
      (aset flash :on? true))))



(defn reel-flash-all-off [mgr]
  (doseq [[k flash] @mgr]
    (aset flash :on? false)))
