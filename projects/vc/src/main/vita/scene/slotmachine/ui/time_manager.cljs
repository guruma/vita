(ns vita.scene.slotmachine.ui.time-manager
  "타이머."
  (:require [milk.event :as event :refer [new-gatom]]
            [vita.scene.slotmachine.behaviour.timer :refer [timer]]))


(defn new-time-manager []
  (new-gatom "timer"
             []
             :pos [9999999 99999]))


(defn clear! [mgr]
  nil)


(defn start-timer! [mgr duration & {:keys [interval end-callback]}]
  (-> mgr
      (event/add-component! (timer :duration duration :interval interval :end-callback end-callback))))
