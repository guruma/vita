(ns vita.scene.slotmachine.ui.line-manager
  "빙고 라인 관리자."
  (:require [milk.event :refer [new-gatom del-gatom]]
            [vita.scene.slotmachine.behaviour.bet-line :refer [bet-line]]
            [vita.scene.slotmachine.const :as CONST]))



(defn new-line-manager
  " {line-num {:over
               :bingo
               :line-num
               :ga}}
  "
  [line-num-dic]
  (atom line-num-dic))


(defn clear! [mgr]
  (reset! mgr nil))


(defn- show-line [mgr n]
  (if-let [ga (get-in @mgr [n :ga])]
    (aset ga "active" true)

    (let [line-info (get CONST/BET_LINE_INFO_DIC n)
          pos (:pos line-info)]
      (swap! mgr
             assoc-in [n :ga] (new-gatom "btn-line-1"
                                         [(bet-line line-info)]
                                         :pos pos)))))


(defn- hide-line [mgr line-num]
  (when-let [ga (get-in @mgr [line-num :ga])]
    (aset ga "active" false)))


(defn- invalidate [mgr n]
  (let [line-info (get @mgr n)
        over (:over line-info)
        bingo (:bingo line-info)
        line-num (:line-num line-info)]

    (aset line-num :on? bingo)

    (if (or over bingo)
      (show-line mgr n)
      (hide-line mgr n))))


(defn set-over [mgr n over?]
  (swap! mgr assoc-in [n :over] over?)
  (invalidate mgr n))


(defn set-bingo [mgr n bingo?]
  (swap! mgr assoc-in [n :bingo] bingo?)
  (invalidate mgr n))
