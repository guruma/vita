(ns vita.scene.slotmachine.ui.reel-manager
  "릴 회전 및 갱신 담당"
  (:require [milk.event :refer [new-gatom del-gatom]]
            [vita.scene.slotmachine.behaviour.m-reel :as m-reel]
            [vita.scene.slotmachine.const :as CONST]
            [vita.scene.slotmachine.data :as data]))


(defn new-reel-manager []
  (let [reels (mapv #(m-reel/m-reel %) CONST/FRAME_REEL_POSITIONS)]
    (doseq [r reels]
      (new-gatom "reel" [r]))
    reels))


(defn clear! [mgr]
  nil)


(defn spin-reels [mgr]
  (doseq [r mgr]
    (m-reel/spin! r)))


(defn stop-reel [mgr]
  (let [[idx r] (some (fn [[i v]]
                        (and (aget v :spin?) [i v]))
                      (map-indexed vector mgr))]

    (m-reel/stop! r (nth (:triples @data/my*) idx))

    [idx (aget r :triple)]))


(defn spinning? [mgr]
  (->> mgr
       (some #(aget % :spin?))
       boolean))


(defn- get-gold7-count [reel]
  (->> (aget reel :triple)
       (filter #{:gold7})
       (count)))


(defn get-sum-gold7-count [mgr]
  (->> mgr
       (map get-gold7-count)
       (reduce +)))


(defn get-gold7-reel-idxs [mgr]
  (->> mgr
       (keep-indexed (fn [idx v]
                       (when (pos? (aget v :gold7-count))
                         idx)))))


(defn qm-reverting-effect [mgr dt]
  (doseq [r mgr]
    (m-reel/qm-reverting-effect! r dt)))


(defn set!-backs [mgr backs]
  (let [remap-backs (->> backs
                         (reduce-kv (fn [acc k v]
                                      (assoc acc (first k) v)) {}))]
    (doseq [[i r] (map-indexed vector mgr)]
      (when-let [v (get remap-backs i)]
        (aset r :revert-q v)))))
