(ns vita.scene.slotmachine.ui.ui-manager
  "슬롯머신 전체 UI 담당."
  (:require
   [milk.gatom :as gatom]
   [milk.event :as event :refer [new-gatom]]

   [sonaclo.util :as util]
   [vita.helper]

   [vita.app :as app]

   ;; - ui.
   [vita.scene.common.behaviour.ui-image :refer [ui-image]]
   [vita.scene.common.behaviour.ui-button :refer [ui-button]]
   [vita.scene.common.behaviour.ui-toggle-button :refer [ui-toggle-button]]

   ;; - dialog
   [vita.scene.common.dialog.goto-lobby]
   [vita.scene.slotmachine.behaviour.paytable]
   [milk.component.box-collider :as component.box-collider]

   ;; - slotmachine.
   [vita.scene.slotmachine.behaviour.bet-line-num :refer [bet-line-num]]
   [vita.scene.slotmachine.behaviour.number :refer [number]]
   [vita.scene.slotmachine.behaviour.payout-number :refer [payout-number]]
   [vita.scene.slotmachine.behaviour.multiplier :refer [multiplier]]
   [vita.scene.slotmachine.behaviour.m-payout-flash :refer [m-payout-flash]]
   [vita.scene.slotmachine.behaviour.payout-text :refer [payout-text]]

   ;; - manager.
   [vita.scene.slotmachine.ui.reel-manager :as reel-manager]
   [vita.scene.slotmachine.ui.time-manager :as time-manager]
   [vita.scene.slotmachine.ui.line-manager :as line-manager]
   [vita.scene.slotmachine.ui.reel-flash-manager :as reel-flash-manager]

   ;; data
   [vita.scene.slotmachine.data :as data]
   [vita.scene.slotmachine.const :as CONST]



   ;; network.
   [vita.scene.slotmachine.network :as network]
   [vita.shared.const.slotmachine :as PROTOCOL]

   ;; sound
   [vita.resource.sound :as sound]
   [vita.resource.image :as image]))


(def spinning?*
  (atom false))

(def ui*
  (atom {:num-line-view nil
         :num-bet-view nil
         :num-total-bet nil
         :num-jackpot nil
         :num-chip nil
         :num-freespin nil

         :money-payout nil
         :payout-flash nil
         :payout-text nil

         :btn-spin nil
         :btn-auto nil
         :btn-line nil
         :btn-bet nil
         :btn-tolobby nil

         :multiplier nil
         :reel-flash-manager nil
         :reel-manager nil
         :time-manager nil
         :line-manager nil}))


(defn clear! [ui]
  (let [{:keys [reel-flash-manager reel-manager time-manager line-manager]} @ui]
    (reel-flash-manager/clear! reel-flash-manager)
    (reel-manager/clear! reel-manager)
    (time-manager/clear! time-manager)
    (line-manager/clear! line-manager))
  (reset! ui nil))



;; ==================
;; ui update handler.

(declare do-spin!)
(declare update-ui)

(defn update-ui! [& ids]
  (let [my @data/my* ui @ui*]
    (doseq [id ids]
      (update-ui my ui id))))


(defmulti update-ui (fn [my ui id] id))


(defmethod update-ui :line
  [my ui id]
  ;; 라인 카운팅 갱신.

  (let [{:keys [bet-line-count per-line-bet]} my
        {:keys [num-line-view num-total-bet]} ui]
    (aset num-line-view :number bet-line-count)
    (aset num-total-bet :number (* bet-line-count per-line-bet))))


(defmethod update-ui :bet
  [my ui id]
  ;; 라인당 베팅 머니 갱신
  .
  (let [{:keys [bet-line-count per-line-bet]} my
        {:keys [num-bet-view num-total-bet]} ui]
    (aset num-bet-view :number per-line-bet)
    (aset num-total-bet :number (* bet-line-count per-line-bet))))


(defmethod update-ui :chip
  [my ui id]
  ;; 보유 chip 갱신.

  (let [{:keys [chip]} my
        {:keys [num-chip]} ui]
    (aset num-chip :number chip)))


(defmethod update-ui :jackpot
  [my ui id]
  ;; 잭팟시 얻을 칩 갱신.

  (let [{:keys [jackpot]} my
        {:keys [num-jackpot]} ui]
    (aset num-jackpot :number jackpot)))


(defmethod update-ui :reel-flash
  [my ui id]
  ;; 잭팟시 얻을 칩 갱신.

  (let [{:keys [triples]} @data/frame*
        {:keys [reel-flash-manager]} ui]

    (reel-flash-manager/reel-flash-on reel-flash-manager
                                      (map first (filter (fn [[i v]]
                                                           (some #{:gold7} v))
                                                         (map-indexed vector triples))))))

(declare on-btn-click)

(defmethod update-ui :spin!
  [my ui id]

  ;; 슬롯을 돌릴시, ui 초기화 작업.
  (reset! spinning?* true)

  (let [{:keys [jackpot freespin-count freespin-mode?]} my
        {:keys [payout-text payout-flash num-freespin
                reel-flash-manager reel-manager multiplier
                btn-auto btn-line btn-bet btn-max-bet btn-spin money-payout btn-freespin
                btn-max-bet-ga]} ui]

    (if (pos? freespin-count)

      ;; freespin이 남아있으면, max-bet 상태로 돌린다
      (do
        (on-btn-click btn-max-bet-ga :btn-max-bet)
        (data/sub-my-freespin-count)
        (data/freespin-mode :on)
        (update-ui! :freespin))

      ;; freespin이 없으면, chip을 소모한다.
      (do
        (data/sub-my-chip)
        (update-ui! :chip)
        (aset payout-text :text :goodluck)))

    ;; 릴 회전.
    (aset multiplier :number 1)
    (aset multiplier :spin? true)

    (reel-manager/spin-reels reel-manager)

    (aset payout-flash :on? false)
    (aset money-payout :number 0)

    (aset btn-max-bet :enabled? false)
    (aset btn-line :enabled? false)
    (aset btn-bet :enabled? false)
    (aset btn-spin :enabled? false)

    (data/reset!-triples)
    (data/reset!-gold7-count)

    (doseq [bingo (range 1 (inc 20))]
      (line-manager/set-bingo (:line-manager @ui*) bingo false))

    (reel-flash-manager/reel-flash-all-off reel-flash-manager)
    ))


(defmethod update-ui :complete!
  [my ui id]
  ;; 슬롯을 돌린후, ui 초기화 작업.

  (let [{:keys [jackpot auto-mode? freespin-count]} my
        {:keys [btn-auto payout-text payout-flash btn-line btn-bet btn-spin]} ui]

    (let [[score multiple] (data/get-score-multiple)]
      (if (pos? score)
        (aset payout-flash :on? true)
        (aset payout-text :text :oops))
      (when (pos? score)
        (sound/play :bingo-1)))))


(defmethod update-ui :normal!
  [my ui id]

  (let [{:keys [jackpot auto-mode? freespin-mode? freespin-count]} my
        {:keys [btn-auto btn-max-bet btn-line btn-bet btn-spin
                payout-text payout-flash]} ui]

    (when-not (pos? freespin-count)
      (data/freespin-mode :off))


    (if (or auto-mode? (and (pos? freespin-count)
                            freespin-mode?))

      (do-spin!)

      (do (reset! spinning?* false)
          (aset btn-max-bet :enabled? true)
          (aset btn-line :enabled? true)
          (aset btn-bet :enabled? true)
          (aset btn-spin :enabled? true)))))


(defmethod update-ui :freespin
  [my ui id]
  (let [{:keys [freespin-count freespin-mode?]} my
        {:keys [num-freespin num-freespin-ga
                btn-freespin btn-freespin-ga
                btn-freespin-stop btn-freespin-stop-ga
                btn-auto btn-max-bet btn-line btn-bet btn-spin]} ui]

    (if (pos? freespin-count)

      (do (aset num-freespin :number freespin-count)
          (aset num-freespin :visible true)

          (if freespin-mode?

            (do (aset btn-freespin :visible? false)
                (aset btn-freespin-stop :visible? true)
                (let [box-comp (gatom/get-component btn-freespin-ga :box-collider)
                      stop-box-comp (gatom/get-component btn-freespin-stop-ga :box-collider)]
                  (aset box-comp :z -10)
                  (aset stop-box-comp :z 10)))

            (do (aset btn-freespin :visible? true)
                (aset btn-freespin-stop :visible? false)
                (let [box-comp (gatom/get-component btn-freespin-ga :box-collider)
                      stop-box-comp (gatom/get-component btn-freespin-stop-ga :box-collider)]
                  (aset box-comp :z 10)
                  (aset stop-box-comp :z -10)))))

      ;; 프리스핀 횟수 다 끝난 이후에는, 숫자 ui 와 버튼 ui 를 안보이게 한다
      (do (aset num-freespin :visible false)
          (aset btn-freespin :visible? false)
          (aset btn-freespin-stop :visible? false)
          (let [box-comp (gatom/get-component btn-freespin-ga :box-collider)
                stop-box-comp (gatom/get-component btn-freespin-stop-ga :box-collider)]
            (aset box-comp :z -10)
            (aset stop-box-comp :z -10))

          ;; auto, spin 버튼 활성
          (reset! spinning?* false)
          (aset btn-max-bet :enabled? true)
          (aset btn-line :enabled? true)
          (aset btn-bet :enabled? true)
          (aset btn-spin :enabled? true)
          (aset btn-auto :enabled? true)))))


(defn- goto-lobby-dialog [msg callback]
  (app/dialog:show {:make-fn (fn [] [(new-gatom "do-spin-error"
                                                [(vita.scene.common.dialog.goto-lobby/dialog msg
                                                                                             #(do (callback)
                                                                                                  (vita.helper/load-scene! :main-lobby)))]
                                                :pos [400 400])])}))


;; spin!!
(defn do-spin! []
  (let [{:keys [bet-line-count per-line-bet chip freespin-count]} @data/my*
        is-freespin? (pos? freespin-count)]

    (if (and (not is-freespin?) (< chip (* bet-line-count per-line-bet)))

      ;; 프리스핀도 아니고, 보유칩도 부족하다면 에러 다이얼로그
      (goto-lobby-dialog "doesn't enought chip" (fn [] (.setTimeout js/window #(clear! ui*) 1000)))

      ;; spin 돌리기 시작.
      (do
        (sound/play :reel-spin-1)
        (update-ui! :spin!)

        (->> @data/my*
             (util/filter-k #{:bet-line-count :per-line-bet :chip})
             (network/send PROTOCOL/Q_SPIN_SLOTMACHINE))))))

;; ==============
;; ui 버튼 핸들러.
(defmulti on-btn-click (fn [ga id]
                         (sound/play :click-1)
                         id))


(defmethod on-btn-click :btn-spin
  [ga]

  (aset ga :enabled? false)
  (do-spin!))


(defmethod on-btn-click :btn-auto-on
  [ga]

  (when-not @spinning?*
    (do-spin!))
  (swap! data/my* assoc :auto-mode? true))


(defmethod on-btn-click :btn-freespin
  [ga]
  (data/freespin-mode :on)
  (update-ui! :freespin)
  (if-not @spinning?*
    (do-spin!)))


(defmethod on-btn-click :btn-freespin-stop
  [ga]
  (data/freespin-mode :off)
  (update-ui! :freespin))


(defmethod on-btn-click :btn-auto-off
  [ga]
  (swap! data/my* assoc :auto-mode? false))


(defmethod on-btn-click :btn-line
  [ga]
  (swap! data/my* update-in [:bet-line-count]
         (fn [line] (inc (mod line CONST/MAX_BET_LINE_COUNT))))
  (update-ui! :line))


(defmethod on-btn-click :btn-bet
  [ga]
  (let [{:keys [begin-per-line-bet]} @data/my*]
    (swap! data/my* update-in [:per-line-bet]
           (fn [bet]
             (if (< bet (* begin-per-line-bet CONST/MAX_PER_LINE_BET_COUNT))
               (+ bet begin-per-line-bet)
               begin-per-line-bet))))
  (update-ui! :bet))


(defmethod on-btn-click :btn-max-bet
  [ga]
  (let [{:keys [begin-per-line-bet]} @data/my*]
    (swap! data/my*
           assoc
           :per-line-bet (* begin-per-line-bet CONST/MAX_PER_LINE_BET_COUNT)
           :bet-line-count CONST/MAX_BET_LINE_COUNT))
  (update-ui! :bet :line))


(defmethod on-btn-click :btn-tolobby
  [ga]
  (aset ga :enabled? false)

  ;; FIXME(kep) 매직이 있는 코드임. :waiting-calc 처리중, 로비누르면, ui에 대한 레퍼런스들이 날라가면서, access 에러가 남.
  ;; 일단 우회로 setTimeout을 걸어두겠음.
  (.setTimeout js/window #(clear! ui*) 1000)

  (vita.helper/load-scene! :main-lobby))


(defmethod on-btn-click :btn-paytable
  [ga]
  (app/dialog:show {:make-fn (fn []

                               [
                                (let [{:keys [img-k table pos]} CONST/UI_BTN_PAY_TABLE_CLOSE
                                      com (ui-button :button-info {:img (image/load img-k)
                                                                   :table table}
                                                     :order 30
                                                     :on-click #(app/dialog:hide))
                                      com2 (let [[_ _ w h] (:normal table)]
                                             (component.box-collider/box-collider :w w :h h))
                                      ]
                                  (new-gatom "pay_close" [com com2] :pos pos))

                                (let [{:keys [img-k pos]} CONST/BG_PAY_TABLE]

                                  (new-gatom "" [(vita.scene.slotmachine.behaviour.paytable/paytable {:img (image/load img-k)
                                                                                                :paytable
                                                                                                (vita.shared.slotmachine.formula/get-paytable
                                                                                                 (:per-line-bet @data/my*))
                                                                                                :paytable-poses
                                                                                                CONST/PAY_TABLE_POSES
                                                                                                })]
                                             :pos pos)
                                  )
                                ]
                               )})

  )

;; maker.
(defn- make-button [name info on-click]
  (let [{:keys [img-k table pos visible]} info
        com (ui-button :button-info {:img (image/load img-k)
                                     :table table}
                       :on-click on-click
                       :visible visible)
        ga (new-gatom name [com] :pos pos)]

    {name com
     (util/keyword+ name "-ga") ga}))


(defn- make-toggle-btn [{:keys [name button-info on-click]}]
  (let [com (ui-toggle-button :img (image/load (:img-k button-info))
                              :button-info button-info
                              :on-click on-click)]
    (new-gatom name [com] :pos (:pos button-info))
    {name com}))



(defn- make-number [name info]
  (let [com (number info 0)]
    (new-gatom name [com] :pos (:pos info))
    {name com}))


(defn- make-payout-num [{:keys [name img-info n]}]
  (let [com (payout-number img-info n)]
    (new-gatom name [com] :pos (:pos img-info))
    {name com}))


(defn- make-multiplier [{:keys [name pos]}]
  (let [com (multiplier)]
    (new-gatom name [com] :pos pos)
    {name com}))


(defn- make-payout-flash [{:keys [name pos img-info]}]
  (let [com (m-payout-flash img-info)]
    (new-gatom name [com] :pos pos)
    {name com}))


(defn- make-payout-text [{:keys [name pos text-info num-info num-pos]}]
  (let [com (payout-text text-info :goodluck num-info num-pos)]
    (new-gatom name [com] :pos pos)
    {name com}))


;; =============
;; ui initalize!

(defn init-ui! []

  ;; backgrounds.
  (let [{:keys [img-k pos]} CONST/BG_BACKGROUND_INFO]
    (new-gatom "background" [(ui-image :image (image/load img-k))] :pos pos))

  (let [{:keys [img-k pos]} CONST/BG_JACKPOT_INFO]
    (new-gatom "background" [(ui-image :image (image/load img-k))] :pos pos))

  ;; (let [{:keys [img pos]} CONST/BG_REELS_INFO]
  ;;   (new-gatom "bg-reels" [(ui-image :img img)] :pos pos))

  (reset! ui*
          (merge (make-button :btn-spin CONST/UI_BTN_SPIN #(on-btn-click % :btn-spin))
                 (make-button :btn-line CONST/UI_BTN_LINE #(on-btn-click % :btn-line))
                 (make-button :btn-bet CONST/UI_BTN_BET #(on-btn-click % :btn-bet))
                 (make-button :btn-max-bet CONST/UI_BTN_MAX_BET #(on-btn-click % :btn-max-bet))
                 (make-button :btn-tolobby CONST/UI_BTN_TOLOBBY #(on-btn-click % :btn-tolobby))
                 (make-button :btn-paytable CONST/UI_BTN_PAYTABLE #(on-btn-click % :btn-paytable))

                 (make-toggle-btn {:name :btn-auto
                                   :button-info CONST/UI_BTN_AUTO
                                   :on-click (fn [ga down?]
                                               (if down?
                                                 (on-btn-click ga :btn-auto-on)
                                                 (on-btn-click ga :btn-auto-off)))})

                 (make-button :btn-freespin CONST/UI_BTN_FREESPIN #(on-btn-click % :btn-freespin))
                 (make-button :btn-freespin-stop CONST/UI_BTN_FREESPIN_STOP #(on-btn-click % :btn-freespin-stop))
                 (make-number :num-freespin CONST/UI_NUM_FREESPIN)

                 (make-number :num-line-view CONST/UI_NUM_LINE_VIEW)
                 (make-number :num-bet-view CONST/UI_NUM_BET_VIEW)
                 (make-number :num-total-bet CONST/UI_NUM_TOTAL_BET)
                 (make-number :num-jackpot CONST/UI_NUM_JACKPOT)
                 (make-number :num-chip CONST/UI_NUM_CHIP)

                 (make-payout-num {:name :money-payout
                                   :img-info CONST/UI_MONEY_PAYOUT
                                   :num 0})

                 (make-payout-flash {:name :payout-flash
                                     :pos [460 779]
                                     :img-info CONST/PAYOUT_FLASH_INFO})

                 (make-payout-text {:name :payout-text
                                    :pos [100 100]
                                    :text-info CONST/UI_PAYOUT_TEXT_INFO
                                    :num-pos [520 795]
                                    :num-info CONST/NUMS_RED_LARGE_IMG_INFO})

                 (make-multiplier {:name :multiplier
                                   :pos [555 255]})

                 {:reel-flash-manager (reel-flash-manager/new-reel-flash-manager)}
                 {:reel-manager (reel-manager/new-reel-manager)}
                 {:line-manager (line-manager/new-line-manager
                                 (reduce-kv (fn [acc k v]
                                              (let [line-num (bet-line-num k v
                                                                           #(line-manager/set-over (:line-manager @ui*) k true)
                                                                           #(line-manager/set-over (:line-manager @ui*) k false))
                                                    pos (:pos v)]
                                                (new-gatom "btn-line-num"
                                                           [line-num]
                                                           :pos pos)
                                                (assoc acc k {:line-num line-num})))
                                            {}
                                            CONST/BET_LINE_NUM_INFO_DIC))}
                 {:time-manager (time-manager/new-time-manager)})))
