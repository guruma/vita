(ns vita.scene.loading.core
  (:require-macros
   [cljs.core.async.macros :as m :refer [go]]
   [vita.config.macros :as vcm]
   [vita.logger :as vcl])
  (:require
   [cljs.core.async :as async :refer [<! >! chan close! sliding-buffer put! alts! take! timeout]]
   [milk.event :as event :refer [new-gatom]]
   [vita.scene.common.behaviour.ui-image :refer [ui-image]]
   [vita.scene.main-lobby.const :as CONST]
   [vita.app :as app]
   [vita.scene.common.behaviour.ui-animation-progress :refer [ui-animation-progress]]
   [vita.scene.common.behaviour.ui-label :refer [ui-label]]
   [vita.scene.common.behaviour.rotate-circle :refer [rotate-circle]]
   [vita.resource.image :as image]
   [vita.asset.loader]
   [sonaclo.util :as util]
   [vita.my]

   [sdk.facebook.core :as fb]
   ))


(def first?* (atom true))


(defn access-token-setting
  "비동기적으로, 페이스북을 통해 access-token 을 받아서 vita.my 에 저장한다"
  []
  (go
    (let [receive-login-token (fn [access-token]
                                (vcl/INFO "login token done! tk = " access-token)
                                (vita.my/set-login-token access-token))]

      (vcm/LEVEL
       :internal (receive-login-token (sonaclo.util/get-user-id))
       ;; (fb/init! (vcm/config:get-in [:api :facebook])
       ;;           receive-login-token)

       :live (fb/init! (vcm/config:get-in [:api :facebook])
                       receive-login-token)

       :qa (fb/init! (vcm/config:get-in [:api :facebook])
                     receive-login-token)))))


(defn loading-start [scene-name asset-loaded-callback]

  ;; 배경
  (new-gatom "bg-loading"
             [(ui-image :img (image/load :loading-back)
                        :order -100)]
             :pos [0 0])

  ;; 로딩 진행바, 로딩 라벨
  (let [image-dic {:start (image/load :loading-start)
                   :middle (image/load :loading-middle)
                   :end (image/load :loading-end)
                   :light (image/load :loading-light)}

        prg-loading (ui-animation-progress image-dic)
        lbl-loading (ui-label :text "0%"
                              :align :center
                              :style {:font "10pt Arial" :color :#696969})]

    (new-gatom "prg-loading"
               [prg-loading]
               :pos [286 646])

    (new-gatom "lbl-loading"
               [lbl-loading]
               :pos [650 690])

    ;; 로딩 진행바, 라벨 업데이트 함수
    (let [ch1-asset-load (chan 1)
          ch2-server-connect (chan 1)

          server-connect #(asset-loaded-callback [prg-loading lbl-loading] ch1-asset-load %)

          on-load-fn (fn [total cur]
                       (let [progress (/ cur total)
                             label (str (.round js/Math (* 100 (/ cur total))) "%")]

                         (aset prg-loading :progress progress)
                         (aset lbl-loading :text label)))

          load-end-fn (fn []
                        (aset lbl-loading :text "loading complete...")
                        (js/setTimeout #(aset lbl-loading :text "waiting from server...") 500)
                        (close! ch1-asset-load))

          asset-load-start #(vita.asset.loader/async-preload scene-name on-load-fn load-end-fn)
          ]

      ;; 로딩진행
      (server-connect asset-load-start) ;; 게임서버 커넥팅
      )))



;; main.
(defn load
  "asset-loaded-callback 리소스 로딩 후 실행되는 콜백함수"

  [scene-name asset-loaded-callback]

  (if @first?*

    (do (reset! first?* false)

        ;; 비동기로 페북 접근토큰 받기
        (access-token-setting)

        ;; 로딩 화면에 필요한 이미지 부터 로딩 loading -> common -> 해당 씬
        (image/preload :loading
                       (fn [& _])
                       (fn []
                         (image/preload :common
                                        (fn [& _])
                                        #(loading-start scene-name asset-loaded-callback)))))

    ;; 이후 로딩시에는 바로 다음 단계로 진입
    (loading-start scene-name asset-loaded-callback)))
