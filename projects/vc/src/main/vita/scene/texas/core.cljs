(ns vita.scene.texas.core
  (:require-macros
   [milk.macro :refer [def-behaviour letc]]
   [vita.logger :as vcl])
  (:require
   [milk.gameloop]
   [milk.event :as event]

   [vita.net.core :as network]
   [vita.poker.room :as r]
   [vita.poker.behaviour.receiver :refer [receiver]]
   [sonaclo.util :as util]
   [vita.poker.net.sender :as sender]
   [vita.poker.net.handler :as handler]
   [vita.poker.manager.buttonmanager :as btnm]
   [vita.poker.manager.roommanager :as rm]
   [vita.poker.manager.lobbymanager :as lm]
   [vita.poker.const.pos :refer [pos-map*]]
   [vita.poker.resource.audio]

   [vita.url :as URL]
   [vita.shared.const.err-code :as ERR]
   [vita.scene.common.bar.bar :as barm]
   [vita.resource.sound :as sound]
   [vita.scene.common.behaviour.loading :refer [loading]]
   [vita.scene.common.behaviour.key-cheat :refer [key-cheat]]
   [vita.app :as app]
   [vita.my :as my]
   [vita.shared.const.common :as COMMON]
   ))


(def sock [COMMON/TEXAS URL/APP_GAME])


(defn texas-init []

  (reset! vita.poker.net.handler/first-roomlist? true)
  (app/dialog:show {:make-fn (fn [] [(event/new-gatom "loading"
                                                      [(loading)]
                                                      :pos [400 400])])})

  ;; TODO(jdj) 포커로직에는 토큰을 room 에서 찾아가도록 되어있는데, my 에서 찾도록 변경해야함
  (r/set-sid (my/get-social-id))


  (sender/sock-init sock)

  ;; 서버-클라 통신용 리시버 생성
  (event/new-gatom "net-receiver"
                   [(receiver :dequeue #(network/dequeue sock)
                              :on-awake #(do (sender/v-send :account)
                                             (sender/v-send :roomlist))
                              :on-receive #(handler/handle %)
                              :on-close #(app/dialog:show {:make-fn (fn [] (vita.scene.common.dialog.err/dialog ERR/SOCKET_CLOSE vita.scene.common.dialog.util/page-reload))})
                              :on-error #(app/dialog:show {:make-fn (fn [] (vita.scene.common.dialog.err/dialog ERR/SOCKET_ERROR vita.scene.common.dialog.util/page-reload))})
                              :sock sock)]
                   :pos [20 20])

  ;; 로비 초기화
  (lm/lobby-init)


  ;; TODO(jdj) 삭제 및 옮길것..... 프리로딩에서 해야되는데... 일단 여기다....
  (sound/add-loads vita.poker.resource.audio/audio-paths)


  ;; 상단 메뉴 초기화
  (barm/bar-init :scene :texas
                 :sound-on #(sound/sound-on (fn [] (r/user-not-in-room?)) :bgm-main)
                 :sound-off sound/sound-off
                 :sock-close #(network/close sock))

  ;; key cheat
  (event/new-gatom "key-cheat"
                   [(key-cheat
                     ;; f :: fold
                     "f" #(when (btnm/btn-on? :btn-fold)
                            (btnm/game-play-btns-off)
                            (sender/v-send :fold))
                     ;; c :: call
                     "c" #(when (btnm/btn-on? :btn-call)
                            (btnm/game-play-btns-off)
                            (sender/v-send :call))
                     ;; r :: raise
                     "r" #(when (btnm/btn-on? :btn-raise)
                            (btnm/game-play-btns-off)
                            (sender/v-send :raise))
                     ;; e :: check
                     "e" #(when (btnm/btn-on? :btn-check)
                            (btnm/game-play-btns-off)
                            (sender/v-send :check))

                     ;; payment 패킷 보내기
                     "q" #(do
                            (vcl/WARN "qqqqqq")
                            (sender/v-send :payment))
                     )]
                   :pos [0 0])

  )


(defn scene []
  (r/init!)
  (r/set-user-id (:user-id (util/parse-query (.-href (.-location js/document)))))
  (texas-init)
  )


;; TODO(jdj) 삭제 개발코드 :: 게임 로비를 통해 들어왔을때는 사용하지 않는 함수
(defn ^:export main []
  (vcl/DEBUG "texas main start")
  (milk.gameloop/init! {:default scene})
  (milk.gameloop/start!))
