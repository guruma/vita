(ns vita.scene.main-lobby.core
  "main-lobby main scene."
  (:require-macros [vita.logger :as vcl])
  (:require
   [milk.event :as event :refer [new-gatom]]

   [milk.component.box-collider :as component.box-collider]

   ;; debug.
   [vita.scene.common.behaviour.fps :refer [fps]]
   [vita.scene.common.behaviour.ui-image :refer [ui-image]]
   [vita.scene.common.behaviour.ui-ani-images :refer [ui-ani-images]]
   [vita.scene.common.behaviour.ui-button :refer [ui-button]]
   [vita.scene.common.behaviour.ui-event :refer [ui-event]]
   [vita.scene.common.behaviour.ui-label :refer [ui-label]]

   [vita.app :as app]
   [vita.scene.common.behaviour.loading :refer [loading]]
   [vita.scene.common.dialog.buy-chips]
   [vita.scene.common.giftbox.core :as giftbox.core]
   [vita.scene.common.giftbox.count :as giftbox.count]
   [vita.resource.image :as image]
   [vita.resource.sound :as sound]
   [vita.scene.common.bar.bar :as bar]
   [vita.scene.common.rank-bar.core :as rank-bar]

   [vita.scene.main-lobby.networker :refer [networker]]
   [vita.scene.main-lobby.const :as CONST]
   [vita.scene.main-lobby.network :as network]
   [vita.shared.const.main-lobby :as PROTOCOL]
   [vita.helper]

   [sdk.facebook.core :as fb]

   ))



;; main.
(defn scene []

  (vita.scene.main-lobby.net.r-login/reset-first)

  (app/dialog:show {:make-fn (fn [] [(new-gatom "" [(loading)] :pos [400 400])])})

  (new-gatom "bg-main-lobby"
             [(ui-image :img-k CONST/BG_MAIN_LOBBY_IMG_K :order -100)]
             :pos [0 0])

  ;; 게임 버튼들
  (let [{:keys [pos]} CONST/UI_GAME_1]
    (new-gatom "ui_game_1" ;; 위-왼쪽
               [(ui-button :button-info CONST/UI_GAME_1
                           :on-click #(vita.helper/load-scene! :texas))]
               :pos pos))


  (let [{:keys [pos]} CONST/UI_GAME_2]
    (new-gatom "ui_game_2" ;; 위-오른쪽
               [(ui-button :button-info CONST/UI_GAME_2
                           :on-click #(vita.helper/load-scene! :omaha))]
               :pos pos))


  (let [{:keys [pos]} CONST/UI_GAME_3]
    (new-gatom "ui_game_3" ;; 아래-왼쪽
               [(ui-button :button-info CONST/UI_GAME_3
                           :on-click #(vita.helper/load-scene! :omahahl))]
               :pos pos))


  (let [{:keys [pos]} CONST/UI_GAME_4]
    (new-gatom "ui_game_4" ;; 아래-오른쪽
               [(ui-button :button-info CONST/UI_GAME_4
                           :on-click #(vita.helper/load-scene! :slotmachine))]
               :pos pos))


  ;; (let [{:keys [img pos]} CONST/UI_BG_CONCURRENT_USERS]
  ;;   (new-gatom "fps"
  ;;              [(ui-image :img img)]
  ;;              :pos pos))


  ;; (new-gatom "fps"
  ;;            [(ui-button :button-info CONST/BTN_FREECOIN_INFO)]
  ;;            :pos (:pos CONST/BTN_FREECOIN_INFO))


  ;; (new-gatom "fps"
  ;;            [(ui-button :button-info CONST/BTN_INVITE_GET_COIN_INFO)]
  ;;            :pos (:pos CONST/BTN_INVITE_GET_COIN_INFO))


  ;;
  (new-gatom "btn_more_coin_info"
             [(ui-button :button-info CONST/BTN_MORE_COIN_INFO
                         :order 1
                         :on-click
                         #(app/dialog:show {:make-fn vita.scene.common.dialog.buy-chips/dialog})
                         )]
             :pos (:pos CONST/BTN_MORE_COIN_INFO))


  (let [{:keys [img-k pos]} CONST/UI_BG_MORE_COIN_ICON]
    (new-gatom "bg_more_coin_icon"
               [(ui-image :img (image/load img-k))]
               :pos pos))


  ;; (let [{:keys [img-ks pos]} CONST/UI_BG_HOT_WOMAN
  ;;       img-k (first img-ks)]
  ;;   (new-gatom "bg_hot_woman"
  ;;              [(ui-image :img (image/load img-k))]
  ;;              :pos pos))

  ;; 선물함 버튼
  (new-gatom "btn_giftbox"
              [(ui-button :button-info CONST/BTN_GIFTBOX_INFO
                          :on-click #(giftbox.core/q_gift_info)
                          )]
              :pos (:pos CONST/BTN_GIFTBOX_INFO))

  ;; ;; 페이스북 버튼
  ;; (new-gatom "btn_facebook"å
  ;;            [(ui-button :button-info CONST/BTN_PLUS_FACEBOOK_INFO
  ;;                        :on-click
  ;;                        (fn [] (fb/friends-invite
  ;;                                #(network/send PROTOCOL/Q_INVITE_FRIEND {})))
  ;;                        )
  ;;             (ui-label :text "+$125,000" :dx 50 :dy 89 :style {:color :#FFFFFF :font "9pt Arial"} :align :center)]
  ;;            :pos (:pos CONST/BTN_PLUS_FACEBOOK_INFO))

  ;; ;; 트위터 버튼
  ;; (new-gatom "btn_twitter"
  ;;            [(ui-button :button-info CONST/BTN_PLUS_TWITTER_INFO
  ;;                        :on-click #(vcl/DEBUG "click twitter!")
  ;;                        )
  ;;             (ui-label :text "+$125,000" :dx 50 :dy 89 :style {:color :#FFFFFF :font "9pt Arial"} :align :center)]
  ;;            :pos (:pos CONST/BTN_PLUS_TWITTER_INFO))


  ;; 통신 전용 가톰 생성
  (event/new-gatom "networker" [(networker)])


  ;; 상단바 초기화
  (bar/bar-init :scene :main-lobby
                :sound-on #(sound/sound-on (fn [] true) :bgm-main)
                :sound-off sound/sound-off
                :sock-close #(vita.scene.main-lobby.network/close))


  ;; 하단 친구목록 초기화
  (rank-bar/init network/send))
