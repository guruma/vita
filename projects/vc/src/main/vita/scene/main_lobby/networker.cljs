(ns vita.scene.main-lobby.networker
  (:require-macros [milk.macro :refer [def-behaviour letc]]
                   [vita.logger :as vcl])
  (:require
   [milk.gatom :as gatom]
   [vita.scene.main-lobby.network :as network]
   [vita.shared.const.err-code :as ERR]

   [vita.my]
   [vita.app :as app]
   [vita.helper]
   [vita.scene.common.dialog.err :as dialog.err]
   [vita.scene.common.dialog.util :as dialog.util]

   ;; protocol
   [vita.scene.main-lobby.net.r-login :as protocol.login]
   [vita.scene.main-lobby.net.r-join-lobby :as protocol.join-lobby]
   [vita.scene.main-lobby.net.r-timeout :as protocol.timeout]
   [vita.scene.main-lobby.net.r-event :as protocol.event]
   [vita.scene.main-lobby.net.r-account :as protocol.account]
   [vita.scene.main-lobby.net.r-payment :as protocol.payment]
   [vita.scene.main-lobby.net.r-catalog :as protocol.catalog]
   [vita.scene.main-lobby.net.r-friendlist :as protocol.friendlist]
   [vita.scene.main-lobby.net.r-gift :as protocol.gift]
   [vita.shared.const.main-lobby :as PROTOCOL]
   ))

;;------------------
;; protocol

(def ^:private protocol-dic*
  (atom {PROTOCOL/R_LOGIN protocol.login/login
         PROTOCOL/R_JOIN_LOBBY protocol.join-lobby/join-lobby
         PROTOCOL/B_TIMEOUT protocol.timeout/timeout
         PROTOCOL/R_EVENT protocol.event/R-event
         PROTOCOL/R_FRIENDLIST protocol.friendlist/Rfriendlist
         PROTOCOL/R_ACCOUNT protocol.account/Raccount
         PROTOCOL/R_PAYMENT protocol.payment/Rpayment
         PROTOCOL/R_CATALOG protocol.catalog/Rcatalog
         PROTOCOL/R_GIFT_INFO protocol.gift/R_GIFT_INFO
         PROTOCOL/R_GIFT_COUNT protocol.gift/R_GIFT_COUNT
         PROTOCOL/R_GIFT_SEND protocol.gift/R_GIFT_SEND
         PROTOCOL/R_GIFT_ACCEPT protocol.gift/R_GIFT_ACCEPT}))


(defn- get-protocol [k]
  (get @protocol-dic* k (fn [& args] (vcl/ERROR "undefined cmd= %s in MAINLOBBY" k))))


(defn send-q-login
  []
  (->> {:login-token (vita.my/get-login-token) }
       (network/send PROTOCOL/Q_LOGIN)))


(defn send-q-join-lobby
  []
  (network/send PROTOCOL/Q_JOIN_LOBBY {}))


(def-behaviour networker []
  []

  :on-awake
  #(if-not (vita.my/is-loginned?)
     (do (send-q-login)
         (vita.my/set-friend-list)) ;VT-528 친구정보 여기에서 세팅
     (send-q-join-lobby))

  :update
  #(when-let [p (network/dequeue)]

     (case (:type p)

       :receive
       (let [packet (:val p)
             {:keys [err cmd data]} packet
             protocol (get-protocol cmd)]

         (if (= err ERR/ERR_NONE)
           (protocol data)
           (do
             ;; TODO(jdj) 리팩...
             ;; 에러코드를 한곳에서 처리하기 위해서 여기서 다이얼로그를 띄우도록 구조가 잡혀있는데,
             ;; Rjoin 을 받기전에 미니로딩으로 가려져있는 상태에서 에러코드가 오면, 미니로딩때문에 에러 다이얼로그 창이 잘 안보인다.
             ;; 그렇다고,
             ;; err 코드가 있는 패킷을 넘겨서 처리 시키면,
             ;; err 코드를 한곳에서 감지해서 처리하지 못하고 흩어진다.
             ;; 아마도 미니로딩 구조를 변경해야 할듯.
             (when (= cmd :Rjoin)
               (app/dialog:hide))

             (let [default-callback (fn [] (vita.helper/load-scene! :main-lobby))
                   callback (dialog.err/get-err-callback err default-callback)]
               (app/dialog:show {:make-fn (fn [] (dialog.err/dialog err callback))})))))

       :close
       (app/dialog:show {:make-fn (fn [] (dialog.err/dialog ERR/SOCKET_CLOSE
                                                            (fn [] (dialog.util/page-reload))))})

       :error
       (app/dialog:show {:make-fn (fn [] (dialog.err/dialog ERR/SOCKET_ERROR
                                                            (fn [] (network/close) (dialog.util/page-reload))))})

       nil))

  ;; 원소켓 정책으로 인해서 씬마다 소켓 끊는 처리는 필요없어짐
  :on-destroy
  #_(do (vcl/DEBUG " ===== MAIN_LOBBY socket close!!! =====")
        (network/close))
  )
