(ns vita.scene.main-lobby.net.r-rank
  (:require
   [vita.my]
   [vita.scene.common.rank-bar.core :as rank-bar]
   [vita.scene.common.rank-bar.friendlist :as rank-bar.friendlist]
   ))


(defn R-rank
  [{:keys [page last-page? rank-list] :as data}]
  (let [first-page? (= page 1)]
    (rank-bar/set-cur-page page)
    (rank-bar/set-last-page last-page?)
    (rank-bar.friendlist/set-rank-list rank-list)

    ;; TODO
    ;; first-page?, last-page? 에 따른 페이지 버튼 비활성화....
    ))
