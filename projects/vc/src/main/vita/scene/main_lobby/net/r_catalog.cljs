(ns vita.scene.main-lobby.net.r-catalog
  (:require-macros [vita.logger :as vcl])
  (:require
   [vita.scene.main-lobby.network :as network]
   [vita.shared.const.main-lobby :as PROTOCOL]
   [vita.my :as my]
   [vita.scene.common.bar.bar :as barm]
   [vita.net.core :as net]
   [sdk.facebook.core :as fb]
   ))


(defn send-payment-result-to-game-server [result]
  (vcl/INFO "====== send-payment-result-to-game-server ======")
  (vcl/INFO result)
  (net/send {:cmd :Qpayment :data result}))


(defn Rcatalog [product-info]
  (fb/invoke-payment-dialog product-info send-payment-result-to-game-server))
