(ns vita.scene.main-lobby.net.r-timeout
  (:require
   [vita.my]
   [vita.app :as app]
   [vita.scene.common.dialog.timeout :as timeout]
   ))


(defn timeout
  [& _]
  (app/dialog:show {:make-fn #(timeout/dialog)}))
