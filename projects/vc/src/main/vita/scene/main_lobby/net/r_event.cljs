(ns vita.scene.main-lobby.net.r-event
  (:require-macros [vita.logger :as vcl])
  (:require
   [vita.my]
   [vita.app :as app]
   [vita.scene.common.dialog.freecoin]
   [vita.scene.common.event.core :as vita.event]
   [vita.scene.common.bar.bar :as barm]
   [vita.scene.main-lobby.network :as network]
   [vita.shared.const.event :as CONST_EVENT]
   [vita.shared.const.main-lobby :as PROTOCOL]))




(defn R-event
  [{:keys [uid account]}]

  (when account
    (let [{:keys [user game]} account]
      (vita.my/set-user user)
      (vita.my/set-game game)
      (barm/update-money)))

  (vita.event/unblock))
