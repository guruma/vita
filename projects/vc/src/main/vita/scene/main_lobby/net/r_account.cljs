(ns vita.scene.main-lobby.net.r-account
  (:require
   [vita.my :as my]
   [vita.app :as app]
   [vita.scene.common.bar.bar :as barm]
   ))


(defn Raccount [{:keys [account]}]
  (let [{:keys [user game]} account]
    (my/set-user user)
    (my/set-game game)
    (barm/update-money)))
