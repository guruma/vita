(ns vita.scene.main-lobby.net.r-login
  (:require-macros
   [cljs.core.async.macros :as m :refer [go]]
   [vita.logger :as vcl])
  (:require [vita.my]
            [vita.app :as app]
            [vita.scene.common.rank-bar.core :as rank-bar]
            [vita.scene.common.rank-bar.friendlist :as rank-bar.friendlist]
            [vita.scene.common.event.core :as vita.event]
            [cljs.core.async :as async :refer [<! >! chan close! sliding-buffer put! alts! take! timeout]]
            [vita.scene.common.giftbox.count :as giftbox.count]
            ))

(def retry-cnt 5)
(def retry-time 8000)
(def lp-time (atom nil))


(defn timer-end []
  (vcl/INFO "timer end!!!!!!!!!!!")
  (reset! lp-time nil))


(defn timer-init []
  (vcl/INFO "timer init")
  (reset! lp-time (.now js/Date)))


(defn timer-start [callback]
  (go
    (vcl/INFO "!!!!!!! timer start")
    (loop [i retry-cnt]
      (vcl/INFO "loop")
      (<! (timeout retry-time))

      (if (<= i 0)
        (timer-end)
        (when @lp-time
          (callback)
          (timer-init)
          (recur (dec i)))))))



(def first*? (atom true))

(defn reset-first []
  (reset! first*? true))

(defn login
  [data]

  (if @first*?
    (do
      (reset! first*? false)

      (timer-end)

      (let [{:keys [tk sid account friendlist events]} data
            {:keys [user game]} account]

        (app/dialog:hide)
        (vita.my/set-social-id sid)
        (vita.my/set-token tk)
        (vita.my/set-user user)
        (vita.my/set-game game)

        (giftbox.count/q_gift_count)

        (let [{:keys [friends max-page]} friendlist]
          (rank-bar/set-max-page max-page)
          (rank-bar/page-btn-check 1 max-page)
          (rank-bar.friendlist/set-friend-list friends))


        (when events
          (vita.event/run events))

        (vita.my/set-is-loginned? true)
        ))

    (vcl/WARN "already Rjoin received.")))
