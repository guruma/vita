(ns vita.scene.main-lobby.net.r-friendlist
  (:require
   [vita.scene.common.rank-bar.core :as rank-bar]
   [vita.scene.common.rank-bar.friendlist :as rank-bar.friendlist]
   ))


(defn Rfriendlist [{:keys [friends cur-page max-page]}]
  (rank-bar/set-cur-page cur-page)
  (rank-bar/set-max-page max-page)
  (rank-bar/page-btn-check cur-page max-page)
  (rank-bar.friendlist/set-friend-list friends)
  )
