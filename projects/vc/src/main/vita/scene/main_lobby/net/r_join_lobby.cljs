(ns vita.scene.main-lobby.net.r-join-lobby
  (:require-macros
    [cljs.core.async.macros :as m :refer [go]]
    [vita.logger :as vcl])
  (:require [vita.my]
            [vita.app :as app]
            [vita.scene.common.rank-bar.core :as rank-bar]
            [vita.scene.common.rank-bar.friendlist :as rank-bar.friendlist]
            [vita.scene.common.event.core :as vita.event]
            [cljs.core.async :as async :refer [<! >! chan close! sliding-buffer put! alts! take! timeout]]

            [vita.scene.common.giftbox.count :as giftbox.count]
            ))

(defn join-lobby
  [data]

  (let [{:keys [account friendlist]} data
        {:keys [user game]} account]

    (app/dialog:hide)
    (vita.my/set-user user)
    (vita.my/set-game game)


    ;; 선물함 accept count
    (giftbox.count/q_gift_count)

    (let [{:keys [friends max-page]} friendlist]
      (rank-bar/set-max-page max-page)
      (rank-bar/page-btn-check 1 max-page)
      (rank-bar.friendlist/set-friend-list friends))
    )
  )
