(ns vita.scene.main-lobby.net.r-gift
  (:require-macros [vita.logger :as vcl])
  (:require
    [vita.scene.main-lobby.network :as network]
    [vita.shared.const.main-lobby :as PROTOCOL]
    [vita.my :as my]

    [vita.scene.common.giftbox.core :as giftbox.core]
    [vita.scene.common.giftbox.count :as giftbox.count]
    ))


(defn R_GIFT_INFO [data]
  (giftbox.core/r_gift_info data))

(defn R_GIFT_COUNT [{:keys [count] :as data}]
  (vcl/DEBUG "r_gift_count: data-> " data " count-> " count)
  (giftbox.count/set_gift_count count))

(defn R_GIFT_SEND [_])

(defn R_GIFT_ACCEPT [_])