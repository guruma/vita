(ns vita.scene.main-lobby.net.r-freecoin
  (:require
   [vita.my]
   [vita.app :as app]
   [vita.scene.common.dialog.freecoin]
   [vita.scene.main-lobby.network :as network]
   [vita.shared.const.main-lobby :as PROTOCOL]))


(defn freecoin
  [{:keys [event-type money]}]

  (when (and event-type money)
    (let [f (fn [] (vita.scene.common.dialog.freecoin/dialog event-type
                                                money
                                                #(network/send PROTOCOL/Q_FREECOIN {})))]


      (app/dialog:show {:make-fn f}))))
