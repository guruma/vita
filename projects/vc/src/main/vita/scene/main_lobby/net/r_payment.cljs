(ns vita.scene.main-lobby.net.r-payment
  (:require-macros [vita.logger :as vcl])
  (:require
   [vita.scene.main-lobby.network :as network]
   [vita.shared.const.main-lobby :as PROTOCOL]
   [vita.my :as my]
   [vita.scene.common.bar.bar :as barm]
   [vita.scene.common.dialog.buy_result :as buy_result]
   ))


(defn Rpayment [{:keys [status account]}]
  (vcl/INFO (str "Rpayment result: " status))
  (if (= status "completed")
    (let [{:keys [user game]} account]
      (my/set-user user)
      (my/set-game game)
      (barm/update-money)
      (buy_result/show (:money user)))))
