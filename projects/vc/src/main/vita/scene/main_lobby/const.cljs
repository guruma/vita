(ns vita.scene.main-lobby.const
  "클라이언트 전용 상수."
  (:require
   [vita.url :as URL]
   [vita.resource.image :as image]
   [vita.shared.const.common :as COMMON]))

;; helper

(defn- item-table [n [w h]]
  (->> (iterate (partial + w) 0)
       (take n)
       (mapv (fn [x] [x 0 w h]))))


(defn- button-table [[tw h]]
  (let [w (/ tw 4)
        [normal over down disable] (item-table 4 [w h])]
    {:normal normal :over over :down down :disable disable}))



;; network

(def SOCKET [COMMON/MAIN_LOBBY URL/APP_GAME])

(def BG_MAIN_LOBBY_IMG_K :mainlobby-back)

(def UI_GAME_1
  {:img-k :game-texas :table (button-table [1408 260]) :pos [279 136]})

(def UI_GAME_2
  {:img-k :game-omaha :table (button-table [1408 260]) :pos [646 136]})

(def UI_GAME_3
  {:img-k :game-omahahl :table (button-table [1408 260]) :pos [279 407]})

(def UI_GAME_4
  {:img-k :game-9slot :table (button-table [1408 260]) :pos [646 407]})

(def UI_BG_CONCURRENT_USERS
  {:img-k :mlob-conusers
   :pos [845 783]})

(def UI_ANI_TOPGAME
  {:img-ks [:game-9slot]
;[:topgame_texs_01 :topgame_texs_02 :topgame_texs_03 :topgame_texs_04 :topgame_texs_05]
   :pos [494 119]
   :w 550 :h 330})

(def UI_ANI_DOWNGAME_1
  {:img-ks [:game-texas]
   :pos [494 458]
   :w 180 :h 250})

(def UI_ANI_DOWNGAME_2
  {:img-ks [:game-omaha]
   :pos [679 458]
   :w 180 :h 250})

(def UI_ANI_DOWNGAME_3
  {:img-ks [:game-omahahl]
   :pos [864 458]
   :w 180 :h 250})


;; (def UI_BTN_FREIND_RANK)
;; (new-gatom "fps" [(ui-image :path
;;                             "/img/main_lobby/mlob_fr.png"
;;                             )] :pos
;;                                [237 780]
;;                                )

(def BTN_FREECOIN_INFO
  {:img-k :mlob-freecoin :table (button-table [656 30]) :pos [896 725]})


(def BTN_INVITE_GET_COIN_INFO
  {:img-k :mlob-invite :table (button-table [304 36]) :pos [563 919]})


(def BTN_MORE_COIN_INFO
  {:img-k :mlob-morecoin :table (button-table [1108 20]) :pos [502 790]})


(def SEND_GIFT_BTN_INFO
  {:img-k :mlob-send-gift :table (button-table [336 20])})


(def UI_BG_MORE_COIN_ICON
  {:img-k :mlob-morecoin-c
   :pos [609 745]
   ;;:pos [609 732]
   })


;; (def UI_BG_HOT_WOMAN
;;   {:img-ks [:mlob-woman
;;             :mlob-woman2]
;;    :pos [100 100]})


(def BTN_FACEBOOK_INVITE_INFO
  {:img-k :btn-facebook-invite
   :table (button-table [308 30])
   :pos [1006 70]})

(def BTN_TWITTER_INVITE_INFO
  {:img-k :btn-twitter-invite
   :table (button-table [308 30])
   :pos [1006 70]})

(def BTN_PLUS_FACEBOOK_INFO
  {:img-k :btn-plus-facebook
   :table (button-table [400 105])
   :pos [944 704]})

(def BTN_PLUS_TWITTER_INFO
  {:img-k :btn-plus-twitter
   :table (button-table [400 105])
   :pos [843 704]})

(def BTN_GIFTBOX_INFO
  {:img-k :btn-giftbox
   :table (button-table [240 62])
   :pos [237 720]})
