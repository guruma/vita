(ns vita.config.macros
  (:require
   [vita.config.loader :as loader]))


(def config*
  (loader/load-config :vc-config))


(defmacro config:get-in [get-v]
  (get-in config* get-v))


(defmacro DEBUG [& body]
  (when (get config* :debug)
    `(do
       ~@body)))


(defmacro LEVEL [& body]
  (let [level
        (get config* :level)

        cond-dic
        (->> `~body
             (partition 2)
             (reduce (fn [acc [f s]] (assoc acc f s)) {}))]
    (get cond-dic level)))
