(ns vita.config.loader
  (:require
   [clojure.java.io :as io]
   [environ.core :as env]
   [clojure.tools.reader.edn :as edn]))


(defn- file-exist? [fpath]
  (-> fpath
      (io/as-file)
      (.exists)))


(defn- get-config [config-fname]
  (-> config-fname
      (slurp)
      (edn/read-string)))


(defn load-config [env-config]
  (let [fpath (env/env env-config)]
    (println "vita config file: " fpath)
    (println (slurp fpath))

    (when-not (some? fpath)
      (throw (Exception. "fpath not found" env-config)))

    (when-not (file-exist? fpath)
      (throw (Exception. (str "file not exiest: " fpath))))

    (get-config fpath)))
