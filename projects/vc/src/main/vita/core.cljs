(ns vita.core
  (:require-macros [vita.logger :as vcl])
  (:require
   [milk.gameloop]
   [vita.unvalid-browser :as ub]

   [vita.helper]
   [vita.scene.main-lobby.core]
   [vita.scene.slotmachine.core]
   [vita.scene.texas.core]
   [vita.scene.omaha.core]
   [vita.scene.omahahl.core]

   ))


(let [running?* (atom false)]

  (defn run []
    (when-not @running?*
      (reset! running?* true)
      (let [scene-dic {:default #(vita.helper/load-scene! :main-lobby)
                       :loading vita.helper/loading->scene
                       :main-lobby vita.scene.main-lobby.core/scene
                       :slotmachine vita.scene.slotmachine.core/scene
                       :texas vita.scene.texas.core/scene
                       :omaha vita.scene.omaha.core/scene
                       :omahahl vita.scene.omahahl.core/scene}]
        (milk.gameloop/init! scene-dic)
        (milk.gameloop/start!)))))


;; main.
(defn ^:export main []

  (if-not (ub/supported-browser?)
    (ub/unvalid-warn "Unsupported browser!")
    (run)))
