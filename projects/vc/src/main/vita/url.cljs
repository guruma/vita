(ns vita.url
  (:require-macros
   [vita.config.macros :as cm]))


(def ^:const APP_SERVER (cm/config:get-in [:va :url]))

(def ^:const APP_GAME (str APP_SERVER "/game/"))


;; TODO 삭제 예정...
;; App.
(def ^:const APP_MAIN_LOBBY (str APP_SERVER "/game/main-lobby"))
(def ^:const APP_SLOTMACHINE (str APP_SERVER "/game/slotmachine"))
(def ^:const APP_TEXAS (str APP_SERVER "/game/texas"))
(def ^:const APP_OMAHA (str APP_SERVER "/game/omaha"))
(def ^:const APP_OMAHAHL (str APP_SERVER "/game/omahahl"))
