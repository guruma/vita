(ns vita.log
  (:require [philos.cljs.debug :refer-macros [dbg clog break]]))


;; css const style
(def DEBUG {:color :#000000})
(def INFO  {:color :#00bfFf})
(def WARN  {:color :#ff7f50 :font-size 13})
(def ERROR {:color :#FF0000 :font-size 13})
(def NOTICE {:color :#8a2be2 :font-size 13})


(defn parse-args [args]
  (loop [args args parsed {:-text []}]
    (if (seq args)
      (let [v (first args)]
        (cond

         (= :-css v)
         (let [v2 (second args)]
           (recur (drop 2 args) (assoc parsed v v2)))

         (contains? #{:-o :-raw} v)
         (recur (drop 1 args) (assoc parsed v true))

         :else
         (recur (drop 1 args) (update-in parsed [:-text] conj v))))
      parsed)))


(defn css->%c
  " input  => {:color :#FFFFFF :font-size 15}
    output => ';color:#FFFFFF;font-size:15pt' "
  [css]
  (loop [%c "" css css]
    (if (seq css)
      (let [[k v] (first css)]
        (recur (str %c
                    ;; 필요시 확장할 것... 현재는 color, font-size 만 설정함
                    (case k
                      :color     (str ";color:" (name v))
                      :font-size (str ";font-size:" v "px")
                      ""))
               (dissoc css k)))
      %c)))


(defn extract-%
  " input  => 'this is test %c hello %s world %c'
    output => (%c, %s, %c) "
  [text]
  (->> (partition 2 1 text)
       (filter #(= \% (first %)))
       (map (fn [[c1 c2]] (str c1 c2)))))


(defn match-change-css-to-%c
  "%c 에 들어올 인자들에 대해 css->%s 함수를 적용시킨다."
  [text]
  (let [[fmt fmt-args] (let [first-arg (first text)]
                         (if (= (type first-arg) (type ""))
                           [first-arg (rest text)]
                           ["" text]))
        %list (extract-% fmt)

        ;; 포맷팅 이스케이프 문자 부족시에는 %s 로 채워준다.
        d (- (count fmt-args) (count %list))
        fmt (apply str (concat fmt (repeat d " %s")))
        %list (concat %list (repeat d "%s"))

        f (fn [v c]
            (cond (= c "%c")
                  (css->%c v)
                  (= c "%s")
                  (str v)
                  :else
                  v))]
    (concat [fmt] (map f fmt-args %list))))


(defn vclog [& args]

  (try

    (let [level (first args)
          parsed (parse-args (rest args))

          text (:-text parsed)
          log-args' (match-change-css-to-%c text)
          ;; log-args' (if (or (= level :ERROR) (:-raw parsed))
          ;;             log-args''
          ;;             (map str log-args''))
          log-args (if-let [css (:-css parsed)]
                     (concat [(str "%c" (first log-args'))]
                             [(css->%c css)]
                             (rest log-args'))
                     (concat [(str "%c" (first log-args'))]
                             [(css->%c (condp = level
                                         :DEBUG DEBUG
                                         :INFO INFO
                                         :WARN WARN
                                         :ERROR ERROR
                                         :NOTICE NOTICE))]
                             (rest log-args')))]


      (if (= level :ERROR)
        (.apply js/console.error js.console (into-array log-args))
        (.apply js/console.log js.console (into-array log-args))))

    ;;(.apply js/console.log js.console (into-array ["%c %chello, %cworld" "color:red;" "color:blue;" "color:green;"]))

    (catch js/Error err
      (.error js/console "ERROR!! vclog format error, args => %s" args))))
