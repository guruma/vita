(ns vita.const
  (:require [vita.url :as URL]))


(def URL_DIC
  {:main-lobby URL/APP_GAME
   :slotmachine URL/APP_GAME
   :texas URL/APP_GAME
   :omaha URL/APP_GAME
   :omahahl URL/APP_GAME})
