(ns vita.my
  (:require-macros [cljs.core.async.macros :as m :refer [go]]
                   [vita.config.macros :as vcm]
                   [vita.logger :as vcl])
  (:require [cljs.core.async :as async :refer [<! >! chan close! put! alts! take! timeout pub sub unsub]]
            [vita.log :as glog :refer [vclog]]
            [sonaclo.util :as util]
            [sdk.facebook.core :as fb])
  )

(def ^:private my*
  (atom {:token ""
         :social-id nil
         :game nil
         :user nil}))


;;-------------------
;; user 정보 갱신시 불리는 콜백 리스트

(def cb* (atom []))


;; 콜백 추가
(defn add-callback [f]
  (swap! cb* conj f))


;; login token 얻었음을 알려주는 채널 (로그인시 확인을 위한 용도)
(def login-token-ch* (chan 1))


;;--------------------
;; set

;; Login token
(defn set-login-token [login-token]
  (swap! my* assoc :login-token login-token)
  (go (>! login-token-ch* true)))


(defn get-login-token []
  (:login-token @my*))


(defn set-token [token]
  (swap! my* assoc :token token))


(defn set-social-id [social-id]
  (vcl/DEBUG "[set-social-id] = %c%s" {:color :red} social-id)
  (swap! my* assoc :social-id social-id))


(defn -run-callbacks []
  ;; user 정보에 bar 관련해서 콜백 실행
  (doseq [f @cb*]
    (f)))


(defn set-user [user]
  (vcl/DEBUG "[set-user] :: " user)
  (swap! my* assoc :user user)
  (-run-callbacks))


(defn set-game [game]
  (vcl/DEBUG "[set-game] :: " game)
  (swap! my* assoc :game game))


(defn add-money [money]
  (swap! my* update-in [:user :money] #(+ % money))
  (-run-callbacks))


(defn set-is-loginned? [is-loginned?]
  (swap! my* assoc :is-loginned? is-loginned?))

(defn add-freespin-count [count]
  (swap! my* update-in [:game :slotmachine :freespin-count] #(+ % count)))

;;-------------------
;; get

(defn get-token []
  (:token @my*))


(defn get-social-id []
  (get @my* :social-id))


(defn get-name []
  ;; 이름 길이 제한
  (util/ellipsis (get-in @my* [:user :name]) 17))


(defn get-money []
  (get-in @my* [:user :money]))


(defn get-avatar-url []
  (get-in @my* [:user :avatar-url]))


(defn is-loginned? []
  (get @my* :is-loginned?))

(defn get-freespin-count []
  (:freespin-count (:slotmachine (:game @my*))))

;;-------------------
;; friendlist

(def friend-list* (atom []))

(defn add-friend-list-callback [data]
  (swap! friend-list* concat data))

(defn set-friend-list []
  (reset! friend-list* [])

  (vcm/LEVEL
  :live (do
          (fb/get-friends add-friend-list-callback false)
          (fb/get-friends add-friend-list-callback true))
  :qa (do
        (fb/get-friends add-friend-list-callback false)
        (fb/get-friends add-friend-list-callback true))
  :internal nil))

(defn get-friend-list []
  @friend-list*)
