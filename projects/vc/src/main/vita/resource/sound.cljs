(ns vita.resource.sound
  (:require-macros [vita.logger :as vcl])
  (:require [vita.resource.audios :as aud]
            [js.audio :as snd]))


(def paths
  {
   ;; 공통
   :click-1 "sound/common/click-1.mp3"
   :click-2 "sound/common/click-2.mp3"
   :click-3 "sound/common/click-3.mp3"
   :click-4 "sound/common/click-4.mp3"

   ;; 포커
   ;; :card-open "sound/card/card-open.mp3"
   ;; :card-deal "sound/card/card-deal.mp3"

   ;; :chip-return-1 "sound/card/chip-return-1.mp3"
   ;; :chip-return-2 "sound/card/chip-return-2.mp3"
   ;; :chip-stack "sound/card/chip-stack.mp3"

   ;; :call "sound/card/voice-call.mp3"
   ;; :check "sound/card/voice-check.mp3"
   ;; :fold "sound/card/voice-fold.mp3"
   ;; :raise "sound/card/voice-raise.mp3"

   ;; ;; 슬롯머신
   ;; :bingo-1 "sound/slotmachine/bingo-1.mp3"
   ;; :bingo-2 "sound/slotmachine/bingo-2.mp3"
   ;; :bingo-3 "sound/slotmachine/bingo-3.mp3"
   ;; :chip-collect "sound/slotmachine/chip-collect.mp3"

   ;; :reel-spin-1 "sound/slotmachine/reel-spin-1.mp3"
   ;; :reel-spin-2 "sound/slotmachine/reel-spin-2.mp3"
   ;; :reel-spin-3 "sound/slotmachine/reel-spin-3.mp3"
   ;; :reel-spin-4 "sound/slotmachine/reel-spin-4.mp3"
   ;; :reel-stop-1 "sound/slotmachine/reel-stop-1.mp3"
   ;; :reel-stop-2 "sound/slotmachine/reel-stop-2.mp3"
   ;; :reel-stop-3 "sound/slotmachine/reel-stop-3.mp3"
   ;; :reel-stop-4 "sound/slotmachine/reel-stop-4.mp3"
   })


;;----------------------------
;; 로딩 및 초기화

(def ac (aud/audio-context #()))


;; 배경음악 세팅
(defn preload-bgm [scene]
  (case scene
    :slotmachine (aud/add-dom-audio ac :bgm-slotmachine "bgm-slotmachine")
    (aud/add-dom-audio ac :bgm-main "bgm-main")
    ))


;; 오디오 등록
(aud/add-audios ac paths)


(defn preload []
  (aud/preload ac))


(defn add-loads [paths]
  (aud/add-audios ac paths)
  (aud/preload ac))



;;--------------------
;; for bgm

(declare play)
(declare pause)


(def current-bgm* (atom nil))


(defn bgm-play [bgm-keyword]
  (reset! current-bgm* bgm-keyword)
  (play @current-bgm*))


(defn bgm-stop []
  (pause @current-bgm*))


;;-----------------------------
;; 공개 메서드

(def on? (atom true))


(defn play
  [sound]
  (if @on?
    (let [a (aud/audio ac sound)]
      (try
        (if (and a @a)
          (snd/play @a))
        (catch js/Error err (vcl/ERROR "%s SOUND ERROR! err = %o" sound err))))))


(defn pause
  [sound]
  (let [a (aud/audio ac sound)]
    (if (and a @a)
      (snd/pause @a))))


(defn sound-off
  []
  (reset! on? false)
  (bgm-stop))


(defn sound-on
  [bg-check bgm-keyword]
  (reset! on? true)
  (if (bg-check)
    (bgm-play bgm-keyword)))
