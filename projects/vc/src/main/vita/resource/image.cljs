(ns vita.resource.image
  (:require-macros [cljs.core.async.macros :as m :refer [go go-loop]]
                   [vita.logger :as vcl])
  (:require [vita.resource.images :as rimg]
            [cljs.core.async :as async :refer [<! >! chan close! sliding-buffer put! alts! take! timeout]]))


(def paths*
  {
   :loading  {:loading-back "img/loading/loading_back.png"
              :loading-start "img/loading/graph_start.png"
              :loading-middle "img/loading/graph_middle.png"
              :loading-end "img/loading/graph_end.png"
              :loading-light "img/loading/graph_light.png"
              :loading-mini "img/common/loading/loading_mini.png"
              }

   :common {:dialog-back "img/common/dialog_back.png"

            :freecoin-day "img/common/freecoin/freecoin_day.png"
            :freecoin-start "img/common/freecoin/freecoin_start.png"
            :gift-ok "img/common/freecoin/gift_ok.png"
            :df "img/m_slotmachine/df.png"
            :df-s "img/poker/df_s.png"

            :big-large "img/poker/big_large.png"

            :bar-musicon "img/bar/bar_musicon.png"
            :bar-musicoff "img/bar/bar_musicoff.png"
            :bar-buychips "img/bar/bar_buychips.png"
            :bar-max "img/bar/bar_max.png"
            :bar-help "img/bar/bar_help.png"
            :bar-short "img/bar/bar_short.png"
            :bar-shortgame "img/bar/bar_shortgame.png"
            :bar-player"img/bar/bar_player.png"

            :btn-ok  "img/poker/btn-ok.png"
            :btn-cancel  "img/poker/btn-cancel.png"

            :bar-back "img/bar/bar_back.png"
            :bar-logo "img/bar/bar_logo.png"
            :logo-texas "img/bar/bar_texas.png"
            :logo-omahahl "img/bar/bar_omahahl.png"
            :logo-omaha "img/bar/bar_omahapoker.png"
            :logo-default "img/bar/bar_logo.png"

            ;; buy chips dialog
            :buychips-back "img/common/buychips/buy_chips_back.png"
            :buychips-line "img/common/buychips/buy_chips_line.png"
            :buychips-buy-btn "img/common/buychips/buy_chips_buy_btn.png"
            :buychips-x-btn "img/common/buychips/buy_chips_x_btn.png"

            :blue-window-up "img/common/bluewindow/pop_up.png"
            :blue-window-m "img/common/bluewindow/pop_m.png"
            :blue-window-down "img/common/bluewindow/pop_down.png"
            :blue-btn-ok "img/common/bluewindow/pop_ok.png"

            ;; giftbox
            :giftbox-freespin "img/common/giftbox/freespin_mark.png"
            :giftbox-back "img/common/giftbox/giftbox_back.png"
            :giftbox-x-btn "img/common/buychips/buy_chips_x_btn.png"
            :giftbox-tab01 "img/common/giftbox/giftbox_tab01.png"
            :giftbox-mback "img/common/giftbox/giftbox_mback.png"
            :giftbox-accept "img/common/giftbox/giftbox_accept.png"
            :giftbox-go "img/common/giftbox/giftbox_go.png"

            :giftbox-tab02 "img/common/giftbox/giftbox_tab02.png"
            :giftbox-send-button "img/common/giftbox/giftbox_send_button.png"
            :giftbox-send-allfriends "img/common/giftbox/giftbox_send_allfriends.png"
            :giftbox-send-listback "img/common/giftbox/giftbox_send_listback.png"
            :giftbox-send-friendsback "img/common/giftbox/giftbox_send_friendsback.png"
            }

   :poker {:myturn "img/poker/mytime.png"
           :time-graph "img/poker/time_graph.png"
           :time-back "img/poker/time_back.png"

           :winani "img/poker/winani.png"

           :raise-w-non "img/poker/raise_w_non.png"
           :raise-w "img/poker/raise_w.png"

           :club-A "img/poker/card/c_a.png"
           :club-2 "img/poker/card/c_2.png"
           :club-3 "img/poker/card/c_3.png"
           :club-4 "img/poker/card/c_4.png"
           :club-5 "img/poker/card/c_5.png"
           :club-6 "img/poker/card/c_6.png"
           :club-7 "img/poker/card/c_7.png"
           :club-8 "img/poker/card/c_8.png"
           :club-9 "img/poker/card/c_9.png"
           :club-10 "img/poker/card/c_10.png"
           :club-J "img/poker/card/c_j.png"
           :club-Q "img/poker/card/c_q.png"
           :club-K "img/poker/card/c_k.png"

           :heart-A "img/poker/card/h_a.png"
           :heart-2 "img/poker/card/h_2.png"
           :heart-3 "img/poker/card/h_3.png"
           :heart-4 "img/poker/card/h_4.png"
           :heart-5 "img/poker/card/h_5.png"
           :heart-6 "img/poker/card/h_6.png"
           :heart-7 "img/poker/card/h_7.png"
           :heart-8 "img/poker/card/h_8.png"
           :heart-9 "img/poker/card/h_9.png"
           :heart-10 "img/poker/card/h_10.png"
           :heart-J "img/poker/card/h_j.png"
           :heart-Q "img/poker/card/h_q.png"
           :heart-K "img/poker/card/h_k.png"

           :diamond-A "img/poker/card/d_a.png"
           :diamond-2 "img/poker/card/d_2.png"
           :diamond-3 "img/poker/card/d_3.png"
           :diamond-4 "img/poker/card/d_4.png"
           :diamond-5 "img/poker/card/d_5.png"
           :diamond-6 "img/poker/card/d_6.png"
           :diamond-7 "img/poker/card/d_7.png"
           :diamond-8 "img/poker/card/d_8.png"
           :diamond-9 "img/poker/card/d_9.png"
           :diamond-10 "img/poker/card/d_10.png"
           :diamond-J "img/poker/card/d_j.png"
           :diamond-Q "img/poker/card/d_q.png"
           :diamond-K "img/poker/card/d_k.png"

           :spade-A "img/poker/card/s_a.png"
           :spade-2 "img/poker/card/s_2.png"
           :spade-3 "img/poker/card/s_3.png"
           :spade-4 "img/poker/card/s_4.png"
           :spade-5 "img/poker/card/s_5.png"
           :spade-6 "img/poker/card/s_6.png"
           :spade-7 "img/poker/card/s_7.png"
           :spade-8 "img/poker/card/s_8.png"
           :spade-9 "img/poker/card/s_9.png"
           :spade-10 "img/poker/card/s_10.png"
           :spade-J "img/poker/card/s_j.png"
           :spade-Q "img/poker/card/s_q.png"
           :spade-K "img/poker/card/s_k.png"

           :card-back-l "img/poker/card/card_back_l.png"
           :card-back-r "img/poker/card/card_back_r.png"
           :card-back-s "img/poker/card/card_back_s.png"
           :test "img/poker/test.png"

           :1kd "img/poker/coin_1kdown.png"
           :1km "img/poker/coin_1kmiddle.png"
           :1ku "img/poker/coin_1kup.png"
           :5kd "img/poker/coin_5kdown.png"
           :5km "img/poker/coin_5kmiddle.png"
           :5ku "img/poker/coin_5kup.png"
           :10kd "img/poker/coin_10kdown.png"
           :10km "img/poker/coin_10kmiddle.png"
           :10ku "img/poker/coin_10kup.png"
           :50kd "img/poker/coin_50kdown.png"
           :50km "img/poker/coin_50kmiddle.png"
           :50ku "img/poker/coin_50kup.png"
           :100kd "img/poker/coin_100kdown.png"
           :100km "img/poker/coin_100kmiddle.png"
           :100ku "img/poker/coin_100kup.png"

           :pot-back "img/poker/pot_back.png"

           :high-card "img/poker/hand_high.png"
           :one-pair "img/poker/hand_one.png"
           :two-pair "img/poker/hand_two.png"
           :three-of-a-kind "img/poker/hand_tri.png"
           :straight "img/poker/hand_stra.png"
           ;;:mountain "img/poker/hand_stra.png"
           :flush "img/poker/hand_flush.png"
           :full-house "img/poker/hand_full.png"
           :four-of-a-kind "img/poker/hand_four.png"
           ;;:quad "img/poker/hand_four.png"
           :straight-flush "img/poker/hand_sf.png"
           :royal-straight-flush "img/poker/hand_rsf.png"
           :low-card "img/poker/hand_low.png"

           :player "img/poker/player.png"
           :player-back "img/poker/player_back.png"

           :all-in "img/poker/player_allin.png"
           :bet "img/poker/player_bet.png"
           :big "img/poker/player_big.png"
           :call "img/poker/player_call.png"
           :check "img/poker/player_check.png"
           :fold "img/poker/player_fold.png"
           :post "img/poker/player_post.png"
           :raise "img/poker/player_raise.png"
           :small "img/poker/player_small.png"
           :tie "img/poker/player_tie.png"
           :winner "img/poker/player_winner.png"

           :lo-playnow "img/poker/poker_lobby/lo_playnow.png"
           :lo-backb "img/poker/poker_lobby/lo_backb.png"


           :lo-join "img/poker/poker_lobby/lo_join.png"
           :lo-left "img/poker/poker_lobby/lo_left.png"
           :lo-right "img/poker/poker_lobby/lo_right.png"
           :lo-see "img/poker/poker_lobby/lo_see.png"
           :join-cancel "img/poker/close.png"
           :close "img/poker/close.png"

           :stand-up "img/poker/standup.png"
           :to-lobby "img/poker/lobby.png"
           :auto-seat "img/poker/sit.png"
           :click-to-sit "img/poker/sit.png"
           :click-here "img/poker/clickhere.png"

           :btn-bet "img/poker/b_call.png"
           :btn-call "img/poker/b_call.png"
           :btn-raise "img/poker/b_raise.png"
           :btn-check "img/poker/b_check_chk.png"
           :btn-fold "img/poker/b_fold_chk.png"
           :btn-minus "img/poker/bet_minus.png"
           :btn-plus "img/poker/bet_plus.png"
           :btn-min "img/poker/bet_min.png"
           :btn-half "img/poker/bet_half.png"
           :btn-pot "img/poker/bet_pot.png"
           :btn-all-in "img/poker/bet_allin.png"

           :buyin-a "img/poker/buyin_a.png"
           :buyin-back "img/poker/buyin_back.png"


           :check-p "img/poker/check_v.png"
           :check-n "img/poker/check.png"

           :chat-back  "img/poker/chat_back.png"
           :chat-dealer "img/poker/chat_dealer_n.png"
           :chat-textbox "img/poker/chat_textbox.png"
           :chat-enter "img/poker/chat_enter.png"

           :lo-back "img/poker/poker_lobby/lo_back.png"
           :lo-player "img/poker/poker_lobby/lo_player.png"
           :lo-join-back "img/poker/poker_lobby/lo_join_back.png"
           :lo-table "img/poker/poker_lobby/lo_table.png"
           :poker-back "img/poker/poker_back.png"

           :d "img/poker/d.png"

           :player-winner "img/poker/player_winner.png"
           :player-tie "img/poker/player_tie.png"

           :cardreversion "img/poker/cardreversion.png"
           }

   :slotmachine {
                 :slot-main-back "img/m_slotmachine/main_back.png"
                 :jackpot "img/m_slotmachine/jackpot.png"
                 :icon-7b "img/m_slotmachine/icon_7b.png"
                 :icon-7y "img/m_slotmachine/icon_7y.png"
                 :icon-7g "img/m_slotmachine/icon_7g.png"
                 :icon-bar "img/m_slotmachine/icon_bar.png"
                 :icon-dbar "img/m_slotmachine/icon_dbar.png"
                 :icon-tbar "img/m_slotmachine/icon_tbar.png"
                 :icon-apple "img/m_slotmachine/icon_apple.png"
                 :icon-cherry "img/m_slotmachine/icon_cherry.png"
                 :icon-grape "img/m_slotmachine/icon_grape.png"
                 :icon-free "img/m_slotmachine/icon_free.png"
                 :icon-lemon "img/m_slotmachine/icon_lemon.png"
                 :icon-water "img/m_slotmachine/icon_water.png"

                 :spin "img/m_slotmachine/spin.png"
                 :lineplus "img/m_slotmachine/lineplus.png"
                 :slot-bet "img/m_slotmachine/bet.png"
                 :slot-lobby "img/m_slotmachine/lobby.png"
                 :paytable "img/m_slotmachine/paytable.png"
                 :slot-auto "img/m_slotmachine/auto.png"
                 :slot-maxbet "img/m_slotmachine/maxbet.png"

                 :bln4 "img/m_slotmachine/bln4.png"
                 :bln2 "img/m_slotmachine/bln2.png"
                 :bln8 "img/m_slotmachine/bln8.png"
                 :bln10 "img/m_slotmachine/bln10.png"
                 :bln6 "img/m_slotmachine/bln6.png"
                 :bln1 "img/m_slotmachine/bln1.png"
                 :bln7 "img/m_slotmachine/bln7.png"
                 :bln9 "img/m_slotmachine/bln9.png"
                 :bln3 "img/m_slotmachine/bln3.png"
                 :bln5 "img/m_slotmachine/bln5.png"

                 :bln12 "img/m_slotmachine/bln12.png"
                 :bln18 "img/m_slotmachine/bln18.png"
                 :bln14 "img/m_slotmachine/bln14.png"
                 :bln20 "img/m_slotmachine/bln20.png"
                 :bln11 "img/m_slotmachine/bln11.png"
                 :bln16 "img/m_slotmachine/bln16.png"
                 :bln17 "img/m_slotmachine/bln17.png"
                 :bln13 "img/m_slotmachine/bln13.png"
                 :bln19 "img/m_slotmachine/bln19.png"
                 :bln15 "img/m_slotmachine/bln15.png"

                 :bl1 "img/m_slotmachine/bl1.png"
                 :bl2 "img/m_slotmachine/bl2.png"
                 :bl3 "img/m_slotmachine/bl3.png"
                 :bl4 "img/m_slotmachine/bl4.png"
                 :bl5 "img/m_slotmachine/bl5.png"
                 :bl6 "img/m_slotmachine/bl6.png"
                 :bl7 "img/m_slotmachine/bl7.png"
                 :bl8 "img/m_slotmachine/bl8.png"
                 :bl9 "img/m_slotmachine/bl9.png"
                 :bl10 "img/m_slotmachine/bl10.png"
                 :bl11 "img/m_slotmachine/bl11.png"
                 :bl12 "img/m_slotmachine/bl12.png"
                 :bl13 "img/m_slotmachine/bl13.png"
                 :bl14 "img/m_slotmachine/bl14.png"
                 :bl15 "img/m_slotmachine/bl15.png"
                 :bl16 "img/m_slotmachine/bl16.png"
                 :bl17 "img/m_slotmachine/bl17.png"
                 :bl18 "img/m_slotmachine/bl18.png"
                 :bl19 "img/m_slotmachine/bl19.png"
                 :bl20 "img/m_slotmachine/bl20.png"

                 :freespin "img/m_slotmachine/freespin.png"
                 :freespin-stop "img/m_slotmachine/freespin_stop.png"
                 :freespin-num "img/m_slotmachine/freespin_num.png"

                 :goodluck "img/slotmachine/goodluck.png"
                 :youwon "img/slotmachine/youwon.png"
                 :oops "img/slotmachine/oops.png"
                 ;;:freespin "img/slotmachine/freespin.png"
                 :flash-horz "img/slotmachine/flash-horz.png"

                 :light-left-01 "img/m_slotmachine/light_left_01.png"
                 :light-left-02 "img/m_slotmachine/light_left_02.png"
                 :light-left-03 "img/m_slotmachine/light_left_03.png"
                 :light-left-04 "img/m_slotmachine/light_left_04.png"
                 :light-right-01 "img/m_slotmachine/light_right_01.png"
                 :light-right-02 "img/m_slotmachine/light_right_02.png"
                 :light-right-03 "img/m_slotmachine/light_right_03.png"
                 :light-right-04 "img/m_slotmachine/light_right_04.png"

                 :pay-back "img/m_slotmachine/pay_back.png"
                 :pay-close "img/m_slotmachine/pay_close.png"

                 :light-w "img/m_slotmachine/light_w.png"
                 :light-b "img/m_slotmachine/light_b.png"

                 :flash-vert "img/slotmachine/flash-vert.png"
                 :mult0 "img/slotmachine/mult0.png"
                 :mult1 "img/slotmachine/mult1.png"
                 :mult2 "img/slotmachine/mult2.png"
                 :mult3 "img/slotmachine/mult3.png"
                 :mult4 "img/slotmachine/mult4.png"
                 :mult5 "img/slotmachine/mult5.png"
                 :mult6 "img/slotmachine/mult6.png"
                 :mult7 "img/slotmachine/mult7.png"
                 :mult8 "img/slotmachine/mult8.png"
                 :mult9 "img/slotmachine/mult9.png"
                 :multx "img/slotmachine/multx.png"
                 }

   :main-lobby {
                :mainlobby-back "img/main_lobby/mainlobby_back.png"
                :game-9slot "img/main_lobby/game_9slot.png"
                :game-texas "img/main_lobby/game_texas.png"
                :game-omaha "img/main_lobby/game_omahap.png"
                :game-omahahl "img/main_lobby/game_omahahi.png"
                :mlob-player-02 "img/main_lobby/mlob_player_02.png"
                :mlob-player-01 "img/main_lobby/mlob_player_01.png"
                :mlob-tab-friend-ranking "img/main_lobby/mlob_fr.png"
                :mlob-side-left-edge "img/main_lobby/mlob_side_l.png"
                :mlob-side-right-edge "img/main_lobby/mlob_side_r.png"
                :mlob-side-left-page "img/main_lobby/mlob_side_up01.png"
                :mlob-side-right-page "img/main_lobby/mlob_side_up01_r.png"
                :mlob-side-left-page-end "img/main_lobby/mlob_side_up03.png"
                :mlob-side-right-page-end "img/main_lobby/mlob_side_up03_r.png"
                :mlob-send-gift "img/main_lobby/mlob_sendg.png"

                :mlob-conusers "img/main_lobby/mlob_conusers.png"
                :mlob-freecoin "img/main_lobby/mlob_freecoin.png"
                :mlob-invite "img/main_lobby/mlob_invite.png"
                :mlob-morecoin "img/main_lobby/mlob_morecoin.png"
                :mlob-morecoin-c "img/main_lobby/mlob_morecoin_c.png"
                ;; :mlob-woman "img/main_lobby/mlob_woman.png"
                ;; :mlob-woman2 "img/main_lobby/mlob_woman2.png"
                :btn-facebook-invite "img/main_lobby/mlob_sns_f_in.png"
                :btn-twitter-invite "img/main_lobby/mlob_sns_t_in.png"
                :btn-plus-facebook "img/main_lobby/plus_face.png"
                :btn-plus-twitter "img/main_lobby/plus_tw.png"
                :btn-giftbox "img/main_lobby/giftbox.png"

                :gift-count-01 "img/main_lobby/gift_count_01.png"
                :gift-count-02 "img/main_lobby/gift_count_02.png"
                :gift-count-03 "img/main_lobby/gift_count_03.png"

                ;; main-lobby ani.
                ;; :downgame_omaha_ori_01 "img/main_lobby/ani_game/downgame_omaha_ori_01.png"
                ;; :downgame_omaha_ori_02 "img/main_lobby/ani_game/downgame_omaha_ori_02.png"
                ;; :downgame_omaha_hl_01 "img/main_lobby/ani_game/downgame_omaha_hl_01.png"
                ;; :downgame_omaha_hl_02 "img/main_lobby/ani_game/downgame_omaha_hl_02.png"
                ;; :downgame_slot_01 "img/main_lobby/ani_game/downgame_slot_01.png"
                ;; :downgame_slot_02 "img/main_lobby/ani_game/downgame_slot_02.png"
                ;; :topgame_texs_01 "img/main_lobby/ani_game/topgame_texs_01.png"
                ;; :topgame_texs_02 "img/main_lobby/ani_game/topgame_texs_02.png"
                ;; :topgame_texs_03 "img/main_lobby/ani_game/topgame_texs_03.png"
                ;; :topgame_texs_04 "img/main_lobby/ani_game/topgame_texs_04.png"
                ;; :topgame_texs_05 "img/main_lobby/ani_game/topgame_texs_05.png"
                }})


;; cur scene path-id
(def id* (atom nil))


;; ex) {:path-id1 ic1
;;      :path-id2 ic2 ...}
(def ic-map* (atom nil))


(defn- scene->path-id
  [scene]
  (case scene
    (:texas :omaha :omahahl) :poker
    :default :main-lobby
    scene))


(defn reset-ic!
  "씬 관련 ic 세팅"

  [scene]
  (let [id (scene->path-id scene)]

    (reset! id* id)

    (when-not (get @ic-map* id)
      (let [ic (rimg/image-context)]

        ;; 관련 이미지들 등록
        (rimg/add-images ic (get paths* id))
        ;; ic 등록
        (swap! ic-map* assoc id ic)))))


(defn preload
  "on-load-callback  한개씩 로드 될따마다 호출
   end-callback      해당 씬 이미지 로드가 끝났을때 호출"

  [scene on-load-callback end-callback]

  ;; ic 갱신
  (reset-ic! scene)

  ;; 이미지 프리로드 시작
  (let [id (scene->path-id scene)
        ic (get @ic-map* id)]

    (rimg/preload ic)

    (go-loop [ch (rimg/on-load-ch ic)]
      ;;(.log js/console (str [(rimg/total-count ic) (rimg/loaded-count ic)]))

      (if (<= (rimg/total-count ic) (rimg/loaded-count ic))

        (do (on-load-callback 100 100)
            (end-callback))

        (do (<! ch)
            (on-load-callback (rimg/total-count ic) (rimg/loaded-count ic))
            (recur ch))))))


(defn load
  "특정한 키워드를 통해 이미지를 찾아온다"

  [k]
  (or (rimg/image (get @ic-map* @id*) k)
      (rimg/image (get @ic-map* :common) k)
      (rimg/image (get @ic-map* :loading) k)))



;;=========================
;; image 관련 util 함수

(defn img-loaded? [img]
  (and img @img (aget @img "loaded")))


(defn img-w-h [img]
  (if (img-loaded? img)
    [(.-width @img) (.-height @img)]))
