(ns vita.resource.audios
  (:require-macros
   [cljs.core.async.macros :as m :refer [go]]))


;; usage: see test/cljs/vita/resource_test.cljs

(defn audio-context
  "#on-load {f} onload callback funtion"
  [on-load]
  (atom {:on-load on-load
         :audios  {}} ))

(defn- make-element
  "#ac {atom<map>} audio context
   #src {string} audio source
   #return {HTMLAudioElement}

  (make-element \"audio/common/click-1.wav\")"
  [ac src on-load]
  (let [audio-el (js/Audio.)]
    (aset audio-el "preload" "none")
    (aset audio-el "src" src)
    audio-el))

(defn add-audio
  "#ac {atom<map>} audio context
   #k {keyword} the keyword to use instead of audio src
   #src {string} audio source
   #return {delay}

  (add-audio ac* :bgm-main \"audio/bgm/main.mp3\")"
  [ac k src]
  (swap! ac assoc-in [:audios k] (delay (let [audio-el (make-element ac src (get @ac :on-load))]
                                          (.load audio-el)
                                          audio-el))))

(defn add-audios
  [ac k-srcs]
  (doseq [[k src] k-srcs]
    (add-audio ac k src)))

(defn add-dom-audio
  "html 파일에 정적으로 선언된 audio dom element를 audio-context에 추가한다.
   #ac {atom<map>} audio context
   #k {keyword} the keyword to use instead of audio src.
   #dom-id {string} the dom id of audio file

   (add-dom-audio ac* :bgm-main \"bgm-main\")"
  [ac k dom-id]
  (let [dom-el (.getElementById js/document dom-id)]
    (aset dom-el "preload" "none")
    (swap! ac assoc-in [:audios k] (delay ((get @ac :on-load))
                                          dom-el)) ))

(defn audio
  "Gets an audio element corresponding to k.
   #ac {atom<map>} audio context
   #k {keyword} the keyword to use instead of image src
   #return {HTMLAudioElement}"
  [ac k]
  (get-in @ac [:audios k]))


(defn preload
  "Realizes all audio delays in ac
   #ac {atom<map>} audio context"
  [ac]
  (doseq [[k v] (get @ac :audios)]
    (go
      @v)))

(defn count-audios
  "Counts all audio elements in ac
   #ac {atom<map>} audio context"
  [ac]
  (count (get @ac :audios)))
