(ns vita.resource.images
  (:require-macros [cljs.core.async.macros :as m :refer [go]])
  (:require [cljs.core.async :as async :refer [<! >! chan close! sliding-buffer put! alts! take! timeout]]))


;; usage: see test/cljs/vita/resource_test.cljs

(defn image-context
  []
  (atom {:total-cnt 0
         :loaded-cnt 0
         :on-load-ch (chan) ;; TODO...채널은 다 쓰고 난 후 지우는 것이 나을듯...
         :images {}}))

(defn total-count [ic]
  (get @ic :total-cnt))

(defn loaded-count [ic]
  (get @ic :loaded-cnt))

(defn on-load-ch [ic]
  (get @ic :on-load-ch))

(defn- make-element
  "#ic {atom<map>} image context
   #src {string} image source
   #return {HTMLImageElement}

  (make-element ic* \"img/poker/check.png\")"
  [ic k src]
  (let [image-el (js/Image.)
        ch (:on-load-ch @ic)]
    (aset image-el "onload" #(do (aset image-el "loaded" true)
                                 (swap! ic update-in [:loaded-cnt] inc)
                                 (go (>! ch k))))
    (aset image-el "src" src)
    image-el))

(defn add-image
  "#ic {atom<map>} image context
   #k {keyword} the keyword to use instead of image src
   #src {string} image source
   #return {delay}

  (add-image ic* :check \"img/poker/check.png\")"
  [ic k src]
  (swap! ic update-in [:total-cnt] inc)
  (swap! ic assoc-in [:images k] (delay (make-element ic k src))))

(defn add-images
  "#ic {atom<map>} image context
   #k {keyword} the keyword to use instead of image src
   #src {string} image source
   #return {delay}

  (add-image ic* :check \"img/poker/check.png\")"
  [ic k-srcs]
  (doseq [[k src] k-srcs]
    (add-image ic k src)))

(defn image
  "#ic {atom<map>} image context
   #k {keyword} the keyword to use instead of image src
   #return {HTMLImageElement}"
  [ic k]
  (get-in @ic [:images k]))

(defn preload
  "Realizes all image delays in ic
   #ic {atom<map>} image context"
  [ic]
  (doseq [[k v] (get @ic :images)]
    (go
      @v)))
