(ns vita.logger
  (:require [vita.config.macros :refer [config*]]))

;; --------------------------------------------------
;;   < document >
;; this macro use [vita.log :refer [vclog]] function
;;
;;
;;   < logging level > :DEBUG < :INFO < :WARN < :ERROR < :NOTICE < :NONE
;; this property is set by vc-macro/config* that is set by dist/config/vc.edn
;;
;;
;;   < use case >
;; (vc-log/DEBUG "only text")
;;
;; (vc-log/DEBUG "text formatting, %s" "hello")
;;
;; (vc-log/DEBUG "total text css setting" :-css {:color :#FFFF00 :font-size 20})
;;
;; (vc-log/DEBUG "total text css setting" :-css glog/INFO)
;;
;; (vc-log/DEBUG "%c css setting" {:color :red})
;;
;; (vc-log/DEBUG "%c css1 %c css2 %s" {:color :blue} {:color :red} "hahaha")
;;
;; (vc-log/DEBUG "print object = %o" object)
;;
;; (vc-log/DEBUG "print object raw data = %o" object :-raw)
;;
;; --------------------------------------------------



(def levels [:DEBUG :INFO :WARN :ERROR :NOTICE :NONE])


(def available-levels
  ;; (ex) config:loggin-level :WARN
  ;;
  ;;      drop    |           set
  ;; :DEBUG :INFO | :WARN :ERROR :NOTICE :NONE
  (let [logging-level (get config* :logging-level)]
    (set (drop-while (fn [v] (not= logging-level v)) levels))))


(defmacro DEBUG [& args]
  (when (contains? available-levels :DEBUG)
    `(~'vita.log/vclog :DEBUG ~@args)
    ))


(defmacro INFO [& args]
  (when (contains? available-levels :INFO)
    `(~'vita.log/vclog :INFO ~@args)
    ))


(defmacro WARN [& args]
  (when (contains? available-levels :WARN)
    `(~'vita.log/vclog :WARN ~@args)
    ))


(defmacro ERROR [& args]
  (when (contains? available-levels :ERROR)
    `(~'vita.log/vclog :ERROR ~@args)
    ))


(defmacro NOTICE [& args]
  (when (contains? available-levels :NOTICE)
    `(~'vita.log/vclog :NOTICE ~@args)
    ))


;; ERROR-STYLE {:color :#FFFFFF :font "13pt Arial" :bgcolor :#F0F0F0}
;; (vcl/DEBUG "asdfsdaf" :-css ERROR-STYlE :-o)
