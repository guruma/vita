(ns sdk.facebook.core
  (:require-macros [vita.config.macros :as vcm]
                   [vita.logger :as vcl])
  (:require [ajax.core :refer [POST]]))


;; ref: https://developers.facebook.com/docs/javascript/



(defn my-print [s]
  (vcl/NOTICE s))



;;------------------------------------------------------------------------------
;; login & connect

(def permissions ["email" "user_friends"])


(defn- login
  [& {:keys [scope callback] :or {}}]
  ((aget js/FB "login") #(do (vcl/NOTICE "FB login resp = %o" %)
                             (if (and callback (= "connected" (.-status %)))
                               (callback)))
   (clj->js {:scope (->> (or scope
                             permissions)
                         (interpose ",")
                         (apply str))})))


(defn- permission-check-&-request []

  (let [api-path "/v2.0/me/permissions"
        handler (fn [res]
                  (vcl/NOTICE "permission resp = %o" res)
                  (let [p-list (js->clj (aget res "data"))
                        p-map (reduce (fn [acc m]
                                        (assoc acc
                                          (m "permission")
                                          (m "status")))
                                      {}
                                      p-list)
                        __ (vcl/NOTICE "permission check = %s " p-map)
                        not-granted (keep (fn [permission]
                                            (let [status (p-map permission)]
                                              (if-not (= status "granted") permission)))
                                          permissions)]

                    ;; 허가 안된 권한이 있으면 login 을 통해 요청한다
                    ;; https://developers.facebook.com/docs/facebook-login/permissions/v2.0
                    (when (seq not-granted)
                      (vcl/NOTICE "user's permission not-granted = %s, start login permission!" not-granted)
                      (login :scope not-granted))))]

    ((aget js/FB "api") api-path handler)))


(defn- get-access-token [& _]
  ;; 페이스북 로그인 상태일때 authResponse object 를 바로 가져온다
  (let [get-auth-resp (aget js/FB "getAuthResponse")]
    (aget (get-auth-resp) "accessToken")))


(defn- connected-process [callback]

  ;; 권한 체크 (없으면 요청)
  (permission-check-&-request)

  ;; access token 처리
  (callback (get-access-token))

  ;; 유저 access token 변경시 호출되는 콜백 등록
  ((aget js/FB.Event "subscribe") "auth.authResponseChange" #(callback (get-access-token))))


(defn- get-login-status [callback]

  ;; access token 얻어오기
  ;; https://developers.facebook.com/docs/reference/javascript/FB.getLoginStatus#response_and_session_objects

  ((aget js/FB "getLoginStatus") (fn [resp]
                                   (let [status (.-status resp)]
                                     (case status

                                       "connected"
                                       (do (vcl/NOTICE "connected process begin...")
                                           (connected-process callback)
                                           (vcl/NOTICE "connected process success!"))

                                       "not_authorized"
                                       (do
                                         ;; 앱 인증이 안되었음. 로그인을 통해 가입
                                         (vcl/NOTICE "loginstatus = not authorized")
                                         (login :callback #(connected-process callback)))

                                       "unknown"
                                       (do
                                         (vcl/NOTICE "loginstatus = unknown, resp = %o" resp)
                                         (login :callback #(connected-process callback)))

                                       ;; else
                                       (do
                                         (vcl/ERROR "FB get-login-status handler error! %o" resp)
                                         (login :callback #(connected-process callback))
                                         ))))))



(defn- fb-sdk-load [doc]
  (vcl/DEBUG "fb-sdk-load-start")
  (let [id "facebook-jssdk"
        element-id? (. doc (getElementById id))]

    (when-not element-id?

      (let [tag-name "script"
            js (. doc (createElement tag-name))]
        (set! (.-id js) id)
        (set! (.-async js) true)
        (set! (.-src js) "//connect.facebook.net/en_US/all.js")

        (let [ref (-> (. doc (getElementsByTagName tag-name))
                      (aget 0))]
          (. (.-parentNode ref)
             (insertBefore js ref)))))))




(defn init! [fb-opt callback]

  (vcl/NOTICE "init! called... (fb-opt: %s )" fb-opt)

  (let [fb-async-init (fn []
                        (my-print "Facebook Javascript SDK Completed !!!!")
                        ;; 페이스북 sdk 초기화
                        (let [fb-init-data (clj->js fb-opt)]
                          (js/FB.init fb-init-data))

                        ;; 로그인 상태 조회
                        (get-login-status callback))]

    ;; window 객체에 콜백함수 걸어넣고,
    (aset js/window "fbAsyncInit" fb-async-init)

    ;; sdk 다운로드 -> 완료되면 자동으로 콜백 호출한다.
    (fb-sdk-load js/document)
    ))


;;------------------------------------------------------------------------------
;; friend

(defn friends-request [sids message on-response]
  (let [ui (aget js/FB "ui")]

    (ui (clj->js {"method" "apprequests"
                  "message" message
                  ;;"filters" ["app_non_users"]
                  "to" sids
                  })
        (fn [resp]
          (vcl/DEBUG "REQUEST response = %o" resp)
          (if (and (nil? (aget resp "error_code"))
                   (aget resp "request"))
            ;; TODO 리팩: 아직 에러코드 확인 정도만 하고 넘어간다.
            (on-response (js->clj (aget resp "to")))))
        )))

(defn get-friends
  [callback is_invitable]
  (let [api-path (if is_invitable
                    "/v2.0/me/invitable_friends?fields=id,name,picture"
                    "/v2.0/me/friends?fields=id,name,picture")
        handler (fn [res]
                  (callback
                    (map #(assoc % "is_invitable" is_invitable)
                         ((js->clj res) "data"))))]
    ((aget js/FB "api") api-path handler)))

;;------------------------------------------------------------------------------
;; payment


;; (defn process-payment-result [resp code callback]
;;   (vcl/NOTICE resp)
;;   (if (= code :success)
;;     (let [result (cljs.reader/read-string resp)
;;           status (:status result)
;;           server-result (:server-status result)]
;;       (case server-result
;;         "completed" (callback result)
;;         "invalid-payment" (vcl/NOTICE "invalid-payment")
;;         "no-facebook-payment" (vcl/NOTICE "no-facebook-payment")
;;         "default"))))


;; (defn send-payment-result [payment-result callback]
;;   (my-print (str "send-payment-result: " payment-result))
;;   (POST "/payment/paid/client"
;;         {:params  (js->clj payment-result)
;;          :handler #(process-payment-result % :success callback)
;;          :error-handler #(process-payment-result % :fail callback)}))


(defn callback-for-payment-dialog
  "INVOKE facebook payment dialog resp! ::
  #js {:payment_id 575627982551052, :amount 0.30, :currency USD, :quantity 1,
  :request_id 123456, :status completed,
  :signed_request qfQ-ED04gqth_MGVFb9iO1oE1r9LnmHkR738aG7Wx6o.eyJhbGdvcml0aG0iOiJITUFDLVNIQTI1NiIsImFtb3VudCI6IjAuMzAiLCJjdXJyZW5jeSI6IlVTRCIsImlzc3VlZF9hdCI6MTQwMjQ3NDQ2MywicGF5bWVudF9pZCI6NTc1NjI3OTgyNTUxMDUyLCJxdWFudGl0eSI6IjEiLCJyZXF1ZXN0X2lkIjoiMTIzNDU2Iiwic3RhdHVzIjoiY29tcGxldGVkIn0}"

  [request-id callback resp]
  (let [res (js->clj resp)]
    (my-print (str "INVOKE facebook payment dialog resp! :: " (str res)))
    (let [status (res "status")
          payment-result (as-> res k
                               (if status k (assoc k "status" "canceled"))
                               (assoc k :request-id request-id))]
      (callback payment-result))))


(defn gen-callback-payment-dialog [request-id callback]
  (let [rid request-id]
    (fn [resp]
      (callback-for-payment-dialog rid callback resp))))


(defn invoke-payment-dialog [{:keys [request-id product-url quantity]} callback]
  (my-print (str "INVOKE facebook payment dialog :: "))
  (my-print (str "    request-id : " request-id))
  (my-print (str "    product-url : " product-url))
  (my-print (str "    quantity: " quantity))

  (let [ui (aget js/FB "ui")]
    (ui (clj->js {:method "pay"
                  :action "purchaseitem"
                  :product product-url
                  :quantity quantity
                  :request_id request-id})
        (gen-callback-payment-dialog request-id callback))))
