(ns js.audio
  "Audio element wrapper."
  (:require [sonaclo.shared.case :refer [camel-case]] ))

(defn- set-props
  "#elm {HTMLAudioElement}
   #properties {map} the properties of an HTML5 Audio element to set

   (set-props (sound :spin) {:loop true :current-time 0})"
  [elm properties]
  (doseq [[k v] properties]
    (aset elm (camel-case k) v) ))

(defn- get-prop
  "#elm {HTMLAudioElement}
   #k {keyword} the key of a property of an HTML5 Audio element to get

   (get-prop (sound :spin) :loop) => true"
  [elm k]
  (aget elm (camel-case k)))

(defn load
  "#elm {HTMLAudioElement}

   (load (sound :spin))"
  [elm]
  (.load elm))

(defn pause
  "#elm {HTMLAudioElement}

   (pause (sound :spin))"
  [elm]
  (try
    (.pause elm)
    (catch js/Error err
      (milk.exception/err-handle :info (str "sound pause ERROR " elm)
                                 :err err))))

(defn reset
  "Pauses and resets sound.
   #el {HTMLAudioElement}

   (reset (sound :spin))"
  [elm]
  (try
    (do
      (pause elm)
      (set-props elm {:current-time 0}))
    (catch js/Error err
      (milk.exception/err-handle :info (str "sound reset ERROR " elm)
                                 :err err))))
(defn play
  "#el {HTMLAudioElement}

   (play (sound :spin))"
  [elm]
  (try
    ;; google chrome 34에서 play 실행시, 한 번 소리가 난 후에는, 다시 play를
    ;; 실행해도 소리가 안나는 버그를 해결하기 위한 코드
    (if (aget js/window "chrome")
      (.load elm))

    ;; 추가로 IE에서 같은 사운드 재생될때 간혹 씹히는 문제를 해결하기 위한 코드(VT-322)
    ;; TODO: 매번 재로드하면 성능상에 문제가 될 수 있으므로 같은 elm인 경우에만 재로드하는 방식으로 바꿔야 함
    ;;(.load elm)
    ;; => ie 테스트 결과 load 가 너무 오래 걸려서 오히려 소리가 더 안나는 현상 발생하여 다시 원복함
    ;;(VT-322) 다시 TODO 리스트로...

    (.play elm)
    (catch js/Error err
      (milk.exception/err-handle :info (str "sound play ERROR " elm)
                                 :err err))))
