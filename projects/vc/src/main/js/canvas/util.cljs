(ns js.canvas.util
  (:require [js.canvas :as cv]))


(defn set-style [ctx & {:keys [font color align] :or {}}]
  (if font
    (cv/font-style ctx font))
  (if color
    (cv/fill-style ctx color))
  (if align
    (cv/text-align ctx align))

  ctx)
