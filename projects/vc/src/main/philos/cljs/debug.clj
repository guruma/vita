(ns philos.cljs.debug
  (:require (clojure [string :as str]) ))

;; ClojureScript에 아직 printf 함수가 구현되어 있지 않아 이용 불가
;; (defmacro dbg [x & args]
;;   "Prints the evaluated form of x by using the nRepl."
;;   `(let [[return# args#] [~x (vector ~@args)]
;;         n#   (some #(if (number? %) %) args#)
;;         msg# (some #(if (string? %) %) more#)
;;         return# (or (and n# (take n# return#))
;;                     return#)
;;         format# (str ">> dbg"
;;                      (and msg# " <%s>")
;;                      ": %s\n => %s <<") ]
;;      (if msg#
;;        (printf format# msg# '~x return#)
;;        (printf format# '~x return#))
;;     (flush)
;;     return#))

(defmacro dbg
  "Prints the evaluated form of x by using the nRepl."
  [x & more]

  `(let [[return# more#] [~x (vector ~@more)]
         msg# (some #(if (string? %) %) more#)
         n#   (some #(if (number? %) %) more#)
         if#  (some #{:if} more#)
         passed# (if if#
                   (let [v# (map-indexed vector more#)
                         i# (some #(if (= (get % 1) :if)
                                     (inc (get % 0)))
                                  v#)]
                     (get-in (vec v#) [i# 1]) ))
         once# (some #{:once :o} more#)

         return# (or (and n# (take n# return#))
                     return#)
         format# (str ">> dbg"
                      (and msg# (str " <" msg# ">"))
                      ": " '~x "\n =>")]
     (cond
       once#
       (if if#
         (if passed#
           (if (philos.cljs.debug/changed? (str '~x " " '~more) (str return#))
             (print format# return# "<< (:once mode)\n") ))
         (if (philos.cljs.debug/changed? (str '~x " " '~more) (str return#))
           (print format# return# "<< (:once mode)\n") ))

       :else
       (if if#
         (if passed#
           (print format# return# "<<\n" ))
         (print format# return# "<<\n") ))
     (flush)
     return#))

(defmacro clog
  "Prints the evaluated form of x by using the browser's console.log."
  [x & more]
  `(let [[return# more#] [~x (vector ~@more)]
         msg# (some #(if (string? %) %) more#)
         n#   (some #(if (number? %) %) more#)
         js#  (some #{:js} more#)

         if#  (some #{:if} more#)
         passed# (if if#
                   (philos.cljs.debug/key->value :if more#))

         css#  (some #{:css :c} more#)
         form-css# (if css#
                     (philos.cljs.debug/css-value css# more#)
                     (:debug @philos.cljs.debug/css*))

         once# (some #{:once :o} more#)

         return# (or (and n# (take n# return#))
                     return#)

         header# (str "%cclog%c"
                       (and msg# (str " [" msg# "]"))
                       ": %c " '~x " %c =>")
         result#  (str (pr-str return#)
                       (and once# " (:once mode)")
                       (and js# "\n    %O") )]
     (cond
      once#
      (if if#
        (if passed#
          (if (philos.cljs.debug/changed? (str '~x " " '~more) (str return#))
            (philos.cljs.debug/print-log header# form-css# result# js# return#) ))
        (if (philos.cljs.debug/changed? (str '~x " " '~more) (str return#))
          (philos.cljs.debug/print-log header# form-css# result# js# return#) ))

      :else
      (if if#
        (if passed#
          (philos.cljs.debug/print-log header# form-css# result# js# return#))
        (philos.cljs.debug/print-log header# form-css# result# js# return#) ))

     return#))


;; break point 설정
(defmacro break
  ([] '(js* "debugger;"))
  ([kw test]
     `(if (and (= ~kw :if) ~test)
        ~'(js* "debugger;") )))


;; for debugging dbg macro internally
(defmacro dbg_ [x & more]
  `(let [[return# more#] [~x (vector ~@more)]]
     (.log js/console (str ">>> dbg_: " '~x "=> " return# " <<<"))
     return#))

(defmacro ctime [& body]
  `(do
     (.time js/console ">> Elapsed Time")
       ~@body
     (.timeEnd js/console "<< Elapsed Time") ))
