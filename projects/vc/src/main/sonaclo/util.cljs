(ns sonaclo.util
  (:require
   [goog.string :as gstring]
   [goog.string.format]))


(defn get-current-url []
  (-> js/document
      (.-location)
      (.-href)))


(defn- parse-query [url]
  (let [query (last (.split url "?"))
        pairs (.split query "&")]
    (->> pairs
         (map (fn [p]
                (let [[a b] (vec (.split p "="))]
                  (vector (keyword a) b))))
         (into {}))))


(defn get-user-id []
  (-> (get-current-url)
      (parse-query)
      :user-id
      (str)))


;; 여러 키워드와 문자를 받아서 하나의 키워드를 만드는 함수
(defn keyword+ [& args]
  (let [args (map #(if (keyword? %) (name %) %) args)]
    (keyword (apply str args))))

;; not nil 체크
(def not-nil? (comp not nil?))


(defn startswith [k1 k2]
  (let [s1 (name k1)
        s2 (name k2)]
    (= s2 (.substring s1 0 (count s2)))))


(defn indexof [lst v]
  (second (find (into {} (map-indexed #(vector %2 %1) lst)) v)))


(defn unzip-by-nth
  "[[1 10] [2 20] [3 30]]  =>  [1 2 3 10 20 30]"

  [nested-list]
  (apply concat (apply (partial map #(vec %&)) nested-list)))


(defn with-idx
  "index insertion :: [:a :b :c :d]  =>  ([0 :a] [1 :b] [2 :c] [3 :d])"

  [lst]
  (map-indexed #(vec %&) lst))


(defn zip [& ls]
  (apply (partial map (fn [& a] a)) ls))


(defn number-divs=>div-quots
  "get quot list :: 257 [100 50 1]  =>  [[100 2] [50 1] [1 7]]"

  [number div-lst]
  (let [ordered (sort > div-lst)
        f (fn [[number lst] div]
            [(rem number div) (conj lst (quot number div))])]

    (zip ordered (last (reduce f [number []] ordered)))))


(defn merge-pos
  "[1 10] [1 12]  =>  [2 22]"

  [& pos-lst]
  (apply (partial map +) pos-lst))


(defn multiply-pos
  "2 [1 2]  =>  [2 4]"

  [n pos]
  (map #(* n %) pos))


;;-----------------------
;; helper

(defn load-image [path & infos]
  (delay
   (let [img (js/Image.)]
     (aset img "infos" infos)
     (aset img "onload" #(aset img "loaded" true))
     (aset img "src" path)
     img)))


(defn make-btn [img-k w h z]
  {:img-k img-k
   :table {:normal  [     0  0 w h]
           :over    [     w  0 w h]
           :down    [(* 2 w) 0 w h]
           :disable [(* 3 w) 0 w h]}
   :box {:w w :h h :z z}})



(defn- item-table [n [w h]]
  (->> (iterate (partial + w) 0)
       (take n)
       (mapv (fn [x] [x 0 w h]))))


(defn button-table [[tw h]]
  (let [w (/ tw 4)
        [normal over down disable] (item-table 4 [w h])]
    {:normal normal :over over :down down :disable disable}))


(defn parse-query [url]
  (let [query (last (.split url "?"))
        pairs (.split query "&")
        f (fn [p] (let [[a b] (vec (.split p "="))] (vector (keyword a) b)))]
    (into {} (map f pairs))))


(defn format
  "Formats a string using goog.string.format."
  [fmt & args]
  (apply #(gstring/format fmt %) args))


(defn with-comma [number]
  (->> (str number)
       reverse
       (partition-all 3)
       (interpose ",")
       (apply concat)
       reverse
       (apply str)))

(defn money-with-comma [money]
  (str "$" (with-comma money)))


(defn number-abbreviation [n]
  (let [k 1000
        m 1000000
        unit (atom "")

        s (cond
           (<= m n)
           (do (reset! unit "M")
               (format "%.1f" (float (/ n m))))
           (<= k n)
           (do (reset! unit "K")
               (format "%.1f" (float (/ n k))))
           :else
           (str n))

        [f t] (clojure.string/split s #"\.")]

    (->> (if (= "0" (first t)) f s)
         (#(str % @unit)))))


(defn money-with-abbreviation [n]
  (str "$ " (number-abbreviation n)))


(defn rotate-list [l n]
  (let [n' (if (<= 0 n) n (+ (count l) n))
        h (take n' l)
        t (drop n' l)]
    (concat t h)))


(defn ellipsis
  "말줄임표 '...'"
  [s n]
  (if (or (< (count s) n) (nil? n))
    s
    (str (subs s 0 n) "...")))


(defn filter-k [filter-set dict]
  (reduce-kv (fn [acc k v]
               (if (contains? filter-set k)
                 (assoc acc k v)
                 acc))
             {} dict))
