(ns sonaclo.log
  "web client --> log server"
  (:require [js.websocket :as ws]))

;; remote log server
(def ^:private log-server-url*
  (atom "ws://log.sonaclo.com/log"))

(defn set-url
  [url]
  (reset! log-server-url* url))

(defn- current-time
  "#return {int}"
  []
  (.. (js/Date.) getTime))

(defn- send-log
  "Sends data from weg client to log server."
  [sock data]
  (-> sock (ws/send data) (ws/close)))

(defn log-context
  [app-name user-id]
  {:app_name app-name
   :user_id  user-id
   :source   :client})

(defn dlog
  "log for debugging"
  [lc message]
  (let [sock (ws/open @log-server-url*)
        data (into lc {:log_type :debug
                       :time     (current-time)
                       :message  message} )]
    (ws/on-open sock #(send-log sock data)) ))

(defn ulog
  "log for user action analysis"
  [lc message]
  (let [sock (ws/open @log-server-url*)
        data (into lc {:log_type :user
                       :time     (current-time)
                       :message  message} )]
    (ws/on-open sock #(send-log sock data)) ))


(comment
  (def lc* (log/log-context :texas :user-id))
  (dlog lc* {:message-from-web-client  {:zzz 300}})
  (ulog lc* {:message-from-web-client  {:action {:click :call}}})
)
