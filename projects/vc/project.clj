(defproject vc "0.1.0-SNAPSHOT"

  :description "Vita Client"
  :min-lein-version "2.3.4"

  :dependencies
  [[org.clojure/clojure "1.6.0"]
   [org.clojure/clojurescript "0.0-2311"]

   [org.clojure/core.async "0.1.338.0-5c5012-alpha"]

   [jayq "2.5.1"]
   [com.cemerick/url "0.1.1"]
   [environ "0.5.0"]
   [cljs-ajax "0.2.6"]

   [im.chit/purnam "0.4.3"]         ; https://github.com/purnam/purnam
   ]

  :plugins
  [[lein-cljsbuild "1.0.3-SNAPSHOT"]
   [lein-environ "0.5.0"]]

  :source-paths
  ["src/main"]

  :clean-targets ^{:protect false}
  [:target-path :compile-path "out" "../vw/resources/public/js/game/" "resources/public/js/game/"]

  :jvm-opts ^:replace ["-Xss128m"]

  :profiles
  {:dev
   {:env {:vc-config "../../dist/config/vc.edn" :va-config "../../dist/config/va.edn"}

    :dependencies
    [[ring "1.3.0"]
     [compojure "1.1.8"]
     [com.cemerick/url "0.1.1"]
     [de.ubercode.clostache/clostache "1.4.0"]
     [com.ashafa/clutch "0.4.0"]
     [org.clojure/test.check "0.5.8"]
     [com.cemerick/piggieback "0.1.3"]
     [weasel "0.4.0-SNAPSHOT"]]

    :source-paths
    ["dev/clj"]

    :repl-options
    {:init-ns repl
     :nrepl-middleware [cemerick.piggieback/wrap-cljs-repl]
     :init (run!)}

    :resource-paths
    ["resources" "../vw/resources"]

    :cljsbuild
    {:builds
     [{:id "dev"
       :source-paths ["src/main"]
       :compiler {:output-to "../vw/resources/public/js/game/vita.js"
                  :optimizations :whitespace
                  :pretty-print true
                  :static-fns true
                  :externs ["externs/js/vendor/jquery/extern-jquery-1.11.0.min.js"]}}
      {:id "repl"
       :source-paths ["src/main" "dev/cljs"]
       :compiler {:output-to "resources/public/js/game/vita.js"
                  :output-dir "resources/public/js/game/out"
                  :source-map true
                  :optimizations :none
                  :static-fns false
                  :pretty-print true}}]}

    :aliases
    {"vita-dev" ["do"
                 ["cljsbuild" "clean"]
                 ["cljsbuild" "auto" "dev"]]}
    }

   :proxy
   {:env {:vc-config "../../dist/config/proxy-vc.edn" :va-config "../../dist/config/proxy-va.edn"}

    :dependencies
    [[ring "1.3.0"]
     [compojure "1.1.8"]
     [com.cemerick/url "0.1.1"]
     [de.ubercode.clostache/clostache "1.4.0"]
     [com.ashafa/clutch "0.4.0-RC1"]
     [org.clojure/test.check "0.5.8"]
     [com.cemerick/piggieback "0.1.3"]
     [weasel "0.4.0-SNAPSHOT"]]

    :source-paths
    ["dev/clj"]

    :repl-options
    {:init-ns repl
     :nrepl-middleware [cemerick.piggieback/wrap-cljs-repl]
     :init (run!)}

    :resource-paths
    ["resources" "../vw/resources"]

    :cljsbuild
    {:builds
     [{:id "dev"
       :source-paths ["src/main"]
       :compiler {:output-to "../vw/resources/public/js/game/vita.js"
                  :optimizations :whitespace
                  :pretty-print true
                  :static-fns true
                  :externs ["externs/js/vendor/jquery/extern-jquery-1.11.0.min.js"]}}
      {:id "repl"
       :source-paths ["src/main" "dev/cljs"]
       :compiler {:output-to "resources/public/js/game/vita.js"
                  :output-dir "resources/public/js/game/out"
                  :source-map true
                  :optimizations :none
                  :static-fns false
                  :pretty-print true}}]}
   }

   :qa
   {:env {:vc-config "../../dist/config/qa1-vc.edn"}
    :cljsbuild
    {:builds [{:id "release"
               :source-paths ["src/main"]
               :compiler {:output-to "../vw/resources/public/js/game/vita.js"
                          :optimizations :whitespace
                          :pretty-print true
                          :static-fns true
                          :externs ["externs/js/vendor/jquery/extern-jquery-1.11.0.min.js"]}}]}
    }

   :live
   {:env {:vc-config "../../dist/config/live1-vc.edn"}
    :jvm-opts ^:replace ["-Xms2G" "-Xmx4G" "-server"]
    :cljsbuild
    {:builds [{:id "release"
               :source-paths ["src/main"]
               :compiler {:output-to "../vw/resources/public/js/game/vita.js"
                          :optimizations :advanced
                          :pretty-print false
                          :static-fns true
                          :externs ["externs/js/vendor/jquery/extern-jquery-1.11.0.min.js"]}}]}}
   }

   :aliases
   {"proxy-vc" ["do"
                 ["cljsbuild" "clean"]
                 ["with-profile" "proxy" "cljsbuild" "auto" "dev"]]}
  )
