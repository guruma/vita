(ns sonaclo.util
  (:require
   [clj-time [core :as time]
    [format :as timef]
    [local :as local]]))


(defn now
  "current time in DateTime."
  []
  (local/local-now))


(defn now-str
  "current time in string."
  []
  (timef/unparse (timef/with-zone
                   (timef/formatter "yyyy.MM.dd HH:mm:ss")
                   (time/default-time-zone))
                 (local/local-now)))


(defn parse-time [t-str]
  (timef/parse (timef/with-zone
                 (timef/formatter "yyyy.MM.dd HH:mm:ss")
                 (time/default-time-zone))
               t-str))


;; initiated일 때 실제 결제에 소요되는  시간이 얼마인지...
(defn elapsed-24hours? [t-str]
  (<= 24 (time/in-hours
           ;;   (<= 1 (time/in-minutes
           (time/interval (parse-time t-str) (now)))))


(defn make-uuid []
  (str (java.util.UUID/randomUUID)))
