(ns vita.npc.player
  (:require
    [clojure.tools.logging :refer :all]))

(def actions*
  {:winner {:preflop {:call 100 :raise   0}
            :flop    {:call  80 :raise  95}
            :turn    {:call  70 :raise 100}
            :river   {:call  50 :raise 100}}
   :loser  {:preflop {:call  70 :raise 100}
            :flop    {:call  70 :raise  90} ;; 0~70% call, 70%~95% raise, 95%~100 fold
            :turn    {:call  70 :raise  90}
            :river   {:call  70 :raise  98}}})


(defn bet
  "#winner-sid {str}
   #sid {str}
   #round {kw}
   #prev-bet-cmd {kw}"
  [winner-sid sid round prev-bet-cmd]
  (let [n       (rand-int 100)
        mode    (if (= sid winner-sid) :winner :loser)
        bet-cmd (cond
                  (< n (get-in actions* [mode round :call]))  :call
                  (< n (get-in actions* [mode round :raise])) :raise
                  :else                                       :fold)]

    ;; https://sonaclo.atlassian.net/browse/VT-284  콜-레이즈(Call-Raise) 관행이 있음.
    (if (and (= prev-bet-cmd :call)
             (not= bet-cmd :fold))
      ;:call
      bet-cmd
      bet-cmd)))

(defn get-raise-bet
  [raise-bet user-money room-data]
  (let [rand (+ (rand-int 2) 1)
        new-bet (* raise-bet rand)]
    (min new-bet user-money)))


(defn get-npc-bet-money
  "INPUT
  bet? -> 앞 사람이 call, raise를 하였는가 여부. check, fold는 bet? false가 된다."
  [bet? bet-cmd room-data cur-user]
  (let [{:keys [raise-bet round-bet]} room-data
        {:keys [bet money]} cur-user
        default-raise-money (+ @round-bet @raise-bet (- bet))
        user-call-money  (min money (if (< (- @round-bet bet) 0)
                                      0
                                      (- @round-bet bet)))
        ;; bet? false인 경우는 round가 변경, 앞 사람이 call, raise를 하지 않았을 경우이다.
        user-raise-money  (if bet?
                            ;; 이 경우는 raise bet이 bb bet 금액으로 보정된다.a
                            (min (* (+ 1(rand-int 3 )0) default-raise-money ) money)
                            (get-raise-bet default-raise-money money room-data)) ]

    (condp = bet-cmd
      :check 0
      :raise  user-raise-money
      :call user-call-money
      :fold 0
      (do
        (errorf "get-npc-bet-money wrong bet-cmd = %s" bet-cmd)
        0)) ))

;; TODO(kyt): 현재는 일단 bet 함수의 경우와 동일하게 처리하지만, 개선 필요.
(defn check
  "#winner-sid {str}
   #sid {str}
   #round {kw}"
  [winner-sid sid round]
  (bet winner-sid sid round nil))

(defn req-bet
  "npc 배팅을 가져온다.
  현재 배팅을 기준으로 check일 경우 npc도 check를 호출해준다.
  그 외는 배팅을 한다.
  cur-select-bet 이 nil일 경우 (round 가 변경 될 때 ?)에 :check를 전달한다.
  "
  [cur-select-bet winner-sid sid round prev-bet-cmd possible-select-bet]
  (if (= cur-select-bet check)
    (check winner-sid sid round)
    (let [new-bet (bet winner-sid sid round prev-bet-cmd)]
      (if (contains? possible-select-bet new-bet)
        new-bet
        :raise))))
