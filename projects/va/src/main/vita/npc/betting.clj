(ns vita.npc.betting
  (:require
    [vita.npc.player :as npc.player]
    [vita.poker.room.api.user :as room.user]
    [vita.session :as session]
    [sonaclo.shared.util :as share-util]
    [clojure.core.async :as async :refer [<! >! <!! >!! timeout chan put! go close! thread]]))

(defn remove-fold-cmd
  "배팅 cmd 중 랜덤하게 나타낸준다.
  take 4에서 숫자를 짝수를 추천.
  [:call :raise :fold]의 1/3 확률에서
  [:call :raise :call :raise :call :raise :call :fold]의 1/8 확률로 줄인다."
  [cmd]
  (if cmd
    (->
      (->> cmd
           (remove #(= :fold %))
           (cycle)
           (take 7))
      ;(conj :fold)
      (rand-nth))
    cmd))

(defn contains-check-cmd?
  "앞 사람이 배팅을 했는지 여부이다.
  현재 내 bet cmd가 check가 있으면 앞 사람이 check or fold를 한것이다.
  "
  [cur-user-select-list]
  (contains? @cur-user-select-list :check))


(defn execute-bet
  "현재 배팅 차례인지 검사 후 배팅을 한다.
  INPUT prev-bet-cmd : 나의 앞 사람 배팅이다."
  [room prev-bet-cmd]
  (let [{:keys [cur-user-select-list cur-user-sid room-id game-name state stage-info]} room
        cur-user  (first (vals (room.user/select-user-by-sid @(:users room) @cur-user-sid)))
        sid       (:sid cur-user)]

    ;; 현재 배팅차례가 npc인지.
    (when (and (share-util/not-nil? @cur-user-sid)
               (:is-npc cur-user))
      ;; npc 기본 배팅을 가져온다.
      ;; 배팅 머니
      ;; 배팅 시간
      ;; room의 channel에 data 입력한다.
      (let [bet-cmd (-> ;; 명렁에서 fold를 제외힌다. fold는 npc의 betting 로직에서 한다.
                      (remove-fold-cmd @cur-user-select-list)
                      (npc.player/req-bet (:winner-sid @stage-info)
                                          sid
                                          @state prev-bet-cmd
                                          @cur-user-select-list))
            bet? (contains-check-cmd? cur-user-select-list)
            bet-money (npc.player/get-npc-bet-money bet? bet-cmd room cur-user)
            wait-time   (as->
                          (+ (rand-int (- 4001 1000)) 1000) r ; 1~3초
                          (* (quot r 1000) 1000))]

        (go (<! (timeout wait-time))
            (when-let [ch @(:protocol-ch room)]
              (let [packet {:cmd :Qbet
                            :data {:room-id room-id :sid sid
                                   :select bet-cmd :bet bet-money }
                            :scene game-name}]
                (dosync (session/set-last-request-time sid))
                (>! ch (assoc packet :sid sid)))))))))
