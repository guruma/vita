(ns vita.npc.npc_manager
  (:require
    [clojure.tools.logging :refer :all]
    [ruiyun.tools.timer :refer :all]

    [clojure.core.async :as async]

    [vita.config.macros :as cm]

    [vita.const :as CONST]
    [sonaclo.shared.util :as util]
    [vita.session :as session]
    [vita.npc.npc-url :as npc-url]
    [vita.gen :as gen]

    [vita.poker.room.room-manager :as room.room-manager]
    [vita.poker.room.api.user :as room.user]
    [vita.poker.room.roomlist :as roomlist]
    ))

(defn- make-npc
  []
  (let [sid     (gen/gen-tk)
        name-url  (npc-url/pop-npc (if (util/probility 57) ;사진 중북을 줄이기 위해 남자사진 73, 여자사진 55 비율대로 나오도록 57%로 함.
                                     :man
                                     :woman))
        account {:social-id sid
                 :user {:money  9999999999
                        :name (first name-url)
                        :avatar-url (second name-url)
                        :is-npc true}}]

    (session/add-sid-tk sid sid)
    (session/add-sid-account sid account)
    sid))

(defn make-buy-in-money
  [sb-money]
  (condp = sb-money
    roomlist/BLIND-50        20000
    roomlist/BLIND-200       80000
    roomlist/BLIND-500       200000
    roomlist/BLIND-2k        800000
    roomlist/BLIND-10k       4000000
    roomlist/BLIND-100k      40000000
    20000 ))

(defn join-and-seat-room [room seat]
  (let [sb (:sb room)
        buy-in (make-buy-in-money sb)
        sid (dosync (make-npc))
        room-id (:room-id room)
        scene (:game-name room)
        join-packet {:cmd :Qjoinroom
                     :data {:room-id room-id :is-npc true}
                     :scene scene :tk ""}
        seat-packet {:cmd :Qseat
                     :data {:room-id room-id :seat seat
                            :buy-in buy-in :auto-refill true
                            :is-npc true}
                     :scene scene :tk ""}]
    (room.room-manager/pass-packet join-packet sid)
    (room.room-manager/pass-packet seat-packet sid)
    ))

;(def room
;  (nth (sort-by :sb (vals @(room.room-manager/get-rooms :texas))) 0))

(defn join-and-seat-for-all-rooms [game-name]
  (let [rooms (vals @(room.room-manager/get-rooms game-name))]
    (doseq[room rooms]
      (let [available_seats (filter #(not (room.user/find-user-by-seat room %)) [:seat0 :seat1 :seat2 :seat3 :seat4])]
        (when (and (>= (count available_seats) 3)
                   (util/probility (get-in cm/config* [:npc :join-room-probility])))
          (join-and-seat-room room (rand-nth available_seats))
          (Thread/sleep 2)
          )))))

(defn init-game [game-name]
  (dotimes [_ 3]
    (join-and-seat-for-all-rooms game-name)))

(defn go-timer [f msecs & max-num]
  (let [num* (atom 0)]
    (async/go-loop [ch (async/timeout msecs)]
                   (async/<! ch)
                   #_(println "gotimer executed")
                   (f)
                   (swap! num* inc)
                   (when (or (nil? max-num)
                             (< @num* (first max-num)))
                     (recur (async/timeout msecs)))
                   )))

(defn- rooms-having-npc [rooms]
  (filter #(not (empty?
                  (room.user/select-user-by @(:users %) :is-npc)))
          rooms))

(defn- npc-users [room]
  (room.user/select-user-by @(:users room) :is-npc))

(defn- remove-rand-npc [room]
  (let [sid (rand-nth (keys (npc-users room)))
        quit-packet {:cmd :Qquit :data {:room-id (:room-id room)} :scene (:game-name room) :tk ""}]
    (when sid
      (room.room-manager/pass-packet quit-packet sid))))

(defn- remove-rand-npc-in-rand-rooms [game-name]
  (let [rooms (vals @(room.room-manager/get-rooms game-name))
        npc-rooms (rooms-having-npc rooms)]
    (println "called: remove-rand-npc-in-rand-rooms")
    (when-not (empty? npc-rooms)
      (remove-rand-npc (rand-nth npc-rooms)))))

(defn timer-join [game-name]
  (go-timer #(join-and-seat-for-all-rooms game-name) (get-in cm/config* [:npc :join-period-time])))

(defn timer-remove [game-name]
  (go-timer #(remove-rand-npc-in-rand-rooms game-name) (get-in cm/config* [:npc :remove-period-time])))

(defn npc-init-by-game [game-name]
  (init-game game-name)
  (timer-join game-name)
  ;(timer-remove game-name)
  )

(defn test_join_a_npc [game-name room-index]
  (let [room (nth (sort-by :sb (vals @(room.room-manager/get-rooms game-name))) room-index)
        available_seats (filter #(not (room.user/find-user-by-seat room %)) [:seat0 :seat1 :seat2 :seat3 :seat4])]
    (when (>= (count available_seats) 3)
      (join-and-seat-room room (rand-nth available_seats)))))

(defn npc-init
  []
  (try
    (println "npc-init start")

    (npc-init-by-game CONST/TEXAS)
    (npc-init-by-game CONST/OMAHA)
    (npc-init-by-game CONST/OMAHA-HL)

    ;테스트용으로 npc 하나 넣는 코드
    #_(test_join_a_npc CONST/TEXAS 0)

    (println "npc-init end")
    (catch Exception e
      (errorf e "NPC ERROR - activate"))))
