(ns vita.core
  (:require
    [clojure.tools.logging :as log :refer :all]
    [clojure.tools.cli :refer [parse-opts]]
    [clojure.core.async :as async :refer [<! >! <!! >!! timeout chan alt! alts!! go]]

    ;; http-kit
    [org.httpkit.server :as kit]
    [ns-tracker.core :refer [ns-tracker]]

    ;; nrepl
    [clojure.tools.nrepl.server :as nrepl-server]
    [cider.nrepl :refer [cider-nrepl-handler]]

    ;; vita.
    [vita.dispatcher :as dispatcher]
    [vita.session :as session]
    [vita.session-closer :as session-closer]

    [vita.common.websocket :as ws]
    [vita.config.macros :as am]
    [vita.npc.npc_manager :as npc]
    [vita.db.game-db :as game-db]

    [vita.poker.room.roomlist :as room.roomlist]

    )
  (:gen-class))

(declare npc-init)
(def modified-namespaces (ns-tracker ["src"]))

(defn reload-modified-namespaces
  "reload modified namespaces in one or more source directories."
  []
  (when (not= :live (am/config:get-in [:level]))
    (doseq [ns-sym (modified-namespaces)]
      (require ns-sym :reload))))


(defn- save-last-accss-time
  [sock]
  (when-let [sid (-> sock
                     session/sock->tk
                     session/tk->sid)]
    (future
        (game-db/save-last-logout-time sid))))


;; HTTP KIT
(defn receive-handler
  [sock data]

  (reload-modified-namespaces)
  (log/debug "packet-receive")
  (go (log/debug "packet-dispatch")
      (dispatcher/dispatch sock data)))


(defn close-handler
  [sock status]
  (go
    (save-last-accss-time sock)
    (session-closer/close-session sock)))


(defn handler [request]
  (kit/with-channel request channel

    (if-not (= "/game/" (:uri request))
      (ws/reject channel)
      (do
        (kit/on-receive channel (fn [data] (receive-handler channel data)))
        (kit/on-close channel (fn [status] (close-handler channel status)))))))



(defn start-repl
  []

  (let [port 8090]
    (nrepl-server/start-server
      :port port
      :handler cider-nrepl-handler)

    (warnf "[REPL_INIT] nrepl-server :port %s" port)))


(defn run!
  ([] (run! true))
  ([options]

  (try
    (let [host (am/config:get-in [:host])
          port (am/config:get-in [:port])]

      ;; 웹 소켓 서버 실행.
      (kit/run-server handler {:port port})
      (warnf "[SERV_INIT] SERVER RUNNING %s:%s" host port)

      ;; session closer 활성화.
      (session-closer/session-close-timer)

      ;; 룸 초기화
      (room.roomlist/init! :texas)
      (room.roomlist/init! :omaha)
      (room.roomlist/init! :omahahl)

      ;; npc 초기화.
      (npc-init (:npc options))
      )

    (catch Exception e
           (errorf e "[SERV_INIT] ERROR")))))


(defmacro try-done [[tag message] & body]
  `(do
     (warnf "[%s] [%s] [%s]" ~tag "try" ~message)
      ~@body
     (warnf "[%s] [%s] [%s]" ~tag "done" ~message)))


(def cli-options
  ;; An option with a required argument
  [["-n" "--npc" "npc init" :flag true]])


(defn -main [& args]
  (let [parsed (parse-opts args cli-options)
        options (:options parsed)]

    (try
      (try-done
       [:SERV_MAIN "start server "]
       (run! options))

      (try-done
       [:SERV_MAIN "start repl"]
       (start-repl))
      (catch Exception e
        (errorf e "[SERV_MAIN] ERROR")))))


(defn npc-init
  [npc?]

  ;; 커맨드 옵션으로 들어올 경우 무조건 npc init
  (if npc?
    (npc/npc-init)

    ;; 설정 파일 검사
    (let [game-config (am/config:get-in [:level])]
      (if (not= game-config :internal)
        (npc/npc-init) ;; qa, live는 무조건 실행
        (npc/npc-init) ;; 개발일 때 주석으로 활성/비활성할 수 있다.
        ))))
