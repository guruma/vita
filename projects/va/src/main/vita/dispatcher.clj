(ns vita.dispatcher
  (:require

  [clojure.tools.logging :as log :refer :all]
  [clojure.tools.reader.edn :as edn]
  [clojure.string :as str]

  [schema.core :as s]
  [slingshot.slingshot :refer [throw+ try+]]

  [vita.net :as net]
  [vita.err :as err]
  [vita.session :as session]

  [vita.shared.const.err-code :as ERR]

  ;; handlers.ffff
  [vita.scene.common.net.handler :as common]
  [vita.scene.main-lobby.net.handler :as main-lobby]
  [vita.scene.texas.net.handler :as texas]
  [vita.scene.slotmachine.net.handler :as slotmachine]
  [vita.scene.omaha.net.handler :as omaha]
  [vita.scene.omahahl.net.handler :as omahahl]
  [vita.shared.const.common :as COMMON]

  [vita.poker.room.protocol-manager :as poker.room.protocol-manager]
  [vita.poker.room.room-manager :as poker.room.room-manager]
  ))


;; TODo(kep) 리펙토링.
(defn bind-gamename [handler-dic game-name]
  (reduce-kv (fn [acc k v]
               (assoc acc [game-name k] v))
             {}
             handler-dic))


;; TODo(kep) 리펙토링.
(def cmd-handlers*
  (->> [(bind-gamename slotmachine/x-handler* COMMON/SLOTMACHINE)
        (bind-gamename main-lobby/x-handler* COMMON/MAIN_LOBBY)
        (bind-gamename texas/x-handler* COMMON/TEXAS)
        (bind-gamename omaha/x-handler* COMMON/OMAHA)
        (bind-gamename omahahl/x-handler* COMMON/OMAHAHL)
        (bind-gamename common/x-handler* COMMON/COMMON)]
       (apply merge)))


;; helpers...
(defn is-Qcmd? [cmd]
  (= \Q (-> cmd
            (name)
            (first))))


(def Packet$
  {:cmd (s/pred is-Qcmd?)
   :tk s/Any ;;(s/either (s/eq nil) s/Str)
   :data (s/pred map?)
   :scene s/Keyword})


(defn check-recv-packet-validator [recv-packet]

  ;; packet 스키마 체크
  (if-let [r (s/check Packet$ recv-packet)]
    (err/sys-error ERR/INVALID_PACKET "invalidate packet %s" r))

  ;; 각 패킷의 data 의 스키마 체크
  #_(let [{:keys [cmd scene data]} recv-packet
        Data$ (:Data$ (get cmd-handlers* [scene cmd]))]
    (when Data$
      (if-let [r (s/check Data$ data)]
        (err/sys-error ERR/INVALID_PACKET "invalidate (packet) data %s" r)))))


(defn Qcmd->Rcmd [qcmd]
  (->> qcmd
       name
       (drop 1)
       (#(conj % \R))
       (apply str)
       keyword))



(defn- data->edn [data]
  (-> (str data)
      (edn/read-string)))


(defn- proc-handler [proc sid data]
  (try+

    {:err ERR/ERR_NONE
     :data (proc sid data)}

    (catch Exception e
           (errorf e "[IN_PROC_EXCPT] sid=%s, data=%s ctx=%s" sid data &throw-context)

           {:err ERR/UNKNOWN_EXCEPTION})

    (catch err/sys-error? _
           (throw+))

    (catch err/in-error? {:keys [err]}
           (let [throwable (:throwable &throw-context)]
             (errorf throwable "[IN_PROC_ERROR] sid=%s, data=%s, err=%s", sid data err)
             {:err err}))

    (catch Object o
           (throw+))))


(defn dispatch
  [sock data]

    (try+
      (let [recv-packet (data->edn data)]

        ;; [check] packet schema.
        (check-recv-packet-validator recv-packet)

        ;;
        ;; # handle packet.
        (let [{:keys [cmd tk data scene]} recv-packet]

          ;; [check] cmd handler.
          (when-not (contains? cmd-handlers* [scene cmd])
            (err/sys-error ERR/INVALID_CMD "INVALIDATE CMD"))

          (let [{:keys [is-req-login? self-sender? in-schema out-schema proc]} (get cmd-handlers* [scene cmd])
                is-req-login? (or is-req-login? false)
                sid (session/tk->sid tk)]


            ;; session에 마지막 요청 시간을 갱신해준다.
            (dosync
             (session/set-last-request-time sid))

            ;; [check] sid
            (when-not is-req-login?

              (when-not (session/is-connected? sid)
                (err/sys-error ERR/INVALID_SID "INVALID SID")))


            ;; [check] in-data schema.
            (when (and in-schema (s/check in-schema data))
              (err/sys-error ERR/INVALID_INDATA "INVALIDATE IN-DATA"))

            (cond

             ;; 포커 룸 관련 패킷은 매니저에게 전달
             (poker.room.protocol-manager/is-poker-room-cmd? cmd)
             (poker.room.room-manager/pass-packet recv-packet sid)

             ;; 처리후 데이터 자동 전송함
             self-sender?
             (proc-handler proc sid data)

             ;; 처리된 데이터를 수동 전송함
             :else
             ;; cmd proc.
             (let [send-data (if is-req-login?
                               (proc-handler proc sock data)
                               (proc-handler proc sid data))
                   rcmd (Qcmd->Rcmd cmd)]

               (if (not= ERR/ERR_NONE (:err send-data))

                 (net/send% sock (assoc send-data :cmd rcmd :scene scene))

                 (do
                   ;; [check] out-data schema.
                   (when (and out-schema (s/check out-schema (:data send-data)))
                     (err/sys-error ERR/INVALID_OUTDATA "INVALIDATE OUT-DATA"))

                   ;; [send] packet data.
                   (net/send% sock (assoc send-data :cmd rcmd :scene scene)))))))))

      (catch err/sys-error? {:keys [err]}
             (let [sid (-> sock
                           (session/sock->tk)
                           (session/tk->sid))
                   throwable (:throwable &throw-context)]
               (errorf throwable "SYS_ERROR: sid=%s, err=%s" sid err)
               (net/close% sock)))

      (catch Exception e
             (let [sid (-> sock
                           (session/sock->tk)
                           (session/tk->sid))]
               (errorf e "EXCEPTION: sid=%s" sid)
               (net/close% sock)))

      (catch Object _
             (let [sid (-> sock
                           (session/sock->tk)
                           (session/tk->sid))
                   throwable (:throwable &throw-context)]
               (errorf throwable "Object: sid=%s" sid)
               (net/close% sock)))))
