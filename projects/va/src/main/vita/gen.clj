(ns vita.gen)


(defn gen-uid
  []
  (str (java.util.UUID/randomUUID)))

(defn gen-tk
  []
  (gen-uid))

