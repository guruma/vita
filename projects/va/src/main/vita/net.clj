(ns vita.net
  (:require
    [vita.common.websocket :as ws]))

(defn send%
  [sock data]
  (if (not= (type {}) (type data))
    (println data))
  (ws/send sock (str data)))


(defn close% [sock]
  (ws/close sock))

