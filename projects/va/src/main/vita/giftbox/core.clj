(ns vita.giftbox.core)

(require '[philos.debug :refer :all])

(def gift-info*
  (atom {}))

(defn get-gift-info-from-db
  []
  (reset! gift-info*
          {:gift-id-1 {:type         :daily
                       :title        "Free Chips Gift"
                       :sub-title    "free chips +50,000"
                       :description  "free chips를 받으세요!"
                       :value        50000
                       :end-date     [2014 10 31]
                       :facebook-msg "아무개 A씨가 free chpis 선물을 아무개 B씨에게 드립니다."}
           :gift-id-2 {:type         :daily
                       :title        "Free Spins Gift"
                       :sub-title    "free Spins +30 times"
                       :description  "free Spins를 받으세요!"
                       :value        30
                       :end-date     [2014 10 31]
                       :facebook-msg "아무개 A씨가 free spins 선물을 아무개 B씨에게 드립니다."} }))

(defn get-giftbox-from-db
  [sid]
  {:gift-id-1 {:sender       "sender-name"
                :receive-date [2014 10 24]}
   :gift-id-2 {:sender       "sender-name"
                :receive-date [2014 10 24] }})
(defn get-sent-gifts
  [sid]
  {:gift-id {:receivers [sid]
              :send-date [2014 8 31] }})

(defn get-gift-count
  [sid]
  (count (get-giftbox-from-db sid)))
