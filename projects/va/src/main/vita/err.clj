(ns vita.err
  (:require
   [slingshot.slingshot :refer [throw+]]))


(defmacro throw-when-not
  "Evaluates test and return, if logical false. Evaluates body in an implicit do."
  {:added "1.0"}
  [test & body]
    (list 'if test nil (cons 'do body)))


(defmacro in-error
  "client에 전달 되는 Error 이다."
  ([error-code]
     `(throw+ {:type :in-error :err ~error-code}))
  ([error-code message]
     `(throw+ {:type :in-error :err ~error-code} ~message))
  ([error-code fmt arg & args]
     `(throw+ {:type :in-error :err ~error-code} ~fmt ~arg ~@args)))


(defmacro sys-error
  "중요한 에러.
  Socket를 Close 한다."
  ([error-code]
     `(throw+ {:type :sys-error :err ~error-code}))
  ([error-code message]
     `(throw+ {:type :sys-error :err ~error-code} ~message))
  ([error-code fmt arg & args]
     `(throw+ {:type :sys-error :err ~error-code} ~fmt ~arg ~@args)))


(defn in-error? [x]
  (= (:type x) :in-error))


(defn sys-error? [x]
  (= (:type x) :sys-error))
