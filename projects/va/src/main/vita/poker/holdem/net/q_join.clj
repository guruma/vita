(ns vita.poker.holdem.net.q-join
  (:require
    [clojure.tools.logging :refer :all]
    [vita.poker.holdem.net.holdem-sender :as sender]
    [vita.shared.const.err-code :as INERR]
    [vita.err :as err]
    [vita.session :as session]
    [schema.core :as s]

    [sonaclo.shared.util :as util]

    [vita.poker.room.api.user :as room.user]
    [vita.poker.room.api.game :as room.game]
    [vita.poker.room.api.util :as room.util]
    ))


;; data 의 스키마가 아래의 형태와 맞아야 한다
(def Data$
  {:room-id s/Keyword})



(defn req-join
  [room sid data]

  (dosync
    ;; 세션 값 변경
    (session/add-sid-rid sid (:room-id room))
    (session/add-sid-game-name sid (:game-name room))

    (let [name   (session/sid->username sid)
          avatar-url (session/sid->avatar-url sid)
          join-time (System/currentTimeMillis)]

        ;; 유저 조인 시키기
      (room.game/join-room room sid (str name) (str avatar-url) join-time (or (:is-npc data) false))))

  (let [room-data (room.util/convert-room-for-client room)
        change-to-card-back (fn [user] (update-in user [:cards] #(map (constantly :card-back) %)))
        cards-back-room-data (update-in room-data [:users] room.util/update-values change-to-card-back)]

    ;; [R]
    (sender/send->user sid (:game-name room) :Rjoinroom {:room cards-back-room-data})

    ;; [B]
    (sender/broadcast-users :Bjoinroom room-data {:user (get-in room-data [:users sid])
                                                  :room-id (:room-id room-data)})
    ))
