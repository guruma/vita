(ns vita.poker.holdem.net.q-room-list
  (:require
   [clojure.tools.logging :as log]
   [vita.poker.holdem.net.holdem-sender :as sender]
   [vita.session :as session]
   [schema.core :as s]

   [vita.poker.room.roomlist :as room.roomlist]))



(def Data$
  {(s/optional-key :page) (s/maybe s/Num)})



(defn req-room-list
  [game-name sid data]

  ; 1.적당한 나의 레벨.
  ;(hr/get-room-list-by-blind user-money game-name page
  (let [page          (or (:page data) (room.roomlist/get-suitable-page sid))
        roomlist-info (room.roomlist/get-rooms-for-client game-name sid page)]

    {:roomlist roomlist-info
     :cur-page page
     :max-page (room.roomlist/get-max-page)
     }))
