(ns vita.poker.holdem.net.q-seat
  (:require [clojure.core.async :as async :refer [<! >! timeout chan take! go close! thread]]
            [vita.session :as session]
            [vita.shared.const.err-code :as INERR]
            [vita.err :as err]
            [schema.core :as s]

            [vita.poker.holdem.net.holdem-sender :as sender]
            [vita.poker.room.api.game :as room.game]
            [vita.poker.room.stage-manager :as room.stage-manager]
            [vita.poker.room.api.user :as room.user]
            [vita.poker.room.api.util :as room.util]
            [vita.db.util.log-sender :as log-sender]
            [vita.poker.room.api.room :as room]))



;TODO 이상적으로는 npc도 가상소켓을 두어 그것으로 처리하는 것이 깔끔하다.
(defn remove-npc [room sid]
  (room.game/quit-room room sid)
  (session/wipe-session sid)
  )


;; data 의 스키마가 아래의 형태와 맞아야 한다
(def Data$
  {:seat s/Keyword
   :buy-in s/Num
   :auto-refill s/Bool
   :room-id s/Keyword})


(defn req-seat
  [room sid data]

  (let [seat-num      (:seat data)
        buy-in-money  (:buy-in data)
        auto-refill   (:auto-refill data)
        game-name     (:game-name room)
        room-id       (:room-id room)

        end-callbacks (atom [])]

    (dosync
     ;; retry 대비
     (reset! end-callbacks [])

     (let [acc-money  (session/sid->usermoney sid)]

       (cond

        ;; 돈이 부족한 상태
        (< acc-money buy-in-money)
        (if (:is-npc data)
          (remove-npc room sid)
          (swap! end-callbacks
                 conj
                 #(sender/send->user sid game-name :Rseat INERR/NOT_ENOUGH_ACCOUNT_MONEY {:room-id room-id})))

        ;; 누군가 앉아있는 경우
        (room.user/find-user-by-seat room seat-num)
        (if (:is-npc data)
          (remove-npc room sid)
          (swap! end-callbacks
                 conj
                 #(sender/send->user sid game-name :Rseat INERR/DUPLICATE_SEAT {:room-id room-id})))

        ;; 앉기 성공
        :else
        (do
          ;; 유저 앉히기
          (room.game/sit-room room sid seat-num buy-in-money auto-refill)
          (log-sender/seat-poker sid game-name)

          ;; Rseat, Bseat 보내기
          (let [room-data (room.util/convert-room-for-client room)]
            (swap! end-callbacks conj #(do
                                         ;; [R]
                                         (sender/send->user sid game-name :Rseat {:seat (:seat data)
                                                                                  :room-id room-id})

                                         ;; [B] 1
                                         (sender/broadcast-users :Bseat room-data
                                                                 {:sid sid
                                                                  :seat seat-num
                                                                  :user (get-in room-data [:users sid])
                                                                  :room-id room-id}))))

          ;; 게임 시작 가능한지 확인
          (swap! end-callbacks conj #(room.stage-manager/start-check-&-run room))))))

    ;; 등록된 콜백들 실행
    (let [cbs @end-callbacks]
      (doseq [cb cbs]
        (cb)))))
