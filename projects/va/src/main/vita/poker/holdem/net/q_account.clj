(ns vita.poker.holdem.net.q-account
  (:require
    [clojure.tools.logging :refer :all]

    [vita.session :as session]
    [vita.poker.holdem.net.holdem-sender :as sender]
    [vita.err :as err]
    [vita.shared.const.err-code :as INERR]
    ))


(defn req-account
  [_ sid _]

  (if (seq sid)
    (let [account (session/sid->account sid)]

      (when-not account
        (err/in-error INERR/EMTPY_ACCOUNT))

      ;; [R]
      ;; FIXME(kep) account에 대한 개념 설립 필요.

      {:account (-> account
                    (dissoc :_id :_rev))})))

