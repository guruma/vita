(ns vita.poker.holdem.net.q-quit
  (:require
    [sonaclo.shared.util :as util]
    [clojure.core.async :as async :refer [<! >! alts! put! timeout chan go close! thread]]
    [vita.session :as session]
    [schema.core :as s]

    [vita.poker.holdem.net.holdem-sender :as sender]
    [vita.poker.room.api.game :as room.game]
    [vita.poker.room.api.util :as room.util]
    [vita.poker.room.api.user :as room.user]
    [vita.shared.const.poker :as CONST]
    [vita.db.util.log-sender :as log-sender]
    [vita.db.db-const :as db-const]))



(def Data$
  {(s/required-key :room-id) s/Keyword
   (s/optional-key :page) (s/maybe s/Num)})



(defn req-quit
  "패킷 구조
  quit-packet {:cmd :Qquit :data {:room-id (:room-id room)} :scene (:game-name room) :tk ''}"

  [room sid data]

  (let [nil-agent      (agent nil)
        quit-user      (get @(:users room) sid)
        quit-seat      (:seat quit-user)
        user-action-ch (:user-action-ch room)
        room-id        (:room-id room)]

    (dosync

     (let [{:keys [last-stage-money money]} quit-user]
       (when (and money last-stage-money)
         (let [delta-money    (- money last-stage-money)
               holding-money  (session/sid->usermoney sid)
               game-name      (:game-name room)]

           (log-sender/money sid delta-money game-name holding-money "quit" db-const/IN-GAME)
           ;; 세션 머니 업데이트
           (session/update-account-money sid delta-money))))

     ;; 세션에서 지워주기
     (session/remove-rid sid)

     ;; 유저퇴장
     (room.game/quit-room room sid)

     (cond
      ;; 해당 유저 턴일 경우 user-action-ch에 fold로 등록시켜야 한다.
      (room.user/turn-user? room sid)
      (send nil-agent (fn [_] (put! @user-action-ch [:quit-fold sid nil])))

      ;; 유저가 나가므로 게임이 종료되면,
      (room.game/stage-end? room)
      (send nil-agent (fn [_] (put! @user-action-ch :end)))

      :else
      nil))


    ;; [R]
    (sender/send->user sid (:game-name room) CONST/R_QUIT {:room-id room-id})

    ;; [B]
    (sender/broadcast-users :Bquit (room.util/convert-room-for-client room) {:sid sid
                                                                             :seat quit-seat
                                                                             :room-id room-id})
    ))
