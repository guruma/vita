(ns vita.poker.holdem.net.q-standup
  (:require
    [clojure.tools.logging :refer :all]
    [sonaclo.shared.util :as util]
    [clojure.core.async :as async :refer [<! >! alts! put! timeout chan go close! thread]]
    [schema.core :as s]

    [vita.session :as session]
    [vita.shared.const.err-code :as INERR]

    [vita.poker.holdem.net.holdem-sender :as sender]
    [vita.poker.room.api.game :as room.game]
    [vita.poker.room.api.user :as room.user]
    [vita.db.util.log-sender :as log-sender]
    [vita.db.db-const :as db-const]
    [vita.poker.room.api.util :as room.util]))



(def Data$
  {:room-id s/Keyword})


(defn req-stand-up
  [room sid data]

  (let [nil-agent (agent nil)
        user (room.user/find-user-by-sid room sid)
        room-id (:room-id room)]

    (cond

     ;; 방에 유저가 없는 경우
     (not user)
     (sender/send->user sid (:game-name room) :Rseat INERR/NOT_IN_ROOM {}) ;; send error

     ;; 앉아 있지 않은 경우
     (not (:seat user))
     (sender/send->user sid (:game-name room) :Rseat INERR/ALREADY_STAND {}) ;; send error

     :else
     (let [stand-user (room.user/find-user-by-sid room sid)
           user-action-ch   (:user-action-ch room)]

       (dosync

        (let [{:keys [last-stage-money money]} stand-user
              delta-money     (- money last-stage-money)
              game-name       (:game-name room)
              holding-money  (session/sid->usermoney sid)]

          (log-sender/money sid delta-money game-name
                            holding-money "standup" db-const/IN-GAME)
                             ;; 세션 업뎃
          (session/update-account-money sid delta-money))

        ;; 룸 user 정보 업뎃
        (room.game/stand-room room sid)

        (cond
         ;; 해당 유저 턴일 경우 user-action-ch에 fold로 등록시켜야 한다.
         (room.user/turn-user? room sid)
         (send nil-agent (fn [_] (put! @user-action-ch [:standup-fold sid nil])))

         ;; 유저가 나가므로 게임이 종료되면,
         (room.game/stage-end? room)
         (send nil-agent (fn [_] (put! @user-action-ch :end)))

         :else
         nil))

       (let [room-data (room.util/convert-room-for-client room)
             game-name (:game-name room)
             stand-bet-money   (:stage-delta-money stand-user)]

         ;; [R]
         (sender/send->user sid game-name :Rstandup {:room-id room-id})

         ;; [B]
         (sender/broadcast-users :Bstandup room-data
                                 {:sid sid
                                  :seat (:seat stand-user)
                                  :pot (:pot room-data)
                                  :room-id room-id}))))))
