(ns vita.poker.holdem.net.q-bet
  (:require
    [clojure.tools.logging :as log]
    [clojure.core.async :as async :refer [<! >! alts! put! timeout chan go close! thread]]

    [vita.poker.holdem.net.holdem-sender :as sender]
    [sonaclo.util :as util]
    [vita.err :as err]
    [vita.shared.const.err-code :as INERR]
    [vita.session :as session]
    [schema.core :as s]

    [vita.poker.room.api.game :as room.game]
    [vita.poker.room.api.util :as room.util]
    [vita.poker.room.api.room :as room]
    [vita.poker.room.api.user :as room.user]))


(defn send-err [sid game-name err-code]
  (sender/send->user sid game-name :Rbet err-code {}))


(def Data$
  {(s/required-key :room-id) s/Keyword
   (s/required-key :select) s/Keyword

   (s/optional-key :bet) s/Any
   (s/optional-key :sid) s/Str})


(defn req-bet
  [room sid data]

  (let [{:keys [room-id select bet]} data
        {:keys [user-action-ch]} room]

    (assert (and room-id select))

    (cond
      ;; 유저 턴이 맞는지 확인
      ;; 자동 폴드하는 할 때 client에서 betting이 올 때는 로깅을 보여주지 않는다.
      (not= sid @(:cur-user-sid room))
      (if (and (seq (room.user/find-user-by-sid room sid))
               (= false (contains? #{:fold nil} ;; 이미 배팅을 했다.(게임 종료 포함)
                                   (:state  (get @(:users room) sid)))))
        (log/warnf "ERROR : not correct turn
            input sid = %s,
            cur-user-sid = %s,
            room-id = %s,
            room-state=%s,
            cur-user-sid-settime = %s,
            now-time = %s,
            user = %s,
            room = %s
            "

            sid
            @(:cur-user-sid room)
            (:room-id room)
            (:state room)
            @(:cur-user-sid-settime room)
            (str (util/now))
            (get @(:users room) sid)
            room
            ))


      ;; 유저 선택한 리스트가 올바른지 확인
      (not (contains? @(:cur-user-select-list room) select))
      (log/warnf "ERROR : select not in list, select = %s, cur-user-select-list = %s "
                 select @(:cur-user-select-list room))

      :else
      ;; protocol ch 에 통보
      (when-let [ch @user-action-ch]
        (put! ch [select sid bet])))))
