(ns vita.poker.holdem.net.holdem-sender
  "sender"
  (:require
    [clojure.tools.logging :refer :all]

    [vita.net :as net]
    [vita.session :as session]
    [sonaclo.shared.util :as util]
    [vita.shared.const.err-code :as err]
    ))


(defn- filter-seated-user
  "방에 seat 하지 않은 유저는 제외하고 lient에 보내준다."
  [room]

  ;; TODO(kep) 짠 로직이 살작 이상.

  (let [users (:users room)
        filtered-users (into {}
                             (remove (fn [[_ v]] (= :none (:seat v) )) users) ) ]
    (assoc room :users filtered-users)))


(defn- select-user-id-from-room
  "return : ([user-id] [user-id])"
  [room]
  (let [users  (:users room)
        real-users (filter #(not= false (:npc %)) users)]
    (map #(:sid %) (vals real-users)) ))


(defn- usercard->back
  "자신의 카드 외의 다른 유저의 카드는 :back 처리한다.
  data는 {:sid {:cards [] ~~ } :sid {}~~} 이다. 이 외는 무시된다. "
  [b-sid data]
  (if (and (map? data)
           (contains? data :users))
    (update-in data [:users] (fn [users]
                               (let [f (fn [[sid user]]
                                         (if (and (not= b-sid sid)
                                                  (contains? user :cards))
                                           [sid (assoc-in
                                                  user [:cards]
                                                  (loop [x 0 back-card nil]
                                                    (if (= x (count (:cards user)))
                                                      back-card
                                                      (recur (inc x) (conj back-card :card-back)) ) ))]
                                           [sid user])) ]
                                 (into {} (map f users)))))
    data))


(defn- filter-user-data
  [data]

  ;; TODO(kep) ???
  (if (and (map? data)
           (contains? data :users))
    (update-in data [:users] (fn [users]
                               (let [f (fn [[sid user]]
                                         [sid (dissoc user :npc)] ) ]
                                 (into {} (map f users)))))
    data))

(defn- broadcast [sids packet]

  (doseq [sid sids]
    (let [card-back-data  (update-in packet [:data] #(usercard->back sid %))]

      (when-let [sock (session/sid->sock sid)]
        (try
          (net/send% sock card-back-data)
          (catch Exception e
            (error "[SEND ERROR] " (.printStackTrace e))
             ))))))




(defn send->user
  "client와 1:1 용이다.
  INPUT PARM 구조
      sid -> 문자열
      cmd -> keyword
      data -> {}
      err -> INNER/ 등등.."
  ([sid game-name cmd data]
   (send->user sid game-name cmd err/ERR_NONE data))

  ([sid scene cmd err data ]
   (when-let [sock (session/sid->sock sid)]
     (->> {:cmd cmd
           :err err
           :data data
           :scene scene}
          (net/send% sock)))) )


(defn is-pure-data? [m]
  (and (not (nil? m))
       (not= (type m) (type (ref 0)))
       (every? true? (map (fn [[k v]] (not= (type (ref 0)) (type v))) m))))


(defn broadcast-users [command room data]

  ;; room 데이터가 순수 맵 데이터인지 확인
  (assert (is-pure-data? room))

  (if (util/not-nil? room)
    (let [game-name (:game-name room)
          packet  {:cmd command :data data :scene game-name :err err/ERR_NONE}
          sids      (select-user-id-from-room room)]

      (debug "boradcast-------------------------")
      (debug "broadcat->  " command " bet-cmd-> " (:cur-user-select-list data)
             "raise-bet-> " (:raise-bet room) "round-bet-> " (:round-bet room))
      (broadcast sids packet) )))
