(ns vita.poker.holdem.net.handler
  (:require
   [vita.poker.holdem.net
    [q-join :as q-join]
    [q-seat :as q-seat]
    [q-bet :as q-bet]
    [q-quit :as q-quit]
    [q-room-list :as q-room-list]
    [q-standup :as q-standup]
    [q-account :as q-account]]
    ))


(def handler*
  {
   :Qaccount
   {:proc #'q-account/req-account
    }

   :Qroomlist
   {:proc #'q-room-list/req-room-list
    :Data$ q-room-list/Data$}

   :Qjoinroom
   {:proc #'q-join/req-join
    :self-sender? true
    :Data$ q-join/Data$}

   :Qseat
   {:proc #'q-seat/req-seat
    :self-sender? true
    :Data$ q-seat/Data$}

   :Qbet
   {:proc #'q-bet/req-bet
    :self-sender? true
    :Data$ q-bet/Data$}

   :Qquit
   {:proc #'q-quit/req-quit
    :self-sender? true
    :Data$ q-quit/Data$}

   :Qstandup
   {:proc #'q-standup/req-stand-up
    :self-sender? true
    :Data$ q-standup/Data$}

   })


(defn gen-middleware [game-name]
  (->> handler*
       (reduce-kv (fn [acc k v]
                    (assoc acc k (update-in v [:proc]
                                            (fn [proc]
                                              (fn [sid data]
                                                (proc game-name sid data)))))) {})))
