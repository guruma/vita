(ns vita.poker.room.roomlist
  (:require [clojure.tools.logging :as log]
            [vita.poker.room.room-manager :as room.room-manager]
            [vita.poker.room.api.util :as room.util]
            [vita.session :as session]
            [clojure.core.async :as async]
            [vita.config.macros :as am]))

;;--------------------------------------------------------------------
;; room.roomlist 모듈 설명
;;
;; 포커 로비에 보여줄 roomlist 명단을 관리한다.
;;--------------------------------------------------------------------


(require '[philos.debug :refer :all])

(defn sort-rooms-by-sb [rooms]
  (sort-by :sb rooms))

(def ^:const BLIND-50        50)
(def ^:const BLIND-200      200)
(def ^:const BLIND-500      500)
(def ^:const BLIND-2k      2000)
(def ^:const BLIND-10k    10000)
(def ^:const BLIND-100k  100000)

(def BLIND_LVL_DIC
  {BLIND-50   0
   BLIND-200  1
   BLIND-500  2
   BLIND-2k   3
   BLIND-10k  4
   BLIND-100k 5})

(def LVL_BLIND_DIC
  {0  BLIND-50
   1  BLIND-200
   2  BLIND-500
   3  BLIND-2k
   4  BLIND-10k
   5  BLIND-100k})

(defn get-proper-sb
  "sb blind의 2배(bb blind)의 10배의 돈을 기준으로 한다.
  DoubleDown 기준
  방         by-in
  50/100   -> 50   * 2 * 30000 => 3백만
  200/400  -> 200  * 2 * 30000 => 1천2백만
  500/1k   ->
  2k/4k    ->
  10k/20k  ->
  100k/200k-> "
  [user-money]
  (cond
    (<= user-money (* BLIND-50   2 30000))   BLIND-50
    (<= user-money (* BLIND-200  2 30000))   BLIND-200
    (<= user-money (* BLIND-500  2 30000))   BLIND-500
    (<= user-money (* BLIND-2k   2 30000))   BLIND-2k
    (<= user-money (* BLIND-10k  2 30000))   BLIND-10k
    :else BLIND-100k ) )

(defn get-userlevel
  [sid]
  (-> (session/sid->usermoney sid)
      (get-proper-sb)
      (BLIND_LVL_DIC)))

(defn pool-sb-page-list
  "RETURN: level이 0 이라면, [[50 0 5] [50 5 10] [50 10 15] [50 15 20] [200 0 5] [200 5 10] [500 0 5] [500 5 10] [2000 0 5] [2000 5 10] [10000 0 5] [10000 5 10] [100000 0 5] [100000 5 10]]"
  [user-level]
  (let [result (atom [])]
    (loop [level 0]
      (when (< level 6)
        (let [max-page (if (= level user-level)
                         4
                         2)]
          (loop [page 0]
            (when (< page max-page)
              (swap! result conj [(LVL_BLIND_DIC level) (* 5 page) (* 5 (inc page))])
              (recur (inc page)))))
        (recur (inc level))))
    @result))

(defn user-page-to-sb-start-end
  "RETURN [sb start end]"
  [page level]
  (nth (pool-sb-page-list level) page))

(defn get-suitable-page [sid]
  (+ (* 2 (get-userlevel sid)) 1))

(defn get-max-page []
  14)

(declare ensure-pool-rooms)

(defn rooms-group-by-sb [rooms]
  (merge {BLIND-50 []
          BLIND-200 []
          BLIND-500 []
          BLIND-2k []
          BLIND-10k []
          BLIND-100k []}
         (group-by (fn [room]
                     (:sb (second room)))  @rooms)))

(defn suffle-room [room]
  (if (= :live (am/config:get-in [:level]))
    (shuffle room)
    room))
(defn get-rooms-for-client
  "룸 구조에서 ref 타입인 것을 모두 데이터로 변환해준다"
  [game-name sid page]
  (when-let [rooms (room.room-manager/get-rooms game-name)]
    (map room.util/convert-room-for-client
         (let [[sb start end]
               (user-page-to-sb-start-end (dec page) (get-userlevel sid))]
           (-> (rooms-group-by-sb rooms)
               (get  sb)
               (suffle-room)
               (->>
                 (take end)
                 (drop start)
                 (vals))) ))))


(defn ensure-max-user-room-ratio
  "각 sb 레벨마다 'user num / room num' 가 :max-user-room-ratio 보다 커지면 room 추가한다."
  [game-name]
  (let [rooms (room.room-manager/get-rooms game-name)
        max-user-room-ratio (get-in am/config* [:room :max-user-room-ratio])]
    (doseq [[sb sb-rooms] (rooms-group-by-sb rooms)]
      (let [user-cnt (reduce +
                             (map #(count @(:users (second %))) sb-rooms))
            room-cnt (count sb-rooms)
            min-room-cnt (int (/ user-cnt max-user-room-ratio))
            diff-room-cnt (- min-room-cnt room-cnt)]
        (when (> diff-room-cnt 0)
          (dotimes [_ diff-room-cnt]
            (room.room-manager/new-room game-name sb))))))
  (log/warn "ensure-max-user-room-ratio -> room count" game-name (count @(room.room-manager/get-rooms game-name))))

(defn timer [f msecs & max-num]
  (let [num* (atom 0)]
    (async/go-loop [ch (async/timeout msecs)]
                   (async/<! ch)
                   (f)
                   (swap! num* inc)
                   (when (or (nil? max-num)
                             (< @num* (first max-num)))
                     (recur (async/timeout msecs)))
                   )))

(defn init! [game-name]
  (let [room-num (get-in am/config* [:npc :room-pool-cnt])]
    (reset! (room.room-manager/get-rooms game-name) {})
    (doseq [sb [BLIND-50 BLIND-200 BLIND-500 BLIND-2k BLIND-10k BLIND-100k]]
      (dotimes [_ room-num]
        (room.room-manager/new-room game-name sb)))

    ;5분마다
    (timer #(ensure-max-user-room-ratio game-name) (* 1000 60 5))))

