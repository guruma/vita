(ns vita.poker.room.room-manager
  (:require [clojure.core.async :as async :refer [put! timeout chan go close! thread]]
            [clojure.tools.logging :as log]

            [vita.session :as session]
            [vita.poker.room.pool.room-pool :as room-pool]
            [vita.poker.room.api.room :refer [create-room]]
            [vita.poker.room.api.util :refer [uuid convert-room-for-client]]
            [vita.poker.room.protocol-manager :as room.protocol-manager]
            [vita.poker.holdem.net.q-quit :as q-quit]

            [vita.err :as err :refer [throw-when-not]]
            [vita.shared.const.err-code :as err-code]
            [vita.shared.const.poker :as CONST]
            ))


;;--------------------------------------------------------------------
;; room.room-manager 모듈 설명
;;
;; 이 모듈은 특정 game-name (ex- :texas :omaha) 의 룸을 관리해준다.
;;--------------------------------------------------------------------

;; 포커류 관련 room 의 그룹
(def pokers* {:texas room-pool/texas-rooms*
              :omaha room-pool/omaha-rooms*
              :omahahl room-pool/omahahl-rooms*})


(defn get-rooms [game-name]
  (pokers* game-name))


(defn get-rooms-for-client
  "룸 구조에서 ref 타입인 것을 모두 데이터로 변환해준다"
  [game-name page]
  (when-let [rooms (get-rooms game-name)] ;; TODO type 체크, page 적용
    (map convert-room-for-client
         (nthnext (take (* page 5) (vals @rooms)) (* (dec page) 5)))))


;;------------------
;; room

(defn get-room [game-name room-id]
  (when-let [rooms (get-rooms game-name)]
    (get @rooms room-id)))


(defn- add-room [game-name room]
  ;; todo room 유효성 검사 필요
  (when-let [rooms (get-rooms game-name)]
    (swap! rooms assoc (:room-id room) room)))


(defn del-room [game-name room-id]
  ;; 채널 닫기
  (when-let [room (get-room game-name room-id)]
    (close! @(:protocol-ch room))
    (if-let [ch @(:user-action-ch room)]
      (close! ch))
    ;; 룸 삭제
    (let [rooms (get-rooms game-name)]
      (swap! rooms dissoc room-id))))


(defn new-room [game-name sb]
  (let [protocol-ch (chan 20)
        new-room (create-room (uuid) game-name sb protocol-ch)]

    ;; 룸 추가
    (add-room game-name new-room)

    ;; 룸 패킷 처리 go 쓰레드 시작
    (room.protocol-manager/start-room-protocol-loop protocol-ch new-room)))


;;------------------
;; net

(defn- insert-packet [room sid packet]
  (when-let [ch (:protocol-ch room)]
    (put! @ch (assoc packet :sid sid))))


(defn user-quit-if-room
  "유저가 있는 방에 있다면, 찾아서 quit 처리해준다"
  [sid]
  (when-let [rid (session/sid->rid sid)]
    (let [game-names (keys pokers*)
          rooms (keep #(get-room % rid) game-names)]

      (doseq [room rooms]
        (insert-packet room sid {:cmd CONST/Q_QUIT :room-id (:room-id room)})))))


(defn pass-packet
  "네트워크에서 날라온 패킷 중에 룸에 관련된 패킷을 해당 룸의 '패킷 큐' 에 넣어준다.
   패킷 처리는 해당 룸에 달려있는 'protocol-go-loop' 가 처리한다"

  [packet sid]

  (let [game-name (:scene packet)
        room-id (get-in packet [:data :room-id])
        room (get-room game-name room-id)]

    ;; 해당 룸을 찾을 수 없는 패킷일 경우
    ;; TODO 현재 클라이언트에서 room-id nil 을 보내는 경우가 있어서 주석처리해놓음
    #_(throw-when-not (and game-name room-id room)
                    (err/in-error err-code/NOT_FOUND_ROOM))

    ;; 룸 유저 클리어 처리
    ;; 버그로 인해 룸에 유저 데이터가 남은 경우를 대비해,
    ;; Q-joinroom 패킷일때 잔여데이터 클리어 처리를 한다
    (when (= :Qjoinroom (:cmd packet))
      (user-quit-if-room sid))

    (insert-packet room sid packet)))
