(ns vita.poker.room.pool.room-pool)


;;------------------------------
;; 이 모듈은 룸들의 데이터를 모아놓은 곳.
;; 개발 모드에서 자동 reload 할때나,
;; 다른 모듈 문제에 인해서 룸 정보들이 날아가지 않도록 별도의 모듈로 빼놓았음
;;-----------------------------------------------------------


;;  room-id : room data
(def texas-rooms* (atom {}))

(def omaha-rooms* (atom {}))

(def omahahl-rooms* (atom {}))
