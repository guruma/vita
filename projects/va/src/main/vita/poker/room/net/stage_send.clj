(ns vita.poker.room.net.stage-send
    (:require [clojure.core.async :as async :refer [<! >! alts! put! take! timeout chan go close!]]
              [clojure.tools.logging :as log :refer :all]
              [vita.poker.room.api.game :as room.game]
              [vita.poker.room.api.util :as room.util]
              [vita.poker.room.api.user :as room.user]
              [vita.poker.room.api.room :as room]
              [vita.npc.betting :as npc.betting]
              [vita.poker.holdem.net.holdem-sender :as sender]))


;;--------------------------------------------------------------------
;; room.stage-send 모듈 설명
;;
;; 용어 :: stage -> 포커 한게임, 카드 분배부터 정산까지를 하나의 stage 라고 명명한다.
;;
;; 이 모듈은 stage 중에 발생하는 클라이언트 패킷 전송을 담당한다.
;; 리팩토링 전에 holdem-sender 모듈을 사용한다.
;;--------------------------------------------------------------------


;;--------------
;; send helper


(defn send-Bstart [room]
  (log/info "send Bstart!")
  (let [room-data (room.util/convert-room-for-client room)]
    ;; [B] Bstart
    (go (sender/broadcast-users :Bstart room-data
                                {:room-id (:room-id room-data)
                                 :dealer-sid (room/get-dealer-sid room)
                                 :users (:users room-data)}))))

(defn send-Bbet [room]
  (log/info "send-Bbet!")
  (let [room-data (room.util/convert-room-for-client room)
        {:keys [last-user-sid cur-user-sid cur-user-select-list round-bet raise-bet pot]} room-data
        last-user (get-in room-data [:users last-user-sid])]

    (go (sender/broadcast-users :Bbet room-data
                                (-> {:room-id (:room-id room-data)
                                     :cur-user-sid cur-user-sid
                                     :cur-user-select-list cur-user-select-list
                                     :pot pot
                                     :raise-bet raise-bet
                                     :round-bet round-bet}
                                    ;; 그 사이에 라스트 유저가 standup 해서 게임에서 빠졌을 경우 대비하여 확인해서 넣는다
                                    (merge (if (:state last-user)
                                             {:last-user-sid last-user-sid
                                              :last-user last-user})))))

    (go (npc.betting/execute-bet room (:state last-user)))))


(defn send-Bbetend [room]
  (log/info "send Bbetend!")
  (let [room-data (room.util/convert-room-for-client room)
        {:keys [pot users]} room-data]

    (go (sender/broadcast-users :Bbetend room-data {:room-id (:room-id room-data)
                                                    :pot pot
                                                    :users users}))))


(defn send-Bround [room]
  (log/info "send Bround! " @(:state room))
  (let [room-data (room.util/convert-room-for-client room)
        {:keys [state]} room-data

        cmd (case state
              :preflop :Bpreflop
              :flop :Bflop
              :turn :Bturn
              :river :Briver
              nil)]

    (go
      (if (= cmd :Bpreflop)

        (sender/broadcast-users cmd room-data
                                {:room-id (:room-id room-data)
                                 :users (:users room-data)})

        (sender/broadcast-users cmd room-data
                                {:room-id (:room-id room-data)
                                 :community-cards (vec (:community-cards room-data))})))))


(defn send-Bend
  "winner : {:high {'4b01fc4c-6ebf-4852-b522-151cd4d2bbd6' 1400}, :low {}}
   player : {'4b01fc4c-6ebf-4852-b522-151cd4d2bbd6'
              {:score {:high {:category :two-pair,
                              :hand [:diamond-K :heart-K ~]}},
                              :personal-cards [:heart-9 :heart-2],
                              :cards {:high [:diamond-K ~],
                       :low nil}}
           'sid'
           {:score ~~ }}
   overage : 1110"
  [room]
  (log/info "send Bend!")
  (let [end-info (:end-info room)
        room-data (room.util/convert-room-for-client room)

        {:keys [community-cards
                winner
                players
                overage]} @end-info]

    ;; [B] Bpreflop
    (go (sender/broadcast-users :Bend room-data
                                {:room-id (:room-id room-data)
                                 :community-cards community-cards
                                 :winner winner
                                 :players players
                                 :room room-data
                                 :overage overage}))))
