(ns vita.poker.room.protocol-manager
  (:require [clojure.tools.logging :as log]
            [clojure.core.async :as async :refer [<! >! timeout chan alts! go-loop close! thread]]
            [vita.poker.holdem.net
             [q-join :as q-join]
             [q-seat :as q-seat]
             [q-bet :as q-bet]
             [q-quit :as q-quit]
             [q-standup :as q-standup]]))


;;--------------------------------------------------------------------
;; room.protocol-manager 모듈 설명
;;
;; 이 모듈은 룸 마다 달려있는 패킷 처리 loop 를 시작시키고,
;; 해당 룸으로 들어온 패킷을 가지고,
;; 그에 맞는 protocol 함수를 찾아 실행시켜준다.
;;--------------------------------------------------------------------


(def protocols
  ;; 룸에 관련된 프로토콜 함수들 매핑
  {:Qjoinroom #'q-join/req-join
   :Qseat #'q-seat/req-seat
   :Qbet #'q-bet/req-bet
   :Qquit #'q-quit/req-quit
   :Qstandup #'q-standup/req-stand-up})


(defn is-poker-room-cmd? [cmd]
  (some? (protocols cmd)))


(defn process-protocol [room cmd sid data]
  (let [protocol (protocols cmd)]

    (if protocol
      (try
        (protocol room sid data)
        (catch Exception e
          (log/warn "ERROR protocol process fail!, sid = " sid ", cmd = " cmd ", data = " data)
          (log/warn (.printStackTrace e))))
      (log/warn "ERROR not defined protocol, cmd = " cmd))))


(defn start-room-protocol-loop
  "방마다 패킷 리시브용 go-loop 가 돈다."

  [ch room]

  (go-loop []

    (let [packet (<! ch)]

      (when packet ;; 패킷이 nil 이면 '채널이 닫힘' = '방 없어짐' = '루프 종료'

        (let [cmd (:cmd packet)
              sid (:sid packet)
              data (:data packet)]
          (process-protocol room cmd sid data))

        (recur)))))
