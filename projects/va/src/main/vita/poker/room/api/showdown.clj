(ns vita.poker.room.api.showdown
  (:require [clojure.tools.logging :as log]
            [clojure.data.generators :as data.generators]
            [vita.dealer :as dealer]
            [vita.config.macros :as am]
            [sonaclo.shared.util]
            [vita.poker.room.api.util :as room.util]))


;;--------------------------------------------------------------------
;; room.api.showdown 모듈 설명
;;
;; 이 모듈은 dealer 모듈의 다리역할이다.
;; 초기 카드 분배 로직 및 정산 로직을 이어준다
;;
;; TODO :: 정산 결과 스키마가 예전과 달라서 맞추느라 코드가 복잡해졌다. 정리할 필요.
;;--------------------------------------------------------------------


(def rand-int-range (range 100))


(defn- decide-winner-sid
  "config 값에 설정된 확률로 winner 를 결정한다. (npc vs person)"
  [users]

  (when-not (empty? users)

    (let [[npc-user-list person-user-list] (split-with :is-npc (vals users))
          npc-win-probility (am/config:get-in [:npc :win-probility])
          is-npc-win (if (sonaclo.shared.util/probility npc-win-probility) true false)]

      ;; 결정된 winner-sid 를 반환한다
      (cond (empty? person-user-list)
            (data.generators/rand-nth (map :sid npc-user-list))
            (empty? npc-user-list)
            (data.generators/rand-nth (map :sid person-user-list))
            :else
            (if (and is-npc-win (seq npc-user-list))
              (data.generators/rand-nth (map :sid npc-user-list))
              (data.generators/rand-nth (map :sid person-user-list)))))))


(defn hand-out-cards
  "카드 분배 결정 - 여기서 승자 및 결과값까지 모두 결정된다."
  [game-name users]
  (let [{cmm-cards :community-cards cards-score :users} (dealer/request-cards game-name (count users))

        ;; 1 등 지정
        winner-sid (decide-winner-sid users)

        ;; 맨 앞의 카드가 1등 카드임으로 1등 sid 를 맨 앞에 놓는다
        sorted-sids (if (nil? winner-sid)
                      (data.generators/shuffle (keys users))
                      (concat [winner-sid] (->> (dissoc users winner-sid)
                                                keys
                                                data.generators/shuffle)))

        ;; 리스트 순서대로 각 sid 에 카드 및 결과를 할당한다
        [sid-cards sid-score] (reduce (fn [[m1 m2] [sid cards-score]]
                                        [(assoc m1 sid (:cards cards-score))
                                         (assoc m2 sid (:score cards-score))])
                                      [{} {}]
                                      (map list sorted-sids cards-score))]

    ;; return
    [sid-cards, sid-score, cmm-cards, winner-sid]))



(defn- adapt-calc-awards
  "dealer api 함수 결과와 현재 클라이언트와의 데이터 규격이 달라서 변경하는 작업을 한다."
  [{:keys [overage high low] :as result}]

  ;; 주 요지는,
  ;; 기존의 high, low 의 들어있는 금액이 pot 크기를 의미한다.
  ;; pot 금액에서 실제 받을 수 있는 금액으로 바꿔야한다.
  ;; 따라서, high 와 low 가 동시에 있는 pot 에 대해서는 1/2 pot 으로 쪼개야 한다.

  ;; TODO...복잡함...

  (let [split-pot-in-half (fn [[new-high new-low] [[high-pot high-sids] [low-pot low-sids]]]

                            (assert (= high-pot low-pot))

                            (cond (and (seq high-sids) (seq low-sids))
                                  (let [half-pot (int (/ high-pot 2))]
                                    [(conj new-high [half-pot high-sids]) (conj new-low [half-pot low-sids])])

                                  (seq high-sids)
                                  [(conj new-high [high-pot high-sids])  new-low]

                                  :else
                                  [new-high  (conj new-low [low-pot low-sids])]))

        [high low] (if (or (empty? high) (empty? low))
                     [high low]
                     (reduce split-pot-in-half [[] []] (map list high low)))]

    ;; 기존 값에서 규격을 변경한 high, low 값을 새로 할당
    (assoc result :high high :low low)))




(defn calc
  "초기에 결정되어있는 카드 및 결과 값과, 게임 종료 후 실제로 남아있는 유저만을 선별하여 정산결과를 산출한다."

  [game-name users stage-info is-card-open-end?]

  ;; 게임에 참가한 유저들 중에서 마지막까지 남아있으면서 fold 가 아닌 유저들
  (let [alive-users (->> (vals users)
                         (filter #(let [state (:state %)] (and state (not= :fold state))))
                         (map :sid)
                         (select-keys users))]

    ;; 정산처리할 대상이 있는지 확인
    (when-not (empty? alive-users)

      (let [{:keys [community-cards sid-bet sid-cards sid-score]} stage-info

            ;; 게임 참가한 유저들 중에 정산 대상이 안되는 유저들의 cards 를 지운다.
            sid-cards (room.util/update-values-with-kv sid-cards (fn [sid cards] (if (alive-users sid)
                                                                                   (vec cards))))


            ;; 재정산
            end-list (->> (map (fn [[sid bet]] {:sid sid :bet bet}) sid-bet)
                          (map (fn [m] (assoc m :cards (get sid-cards (:sid m)))))
                          (map (fn [m] (assoc m :score (get sid-score (:sid m))))))

            {:keys [overage high low]} (adapt-calc-awards (dealer/calc-awards game-name end-list))

            ;;----------------------------------------------------------------------------
            ;; 규격 변경
            ;; TODO... 클라이언트와 규격을 같이 변경하든지 해서 작업을 단순화할 필요성 있음...
            ;; (int (min (Integer/MAX_VALUE) (float (/ total-money sids-cnt)))) 이 함수는 npc에서 int max보다 클 경우 대비.
            high-winner (reduce (fn [acc [total-money sids]]
                                  (let [sids-cnt    (count sids)
                                        new-money   (int (min (Integer/MAX_VALUE) (float (/ total-money sids-cnt))))
                                        ;new-money (int (/ total-money sids-cnt))
                                        money-sids (map (fn [sid] [new-money sid]) sids)]
                                    (reduce (fn [acc [money sid]]
                                              (update-in acc [sid] (fn [v] (if v (+ v money) money))))
                                            acc
                                            money-sids)))
                                {}
                                high)

            low-winner (reduce (fn [acc [money sids]]
                                 (let [sids-cnt (count sids)
                                       new-money (int (/ money sids-cnt))
                                       money-sids (map (fn [sid] [new-money sid]) sids)]
                                   (reduce (fn [acc [money sid]]
                                             (update-in acc [sid] (fn [v] (if v (+ v money) money))))
                                           acc
                                           money-sids)))
                               {}
                               low)

            f (fn [acc {:keys [sid cards score]}]
                (assoc acc sid {:score score
                                :personal-cards cards
                                :cards {:high (get-in score [:high :hand])
                                        :low (get-in score [:low :hand])}}))

            players (reduce f {} end-list)

            ;;-----------------------------------------------------------------------------
            ;; 규격 변경 완료
            end-info {:community-cards (vec community-cards)
                      :winner {:high high-winner
                               :low low-winner}
                      :players players
                      :overage overage
                      }]


        ;; 카드 오픈 하지 않고 끝내는 경우에는 개인카드들, 커뮤니티 카드들을 지운다
        (if is-card-open-end?
          end-info
          (let [delete-cards #(dissoc % :personal-cards :cards :score)]
            (-> end-info
                ;; 커뮤니티 카드 비우기
                (assoc :community-cards [])
                ;; 개인 카드들 지우기
                (update-in [:players] (fn [players]  (room.util/update-values players delete-cards))))))))))
