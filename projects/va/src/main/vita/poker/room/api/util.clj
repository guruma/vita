(ns vita.poker.room.api.util
  (:require
    [clojure.data :refer [diff]]
    [vita.shared.const.common :as common]))


;;------------
;; util

(defn uuid []
  ;; edn 구조상 :123 같이 숫자로 시작하는 키워드에서 파싱 에러가 나서 "k" 를 첨부하였음
  (keyword (str "k" (java.util.UUID/randomUUID))))


(defn same-kv?
  "{:a 1 :b 2} :a 1 :b 2 => true"
  [m & kv]
  (nil? (second (diff m (apply hash-map kv)))))


(defn arrange-by-idx [idx lst]
  (let [[l1 l2] (split-at idx lst)]
    (concat l2 l1)))


(defn inc-idx [idx lst]
  (mod (inc idx) (count lst)))


(defn dec-idx [idx lst]
  (if (= 0 idx)
    (dec (count lst))
    (dec idx)))


(defn print-diff [old new]
  #_(let [[before after _] (diff old new)]
    (if (and (nil? before) (nil? after))
      (println "---- nothing changed")
      (do
        (println "---- before :")
        (clojure.pprint/pprint before)
        (println)
        (println "---- after : ")
        (clojure.pprint/pprint after)))

    #_(println)
    #_(println)))


(defn next-value? [v next-v lst]
  (< (.indexOf lst v) (.indexOf lst next-v)))


(defn update-values [m f]
  ;; f 의 인자는 v
  (reduce (fn [acc k] (update-in acc [k] f)) m (keys m)))


(defn update-values-with-kv [m f]
  ;; f 의 인자는 k,v
  (reduce (fn [acc k] (update-in acc [k] #(f k %))) m (keys m)))


(def ref-type (type (ref 0)))

(defn convert-ref-to-data [m]
  (update-values m (fn [v] (if (= (type v) ref-type)
                             (deref v)
                             v))))


(defn dissoc-data [room]
  (dissoc room :protocol-ch :user-action-ch :cards :stage-info :end-info))


(defn convert-room-for-client [room]
  (-> room
      dissoc-data
      convert-ref-to-data
      (update-in [:users]
                 #(update-values % (fn [user] (dissoc user :is-npc))))))



(defn distribute-delay-card-time
  "preflop의 카드 배포 시간을 조절해서 call 등의 btn 활성화 시간을 변경한다."
  [game-name player-count]
  (condp = game-name
    common/TEXAS    (+ 1000 (* player-count 500))
    common/OMAHA    (+ 1000 (* player-count 1000))
    common/OMAHAHL  (+ 1000 (* player-count 1000))
    (throw (Exception. (str "[ERROR] invalid game-name : " game-name)))
    ) )


(defn delay-restart-time
  "winner 중에 high, low winner 가 있을 경우에 정산 애니매이션 시간이 다르다.
   {:high {a1 150 a2 150}, :low {a3 100}}에서 승자수를 계산한다.
   기본적인 애니매이션 시간 6초. 승자 표시 3초.
   "
  [end-info]
  (let [winner  (:winner @end-info)
        win-cnt (count (apply merge (filter #(seq %)(vals winner))))]

  (+ 7500 (* 3000 win-cnt))))
