(ns vita.poker.room.api.room
  (:require [clojure.data :refer [diff]]
            [clojure.pprint :refer [pprint]]
            [vita.poker.room.api.util :refer [next-value?]]
            [vita.poker.room.api.user :as room.user]
            ))


;;--------------------------------------------------------------------
;; room.api.room 모듈 설명
;;
;; 이 모듈은 room 에 직접 관련있는 ref 값들을 변경하는 함수들의 모임
;; (단, user ref 값은 덩치가 커서 room.api.user 가 관할한다)
;;--------------------------------------------------------------------


;;-----------------------------------------------------------------
;; room schema

(def default-seats* [:seat0 :seat1 :seat2 :seat3 :seat4]) ;; 자릿수

(defn create-room [uuid game-name sb protocol-ch]
  {;; 변경되지 않는 const 값
   :room-id uuid
   :game-name game-name
   :sb sb
   :bb (* 2 sb)
   :seats default-seats*

   ;;--------------
   ;; channels

   :protocol-ch (ref protocol-ch) ;; protocol ch
   :user-action-ch (ref nil)      ;; stage process ch (게임 시작할때 만들어지고 끝나면 없어진다)

   ;;---------------
   ;; seat idx

   :dealer-seat-idx (ref 0)      ;; 딜러 턴
   :turn-seat-idx (ref 0)        ;; 게임 턴

   ;;---------------
   ;; user data     ;; { sid : {user} }

   :users (ref {})

   ;;---------------
   ;; game

   :state (ref :none)       ;; [:start :preflop :flop :turn :river :end]

   :real-time-pot (ref 0)   ;; 베팅시에 바로바로 업뎃되는 pot
   :pot (ref 0)             ;; 라운드 종료시마다 업뎃되는 pot
   :community-cards (ref [])
   :round-bet (ref 0)
   :raise-bet (ref (* 2 sb))

   :last-user-sid (ref nil)
   :cur-user-sid (ref nil)
   :cur-user-select-list (ref nil)

   ;; for debug info
   :cur-user-sid-settime (ref nil)

   ;;----------------
   ;; private info

   :stage-info (ref {:community-cards nil
                     :sid-bet nil
                     :sid-cards nil
                     :sid-score nil})

   ;; 유저에게 보내주는 정보 저장용
   :end-info (ref nil)
   })


;;------------
;; room state

(def room-states* [:none :start :preflop :flop :turn :river :showdown])


(defn reset-room-state [room]
  (ref-set (:state room) :none))


(defn move-to-next-room-state [room]
  (let [idx (.indexOf room-states* @(:state room))
        next-state (first (drop (inc idx) (cycle room-states*)))]
    (ref-set (:state room) next-state)))


(defn start-room-state [room]
  (ref-set (:state room) :start))


(defn end-room-state [room]
  (ref-set (:state room) :end))


(defn is-playing-room-state? [room]
  (not= :none @(:state room)))




;;------------
;; helper


(defn- get-next-seat-idx [users cur-idx seats]
  (let [cur-seat (get seats cur-idx)
        user-seats (keep :seat (vals users))
        ordered (sort user-seats)
        ;; 다음 자리를 찾는다. (다음 자리가 없으면 첫번째 자리부터 다시 시작)
        next-seat (first (filter #(next-value? cur-seat % seats) ordered))
        next-seat (or next-seat (first ordered))]
    (.indexOf seats next-seat)))


(defn get-dealer-seat [room]
  (let [{:keys [seats dealer-seat-idx]} room]
    (get seats @dealer-seat-idx)))


(defn get-dealer-sid [room]
  (let [dealer-user (room.user/find-user-by-seat room (get-dealer-seat room))]
    (:sid dealer-user)))


(defn update-stage-sid-bet [room sid bet]
  (let [{:keys [stage-info]} room]
    (alter stage-info update-in [:sid-bet sid] #(if % (+ % bet) bet))))


;;-----------------------
;; round-bet, raise-bet

(defn init-round-bet [room]
  (let [{:keys [round-bet]} room]
    (ref-set round-bet 0)))


(defn update-round-bet [room bet last-bet]
  (let [{:keys [round-bet]} room]
    (alter round-bet max (+ bet last-bet))))


(defn init-raise-bet [room]
  (let [{:keys [raise-bet bb]} room]
    (ref-set raise-bet bb)))


(defn update-raise-bet [room bet]
  (let [{:keys [raise-bet]} room]
    (alter raise-bet max bet)))


;;----------------------
;; seat idx

(defn init-turn-seat-idx [room]
  (let [{:keys [turn-seat-idx dealer-seat-idx]} room]
    (ref-set turn-seat-idx @dealer-seat-idx)))


(defn update-turn-seat-idx [room]
  (let [{:keys [turn-seat-idx seats]} room
        available-users (room.user/get-bet-available-users room)
        turn-idx (get-next-seat-idx available-users @turn-seat-idx seats)]
    (ref-set turn-seat-idx turn-idx)))


(defn update-stage-seat-idx [room]
  (let [{:keys [users seats dealer-seat-idx]} room
        seat-users (room.user/get-seat-users room)
        next-idx (get-next-seat-idx seat-users @dealer-seat-idx seats)]

    (when (not= -1 next-idx)
      (ref-set dealer-seat-idx next-idx))))
