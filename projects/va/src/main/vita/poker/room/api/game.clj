(ns vita.poker.room.api.game
  ;; WARN!! - dosync 안에서 사용되기 때문에 go 채널같은 부수효과 코드는 넣어선 안된다.
  (:require [vita.session :as session]
            [clojure.tools.logging :as log]
            [sonaclo.util :as util]
            [vita.poker.room.api.room :as room]
            [vita.poker.room.api.user :as room.user]
            [vita.poker.room.api.util :as room.util]
            [vita.poker.room.api.showdown :as room.showdown]
            [vita.db.util.log-sender :as log-sender]
            [vita.db.db-const :as db-const]
            [vita.poker.room.api.util :refer [inc-idx
                                              arrange-by-idx
                                              print-diff
                                              convert-ref-to-data]]))

;;--------------------------------------------------------------------
;; room.api.game 모듈 설명
;;
;; 용어 :: stage -> 포커 한게임, 카드 분배부터 정산까지를 하나의 stage 라고 명명한다.
;;        round -> 하나의 베팅라운드를 의미. preflop 라운드, flop 라운드, ...
;;
;; 이 모듈은 api.room 및 api.user 단위 작업보다 큰 범위의 함수들이다.
;; (예를들어 '유저 베팅'은 room 의 pot 정보도 갱신하면서 user 정보도 갱신해야한다)
;;--------------------------------------------------------------------


(defn get-select-list [room]
  (let [{:keys [round-bet ]} room
        turn-user (room.user/get-turn-user room)
        delta-bet (- @round-bet (:bet turn-user))
        select-list (atom #{})]

    ;; call
    (if (and (< 0 @round-bet) ;; 현재 베팅 금액이 존재하고,
             (<= delta-bet (:money turn-user))) ;; 콜할 금액이 충분하면 call
      (swap! select-list conj :call))

    ;; check :; 플레이 유저들 중에서 돈을 베팅한 사람이 없을때
    (if (= 0 @round-bet)
      (swap! select-list conj :check))

    ;; fold 와 raise 는 항상 들어간다
    (swap! select-list conj :fold :raise)
    @select-list))


;;------------
;; core

(defn join-room [room sid name avatar-url join-time is-npc]
  (room.user/join-user room sid name avatar-url join-time is-npc)
  room)


(defn sit-room [room sid seat buy-in auto-refill]
  (room.user/sit-user room sid seat buy-in auto-refill)
  room)


(defn stand-room [room sid]
  (room.user/stand-user room sid)
  room)


(defn quit-room [room sid]
  (room.user/quit-user room sid)
  room)


(defn bet-room [room sid select delta-bet]

  (when (contains? #{:sb :bb :call :raise :all-in} select)
    (let [{:keys [round-bet raise-bet stage-info]} room
          last-user (room.user/find-user-by-sid room sid)
          last-bet (:bet last-user)
          ;; 가지고 있는 돈보다 더 많은 돈을 걸 수 없다
          money (:money last-user)
          delta-bet (min delta-bet money)
          select (if (= delta-bet money) :all-in select)]

      ;; 룸의 round-bet, raise-bet, stage-info 업뎃
      (room/update-stage-sid-bet room sid delta-bet)
      (room/update-raise-bet room delta-bet)
      (room/update-round-bet room delta-bet last-bet)))

    ;; 유저의 베팅 처리
  (if (contains? #{:sb :bb :call :raise :all-in} select)

    (let [last-user (room.user/find-user-by-sid room sid)
          ;; 가지고 있는 돈보다 더 많은 돈을 걸 수 없다
          money (:money last-user)
          delta-bet (min delta-bet money)
          select (if (= delta-bet money) :all-in select)]
      (room.user/bet-user room sid select delta-bet))

    (room.user/bet-user room sid select delta-bet))

  room)



(defn bet-end-room [room]
  (let [{:keys [dealer-seat-idx
                turn-seat-idx
                pot
                stage-info
                cur-user-sid
                cur-user-sid-settime
                cur-user-select-list
                last-user-sid]} room]

    ;; pot 갱신
    (let [sid-bet (:sid-bet @stage-info)]
      (ref-set pot (reduce + (vals sid-bet))))

    ;; turn idx 초기화
    (ref-set turn-seat-idx @dealer-seat-idx)

    (ref-set cur-user-sid nil)
    (ref-set cur-user-select-list nil)
    (ref-set cur-user-sid-settime nil)
    (ref-set last-user-sid nil)

    ;; 유저들 라운드 초기화
    (room.user/play-users-round-init room))
  room)


(defn next-turn [room]
  ;; last-user-sid 갱신
  (let [{:keys [last-user-sid cur-user-sid]} room]
    (ref-set last-user-sid @cur-user-sid))

  (room/update-turn-seat-idx room)

  ;; cur-user-sid,
  ;; cur-user-select-list 갱신
  (let [{:keys [cur-user-sid cur-user-select-list cur-user-sid-settime]} room
        cur-user (room.user/get-turn-user room)]
    (ref-set cur-user-sid (:sid cur-user))
    (ref-set cur-user-sid-settime (str (util/now)))
    (ref-set cur-user-select-list (get-select-list room)))

  room)


(defn next-round [room]
  (let [{:keys [state community-cards bb round-bet stage-info]} room]

    ;; 룸 state 변경
    (room/move-to-next-room-state room)

    ;; turn-idx 초기화
    (room/init-turn-seat-idx room)

    ;; round raise bet  초기화
    (room/init-round-bet room)
    (room/init-raise-bet room)

    ;; 카드 업뎃
    (condp = @state

      :preflop
      (let [play-users (room.user/get-play-users room)
            [sid-cards sid-score community-cards winner-sid] (room.showdown/hand-out-cards (:game-name room) play-users)]

        ;; 유저 카드 분배
        (room.user/hand-out-preflop-cards room sid-cards)

        ;; 포커 결과 저장
        (alter stage-info #(assoc % :sid-cards sid-cards
                                  :sid-score sid-score
                                  :community-cards community-cards
                                  :winner-sid winner-sid
                                  )))

      :flop
      (ref-set community-cards (vec (take 3 (get @stage-info :community-cards))))

      :turn
      (ref-set community-cards (vec (take 4 (get @stage-info :community-cards))))

      :river
      (ref-set community-cards (vec (take 5 (get @stage-info :community-cards))))

      nil
      ))

  ;; 유저 라운드 초기화
  (room.user/play-users-round-init room)

  room)


(defn next-stage [room]

  ;; end-info 초기화
  (ref-set (:end-info room) nil)

  ;; 딜러 자리 업뎃
  (room/update-stage-seat-idx room)

  ;; turn-idx 초기화
  (room/init-turn-seat-idx room)

  ;; 앉은 유저들 게임 준비시키키
  (let [users (room.user/play-users-stage-init room)]
    ;; stage-info 의 sid-bet 을 초기화
    (alter (:stage-info room) assoc :sid-bet (room.util/update-values users (constantly 0))))

  room)


(defn sb-bb-betting [room]

  (let [{:keys [sb bb]} room]

    ;; 턴넘기기
    (next-turn room)

    ;; sb
    (bet-room room @(:cur-user-sid room) :sb sb)

    ;; 턴넘기기
    (next-turn room)

    ;; bb
    (bet-room room @(:cur-user-sid room) :bb bb)
    )

  room)


(defn is-card-open-end?
  "WARN :: 이미 종료된 이후에 사용해야 하는 함수"
  [room]
  (let [end-users (room.user/get-users-by-state room :all-in :wait)]
    (<= 2 (count end-users))))


(defn update-last-stage-money [room]
  (let [play-users (room.user/get-play-users room)]

      ;; 참가자들 money 변경값을 세션에 적용
      (doseq [[sid {:keys [money last-stage-money]}] play-users]
        ;; 게임하다가 나갔다가 다시 앉은 사람은 제외
        (when (and money last-stage-money)
          (let [delta-money (- money last-stage-money)
                holding-money (session/sid->usermoney sid)
                game-name   (:game-name room)]
            (session/update-account-money sid delta-money)

            (log-sender/money sid delta-money game-name holding-money nil db-const/IN-GAME) )))

      ;; last-stage-money 값 새로 갱신
      (room.user/update-users-last-stage-money room)))


(defn refill-users [room]
  (let [refill-users (room.user/get-need-refill-users room)]
      (doseq [[sid user] refill-users]
        (let [buy-in (:buy-in user)
              acc-money (session/sid->usermoney sid)]
          (when (<= buy-in acc-money)
            (room.user/refill-user room (:sid user) buy-in))))))


(defn stage-clean [room]
  (let [{:keys [community-cards stage-info
                round-bet raise-bet pot real-time-pot bb
                cur-user-sid cur-user-select-list last-user-sid]} room]
    (ref-set stage-info nil)
    (ref-set real-time-pot 0)
    (ref-set pot 0)
    (ref-set community-cards [])
    (ref-set round-bet 0)
    (ref-set raise-bet bb)

    (ref-set last-user-sid nil)
    (ref-set cur-user-sid nil)
    (ref-set cur-user-select-list nil)

    ;; 유저 상태 초기화
    (room.user/play-users-stage-over room)
    ))


(defn stage-end [room]

  (let [{:keys [game-name users stage-info end-info]} room
        sit-bet (:sit-bet @(:stage-info room))]

    ;;------------
    ;; 1. 정산 계산
    ;; stage-info 를 가지고 정산처리한 결과를 end-info 에 저장.
    ;; stage-info 는 비운다
    (ref-set end-info (room.showdown/calc game-name @users @stage-info (is-card-open-end? room)))

    ;;-----------
    ;; 2. 정산결과 winner-money 를 user money 에 적용
    (room.user/apply-winner-money room (:winner @end-info) (:overage @end-info))

    ;;-----------
    ;; 3. last-stage-money 를 사용해서 세션 계정돈 업뎃 (db 업뎃은 세션에서 관리해준다)
    (update-last-stage-money room)

    ;;-----------
    ;; 4. 기타 룸 정보들 초기화
    (stage-clean room)

    ;;------------
    ;; 5. 리필 유저 처리
    (refill-users room))

  room)


(defn round-end? [room]
  (let [{:keys [round-bet]} room]

    (cond

     ;; 한명이라도 wait 가 있으면 베팅 안끝났다.
     (<= 1 (count (room.user/get-users-by-state room :wait)))
     false

     ;; 한번씩은 다 베팅한 상태에서...
     ;; 베팅 가능한 유저들(폴드, 올인 제외)의 베팅 금액이 모두 round-bet 으로 같다면 라운드 종료이다.
     (let [bet-available-users (room.user/get-bet-available-users room)]
       (every? true? (map #(= @round-bet (:bet %)) (vals bet-available-users))))
     true

     :else
     false)))


(defn stage-end? [room]
  (let [{:keys [state round-bet]} room
        play-users-cnt      (count (room.user/get-play-users room))
        bet-available-player    (room.user/get-bet-available-users room)
        avaible-player-cnt  (count bet-available-player)
        player-bets (map #(:bet %) (vals bet-available-player))
        play-min-bet (if (empty? player-bets)
                       0
                       (apply min player-bets))]

    (when (or
            ;; river 까지 다 하고 베팅도 다 끝났을때
            (and (= :river @state) (round-end? room))

            ;; 할사람이 없을경우 (다 fold, all-in)
            (= 0 (count (room.user/get-users-by-state room :wait :call :raise :check)))

            ;; 1명 빼고 다 fold 일 경우
            (= (dec play-users-cnt) (count (room.user/get-users-by-state room :fold)))

            ;; 게임 가능한 유저(call,raise, check)가 한명일 경우
            (and (<= avaible-player-cnt 1)
                 ;allin의 최대값(round-bet)이 현재 자기값보다 작은 경우 stage 끝낸다
                 (<= @round-bet play-min-bet)))
      true)))


(defn stage-flag-on [room]
  ;; room state 시작 플래그 변경으로 따른 쓰레드에서 시작 못하게 막는다
  (room/start-room-state room)
  room)


(defn stage-flag-off [room]
  (room/reset-room-state room)
  room)


(defn stage-start-setting [room]

  ;; 다음 게임을 위한 초기화 및 준비
  (next-stage room)

  room)
