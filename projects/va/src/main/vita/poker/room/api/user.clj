(ns vita.poker.room.api.user
  (:require [clojure.data :refer [diff]]
            [clojure.pprint :refer [pprint]]
            [clojure.core.async :as async :refer [<! >! alts! put! timeout chan go close! thread]]
            [vita.poker.room.api.util :refer [same-kv? print-diff]]))


;;--------------------------------------------------------------------
;; room.api.user 모듈 설명
;;
;; 이 모듈은 room 의 users ref 값을 변경을 담당한다
;;
;; 용어 ::
;; play-user          게임에 참가한 유저 (state 값 존재 : 베팅, 올인, 폴드 유저 모두 포함)
;; alive-user         마지막까지 게임 포기하지 않고 남은 사람 (정산 대상이 됨 : 베팅유저, 올인유저)
;; bet-available-user 베팅 가능한 참가자 (올인 유저 제외)
;;--------------------------------------------------------------------


(def default-user*
  {;; 기본정보
   :sid nil
   :name nil
   :avatar-url nil
   :join-time nil
   :is-npc nil

   ;; 앉을때 정보
   :seat nil
   :money nil
   :last-stage-money 0  ;; 이전 스테이지 머니
   :buy-in nil
   :auto-refill nil

   ;; 플레이 정보 - 게임시작할때마다 초기값으로 세팅되고, 끝나면 모두 nil 로...
   :state nil   ;; [ :wait :call :raise :check :fold :all-in ]
   :bet nil
   :cards nil
   })


(defn create-user [& kv]
  ;; todo kv validator 필요
  (apply assoc default-user* kv))

;;----------------------------
;; 여기 있는 함수들은 데이터형태의 users 를 인자로 받고 users (혹은 수정된 users) 를 반환해야한다.

(defn add-user
  [users & kv]
  ;; TODO kv validator 필요
  (let [new-user (apply create-user kv)]
    (assoc users (:sid new-user) new-user)))


(defn select-user-by
  [users & select-fns]
  (let [every-pass? (fn [user]
                      (every? boolean (map #(% user) select-fns)))]
    (reduce (fn [acc [sid user]]
              (if (every-pass? user)
                (assoc acc sid user)
                acc))
            {}
            users)))


(defn select-user-by-sid
  [users & sids]
  (select-keys users sids))


(defn select-user
  [users & kv]
  (select-user-by users #(apply same-kv? % kv)))


(defn remove-user-by-sid
  [users & sids]
  (with-meta (apply dissoc users sids) {:commit :reset}))


(defn remove-user
  [users & kv]
  (let [sids (keys (apply select-user users kv))]
    (apply remove-user-by-sid sids)))


(defn update-user-by
  "user 단위의 업데이트 함수를 적용한다"
  [users & update-fns]
  (let [user-update-fn (fn [user] (reduce #(%2 %1) user update-fns))]
    (reduce (fn [acc [sid user]]
              (assoc acc sid (user-update-fn user)))
            {}
            users)))


(defn reset-user
  "user 의 특정 값을 새값으로 바꾼다."

  [users & kv]
  ;; TODO kv validator 필요
  (let [user-update-fn (fn [user] (apply assoc user kv))]

    (reduce (fn [acc [sid user]]
              (assoc acc sid (user-update-fn user)))
            {}
            users)))


(defn reset-user-with-nil
  "user 의 특정 키워드들(ks) 값을 nil 로 바꾼다."

  [users & ks]
  ;; TODO kv validator 필요
  (let [k-nil (flatten (map list ks (repeat nil)))]
    (apply reset-user users k-nil)))


(defn reset-commit-users
  [new-users users]
  (ref-set users new-users))


(defn merge-commit-users
  [new-users users]
  (alter users #(merge % new-users)))


(defn commit-users
  [new-users users]
  (let [commit (:commit (meta new-users))]
    (if (= commit :reset)
      (reset-commit-users new-users users)
      (merge-commit-users new-users users))))


(defmacro with-users
  "연속된 처리 후, 마지막에 자동으로 commit 까지 해준다."
  [users & body]
  `(-> (deref ~users)
       ~@body
       (commit-users ~users)))


;; for debug
#_(defmacro with-users [users & body]
 `(let [users-before# (deref ~users)]
    (-> (deref ~users)
        ~@body
        (commit-users ~users))

    (println " ========== with users :: ")
    (~print-diff users-before# (deref ~users))))


;;------------------------------------------------------
;; get :: RETURN 은 모두 순수 데이터형태의 user OR users 이다.
;; (단, ? 로 끝나는 predicate 의 리턴값은 BOOLEAN 이다)

(defn find-user-by-seat [room seat]
  (first (vals (select-user @(:users room) :seat seat))))


(defn find-user-by-sid [room sid]
  (first (vals (select-user-by-sid @(:users room) sid))))


(defn get-users-by-state [room & states]
  (let [users (:users room)]
    (select-user-by @users (fn [user] (contains? (set states) (:state user))))))


(defn get-play-users
  "게임에 참가한 유저 목록 (:state 가 nil 이 아니다)
  :fold :all-in :wait :call :raise :check가 다 포함된 유저인다."
  [room]
  (let [users (:users room)]
    (select-user-by @users :state)))

(defn get-play-users-sid
  "('c263b862-xxx' '7547162f-xxx')"
  [room]
  (keys (get-play-users room)))

(defn get-bet-available-users
  [room]
  (get-users-by-state room :wait :call :raise :check))


(defn get-seat-users [room]
  (let [users (:users room)]
    (select-user-by @users :seat)))


(defn get-turn-seat [room]
  (let [{:keys [seats turn-seat-idx]} room]
    (get seats @turn-seat-idx)))


(defn get-turn-user [room]
  (let [seat (get-turn-seat room)]
    (find-user-by-seat room seat)))


(defn get-turn-sid [room]
  (:sid (get-turn-user room)))


(defn turn-user?
  [room sid]
  (= sid @(:cur-user-sid room)))


(defn get-need-refill-users
  "auto-refill 값이 true 이면서 돈이 bb 보다 적은 유저들"
  [room]
  (let [{:keys [users bb]} room]
    (-> @users
        (select-user-by #(and (:auto-refill %)
                              (< (:money %) bb))))))

;;-------------------------------------------------
;; modify  :: 리턴값은 모두 순수 데이터 형태의 users 이다


(defn join-user [room sid name avatar-url join-time is-npc]
  (let [{:keys [users]} room]
    (with-users users
      (add-user :sid sid :name name :avatar-url avatar-url :join-time join-time :is-npc is-npc))))


(defn sit-user [room sid seat buy-in auto-refill]
  (let [{:keys [users]} room]
    (with-users users
      (select-user-by-sid sid)
      (select-user-by #(not (:seat %)))
      (reset-user :seat seat :money buy-in :last-stage-money buy-in :buy-in buy-in :auto-refill auto-refill))))


(defn stand-user [room sid]
  (let [{:keys [users]} room]
    (with-users users
      (select-user-by-sid sid)
      (reset-user-with-nil :state :seat :money :buy-in :auto-refill))))


(defn quit-user [room sid]
  (let [{:keys [users]} room]
    (with-users users
      (remove-user-by-sid sid))))


(defn hand-out-preflop-cards [room user-cards]
  (let [{:keys [users]} room]
    ;; 카드 분배
    (with-users users
      (select-user-by :state)
      (update-user-by (fn [user] (let [cards (get user-cards (:sid user))]
                                   (assoc user :cards cards)))))))


(defn play-users-round-init [room]
  (let [{:keys [users]} room]
    ;; 플레이 중인 유저들 라운드 초기화
    (with-users users
      (select-user-by :state)
      (reset-user :bet 0)
      (select-user-by #(not (contains? #{:fold :all-in} (:state %))))
      (reset-user :state :wait))))


(defn play-users-stage-init [room]
  (let [{:keys [users bb]} room]
    ;; 앉은 유저들 (돈은 bb 보다 커야함) 준비 시키기
    (with-users users
      (select-user-by :seat)
      (select-user-by #(< bb (:money %)))
      (reset-user :state :wait :bet 0 :cards []))))


(defn play-users-stage-over [room]
  (let [{:keys [users]} room]
    (with-users users
      (select-user-by :state)
      (reset-user :state nil :bet nil :cards nil))))


(defn bet-user [room sid select delta-bet]

  (let [users (:users room)
        last-user (find-user-by-sid room sid)]

    ;; 유저 베팅 금액 변경
    (case select

      (:sb :bb)
      (with-users users
        (select-user-by-sid sid)
        (update-user-by (fn [user]
                          (let [{:keys [bet money]} user]
                            (assoc user :state :wait :bet (+ bet delta-bet) :money (- money delta-bet))))))

      (:call :raise :all-in)
      (with-users users
        (select-user-by-sid sid)
        (update-user-by (fn [user]
                          (let [{:keys [bet money]} user]
                            (assoc user :state select :bet (+ bet delta-bet) :money (- money delta-bet))))))

      :fold
      (with-users users
        (select-user-by-sid sid)
        (update-user-by (fn [user]
                          (assoc user :state select :bet 0))))

      :check
      (with-users users
        (select-user-by-sid sid)
        (reset-user :state select)))))


(defn apply-money [room sid-delta-money]
  (with-users (:users room)
    (select-user-by :state)
    (#(apply select-user-by-sid % (keys sid-delta-money)))
    (update-user-by (fn [user]
                      (let [{:keys [money sid]} user]
                        (assoc user :money (+ money (sid-delta-money sid))))))))


(defn apply-winner-money [room winner overage]
  (let [[money sid] overage
        sum-money (merge-with + (:high winner) (:low winner) {sid money})]
    (apply-money room sum-money)))


(defn refill-user [room sid refill-money]
  (with-users (:users room)
    (select-user-by-sid sid)
    (update-user-by #(let [money (:money %)
                           new-money (+ money refill-money)]
                      (assoc % :money new-money :last-stage-money new-money)))
    ))



(defn update-user-last-stage-money
  ;; 다음에 사용하기 위해 last-stage-money 업뎃
  [users sid]
  (with-users users
    (select-user-by-sid sid)
    (update-user-by (fn [user] (let [{:keys [last-stage-money money]} user
                                     delta-money (- money last-stage-money)]
                                 (assoc user :last-stage-money money))))))


(defn update-users-last-stage-money [room]
  (let [{:keys [users]} room]
    (doseq [[sid user] @users]
      (when (and (not (:is-npc user))
                 (:state user))
        (update-user-last-stage-money users sid)))))
