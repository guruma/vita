(ns vita.poker.room.stage-manager
  (:require [clojure.core.async :as async :refer [<! <!! >! alts! put! take! timeout chan go close!]]
            [clojure.tools.logging :as log]
            [vita.poker.room.api.game :as room.game]
            [vita.poker.room.api.util :as room.util]
            [vita.poker.room.api.user :as room.user]
            [vita.poker.room.api.room :as room]
            [vita.poker.room.net.stage-send :as sender]
            [vita.config.macros :as cm]
            [sonaclo.shared.util :as util]
            [vita.session :as session]
            [vita.db.util.log-sender :as log-sender]
            [vita.poker.holdem.net.q-quit :as q-quit]
            ))


;;--------------------------------------------------------------------
;; room.stage-manager 모듈 설명
;;
;; 용어 :: stage -> 포커 한게임, 카드 분배부터 정산까지를 하나의 stage 라고 명명한다.
;;
;; 이 모듈은 stage-loop 를 관리한다.
;; 게임 시작시 loop 가 시작하고, 정산하면서 끝난다
;;--------------------------------------------------------------------


(defn- get-play-available-users [room]
  (let [{:keys [users bb]} room]
    ;; 플레이 가능한 유저는 앉아있고, 돈이 bb 보다 커야한다.
    (-> (room.user/select-user-by @users :seat)
        (room.user/select-user-by #(< bb (:money %))))))


(defn stage-start-available? [room]
  ;; 게임중이 아니고, 2 명 이상이 앉아있을때 (돈이 bb 머니 이상)
  (let [{:keys [state users bb]} room
        play-available-users (get-play-available-users room)]
    (and (= @state :none) (< 1 (count play-available-users)))))


;;---------------------
;; stage core func

(defn stage-start [room ch]

  (go
    (dosync
      ;; 게임 시작 세팅
      (room.game/stage-start-setting room)

      ;; preflop 단계로 이동
      (room.game/next-round room)

      ;; sb-bb-setting
      (room.game/sb-bb-betting room))

    ;; [B] Bstart
    (sender/send-Bstart room)

    ;; sb, bb 자동 베팅 시간
    (<! (timeout 1000))

    ;; [B] preflop
    (sender/send-Bround room)

    ;; 카드 분배 시간
    (let [game-name   (:game-name room)
          player-cnt  (count (room.user/get-play-users room))
          delay-time  (room.util/distribute-delay-card-time game-name player-cnt)]
      (<! (timeout delay-time)))  ) )


(defn- npc-users [room]
  (room.user/select-user-by @(:users room) :is-npc))

(defn- npc-out
  [sid room]
  (let [quit-packet {:cmd :Qquit :data {:room-id (:room-id room)} :scene (:game-name room) :tk ""}]
    (when-let [ch (:protocol-ch room)]
      (put! @ch (assoc quit-packet :sid sid)))))


(defn remove-npc-after-stage-end [room]
  (doseq [sid (keys (npc-users room))]
    (when (util/probility (get-in cm/config* [:npc :logout-probility]))
      (npc-out sid room) )))


(defn remove-npc-not-enough-money
  [room]
  (let [bb    (:bb room)]
    (doseq [sid (keys (npc-users room))]
      (let [npc   (room.user/find-user-by-sid room sid)]
        (when (and (sonaclo.shared.util/not-nil? npc)
                   (sonaclo.shared.util/not-nil? (:money npc))
                   (<= (:money npc) bb))
          (npc-out sid room)))  )))

(defn stage-end [room]
  ;; stage 종료 (with 정산처리)
  (dosync (room.game/bet-end-room room)
          (room.game/stage-end room))

  ;; npc 확률적으로 나가기
  (remove-npc-after-stage-end room)
  #_(remove-npc-not-enough-money room))


(defn action-from-ch [room v]

  (cond

    ;; 타임아웃 이라면 폴드 액션 처리
    (= :timeout v)
    (dosync (room.game/bet-room room @(:cur-user-sid room) :fold nil))

    ;; 유저가 일어나거나 나감으로 해서 게임 종료되었음을 알려옴
    (= :end v)
    (log/info "user stand or quit => end")

    (= :pass v)
    nil

    ;; 유저 액션
    (and (= (type []) (type v)) (= 3 (count v)))
    (let [[select sid bet] v]
      ;; cur-user-sid 맞는지 체크
      (if-not (= sid @(:cur-user-sid room))
        (log/warn "not correct bet sid " select sid bet)

        (let [nil-agent (agent nil)]
          (dosync
            (if (not (contains? #{:standup-fold :quit-fold} select))
              (room.game/bet-room room sid select bet)))

          ;; 다음 베팅 Bbet 할때 이번 유저 액션 결과를 알려주지만,
          ;; 라운드 마지막 베팅이면 따로 알려야 한다
          (when (room.game/round-end? room)
            (dosync (let [{:keys [cur-user-sid last-user-sid]} room]
                      (ref-set last-user-sid @cur-user-sid)
                      (ref-set cur-user-sid nil)))
            (sender/send-Bbet room )))))

    :else
    (log/warn "unexpected user action = " v)))


;;-----------------------
;; game thread

(declare start-check-&-run)


(defn stage-loop [room]

  (go

    ;; 게임 채널 초기화
    (let [user-action-ch (chan 10)
          timeout-ch (atom (chan))]

      (try

        ;; user-action-ch 룸에 저장
        ;; 유저들이 베팅 액션을 취할때마다 결과를 이 채널을 통해 알려온다
        (dosync (ref-set (:user-action-ch room) user-action-ch))

        ;; stage 준비
        (<! (stage-start room user-action-ch))

        ;; 게임 참여했다른 로그 기록
        (go
          (log-sender/play-game-sids (room.user/get-play-users-sid room) (:game-name room) ))

        ;; sb,bb 자동베팅 되었음으로 첫번째는 통과할 수 있게 넣어준다
        (>! user-action-ch :pass)

        ;; stage 진행
        (loop []
          ;; 채널을 통해 외부의 액션을 받을 때까지 대기...
          (let [[v ch] (alts! [user-action-ch @timeout-ch])]

            ;; 사용끝난 timeout 용 채널을 새것으로 교체한다
            (close! @timeout-ch)
            (reset! timeout-ch (chan))

            ;; ch 에서 나온 값 처리 (통과 이거나 유저 베팅)
            (action-from-ch room v)

            ;; stage 종료가 아니면 계속 진행
            (when-not (room.game/stage-end? room)
              ;; 다음사람 베팅할 차례인지, 라운드 종료인지?
              (if (room.game/round-end? room)

                (do
                  ;; 베팅 종료
                  (<! (timeout 1000))
                  (dosync (room.game/bet-end-room room))
                  (sender/send-Bbetend room)
                  (<! (timeout 1000))

                  ;; 다음 라운드 이동
                  (dosync (room.game/next-round room))
                  (sender/send-Bround room)
                  (<! (timeout 1000))

                  ;; pass
                  (>! user-action-ch :pass))

                (do
                  ;; 다음 Bbet 으로 이동
                  (dosync (room.game/next-turn room))
                  (sender/send-Bbet room)

                  (go (let [old-timeout-ch @timeout-ch]
                        (alts! [old-timeout-ch (timeout 15000)])
                        (put! old-timeout-ch :timeout))))
                )
              (recur))))

        ;; 채널 정리
        (if-let [ch @timeout-ch]
          (close! ch))
        (close! user-action-ch)
        (dosync (ref-set (:user-action-ch room) nil))

        (stage-end room)
        (log/info "stage end!!!" room)

        (<! (timeout 1000))

        ;; [B] Bend
        (sender/send-Bend room)
        ;; 승자 로그
        (log-sender/winner (:winner @(:end-info room)) (:game-name room))



        (catch Exception e
          (log/warn "stage loop error!!")
          (.printStackTrace e)
          ;; room 초기화
          (dosync (let [sid-bet (:sid-bet @(:stage-info room))]
                    ;; bet 돌려줌
                    (room.user/apply-money room sid-bet)
                    (room.game/stage-clean room)
                    (ref-set (:end-info room) nil)
                    ))
          (sender/send-Bend room))

        (finally
          ;; 시간차 두고 다시 확인
          (<! (timeout (room.util/delay-restart-time (:end-info room))))
          (dosync (room.game/stage-flag-off room))

          ;; 남아있는 유저들 중에, session 값이 이상있는 유저들을 강퇴시킨다
          (doseq [sid (keys @(:users room))]
            (let [rid (session/sid->rid sid)]
              (when (not= rid (:room-id room))
                (q-quit/req-quit room sid nil))))

          (start-check-&-run room))))))



(defn start-check-&-run [room]
  (let [a (agent nil)]

    (dosync

      (when (and (not (room/is-playing-room-state? room))
                 (stage-start-available? room))
        ;; state 플래그 온
        (room.game/stage-flag-on room)

        ;; 게임 처리 쓰레드 시작
        (send a (fn [_] (stage-loop room)))))))
