(ns vita.poker.calc.score
  "목  적: 사용자 한 명의 카드 패에 대한 점수를 계산한다.
   작성자: 김영태"
  (:require [clojure.string :as str]
            [clojure.set :as set]
            [clojure.math.combinatorics :as combo] ))

(def ^:dynamic *ranks*
  {:2 2 :3 3 :4 4 :5 5 :6 6 :7 7 :8 8 :9 9 :10 10 :J 11 :Q 12 :K 13 :A 14})

(def ^:private suits*
  {:club 1 :heart 2 :diamond 3 :spade 4})

(def category*
  {:high-card      0 :one-pair             1 :two-pair    2     :three-of-a-kind 3
   :straight       4 :flush                5 :full-house  6     :four-of-a-kind  7
   :straight-flush 8 :royal-straight-flush 9 :low-card   -1})

(defn- split-card
  "<card kw>
   <return (suit kw, rank kws)>

   :heart-K => (:heart :K)"
  [card]
  (map keyword (str/split (name card) #"-")))

(defn- card->rank%
  "<card kw>
   <return rank kw>

   :heart-K => :K"
  [card]
  (second (split-card card)))

(defn- card->suit%
  "<card kw>
   <return suit kw>

   :heart-K => :heart"
  [card]
  (first (split-card card)))

(def card->rank (memoize card->rank%))
(def ^:private card->suit (memoize card->suit%))


;; -------------------
;; 정렬 관련 함수

(defn- cmp-ranks
  "<f fn> comparator function: < or >
   <rank-a kw>
   <rank-b kw>
   <return int> -1 if rank-a is less than rank-b
                 0 if rank-a is equal to rank-b
                 1 if rank-a is greater than rank-b"
  [f rank-a rank-b]
  (let [a (*ranks* rank-a)
        b (*ranks* rank-b)]
    (cond
      (f a b) -1
      (= a b) 0
      :else   1)))

(defn- sort-ranks
  "<f fn> comparator function: < or >
   <ranks (<rank kw>+)>
   <return (<rank kw>+)> sorted ranks

   (sort-ranks < [:2 :7 :5 :J :K :K :Q :J :A])
   => (:2 :5 :7 :J :J :Q :K :K :A)

   (sort-ranks > [:2 :7 :5 :J :K :K :Q :J :A])
   => (:A :K :K :Q :J :J :7 :5 :2)"
  [f ranks]
  (sort #(cmp-ranks f % %2) (vec ranks)))

(defn- cmp-suits
  "<f fn> comparator function: < or >
   <suit-a kw>
   <suit-b kw>
   <return int> -1 if suit-a is less than suit-b
                 0 if suit-a is equal to suit-b
                 1 if suit-a is greater than suit-b"
  [f suit-a suit-b]
  (let [a (suits* suit-a)
        b (suits* suit-b)]
    (cond
      (f a b) -1
      (= a b) 0
      :else   1)))

(defn- sort-suits
  "<f fn> comparator function: < or >
   <suits (<suit kw>+)>
   <return (<suit kw>+)> sorted suits

   (sort-suits < [:diamond :spade :club :heart :spade :club])
   => (:club :club :heart :diamond :spade :spade)

   (sort-suits > [:diamond :spade :club :heart :spade :club])
   => (:spade :spade :diamond :heart :club :club)"
  [f suits]
  (sort #(cmp-suits f % %2) (vec suits)))

(defn- cmp-scores
  "<cmp-fn fn> comparator function: < or >
   <score-a [category kw, [<rank kw>+]]>
   <score-b [category kw, [<rank kw>+]]>"
  [cmp-fn score-a score-b]
  (let [a [(category* (first score-a)) (vec (map *ranks* (second score-a)))]
        b [(category* (first score-b)) (vec (map *ranks* (second score-b)))]]
    (cond
      (= cmp-fn <) (compare a b)
      (= cmp-fn >) (compare b a) )))

(defn- sort-scores
  "사용자 한 명의 카드 패에 대한 모든 가능한 점수들을 대상으로 비교 정렬한다.
   <cmp-fn fn> comparator function: < or >
   <scores [[category kw, [<rank kw>+]]+]>
   <return scores [[category kw, [<rank kw>+]]+]> soreted scores

   scores => [[:high-card [:Q :K :10 :9 :5]]
              [:one-pair  [:10 :10 :8 :7 :1]]
              [:high-card [:Q :K :J :9 :3]]
              [:one-pair  [:10 :10 :6 :5 :4]] ])

   (sort-scores < scores)
   => [[:high-card [:Q :K :10 :9 :5]]
       [:high-card [:Q :K :J :9 :3]]
       [:one-pair [:10 :7 :6 :5 :4]]
       [:one-pair [:10 :9 :8 :7 :1]]]

   (sort-scores > scores)
   => [[:one-pair [:10 :9 :8 :7 :1]]
       [:one-pair [:10 :7 :6 :5 :4]]
       [:high-card [:Q :K :J :9 :3]]
       [:high-card [:Q :K :10 :9 :5]]]"
  [f scores]
  (vec (sort #(cmp-scores f %1 %2) scores)))

(defn- arrange-hand-by-rank
  "score의 :hand 영역에 나열되어 있는 카드들 중에서, :ranks 영역에 나열된 순서대로
   5장의 카드를 나열한다.
   <score [category kw, [<rank kw>+] [<card kw>+]]>
   <return hand [<card kw>]>

   [:straight [:A :K :Q :J :10]
    [:heart-A :diamond-A :heart-K :club-5 :club-Q :spade-J :spade-10]]
   => [:heart-A :heart-K :club-Q :spade-J :spade-10]"
  [score]
  (loop [ranks (second score)
         cards (get score 2)
         acc   []]
    (if (empty? ranks)
      acc
      (let [rank  (first ranks)
            card  (some #(if (= rank (card->rank %))
                           %)
                        cards)
            acc   (conj acc card)
            cards (remove #(= card %) cards)]
        (recur (rest ranks) cards acc) ))))

;; --------------------
;; 점수 계산 관련 함수

(defn- group-by-rank
  "#ranks [<rank kw>+] ranks of one hand (in descended order)
   #return [[count int, rank kw]+]>

   [:7 :7 :5 :5 :3] => [[2 :7] [2 :5] [1 :3]]
   [:7 :7 :7 :5 :3] => [[3 :7] [1 :5] [1 :3]]
   [:7 :7 :7 :7 :3] => [[4 :7] [1 :3]]
   [:7 :5 :4 :3 :2] => [[1 :7] [1 :5] [1 :4] [1 :3] [1 :2]]"
  [ranks]
  (let [freqs (frequencies ranks)]
    (vec (map (fn [[fst snd]] [snd fst])
              (sort-by second > freqs) ))))

(defn- kickers
  "category가 동일할 떄, 승부를 결정짓는 kicker 부분만을 추출한다.
   <groups [[count int, rank kw]+]>
   <drop-count int>
   <take-count int>
   <return kickers [[count int, rank kw]+]>"
  [groups drop-count take-count]
  (vec (map (fn [[_ rank]] rank)
            (take take-count (drop drop-count groups)) )))

(defn- check-flush
  "<cards [<card kw>+]> the names of cards
   <return [category kw, [<rank kw>+] [<card kw>+]]>

   [:heart-A :heart-K :heart-4 :diamond-6 :heart-10 :heart-8 :heart-6]
   => [:flush [:A :K :10 :8 :6]
              [:heart-A :heart-K :heart-4 :diamond-6 :heart-10 :heart-8 :heart-6]]"
  [cards]
  (when (>= (count cards) 5)
    (let [suits (sort-suits > (map card->suit cards))
          freqs (frequencies suits)]
      (some (fn [[suit freq]]
              (if (>= freq 5)
                (let [cards' (filter #(= suit (card->suit %))
                                     cards)
                      ranks (take 5 (sort-ranks > (map card->rank cards')))]
                  [:flush (vec ranks) (filter #(= (card->suit %) suit)
-                                             cards) ])))
            freqs) )))

(defn- straight?
  "<ranks (<rank kw>+))> the ranks of one hand (in descended order)
   <return bool>

   [:10 :9 :8 :7 :6] => true
   [:10 :9 :7 :6 :5] => false"
  [ranks]
  (let [pairs (partition 2 1 (map *ranks* ranks))]
    (every? (fn [[n1 n2]] (= (dec n1) n2))
            pairs) ))

(defn- check-straight
  "<cards (<card kw>+)> the names of cards (in descended order)
   <return score [category kw, [<rank kw>+] [<card kw>+]]>

   [:club-A :spade-8 :club-7 :diamond-6 :heart-5 :club-K :spade-4]
   => [:straight [:8 :7 :6 :5 :4]
                 [:club-A :spade-8 :club-7 :diamond-6 :heart-5 :club-K :spade-4]]

   [:heart-2 :diamond-A :heart-K :club-5 :club-Q :spade-J :spade-10]
   => [:straight [:A :K :Q :J :10]
                 [:heart-2 :diamond-A :heart-K :club-5 :club-Q :spade-J :spade-10]]"
  [cards]
  (when (>= (count cards) 5)
    (let [ranks (sort-ranks > (seq (set (map card->rank cards))))]
      (binding [*ranks* (if (set/subset? #{:5 :4 :3 :2} (set ranks))
                          (assoc *ranks* :A 1)
                          *ranks*)]
        (let [ranks' (sort-ranks > ranks)
              hands  (partition 5 1 ranks')]
          (some (fn [hand]
                  (if (straight? hand)
                    [:straight (vec hand) cards] ))
                hands) )))))

(defn- calc-score-by-high
  "<cards (<card kw>+)> 주어질 수 있는 cards 개수는 다음과 같다.
                        (texas: 2, 5, 6, or 7;   omaha: 2 or 5;   omaha-hl: 2 or 5)
   <return score [category kw, [<rank kw>+] [<card kw>+]]>"
  [cards]
  (let [flush       (check-flush cards)
        straight    (check-straight cards)
        royal       (= :A (get-in straight [1 0]))

        ranks       (sort-ranks > (map card->rank cards))
        rank-groups (group-by-rank ranks)
        [[freq1 rank1] [freq2 rank2]] rank-groups]
    (cond
      (and flush straight royal)
      [:royal-straight-flush (second straight) cards]

      (and flush straight)
      [:straight-flush (second straight) cards]

      flush    flush
      straight straight

      (= freq1 4)
      [:four-of-a-kind (vec (concat (repeat 4 rank1) [rank2])) cards]

      (and (= freq1 3)
           (>= freq2 2))
      [:full-house (vec (concat (repeat 3 rank1) (repeat 2 rank2))) cards]

      (= freq1 3)
      [:three-of-a-kind
       (vec (concat (repeat 3 rank1) (kickers rank-groups 1 2)))
       cards]

      (= [freq1 freq2] [2 2])
      [:two-pair
       (vec (concat (repeat 2 rank1) (repeat 2 rank2) (kickers rank-groups 2 1)))
       cards]

      (= freq1 2)
      [:one-pair
       (vec (concat (repeat 2 rank1) (kickers rank-groups 1 3)))
       cards]

      :else
      [:high-card
       (vec (concat [rank1] (kickers rank-groups 1 4)))         cards] )))

(defn extract-low-cards
  "<cards (<card kw>+)>
   <return (<card kw>+)"
  [cards]
  (remove (fn [card]
            (#{:K :Q :J :10 :9} (card->rank card)))
          cards))

(defn- count-low-cards
  "<cards (<card kw>+)>
   <return int>"
  [cards]
  (count (extract-low-cards cards)))

(defn- calc-score-by-low
  "<cards (<card kw>+)> 주어지는 cards의 개수는 2 or 5개이다.
   <return score [category :low, [<rank kw>+] [<card kw>+]]?>

   [:spade-8 :club-7 :diamond-6 :heart-5 :club-A]
   => [:low [:8 :7 :6 :5 :A] [:spade-8 :club-7 :diamond-6 :heart-5 :club-A]]

   [:spade-9 :club-7 :diamond-6 :heart-5 :club-4]
   => nil"
  [cards]
  (let [flush    (check-flush cards)
        straight (check-straight cards)
        royal    (= :A (get-in straight [1 0]))

        ranks    (sort-ranks > (map card->rank cards))
        rank-groups (group-by-rank ranks)
        [[freq1 _] [freq2 _]] rank-groups]
    (cond
      (or (and flush straight royal)   ; royal-straight-flush
          (= freq1 4)                  ; four-of-a-kind
          (and (= freq1 3)             ; full-house
               (>= freq2 2))
          (= freq1 3)                  ; three-of-a-kind
          (= [freq1 freq2] [2 2])      ; two-pair
          (= freq1 2))                 ; one-pair
      nil

      :else
      (let [low-cards (extract-low-cards cards)]
        (if (< (count low-cards) 5)
          nil
          (binding [*ranks* (assoc *ranks* :A 1)]
            (let [low-ranks (sort-ranks > (map card->rank low-cards))]
              [:low-card (vec low-ranks) (vec low-cards)] )))))))

;; -------------------
;; 게임별 처리 함수

(defn- calc-texas
  "<personal-cards (<card kw>+)
   <community-cards (<card kw>+)
   <return score {:high {:categpry <category kw>
                         :hand <card kw>+}} >

   [:heart-9 :heart-5] [:spade-8 :club-7 :diamond-7 :heart-3 :heart-A]))
   => {:high {:category :one-pair,
              :hand     [:club-7 :diamond-7 :heart-A :heart-9 :spade-8]}}"
  [personal-cards community-cards]
  (let [[category ranks _ :as score] (calc-score-by-high (into personal-cards community-cards))]
    {:high {:category category
            :hand     (vec (take 5 (arrange-hand-by-rank score)))} }))


(defn- calc-omaha
  "<personal-cards (<card kw>+)
   <community-cards (<card kw>+)
   <return score {:high {:categpry <category kw>
                         :hand <card kw>+} }>

   [:club-6 :heart-9 :spade-K :diamond-A] []
   => {:high {:category :high-card, :hand [:diamond-A :spade-K]}}

   [:heart-9 :heart-5 :spade-K :diamond-A] [:spade-8 :club-7 :diamond-J :heart-3 :heart-A]))
   => {:high {:category :one-pair
              :hand     [:diamond-A :heart-A :spade-K :diamond-J :spade-8]}}"
  [personal-cards community-cards]
  (let [personal-cards-combi  (combo/combinations personal-cards 2)
        community-cards-combi (combo/combinations community-cards 3)
        cards-combi           (if (seq community-cards)
                                (for [personal  personal-cards-combi
                                      community community-cards-combi]
                                  (vec (concat personal community)))
                                personal-cards-combi)
        scores                (map calc-score-by-high cards-combi)
        highest-score         (first (sort-scores > scores))]
    {:high {:category (first highest-score)
            :hand     (arrange-hand-by-rank highest-score)} }))

(defn- calc-omaha-hl
  "<personal-cards (<card kw>+)
   <community-cards (<card kw>+)
   <return score {:high {:categpry <category kw>
                         :hand <card kw>+}
                  :low  {:categpry <category kw>
                         :hand <card kw>+}? }>

   [:heart-8 :heart-5 :spade-4 :diamond-A] [:spade-8 :club-7 :diamond-2 :heart-3 :heart-K]
   => {:high {:category :one-pair
              :hand     [:heart-8 :spade-8 :diamond-A :heart-K :club-7]},
       :low {:category :low-card
             :hand    [:club-7 :spade-4 :heart-3 :diamond-2 :diamond-A]}}

   [:club-6 :heart-9 :spade-K :diamond-A] []
   => {:high {:category :high-card :hand [:diamond-A :spade-K]},
       :low nil}"
  [personal-cards community-cards]
  (let [personal-cards-combi  (combo/combinations personal-cards 2)
        community-cards-combi (combo/combinations community-cards 3)
        cards-combi           (if (seq community-cards)
                                (for [personal  personal-cards-combi
                                      community community-cards-combi]
                                  (vec (concat personal community)))
                                personal-cards-combi)
        scores                (map calc-score-by-high cards-combi)
        highest-score         (first (sort-scores > scores))

        lowest-score (when (and (>= (count-low-cards personal-cards) 2)
                                (>= (count-low-cards community-cards) 3))
                       (let [low-scores (remove #(nil? %) (map calc-score-by-low cards-combi))]
                         (first (sort-scores < low-scores)) ))]
    {:high {:category (first highest-score)
            :hand     (arrange-hand-by-rank highest-score)}
     :low  (if lowest-score
             {:category (first lowest-score)
              :hand     (arrange-hand-by-rank lowest-score)} )}))

(defn calc-score
  "Calculates the score of cards based on game-name. (내부 처리용)
   <game-name kw> :texas | :omaha | :omahahl
   <personal-cards (<card kw>+)
   <community-cards (<card kw>+)
   <return score {:high {:categpry <category kw>
                         :hand <card kw>+}
                  :low  {:categpry <category kw>
                         :hand <card kw>+}? }>"
  ([game-name personal-cards]
   (calc-score game-name personal-cards []))
  ([game-name personal-cards community-cards]
   (case game-name
     :texas    (calc-texas    personal-cards community-cards)
     :omaha    (calc-omaha    personal-cards community-cards)
     :omahahl  (calc-omaha-hl personal-cards community-cards) )))
