(ns vita.poker.calc.pots
  "목  적: 게임 한 판이 끝난 후
           1) overage 유무 및 금액을 계산한다.
           2) pot별 상금과 그 상금을 탈수 있는 대상자를 추출한다.
   작성자: 김영태")

(defn- cut-overage
  "fold한 사람들의 최대 베팅액이 일반 player들의 최대 베팅액보다 많을 경우
   (이런 경우는 실제 포커 게임에서는 일어날 수 없지만, online 게임에서는 일어날
    수 있다. 예를 들면, 사용자의 인터넷 통신이 갑자기 끊긴다든가, 게임 도중
    친한 친구의 전화가 와서 게임의 진행을 놓친다든가 하는 경우에 발생할 수 있다) 
   그 초과 금액만큼을 무조건 삭감한다.
 
   <sid-bets {<sid str, bet int>+}>
   <cut-line int>
   <return   [overage nil, sid-bets {<sid str, bet int>+}]"
  [sid-bets cut-line]
  (let [sid-bets' (map (fn [[sid bet]]
                         (if (> bet cut-line)
                           [sid cut-line]
                           [sid bet]))
                        sid-bets)]
    [nil (into {} sid-bets')] ))

(defn- get-overage
  "1) overage 유무를 판별한다.
   2) overage가 발생한 경우, 그 금액을 계산한다.
   3) 발생한 금액만큼을 해당자에게서 차감한다.  

   <sid-bets {<sid str, bet int>+}>
   <return   [overage [money int, sid str]?,
              sid-bets {<sid str, bet int>+}]>"
  [sid-bets]
  (let [[max-bet next-bet] (sort > (vals sid-bets))
        delta              (- max-bet next-bet)
        overage   (if (pos? delta)
                    (some (fn [[sid bet]]
                            (if (= bet max-bet)
                              [delta sid]))
                          sid-bets))
        sid-bets' (map (fn [[sid bet]]
                    (if (= bet max-bet)
                      [sid (- max-bet delta)]
                      [sid bet]))
                    sid-bets)]
     [overage (into {} sid-bets')] ))

(defn- calc-overage
  "See the doc-string of cut-overage and get-overage.

   <sid-bets  {<sid str, bet int>+}>
   <play-sids (<sid str>+)>
   <fold-sids (<sid str>*)>
   <return    [overage [money int, sid str]?,
               sid-bets {<sid str, bet int>+}]"
  [sid-bets play-sids fold-sids]
  (let [fold-bets (keep #(and ((set fold-sids) (key %))
                                 (val %))
                           sid-bets)
        fold-max-bet (apply max 0 fold-bets)
        play-bets    (keep #(and ((set play-sids) (key %))
                                 (val %))
                           sid-bets)
        play-max-bet (apply max play-bets)
        delta        (- fold-max-bet play-max-bet)]
  (if (neg? delta)
    (get-overage sid-bets)
    (cut-overage sid-bets play-max-bet) )))

(defn- get-play-sids
  "1) pot-bets의 각 구간별 계산에서, bet 금액이 음수인 사용자를 계산 대상에서 제외한다.
   2) 1)의 계산 결과에서 fold한 사람들을 다시 뺸다.  

   <sid-bets  {[sid str, bet int]+}>
   <fold-sids (<sid str>+)>
   <return (<sid str>+)>"
  [sid-bets fold-sids]
  (remove (set fold-sids)
          (keep (fn [[sid bet]]
                  (if (neg? bet) nil sid))
                sid-bets) ))

(defn- calc-awards
  "면적의 개념을 이용해, 각 구간 별로 pot money를 구한다.
   일단 총액을 먼저 구한 후, 계산에 불필요한 금액을 빼는 방식으로 구현했다.

   <sid-bets  {<sid str, bet int>+}>
   <play-sids (<sid str>+)>
   <return    [pot+]>
     pot은 sub-pot-n, ..., sub-pot-2, sub-pot-1, main-pot의 순서대로 나열된다.
     <pot [award int, [<sid str>+]]>
       award: pot별 상금; sid: 해당 pot내의 우승 가능 대상자들"
  ([sid-bets fold-sids]
   (calc-awards sid-bets fold-sids []))
  ([sid-bets fold-sids acc]
   (let [width     (count sid-bets)
         play-sids (get-play-sids sid-bets fold-sids)
         ;; pot 구간별 bet 금액을 구한다.
         pot-bets (sort (set (map (fn [sid]
                                     (get sid-bets sid))
                                   play-sids)))]
      (loop [sid-bets sid-bets
             pot-bets pot-bets
             acc      []]
        (if (seq pot-bets)
          (let [height     (first pot-bets)
                total-area (* width height)
                sid-bets'  (map (fn [[sid bet]]   ; <sid-bets' ([sid str, bet int]+)>
                                  [sid (- bet height)])
                                sid-bets)
                net-area   (+ total-area
                              ;; 이 부분의 연산 결과가 음수를 반환하므로
                              ;; 결과적으로 빼기 연산을 수행한다
                              (apply + (keep (fn [[_ bet]]
                                               (if (neg? bet) bet))
                                             sid-bets') ))
                play-sids (get-play-sids sid-bets' fold-sids)]
            (recur (map (fn [[sid bet]]
                          (if (neg? bet) [sid 0] [sid bet]))
                        sid-bets')
                   (drop 1 (map #(- % height) pot-bets))
                   (conj acc [net-area (vec play-sids)]) ))
          acc) ))))

(defn calc-pots1
  "<sid-bets  {<sid str, bet int>+}>
   <fold-sids (<sid str>+)>
   <return    {:overage [money int, sid str]?
               :awards  [pot+]}
     pot은 sub-pot-n, ..., sub-pot-2, sub-pot-1, main-pot의 순서대로 나열된다.
     <pot [award int, [<sid str>+]]>
       award: pot별 상금; sid: 해당 pot내의 우승 가능 대상자들"
  [sid-bets fold-sids]
  (let [all-sids  (keys sid-bets)
        play-sids (remove (set fold-sids) all-sids)
        [overage sid-bets'] (calc-overage sid-bets play-sids fold-sids)
        awards    (calc-awards sid-bets' fold-sids)]
   (cond
      ;; overage가 발생했지만, 승자가 한 명뿐인 경우
      (and overage (= 1 (count awards)))
      {:overage nil
       :awards (update-in awards [0 0] + (get overage 0))}
 
      :else
      {:overage overage :awards awards} )))

(defn calc-pots2
  "<sid-bets  {<sid str, bet int>+}>
   <fold-sids (<sid str>+)>
   <return    {:overage [money int, sid str]?
               :awards  [pot+]}
     pot은 sub-pot-n, ..., sub-pot-2, sub-pot-1, main-pot의 순서대로 나열된다.
     <pot [award int, [<sid str>+]]>
       award: pot별 상금; sid: 해당 pot내의 우승 가능 대상자들"
  [sid-bets fold-sids]
  (let [all-sids  (keys sid-bets)
        play-sids (remove (set fold-sids) all-sids)
        [overage sid-bets'] (calc-overage sid-bets play-sids fold-sids)
        awards    (calc-awards sid-bets' fold-sids)]
   (cond
      ;; overage가 발생했지만, 승자가 한 명뿐인 경우
      (and overage 
           (= 1 (count (get-in awards [0 1])))
           (= [(get overage 1)] (get-in awards [0 1])))
      {:overage nil
       :awards (update-in awards [0 0] + (get overage 0))}
 
      :else
      {:overage overage :awards awards} )))

(defn calc-pots
  "<sid-bets  {<sid str, bet int>+}>
   <fold-sids (<sid str>+)>
   <return    {:overage [money int, sid str]?
               :awards  [pot+]}
     pot은 sub-pot-n, ..., sub-pot-2, sub-pot-1, main-pot의 순서대로 나열된다.
     <pot [award int, [<sid str>+]]>
       award: pot별 상금; sid: 해당 pot내의 우승 가능 대상자들"
  [sid-bets fold-sids]
  (let [all-sids  (keys sid-bets)
        play-sids (remove (set fold-sids) all-sids)
        [overage sid-bets'] (calc-overage sid-bets play-sids fold-sids)
        awards    (calc-awards sid-bets' fold-sids)]
    {:overage overage :awards awards} ))

(comment   ;; test

(def sid-bets1 {"sid0" 30 "sid1" 15 "sid2" 15 "sid3" 60 "sid4" 60})
(calc-pots sid-bets1 ["sid0" "sid1" "sid2"])
; => {:overage nil, :awards [[180 ["sid3" "sid4"]]]}

(calc-pots sid-bets1 ["sid0" "sid2"])
; => {:overage nil, :awards [[75 ["sid3" "sid1" "sid4"]] [105 ["sid3" "sid4"]]]}

(calc-pots sid-bets1 ["sid1" "sid2"])
; => {:overage nil, :awards [[120 ["sid3" "sid0" "sid4"]] [60 ["sid3" "sid4"]]]}

(calc-pots sid-bets1 ["sid2"])
; => {:overage nil, :awards [[75 ["sid3" "sid0" "sid1" "sid4"]] [45 ["sid3" "sid0" "sid4"]] [60 ["sid3" "sid4"]]]}


(def sid-bets2 {"sid0" 60 "sid1" 60 "sid2" 60 "sid3" 60 "sid4" 60})
(calc-pots sid-bets2 [])
; => {:overage nil, :awards [[300 ["sid3" "sid0" "sid2" "sid1" "sid4"]]]}

(def sid-bets3 {"sid0" 60 "sid1" 15 "sid2" 25 "sid3" 7 "sid4" 9})
(calc-pots sid-bets3 [])
; => {:overage [35 "sid0"],
;     :awards [[35 ["sid2" "sid1" "sid3" "sid4" "sid0"]]
;              [8 ["sid2" "sid1" "sid4" "sid0"]]
;              [18 ["sid2" "sid1" "sid0"]]
;              [20 ["sid2" "sid0"]]]}

(def sid-bets4 {"sid0" 20 "sid1" 50 "sid2" 90 "sid3" 140 "sid4" 200})
(calc-pots sid-bets4 ["sid0" "sid2" "sid3"])
; => {:overage [60 "sid4"], :awards [[220 ["sid1" "sid4"]] [220 ["sid4"]]]}

(def sid-bets5 {"sid1" 100 "sid3" 30})
(calc-pots sid-bets5 ["sid3"])
; => {:overage nil, :awards [[130 ["sid1"]]]}

(def sid-bets6 {"sid1" 100 "sid3" 30})
(calc-pots sid-bets6 ["sid1"])
; => {:overage nil, :awards [[60 ["sid3"]]]}

(def sid-bets7 {"sid0" 200 "sid1" 150 "sid2" 70 "sid3" 180})
(calc-pots sid-bets7 ["sid0" "sid1"])
; => {:overage nil, :awards [[280 ["sid3" "sid2"]] [300 ["sid3"]]]}

(def sid-bets8 {"sid0" 100 "sid1" 30 "sid2" 50 "sid3" 80})
(calc-pots sid-bets8 ["sid1" "sid3"])
; => {:overage [20 "sid0"], :awards [[180 ["sid0" "sid2"]] [60 ["sid0"]]]}

(def sid-bets9 {"sid0" 100 "sid1" 90 "sid2" 30 "sid3" 50})
(calc-pots sid-bets9 ["sid0" "sid1"])
; => {:overage nil, :awards [[120 ["sid3" "sid2"]] [60 ["sid3"]]]}

)   ;; end of comment
