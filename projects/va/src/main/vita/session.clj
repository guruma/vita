(ns vita.session
  "게임 유저의 tk을 저장한다."
  (:require
    [clojure.core.async :as async]
    [ruiyun.tools.timer :refer :all]
    [clojure.tools.logging :refer :all]
    [sonaclo.shared.util :as util]
    [vita.gen :as gen]
    [vita.common.websocket :as ws]
    [vita.db.game-db :as game-db]
    [vita.friends]
    ))

;; TODO(kep) 로그인 세션만 처리하고 싶다. session/login.clj 로 옮기고, session/game.clj와 같은 방식 고려.

;; tk = token
;; sid = social_id
;; sock = websocket
;; rid = room_id

(def tk-sid*      (ref {}))
(def sid-tk*      (ref {}))
(def sock-tk*     (ref {}))
(def tk-sock*     (ref {}))
(def sid-rid*     (ref {}))
(def sid-game-name* (ref {}))
(def sid-account* (ref {}))
(def sid-time*    (ref {}))


;;; =========
;;; Getters.
(defn sock->tk [sock]
  (get @sock-tk* sock))

(defn sid->tk [sid]
  (get @sid-tk* sid))

(defn tk->sid [tk]
  (get @tk-sid* tk))

;; TODO(kep) tk=>sid
(defn sid->rid [sid]
  (get @sid-rid* sid))

(defn sid->sock [sid]
  (->> (sid->tk sid)
       (get @tk-sock* )))


(defn sid->account
  "
   {:social-id 100001740185892,
    :user {:last-logout-time 1401868212714,
           :avatar-url ,
           :gender male,
           :last-access-time 1401877571755,
           :name 박재혁,
           :locale ko_KR,
           :email aquua@nate.com,
           :email aquua@nate.com,
           :money 2000000},
    :event none,
    :create_time 1400566406805,
    :payment-id  xxxxxxxxxxxxx,
    :invitee {:sid 'wait' :sid 'wait'}
    :game {:slotmachine {:freespin-cnt 0, :bet-amount 200}}"
  [sid]

  (when-let [account (get @sid-account* sid)]
    (-> account
        (update-in [:invitee]
                   (fn [invitee]
                     (->> invitee
                          (reduce-kv (fn [acc k v] (assoc acc (name k) v)) {})))))))


(defn sid->game-name
  [sid]
  (get @sid-game-name* sid))


(defn sid->username [sid]
  (-> (sid->account sid)
      (get-in [:user :name])))

(defn sid->usermoney [sid]
  (-> (sid->account sid)
      (get-in [:user :money])))

(defn sid->avatar-url [sid]
  (-> (sid->account sid)
      (get-in [:user :avatar-url])))

(defn sid->is-npc [sid]
  (-> (sid->account sid)
      (get-in [:user :is-npc])))


;;; =======
;;; Adders.
(defn add-sid-tk
  "input keyword.. :tk :social-id"
  [sid tk]

  (assert (string? sid))

  (alter sid-tk* assoc sid tk)
  (alter tk-sid* assoc tk sid))


(defn add-sid-account
  [sid account]
  (assert (string? sid))
  (alter sid-account* assoc sid account))


(defn add-tk-sock
  "{ws :tk} ref assoc.
   {:tk ws} ref assoc  2개 한다."
  [tk sock]

  (when-let [before-sock (get @tk-sock* tk)]
    (warnf "[0] sid=%s before_sock=%s sock=%s" (tk->sid tk) before-sock sock)
    (when-not (= sock before-sock)

      (warnf "[1] sid=%s before_sock=%s sock=%s" (tk->sid tk) before-sock sock)
      (alter sock-tk* dissoc before-sock)
      (ws/close before-sock)))

  (alter sock-tk* assoc sock tk)
  (alter tk-sock* assoc tk sock))


(defn add-sid-rid
  "tk에 room-id를 추가한다."
  [sid rid]
  (alter sid-rid* assoc sid rid))


(defn add-sid-game-name
  [sid game-name]
  (alter sid-game-name* assoc sid game-name))


;; TODO account 생성하기 전에 update-account-money가 생성된다.
;;; ===========
;;; Updaters.
;; TODO(kep)
(defn update-account-money
  [sid add-money]

  (when (and (not= add-money 0) (sid->account sid))
    ;; 메모리 데이터 갱신
    (alter sid-account*
           update-in [sid :user :money]
           (fn [old-money]
             (assert (not (nil? old-money)))
             (+ old-money add-money)))

    ;; db 데이터 갱신 (실제 유저에 한해서)
    (when-not (sid->is-npc sid)
      (future (game-db/update-user-money sid add-money)))))


(defn remove-rid
  "유저가 나갈 때 호출된다.
  브라우저가 닫힐 때 rid를 session에서 가져오기 때문에
  room에서 유저를 삭제하는 함수를 다시 타게 된다."
  [sid]
  (alter sid-rid* dissoc sid))


;;; =========
;;; Times.

(defn is-connected?
  [sid]
  (contains? @sid-time* sid))


(defn set-last-request-time
  "유저의 최종 행위 시간을 기록한다."
  [sid]
  (if (seq sid)
    (alter sid-time* assoc sid (System/currentTimeMillis))))


(defn wipe-session
  "완전삭제한다. 서버에서 스케쥴링하면서 삭제된다."
  [sid]
  (let [sock  (sid->sock sid)
        tk    (sid->tk sid)]
    (alter sock-tk*     dissoc sock)

    (alter sid-tk*      dissoc sid)
    (alter sid-rid*     dissoc sid)
    (alter sid-account* dissoc sid)
    (alter sid-time*    dissoc sid)


    (alter sid-game-name* dissoc sid)
    (alter tk-sid*      dissoc tk)
    (alter tk-sock*     dissoc tk) ))

(defn refresh-tk
  [account]
  (let [tk (gen/gen-tk)
        sid (:social-id account)]
    (add-sid-account sid account)
    (set-last-request-time sid)
    (add-sid-tk sid tk)
    tk))
