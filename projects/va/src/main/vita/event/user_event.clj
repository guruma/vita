(ns vita.event.user-event
  (:require
    [clojure.tools.logging :refer :all]

    [clj-time.core :as t]
    [clj-time.coerce :as c]

    [vita.session]
    #_[vita.db.game-db :as db]
    [vita.const.event :as EVENT]
    [vita.common.websocket :as ws]
    [clojure.core.async :as async :refer [alt! <! >! timeout chan go close!]]

    [vita.shared.const.err-code :as ERR]
    [vita.session :as session]
    [sonaclo.shared.util :as util]))

;; FIXME(kep) 이 파일 지우셈.


#_(defn daily-event
  "24시간 후 이벤트 금액을 지급한다.
  유저가 받은 최근의 event도 기록한다.
  return : DB에 저장된 문서 or EVENT/EVENT_DAILY_NONE
  메인 로비에서 재로딩할 때 last-access-time이 갱신되지 않아 이 함수에서 따로 갱신한다.
  :last-access-time -> 최초 접속한 시간..
  "
  [sid]

  (let [account (db/get-hyphen$ sid)
        event   (:event account)]

    (if (not= event EVENT/EVENT_NONE)
      ;; TODO(kep) keyword 제거해야함 땜빵임.
      (keyword event)

      (let [before      (c/from-long (get-in account [:user :last-access-time]))
            after       (c/from-long (System/currentTimeMillis))
            hour-diff   (t/in-hours (t/interval before after))]

        (if (> hour-diff 24)
          (do
            (db/with-transaction sid [account []]
                                 (-> account
                                     (assoc :event EVENT/EVENT_DAILY)))
            EVENT/EVENT_DAILY)

          EVENT/EVENT_NONE)))))


;; TODO(kep) 이거 const map으로 뺄것
#_(defn get-event-money
  [event-name]
  ;; {:pre [(keyword? event-name)]}

  ;; TODO(kep) 없엘것.
  ;; (assert (keyword? event-name))
  (condp = (keyword event-name)
    EVENT/EVENT_START             EVENT/EVENT_MONEY_REGISTER
    EVENT/EVENT_DAILY             EVENT/EVENT_MONEY_DAILY
    EVENT/EVENT_INVITE_FRIEND     EVENT/EVENT_MONEY_INVITE_FRIEND
    0))

#_(defn push-login-event
  [sid]
  (go (<! (timeout 1000))
      (let [event (daily-event sid)
            money (get-event-money event)]
        (when-not (= event EVENT/EVENT_NONE)
          (ws/send (session/sid->sock sid)
                   {:cmd :Bfreecoin
                    :err ERR/ERR_NONE
                    :data {:event-type event
                           :money money}})))
      ))
