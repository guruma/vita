(ns vita.event.events
  (:require
    [vita.shared.const.events :as EVENTS]
    [vita.gen :as gen]
    [vita.const.event :as EVENT]
    ))

; DB TEST CODE
;(insert-new-user "afsfsf" {:name "afsfsf" :avatar-url "" :email "" :locale "" :gender ""})
;(add-invitee "afsfsf" "qwert")
;(insert-new-user "qwert" {:name  "qwert" :avatar-url "" :email "" :locale "" :gender ""})
;(update-events "afsfsf")
;(del-user "afsfsf")
;(del-user "qwert")

(defn event-register
  []
  {:uid (gen/gen-uid)
   :id EVENTS/EVENT_REGISTER
   :info {:money EVENT/EVENT_MONEY_REGISTER}})


(defn event-daily
  []
  {:uid (gen/gen-uid)
   :id EVENTS/EVENT_DAILY
   :info {:money EVENT/EVENT_MONEY_DAILY}})


(defn event-invite-friend
  [invitee-name]
  {:uid (gen/gen-uid)
   :id EVENTS/EVENT_INVITE_FRIEND
   :info {:money EVENT/EVENT_MONEY_INVITE_FRIEND
          :invitee invitee-name}})


(defn has-daily-event?
  [account]
  (->> account
      :events
      (some (fn [event] (= (:id event) EVENTS/EVENT_DAILY)))
      (some?)))
