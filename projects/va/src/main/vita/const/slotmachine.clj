(ns vita.const.slotmachine)

;; TODO(kep) 합치게되면 prefix작업.

;; gold7 item이 나타나게 될 확률을 조절하기 위한 변수이다. 이 값이 11이면 각 triple당
;; 1/11의 확률로 gold7이 나타나게 된다. triple이 모두 5개이므로, 한 번의 spin 버튼이
;; 눌릴 때마다 1/11 * 5 = 5/11의 확률로 gold7이 나타나게 된다. 따라서 이 값을 20으로
;; 올리면, 1/20 * 5 = 1/4의 확률로 gold7이 나타나게 되므로 그만큼 사용자에게 유리한
;; 결과가 나온다.
(def ^:const ADJ-GOLD7-MAX 11)


;; qm이 나타나게 될 확률을 조절하기 위한 변수로, 그 원리는 MAX-GOLD7 변수의 경우와 같다.
(def ^:const ADJ-QUESTION-MAX 11)


;; 사용자에게 너무 유리한 결과가 나오는 것을 방지하기 위한 변수로, 이 값이 70이면
;; 사용자에게 유리하게 나오는 경우 중의 70%만을 통과시킨다.
(def ^:const PERCENT-PASS 70)
