(ns vita.const.poker)

;; TODO(kep) prefix 작업. 아니면 room_api.clj에 넣도록 하자.

(def CARD_BOARDS
  [[:club-A :club-2 :club-3 :club-4 :club-5 :club-6 :club-7 :club-8 :club-9 :club-10 :club-J :club-Q :club-K]
   [:heart-A :heart-2 :heart-3 :heart-4 :heart-5 :heart-6 :heart-7 :heart-8 :heart-9 :heart-10 :heart-J :heart-Q :heart-K]
   [:diamond-A :diamond-2 :diamond-3 :diamond-4 :diamond-5 :diamond-6 :diamond-7 :diamond-8 :diamond-9 :diamond-10 :diamond-J :diamond-Q :diamond-K ]
   [:spade-A :spade-2 :spade-3 :spade-4 :spade-5 :spade-6 :spade-7 :spade-8 :spade-9 :spade-10 :spade-J :spade-Q :spade-K]])


(def ^:const MAX_USER_IN_ROOM 5 );; 방에 들어갈 최대 인원.
(def ^:const SUITABLE_BLIND_PAGE 4)    ;; 나의 레벨에 맞는 page
(def ^:const UNSUITABLE_BLIND_PAGE 2) ;; 나의 레벨에 맞지 않은 page.