(ns vita.const.event)

;; TODO(kep) EVENT_ => EVENT_STATE_
(def ^:const EVENT_NONE                         :none)
(def ^:const EVENT_START "신규가입"              :freecoin-start)
(def ^:const EVENT_DAILY "매일 이벤트"           :freecoin-day)
(def ^:const EVENT_INVITE_FRIEND "친구초대"      :invite-friend)

(def ^:const EVENT_MONEY_REGISTER "신규 가입 150만" 1500000)
(def ^:const EVENT_MONEY_DAILY "신규 가입 150만" 500000)
(def ^:const EVENT_MONEY_INVITE_FRIEND "신규 가입 100만" 1000000)
