(ns vita.common.websocket
  (:refer-clojure :exclude [send])

  (:require
    [clojure.tools.logging :refer :all]
    [org.httpkit.server :as kit]

    ))

(defn s-start-server
  ([{:keys [host port properties ws-handler]}])

  ([host port properties ws-handler]))


(defn send [ws data]
  (kit/send! ws (str data)))



(defn wrap-on-close
  "deprecated"
  [ws handler]
  )



(defn close
  [ws]
  (kit/close ws))



;;; ******************
;;;  helper functions
;;; ******************

(defn path
  "deprecated"
  [ws]
  )




(defn reject
  "deprecated"
  [ws]
  )
