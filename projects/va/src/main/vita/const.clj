(ns vita.const)

;; vita.const.*
;; vita.shared.const*
;; TODO(kep) vita.const => vita.shared.const.common으로 통합.


(def ^:const MAIN_LOBBY :main-lobby)


(def ^:const TEXAS :texas)
(def ^:const OMAHA :omaha)
(def ^:const OMAHA-HL :omahahl)
(def ^:const SL-GOLD-7 :gold-7)


(def ^:const DB_RETRY_COUNT 10)
