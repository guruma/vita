(ns vita.friends)

;; TODO(keo) 친구 목록 캐쉬 문제가 남아있는 상태.
;; TODO(kep) vita.const.* => vita.const로 묶을 방안을 고려해보자.
(def ^:const FRIEND_COUNT_PER_PAGE 7)

(def sid-friends*    (ref {}))


(defn add-friends
  [sid friends]
  (alter sid-friends* assoc sid friends))


(defn del-friends
  [sid]
  (alter sid-friends* dissoc sid))


(defn get-friends
  ([sid]
   (get-friends sid 1))

  ([sid page]
   (assert (pos? page))

   (->> (get @sid-friends* sid)
        (drop (* FRIEND_COUNT_PER_PAGE (dec page)))
        (take FRIEND_COUNT_PER_PAGE)
        (vec))))


(defn get-max-page
  [sid]
  (->  (get @sid-friends* sid)
       (count)
       (/ FRIEND_COUNT_PER_PAGE)
       (Math/ceil)
       (int)))