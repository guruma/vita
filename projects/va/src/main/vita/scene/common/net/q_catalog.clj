(ns vita.scene.common.net.q-catalog
  (:require
    [clojure.tools.logging :refer :all]
    [vita.db.game-db :as db]
    [vita.shared.const.err-code :as ERR]
    [vita.session :as session]
    [vita.payment.request-id :as rid]
    [vita.payment.products :as prod]
    [vita.err :as err :refer [throw-when-not]]))


(defn my-print [& args]
   (warn (apply str args)))

(defn- valid-data? [data]
  true )


(defn- send-product-info [{:keys [product-id-selected token]}]
  (my-print "**** send-product-info ****")
  (my-print "product-id: " product-id-selected)
  (my-print "game token: " token)

  (let [amount 1]
    {:request-id (rid/new-request-id product-id-selected token)
     :product-url (prod/product-url product-id-selected)
     :quantity amount}))


(defn- send-catalog []
  (my-print "**** send-catalog ****")
  {:result "fail"})


(defn Q_CATALOG
  "
# 입력:
{:cmd PROTOCOL/Q_CATALOG
:token token
:data {}}

# 출력:
{:cmd PROTOCOL/R_CATALOG
:err PROTOCOL/ERR_NONE
:data {account}}
"
  ;; 현재 sid는 사용하지 않지만, 추후 로케일에 따른 카달로그 전달이 필요함이 예상되어 추가함.
  [sid data]

  (println "Q_CATALOG : " data)

  (throw-when-not (valid-data? data)
    (err/in-error ERR/INVALID_INDATA))

  (if (:product-id-selected data)
    (send-product-info data)
    (send-catalog)))
