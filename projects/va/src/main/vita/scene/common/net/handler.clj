(ns vita.scene.common.net.handler
  (:require

    [vita.shared.const.main-lobby :as MAIN_LOBBY]
    [vita.scene.common.net.q-payment :as q-payment]
    [vita.scene.common.net.q-catalog :as q-catalog]
    [vita.scene.common.net.q-echo :as q-echo]

    ))


;; TODO Q_PAYMENT,Q_ECHO  MAIN_LOBBY에서 제거.
(def x-handler*
  {
    MAIN_LOBBY/Q_CATALOG
    {:in-schema nil
     :out-schema nil
     :is-req-login? false
     :proc #'q-catalog/Q_CATALOG}

    MAIN_LOBBY/Q_PAYMENT
    {:in-schema nil
     :out-schema nil
     :is-req-login? false
     :proc #'q-payment/Q_PAYMENT}

    MAIN_LOBBY/Q_ECHO
    {:in-schema nil
     :out-schema nil
     :is-req-login? false
     :proc #'q-echo/Q_ECHO}

    })
