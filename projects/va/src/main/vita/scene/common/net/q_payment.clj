(ns vita.scene.common.net.q-payment
  (:require
    [vita.db.game-db :as db]
    [vita.shared.const.err-code :as error-code]
    [vita.session :as session]
    [vita.payment.payments :as pay]
    [vita.payment.request-id :as rid]
    [clojure.tools.logging :refer :all]

    [sonaclo.shared.util :as util]
    [vita.err :as err :refer [throw-when-not]]))



(defn my-print [& args]
  (warn (apply str args)))


(defn- valid-data? [data]
  true )


(defmulti process-payment :status)


(defmethod process-payment "canceled" [payment]
                                      (my-print "==== process-payment-canceled ====")
                                      (my-print payment)
                                      {:status "canceled"})


(defmethod process-payment "initiated" [payment]
                                       (my-print "==== process-payment-initiated ====")
                                       (my-print payment)

                                       (throw-when-not (pay/payment? payment)
                                                       (err/in-error error-code/FB_UNSIGNED_PAYMENT))

                                       (pay/log-payment payment)
                                       {:status "initiated"})


(defmethod process-payment "completed" [payment]
                                       (my-print "==== process-payment-completed ====")
                                       (my-print payment)

                                       (throw-when-not (pay/payment? payment)
                                                       (err/in-error error-code/FB_UNSIGNED_PAYMENT))

                                       (throw-when-not (pay/verify-payment payment)
                                                       (err/in-error error-code/FB_INVALID_PAYMENT))

                                       (try
                                         (pay/write-payment payment)
                                         (catch Exception e
                                           (err/in-error error-code/DB_WRITE_ERROR)))
                                       {:status "completed"})


(defmethod process-payment "failed" [payment]
                                    (my-print "==== process-payment-failed ====")
                                    (my-print payment)

                                    (throw-when-not (pay/payment? payment)
                                                    (err/in-error error-code/FB_UNSIGNED_PAYMENT))

                                    (pay/log-payment payment)
                                    {:status "failed"})


(defn get-account [sid]
  (when-let [account (db/get-hyphen$ sid)]
    (dosync
      (session/add-sid-account sid account)))
  (session/sid->account sid))


(defn dispatch-payment [sid payment]
  (let [params (util/clojure-style-keywordize payment)]
    (my-print "==== dispatch-payment ====")
    (my-print (get-account sid))

    (let [result (process-payment payment)]
      (rid/del-request-id (:request-id payment))
      (assoc result :account (get-account sid)))))


(defn Q_PAYMENT "
# 입력:
{:cmd PROTOCOL/Q_PAYMENT
:token token
:data {}}

# 출력:
{:cmd PROTOCOL/R_PAYMENT
:err PROTOCOL/ERR_NONE
:data {account}}
"
  ;; 사용자 로케일에 따라 카달로그가 다르게 편성될 수 있어 sid를 사용하지 않지만 남겨둔다.
  [sid data]

  (let [payment (util/clojure-style-keywordize data)]
    (println "========================" sid payment)
    (throw-when-not (valid-data? payment)
                    (err/in-error error-code/FB_INVALID_DATA))

    (dispatch-payment sid payment)))
