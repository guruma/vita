(ns vita.scene.common.net.q-echo
  (:require
    [vita.db.game-db :as db]
    [vita.shared.const.err-code :as ERR]
    [vita.session :as session]
    [vita.err :as err] ))

;; FIXME(kep) 이 패킷 없엘꺼임.

(defn- valid-data? [data]
  true )


(defn Q_ECHO
  "
# 입력:
{:cmd PROTOCOL/Q_ECHO {}}

# 출력:
{:cmd PROTOCOL/Q_ECHO
:err PROTOCOL/ERR_NONE
:data {}}
"
  [sid data]

  (when-not (valid-data? data)
    (err/in-error ERR/INVALID_INDATA))

  {})
