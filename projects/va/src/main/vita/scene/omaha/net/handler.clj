(ns vita.scene.omaha.net.handler
  (:require

   [vita.poker.holdem.net.handler :as holdem-handler]
   [vita.shared.const.common :as COMMON]
   ))

(def x-handler*
  (holdem-handler/gen-middleware COMMON/OMAHA))


