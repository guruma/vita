(ns vita.scene.texas.net.handler
  (:require

   [vita.shared.const.common :as COMMON]

   [vita.poker.holdem.net.handler :as holdem-handler]

   ))


(def x-handler*
  (holdem-handler/gen-middleware COMMON/TEXAS))
