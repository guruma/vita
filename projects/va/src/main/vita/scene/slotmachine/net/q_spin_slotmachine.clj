(ns vita.scene.slotmachine.net.q-spin-slotmachine
  (:require
    [vita.scene.slotmachine.generator :as generator]
    [vita.shared.slotmachine.formula :as fm]
    [vita.db.game-db :as db]
    [vita.session :as session]
    [vita.db.util.log-sender :as log-sender]
    [vita.db.db-const :as db-const]
    [vita.scene.slotmachine.util :as util]
    [vita.shared.const.err-code :as ERR]
    [clojure.core.async :as async :refer [<! >! timeout chan take! go close! thread]]
    [vita.err :as err]))


(defn- valid-data? [data]
  ;;(let [{:keys [line bet chip freespin-count]} data])
  true)


(defn- spin-slotmachine [sid bet-line-count per-line-bet triples qm-backs]
  (let [amount (fm/calc-total-earned-chip triples qm-backs bet-line-count per-line-bet)
        betchip (* bet-line-count per-line-bet)]

    (db/with-transaction sid
                         (let [user-money (get-in account [:user :money])
                               is-freespin? (-> account
                                                (get-in [:game :slotmachine :freespin-count] 0) ;; default 0
                                                pos?)]

                           ;; valid check
                           (when (and (not is-freespin?) (< user-money betchip))
                             (throw (Throwable. (str "has no money" sid "," amount "," user-money "," betchip))))


                           (if is-freespin?

                             ;; freespin 카운트 내리고, + 획득머니
                             (do
                               (go
                               (log-sender/money sid amount
                                                 :slotmachine user-money nil db-const/FREE-SPIN))
                               (-> account
                                   (update-in [:game :slotmachine :freespin-count] dec)
                                   (update-in [:user :money] + amount)))

                             ;; 유저 돈 + 획득머니 - 사용한 칩
                             (do
                               (go
                               (log-sender/money sid (+ amount (- betchip))
                                                 :slotmachine user-money nil db-const/IN-GAME))
                               (update-in account [:user :money] + amount (- betchip)))

                             )))))


(defn SPIN_SLOTMACHINE
  "
  # 입력:
  {:bet-line-count
   :per-line-bet
   :chip
   :freespin-count}

  # 출력:
  {:triples
   :chip
   :jackpot
   :freespin-count
   :backs
   :triples}
"

  [sid data]

  (when-not (valid-data? data)
    (err/in-error ERR/INVALID_INDATA))

  (let [{:keys [bet-line-count per-line-bet chip]} data
        {:keys [triples qm-backs]} (generator/get-random-triples-info bet-line-count per-line-bet)
        doc (spin-slotmachine sid bet-line-count per-line-bet triples qm-backs)]

    ;; TODO(kep) 클라이언트가 보내준 chip, freespin-count에 대한 검증 로직 필요.

    (when-not doc
      (err/in-error ERR/DB_INTERNAL_ERROR))

    {:triples (fm/sync-triples triples qm-backs)
     :backs {}
     :jackpot (util/get-jackpot)
     :chip (get-in doc [:user :money])
     :freespin-count (get-in doc [:game :slotmachine :freespin-count])}))
