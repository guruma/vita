(ns vita.scene.slotmachine.net.handler
  (:require

   [vita.shared.const.slotmachine :as SLOTMACHINE]
   [vita.scene.slotmachine.net.q-join-slotmachine :as q-join-slotmachine]
   [vita.scene.slotmachine.net.q-spin-slotmachine :as q-spin-slotmachine]

   ))


(def x-handler*
  {SLOTMACHINE/Q_JOIN_SLOTMACHINE
   {:in-schema nil
    :out-schema nil
    :proc #'q-join-slotmachine/JOIN_SLOTMACHINE}

   SLOTMACHINE/Q_SPIN_SLOTMACHINE
   {:in-schema nil
    :out-schema nil
    :proc #'q-spin-slotmachine/SPIN_SLOTMACHINE}})
