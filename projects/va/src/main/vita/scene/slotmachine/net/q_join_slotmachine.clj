(ns vita.scene.slotmachine.net.q-join-slotmachine
  (:require
    [clojure.tools.logging :as logging]
    [vita.db.game-db :as db]
    [vita.scene.slotmachine.util :as util]
    [vita.shared.const.err-code :as ERR]
    [vita.session :as session]
    [vita.scene.slotmachine.record :as record]

    [vita.err :as err]
    ))


(defn- valid-data? [data]
  (let [{:keys [bet-amount]} data]
    (= bet-amount 200)))


(defn- join-slotmachine [sid bet-amount]
  (db/with-transaction sid
    (-> account
        (update-in [:game :slotmachine]
                   (fn [slotmachine]
                     (-> slotmachine
                         (assoc :bet-amount bet-amount)))))))


(defn JOIN_SLOTMACHINE
  "
  # 입력:
  {:bet-amount}

  # 출력:
  {:jackpot
   :chip
   :begin-per-line-bet}
"

  [sid data]

  (record/in! :slotmachine sid)

  (when-not (valid-data? data)
    (err/in-error ERR/INVALID_INDATA))

  (let [{:keys [bet-amount]} data
        db-account (join-slotmachine sid bet-amount)]

    ;; doc이 없으면 query 실패.
    (when-not db-account
      (err/in-error ERR/DB_INTERNAL_ERROR))

    ;; done.
    {:jackpot (util/get-jackpot)
     :chip (get-in db-account [:user :money])
     :begin-per-line-bet (get-in db-account [:game :slotmachine :bet-amount])
     :freespin-count (get-in db-account [:game :slotmachine :freespin-count])}))
