(ns vita.scene.slotmachine.record
  (:require
   [clojure.tools.logging :refer :all]

   [vita.db.log-db]
   )
  )


;; :sid - milliseconds
(def for-log*
  (atom {}))


(defn in! [game-name sid]
  (swap! for-log*
         assoc [game-name sid] (System/currentTimeMillis)))


(defn out! [game-name sid]
  (when-let [join-time (get @for-log* [game-name sid])]
    #_(vita.db.log-db/insert-game-play-time sid game-name join-time (System/currentTimeMillis))
    (swap! for-log*
           dissoc [game-name sid])))
