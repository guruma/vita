(ns vita.scene.slotmachine.generator
  (:require [vita.shared.slotmachine.formula :as fm]
            [clojure.set :as set]
            [vita.const.slotmachine :as SLOTMACHINE]
            ))

;; FIXME(kep) 전체적으로 수정해야함.

;; 기본적인 item들의 목록
(def ^:private basic-items*
  [:grape :watermelon :cherry :apple :lemon :bar1 :bar2 :bar3 :blue7 :red7 :gold7])


;; 과일 item들이 선택될 가능성을, 비과일 item들이 선택될 가능성의 2배가 되도록 한다.
(def ^:private items*
  [:grape :watermelon :cherry :apple :lemon
   ;; :grape :watermelon :cherry :apple :lemon
   :bar1 :bar2 :bar3 :blue7 :red7])


;; 각각의 좌표에서 qm(Question Mark)이 검출되었을 때, 검사해야할 bingo-line들의 목록.
;; 예를 들어, (0, 0) 좌표에 qm이 있으면, [2 4 8 12 14 18] 라인들을 검사해 사용자에게
;; 가장 유리한 item으로 바꾸어 주게 된다.
(def ^:private lines-for-qm*
  [[[ 1  3  7 11 13 17 19]
    [ 0  5  6  9 10 15 16]
    [ 2  4  8 12 14 18]]
   [[ 1  5  7  9 17]
    [ 0  3  4 11 12 13 14 15 16]
    [ 2  6  8 10 18 19]]
   [[ 1  4  5 13 15 18]
    [ 0  7  8  9 10 11 12]
    [ 2  3  6 14 16 17 19]]
   [[ 1  5  8 10 17]
    [ 0  3  4 11 12 13 14 15 16]
    [ 2  6  7  9 18 19]]
   [[ 1  3  8 11 13 17 19]
    [ 0  5  6  9 10 15 16]
    [ 2  4  7 12 14 18]]])


;;****************************
;; triples 생성 관련 함수들
(defn- random-triple
  "하나의 triple을 임의로 생성한다."
  []
  ;; NOTE(kep) loop 이용하면 어떨까?
  (let [triple (atom #{})]
    (while (< (count @triple) 3)
      (let [item (first (take 1 (shuffle items*)))]
        (if-not (@triple item)
          (swap! triple conj item) )))
    (vec @triple) ))


(defn- random-triples
  "총 5개의 triple을 임의로 생성한다."
  []
  (->> random-triple
       (repeatedly 5)
       vec))


(defn- insert-gold7?
  "gold7 item을 넣을지 말지를 결정한다. 이때 SLOTMACHINE/ADJ-GOLD7-MAX 변수의 값을 참조한다."
  []
  (zero? (rand-int SLOTMACHINE/ADJ-GOLD7-MAX)))


(defn- insert-gold7s
  "random-triples를 통해 생성된 triples에, insert-gold7? 함수의 반환 값에 따라
  gold7 item을 삽입하거나 삽입하지 않는다."
  [triples]
  (->> triples
       (mapv (fn [triple]
               (if (insert-gold7?)
                 (assoc triple (rand-int 3) :gold7)
                 triple)))))


(defn- insert-qm?
  "qm item을 넣을지 말지를 결정한다. 이때 SLOTMACHINE/ADJ-QUESTION-MAX 변수의 값을 참조한다."
  []
  (zero? (rand-int SLOTMACHINE/ADJ-QUESTION-MAX)))


(defn- insert-qms
  "random-triples를 통해 생성된 triples에, insert-qm? 함수의 반환 값에 따라
   qm item을 삽입하거나 삽입하지 않는다."
  [triples]
  (->> triples
       (mapv (fn [triple]
               (if (insert-qm?)
                 (assoc triple (rand-int 3) :question)
                 triple)))))


;;*******************************
;; qm(Question Mark) 검출 루틴들
;; <sample>
;; triples => [[:bar2 :red7 :question]
;;             [:red7 :question :cherry]
;;             [:bar3 :lemon :cherry]
;;             [:grape :bar1 :cherry]
;;             [:blue7 :bar1 :cherry]]
;; return  => ([0 2] [1 1])
(defn- qm-coords
  "triples 내의 items들 중에 :question item들의 좌표만을 뽑아낸다."
  [triples]
  (for [x (range 5)
        y (range 3)
        :when (= :question (get-in triples [x y]))]
    [x y] ))


;; <example>
;; (payline->coords 0)
;; => [[0 1] [1 1] [2 1] [3 1] [4 1]]
(defn- payline->coords
  "해당 num에 해당하는 payline의 좌표 5개를 반환한다."
  [num]
  (->> num
       (get fm/pay-lines*)
       (map-indexed vector)
       vec))


;; <sample>
;; coords  => [[0 1] [1 1] [2 1] [3 1] [4 1]]
;; triples => [[:question :bar3 :cherry]
;;             [:bar1 :lemon :question]
;;             [:lemon :question :apple]
;;             [:watermelon :blue7 :red7]
;;             [:red7 :question :cherry]]
;; return  => [:bar3 :lemon :question :blue7 :question]
(defn- coords->items
  "coords 좌표에 해당하는 item들을 triples에서 뽑아낸다."
  [coords triples]
  (->> coords
       (mapv (fn [coord] (get-in triples coord)))))


(defn- candidate-for-qm
  "한 개의 payline을 대상으로, qm이 뒤집혔을 때 사용자에게 가장 유리한 최적의 item을
   뽑아낸다.
  #[x y] qm이 나타난 좌표값
  #line-num {int} qm을 지나가는 여러 라인들 중의 하나의 라인
  #triples {vec<vec>}"
  [[x y] line-num triples]
  (let [line-coords (payline->coords line-num)
        line-items  (coords->items line-coords triples)]
    (reduce (fn [acc x]
              (cond
               ;; 첫번째 triple에 :question이 있는 경우
               (zero? x)
               (loop [x 1]
                 (cond
                  ;; :question이 5개 연속해 있을 경우에는 jackpot으로 처리한다
                  (= x 5) (assoc acc 0 :gold7)
                  :else (if-let [item (get acc x)]
                          (if (= item :question)
                            ;; :question이 연속해 있는 경우
                            (recur (inc x))
                            (assoc acc 0 item) ))))

               :else
               (if-let [item (get acc x)]
                 (if (= item :question)
                   (assoc acc  x (get acc (dec x)))
                   acc) )))
            line-items
            (range x 5))))

;; <sample>
;; candidates => ([:cherry :cherry :cherry :cherry :cherry]
;;                [:bar3 :bar3 :bar3 :bar1 :cherry]
;;                [:cherry :cherry :lemon :grape :blue7]
;;                [:lemon :lemon :lemon :bar1 :cherry]
;;                [:cherry :cherry :cherry :bar1 :cherry]
;;                [:cherry :cherry :bar3 :cherry :cherry]
;;                [:red7 :red7 :bar3 :grape :cherry])
;; return => ([16000 :cherry] [3600 :bar3] [0 :cherry] [2000 :lemon]
;;            [1600 :cherry] [0 :cherry] [600 :red7])
(defn- calc-candidate-scores
  "qm이 뒤집혔을때 각각의 candidate들에 대해 점수를 계산한다."
  [candidates per-line-bet]
  (map (fn [line-items]
         (let [n (some #(if (apply = (take % line-items)) %)
                       [5 4 3 2])
               n (or n 0)
               bingo-item (first (take 1 line-items))]
           (if (or (zero? n)
                   (and (= n 2)
                        (#{:apple :lemon :grape :watermelon :cherry} bingo-item) ))
             [0 bingo-item]
             [(get-in (fm/get-paytable per-line-bet) [bingo-item n]) bingo-item] )))
       candidates))


;; <sample>
;; See find-qm-back, calc-candidate-scores
(defn- choose-best-candidate
  "qm이 뒤집혔을 때 사용자에게 가장 유리한 item들중, 가장 고득점을 올릴 수 있는 최적의
   item을 뽑아낸다."
  [candidates per-line-bet]
  (let [scores     (apply concat (calc-candidate-scores candidates per-line-bet))
        scores-map (apply hash-map scores)
        max-score  (apply max (keys scores-map))]
    [(get scores-map max-score) max-score]))


;; <sample>
;; [x y]   => [0 2]
;; triples => [[:bar2 :red7 :question]
;;             [:red7 :question :cherry]
;;             [:bar3 :lemon :cherry]
;;             [:grape :bar1 :cherry]
;;             [:blue7 :bar1 :cherry]]
;; line-nums  => [1 3 7 11 13 17 19]
;; candidates => ([:cherry :cherry :cherry :cherry :cherry]
;;                [:bar3 :bar3 :bar3 :bar1 :cherry]
;;                [:cherry :cherry :lemon :grape :blue7]
;;                [:lemon :lemon :lemon :bar1 :cherry]
;;                [:cherry :cherry :cherry :bar1 :cherry]
;;                [:cherry :cherry :bar3 :cherry :cherry]
;;                [:red7 :red7 :bar3 :grape :cherry])
;; [best-item max-score] => [:cherry 16000]
;; return  => [[0 2] :cherry 16000]

(defn- find-qm-back
  "[x y] 좌표에 해당하는 qm이 뒤집혔을 때, 사용자에게 가장 유리한 item 한 개를 찾아낸다."
  [[x y] triples bet-line-count per-line-bet]

  (let [line-nums  (get-in lines-for-qm* [x y])
        line-nums_ (filter #(< % 20) line-nums) ; NOTE(kep) 20은 하드코딩이삼.
        candidates (map #(candidate-for-qm [x y] % triples)
                        line-nums_)
        [best-item _] (choose-best-candidate candidates per-line-bet)]
    [[x y] best-item]))


;; <sample>
;; qm-coords => ([0 2] [1 1])
;; triples   => [[:bar2 :red7 :question]
;;               [:red7 :question :cherry]
;;               [:bar3 :lemon :cherry]
;;               [:grape :bar1 :cherry]
;;               [:blue7 :bar1 :cherry]]
;; return    => [[[0 2] :cherry 16000] [[1 1] :cherry 1600]]
(defn- gen-qm-backs
  "qm-coords에 주어진 모든 qm 좌표들에 대해, 사용자에게 가장 유리한 item을 모두 찾아낸다."
  [qm-coords triples bet-line-count per-line-bet]
  (let [triples_ (atom triples)]
    (reduce (fn [backs [x y]]
              (let [back-info (find-qm-back [x y] @triples_ bet-line-count per-line-bet)
                    back-item (get back-info 1 back-info)]
                (swap! triples_ assoc-in [x y] back-item)
                (conj backs back-info) ))
            []
            qm-coords)))


(defn- replace-qms
  "triples내의 qm item들을 qm-backs에 주어진 item들로 교체한다."
  [triples qm-backs]
  (reduce (fn [triples [[x y] item]]
            (assoc-in triples [x y] item))
          triples
          qm-backs))


(defn- calc-total-score
  "triples의 총 점수를 계산한다."
  [triples bet-line-count per-line-bet]

  (let [qm-coords (qm-coords triples)
        qm-backs  (and (seq qm-coords)
                       (gen-qm-backs qm-coords triples bet-line-count per-line-bet))
        triples1  (and (seq qm-backs)
                       (replace-qms triples qm-backs))
        triples2  (or triples1 triples)]
    (fm/calc-payout-amount (fm/bingo-lines triples2 bet-line-count) per-line-bet)))


(defn- filter-triples
  "첫번째 triple과 두번쨰 triple 각각의 item들이 서로 한 개도 중복되지 않게 하면, payline 상의
   모든 연결이 차단되는 점을 이용해, 연결을 강제로 끊어버려 점수를 무효화 시킨다."
  [triples]
  (->> triples
       (drop 2)
       (concat (->> (shuffle basic-items*)
                    (partition 3)
                    (take 2)
                    (map vec)))
       vec))


(defn get-random-triples-info
  "triples를 임의로 생성한다.

   # 성공:
   [triples qm-backs amount]

   # 참고:
   triples:  [[itm1 itm2 itm3] ...]
   qm-backs: [[coord item] ...]
   amount: number
  "
  [bet-line-count per-line-bet]

  (let [triples (-> (random-triples) insert-gold7s insert-qms)]

    (if (fm/jackpot? (fm/bingo-lines triples bet-line-count))

      ;; jackpot이면,
      (do
        ;; FIXME(kep) jackpot일때 처리.
        (get-random-triples-info bet-line-count per-line-bet))

      ;; jackpot이 터지지 않으면,
      ;; TODO(kep) refactoring 필요.
      (let [triples4  (if (or (<= (calc-total-score triples bet-line-count per-line-bet)
                                  (* bet-line-count per-line-bet))
                              (>= SLOTMACHINE/PERCENT-PASS (rand-int 100)))
                        triples
                        (filter-triples triples))
            qm-coords (qm-coords triples4)
            qm-backs  (and (seq qm-coords)
                           (gen-qm-backs qm-coords triples4 bet-line-count per-line-bet))
            qm-backs (into {} qm-backs)]
        {:triples triples4 :qm-backs qm-backs}))))
