(ns vita.scene.main-lobby.net.q-gift-count
  (:require [vita.db.gift-db :as db]))

(require '[philos.debug :refer :all])

(defn Q_GIFT_COUNT
  "<sid str>
   <return {:count int}>"
  [sock data]
  #_(dbg data "Q_GIFT_COUNT")

  (let [{:keys [sid]} data] 
    (db/respond-gift-count sid) ))
