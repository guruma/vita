(ns vita.scene.main-lobby.net.q-gift-accept
  (:require [vita.db.gift-db :as db]))

(require '[philos.debug :refer :all])

(defn Q_GIFT_ACCEPT
  "<return nil>"
  [sock data]
  (dbg data "Q_GIFT_ACCEPT")

  (let [{:keys [sid gift]} data]   ;; <sid str> <gift {}>
    (db/respond-gift-accept sid gift)
    nil))
