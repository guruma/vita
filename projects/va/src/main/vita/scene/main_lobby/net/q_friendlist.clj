(ns vita.scene.main-lobby.net.q-friendlist
  (:require
    [vita.shared.const.err-code :as ERR]
    [vita.friends :as friends]
    [vita.err :as err] ))


(defn- valid-data? [data]
  (if (nil? (:page data))
    false true))


(defn Q_FRIENDLIST
  "
              # 입력:
              {:cmd PROTOCOL/Q_PAYMENT
               :token token
               :data {}}

              # 출력:
              {:cmd PROTOCOL/R_PAYMENT
               :err PROTOCOL/ERR_NONE
               :data {account}}
            "
  [sid data]

  (when-not (valid-data? data)
    (err/in-error ERR/INVALID_INDATA))

  (let [page (:page data)]

    {:friends (friends/get-friends sid page)
     :cur-page page
     :max-page (friends/get-max-page sid)}))
