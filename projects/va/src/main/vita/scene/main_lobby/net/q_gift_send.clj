(ns vita.scene.main-lobby.net.q-gift-send
  (:require [vita.db.gift-db :as db]
            [vita.db.util.log-sender :as log-sender]
            ))

(require '[philos.debug :refer :all])

(defn Q_GIFT_SEND
  "<return nil>

  GIFT DATA 구조.
  {:description You and friends will get Free Spins +10 times!,
   :_id 8f6a5eb657e98830c1dce78cca1fd0a8,
   :_rev 1-76c305bea96b3c4093b7c0f21eb985e0,
   :facebook-msg Free Spins 10 times gift to you!,
   :end-date [2014 12 31],
   :value [freespin 10],
   :reward [freespin 10],
   :title Free Spins Gift,
   :sub-title Free Spins +10 times,
   :gift-id gift-dbb09c3c-28ec-45bb-9856-a1b3c8d4c2cd,
   :period daily, :start-date [2014 8 20]}

  INVITEE
    [1516741208547903]"

[sock data]

  (let [{:keys [sid new-invitees gift]} data]
    (db/respond-gift-send sid new-invitees gift)
    (log-sender/gift sid new-invitees gift)
    nil))
