(ns vita.scene.main-lobby.net.q-login
  (:require
    [clojure.tools.logging :refer :all]

    [sonaclo.shared.util :as util]
    [clojure.core.async :as async :refer [<! >! alts! put! take! timeout chan go close!]]
    [vita.sdk :as sdk]
    [vita.err :as err]
    [vita.session :as session]
    [vita.account]
    [vita.event.user-event :as event]
    [vita.shared.const.err-code :as ERR]
    [vita.db.game-db :as db]
    [vita.friends :as friends] ))


(declare login)

(defn Q_LOGIN
  "
  # 입력:
  {:login-token}

  # 출력:
  {:tk
   :sidR
   :account}
  "
  [sock data]
  (let [{:keys [login-token]} data
        [sid account] (login login-token)]

    (dosync

     ;; tk 갱신.
     (let [tk (session/refresh-tk account)]

       ;; tk - sock 연결.
       ;; TODO(kep) sid기반으로 만들것.
       (session/add-tk-sock tk sock)

       ;; 로긴 정보 전송.
       {:tk tk
        :sid (:social-id account)
        :friendlist  {:friends (friends/get-friends sid 1)
                      :cur-page 1
                      :max-page (friends/get-max-page sid)}
        :events (:events account)
        :account (dissoc account :event)}))))




(defn save-login-data
  "db에 유저 접속 시간 및 email, avatar-url를 기록한다."
  [sid fb-user]
  (future (db/update-login-data sid fb-user)) )


(defn- login
  "login-token => [sid account]"
  [login-token]
  (let [[sid fb-user] (vita.sdk/get-sid-userinfo login-token)
        account (or (vita.account/get-account sid)
                    (vita.account/register-account sid fb-user))]

    ;; account가 없으면 에러.
    (when-not account
      (err/in-error ERR/DB_INTERNAL_ERROR))

    ;; FB에서 친구 목록 얻어온다.
    (when-let [friends (vita.sdk/get-friends login-token)]
      (dosync (friends/add-friends sid friends)))

    (save-login-data sid fb-user)

    [sid (session/sid->account sid)]))
