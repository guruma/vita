(ns vita.scene.main-lobby.net.q-event
  (:require
    [vita.db.game-db :as db]
    [vita.shared.const.main-lobby :as PROTOCOL]
    [vita.shared.const.freecoin :as FREECOIN]
    [vita.session :as session]
    [vita.event.user-event :as user-event]
    [vita.db.game-db :as db]
    [vita.err :as err]))


(defn- valid-data? [data]
  true )


(defn Q_EVENT
  [sid data]

  (let [{:keys [uid]} data
        holding-money (session/sid->usermoney sid)
        db-account (db/event-award sid uid holding-money)]

    (dosync
     (session/add-sid-account sid db-account))

    {:uid uid
     :account (session/sid->account sid) }))
