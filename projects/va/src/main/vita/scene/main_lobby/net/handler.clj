(ns vita.scene.main-lobby.net.handler
  (:require

    [vita.shared.const.main-lobby :as MAIN_LOBBY]
    [vita.scene.main-lobby.net.q-login :as q-login]
    [vita.scene.main-lobby.net.q-join-lobby :as q-join-lobby]
    [vita.scene.main-lobby.net.q-event :as q-event]
    [vita.scene.main-lobby.net.q-friendlist :as q-friendlist]
    [vita.scene.main-lobby.net.q-invite-friend :as q-invite-friend]
    [vita.scene.main-lobby.net.q-gift-count :as q-gift-count]
    [vita.scene.main-lobby.net.q-gift-info :as q-gift-info]
    [vita.scene.main-lobby.net.q-gift-send :as q-gift-send]
    [vita.scene.main-lobby.net.q-gift-accept :as q-gift-accept]


    ))

(def x-handler*
  {
   MAIN_LOBBY/Q_LOGIN
   {:in-schema nil
    :out-schema nil
    :is-req-login? true
    :proc #'q-login/Q_LOGIN}

   MAIN_LOBBY/Q_JOIN_LOBBY
   {:in-schema nil
    :out-schema nil
    :is-req-login? false
    :proc #'q-join-lobby/Q_JOIN_LOBBY}

   MAIN_LOBBY/Q_EVENT
   {:in-schema nil
    :out-schema nil
    :is-req-login? false
    :proc #'q-event/Q_EVENT}

   MAIN_LOBBY/Q_INVITE_FRIEND
   {:in-schema nil
    :out-schema nil
    :is-req-login? false
    :proc #'q-invite-friend/INVITE_FRIEND}


   MAIN_LOBBY/Q_FRIENDLIST
   {:in-schema nil
    :out-schema nil
    :is-req-login? false
    :proc #'q-friendlist/Q_FRIENDLIST}

   MAIN_LOBBY/Q_GIFT_COUNT
   {:in-schema nil
    :out-schema nil
    :is-req-login? false
    :proc #'q-gift-count/Q_GIFT_COUNT}

   MAIN_LOBBY/Q_GIFT_INFO
   {:in-schema nil
    :out-schema nil
    :is-req-login? false
    :proc #'q-gift-info/Q_GIFT_INFO}

   MAIN_LOBBY/Q_GIFT_SEND
   {:in-schema nil
    :out-schema nil
    :is-req-login? false
    :proc #'q-gift-send/Q_GIFT_SEND}

   MAIN_LOBBY/Q_GIFT_ACCEPT
   {:in-schema nil
    :out-schema nil
    :is-req-login? false
    :proc #'q-gift-accept/Q_GIFT_ACCEPT}

   })
