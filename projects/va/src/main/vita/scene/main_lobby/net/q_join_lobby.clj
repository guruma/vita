(ns vita.scene.main-lobby.net.q-join-lobby
  (:require
    [clojure.tools.logging :refer :all]

    [vita.err :as err]
    [vita.db.game-db :as game-db]
    [vita.session :as session]
    [vita.friends :as friends] ))


(defn Q_JOIN_LOBBY
  "
  # 입력:
  {}

  # 출력:
  {:friendlist
   :account}
  "
  [sid _]
  (let [db-account  (game-db/update-events sid)
        _           (dosync (session/add-sid-account sid db-account))
        account     (session/sid->account sid)]


    ;; 조인 정보 전송.
    {:friendlist  {:friends (friends/get-friends sid 1)
                   :cur-page 1
                   :max-page (friends/get-max-page sid)}
     :account (dissoc account :event)}))
