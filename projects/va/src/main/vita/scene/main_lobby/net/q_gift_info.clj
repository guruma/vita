(ns vita.scene.main-lobby.net.q-gift-info
  (:require [vita.db.gift-db :as db]))

(require '[philos.debug :refer :all])

(defn Q_GIFT_INFO
  "<return {:sendable   {:gifts (<gift {}>*)
                         :invitees (<sid str>+)}
            :acceptable {:gifts (<gift {}>*)} }>"
  [sock data]

  ;; <sid str>
  (let [{:keys [sid]} data]
    (db/respond-gift-info sid) ))
