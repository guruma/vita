(ns vita.scene.main-lobby.net.q-invite-friend
  (:require
    [vita.db.game-db :as db]
    [vita.shared.const.main-lobby :as PROTOCOL]
    [vita.err :as err]
    [vita.db.util.log-sender :as log_sender]
    [vita.shared.const.err-code :as ERR]
    ))


(defn already-exist-invitee?
  [sid invitee]

  (-> sid
      (db/get$)
      :invitee
      (get (keyword invitee))))


(defn INVITE_FRIEND

  [sid data]

  (let [{:keys [invitee]} data]

    ;; invitee 이미 초대한 유저인지 확인
    (when (already-exist-invitee? sid invitee)
      (err/in-error ERR/ALREADY_INVITE))

    (log_sender/invite-friend sid invitee)
    ;; friend-db에 정보를 추가한다.
    (db/add-invitee sid invitee)

    {}))
