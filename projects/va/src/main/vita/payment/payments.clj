(ns vita.payment.payments
  "=======================

   모듈 책임 : 결제를 검증하고 관련 정보를 DB에 기록한다.
   모듈 이름 : vita.payment.payments
   연관 모듈 : vita.payment.products vita.payment.request-id vita.payment.db-updator
   모듈 작성자 : guruma@weebinar.com

   모듈 정의 :
     1. 결제 정보의 무결성을 검증한다.
     2. 결제 정보의 유효성을 검증한다.
     3. 결제를 DB에 기록한다.

   ========================"
  (:require
   ; 연관 모듈
   [vita.payment.products :refer [get-product]]
   [vita.payment.request-id :refer [get-request-info]]
   [vita.payment.verifier :as fb-verif]
   [vita.payment.db-updator :as db]
   [vita.config.macros :as config]
   [sonaclo.shared.util :as util]

   ; 구현에 필요한 라이브러리
   [clojure.tools.logging :refer :all]
   [clj-http.client :as http]
   [ring.util.codec :as codec]
   [pandect.core :as pan]
   [vita.session :as session]
   [clojure.data.json :as json]))


(defn my-print [& args]
   (warn (apply str args)))


(defn- make-payment-info [payment]
  (my-print "====== make-payment-info =======")
  (my-print payment)
  (let [{:keys [product-id token]} (get-request-info (:request-id payment))
        sid     (session/tk->sid token)
        account (session/sid->account sid)]
    (my-print "product-id: " product-id ", game-token: " token)
    (my-print "user-info: " account)
    (if account
      (let [product   (get-product product-id)
            {:keys [payment-id request-id amount currency quantity]} payment]
;;         (my-print payment)
;;         (my-print product)
        {:social-id    sid
         :payment-id   payment-id
         :request-id   request-id
         :amount       amount
         :currency     currency
         :quantity     quantity
         :earned-money        (:chips product)
         :user-name    (-> account :user :name)
         :user-email   (-> account :user :email)
         :user-cur-money   (-> account :user :money)}))))


(defn facebook-payment? [payment]
  (fb-verif/facebook-payment? payment))


(defn verify-payment [payment]
  (fb-verif/verify-payment payment))


(defn verify-realtime-update [req]
  (fb-verif/verify-realtime-update req))


(defn write-payment [payment]

  (my-print "==== write-payment ====")
  (when-let [payment-info (make-payment-info payment)]
    (my-print "==== write-payment : payment-info ====")
;;     (my-print payment-info)
    (db/update-db-payment payment-info)))


(defn log-payment [payment]
  (let [{:keys [status id payment-id request-id]} payment]
    (db/log-payment
     {:social-id    id
      :payment-id   payment-id
      :request-id   request-id
      :status       status})))


(defn payment?
  "checks a request via client from facebook after user payment."
  [params]
  (facebook-payment? params))


(defn subscribe?
  "checks a request for checking facebook RealTime Callback subscrtiption"
  [params]
  (and (= "subscribe" (:hub.mode params))
       ; hub.verify-token은 앱 대쉬보드의 Canvas Payments의 Realtime Updates에 있는 Verification Token에서 설정한다.
       (= "5757" (:hub.verify-token params))))



;; test

;; (time
;;  (dotimes [n 100]
;;    (fetch-payments-object
;;     {:entry [{:id "511634608967335", :time 1405230725, :changed-fields ["actions"]}]})))
;; ;; "Elapsed time: 39463.394 msecs"

;; (def po
;;   (fetch-payments-object
;;    {:entry [{:id "511634608967335", :time 1405230725, :changed-fields ["actions"]}]}))

;; po

;; (write-payment-object (first po))


