(ns vita.payment.db-updator
  "=======================

   모듈 책임 : 결제 처리 정보를 DB에 저장하고 업데이트한다.
   모듈 이름 : vita.payment.db-updator
   연관 모듈 : 없음
   모듈 작성자 : ?

   모듈 정의 :
     1. 결제 처리 정보를 DB에 로그로 기록한다.
     2. 결제 처리 정보에 따라 해당 유저의 chips를 업데이트한다.

   ========================"
  (:require [vita.db.game-db :as game-db]
            [vita.db.payment-db :as payment-db]
            [vita.db.log-db :as log-db]
            [vita.db.db-const :as const]
            [vita.session :as session]
            [clojure.tools.logging :refer :all]))


(defn my-print [& args]
  (warn (apply str args)))


(defn log-payment [info]
  (payment-db/insert info))


(defn update-game-db [info]
  (let [sid (:social-id info)
        earned-money (:earned-money info)
        holding-money (session/sid->usermoney sid)]
    (log-db/money-log sid earned-money nil holding-money nil const/PAYMENT )
    (game-db/update-user-money sid earned-money )))


(defn update-db-payment
  [info]
  (my-print "==== update-db-payment ====")
  ;  (my-print info)
  (try
    (update-game-db info)
    ;; 예외 처리
    (catch Exception e
      (my-print "==== exception : update-game-db ====")
      (my-print e)
      ;TODO : log db에 기록해야 함.
      ))
  (try
    (log-payment info)
    ;; 예외 처리
    (catch Exception e
      (my-print "==== exception : log-payment ====")
      (my-print e)
      ;TODO : log db에 기록해야 함.
      )))

