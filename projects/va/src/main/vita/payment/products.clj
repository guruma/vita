(ns vita.payment.products
  "=======================

   모듈 책임 : 가상화폐(Chip) 혹은 게임머니를 파는 상품을 정의한다.
   모듈 이름 : vw.products
   연관 모듈 : 없음.
   모듈 작성자 : guruma@weebinar.com

   모듈 정의 :
     1. 상품과 가격을 정의한다.
     2. 상품은 지역별로 여러가지 가격 정책을 가질 수 있다.
     3. 가격은 지역(locale)별로 통화 단위 별로 정의한다.
     4. 해당 상품에 대한 페이스북 og(open graph) 객체를 생성한다.

   ========================"
  (:require
   [vita.config.macros :as config])
  )


; :en 이나 :ko 는 지역(locale)을 의미한다. 지역에 따른 판매 전략의 차별화를 반영하기 위해.
(def prices
  {:price-ex  {:ko {:USD 1.00 :KRW 1100}
               :en {:USD 1.00 :MXN  300}}
   :price-2   {:en {:USD 3.00}}
   :price-3   {:en {:USD 9.00}}
   :price-4   {:en {:USD 19.00}}
   :price-5   {:en {:USD 39.00}}
   :price-6   {:en {:USD 59.00}}
   :price-7   {:en {:USD 99.00}}
   :price-8   {:en {:USD 199.00}}
   :price-9   {:en {:USD 299.00}}
   })

; image : 50px by 50px size. png, jpg, gif
;   image는 url이다. http://www.friendsmash.com/images/coin_600.png"
; plural_title : Title of the product when a quantity more than 1 is purchased.
; price : minimum price is 0.01
; currency : Currency is a string representing the ISO-4217-3 currency code.
;   cf. https://developers.facebook.com/docs/concepts/payments/currencies
;   만약 사용자가 제품에 없는 통화로 결제하면, 제품에 표시된 최초의 통화를 기준으로 환전하여 계산.
;   따라서 표시되는 맨 위의 통화는 USD가 되는지 보장해야 한다.
; chip : 게임 통화. 혹은 가상 통화
;   유저는 게임 아이템을 현지 통화(USD KRW 등)로 직접 구매하는 것이 아니라, 게임 통화 혹은 가상 통화로 구입한다.


(def image-url (str "https://"
                    (config/config:get-in [:api :facebook :product-host])
                    "/vita-login/img/common/buychips/wplus_chips.png"))


(def products [
  {:name "name" :category "category"  :image image-url  :description "description"  :price-id :price-2    :chips 750000}
  {:name "name" :category "category"  :image image-url  :description "description"  :price-id :price-3    :chips 3600000}
  {:name "name" :category "category"  :image image-url  :description "description"  :price-id :price-4    :chips 9500000}
  {:name "name" :category "category"  :image image-url  :description "description"  :price-id :price-5    :chips 27300000}
  {:name "name" :category "category"  :image image-url  :description "description"  :price-id :price-6    :chips 64900000}
  {:name "name" :category "category"  :image image-url  :description "description"  :price-id :price-7    :chips 198000000}
  {:name "name" :category "category"  :image image-url  :description "description"  :price-id :price-8    :chips 497500000}
  {:name "name" :category "category"  :image image-url  :description "description"  :price-id :price-9    :chips 897000000}
  ])


(defn- price-table [products prices locale currency]
  (for [{:keys [price-id chips]} products
        :let [price (get-in prices [price-id locale])]]
    [price chips]
  ))


(defn products-catalog []
  (str [products prices]))


(defn product-url [product-id]
  (str "https://"
       (config/config:get-in [:api :facebook :product-host])
       "/products/og/product/" product-id))


(defn price-of [product]
  (apply concat
         (let [price (get-in prices [(:price-id product) :en])]
           (for [[currency amount] price]
             ["product:price:amount"   (str amount)
              "product:price:currency" (name currency)]))))


(def prefix "og: http://ogp.me/ns#
  fb: http://ogp.me/ns/fb#
  product: http://ogp.me/ns/product#")


(defn product-og-item [product-id]
  (if-let [product (get products product-id)]
    (concat
     ["og:type"                  "og:product"
      "og:title"                 "W+ CASINO Chip"
      "og:plural_title"          "W+ CASINO Chips"
      "og:image"                 (:image product)
      "og:description"           (:description product)
      "og:url"                   (product-url product-id)]
     (price-of product))
    (str "no-product-id : " product-id)))


(defn og-product [product-id]
  [:head {:prefix prefix}
   (for [[property content] (partition 2 2 (product-og-item (read-string product-id)))]
     [:meta {:property property :content content}])])


(defn get-product [product-id]
  (get products product-id))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;; test ;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; (def product-item-0 ["og:type"                  "og:product"
;;                      "og:title"                 "W+ CASINO Chip"
;;                      "og:plural_title"          "W+ CASINO Chips"
;;                      "og:image"                 "url"
;;                      "og:description"           "description"
;;                      "og:url"                   "http://test.sonaclo.com/products/og/product/0"
;;                      "product:price:amount"     "3.0"
;;                      "product:price:currency"   "USD"])

;; (def product-item-7 ["og:type"                  "og:product"
;;                      "og:title"                 "W+ CASINO Chip"
;;                      "og:plural_title"          "W+ CASINO Chips"
;;                      "og:image"                 "url"
;;                      "og:description"           "description"
;;                      "og:url"                   "http://test.sonaclo.com/products/og/product/7"
;;                      "product:price:amount"     "299.0"
;;                      "product:price:currency"   "USD"])

;; ;; ; no-product-test : invalid product id
;; (= "no-product-id : -1" (product-item -1))
;; (= "no-product-id : 999999" (product-item 999999))

;; (product-item 0)
;; (= product-item-0 (product-item 0))
;; (= product-item-7 (product-item 7))

;; (for [n (range 16)]
;;   (= (get product-item-7 n) (get (vec (product-item 7)) n)))

;; "og:type"                  "og:product"
;; "og:title"                 "W+ CASINO Coin"
;; "og:plural_title"          "W+ CASINO Coins"
;; "og:image"                 "http://www.friendsmash.com/images/coin_600.png"
;; "og:description"           "W+ Casino Coins Purchase"
;; "og:url"                   "http://test.sonaclo.com/projects/og/product"
;; "product:price:amount"     "0.10"
;; "product:price:currency"   "USD"



;; <html><head prefix=
;;  "og: http://ogp.me/ns#
;;   fb: http://ogp.me/ns/fb#
;;   product: http://ogp.me/ns/product#">
;;   <meta content="og:product" property="og:type">
;;   <meta content="W+ CASINO Coin" property="og:title">
;;   <meta content="W+ CASINO Coins" property="og:plural_title">
;;   <meta content="url" property="og:image">
;;   <meta content="description" property="og:description">
;;   <meta content="http://test.sonaclo.com/products/og/product/1" property="og:url">
;;   <meta content="9.0" property="product:price:amount">
;;   <meta content="USD" property="product:price:currency"></head>
;;   </html>

;; <!DOCTYPE html>
;; <html>
;; <head prefix=
;; "og: http://ogp.me/ns#
;; fb: http://ogp.me/ns/fb#
;;   product: http://ogp.me/ns/product#">
;;   <meta content="og:product" property="og:type">
;;   <meta content="W+ CASINO Chip" property="og:title">
;;   <meta content="W+ CASINO Chips" property="og:plural_title">
;;   <meta content="url" property="og:image">
;;   <meta content="description" property="og:description">
;;   <meta content="http://test.sonaclo.com/products/og/product/0" property="og:url">
;;   <meta content="3.0" property="product:price:amount">
;;   <meta content="USD" property="product:price:currency">
;;   </head>
;;   </html>
