(ns vita.payment.verifier
  "=======================

   모듈 책임 : 페이스북에서 받은 결재 정보를 검증한다.
   모듈 이름 : vita.payment.verifier
   연관 모듈 : vita.payment.request-id vita.config.macros
   모듈 작성자 : guruma@weebinar.com

   모듈 정의 :
     1. 페이스북의 암호화된 정보를 풀 수 있어야 한다.
     2. 페이스북의 결제 정보의 유효함을 검증 할 수 있어야 한다.

   ========================"
  (:require
   ; 연관 모듈
   [vita.payment.request-id :as rig]
   [vita.config.macros :as config]

   ; 구현에 필요한 라이브러리
;;    [sonaclo.shared.util :refer [not-nil?]]
;;    [vw.log :refer [my-print]]
   [clojure.tools.logging :refer :all]
   [sonaclo.shared.util :as util]
   [ring.util.codec :as codec]
   [pandect.core :as pan]
   [clojure.data.json :as json]))


(defn my-print [& args]
  (warn (apply str args)))


(def app-info (config/config:get-in [:api :facebook]))


(defn- bytes->string [bytes]
  (apply str (map char bytes)))


(defn- base64url-decode [xs]
  (-> xs
      (.replace "/" "_")
      (.replace "-" "+")
      codec/base64-decode))


(defn verify-realtime-update [req]
  (let [hub-signature (:x-hub-signature (util/clojure-style-keywordize (:headers req)))
        payload (:body req)
        expected-sig (str "sha1=" (pan/sha1-hmac payload (:app-secret-code app-info)))]
    (my-print "==== verify-realtime-update ====")
;;     (my-print payload)
;;     (my-print expected-sig)
;;     (my-print hub-signature)
    (= hub-signature expected-sig)))


(defn- parse-signed-request
  "parse the signed request from facebook app link."
  [signed-request]
  (let [[encoded-sig payload] (.split signed-request "\\.")
        sig (base64url-decode encoded-sig)
        data (-> payload base64url-decode bytes->string json/read-json)
        expected-sig (pan/sha256-hmac-bytes payload (:app-secret-code app-info))]
;;     (my-print data)
    (if (= (String. sig) (String. expected-sig))
      data
      (my-print "vita error in parse signed-request.")
      )))


(defn- safe-payment? [payment]
  (let [parsed (-> (payment :signed-request)
                   parse-signed-request
                   util/hyphen-keywordize-keys)]
    (my-print "====== safe-payment? ======")
    ;(my-print parsed)
    ;(my-print (:payment-id payment) (:payment-id parsed))
    ;(my-print (:request-id payment) (:request-id parsed))
    ;(my-print (:amount payment) (:amount parsed))
    ;(my-print (:currency payment) (:currency parsed))
    ;(my-print (:quantity payment) (:quantity parsed))
    ;(my-print (:status payment) (:status parsed))
    (and
     (= (:payment-id payment) (:payment-id parsed))
     (= (:request-id payment) (:request-id parsed))
     (= (:amount payment) (:amount parsed))
     (= (:currency payment) (:currency parsed))
     (= (:quantity payment) (:quantity parsed))
     (= (:status payment)  (:status parsed)))))


(defn- not-nil? [n]
  (not (nil? n)))


(defn- valid-payment? [payment]
  (my-print "===== valid-payment? ======")
  ;(my-print payment)
  (not-nil?
   (let [rid (rig/get-request-info (:request-id payment))]
     (my-print "rid = " rid)
     rid)))


(defn verify-payment [payment]
  (my-print "====== verify-payment ======")
    (let [s (safe-payment? payment)
          v (valid-payment? payment)]
      ;(my-print "safe-payment? " s)
      ;(my-print "valid-payment? " v)
      (and s v)))


(defn facebook-payment? [payment]
  (:signed-request payment))



;; test

;(:access-token (get-request-id (:request-id payment)))
;; (def signed_request "qfQ-ED04gqth_MGVFb9iO1oE1r9LnmHkR738aG7Wx6o.eyJhbGdvcml0aG0iOiJITUFDLVNIQTI1NiIsImFtb3VudCI6IjAuMzAiLCJjdXJyZW5jeSI6IlVTRCIsImlzc3VlZF9hdCI6MTQwMjQ3NDQ2MywicGF5bWVudF9pZCI6NTc1NjI3OTgyNTUxMDUyLCJxdWFudGl0eSI6IjEiLCJyZXF1ZXN0X2lkIjoiMTIzNDU2Iiwic3RhdHVzIjoiY29tcGxldGVkIn0")
;; (parse-signed-request signed_request)
