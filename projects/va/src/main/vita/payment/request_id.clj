(ns vita.payment.request-id
  "모듈 이름 : vita.payment.request-id
   연관 모듈 : 없음.
   모듈 책임 : request-id 를 생성 및 관리한다.

   모듈 정의 :

     1. 유일한 request-id를 생성한다 (UUID 값).
     2. 사용자별로 최신 request-id를 쌍으로 하여 관리한다.
     3. request-id를 제거할 수 있다.
     4. 일정 시간이 지난 request-id를 제거한다.


  구현 설명 :
   clj-time based on Joda Time.
  "
  (:require

   ;; 라이브러리
   [clojure.tools.logging :refer :all]
   [clojure.core.async :as async :refer [<! >! <!! >!! timeout chan alt! alts!! go close! thread]]
   [sonaclo.util :as util]))


(defn my-print [& args]
  (warn (apply str args)))


(defonce ^:private request-id-mdb*
  (atom {}))


(def ^:private running-killer*
  (atom false))


;; request id : This ID must be alpha-numeric, no greater than 256 characters in length,
;; and must be unique for each order made within your app
(defn- make-request-id []
  (clojure.string/replace (util/make-uuid) "-" ""))


(defn add-request-id [request-id product-id token time]
  (swap! request-id-mdb* assoc request-id {:product-id product-id :token token :time time}))


(defn get-request-info [request-id]
  (@request-id-mdb* request-id))


(defn del-request-id [request-id]
  (swap! request-id-mdb* dissoc request-id))


(defn rdb []
  @request-id-mdb*)


(defn- kill-expired-request-id []
  (let [rids (keys (rdb))]
    (doseq [rid rids]
      (my-print "check..." rid)
      (if (util/elapsed-24hours? (:time (get-request-info rid)))
        (do (println "kill..." rid)
          (del-request-id rid))))))


(defn- start-killer [interval-time]
  (go
   (when-not @running-killer*
     (my-print "start killer to delete the expired request ids...")
     (reset! running-killer* true)
     (while @running-killer*
       (do
         (<! (timeout interval-time))
         (kill-expired-request-id))))))


(defn new-request-id [product-id token]
  (let [rid (make-request-id)]
    (add-request-id rid product-id token (util/now-str))
    rid))


;; 로딩시 바로 시작.
(def everyday (* 1000 60 60 24))
(start-killer everyday)

;; test

;; (start-killer (* 1000 60))
;; (new-request-id 4 "ddd")
;; (rdb)

;; (del-request-id "d4bbe55a0cf64a91bb6671ae63fb1f8b")
;; (new-request-id)
;; (del-request-id "81a9c0f43f8c48d99546ce56523b4b68")
