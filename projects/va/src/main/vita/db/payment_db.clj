(ns vita.db.payment-db
  (:require
    [clojure.tools.logging :refer :all]
    [slingshot.slingshot :refer [throw+ try+]]

    [vita.db.impl.protocols :as impl]
    [vita.db.impl.cloudant :as cloudant]
    [vita.config.read-config :as config]
    [vita.db.validators :as v]
    [vita.db.db-const :as CONST]

    [vita.err :as err]
    [vita.shared.const.err-code :as INERR]
    [sonaclo.util :as util]))

;; "DB 구조
;;{:_id ":54c4af25-98a7-4ac0-ba55-04d560ba4dcd",
;; :_rev "491-4b84ab074643dd7cc0eb9dc33f23aa23",
;; :social_id   "aquua2",
;; :payment_id "none",
;; :request_id ""
;; :cash ""
;; :chip ""
;; :create_time 1400156629169,
; }
; }"
;;
;; "
;; ************************
;; DB의 키워드중에 '-'는 반드시 '_' 로 처리. view로 검색할 때 '-'는 처리 되지 않는다.
;; ex) {:social-id :social-id}-> {:social_id :social-id}

;; DB에 _id의 value가 아닌 일반 Field의 value는 keyword가 사라진다.
;; ex)  {:_id :id :social_id :value} -> DB에는 {_id :id :social_id  value} 로 저장
;; ************************
;; "

(defonce db*
         (cloudant/->Cloudant (:payment config/db-info*)))


;;;
(defn- put%
  ([contents validator]
   (put% contents validator CONST/RETRY_COUNT))

  ([contents validator try-count]
   (when-not (pos? try-count)
     (err/in-error INERR/DB_RETRY_FAIL))

   (try+
     (-> db*
         (impl/insert
           (if validator (v/validate validator contents) contents)))
     (catch Object _
       (put% contents validator (dec try-count))))))


(defn- get%
  ([sid validator]
   (get% sid :social_id validator CONST/RETRY_COUNT))

  ([sid view-name validator]
   (get% sid view-name validator CONST/RETRY_COUNT))

  ([sid view-name validator try-count]
   (when-not (pos? try-count)
     (err/in-error INERR/DB_RETRY_FAIL))

   (try+
     (let [doc (-> db*
                   (impl/query ["views" view-name {:key sid}])
                   (first))]
       (if validator (v/validate validator doc) doc))
     (catch Object _
       (get% sid view-name validator (dec try-count))))))


;;; Basic API.
(defn put$
  ([contents]           (put$ contents nil))
  ([contents validator] (put% contents validator)))


(defn get$
  ([key]           (get$ key nil))
  ([key validator] (get% key validator))
  ([key view-name _] (get% key view-name nil)))


;;; Db Init.
(defn insert
  [{:keys [social-id payment-id request-id earned-money amount currency quantity user-name user-email user-cur-money] :as info}]
  (println "===== payment-db/insert ======")
  (println info)
  (if (nil? (get%  payment-id :payment_id nil))
    (let [ctime (util/now-str)]
      (put$ {:social_id    social-id
            :payment_id    payment-id
            :request_id    request-id
            :payment_time  ctime
            :earned-money  earned-money
            :amount        amount
            :currency      currency
            :quantity      quantity
            :name          user-name
            :email         user-email
            :user-cur-money user-cur-money}))))
