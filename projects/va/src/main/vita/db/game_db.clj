(ns vita.db.game-db
  (:require
    [clojure.tools.logging :refer :all]
    [slingshot.slingshot :refer [throw+ try+]]
    [clojure.tools.logging :as log]
    [vita.db.impl.protocols :as impl]
    [vita.db.impl.cloudant :as cloudant]
    [vita.app :as app]
    [sonaclo.shared.util :as util]
    [vita.db.validators :as v]

    [com.ashafa.clutch :as couch]
    [vita.err :as err]
    [vita.const :as CONST]
    [vita.const.event :as EVENT]
    [vita.shared.const.err-code :as INERR]
    [vita.event.events :as events]
    [vita.const.db :as DB]
    [clojure.core.async :as async :refer [<! >! <!! alts! put! take! timeout chan go close!]]
    [clj-time.core :as t]
    [clj-time.coerce :as c]
    [vita.db.gift-db :as gift-db]
    [vita.db.log-db :as log-db]
    [vita.db.db-const :as const]))


;; FIXME(kep) DB가 너무 커졌고 작업이 필요함.

;; "DB 구조
;;{:_id ":54c4af25-98a7-4ac0-ba55-04d560ba4dcd",
;; :_rev "491-4b84ab074643dd7cc0eb9dc33f23aa23",
;; :social_id "aquua2",
;; :user {:gender nil, :locale nil, :email nil,
;;        :last_logout_time 1401765041551, :avatar_url "",
;;        :last_access_time 1401765777216, :name "aquua2",
;;        :money 2000000},
;; :event "none",
;; :create_time 1400156629169,
;; :payment_id xxxxxxxxxxxxx,
;; :game {:slotmachine {:freespin_cnt 0, :bet_amount 200}},
;; :_conflicts ["220-88c36bf0528a3d2e0648c092140e96eb"]
; }
; }"
;;
;; "
;; ************************
;; DB의 키워드중에 '-'는 반드시 '_' 로 처리. view로 검색할 때 '-'는 처리 되지 않는다.
;; ex) {:social-id :social-id}-> {:social_id :social-id}

;; DB에 _id의 value가 아닌 일반 Field의 value는 keyword가 사라진다.
;; ex)  {:_id :id :social_id :value} -> DB에는 {_id :id :social_id  value} 로 저장
;; ************************
;; "

(defonce game-db*
         (cloudant/->Cloudant (:game app/db-info*)))

(declare update-inviter)

;;;
(defn- put%
  ([contents validator]
   (put% contents validator CONST/DB_RETRY_COUNT))

  ([contents validator try-count]

   (when-not (pos? try-count)
     (err/in-error INERR/DB_RETRY_FAIL))

   (try+
     (-> game-db*
         (impl/insert
           (if validator (v/validate validator contents) contents)))
     (catch Object _
       (put% contents validator (dec try-count))))))



(defn- get%
  ([sid validator]
   (get% sid :social_id validator CONST/DB_RETRY_COUNT))

  ([sid view-name validator]
   (get% sid view-name validator CONST/DB_RETRY_COUNT))

  ([sid view-name validator try-count]

   (when-not (pos? try-count)
     (err/in-error INERR/DB_RETRY_FAIL))

   (try+
     (let [doc (-> game-db*
                   (impl/query ["views" view-name  {:key sid}])
                   (first))]
       (if validator (v/validate validator doc) doc))
     (catch Object _
       (get% sid view-name validator (dec try-count))))))


(defn- gets%
  ([sid validator]
   (gets% sid :social_id  validator))

  ([sid view-name validator ]
   (gets% sid view-name validator CONST/DB_RETRY_COUNT))

  ([sid view-name validator try-count]
   (when-not (pos? try-count)
     (err/in-error INERR/DB_RETRY_FAIL))

   (try+
     (let [doc (-> game-db*
                   (impl/query ["views" view-name  {:key sid}])
                   )]
       (if validator (v/validate validator doc) doc))
     (catch Object _
       (gets% sid view-name validator (dec try-count))))))


(defn- get-friend%
  "친구들 목록을 money순으로 가져온다.
  INPUT PARAM
  sids : [id id id~~]
  DB에서 나오는 결과물
  ['100001224562514' 1496000 ] ['100001321343782' 1500000]"
  ([sids validator]
   (get-friend% sids validator CONST/DB_RETRY_COUNT))

  ([sids validator try-count]

   (when-not (pos? try-count)
     (err/in-error INERR/DB_RETRY_FAIL))
   (try+
     (-> game-db*
         (impl/query ["views" :friend_rank  {:keys sids}]))
     (catch Object _
       (get-friend% sids validator (dec try-count))))))


(defn- update%
  ([contents validator]
   (update% contents validator CONST/DB_RETRY_COUNT))

  ([contents validator try-count]
   (when-not (pos? try-count)
     (err/in-error INERR/DB_RETRY_FAIL))

   (try+
     (-> game-db*
         (impl/update
           (if validator (v/validate validator contents) contents)))
     (catch Exception e
       (println "[UPDATE ERROR] " )
       (println (.printStackTrace e))
       (throw+)
       )
     (catch Object _
       (update% contents validator (dec try-count))))))


;;; Basic API.
(defn put$
  "외부 노출 함수"
  ([contents]           (put$ contents nil))
  ([contents validator] (put% contents validator)))



(defn get$
  "외부 노출 함수"
  ([sid]           (get$ sid nil))
  ([sid validator] (get% sid validator)))

(defn get-hyphen$
  "keyword의 _를 - 로 변경한 후 return한다."
  [sid]
  (let [db-doc (get$ sid)]
    (util/hyphen-keywordize-keys (dissoc db-doc :_id :_rev))))

(defn update$
  "외부 노출 함수"
  ([contents]           (update$ contents nil))
  ([contents validator] (update% contents validator)))

(defn get-friend$
  "ids 을 가져온다."
  ([sids]           (get-friend$ sids nil))
  ([sids validator]
   (let [db-doc (get-friend% sids validator)]
     (reverse (sort-by #(second %) db-doc)) )))

;;; Helper functions && macros.
(defn valid-sid? [sid]
  (util/not-nil? sid))


(defn transaction-executor [sid fnc retry-count]
  (when-not (pos? retry-count)
    (err/in-error INERR/DB_RETRY_FAIL))

  (try
    (do
      (let [old-doc (get$ sid v/UserData$)
            new-doc (fnc old-doc)
            ret-val (update% (merge {:_id (:_id old-doc)
                                     :_rev (:_rev old-doc)}
                                    new-doc)  v/UserData$ 1)]
        ret-val))
    (catch Exception e
      (when (<= retry-count 5)
        (<!! (timeout 1500)))
      (transaction-executor sid fnc (dec retry-count) ) )))


(defmacro with-transaction [sid & body]
  `(when (valid-sid? ~sid)
     (try+
       (transaction-executor ~sid (fn [~'account] ~@body) CONST/DB_RETRY_COUNT )
       (catch Exception ex#
         (warnf "ERROR= %s " ex#)
         (warnf "[TRANSACTION] sid=%s body=%s" ~sid '~body)
         (throw+))

       (catch Object o#
         (warnf "[TRANSACTION] sid=%s body=%s" ~sid '~body)
         (throw+)))))

;;; Db Init.
(defn insert-new-user
  "sid를 key값으로 DB에 입력한다.
   userinfo = {:name :avatar-url :email :locale :gender}"
  [sid userinfo]

  (let [ctime (System/currentTimeMillis)
        user (assoc userinfo
               :money 0
               :last_access_time ctime
               :last_logout_time ctime)]

    ;; FIXME(kep) _id에 대한 타입을 정하자.

    (let [db-doc (->> {:_id (keyword (str (java.util.UUID/randomUUID)))
                       :social_id sid
                       :user user
                       :events [(events/event-register)]
                       :create_time ctime
                       :game {:slotmachine {:bet-amount 200
                                            :freespin-count 0}}}
                      (put$))]
      (update-inviter sid)
      (util/hyphen-keywordize-keys (dissoc db-doc :_id :_rev)) )))

;;;;;;;;;;
;;;;;;;;;;   INVITEE
;;;;;;;;;;
(defn- change-invitee-state
  [sid invitee state]

  (with-transaction sid
                    (-> account
                        (update-in [:invitee]
                                   #(assoc % invitee state)))) )


(defn add-invitee
  "sid가 초대한 유저(invitee)를 update한다.
  INPUT PARAM
  -> invitee : sid
  DB에 들어가는 값. :invitee {:qwert 'join'} keyword로 바껴서 들어간다.
  "
  [sid invitee]
  (change-invitee-state sid invitee DB/FRIEND_INVITE_WAIT) )


(defn- invitee->event
  "INPUT PARAM
  -> invitee-info [:qwert join]
  "
  [invitee-info]
  ;; TODO(kep) invitee 자료구조 적용.
  (let [invitee-sid (name (first invitee-info))]
    (events/event-invite-friend (get% invitee-sid "sid_name" nil))))



(defn- check-daily-event
  [account]

  (if (events/has-daily-event? account)

    account

    (-> account
        (update-in [:events]
                   (fn [events]
                     (let [before      (c/from-long (get-in account [:user :last_access_time]))
                           after       (c/from-long (System/currentTimeMillis))
                           hour-diff   (t/in-hours  (t/interval before after))]
                       (if (> hour-diff 24)
                         (do
                           (gift-db/send-gift-from-casino (:social_id account)
                                                          (first (gift-db/filter-gift-list)))
                           events)
                         events)))))))


(defn- award-inviter
  [account]
  (let [invitee (group-by second (:invitee account))
        joins (get invitee "join")
        waits (get invitee "wait")
        invitee (into {} waits) ]
    (-> account
        (assoc :invitee invitee)
        (update-in [:events] (fn[events]
                               (if (util/not-nil? joins)
                                 (do
                                   (log-db/join-invite-friend (:social_id account) invitee)
                                   (->> joins
                                        (map invitee->event)
                                        (concat events)
                                        (vec)))
                                 events
                                 ) )))))



(defn update-events [sid]
  (let [data (with-transaction sid
                               (-> account
                                   (check-daily-event)
                                   (award-inviter)))]

    (into {} (util/hyphen-keywordize-keys (dissoc data :_id :_rev )))))


(defn event-award
  "event.clj에 event 정의되어 있다."
  [sid uid holding-money]
  (let [data (with-transaction sid
                               (let [events (:events account)
                                     event (some (fn [event] (when (= uid (:uid event)) event)) events)]

                                 (when-not event
                                   (err/in-error INERR/INVALID_EVENT_UID))

                                 (let [event-id (:id event)
                                       event-money (get-in event [:info :money])]
                                   (log-db/money-log sid event-money nil
                                                     holding-money nil
                                                     (const/eventid->log-type event-id))
                                   (-> account
                                       (assoc-in [:events] (vec (remove #{event} events)))
                                       (update-in [:user :money] + event-money )))))]

    (into {} (util/hyphen-keywordize-keys (dissoc data :_id :_rev )))))


(defn- get-inviter
  "invitee기준으로 inviter를 모두 가져온다."
  [invitee]
  (gets% invitee :invitee nil))


(defn- bulkupdate-inviter
  ([docs]
   (bulkupdate-inviter docs CONST/DB_RETRY_COUNT))

  ([docs try-count]
   (when-not (pos? try-count)
     (err/in-error INERR/DB_RETRY_FAIL))
   (try+
     (couch/bulk-update (:db game-db*) docs)
     (catch Object _
       (bulkupdate-inviter docs (dec try-count))))))


(defn update-inviter
  "INPUT PARAM
  -> state    :  vita.const.db 에 정의 됨
  -> invitee  : sid
  초대받은 사람을 기준으로 검색하여 :state를 변경한다."
  [invitee]
  (let [db-docs (get-inviter invitee )
        docs (for [doc db-docs]
               (assoc-in doc [:invitee (keyword invitee)] DB/FRIEND_INVITE_JOIN))]
    (when-not (empty? docs)
      (bulkupdate-inviter docs))))

;;;;;;;;;;
;;;;;;;;;;   USER - DATA
;;;;;;;;;;

(defn exist-user?
  "유저가 전재하는지 여부"
  [sid]
  (not-empty (get$ sid)))



(defn update-login-data
  " {:name name, :avatar-url ~.jpg, :email aquua@nate.com,
      :locale ko_KR, :gender male}"
  [sid fb-data]
  (with-transaction sid
                    (-> account
                        (update-in [:user]
                                   (fn [user]
                                     (-> user
                                         (assoc :last_access_time  (System/currentTimeMillis)
                                                                   :avatar_url        (:avatar-url fb-data)
                                                                   :email             (:email fb-data))))))))



(defn change-user-money
  [sid value]
  (with-transaction sid
                    (-> account
                        (assoc-in [:user :money] value))))


(defn save-last-logout-time
  "유저가 최종 접속한 시간을 저장한다."
  [sid]
  (with-transaction sid
                    (-> account
                        (assoc-in [:user :last_logout_time] (System/currentTimeMillis)))))

(defn del-user
  [sid]
  (let [db-doc (get$ sid)]
    (couch/delete-document (:db game-db*) db-doc )
    ))

(defn update-user-money
  [sid value]
  (with-transaction sid
                    (-> account
                        (update-in [:user :money] + value))))


;(def cc (->> (into{} (:events c))
;             (assoc c :events)
;             ))

;{:cmd :Qgiftcount, :data {:sid "100001740185892"},
;{:cmd :Qgiftcount, :data {:sid "100006361147835"}, :scene :main-lobby, :tk "7e682d67-a4db-434a-b479-bbb58e27dfc9"} :scene :main-lobby, :tk "b7cc3029-4ce7-4a97-b42b-784dfbb1d311"}
;(add-invitee "692924154108918" "692924154108918")
;(del-user "100006361147835")


;(del-user "692924154108918")
;(get$ "692924154108918")

;(del-user "sid-a1")

;(let [od    (get$ "100001740185892") ;;라이비 재혁 계정
;      sid   (:social_id od)
;      keyd  (into{} (:events od))
;      keyc  (dissoc keyd :uid :id :info)]
;  (println keyc)
;  (with-transaction sid
;                    (-> account
;                        (assoc :events keyc))))


;game-db*
#_(-> game-db*
    (impl/query ["views" :email {:key "aquua@nate.com"}])
    (first))
;(get% "aquua@nate.com" "email" nil)
;(change-user-money "sid-a1321312" 1000000)
;(change-user-money "sid-aquua2" 1000000)
#_(defn print-data []
  (println "data base...")
  (println "[sid:]  sid-a1      "(get-in (get$ "sid-a1") [:user :money]))
  (println "[sid:]  sid-aquua2  "(get-in (get$ "sid-aquua2") [:user :money]))
  (println "[sid:]  sid-aquua3  "(get-in (get$ "sid-aquua3") [:user :money]))
  (println "[sid:]  sid-aquua4  "(get-in (get$ "sid-aquua4") [:user :money]))
  (println "=============================================")

  ;(println "[sid:]  sid-aquua3  " (get-in (get$ "sid-aquua3") [:user :money]))
  )
;(print-data)


