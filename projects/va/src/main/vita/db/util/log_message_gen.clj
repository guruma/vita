(ns vita.db.util.log-message-gen
  (:require
    [vita.db.db-const :as db-const]))
(import 'java.util.Date)
(import 'java.sql.Timestamp)


(defn- time-stamp
  []
  (java.sql.Timestamp. (.getTime (java.util.Date.))))

(defn make-money-msg
  "유저의 돈 증감치를 저장한다. (go channel를 사용한다.)
  variation-money : 유저의 돈 증감치이다.
  game-name       : 돈 증감치가 어디서 되어 있는지 여부이다. (scene 이름이 된다.)
  holding-money   : 현재 보유한 본이다. game-db와 정확히 매칭은 되지 않는 데이터이다.
  log-type        : in-game중, gift, free-spin, invite-friend 등으로 얻는 데이터인지.
                    db_const에 정의되어 있다.
  etc             : 그외 다른 사항을 기록한다..
  "
  [sid variation-money game-name holding-money etc log-type ]
  {:social_id sid
   :game game-name
   :log_type log-type
   :variation_money variation-money
   :holding_money holding-money
   :etc etc
   :create_time (time-stamp)})

(defn make-gift-msg
  [sid receive-sids gift]
  {:social_id sid
   :receivers  receive-sids
   :log_type db-const/GIFT
   :gift    gift
   :create_time (time-stamp)})

(defn make-seat-poker
  [sid game-name]
  {:social_id sid
   :log_type db-const/SEAT-POKER-CNT
   :game game-name
   :create_time (time-stamp)})

(defn play-game-sid
  [sid game-name]
  {:social_id sid
   :log_type db-const/IN-PLAY-CNT
   :game game-name
   :create_time (time-stamp)})

(defn invite-friend
  "main lobby 하단의 친구 초대이다.
  sid가 invitee를 초대하였다.
  "
  [sid invitee]
  {:social_id sid
   :invitee  invitee
   :log_type db-const/INVITE-FRIEND
   :create_time (time-stamp)})

(defn join-invite-friend
  "sid가 초대한 invitee가 접속을 하였다."
  [sid invitee]
  {:social_id sid
   :invitee invitee
   :log_type db-const/JOIN-INVITE-FRIEND
   :create_time (time-stamp)
   })

(defn winner
  [sid game-name]
  {:social_id sid
   :log_type db-const/WINNER
   :game game-name
   :create_time (time-stamp)}
  )

(defn test-time
  [sid-int log-int]
  {:social_id (str "sid" sid-int)
   :log_type (str "log type" log-int)
   :test "test"
   :data  (rand-int 10000)
   :create_time (time-stamp)}
  )

;"views": {
;           "id": {
;                   "map": "function(doc){\n if(doc._id){\nemit(doc._id, doc);\n}\n}"
;},
;"money_sum": {
;               "map": "function(doc) {\n    if (doc.variation_money){\n        emit(doc.create_time, doc.variation_money);      \n    }\n  \n}",
;"reduce": "_sum"
;},
;"data_test": {
;               "map": "function(doc) {\n    if (doc.data){\n        emit(doc.create_time, doc.data);      \n    }\n  \n}",
;"reduce": "_sum"
;}
;},
;"language": "javascript"
;}