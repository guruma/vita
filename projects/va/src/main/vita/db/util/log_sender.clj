(ns vita.db.util.log-sender
  (:require
    [clojure.tools.logging :refer :all]
    [vita.app :as app]
    [com.ashafa.clutch :as cloudant]
    [sonaclo.shared.util :as util]
    [clj-time.core :as t]
    [clj-time.coerce :as c]
    [vita.db.db-const :as const]
    [vita.db.log-db :as log-db]
    [vita.db.util.log-message-gen :as make-msg]
    [vita.session :as session]
    [clojure.core.async :as async :refer [<! <!! >! alts! put! take! timeout chan go close!]]))
(import 'java.util.Date)


(defn npc-user?
  [sid]
  (-> (session/sid->account sid)
      (get-in [:user :is-npc])))


(defn money
  "유저의 돈 증감치를 저장한다. (go channel를 사용한다.)
  variation-money : 유저의 돈 증감치이다.
  game-name       : 돈 증감치가 어디서 되어 있는지 여부이다. (scene 이름이 된다.)
  holding-money   : 현재 보유한 본이다. game-db와 정확히 매칭은 되지 않는 데이터이다.
  log-type        : in-game중, gift, free-spin, invite-friend 등으로 얻는 데이터인지.
                    db_const에 정의되어 있다..
  etc             : 그외 다른 사항을 기록한다.
  "
  [sid variation-money game-name holding-money etc log-type]
  (if-not (npc-user? sid)
    (->  (make-msg/make-money-msg sid variation-money game-name holding-money etc log-type)
         (log-db/write ))))

(defn gift
  "sid -> 보낸자
  receive-sids : 받는 자."
  [sid receive-sids gift]
  (-> (make-msg/make-gift-msg sid receive-sids gift)
      (log-db/write ) ))

(defn seat-poker
  "포커 게임에 앉는 log"
  [sid game-name]
  (if-not (npc-user? sid)
    (-> (make-msg/make-seat-poker sid game-name)
        (log-db/write ))))

(defn play-game-sids
  [sids game-name]
  (doseq [sid sids]
    (if-not (npc-user? sid)
      (-> (make-msg/play-game-sid sid game-name)
          (log-db/write ))) ))

(defn invite-friend
  [sid invitee]
  (-> (make-msg/invite-friend sid invitee)
      (log-db/write)))

(defn winner
  "sid  {:high {'4b01fc4c-6ebf-4852-b522-151cd4d2bbd6' 1400}, :low {}}"
  [win-sids game-name]
  (doseq [sid (->> win-sids
                   (vals)
                   (into {})
                   (keys))]
    (if-not (npc-user? sid)
      (-> (make-msg/winner sid game-name)
          (log-db/write)) )))