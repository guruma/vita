(ns vita.db.catalog-db

  "작성자: 박상규
   목  적: https://vita.cloudant.com/catalog db를 만든다."
  (:require [com.ashafa.clutch :as clutch]
            [cemerick.url :as url]
            [clojure.walk :as walk]
            [clojure.string :as str] ))

(def catalog-db* (assoc (url/url "https://vitatest.cloudant.com" "catalog")
                     :username "vitatest"
                     :password "vitatest0987"))


(defn get-catalog []
  (clutch/get-view catalog-db* "catalog" :product-id))

; :en 이나 :ko 는 지역(locale)을 의미한다. 지역에 따른 판매 전략의 차별화를 반영하기 위해.
; image : 50px by 50px size. png, jpg, gif
;   image는 url이다. http://www.friendsmash.com/images/coin_600.png"
; plural_title : Title of the product when a quantity more than 1 is purchased.
; price : minimum price is 0.01
; currency : Currency is a string representing the ISO-4217-3 currency code.
;   cf. https://developers.facebook.com/docs/concepts/payments/currencies
;   만약 사용자가 제품에 없는 통화로 결제하면, 제품에 표시된 최초의 통화를 기준으로 환전하여 계산.
;   따라서 표시되는 맨 위의 통화는 USD가 되는지 보장해야 한다.
; chip : 게임 통화. 혹은 가상 통화
;   유저는 게임 아이템을 현지 통화(USD KRW 등)로 직접 구매하는 것이 아니라, 게임 통화 혹은 가상 통화로 구입한다.

#_
({:id "_design/catalog",
  :key "_design/catalog",
  :value {:rev "1-b510a8bbdc023b9a74b4eda2b0c75032"},
  :doc
  {:_id "_design/catalog",
   :_rev "1-b510a8bbdc023b9a74b4eda2b0c75032",
   :views
   {:product-id
    {:map "function(doc) {\n  emit(doc[\"product-id\"], doc);\n}"}},
   :language "javascript"}}
 {:id "ffb585d913c50f9b3460fb9085d9a79d",
  :key "ffb585d913c50f9b3460fb9085d9a79d",
  :value {:rev "1-63cc1987a34571db5186629973e95094"},
  :doc
  {:description "description",
   :category "category",
   :_id "ffb585d913c50f9b3460fb9085d9a79d",
   :_rev "1-63cc1987a34571db5186629973e95094",
   :chips 750000,
   :name "name",
   :image "http://www.friendsmash.com/images/coin_600.png",
   :price {:discount {:en {:USD 3.0}}, :original {:en {:USD 3}}},
   :product-id 1}}
 {:id "ffb585d913c50f9b3460fb9085d9a9fe",
  :key "ffb585d913c50f9b3460fb9085d9a9fe",
  :value {:rev "1-32e5a9e14a0d8571eea8030eee100996"},
  :doc
  {:description "description",
   :category "category",
   :_id "ffb585d913c50f9b3460fb9085d9a9fe",
   :_rev "1-32e5a9e14a0d8571eea8030eee100996",
   :chips 3600000,
   :name "name",
   :image "http://www.friendsmash.com/images/coin_600.png",
   :price {:discount {:en {:USD 9.0}}, :original {:en {:USD 14}}},
   :product-id 2}}
 {:id "ffb585d913c50f9b3460fb9085d9b8d9",
  :key "ffb585d913c50f9b3460fb9085d9b8d9",
  :value {:rev "1-27a1e82bd6c2800913f8e3b10e0250b1"},
  :doc
  {:description "description",
   :category "category",
   :_id "ffb585d913c50f9b3460fb9085d9b8d9",
   :_rev "1-27a1e82bd6c2800913f8e3b10e0250b1",
   :chips 9500000,
   :name "name",
   :image "http://www.friendsmash.com/images/coin_600.png",
   :price {:discount {:en {:USD 19.0}}, :original {:en {:USD 38}}},
   :product-id 3}}
 {:id "ffb585d913c50f9b3460fb9085d9c3d1",
  :key "ffb585d913c50f9b3460fb9085d9c3d1",
  :value {:rev "1-0ffe36db03480b3280ac5ea0942cbac6"},
  :doc
  {:description "description",
   :category "category",
   :_id "ffb585d913c50f9b3460fb9085d9c3d1",
   :_rev "1-0ffe36db03480b3280ac5ea0942cbac6",
   :chips 27300000,
   :name "name",
   :image "http://www.friendsmash.com/images/coin_600.png",
   :price {:discount {:en {:USD 39.0}}, :original {:en {:USD 110}}},
   :product-id 4}}
 {:id "ffb585d913c50f9b3460fb9085d9d167",
  :key "ffb585d913c50f9b3460fb9085d9d167",
  :value {:rev "1-4b5babf6312f95e781b0cafc98a77350"},
  :doc
  {:description "description",
   :category "category",
   :_id "ffb585d913c50f9b3460fb9085d9d167",
   :_rev "1-4b5babf6312f95e781b0cafc98a77350",
   :chips 64900000,
   :name "name",
   :image "http://www.friendsmash.com/images/coin_600.png",
   :price {:discount {:en {:USD 59.0}}, :original {:en {:USD 258}}},
   :product-id 5}}
 {:id "ffb585d913c50f9b3460fb9085d9d935",
  :key "ffb585d913c50f9b3460fb9085d9d935",
  :value {:rev "1-72392a2147ce1a95d381c84356277e1a"},
  :doc
  {:description "description",
   :category "category",
   :_id "ffb585d913c50f9b3460fb9085d9d935",
   :_rev "1-72392a2147ce1a95d381c84356277e1a",
   :chips 198000000,
   :name "name",
   :image "http://www.friendsmash.com/images/coin_600.png",
   :price {:discount {:en {:USD 99.0}}, :original {:en {:USD 802}}},
   :product-id 6}}
 {:id "ffb585d913c50f9b3460fb9085d9dbdb",
  :key "ffb585d913c50f9b3460fb9085d9dbdb",
  :value {:rev "1-6759262dd3bf0598317b5366aa850be6"},
  :doc
  {:description "description",
   :category "category",
   :_id "ffb585d913c50f9b3460fb9085d9dbdb",
   :_rev "1-6759262dd3bf0598317b5366aa850be6",
   :chips 497500000,
   :name "name",
   :image "http://www.friendsmash.com/images/coin_600.png",
   :price {:discount {:en {:USD 199.0}}, :original {:en {:USD 2000}}},
   :product-id 7}}
 {:id "ffb585d913c50f9b3460fb9085d9e7da",
  :key "ffb585d913c50f9b3460fb9085d9e7da",
  :value {:rev "1-c1de6670bb7af8437dc73bfb6b8363a2"},
  :doc
  {:description "description",
   :category "category",
   :_id "ffb585d913c50f9b3460fb9085d9e7da",
   :_rev "1-c1de6670bb7af8437dc73bfb6b8363a2",
   :chips 897000000,
   :name "name",
   :image "http://www.friendsmash.com/images/coin_600.png",
   :price {:discount {:en {:USD 299.0}}, :original {:en {:USD 3588}}},
   :product-id 8}})
