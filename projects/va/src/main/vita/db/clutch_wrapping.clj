(ns vita.db.clutch_wrapping
  (:require
    [vita.err :as err]
    [vita.db.db-const :as DB-CONST]
    [com.ashafa.clutch :as cloudant]
    [vita.shared.const.err-code :as INERR]
    [slingshot.slingshot :refer [throw+ try+]]
    [clojure.core.async :as async :refer [<! >! <!! alts! put!  timeout chan go]]))


"clutch lib를 사용할 때 사용하는 인터페이스이다.
clutch를 한번 래핑하여 db try 를 제공한다."

(declare throw-error)
(declare wait-1sec)


(defn get-doc
  "INPUT
    db        : GAME_DB인지 GIFT_DB인지의 DB 정보를 담아둔 정보
    view-name
    key       : 검색하고자 하는 단어"
  ([db view-name key]
   (get-doc db view-name key DB-CONST/RETRY_COUNT nil))

  ([db view-name key retry-count e]
   (when-not (pos? retry-count)
     (throw-error e))
   (try+
     (->> (cloudant/get-view db "views" view-name {:key key})
          (map :value)
          (first))
     (catch  Object e
       (wait-1sec retry-count)
       (get-doc db view-name key (dec retry-count) e ))) ))


(defn put
  "INPUT
   db        : GAME_DB인지 GIFT_DB인지의 DB 정보를 담아둔 정보"
 ([db doc]
  (put db doc DB-CONST/RETRY_COUNT nil))

 ([db doc retry-count e]
  (when-not (pos? retry-count)
    (throw-error e))

  (try+
    (cloudant/put-document db doc)
    (catch Object e
      (wait-1sec retry-count)
      (put db doc (dec retry-count) e))) ))


(defn- wait-1sec
  [retry-count]
  (when (< retry-count 5)
    (<!! (timeout 1000))))


(defn- throw-error
 [exception]
 (let [message (str " [MESSAGE] : " (.getMessage exception))
        err-mage (str INERR/DB_RETRY_FAIL  message)]
    (err/in-error err-mage)))