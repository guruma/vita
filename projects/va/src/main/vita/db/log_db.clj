(ns vita.db.log-db
  (:require
    [clojure.tools.logging :refer :all]
    [vita.app :as app]
    [com.ashafa.clutch :as cloudant]
    [sonaclo.shared.util :as util]
    [vita.db.db-const :as const]
    [cemerick.url :as url]
    [clojure.core.async :as async :refer [<! <!! >! alts! put! take! timeout chan go close!]]
    [vita.db.util.log-message-gen :as make-msg]
    )) ;; session 사용하면 circular error발생.


;; ************************
;; DB의 키워드중에 '-'는 반드시 '_' 로 처리. view로 검색할 때 '-'는 처리 되지 않는다.
;; ex) {:social-id :social-id}-> {:social_id :social-id}

;; DB에 _id의 value가 아닌 일반 Field의 value는 keyword가 사라진다.
;; ex)  {:_id :id :social_id :value} -> DB에는 {_id :id :social_id  value} 로 저장
;; ************************
;; "

(defonce db (:log app/db-info*))

#_(def db (assoc (url/url "https://latte.cloudant.com" "log")
          :username "latte"
          :password "lattelatte"))

(defn write
  [message]
  (future
    (cloudant/put-document db message)))


(defn money-log
  "유저의 돈 증감치를 저장한다. (go channel를 사용한다.)
  variation-money : 유저의 돈 증감치이다.
  game-name       : 돈 증감치가 어디서 되어 있는지 여부이다. (scene 이름이 된다.)
  holding-money   : 현재 보유한 본이다. game-db와 정확히 매칭은 되지 않는 데이터이다.
  log-type        : in-game중, gift, free-spin, invite-friend 등으로 얻는 데이터인지.
                    db_const에 정의되어 있다.
  etc             : 그외 다른 사항을 기록한다.
  이 구조는 log_sedner와 중복되어 있다.
  "
  [sid variation-money game-name holding-money etc log-type]
  #_(go
    (let [message (make-msg/make-money-msg sid variation-money
                                           game-name holding-money etc log-type )]
      (cloudant/put-document db message )) ))

(defn invite-friend
  [sid invitee]
  (future
    (->> (make-msg/invite-friend sid invitee)
         (cloudant/put-document db))))

(defn join-invite-friend
  "초대한 친구가 들어왔다."
  [sid invitee]
  (future
    (->> (make-msg/join-invite-friend sid invitee)
         (cloudant/put-document db))))




(defn read-money
  []
  (cloudant/get-view db "views" :money_sum {:startkey "2014-09-18" :endkey "2014-09-18"}))








;
;(comment
;
;  ;;;;;;;;;
;  ;;;;;;;;;   검색
;  ;;;;;;;;;
;  (defn get-data-by-view
;    "input :social_id  {:key social_id}
;    return -> {:_id , :_rev , :social_id a}}
;    참고사항
;    view로 검색하면  리스트 형태로 나온다.
;    ({:id :id, :key search-key, :value {:_id , :_rev , :social_id a}})"
;    ;(cloudant/get-view db "views" "sid&log_type"  {:key ["aa" "play_time"]})
;    [view-name query]
;    (cloudant/get-view db "views" view-name  query))
;
;
;  (defn get-data-by-sid
;    "DB에 저장된 데이터를 호출한다.
;    {:_id :id, :_rev rev,
;    :social_id :id,
;    :user {:money 9124200, :avatar-url avatar-p1},
;    :game {:blackjack {:level 0}, :slotmachine {:bet-amount 200, :freespin-cnt 0}}}"
;    [social_id]
;    (get-data-by-view :social_id  {:key social_id}))
;
;
;
;  (defn get-user
;    "user 데이터만 나온다."
;    [social_id]
;    (let [db-doc (get-data-by-sid social_id  )]
;      (:user (dissoc db-doc :_id :_rev )) ))
;
;
;  (defn del-user
;    [social-id]
;    (let [db-doc (get-data-by-sid social-id)]
;      (doseq [doc db-doc]
;        (let [new-doc (:value (into{} doc))]
;          (cloudant/delete-document db new-doc) ))))
;
;
;
;
;  ;;;;;;;;;
;  ;;;;;;;;;   GAME - DATA
;  ;;;;;;;;;
;  (defn- make-play-time
;    [join-time out-time]
;    (let [time-data [{:join-time join-time}{:out-time out-time}]]
;      time-data))
;
;  (defn insert-game-play-time
;    "데이터를 변경한다. update가 아니다.
;    game-name : vita.const 참고.
;    data      : [{:join {:time :join-time}}{:out {:time :out-time}}]
;    (insert-game-play-time :social_id CONST/TEXAS time time)"
;
;    ;(:texas (:game (token->account "aaa")))
;    [social_id game-name join-time out-time]
;
;    (let [db-doc (cloudant/put-document
;                   db
;                   {:_id (keyword (str (java.util.UUID/randomUUID)))
;                    :social_id social_id
;                    :game game-name
;                    :log_type LOG-TYPE/PLAY-TIME
;                    :create_time (System/currentTimeMillis)
;                    :data (make-play-time join-time out-time)}) ]
;      db-doc))
;
;
;  (defn insert-pay
;    [sid cash game-money]
;    (let [db-doc (cloudant/put-document
;                   db
;                   {:_id (keyword (str (java.util.UUID/randomUUID)))
;                    :social_id sid
;                    :log_type LOG-TYPE/PAY
;                    :create_time (System/currentTimeMillis)
;                    :data {:cash cash :game-money game-money}})]
;      db-doc))
;
;  (defn insert-game-click-cnt
;    [sid game-name]
;    (cloudant/put-document
;      db
;      {:_id (keyword (str (java.util.UUID/randomUUID)))
;       :social_id sid
;       :log_type LOG-TYPE/GAME-CLICK-CNT
;       :create_time (System/currentTimeMillis)
;       :data {:game_name game-name}}))
;
;  (defn insert-game-play-cnt
;    [sid game-name]
;    (debug "insert-game-play-cnt sid->" sid " ," game-name)
;    (cloudant/put-document
;      db
;      {:_id (keyword (str (java.util.UUID/randomUUID)))
;       :social_id sid
;       :log_type LOG-TYPE/GAME-PLAY-CNT
;       :create_time (System/currentTimeMillis)
;       :data {:game_name game-name}}))
;
;  (defn insert-log
;    [sid data]
;    (cloudant/put-document
;      db
;      {:_id (keyword (str (java.util.UUID/randomUUID)))
;       :social_id sid
;       :log_type LOG-TYPE/GAME-PLAY-CNT
;       :create_time (System/currentTimeMillis)
;       :data data}))
;
;
;  ;; 참고 (cloudant/get-view db "views" "sid&log_type"  {:key ["aa" "play_time"]})
;  (defn get-pay[]
;    (get-data-by-view "log_type" {:key [LOG-TYPE/PAY]}) )
;
;  (defn get-play-time
;    []
;    (get-data-by-view "log_type" {:key [LOG-TYPE/PLAY-TIME]}) )
;
;
;  (defn get-user-play-time
;    [sid]
;    (get-data-by-view "sid&log_type" {:key [(str sid) LOG-TYPE/PLAY-TIME]}) )
;
;  ;; Game Click
;  (defn get-game-click-cnt
;    [game-name]
;    (get-data-by-view "gamename&game_click_cnt" {:key [LOG-TYPE/GAME-CLICK-CNT game-name]}) )
;
;  (defn get-game-click-cnt-sid
;    [sid game-name]
;    (get-data-by-view "sid&gamename&game_click_cnt" {:key [sid LOG-TYPE/GAME-CLICK-CNT game-name]}) )
;
;  ;; Game Play
;  (defn get-game-play-cnt
;    [game-name]
;    (get-data-by-view "gamename&game_play_cnt" {:key [LOG-TYPE/GAME-PLAY-CNT game-name]}) )
;
;  (defn get-game-play-cnt-sid
;    [sid game-name]
;    (get-data-by-view "sid&gamename&game_play_cnt" {:key [sid LOG-TYPE/GAME-PLAY-CNT game-name]}) )
;
;
;
;  )
;;(insert-game-play-cnt :aa :texas)
;;(get-data-by-view "gamename&game_play_cnt" {:key [LOG-TYPE/GAME-PLAY-CNT :texas]})
;;(get-game-play-cnt-sid :aquua1 :texas)
;;(get-game-play-cnt :texas)
;;(get-game-play-cnt :texas)
;;(get-game-click-cnt :texas)
