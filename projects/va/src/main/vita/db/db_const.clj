(ns vita.db.db-const)


(def ^:const RETRY_COUNT 10)


;; 돈 증감치 관련 로그 타입니다.
(def ^:const FREE-SPIN :free-spin)
(def ^:const IN-GAME :in-game)
(def ^:const PAYMENT :payment)
(def ^:const GIFT :gift)

;; 게임 클릭 및 참여횟수
(def ^:const CLICK-GAME-CNT  "4개의 게임 아이콘에서 클릭하였을 때"
  :click-game-cnt)

(def ^:const SEAT-POKER-CNT "게임 자리에 앉았을 때"
  :seat-poker-cnt)

(def ^:const IN-PLAY-CNT "실제 게임을 시작하였을 때"
  :in-play-cnt)


(def ^:const JOIN-INVITE-FRIEND "초대한 친구가 접속을 하였다."
  :join-invite-friend)
(def ^:const INVITE-FRIEND "친구초대"
  :invite-friend)



(def ^:const WINNER "게임 승자"
  :winner)



(defn eventid->log-type
  "event.clj에 정의된 eventid를 log db에 저장할 때 필요한
  log type으로 변경한다."
  [event-id]
  (condp = event-id
    1 :event-register
    2 :event-daily
    3 :event-invite-friend))