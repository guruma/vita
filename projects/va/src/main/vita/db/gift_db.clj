(ns vita.db.gift-db
  (:require [com.ashafa.clutch :as clutch]
            [clj-time.core :as time]
            [clojure.tools.logging :as log]
            [vita.config.read-config :as config] ))

(require '[philos.debug :refer :all])

;; --------------
;;  gift-list db
;; --------------
;; 1. 주고 받을 선물 목록만을 저장한다.
;; 2. 선물 목록은 삭제하지 않고 계속 보관한다.
;; 3. document 구조
;;    {:gift-id      "gift-fe381e7f-dfc1-41d1-bcfa-74c8b2d8aa5e"
;;     :period       "daily"
;;     :title        "Free Spins Gift"
;;     :sub-title    "Free Spin +10 times"
;;     :description  "You and friends will get Free Spins +10 times!"
;;     :value        ["freespin" 10]
;;     :reward       ["freespin" 10]
;;     :start-date   [2014 8 20]
;;     :end-date     [2014 10 31]
;;     :facebook-msg "Free Chips 10 times gift to you!"}
(def ^:private gift-list-db* (:gift-list config/db-info*))
(def ^:private gift-list-docs* (atom (clutch/get-view gift-list-db* "views" :gift-id)))


;; -------------
;;  gift-box db
;; -------------
;; 1. gift-box db에는 받은 선물만을 기록한다.
;; 2. 선물을 받은 후에는 해당 document를 삭제한다.
;; 3. document 구조
;;    {:social-id str          ;; 선물 받을 사람의 sid
;;     :sender    <name str>   ;; 선물 준 사람의 이름
;;     :gift      {}}
(def ^:private gift-box-db* (:gift-box config/db-info*))

(defn- get-gifts-from-gift-box
  "giftbox db에서 <sid>가 받은 모든 선물을 받아온댜.
   <sid str>
   <return docs (<doc {}>*)>"
  [sid]
  (map :value (clutch/get-view gift-box-db* "views" :social-id {:key sid})))


;; ---------
;;  game db
;; ---------
;; 1. document 구조 (gift와 관련되는 부분만 발췌)
;;    {:social_id str
;;     :user {:name str, :money int}
;;     :game {:slotmachine {:freespin-count 0}}
;;     :expired-dates {<gift-id str, date [year int, month int, day int]>+}}
(def ^:private game-db* (:game config/db-info*))

(defn- get-game-doc
  "<sid str>
   <return doc {}>"
  [sid]
  (:value (first (clutch/get-view game-db* "views" :social_id {:key sid}))))


;;; 보조 함수
(defn- within-interval?
  "<start-date [year int, month int, day int]>
   <end-date [year int, month int, day int]>
   <return bool>"
  [start-date end-date]
  (time/within? (time/interval (apply time/date-time start-date)
                               (apply time/date-time end-date))
                (time/now) ))

(defn filter-gift-list
  "gift-list db에서 'gift 행사 기간' 내에 있는 선물 목록만을 걸러낸다.
   <return gifts (<gift {}>*)>"
  []
  (for [doc   @gift-list-docs*
        :let  [gift (:value doc)]
        :when (within-interval? (:start-date gift) (:end-date gift))]
    gift))

(defn- valid-gift?
  "'gift 행사 기간' 내에 보낼 수 있는 선물인지를 확인한다.
   <gift-id str>
   <expired-dates {<gift-id str, date [year int, month int, day int]>+}>
   <return bool>"
  [gift-id expired-dates]
  (if expired-dates
    (some (fn [[id expired-date]]
            (and (= id gift-id)
                 (time/after? (time/now) (apply time/date-time expired-date)) ))
          expired-dates)
    true))

(defn- get-sendable-gifts
  "<sid str>
   <return gifts {<gift {}>*}>"
  [sid]
  (let [my-doc (get-game-doc sid)]
    (filter (fn [gift]
              (valid-gift? (:gift-id gift) (:expired-dates my-doc)))
            (filter-gift-list) )))


;;; Public API
(defn respond-gift-count
  "<sid str>
   <return {:count int}>"
  [sid]
  {:count (count (get-gifts-from-gift-box sid))})

(defn respond-gift-info
  "<sid str>
   <return {:sendable   {:gifts (<gift {}>*)}?
            :acceptable {:gifts (<gift {}>*)} }>"
  [sid]
  {:sendable   {:gifts (vec (get-sendable-gifts sid))}
   :acceptable {:gifts (vec (get-gifts-from-gift-box sid))} })

(defn respond-gift-accept
  "<sid str>
   <gift {}>"
  [sid gift]
         ;; <gift-type kw> :freespin | :freechips
  (let [[gift-type num] (:value gift)
        arg             (case gift-type
                          "freespin"  [:game :slotmachine :freespin-count]
                          "freechips" [:user :money]
                          (log/warn "unmatched gift-type = " gift-type) ) ]
    ;; 받은 선물 내용을 db에 기록한다.
    (as-> (get-game-doc sid) my-doc
          (update-in my-doc arg + num)
          (clutch/update-document game-db* my-doc))

    ;; 받은 선물을 gift-box db에서 삭제한다.
    (some (fn [doc]
            (if (= (get-in doc [:gift :gift-id])
                   (:gift-id gift))
              (clutch/delete-document gift-box-db* doc) ))
          (get-gifts-from-gift-box sid) )))

(defn respond-gift-send
  "<sid str>
   <invitees (<sid str>+)>
   <gift {}>"
  [sid invitees gift]
  (let [my-doc        (get-game-doc sid)
        gift-box-docs (map (fn [sid]
                             {:social-id sid
                              :sender    (get-in my-doc [:user :name])
                              :gift      gift})
                           invitees)]
    ;; gift-box db에 선물 받는 사람들마다 선물 정보를 저장헌다.
    (clutch/bulk-update gift-box-db* gift-box-docs)

    (let [period        (:period gift)
          expired-date  (case period
                          "daily"   (time/plus (time/now) (time/days 1))
                          "weekly"  (time/plus (time/now) (time/weeks 1))
                          "monthly" (time/plus (time/now) (time/months 1))
                          (log/warn "unmatched period-type = " (:period gift)))
          expired-date' [(time/year  expired-date)
                         (time/month expired-date)
                         (time/day   expired-date)]

          [reward-type reward] (:reward gift)
          arg                  (case reward-type
                                 "freespin"  [:game :slotmachine :freespin-count]
                                 "freechips" [:user :money]
                                 (log/warn "unmatched reward-type = " reward-type) )]
      (as-> my-doc doc
            ;; gift-id에 해당하는 선물은, 각 period 마다, 하나의 선물만을 보내도록 구현한다.
            ;; 따라서 위에서 계산된 expired-date가 지나야만 나중에 새로 선물을 보낼 수 있다.
            (assoc-in doc [:expired-dates period (:gift-id gift)] expired-date')

            ;; 선물 준 사람에게도 reward를 보상한다.
            (update-in doc arg #(if %
                                  (+ % reward)
                                  reward))

            (clutch/update-document game-db* doc) ))))

(defn send-gift-from-casino
  "<receiver <sid str>>
   <gift {}>"
  [receiver gift]
  (clutch/put-document gift-box-db* {:social-id receiver
                                     :sender    "W+ Casino"
                                     :gift      gift} ))
