(ns vita.db.rank-db
  (:require
   [com.ashafa.clutch :as cloudant]
   [clj-time.core :as time]

   [vita.app :as app]
   [sonaclo.shared.util :as util]
   [vita.session :as session] ))

(defonce db (:game app/db-info*))


;;;;;;;;;
;;;;;;;;;   검색
;;;;;;;;;
(defn get-data-by-view
  "input :social_id  {:key social_id}
  return -> {:_id , :_rev , :social_id a}}
  참고사항
  view로 검색하면  리스트 형태로 나온다.
  ({:id :id, :key search-key, :value {:_id , :_rev , :social_id a}})"
  ;(cloudant/get-view db "views" "sid&log_type"  {:key ["aa" "play_time"]})
  [view-name query]
  (cloudant/get-view db "views" view-name  query))


(defn get-data-by-sid
  "DB에 저장된 데이터를 호출한다.
  {:_id :id, :_rev rev,
  :social_id :id,
  :user {:money 9124200, :avatar-url avatar-p1},
  :game {:blackjack {:level 0}, :slotmachine {:bet-amount 200, :freespin-cnt 0}}}"
  [social_id]
  (get-data-by-view :social_id  {:key social_id}))

;;;;;;;;;;;;;;;;




(defn get-user
  "user 데이터만 나온다."
  [social_id]
  (let [db-doc (get-data-by-sid social_id  )]
    (:user (dissoc db-doc :_id :_rev )) ))


(defn del-user
  [social-id]
  (let [db-doc (get-data-by-sid social-id)]
    (doseq [doc db-doc]
      (let [new-doc (:value (into{} doc))]
        (cloudant/delete-document db new-doc) ))))


;; (defn get-play-time []
;;   (get-data-by-view "log_type" {:key [LOG-TYPE/PLAY-TIME]}) )

;; (defn get-play-time
;;   [sid]
;;   (get-data-by-view "sid&log_type" {:key [(str sid) LOG-TYPE/PLAY-TIME]}) )
