(ns vita.db.impl.update_dbs
  "목  적: 필요에 따라, 원하는 db의 내용을 갱신한다.
   작성자: 김영태"
  (:require [com.ashafa.clutch :as clutch]
            [cemerick.url :as url]
            [vita.gen :as gen] ))

(def url-info*
  {:vitatest {:url "https://vitatest.cloudant.com"
              :username "vitatest"
              :password "vitatest0987"}
   :vita     {:url "https://vita.cloudant.com"
              :username "vita"
              :password "vita15987"} })

(defn make-db-url
  "<id kw>
   <db-name str>"
  [id db-name]
  (assoc (url/url (get-in url-info* [id :url]) db-name)
         :username (get-in url-info* [id :username])
         :password (get-in url-info* [id :password]) ))

(def db* (make-db-url :vitatest "gift-list"))

(def gift*
  [{:gift-id      (str "gift-" (gen/gen-uid))
    :period       "daily"
    :title        "Free Spins Gift"
    :sub-title    "Free Spins +10 times"
    :description  "You and friends will get Free Spins +10 times!"
    :value        ["freespin" 10]
    :reward       ["freespin" 10]
    :start-date   [2014 8 20]
    :end-date     [2014 12 31]
    :facebook-msg "Free Spins 10 times gift to you!"}
 #_{:gift-id      (str "gift-" (gen/gen-uid))
    :period       "daily"
    :title        "Free Chips Gift"
    :sub-title    "Free Chips $50,000"
    :description  "You and friends will get Free Chips $50,000!"
    :value        [:freechips 50000]
    :reward       [:freechips 50000]
    :start-date   [2014 8 20]
    :end-date     [2014 12 31]
    :facebook-msg "Free Chips $50,000 gift to you!"} ])

(defn update-gift-db
  [db]
  (clutch/get-database db)
  (clutch/bulk-update db gift*)
  (clutch/save-view db "views"
    (clutch/view-server-fns :javascript
      {:gift-id {:map "function (doc) {
                         return emit(doc['gift-id'], doc); }"} })))

(defn update-game-db
  [db]
  (clutch/get-database db)
  (clutch/save-view db "views"
    (clutch/view-server-fns :javascript
      {:user-money {:map "function (doc) {
                             return emit(doc['social_id'], doc.user.money); }"
                    :reduce "_stats"} })))

;(update-gift-db db*)

;(def game-db* (make-db-url :vitatest "game"))
;
;(update-game-db game-db*)
;
;(clutch/get-view game-db* "views" :user-money)



;; (comment


;; )

