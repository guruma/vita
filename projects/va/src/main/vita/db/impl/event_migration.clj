(ns vita.db.impl.event-migration
  (:require [com.ashafa.clutch :as clutch]
            [cemerick.url :as url]
            [clojure.walk :as walk]
            [clojure.string :as str] ))

(def game-db* (assoc (url/url "https://vitatest.cloudant.com" "game")
                :username "vitatest"
                :password "vitatest0987"))

(def game-live-db* (assoc (url/url "https://vita.cloudant.com" "game")
                :username "vita"
                :password "vita15987"))

;(clutch/get-document game-db* "692924154108918")
#_(clutch/get-view game-db* "views" :social_id  {:key "692924154108918"})

(defn migrate [from-db ]
  (let [all-documents   (clutch/all-documents from-db {:include_docs true})]
    (doseq [db-doc all-documents]
      (let [events (:events (:doc db-doc))]
        (when (> (count events) 2)
          (clutch/update-document game-db* (assoc (:doc db-doc) :events []))
          )
        ))

    (println "migration ended...") ))


;(comment
;(migrate game-db*))




;(def c {:_id "3a6fcfb9988c6b52ad4d7ac7cc839003",
;        :_rev "326-18b01e5da3e440127ab76ee091cf727c",
;        :game {:slotmachine {:freespin-count 50, :bet-amount 200, :freespin-cnt 0}},
;        :user {:money 31830750, :email "aquua@nate.com", :locale "ko_KR",
;               :name "박재혁", :last_access_time 1410948634160,
;               :last_logout_time 1410948606282, :gender "male",
;               :avatar_url "https://fbcdn-profile-a.akamaihd.net/hprofile-ak-xaf1/v/t1
;               .0-1/c30.30.380.380/s50x50/306616_425825207485482_1874679193_n
;               .jpg?oh=072b756b2f0e46c21c8c0eacf71fdb1b&oe=54846A37&__gda__
;               =1419026860_6b4d4b07c88c316155bf829b0f1d6fee"},
;        :social_id "100001740185892",
;        :events [["_id" "627b046b8b000fe3758fd229b5e59b98"]
;                 ["_rev" "1-302601c6875e3e6366c59d756e0a56b9"]
;                 ["social-id" "100001740185892"] ["sender" "W+ Casino"]
;                 ["gift" {:period "daily", :reward ["freespin" 10],
;                          :facebook-msg "Free Spins 10 times gift to you!",
;                          :sub-title "Free Spins +10 times", :end-date [2014 12 31],
;                          :_rev "1-8e5882315246b4cddd8a836dfc019008",
;                          :gift-id "gift-0997833f-847b-4d42-9358-fd8b87e505a3",
;                          :title "Free Spins Gift",
;                          :_id "c2182d3b871228fe7cc5ae5464de26f5",
;                          :start-date [2014 8 20], :value ["freespin" 10],
;                          :description "You and friends will get Free Spins+10 times!"}]
;                 {:uid "83a02345-01e1-4d88-b299-f8515b428916",
;                  :id 3, :info {:money 1000000, :invitee ["박재혁"]}}],
;        :invitee {}})
;

;(def d {:_id "3a6fcfb9988c6b52ad4d7ac7cc839003",
;        :_rev "339-fe66653260c96ee19cc218293b962df1",
;        :invitee {},
;        :events {:_id "627b046b8b000fe3758fd229b5e59b98",
;                 :_rev "1-302601c6875e3e6366c59d756e0a56b9",
;                 :social-id "100001740185892", :sender "W+ Casino",
;                 :gift {:period "daily", :reward ["freespin" 10],
;                        :facebook-msg "Free Spins 10 times gift to you!",
;                        :sub-title "Free Spins +10 times",
;                        :end-date [2014 12 31],
;                        :_rev "1-8e5882315246b4cddd8a836dfc019008",
;                        :gift-id "gift-0997833f-847b-4d42-9358-fd8b87e505a3",
;                        :title "Free Spins Gift",
;                        :_id "c2182d3b871228fe7cc5ae5464de26f5",
;                        :start-date [2014 8 20],
;                        :value ["freespin" 10],
;                        :description "You and friends will get Free Spins +10 times!"},
;                 :uid "83a02345-01e1-4d88-b299-f8515b428916",
;                 :id 3,
;                 :info {:money 1000000, :invitee ["박재혁"]}},
;        :social_id "100001740185892",
;        :user {:money 31830750, :email "aquua@nate.com", :locale "ko_KR", :name "박재혁", :last_access_time 1410952402972, :last_logout_time 1410952337830, :gender "male", :avatar_url "https://fbcdn-profile-a.akamaihd.net/hprofile-ak-xaf1/v/t1.0-1/c30.30.380.380/s50x50/306616_425825207485482_1874679193_n.jpg?oh=072b756b2f0e46c21c8c0eacf71fdb1b&oe=54846A37&__gda__=1419026860_6b4d4b07c88c316155bf829b0f1d6fee"}, :game {:slotmachine {:freespin-count 50, :bet-amount 200, :freespin-cnt 0}}})