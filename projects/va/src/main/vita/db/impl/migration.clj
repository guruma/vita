(ns vita.db.impl.migration
  "작성자: 김영태
   목  적: https://vita.cloudant.com/game db의 내용을 일부 수정해,
           https://vita.cloudant.com/game1 db로 이전한다."
  (:require [com.ashafa.clutch :as clutch]
            [cemerick.url :as url]
            [clojure.walk :as walk]
            [clojure.string :as str] ))

(def game-db* (assoc (url/url "https://vita.cloudant.com" "game")
                     :username "vita"
                     :password "vita15987"))

(def game1-db* (assoc (url/url "https://vita.cloudant.com" "game1")
                      :username "vita"
                      :password "vita15987"))

(defn snake-case [k]
  (-> (name k) (str/replace "-" "_") keyword))

(defn modify-doc [doc]
  (let [doc'  (dissoc doc :_id :_rev :event)
        doc'' (walk/postwalk (fn [x] (if (keyword? x) (snake-case x) x))
                             doc')]
    (assoc doc'' :events []) ))


(defn migrate [from-db to-db]
  (let [all-documents   (clutch/all-documents from-db {:include_docs true})
        valid-documents (filter #(get-in % [:doc :social_id])
                                all-documents)
        docs            (map #(:doc %) valid-documents)
        docs'           (map #(modify-doc %) docs)]
    (clutch/delete-database to-db)
    (clutch/create-database to-db)
    (clutch/bulk-update to-db docs')
    (println "migration ended...") ))

;(comment
;  (migrate game-db* game1-db*)
;)

