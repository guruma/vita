(ns vita.session-closer
  (:require
    [clojure.tools.logging :refer :all]
    [ruiyun.tools.timer :refer :all]

    [vita.session :as session]
    [vita.poker.room.room-manager :as room.room-manager]
    [vita.shared.const.err-code :as ERR]

    [vita.scene.texas.net.handler :as texas-handler]
    [vita.scene.omaha.net.handler :as omaha-handler]
    [vita.scene.omahahl.net.handler :as omahahl-handler]
    [vita.common.websocket :as websocket]))


;; TODO(kep) const값을 다른곳으로 빼내야함.

(def ^:const SESSION_ALIVE_TIME  (* 1000 60 60))
(def ^:const PERIOD_SESSION_CHECH_TIME  (* 1000 60 5))


(defn- get-time-over-tk
  "10분 지난 session을 가져온다.
  return -> ([:sid3 1397550724097][token time]~)"
  []
  (let [now     (System/currentTimeMillis)
        timeout SESSION_ALIVE_TIME]
    (filter #(> (- now  (second %)) timeout)  @session/sid-time*  )))


(defn close-session
  [sock]

  (when-let [tk (session/sock->tk sock)]
    (let [sid (session/tk->sid tk)
          rid (session/sid->rid sid)
          game-name (session/sid->game-name sid)]
      (when rid
        (do
           (room.room-manager/pass-packet
             {:cmd :Qquit :scene game-name :data {:room-id rid}} sid)))
      (dosync (session/wipe-session sid)))))


(defn- wipe-sessions
  "1시간동안 반응없는 user의 ws를 close한다.
  유저 SESSION정보를 모두 삭제한다.q"
  []
  (doseq [sid-time (get-time-over-tk)]
    (let [sid   (first sid-time)]
      (when-let [sock (session/sid->sock sid)]
        (websocket/send sock {:cmd :Btimeout :err ERR/ERR_NONE :time (second sid-time)})
        (close-session sock)
        ))))


(defn session-close-timer
  "10분 지난 session을 close한다. 2분마다 한번씩 한다."
  []
  (run-task! wipe-sessions :period PERIOD_SESSION_CHECH_TIME))
