(ns vita.sdk
  (:require
    [sdk.facebook]
    [clojure.tools.logging :refer :all]
    [vita.config.macros :as am]
    [vita.db.game-db :as db]
    [slingshot.slingshot :refer [throw+ try+]]
    ))

;; TODO(kep) 가짜 친구목록을 만들어주기. for tester.

(defn- wrap-facebook-info
  "facebook data => {:name :avatar-url :email :locale :gender"

  [fb-data]

  {:name (:name fb-data)
   :avatar-url (get-in fb-data [:picture :data :url])
   :email (:email fb-data)
   :locale (:locale fb-data)
   :gender (:gender fb-data)})


(defn fb-convter
  "facebook-friends.data => [{:name :sid :avatar-url}]"

  [fb-friends]
  (->> fb-friends
       :data
       (mapv (fn [f]
               {:name (:name f)
                :sid  (:id f)
                :avatar-url (get-in f [:picture :data :url])}))))



(defn get-friends
  "login-token => [{:name :sid :avatar-url :money :registered?}]"
  [login-token]

  ;; TODO(kep) 예외처리 클라이언트에 어떻게 보내줄 것인가?

  (when-not (= :internal (am/config:get-in [:level]))
    (try

      (let [fb-friends            (sdk.facebook/get-friends login-token)
            fb-invitable-friends  (sdk.facebook/get-invitable-friends login-token)

            friends               (fb-convter fb-friends) ;[{:sid xx :name xx avatar-url xx} {~}]
            invitable-friends     (fb-convter fb-invitable-friends) ;[{:sid xx :name xx avatar-url xx} {~}]

            sids                  (map :sid friends)
            sid-mony-dic (into {} (db/get-friend$ sids)) ;{"sid-aquua2" 1500000, "sid-aquua1" 1500000}
            retval (map (fn [friend]
                          (if-let [money (get sid-mony-dic (:sid friend))]
                            (assoc friend :money money :registered? true)
                            (assoc friend :money 0 :registered? false)))
                        (concat friends invitable-friends))]
        (sort-by :money > retval))

      (catch Exception _
             (errorf "VITA.SDK GET_FRIEND ERROR LOGIN-TOKEN = %s " login-token)))))




(defn get-sid-userinfo
  "성공: [sid, {:name :avatar-url :email :locale :gender}]"

  [login-token]

  (if (= :internal (am/config:get-in [:level]))

    ;; 내부 테스트는 가짜로 만들어주자.
    [(str "sid-" login-token) {:name       (str "name-" login-token)
                               :avatar-url ""
                               :email      ""
                               :locale     ""
                               :gender     ""}]

      ;; 외부는 sdk 이용.
      ;; TODO(kep) login-token 에 대한 검증 필요.
      ;; TODO(kep) sdk에 대한 retry기능을 넣고 에러처리를 해주자.
      (let [sdk-userinfo (sdk.facebook/get-facebook-user-info login-token)]
        [(:id sdk-userinfo) (wrap-facebook-info sdk-userinfo)])))