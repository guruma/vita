(ns vita.account
  (:require
   [clojure.tools.logging :refer :all]
   [vita.session :as session]
   [vita.db.game-db :as db]))


(defn get-account
  "친구가 접속했는지 여부를 확인하기 위해 DB에서 읽어와야 한다."
  [sid]

  ;; TODO(kep) 리펙토링 할것.
  (when-let [_ (db/get-hyphen$ sid)]

    ;; 유저가 받을 이벤트를 갱신해주고,
    (let [account (db/update-events sid)]

      ;; sid 와 account 정보를 연결해준다.
      (dosync
       (session/add-sid-account sid account))

      account)))


(defn register-account
  "신규 유저를 DB에 저장한다."
  [sid userinfo]

  ;; 혹시 모르니 account를 얻어와보고,
  (if-let [account (get-account sid)]

    account

    ;; 그래도 없으면, db에 새로이 써주자.
    (do
      (let [account (db/insert-new-user sid userinfo)]
        (dosync
         (session/add-sid-account sid account))
        account))))
