(ns vita.dealer
  "목  적: 1) 카드를 분배한다 -> request-cards
           2) 상금을 계산한다 -> calc-awards
   작성자: 김영태" 
  (:require [vita.poker.calc.score :as score]
            [vita.poker.calc.pots :as pots] ))

(def ^:private deck*
  [:club-A    :club-2    :club-3     :club-4    :club-5    :club-6    :club-7
   :club-8    :club-9    :club-10    :club-J    :club-Q    :club-K
   :heart-A   :heart-2   :heart-3    :heart-4   :heart-5   :heart-6   :heart-7
   :heart-8   :heart-9   :heart-10   :heart-J   :heart-Q   :heart-K
   :diamond-A :diamond-2 :diamond-3  :diamond-4 :diamond-5 :diamond-6 :diamond-7
   :diamond-8 :diamond-9 :diamond-10 :diamond-J :diamond-Q :diamond-K
   :spade-A   :spade-2   :spade-3    :spade-4   :spade-5   :spade-6   :spade-7
   :spade-8   :spade-9   :spade-10   :spade-J   :spade-Q   :spade-K])

(defn- shuffle-deck
  "Shuffles the deck of cards.
   <deck [<card kw>+]>
   <return deck [<card kw>+]> shuffled deck"
  [deck]
  (reduce (fn [acc _]
            (shuffle acc))
          deck
          (range 10) ))

;; ---------------
;; user 관련 함수

(defn- cmp-users
  "<user-a {:cards (<card kw>+)
            :score {<:high | :low> {:category <category kw>
                                    :hand [<card kw>+] }}}>
   <user-b {:cards (<card kw>+)
            :score {<:high | :low> {:category <category kw>
                                    :hand [<card kw>+] }}}>
   <mode kw>  :high
            | :low  (only for :omahahl)"
  ([user-a user-b]
   (cmp-users user-a user-b :high))
  ([user-a user-b mode]
   (let [score-a [(score/category* (get-in user-a [:score mode :category]))
                  (mapv (comp score/*ranks* score/card->rank)
                        (get-in user-a [:score mode :hand]) )]
         score-b [(score/category* (get-in user-b [:score mode :category]))
                  (mapv (comp score/*ranks* score/card->rank)
                        (get-in user-b [:score mode :hand]) )]]
     (case mode
       :high (compare score-b score-a)
       :low  (compare score-a score-b) ))))

(defn- sort-users-by-score
  "Sorts <users> in descended order based on :score.
   <users ({:cards (<card kw>+)
            :score {<:high | :low> {:category <category kw>
                                    :hand [<card kw>+] }}}+ )>
   <mode kw>  :high
            | :low  (only for :omahahl)
   <return (user+)> sorted uesrs"
  ([users]
   (sort-users-by-score users :high))
  ([users mode]
   (case mode
     :high (sort #(cmp-users %1 %2 :high) (vec users))
     :low  (binding [score/*ranks* (assoc score/*ranks* :A 1)]
             (sort #(cmp-users %1 %2 :low) (vec users)) ))))

(defn- filter-users
  "[:score :low]의 값이 nil인 user를 제거한다.
   <users ({:cards (<card kw>+)
            :score {<:high | :low> {:category <category kw>
                                    :hand [<card kw>+] }}}+ )>
   <return (user+)> filtered users"
  [users]
  (remove (fn [user]
            (nil? (get-in user [:score :low])))
          users))

(defn- select-users-by-sids
  "<users ({:cards (<card kw>+)
            :score {<:high | :low> {:category <category kw>
                                    :hand [<card kw>+] }}}+ )>
   <sids  (<sid str>+}
   <return (<user+> selected users"
  [users sids]
  (filter #((set sids) (:sid %))
          users))


;; -----------------
;; winner 관련 함수

(defn calc-winner
  "user들 중에서 winner가 누구인지를 계산한다.
   <users ({:cards (<card kw>+)
            :score {<:high | :low> {:category <category kw>
                                    :hand [<card kw>+] }}}+ )>
   <mode kw>  :high
            | :low  (only for :omahahl)
   <return (<sid str>+)> winner의 sids: 점수가 동일할 때, 복수명의 승자가 나올 수 있다."
  ([users]
   (calc-winner users :high))
  ([users mode]
   (let [users1 (sort-users-by-score users mode)
         users2 (if (= mode :low)
                   (filter-users users1)
                   users1)
         winner  (first users2)]
     (if winner
       (loop [users3 (next users2)
              acc    [(:sid winner)]]
         (let [user (first users3)]
           (if (= 0 (cmp-users winner user mode))
             (recur (next users3) (conj acc (:sid user)))
             (vec (sort acc)) )))))))


;; -------------------
;; 카드 요청 관련 함수

(defmulti request-cards
  "<game-name kw>
   <user-cnt int> the number of users"
  (fn [game-name user-cnt] game-name))

(defmethod request-cards :texas
  [game-name user-cnt]
  (let [deck            (shuffle-deck deck*)
        community-cards (take 5 deck)
        cards           (partition 2 (take (* 2 user-cnt)
                                           (drop 5 deck)))
        users (map (fn [personal-cards]
                   {:cards personal-cards
                    :score (score/calc-score game-name personal-cards community-cards)})
                 cards)]
    {:community-cards community-cards
     :users (sort-users-by-score users)} ))

(defmethod request-cards :default   ;; for :omaha or :omahahl
  [game-name user-cnt]
  (let [deck            (shuffle-deck deck*)
        community-cards (take 5 deck)
        cards           (partition 4 (take (* 4 user-cnt)
                                           (drop 5 deck)))
        users (map (fn [personal-cards]
                   {:cards personal-cards
                    :score (score/calc-score game-name personal-cards community-cards)})
                 cards)]
    {:community-cards community-cards
     :users (sort-users-by-score users)} ))


;; -------------------
;; 상금 계산 관련 함수

(defn make-sid-bets
  "<users ({user}+) See calc-awards.
   <return ([sid str, bet int]+)>"
  [users]
  (into {} (map (fn [user]
                  [(:sid user) (:bet user)])
                users)))

(defn get-fold-sids
  "<users ({user}+) See calc-awards.
   <return (<sid str>+)?>"
  [users]
  (keep (fn [user]
          (if-not (seq (:cards user))
            (:sid user) ))
         users))

(defn calc-awards
  "<game-name kw>
   <users (user+)>
    <user>의 한 예
     {:sid \"sid0\"
      :bet 30
      :cards [:spade-J :spade-5 :spade-8 :club-K]
      :score
      {:high
       {:category :three-of-a-kind,
        :hand [:club-K :heart-K :diamond-K :spade-A :spade-J]},
       :low
       {:category :low-card,
        :hand [:spade-8 :spade-6 :spade-5 :heart-2 :spade-A]}}}
   <return awards {:overage [money int, sid str]?
                   :high    ([money int, [<sid str>+]+])
                   :low     ([money int, [<sid str>*]+])?}"
  [game-name users]
  (let [sid-bets  (make-sid-bets users)
        fold-sids (get-fold-sids users)
        {:keys [overage awards]} (pots/calc-pots sid-bets fold-sids)]
    {:overage overage
     :high (vec (for [[award candidates] awards]
                  [award (calc-winner (select-users-by-sids users candidates))] ))
     :low  (if (= :omahahl game-name)
             (vec (for [[award candidates] awards]
                    [award (calc-winner (select-users-by-sids users candidates) :low)] )))}))


;; (comment   ;; test samples

;; (dbg (request-cards :texas 5))
;; ; => {:community-cards (:club-K :spade-J :spade-5 :heart-K :diamond-A),
;; ;     :users
;; ;     ({:cards (:spade-10 :diamond-J),
;; ;       :score
;; ;       {:high
;; ;        {:category :two-pair,
;; ;         :hand [:heart-K :club-K :spade-J :diamond-J :diamond-A]}}}
;; ;      {:cards (:diamond-8 :club-8),
;; ;       :score
;; ;       {:high
;; ;        {:category :two-pair,
;; ;         :hand [:heart-K :club-K :diamond-8 :club-8 :diamond-A]}}}
;; ;      {:cards (:heart-5 :diamond-10),
;; ;       :score
;; ;       {:high
;; ;        {:category :two-pair,
;; ;         :hand [:heart-K :club-K :spade-5 :heart-5 :diamond-A]}}}
;; ;      {:cards (:diamond-3 :heart-10),
;; ;       :score
;; ;       {:high
;; ;        {:category :one-pair,
;; ;         :hand [:heart-K :club-K :diamond-A :spade-J :heart-10]}}}
;; ;      {:cards (:diamond-4 :heart-6),
;; ;       :score
;; ;       {:high
;; ;        {:category :one-pair,
;; ;         :hand [:heart-K :club-K :diamond-A :spade-J :heart-6]}}})}

;; (dbg (request-cards :omahahl 5))
;; ; => {:community-cards (:spade-Q :spade-A :heart-K :club-4 :club-7),
;; ;     :users
;; ;     ({:cards (:diamond-5 :diamond-3 :diamond-10 :spade-J),
;; ;       :score
;; ;       {:high
;; ;        {:category :straight,
;; ;         :hand [:spade-A :heart-K :spade-Q :spade-J :diamond-10]},
;; ;        :low
;; ;        {:category :low-card,
;; ;         :hand [:club-7 :diamond-5 :club-4 :diamond-3 :spade-A]}}}
;; ;      {:cards (:diamond-J :heart-8 :club-6 :spade-10),
;; ;       :score
;; ;       {:high
;; ;        {:category :straight,
;; ;         :hand [:spade-A :heart-K :spade-Q :diamond-J :spade-10]},
;; ;        :low
;; ;        {:category :low-card,
;; ;         :hand [:heart-8 :club-7 :club-6 :club-4 :spade-A]}}}
;; ;      {:cards (:heart-A :club-Q :club-8 :club-10),
;; ;       :score
;; ;       {:high
;; ;        {:category :two-pair,
;; ;         :hand [:heart-A :spade-A :club-Q :spade-Q :heart-K]},
;; ;        :low nil}}
;; ;      {:cards (:spade-7 :spade-8 :heart-5 :club-K),
;; ;       :score
;; ;       {:high
;; ;        {:category :two-pair,
;; ;         :hand [:club-K :heart-K :spade-7 :club-7 :spade-A]},
;; ;        :low
;; ;        {:category :low-card,
;; ;         :hand [:spade-8 :club-7 :heart-5 :club-4 :spade-A]}}}
;; ;      {:cards (:heart-4 :heart-Q :club-3 :heart-2),
;; ;       :score
;; ;       {:high
;; ;        {:category :two-pair,
;; ;         :hand [:heart-Q :spade-Q :heart-4 :club-4 :spade-A]},
;; ;        :low
;; ;        {:category :low-card,
;; ;         :hand [:club-7 :club-4 :club-3 :heart-2 :spade-A]}}})}


;; (def users*
;;   [{:sid "sid0"
;;     :cards [:heart-3 :diamond-A]
;;     :score {:high {:category :one-pair,
;;                    :hand [:spade-A :diamond-A :diamond-10 :spade-9 :heart-6]}}
;;     :bet 30}
;;    {:sid "sid1"
;;     :cards [:spade-J :diamond-9]
;;     :score {:high {
;;                    :category :one-pair,
;;                    :hand [:spade-9 :diamond-9 :spade-A :spade-J :diamond-10]}}
;;     :bet 15}
;;    {:sid "sid2"
;;     :cards nil
;;     :score {:high {:category :one-pair,
;;                    :hand [:diamond-5 :spade-5 :spade-A :diamond-10 :spade-9]}}
;;     :bet 15}
;;    {:sid "sid3",
;;     :cards [:spade-4 :spade-8]
;;     :score {:high {:category :one-pair,
;;                    :hand [:heart-4 :spade-4 :spade-A :diamond-10 :spade-9]}}
;;     :bet 60}
;;    {:sid "sid4"
;;     :cards nil
;;     :score {:high {:category :high-card,
;;                    :hand [:spade-A :diamond-Q :diamond-10 :spade-9 :heart-6]}}
;;     :bet 60} ])

;; (def users2*
;;   [{:sid "sid0"
;;     :bet 30
;;     :cards [:spade-J :spade-5 :spade-8 :club-K]
;;     :score
;;     {:high
;;      {:category :three-of-a-kind,
;;       :hand [:club-K :heart-K :diamond-K :spade-A :spade-J]},
;;      :low
;;      {:category :low-card,
;;       :hand [:spade-8 :spade-6 :spade-5 :heart-2 :spade-A]}}}
;;    {:sid "sid1"
;;     :bet 15
;;     :cards [:heart-Q :diamond-A :heart-3 :spade-7]
;;     :score
;;     {:high
;;      {:category :two-pair,
;;       :hand [:diamond-A :spade-A :heart-K :diamond-K :heart-Q]},
;;      :low
;;      {:category :low-card,
;;       :hand [:spade-7 :spade-6 :heart-3 :heart-2 :spade-A]}}}
;;    {:sid "sid2"
;;     :bet 15
;;     :cards nil
;;     :score
;;     {:high
;;      {:category :two-pair,
;;       :hand [:heart-K :diamond-K :diamond-6 :spade-6 :spade-10]},
;;      :low
;;      {:category :low-card,
;;       :hand [:diamond-7 :spade-6 :diamond-3 :heart-2 :spade-A]}}}
;;    {:sid "sid3"
;;     :bet 60
;;     :cards [:diamond-2 :diamond-8 :club-Q :heart-9]
;;     :score
;;     {:high
;;      {:category :two-pair,
;;       :hand [:heart-K :diamond-K :diamond-2 :heart-2 :club-Q]},
;;      :low nil}}
;;    {:sid "sid4"
;;     :bet 60
;;     :cards nil
;;     :score
;;     {:high
;;      {:category :one-pair,
;;       :hand [:heart-K :diamond-K :spade-A :spade-Q :club-7]},
;;      :low
;;      {:category :low-card,
;;       :hand [:spade-6 :club-5 :club-4 :heart-2 :spade-A]} }}])

;; (dbg (calc-awards :texas users*))
;; ; => {:overage nil, :high ([75 ["sid0"]] [45 ["sid0"]] [60 ["sid3"]]), :low nil}

;; (dbg (calc-awards :omahahl users2*))
;; ; => {:overage nil,
;; ;     :high ([75 ["sid0"]] [45 ["sid0"]] [60 ["sid3"]]),
;; ;     :low ([75 ["sid1"]] [45 ["sid0"]] [60 nil])}

;; ) ; end of comment
