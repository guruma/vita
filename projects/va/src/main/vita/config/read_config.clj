(ns vita.config.read-config
  "모듈 이름 : vita.config.read-config
   연관 모듈 : 없음.
   모듈 책임 : 구성 파일에서 정보를 읽어온다."

  (:require
   [cemerick.url :as urls]
   [vita.config.macros :as am])
  (:import [java.net.URL]))


(defn- get-db-info
  [db-config where]

  (let [{:keys [username password connect]} db-config]
    (-> (get connect where)
        (urls/url)
        (assoc :username username
               :password password))))


(defn f-zipmap
  "
  ```clojure
  >> (f-zipmap inc [1 2 3])
  => {1 2, 2 3, 3 4}
  "
  [f col]
  (->> col
       (reduce (fn [acc x] (assoc acc x (f x))) {})))


(def db-info*
  (->> [:payment :game :gift :gift-list :gift-box]
       (f-zipmap #(get-db-info (:db am/config*) %))))
