(ns vita.config.macros
  (:require
   [vita.config.loader :as config]))


;; TODO(kep) config*를 app쪽으로 빼는 방안을 생각해보자.
(def config*
  "환경변수 VA_CONFIG를 읽어들인 값이 온다.(edn형식)"
  (config/load-config :va-config))


;; TODO(kep)  config를 app로 옮긴후, 매크로가 아닌 함수로 처리할 수 있도록 하자.
(defmacro config:get-in
  "config의 값을 읽어옴."
  [get-v]
  (get-in config* get-v))


;; TODO(kep) 추후에도 안쓰면 아에 삭제를 해버리자.

#_(defmacro DEBUG
  "config의 :debug값이 true인 경우에만 실행됨."
  [& body]
  (when (get config* :debug)
    `(do
       ~@body)))


#_(defmacro LEVEL
  "config의 :level에 따른 분기를 타도록 도와줌.."
  [& body]
  (let [level
        (get config* :level)

        cond-dic
        (->> `~body
             (partition 2)
             (reduce (fn [acc [f s]] (assoc acc f s)) {}))]
    (get cond-dic level)))
