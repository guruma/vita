(ns vita.macros)

(defmacro casep [param & rst]
    `(condp (fn [pred# _#] (pred# ~param)) nil
            ~@rst))


(defmacro when-let* [bindings & body]
  (when (not (even? (count bindings)))
    (throw (IllegalArgumentException.
             "when-let* requires an even number of forms in binding vector")))

  (let [when-lets (reduce (fn [sexpr bind]
                            (let [form (first bind)
                                  tst (second bind)]
                              (conj sexpr `(when-let [~form ~tst]))))
                          ()
                          (partition 2 bindings))
        body (cons 'do body)]
    `(->> ~body ~@when-lets) ))