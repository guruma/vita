(ns vita.app
  (:require
   [cemerick.url :as urls]
   [sonaclo.shared.util :as util]
   [vita.config.macros :as am])
  (:import [java.net.URL]))


;; TODO(kep) 이쪽 살작 맘에 안듬. 파일이름 변경도 고려. db-config-reader?

(defn- get-db-info
  [db-config where]

  (let [{:keys [username password connect]} db-config]
    (-> (get connect where)
        (urls/url)
        (assoc :username username
               :password password))))


(def db-info*
  (->> [:game :log :rank]
       (util/f-zipmap #(get-db-info (:db am/config*) %))))
