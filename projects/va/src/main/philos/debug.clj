(ns philos.debug
  (:require (clojure [string :as str]
                     [pprint :as pp]
                     [stacktrace :as stacktrace] )))

(defn dhead
  "Prints head message.
   <msg str> the message to print
   <times int> the number to repeat pad-char, default: 20
   <pad-char str> the character to use in padding, default: \"=\""
  [& [msg times pad-char]]
  (let [times    (or times 20)
        pad-char (or pad-char "=")]
    (if msg
      (let [pad-chars (apply str (repeat times pad-char))]
        (println "\n" pad-chars msg pad-chars))
      (println "\n") )))

(defmacro dbg [x & more]
  `(binding [*print-length* 100]
     (let [[return# more#] [~x (vector ~@more)]
           n#   (some #(if (number? %) %) more#)
           msg# (some #(if (string? %) %) more#)
           test# (let [v# (map-indexed vector more#)
                       i# (some #(if (= (get % 1) :if)
                                  (inc (get % 0)))
                           v#)
                       t# (get-in (vec v#) [i# 1])]
                  t#)
           return# (or (and n# (take n# return#))
                       return#)
           pprint# (str/trim (with-out-str (pp/pprint return#)))
           format# (str "dbg"
                        (and msg# (str " <" msg# ">"))
                        ": " '~x " =>%n%s%n%n")]
       (when (or (nil? test#) test#)
         (printf format# pprint#)
         (flush))
       return#) ))

(defmacro assert*
  "Evaluates expr and throws an exception if it doesn't evaluate to logical true.
   Returns true if it does."
  {:added "1.0"}
  ([x]
     (when *assert*
       `(try
          (when-not ~x
            (throw (new AssertionError (str "Assert failed: " (pr-str '~x)))))
          true
          (catch AssertionError ~'e
                 (stacktrace/print-stack-trace ~'e)
                 (throw ~'e) ))))
	([x message]
     (when *assert*
       `(try
          (when-not ~x
            (throw (new AssertionError (str "Assert failed: " ~message "\n" (pr-str '~x)))))
          true
          (catch AssertionError ~'e
                 (stacktrace/print-stack-trace ~'e)
                 (throw ~'e) )))))

(defmacro assert-validator
	[& body]
	(if *assert*
  	`(fn [~'x]
	     (and ~@body)
			 true)
		`(constantly true) ))


;; for debugging dbg macro internally
(defmacro dbg_ [x & more]
  `(let [[return# more#] [~x (vector ~@more)]]
     (println ">>> dbg_:" '~x "=>" return# "<<<")
     return#))

