(ns vita.poker.all-in-test
  (:require [vita.poker.all-in :as allin]
            [vita.poker.calc :as calc :reload true]
            [vita.npc.core :as npc :reload true]
            [philos.debug :refer :all]))

;; (def case-1
;;   {:players {:seat0 [100 :all-in [:spade-2 :club-3]]
;;              :seat1 [200 :fold   [:heart-3 :diamond-K]]
;;              :seat2 [300 nil     [:club-5 :club-2]]
;;              :seat3 [300 nil     [:diamond-5 :heart-3]]
;;              :seat4 [300 nil     [:spade-9 :spade-A]]}
;;    :community-cards [:spade-5 :club-4 :heart-8 :heart-A :heart-6]})

(def players* (atom []))
(def pots*    (atom []))

(defn test-case
  ([case game-name]
   (test-case case game-name :high))
  ([case game-name mode]
   (let [nc (npc/npc-context game-name)]
     (npc/community-cards> nc (get case :community-cards))
     (doseq [[seat-kw data] (get case :players)]
       (let [seat-num (allin/seat-kw->num* seat-kw)]
         (npc/add-player nc seat-num)
         (npc/personal-cards> nc seat-num (get data 2))
         (npc/score> nc seat-num  (calc/calc-cards game-name (npc/personal-cards< nc seat-num)
                                                            (npc/community-cards< nc) ))))

     (reset! players* [])
     (reset! pots* [])  

     (let [players (get case :players)
           room (atom {:seat-bets (into {} (map (fn [[k v]] [k (get v 0)])
                                                players) )})
           allin-seats (keep (fn [[k v]] (if (= (get v 1) :all-in) k))
                             players)
           fold-seats  (keep (fn [[k v]] (if (= (get v 1) :fold) k))
                             players)]
       (doseq [seat-num (sort (map first (get @nc :players)))]
         (let [seat-kw (allin/seat-num->kw* seat-num)
               score   (npc/score< nc seat-num)
               category (get-in score [mode :category])
               ranks    (get-in score [mode :ranks])
               cards    (get-in score [mode :cards])]
           (swap! players* conj [seat-kw category ranks cards]) ))
 
       (let [pots (allin/calc-pots room allin-seats fold-seats)
             overage (:overage pots)
             awards (:awards pots)]
         (doseq [[award seats] awards]
           (let [seats' (sort (map allin/seat-kw->num* seats))
                 seat-nums (npc/recalc-winner nc seats' mode)
                 seat-kws (map allin/seat-num->kw* (sort seat-nums))]
             (swap! pots* conj [mode award (map allin/seat-num->kw* seats') seat-kws]) ))
         {:players @players* :overage overage :pots @pots*} )))))



