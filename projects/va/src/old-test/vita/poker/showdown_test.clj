(ns vita.poker.showdown-test
  (:require  [vita.poker.room-api :as ra]
             [vita.poker.holdem.holdem-room :as hr]
             [vita.poker.holdem.room_exec :as re]
             [sonaclo.shared.util :as util]
             [vita.npc.core :as npc-core]
             [vita.poker.holdem.showdown :as sd]
             [vita.poker.all-in-test-case :as at]
             )
  )

(def room* (atom {}))
(def room-list (atom {}))

(def seat->token {:seat0 :token0 :seat1 :token1 :seat2 :token2 :seat3 :token3 :seat4 :token4})
(def token->seat (clojure.set/map-invert seat->token))






(defn add-in-plyaer-list2
  [room token]
  (let [seat-user   (:seat-user room)
        seat        (first (util/find-key-by-value seat-user token))
        seat-index  (hr/seatkey->seatnum seat)]
    (update-in room [:in-player-list] assoc  seat-index token) ))




(defn init-room
  [game-name]
  (reset! room* {})
  (hr/erase-room-list game-name)
  (swap! room* #(-> %
                    (assoc :room-id :room-id :game-name game-name)
                    (assoc :all-in-player-list (vec (repeat 5 :#not-seat#)) )
                    (assoc :gg-player-list #{})
                    (assoc :in-player-list (vec (repeat 5 :#not-seat#)))
                    (assoc :npc-context (npc-core/get-game-npc-context game-name))
                    ))
  )


(defn setting-user
  [test-case]

  (let [players (:players test-case)]

    (doseq [[seat [money state]] players]
      (swap! room* #(-> %
                        (update-in  [:seat-user] (fn [a] (assoc a seat (seat seat->token))))
                        (update-in  [:seat-bets] (fn [a] (assoc a seat money)))
                        (add-in-plyaer-list2 (seat seat->token))
                        ))

      (if (= state :all-in)
        (swap! room* hr/add-all-in-plyaer-list2  (seat seat->token)))
      (if (= state :fold)
        (swap! room* hr/add-gg-list (seat seat->token)))

      )) )

(defn re-setting-user
  "setting-user()에서 seat-user에 있는 token 유저를 npc-player에 등록한다.
  실제 게임에 fold all-in한 유저를 제외한 사람만 이 함수에서 in-player-list에 등록한다."
  [test-case]
  (swap! room* assoc :in-player-list (vec (repeat 5 :#not-seat#)))
  (let [players (:players test-case)]
    (doseq [[seat [_ state]] players]
      (if (nil? state)
        (swap! room* add-in-plyaer-list2 (seat seat->token))))) )

(defn register-to-nc-player
  []
  (swap! room* hr/init-winner )
  )


(defn hand-card
  [test-case]
  ;:seat0 [20000 nil [:heart-7 :diamond-K]]
  ;:seat1 [20000 :fold [:heart-5 :diamond-10]]
  ;:seat2 [200 :all-in [:spade-K :diamond-Q]]
  ;:seat3 [20000 nil [:diamond-8 :heart-9]]
  ;:seat4 [20000 nil [:diamond-9 :diamond-3]]}
  (let [o-cards (sort (:players test-case))
        b []
        users-cards        (reverse  (flatten (for [[seat [money state card]] o-cards]
                                                (conj b card))))
        comm-card (:community-cards test-case)
        deck      (flatten (conj comm-card users-cards))
        nc (:npc-context @room*)]
    (reset! npc-core/custem-deck? true)
    (npc-core/set-deck nc deck)
    (npc-core/request-cards nc )
    ))

(defn start-test
  [test-case game-name]
  (init-room game-name)
  (setting-user test-case)
  (register-to-nc-player)
  (re-setting-user test-case)
  (hand-card test-case)

  (ra/add-room-to-room-list  room* :room-id (hr/room-list game-name))
  (let [result (sd/adjument-account :room-id game-name )]
    (println "- - - - - -")
    (println "winner -> " (:winner result))
    (println "overage-> " (:overage result))
    ))

;(start-test at/case-1 :texas)
;(start-test at/case-2 :texas)
;(start-test at/case-3)
;(start-test at/case-4)
;(start-test at/case-5)
;(start-test at/case-6)
;(start-test at/case-7)
;(start-test at/case-8)
;(start-test at/case-9)
;(start-test at/case-10)
;(start-test at/case-11)
;(start-test at/case-12)
;(start-test at/case-13)
;(start-test at/case-14)
;(start-test at/case-15)
;(start-test at/case-16)
;(start-test at/case-17)
;(start-test at/case-18)
;(start-test at/case-19)
;(start-test at/case-20)

;(start-test at/case-21 :omaha)
;(start-test at/case-22 :omaha)
;(start-test at/case-23 :omaha)
;(start-test at/case-24 :omaha)
;(start-test at/case-25 :omaha)
;(start-test at/case-26 :omaha)
;(start-test at/case-27 :omaha)
;(start-test at/case-28 :omaha)
;(start-test at/case-29 :omaha)
;(start-test at/case-30 :omaha)
;(start-test at/case-31 :omaha)
;(start-test at/case-32 :omaha)
;(start-test at/case-33 :omaha)
;(start-test at/case-34 :omaha)
;(start-test at/case-35 :omaha)
;(start-test at/case-36 :omaha)
;(start-test at/case-37 :omaha)
;(start-test at/case-38 :omaha)
;(start-test at/case-39 :omaha)
;(start-test at/case-40 :omaha)

;(start-test at/case-41 :omahahl)
;(start-test at/case-42 :omahahl)
;(start-test at/case-43 :omahahl)
;(start-test at/case-44 :omahahl)
;(start-test at/case-45 :omahahl)
;(start-test at/case-46 :omahahl)
;(start-test at/case-47 :omahahl)
;(start-test at/case-48 :omahahl)
;(start-test at/case-49 :omahahl)
;(start-test at/case-50 :omahahl)
;(start-test at/case-51 :omahahl)
;(start-test at/case-54 :omahahl)
;(start-test at/case-55 :omahahl)
;(start-test at/case-57 :omahahl)
;(start-test at/case-58 :omahahl)
;(start-test at/case-60 :omahahl)
