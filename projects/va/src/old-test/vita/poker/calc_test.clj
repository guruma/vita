(ns vita.poker.calc-test
  (:require
    [philos.debug :refer :all]
    [vita.poker.calc :refer :all :reload true]))



;;;; -----------------------
;;;; calc test routines

(defn test-texas []
  (dbg (calc-cards :texas [:club-2 :heart-3] [:spade-A :spade-K :spade-Q :spade-J :spade-10]))
  ; => {:category :royal-straight-flush, :ranks [:A :K :Q :J :10],
  ;     :cards [:spade-A :spade-K :spade-Q :spade-J :spade-10]}

  (dbg (calc-cards :texas [:club-2 :heart-3] [:spade-K :spade-Q :spade-J :spade-10 :spade-9]))
  ; => {:category :straight-flush, :ranks [:K :Q :J :10 :9],
  ;     :cards [:spade-K :spade-Q :spade-J :spade-10 :spade-9]}

  (dbg (calc-cards :texas [:heart-A :heart-K] [:heart-4 :diamond-6 :heart-10 :heart-8 :heart-6]))
  ; => {:category :flush, :ranks [:A :K :10 :8 :6],
  ;     :cards [:heart-A :heart-K :heart-10 :heart-8 :heart-6]}

  (dbg (calc-cards :texas [:heart-8 :heart-7] [:spade-5 :club-4 :spade-3 :heart-2 :heart-A]))
  ; => {:category :straight, :ranks [:5 :4 :3 :2 :A],
  ;     :cards [:spade-5 :club-4 :spade-3 :heart-2 :heart-A]}

  (dbg (calc-cards :texas [:heart-5 :heart-6] [:spade-7 :club-7 :diamond-7 :heart-7]))
  ; => {:category :four-of-a-kind, :ranks [:7 :7 :7 :7 :6],
  ;     :cards [:spade-7 :club-7 :diamond-7 :heart-7 :heart-6]}

  (dbg (calc-cards :texas [:club-5 :heart-5] [:spade-7 :club-7 :diamond-7 :heart-3]))
  ; => {:category :full-house, :ranks [:7 :7 :7 :5 :5],
  ;     :cards [:spade-7 :club-7 :diamond-7 :club-5 :heart-5]}

  (dbg (calc-cards :texas [:heart-9 :heart-5] [:spade-7 :club-7 :diamond-7 :heart-3]))
  ; => {:category :three-of-a-kind, :ranks [:7 :7 :7 :9 :5],
  ;     :cards [:spade-7 :club-7 :diamond-7 :heart-9 :heart-5]}

  (dbg (calc-cards :texas [:heart-9 :heart-5] [:spade-5 :club-7 :diamond-7 :heart-3]))
  ; => {:category :two-pair, :ranks [:7 :7 :5 :5 :9],
  ;     :cards [:club-7 :diamond-7 :heart-5 :spade-5 :heart-9]}

  (dbg (calc-cards :texas [:heart-9 :heart-5] [:spade-8 :club-7 :diamond-7 :heart-3 :heart-A]))
  ; => {:category :one-pair, :ranks [:7 :7 :A :9 :8],
  ;     :cards [:club-7 :diamond-7 :heart-A :heart-9 :spade-8]}

  (dbg (calc-cards :texas [:club-6 :heart-9] [:spade-8 :club-7 :heart-3 :heart-A :club-K]))
  ; => {:category :high-card, :ranks [:A :K :9 :8 :7],
  ;     :cards [:heart-A :club-K :heart-9 :spade-8 :club-7]}

  (dbg (calc-cards :texas [:club-6 :heart-9]))
  ; => {:category :high-card, :ranks [:9 :6], :cards [:heart-9 :club-6]}
  )

(defn test-omaha []
  (dbg (calc-cards :omaha [:club-2 :heart-3 :spade-A :spade-K]
                   [:spade-7 :diamond-8 :spade-Q :spade-J :spade-10]))
  ; => {:category :royal-straight-flush, :ranks [:A :K :Q :J :10],
  ;     :cards [:spade-A :spade-K :spade-Q :spade-J :spade-10]}

  (dbg (calc-cards :omaha [:club-2 :heart-3 :spade-7 :spade-8]
                   [:spade-K :spade-Q :spade-J :spade-10 :spade-9]))
  ; => {:category :straight-flush, :ranks [:J :10 :9 :8 :7],
  ;     :cards [:spade-J :spade-10 :spade-9 :spade-8 :spade-7]}

  (dbg (calc-cards :omaha [:heart-A :heart-K :spade-7 :diamond-8]
                   [:heart-4 :diamond-6 :heart-10 :heart-8 :heart-6]))
  ; => {:category :flush, :ranks [:A :K :10 :8 :6],
  ;     :cards [:heart-A :heart-K :heart-10 :heart-8 :heart-6]}

  (dbg (calc-cards :omaha [:heart-8 :heart-7 :spade-K :diamond-A]
                   [:spade-5 :club-4 :spade-3 :heart-2 :heart-6]))
  ; => {:category :straight, :ranks [:8 :7 :6 :5 :4],
  ;     :cards [:heart-8 :heart-7 :heart-6 :spade-5 :club-4]}

  (dbg (calc-cards :omaha [:heart-5 :heart-6 :spade-7 :diamond-7]
                   [:heart-7 :club-7 :diamond-K :heart-J]))
  ; => {:category :four-of-a-kind, :ranks [:7 :7 :7 :7 :K],
  ;     :cards [:spade-7 :diamond-7 :heart-7 :club-7 :diamond-K]}

  (dbg (calc-cards :omaha [:club-5 :heart-5 :spade-K :diamond-A]
                   [:spade-7 :club-7 :diamond-7 :heart-3]))
  ; => {:category :full-house, :ranks [:7 :7 :7 :5 :5],
  ;     :cards [:spade-7 :club-7 :diamond-7 :club-5 :heart-5]}

  (dbg (calc-cards :omaha [:heart-9 :heart-5 :spade-K :diamond-A]
                   [:spade-7 :club-7 :diamond-7 :heart-3]))
  ; => {:category :three-of-a-kind, :ranks [:7 :7 :7 :A :K],
  ;     :cards [:spade-7 :club-7 :diamond-7 :diamond-A :spade-K]}

  (dbg (calc-cards :omaha [:heart-9 :heart-5 :spade-K :diamond-A]
                   [:spade-5 :club-7 :diamond-7 :heart-3]))
  ; => {:category :two-pair, :ranks [:7 :7 :5 :5 :A],
  ;     :cards [:club-7 :diamond-7 :heart-5 :spade-5 :diamond-A]}

  (dbg (calc-cards :omaha [:heart-9 :heart-5 :spade-K :diamond-A]
                   [:spade-8 :club-7 :diamond-J :heart-3 :heart-A]))
  ; => {:category :one-pair, :ranks [:A :A :K :J :8],
  ;     :cards [:diamond-A :heart-A :spade-K :diamond-J :spade-8]}

  (dbg (calc-cards :omaha [:club-6 :heart-9 :spade-K :diamond-Q]
                   [:spade-8 :club-7 :heart-3 :heart-A :club-2]))
  ; => {:category :high-card, :ranks [:A :K :Q :8 :7],
  ;     :cards [:heart-A :spade-K :diamond-Q :spade-8 :club-7]}

  (dbg (calc-cards :omaha [:club-6 :heart-9 :spade-K :diamond-A]))
  ; => {:category :high-card, :ranks [:A :K], :cards [:diamond-A :spade-K]}
  )

(defn test-omaha-hi-lo []
  (dbg (calc-cards :omaha-hi-lo [:club-2 :heart-3 :spade-A :spade-K]
                   [:spade-7 :diamond-8 :spade-Q :spade-J :spade-10]))
  ; => {:high {:category :royal-straight-flush, :ranks [:A :K :Q :J :10],
  ;            :cards [:spade-A :spade-K :spade-Q :spade-J :spade-10]},
  ;     :low nil}

  (dbg (calc-cards :omaha-hi-lo [:club-2 :heart-3 :spade-7 :spade-6]
                   [:spade-5 :spade-3 :spade-4 :spade-10 :spade-9]))
  ; => {:high {:category :straight-flush, :ranks [:7 :6 :5 :4 :3],
  ;            :cards [:spade-7 :spade-6 :spade-5 :spade-4 :spade-3]},
  ;     :low {:category :low-card, :ranks [:6 :5 :4 :3 :2],
  ;           :cards [:spade-6 :spade-5 :spade-4 :spade-3 :club-2]}}

  (dbg (calc-cards :omaha-hi-lo [:heart-A :heart-K :spade-7 :diamond-8]
                   [:heart-4 :diamond-6 :heart-10 :heart-8 :heart-6]))
  ; => {:high {:category :flush, :ranks [:A :K :10 :8 :6],
  ;            :cards [:heart-A :heart-K :heart-10 :heart-8 :heart-6]},
  ;     :low {:category :low-card, :ranks [:8 :7 :6 :4 :A],
  ;           :cards [:heart-8 :spade-7 :diamond-6 :heart-4 :heart-A]}}

  (dbg (calc-cards :omaha-hi-lo [:heart-8 :heart-7 :spade-K :diamond-A]
                   [:spade-5 :club-4 :spade-3 :heart-2 :heart-6]))
  ; => {:high {:category :straight, :ranks [:8 :7 :6 :5 :4],
  ;            :cards [:heart-8 :heart-7 :heart-6 :spade-5 :club-4]},
  ;     :low {:category :low-card, :ranks [:7 :4 :3 :2 :A],
  ;            :cards [:heart-7 :club-4 :spade-3 :heart-2 :diamond-A]}}

  (dbg (calc-cards :omaha-hi-lo [:heart-5 :heart-6 :spade-7 :diamond-7]
                   [:heart-7 :club-7 :diamond-4 :heart-3]))
  ; => {:high {:category :four-of-a-kind, :ranks [:7 :7 :7 :7 :4],
  ;            :cards [:spade-7 :diamond-7 :heart-7 :club-7 :diamond-4]},
  ;     :low {:category :low-card, :ranks [:7 :6 :5 :4 :3],
  ;           :cards [:heart-7 :heart-6 :heart-5 :diamond-4 :heart-3]}}

  (dbg (calc-cards :omaha-hi-lo [:spade-7 :heart-5 :spade-2 :diamond-A]
                   [:club-5 :club-7 :diamond-7 :heart-3]))
  ; => {:high {:category :full-house, :ranks [:7 :7 :7 :5 :5],
  ;            :cards [:spade-7 :club-7 :diamond-7 :heart-5 :club-5]},
  ;     :low {:category :low-card, :ranks [:7 :5 :3 :2 :A],
  ;           :cards [:club-7 :club-5 :heart-3 :spade-2 :diamond-A]}}

  (dbg (calc-cards :omaha-hi-lo [:heart-7 :heart-5 :spade-K :diamond-A]
                   [:spade-7 :club-7 :diamond-6 :heart-3]))
  ; => {:high {:category :three-of-a-kind, :ranks [:7 :7 :7 :A :6], :cards [:heart-7 :spade-7 :club-7 :diamond-A :diamond-6]}, :low {:category :low-card, :ranks [:7 :6 :5 :3 :A], :cards [:spade-7 :diamond-6 :heart-5 :heart-3 :diamond-A]}}

  (dbg (calc-cards :omaha-hi-lo [:heart-6 :heart-5 :spade-K :diamond-A]
                   [:spade-5 :club-7 :diamond-6 :heart-3]))
  ; => {:high {:category :two-pair, :ranks [:6 :6 :5 :5 :7],
  ;            :cards [:heart-6 :diamond-6 :heart-5 :spade-5 :club-7]},
  ;     :low {:category :low-card, :ranks [:7 :6 :5 :3 :A],
  ;           :cards [:club-7 :heart-6 :spade-5 :heart-3 :diamond-A]}}

  (dbg (calc-cards :omaha-hi-lo [:heart-8 :heart-5 :spade-4 :diamond-A]
                   [:spade-8 :club-7 :diamond-2 :heart-3 :heart-K]))
  ; => {:high {:category :one-pair, :ranks [:8 :8 :A :K :7],
  ;            :cards [:heart-8 :spade-8 :diamond-A :heart-K :club-7]},
  ;     :low {:category :low-card, :ranks [:7 :4 :3 :2 :A],
  ;           :cards [:club-7 :spade-4 :heart-3 :diamond-2 :diamond-A]}}

  (dbg (calc-cards :omaha-hi-lo [:club-6 :heart-7 :spade-K :diamond-Q]
                   [:spade-3 :club-J :heart-4 :heart-A :club-2]))
  ; => {:high {:category :high-card, :ranks [:A :K :Q :J :4],
  ;            :cards [:heart-A :spade-K :diamond-Q :club-J :heart-4]},
  ;     :low {:category :low-card, :ranks [:7 :6 :3 :2 :A],
  ;           :cards [:heart-7 :club-6 :spade-3 :club-2 :heart-A]}}

  (dbg (calc-cards :omaha-hi-lo [:club-6 :heart-9 :spade-K :diamond-A]))
  ; => {:high {:category :high-card, :ranks [:A :K], :cards [:diamond-A :spade-K]},
  ;     :low nil}
  )


;;;; ------------------------
;;;; simulate routines

(def cards*
  [:club-A    :club-2    :club-3     :club-4    :club-5    :club-6    :club-7
   :club-8    :club-9    :club-10    :club-J    :club-Q    :club-K
   :heart-A   :heart-2   :heart-3    :heart-4   :heart-5   :heart-6   :heart-7
   :heart-8   :heart-9   :heart-10   :heart-J   :heart-Q   :heart-K
   :diamond-A :diamond-2 :diamond-3  :diamond-4 :diamond-5 :diamond-6 :diamond-7
   :diamond-8 :diamond-9 :diamond-10 :diamond-J :diamond-Q :diamond-K
   :spade-A   :spade-2   :spade-3    :spade-4   :spade-5   :spade-6   :spade-7
   :spade-8   :spade-9   :spade-10   :spade-J   :spade-Q   :spade-K])

(defn new-scores
  []
  (atom {:royal-straight-flush 0
         :straight-flush 0
         :four-of-a-kind 0
         :full-house 0
         :flush      0
         :straight 0
         :three-of-a-kind 0
         :two-pair 0
         :one-pair 0
         :high-card 0
         :low       0} ))

(defn simulate-texas []
  (let [n      1e3
        scores (new-scores)]
    (dotimes [i n]
      (let [cards (take 7 (shuffle cards*))
            personal-cards (take 2 cards)
            community-cards (take 5 (drop 2 cards))
            score (calc-cards :texas personal-cards community-cards)]
        (swap! scores update-in [(get score 0)] inc) ))
    (printf ":high-card            = %f\n" (/ (:high-card @scores) (double n)))
    (printf ":one-pair             = %f\n" (/ (:one-pair @scores) (double n)))
    (printf ":two-pair             = %f\n" (/ (:two-pair @scores) (double n)))
    (printf ":three-of-a-kind      = %f\n" (/ (:three-of-a-kind @scores) (double n)))
    (printf ":straight             = %f\n" (/ (:straight @scores) (double n)))
    (printf ":flush                = %f\n" (/ (:flush @scores) (double n)))
    (printf ":full-house           = %f\n" (/ (:full-house @scores) (double n)))
    (printf ":four-of-a-kind       = %f\n" (/ (:four-of-a-kind @scores) (double n)))
    (printf ":straight-flush       = %f\n" (/ (:straight-flush @scores) (double n)))
    (printf ":royal-straight-flush = %f\n\n" (/ (:royal-straight-flush @scores) (double n))) ))

;; (time (simulate-texas))
;; => :high-card            = 0.168000
;;    :one-pair             = 0.437000
;;    :two-pair             = 0.241000
;;    :three-of-a-kind      = 0.050000
;;    :straight             = 0.039000
;;    :flush                = 0.025000
;;    :full-house           = 0.032000
;;    :four-of-a-kind       = 0.004000
;;    :straight-flush       = 0.004000
;;    :royal-straight-flush = 0.000000
;;
;;    "Elapsed time: 142.478651 msecs"


(defn simulate-omaha []
  (let [n      1e3
        scores (new-scores)]
    (dotimes [i n]
      (let [cards (take 9 (shuffle cards*))
            personal-cards (take 4 cards)
            community-cards (take 5 (drop 4 cards))
            score (calc-cards :omaha personal-cards community-cards)]
        (swap! scores update-in [(get score 0)] inc) ))

    (printf ":high-card            = %f\n" (/ (:high-card @scores) (double n)))
    (printf ":one-pair             = %f\n" (/ (:one-pair @scores) (double n)))
    (printf ":two-pair             = %f\n" (/ (:two-pair @scores) (double n)))
    (printf ":three-of-a-kind      = %f\n" (/ (:three-of-a-kind @scores) (double n)))
    (printf ":straight             = %f\n" (/ (:straight @scores) (double n)))
    (printf ":flush                = %f\n" (/ (:flush @scores) (double n)))
    (printf ":full-house           = %f\n" (/ (:full-house @scores) (double n)))
    (printf ":four-of-a-kind       = %f\n" (/ (:four-of-a-kind @scores) (double n)))
    (printf ":straight-flush       = %f\n" (/ (:straight-flush @scores) (double n)))
    (printf ":royal-straight-flush = %f\n\n" (/ (:royal-straight-flush @scores) (double n))) ))

;; (time (simulate-omaha))
;; => :high-card            = 0.033000
;;    :one-pair             = 0.277000
;;    :two-pair             = 0.379000
;;    :three-of-a-kind      = 0.084000
;;    :straight             = 0.115000
;;    :flush                = 0.063000
;;    :full-house           = 0.045000
;;    :four-of-a-kind       = 0.003000
;;    :straight-flush       = 0.001000
;;    :royal-straight-flush = 0.000000
;;
;;   "Elapsed time: k4747.864587 msecs"


(defn simulate-omaha-hi-lo []
  (let [n      1e3
        scores (new-scores)]
    (dotimes [i n]
      (let [cards (take 9 (shuffle cards*))
            personal-cards (take 4 cards)
            community-cards (take 5 (drop 4 cards))
            score (calc-cards :omaha-hi-lo personal-cards community-cards)]
        (swap! scores update-in [(get-in score [:high 0])] inc)
        (if (seq (get score :low))
          (swap! scores update-in [:low] inc) )))

    (printf ":low                  = %f\n" (/ (:low @scores) (double n)))
    (printf ":high-card            = %f\n" (/ (:high-card @scores) (double n)))
    (printf ":one-pair             = %f\n" (/ (:one-pair @scores) (double n)))
    (printf ":two-pair             = %f\n" (/ (:two-pair @scores) (double n)))
    (printf ":three-of-a-kind      = %f\n" (/ (:three-of-a-kind @scores) (double n)))
    (printf ":straight             = %f\n" (/ (:straight @scores) (double n)))
    (printf ":flush                = %f\n" (/ (:flush @scores) (double n)))
    (printf ":full-house           = %f\n" (/ (:full-house @scores) (double n)))
    (printf ":four-of-a-kind       = %f\n" (/ (:four-of-a-kind @scores) (double n)))
    (printf ":straight-flush       = %f\n" (/ (:straight-flush @scores) (double n)))
    (printf ":royal-straight-flush = %f\n\n" (/ (:royal-straight-flush @scores) (double n))) ))

;; (time (simulate-omaha-hi-lo))
;; => :low                  = 0.368000
;;    :high-card            = 0.030000
;;    :one-pair             = 0.279000
;;    :two-pair             = 0.369000
;;    :three-of-a-kind      = 0.081000
;;    :straight             = 0.102000
;;    :flush                = 0.062000
;;    :full-house           = 0.074000
;;    :four-of-a-kind       = 0.002000
;;    :straight-flush       = 0.000000
;;    :royal-straight-flush = 0.001000
;;
;;    "Elapsed time: 6778.471195 msecs"


(defn run-tests []
  (test-texas)
  (test-omaha)
  (test-omaha-hi-lo)
  (time (simulate-texas))
  (time (simulate-omaha))
  (time (simulate-omaha-hi-lo)))

;(run-tests)
