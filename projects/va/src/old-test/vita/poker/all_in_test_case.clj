(ns vita.poker.all-in-test-case
  (:require [vita.poker.all-in-test :as all-in :reload true]
            [philos.debug :refer :all]))


; ==== Texas holdem =====================
(def case-1
  {:players {:seat0 [20000 nil [:heart-7 :diamond-K]]
             :seat2 [200 :all-in [:spade-K :diamond-Q]]
             :seat1 [20000 :fold [:heart-5 :diamond-10]]
             :seat3 [20000 nil [:diamond-8 :heart-9]]
             :seat4 [20000 nil [:diamond-9 :diamond-3]]}
   :community-cards [:club-J :spade-10 :heart-6 :diamond-J :club-9]
   :game-name :texas
   }
  )
(def case-2
  {:players {:seat0 [1200 :fold [:heart-4 :diamond-3]]
             :seat1 [800 :all-in [:heart-6 :diamond-2]]
             :seat2 [500 :all-in [:spade-7 :diamond-8]]
             :seat3 [100 :all-in [:diamond-Q :heart-Q]]
             :seat4 [1400 nil [:diamond-9 :diamond-3]]}
   :community-cards [:club-5 :spade-5 :heart-5 :diamond-J :club-J]
   :game-name :texas}
  )
(def case-3
  {:players {:seat0 [1200 nil [:heart-3 :diamond-4]]
             :seat1 [800 :all-in [:heart-5 :diamond-6]]}
   :community-cards [:club-2 :spade-3 :heart-4 :diamond-7 :club-8]}
  )
(def case-4
  {:players {:seat0 [1200 nil [:heart-5 :diamond-6]]
             :seat1 [800 :all-in [:heart-3 :diamond-4]]}
   :community-cards [:club-2 :spade-3 :heart-4 :diamond-7 :club-8]}
  )
(def case-5
  {:players {:seat0 [1200 nil [:heart-5 :diamond-6]]
             :seat1 [800 :all-in [:club-5 :spade-6]]}
   :community-cards [:club-2 :spade-3 :heart-4 :diamond-7 :club-8]}
  )
(def case-6
  {:players {:seat0 [1000 nil [:heart-9 :diamond-Q]]
             :seat1 [800 :all-in [:heart-10 :diamond-K]]
             :seat2 [400 :all-in [:heart-5 :diamond-6]]}
   :community-cards [:club-2 :spade-3 :heart-4 :diamond-7 :club-8]}
  )
(def case-7
  {:players {:seat0 [1000 nil [:heart-9 :diamond-Q]]
             :seat1 [800 :all-in [:heart-5 :diamond-6]]
             :seat2 [400 :all-in [:heart-10 :diamond-K]]}
   :community-cards [:club-2 :spade-3 :heart-4 :diamond-7 :club-8]}
  )
(def case-8
  {:players {:seat0 [1200 nil [:heart-5 :diamond-6]]
             :seat1 [800 :all-in [:heart-9 :diamond-10]]
             :seat2 [400 :all-in [:heart-7 :diamond-8]]}
   :community-cards [:club-2 :spade-3 :heart-4 :diamond-7 :club-8]}
  )
(def case-9
  {:players {:seat0 [1200 nil [:diamond-5 :diamond-6]]
             :seat1 [800 :all-in [:spade-5 :heart-6]]
             :seat2 [400 :all-in [:heart-5 :club-6]]}
   :community-cards [:club-2 :spade-3 :heart-4 :diamond-7 :club-8]}
  )
(def case-10
  {:players {:seat0 [1600 nil [:heart-9 :diamond-Q]]
             :seat1 [800 :all-in [:heart-10 :diamond-10]]
             :seat2 [400 :all-in [:spade-J :heart-J]]
             :seat3 [1400 :all-in [:heart-Q :diamond-K]]}
   :community-cards [:club-J :spade-10 :heart-6 :diamond-J :club-9]}

  )
(def case-11
  {:players {:seat0 [1600 nil [:heart-9 :diamond-Q]]
             :seat1 [800 :all-in [:heart-10 :diamond-10]]
             :seat2 [400 :all-in [:heart-Q :diamond-K]]
             :seat3 [1400 :all-in [:spade-J :heart-J]]}
   :community-cards [:club-J :spade-10 :heart-6 :diamond-J :club-9]}
  )
(def case-12
  {:players {:seat0 [1600 nil [:spade-J :heart-J]]
             :seat1 [800 :all-in [:heart-10 :diamond-10]]
             :seat2 [400 :all-in [:heart-Q :diamond-K]]
             :seat3 [1400 :all-in [:heart-9 :diamond-Q]]}
   :community-cards [:club-J :spade-10 :heart-6 :diamond-J :club-9]}
  )
(def case-13
  {:players {:seat0 [1600 nil [:spade-2 :heart-3]]
             :seat1 [800 :all-in [:heart-2 :diamond-3]]
             :seat2 [400 :all-in [:club-2 :spade-3]]
             :seat3 [1400 :all-in [:diamond-2 :club-3]]}
   :community-cards [:club-J :spade-10 :heart-6 :diamond-J :club-9]}
  )
(def case-14
  {:players {:seat0 [1600 nil [:spade-2 :heart-3]]
             :seat1 [800 :fold [:heart-J :diamond-3]]
             :seat2 [400 :fold [:club-2 :spade-J]]
             :seat3 [1400 :all-in [:diamond-2 :club-3]]}
   :community-cards [:club-J :spade-10 :heart-6 :diamond-J :club-9]}
  )
(def case-15
  {:players {:seat0 [1600 nil [:heart-9 :diamond-Q]]
             :seat1 [800 :all-in [:heart-10 :diamond-10]]
             :seat2 [400 :all-in [:spade-J :heart-J]]
             :seat3 [1400 :all-in [:heart-Q :diamond-K]]
             :seat4 [1600 nil [:heart-8 :diamond-7]]}
   :community-cards [:club-J :spade-10 :heart-6 :diamond-J :club-9]}
  )
(def case-16
  {:players {:seat0 [1600 nil [:heart-K :diamond-K]]
             :seat1 [800 :all-in [:heart-3 :diamond-4]]
             :seat2 [400 :all-in [:spade-A :heart-2]]
             :seat3 [1400 :all-in [:heart-5 :diamond-6]]
             :seat4 [1600 nil [:club-K :spade-K]]}
   :community-cards [:club-J :spade-10 :heart-6 :diamond-J :club-9]}
  )
(def case-17
  {:players {:seat0 [1600 nil [:heart-2 :diamond-3]]
             :seat1 [800 :all-in [:diamond-2 :spade-3]]
             :seat2 [400 :all-in [:spade-2 :club-3]]
             :seat3 [1400 :fold [:heart-5 :diamond-6]]
             :seat4 [1600 nil [:club-2 :heart-3]]}
   :community-cards [:club-J :spade-10 :heart-6 :diamond-J :club-9]}
  )
(def case-18
  {:players {:seat0 [1600 nil [:heart-2 :diamond-3]]
             :seat1 [1600 nil [:diamond-2 :spade-3]]
             :seat2 [1600 nil [:spade-2 :club-3]]
             :seat3 [1600 nil [:heart-5 :diamond-6]]
             :seat4 [1600 nil [:club-2 :heart-3]]}
   :community-cards [:club-J :spade-10 :heart-6 :diamond-J :club-9]}
  )
(def case-19
  {:players {:seat0 [1600000 nil [:heart-2 :diamond-3]]
             :seat1 [1600000 nil [:diamond-2 :spade-3]]
             :seat2 [1600000 nil [:spade-2 :club-3]]
             :seat3 [1600000 nil [:heart-5 :diamond-6]]
             :seat4 [1600000 nil [:club-2 :heart-3]]}
   :community-cards [:club-J :spade-10 :heart-6 :diamond-J :club-9]}
  )
(def case-20
  {:players {:seat0 [1600 :all-in [:heart-8 :diamond-8]]
             :seat1 [800 :all-in [:heart-Q :diamond-Q]]
             :seat2 [400 :all-in [:spade-J :heart-J]]
             :seat3 [1400 :all-in [:spade-Q :club-Q]]
             :seat4 [1800 :all-in [:club-8 :spade-8]]}
   :community-cards [:club-J :spade-10 :heart-6 :diamond-J :club-9]}
  )

; ==== Omaha =====================
(def case-21
  {:players {:seat0 [20000 nil [:heart-7 :diamond-K :heart-4 :heart-5]]
             :seat1 [20000 :fold [:club-5 :diamond-10 :heart-2 :heart-3]]
             :seat2 [200 :all-in [:spade-K :diamond-Q :spade-2 :spade-3]]
             :seat3 [20000 nil [:diamond-8 :heart-9 :diamond-2 :diamond-4]]
             :seat4 [20000 nil [:diamond-9 :spade-8 :club-2 :club-3]]}
   :community-cards [:club-J :spade-10 :heart-6 :diamond-J :club-9]}
  )
(def case-22
  {:players {:seat0 [1200 :fold [:heart-4 :club-3 :club-4 :club-5]]
             :seat1 [800 :all-in [:heart-6 :club-J :spade-2 :spade-3]]
             :seat2 [500 :all-in [:spade-6 :spade-J :heart-2 :heart-3]]
             :seat3 [100 :all-in [:diamond-Q :heart-Q :diamond-2 :diamond-3]]
             :seat4 [1400 nil [:diamond-6 :heart-J :club-2 :heart-4]]}
   :community-cards [:club-5 :spade-5 :heart-5 :diamond-J :club-K]}
  )
(def case-23
  {:players {:seat0 [1200 nil [:heart-3 :diamond-4 :club-K :spade-Q]]
             :seat1 [800 :all-in [:heart-5 :diamond-6 :club-9 :spade-10]]}
   :community-cards [:club-2 :spade-3 :heart-4 :diamond-7 :club-8]}
  )
(def case-24
  {:players {:seat0 [1200 nil [:heart-5 :diamond-6 :club-9 :spade-10]]
             :seat1 [800 :all-in [:heart-3 :diamond-4 :club-K :spade-Q]]}
   :community-cards [:club-2 :spade-3 :heart-4 :diamond-7 :club-8]}
  )
(def case-25
  {:players {:seat0 [1200 nil [:heart-5 :diamond-6 :club-9 :spade-10]]
             :seat1 [800 :all-in [:spade-5 :spade-6 :spade-9 :heart-10]]}
   :community-cards [:club-2 :spade-3 :heart-4 :diamond-7 :club-8]}
  )
(def case-26
  {:players {:seat0 [1000 nil [:heart-2 :diamond-4 :club-K :spade-Q]]
             :seat1 [800 :all-in [:heart-3 :diamond-4 :club-K :spade-Q]]
             :seat2 [400 :all-in [:heart-5 :diamond-6 :club-9 :spade-10]]}
   :community-cards [:club-2 :spade-3 :heart-4 :diamond-7 :club-8]}
  )
(def case-27
  {:players {:seat0 [1000 nil [:heart-2 :diamond-4 :club-K :spade-Q]]
             :seat1 [800 :all-in [:heart-5 :diamond-6 :club-9 :spade-10]]
             :seat2 [400 :all-in [:heart-3 :diamond-4 :club-K :spade-Q]]}
   :community-cards [:club-2 :spade-3 :heart-4 :diamond-7 :club-8]}
  )
(def case-28
  {:players {:seat0 [1200 nil [:heart-5 :diamond-6 :club-9 :spade-10]]
             :seat1 [800 :all-in [:heart-2 :diamond-4 :club-K :spade-Q]]
             :seat2 [400 :all-in [:heart-3 :diamond-4 :club-K :spade-Q]]}
   :community-cards [:club-2 :spade-3 :heart-4 :diamond-7 :club-8]}
  )
(def case-29
  {:players {:seat0 [1200 nil [:diamond-5 :diamond-6 :club-9 :spade-10]]
             :seat1 [800 :all-in [:spade-5 :heart-6 :diamond-9 :diamond-10]]
             :seat2 [400 :all-in [:heart-5 :club-6 :spade-9 :heart-10]]}
   :community-cards [:club-2 :spade-3 :heart-4 :diamond-7 :club-8]}
  )
(def case-30
  {:players {:seat0 [1600 nil [:heart-9 :diamond-Q :club-2 :club-3]]
             :seat1 [800 :all-in [:heart-10 :diamond-10 :diamond-2 :diamond-3]]
             :seat2 [400 :all-in [:spade-J :heart-J :spade-2 :spade-3]]
             :seat3 [1400 :all-in [:heart-Q :diamond-K :heart-2 :heart-3]]}
   :community-cards [:club-J :spade-10 :heart-6 :diamond-J :club-9]}
  )
(def case-31
  {:players {:seat0 [1600 nil [:heart-9 :diamond-Q :club-2 :club-3]]
             :seat1 [800 :all-in [:heart-10 :diamond-10 :diamond-2 :diamond-3]]
             :seat2 [400 :all-in [:heart-Q :diamond-K :heart-2 :heart-3]]
             :seat3 [1400 :all-in [:spade-J :heart-J :spade-2 :spade-3]]}
   :community-cards [:club-J :spade-10 :heart-6 :diamond-J :club-9]}
  )
(def case-32
  {:players {:seat0 [1600 nil [:spade-J :heart-J :spade-2 :spade-3]]
             :seat1 [800 :all-in [:heart-10 :diamond-10 :diamond-2 :diamond-3]]
             :seat2 [400 :all-in [:heart-Q :diamond-K :heart-2 :heart-3]]
             :seat3 [1400 :all-in [:heart-9 :diamond-Q :club-2 :club-3]]}
   :community-cards [:club-J :spade-10 :heart-6 :diamond-J :club-9]}
  )
(def case-33
  {:players {:seat0 [1600 nil [:heart-A :diamond-Q :club-K :spade-3]]
             :seat1 [800 :all-in [:diamond-A :club-Q :spade-K :heart-3]]
             :seat2 [400 :all-in [:club-A :spade-Q :heart-K :diamond-3]]
             :seat3 [1400 :all-in [:spade-A :heart-Q :diamond-K :club-3]]}
   :community-cards [:club-J :spade-10 :heart-6 :diamond-J :club-9]}
  )
(def case-34
  {:players {:seat0 [1600 nil [:spade-J :heart-10 :spade-2 :spade-3]]
             :seat1 [800 :all-in [:heart-7 :diamond-10 :diamond-2 :diamond-3]]
             :seat2 [400 :all-in [:heart-Q :diamond-K :heart-2 :heart-3]]
             :seat3 [1400 :all-in [:heart-J :club-10 :spade-2 :spade-3]]}
   :community-cards [:club-J :spade-10 :heart-6 :diamond-J :club-9]}
  )
(def case-35
  {:players {:seat0 [1600 nil [:heart-9 :diamond-Q :club-2 :club-3]]
             :seat1 [800 :all-in [:heart-10 :diamond-10 :diamond-2 :diamond-3]]
             :seat2 [400 :all-in [:spade-J :heart-J :spade-2 :spade-3]]
             :seat3 [1400 :all-in [:heart-Q :diamond-K :heart-2 :heart-3]]
             :seat4 [1600 :nil [:heart-Q :diamond-8 :heart-2 :heart-3]]}
   :community-cards [:club-J :spade-10 :heart-6 :diamond-J :club-9]}
  )
(def case-36
  {:players {:seat0 [1600 nil [:spade-J :heart-10 :spade-2 :spade-3]]
             :seat1 [800 :all-in [:heart-A :diamond-6 :diamond-2 :diamond-3]]
             :seat2 [400 :all-in [:spade-4 :heart-6 :spade-2 :spade-3]]
             :seat3 [1400 :all-in [:heart-Q :diamond-K :heart-2 :heart-3]]
             :seat4 [1600 :nil [:heart-J :diamond-10 :spade-2 :spade-3]]}
   :community-cards [:club-J :spade-10 :heart-6 :diamond-J :club-9]}
  )
(def case-37
  {:players {:seat0 [1600 nil [:heart-2 :diamond-3 :heart-7 :diamond-K]]
             :seat1 [800 :all-in [:diamond-2 :spade-3 :diamond-7 :spade-K]]
             :seat2 [400 :all-in [:spade-2 :club-3 :spade-7 :club-K]]
             :seat3 [1400 :fold [:heart-5 :diamond-6 :heart-10 :diamond-Q]]
             :seat4 [1600 nil [:club-2 :heart-3 :heart-7 :diamond-K]]}
   :community-cards [:club-J :spade-10 :heart-6 :diamond-J :club-9]}
  )
(def case-38
  {:players {:seat0 [1600 nil [:heart-9 :diamond-Q :club-2 :club-3]]
             :seat1 [1600 :all-in [:heart-10 :diamond-10 :diamond-2 :diamond-3]]
             :seat2 [1600 :all-in [:heart-Q :diamond-K :heart-2 :heart-3]]
             :seat3 [1600 :all-in [:spade-J :heart-J :spade-2 :spade-3]]
             :seat4 [1600 :nil [:heart-Q :diamond-8 :heart-2 :heart-3]]}
   :community-cards [:club-J :spade-10 :heart-6 :diamond-J :club-9]}
  )
(def case-39
  {:players {:seat0 [1600000 nil [:heart-9 :diamond-Q :club-2 :club-3]]
             :seat1 [1600000 :all-in [:heart-10 :diamond-10 :diamond-2 :diamond-3]]
             :seat2 [1600000 :all-in [:heart-Q :diamond-K :heart-2 :heart-3]]
             :seat3 [1600000 :all-in [:spade-J :heart-J :spade-2 :spade-3]]
             :seat4 [1600000 :nil [:heart-Q :diamond-8 :heart-2 :heart-3]]}
   :community-cards [:club-J :spade-10 :heart-6 :diamond-J :club-9]}
  )
(def case-40
  {:players {:seat0 [1600 :all-in [:heart-8 :diamond-K :club-2 :club-3]]
             :seat1 [800 :all-in [:heart-10 :heart-9 :diamond-2 :diamond-3]]
             :seat2 [400 :all-in [:spade-J :heart-J :spade-2 :spade-3]]
             :seat3 [1400 :all-in [:diamond-10 :diamond-9 :diamond-2 :diamond-3]]
             :seat4 [1800 :all-in [:diamond-8 :club-K :heart-2 :heart-3]]}
   :community-cards [:club-J :spade-10 :heart-6 :diamond-J :club-9]}
  )

; ==== Omaha hi-lo =====================
(def case-41
  {:players {:seat0 [20000 nil [:heart-2 :diamond-3 :heart-7 :heart-5]]
             :seat1 [20000 :fold [:diamond-5 :diamond-10 :heart-2 :heart-3]]
             :seat2 [200 :all-in [:spade-J :diamond-Q :spade-2 :spade-9]]
             :seat3 [20000 nil [:diamond-8 :heart-9 :diamond-2 :diamond-4]]
             :seat4 [20000 nil [:diamond-9 :spade-8 :club-2 :club-4]]}
   :community-cards [:club-J :spade-A :heart-6 :diamond-J :club-8]}
  )
(def case-42
  {:players {:seat0 [1200 :all-in [:heart-A :club-K :club-4 :club-9]]
             :seat1 [800 :all-in [:heart-6 :club-J :spade-2 :spade-3]]
             :seat2 [500 :all-in [:spade-6 :spade-J :heart-2 :heart-3]]
             :seat3 [100 :all-in [:diamond-Q :heart-Q :diamond-2 :diamond-3]]
             :seat4 [1400 nil [:diamond-6 :heart-J :club-2 :club-3]]}
   :community-cards [:club-2 :spade-5 :heart-5 :diamond-7 :club-8]}
  )
(def case-43
  {:players {:seat0 [1200 nil [:heart-3 :diamond-A :club-K :spade-J]]
             :seat1 [800 :all-in [:heart-K :diamond-4 :club-9 :spade-10]]}
   :community-cards [:club-J :spade-Q :heart-4 :diamond-7 :club-8]}
  )
(def case-44
  {:players {:seat0 [1200 nil [:heart-5 :diamond-6 :club-9 :spade-10]]
             :seat1 [800 :all-in [:heart-3 :diamond-4 :club-K :spade-Q]]}
   :community-cards [:club-2 :spade-3 :heart-4 :diamond-7 :club-8]}
  )
(def case-45
  {:players {:seat0 [1200 nil [:heart-5 :diamond-6 :club-9 :spade-10]]
             :seat1 [800 :all-in [:spade-5 :spade-6 :spade-9 :heart-10]]}
   :community-cards [:club-2 :spade-3 :heart-4 :diamond-7 :club-8]}
  )
(def case-46
  {:players {:seat0 [1000 nil [:heart-2 :heart-4 :diamond-K :heart-Q]]
             :seat1 [800 :all-in [:heart-3 :heart-5 :club-K :spade-Q]]
             :seat2 [400 :all-in [:heart-A :diamond-4 :club-9 :spade-10]]}
   :community-cards [:club-2 :spade-3 :heart-6 :diamond-7 :club-8]}
  )
(def case-47
  {:players {:seat0 [1000 nil [:heart-A :diamond-2 :club-K :spade-Q]]
             :seat1 [800 :all-in [:heart-J :diamond-6 :club-9 :spade-10]]
             :seat2 [400 :all-in [:heart-3 :diamond-4 :club-K :spade-Q]]}
   :community-cards [:club-2 :spade-9 :heart-4 :diamond-7 :club-8]}
  )
(def case-48
  {:players {:seat0 [1200 nil [:heart-5 :diamond-6 :club-9 :spade-10]]
             :seat1 [800 :all-in [:heart-2 :diamond-4 :club-K :spade-Q]]
             :seat2 [400 :all-in [:heart-3 :diamond-4 :club-K :spade-Q]]}
   :community-cards [:club-2 :spade-3 :heart-4 :diamond-7 :club-8]}
  )
(def case-49
  {:players {:seat0 [1200 nil [:diamond-5 :diamond-6 :club-9 :spade-10]]
             :seat1 [800 :all-in [:spade-5 :heart-6 :diamond-9 :diamond-10]]
             :seat2 [400 :all-in [:heart-5 :club-6 :spade-9 :heart-10]]}
   :community-cards [:club-2 :spade-3 :heart-4 :diamond-7 :club-8]}
  )
(def case-50
     {:players {:seat0 [1600 nil [:heart-5 :diamond-Q :club-2 :club-3]]
                :seat1 [800 :all-in [:heart-7 :diamond-7 :diamond-2 :diamond-3]]
                :seat2 [400 :all-in [:spade-J :heart-J :spade-2 :spade-3]]
                :seat3 [1400 :all-in [:heart-Q :diamond-K :heart-4 :heart-3]]}
      :community-cards [:club-J :spade-7 :heart-6 :diamond-J :club-5]}
     )
(def case-51
  {:players {:seat0 [1600 nil [:heart-9 :diamond-Q :club-2 :club-3]]
             :seat1 [800 :all-in [:heart-10 :diamond-10 :diamond-2 :diamond-3]]
             :seat2 [400 :all-in [:heart-Q :diamond-K :heart-A :heart-3]]
             :seat3 [1400 :all-in [:spade-J :heart-J :spade-2 :spade-3]]}
   :community-cards [:club-J :spade-7 :heart-6 :diamond-J :club-8]}
  )
(def case-54
  {:players {:seat0 [1600 nil [:spade-J :heart-8 :spade-2 :spade-3]]
             :seat1 [800 :all-in [:heart-7 :diamond-10 :diamond-2 :diamond-3]]
             :seat2 [400 :all-in [:heart-Q :diamond-K :heart-2 :heart-3]]
             :seat3 [1400 :all-in [:heart-J :club-8 :spade-2 :spade-3]]}
   :community-cards [:club-J :spade-8 :heart-6 :diamond-J :club-7]}
  )
(def case-55
  {:players {:seat0 [1600 nil [:heart-9 :diamond-Q :club-2 :club-3]]
             :seat1 [800 :all-in [:heart-8 :diamond-8 :diamond-2 :diamond-4]]
             :seat2 [400 :all-in [:spade-J :heart-J :spade-2 :spade-4]]
             :seat3 [1400 :all-in [:heart-Q :diamond-K :heart-2 :heart-4]]
             :seat4 [1600 :nil [:heart-Q :diamond-10 :heart-2 :heart-4]]}
   :community-cards [:club-J :spade-8 :heart-6 :diamond-J :club-7]}
  )
(def case-57
  {:players {:seat0 [1600 nil [:heart-10 :diamond-9 :heart-5 :diamond-K]]
             :seat1 [800 :all-in [:diamond-10 :spade-9 :diamond-5 :spade-K]]
             :seat2 [400 :all-in [:spade-10 :club-9 :spade-5 :club-K]]
             :seat3 [1400 :all-in [:heart-2 :diamond-3 :heart-4 :diamond-Q]]
             :seat4 [1600 nil [:club-10 :heart-9 :heart-5 :diamond-K]]}
   :community-cards [:club-J :spade-7 :heart-6 :diamond-J :club-8]}
  )
(def case-58
  {:players {:seat0 [1600 nil [:heart-9 :diamond-Q :club-2 :club-4]]
             :seat1 [1600 :all-in [:heart-10 :diamond-10 :diamond-2 :diamond-4]]
             :seat2 [1600 :all-in [:heart-Q :diamond-K :heart-2 :heart-4]]
             :seat3 [1600 :all-in [:spade-J :heart-J :spade-2 :spade-3]]
             :seat4 [1600 :nil [:heart-Q :diamond-8 :heart-2 :heart-4]]}
   :community-cards [:club-J :spade-7 :heart-6 :diamond-J :club-8]}
  )
(def case-60
  {:players {:seat0 [1600 :all-in [:heart-7 :diamond-K :club-2 :club-Q]]
             :seat1 [800 :all-in [:heart-10 :heart-8 :diamond-2 :diamond-Q]]
             :seat2 [400 :all-in [:spade-J :heart-J :spade-2 :spade-3]]
             :seat3 [1400 :all-in [:diamond-10 :diamond-8 :diamond-2 :diamond-Q]]
             :seat4 [1800 :all-in [:diamond-7 :club-K :heart-2 :heart-Q]]}
   :community-cards [:club-J :spade-7 :heart-6 :diamond-J :club-8]}
  )


; case 1~20
;(dbg (all-in/test-case case-20 :texas) :pp)

; case 21~40
;(dbg (all-in/test-case case-60 :omaha) :pp)
;
;; case 41~
;(dbg (all-in/test-case case-60 :omaha-hi-lo :low) :pp)