(ns vita.slotmachine.generator-test
  (:require [vita.scene.slotmachine.generator :refer [get-random-triples-info]]
            [clojure.test :refer :all]
            [clojure.test.check.clojure-test :as ct :refer [defspec]]
            [clojure.test.check :as tc]
            [clojure.test.check.generators :as gen]
            [clojure.test.check.properties :as prop]))

;; get-random-triples-info

(deftest non-nil-get-random-triples-info
  (dotimes [_ 10000]
    (let [triples-info (get-random-triples-info (inc (rand-int 20)) (* (inc (rand-int 5)) 200))]
      (is (not= triples-info nil))
      (is (= (count triples-info) 2))
      (when (not= (count (:triples triples-info)) 5)
        (println triples-info))
      (is (= (count (:triples triples-info)) 5))
      )))
