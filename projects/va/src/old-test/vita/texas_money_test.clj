;(ns clj.vita.texas-money-test
;  (:require [clojure.test :refer :all]
;            [vita.game.holdem.holdem-room :as hr]
;            [vita.game.holdem.room_exec :as re]
;            [vita.game.holdem.net.net-join :as p-join]
;            [vita.game.holdem.net.net-seat :as p-seat]
;            [clojure.core.async :as async :refer [alt! <! >!  timeout chan  go close!]]
;            ))
;
;
;
;
;(defn login-proc[]
;  (let [r-id (:room-id @(re/test-join-room))]
;    (println "1.room-id -> " r-id)
;    (let [aa1 (vita.db.game-db/get$ :aa1)
;          aa2 (vita.db.game-db/get$ :aa2) ]
;      (vita.session/add-tk-sid :token-1 "aa1")
;      (vita.session/add-tk-sid :token-2 "aa2")
;      (vita.session/add-tk-account :token-1 aa1)
;      (vita.session/add-tk-account :token-2 aa2)
;      (let [join-data1 {:token :token-1 :room-id r-id}
;            join-data2 {:token :token-2 :room-id r-id}
;
;            seat-data1 {:token :token-1 :room-id r-id :seat :seat1}
;            seat-data2 {:token :token-2 :room-id r-id :seat :seat2}]
;        (p-join/req-join join-data1 :texas)
;        (p-join/req-join join-data2 :texas)
;
;        (p-seat/req-seat seat-data1 :texas)
;        (p-seat/req-seat seat-data2 :texas))
;    ) r-id))
;
;(defn bet
;  []
;  (hr/erasure-room-list :texas)
;  (vita.db.game-db/change-user-money :aa1 10000)
;  (vita.db.game-db/change-user-money :aa2 10000)
;  (println "aa1 db money->" (vita.db.game-db/get$ :aa1))
;  (println "aa2 db money->" (vita.db.game-db/get$ :aa2))
;  (let [ room-id (login-proc)]
;    (let [room    (hr/get-room room-id :texas)
;          cur-tk  (:cur-user-sid @room)
;          data    {:token cur-tk :select :call :bet 100}]
;      (println "room-id -> " room-id)
;      (println "room-> " (dissoc @room :cards))
;      (println "sb-sid-> " (:sb-sid @room) )
;      (println "bb-sid-> " (:bb-sid @room) )
;      (println "cur-tk " cur-tk)
;
;    (go (<! timeout 5000)
;        (let [room    (hr/get-room room-id :texas)
;              cur-tk  (:cur-user-sid @room)
;              data    {:token cur-tk :select :call :bet 100}]
;          (println "sb-sid-> " (:sb-sid @room) )
;          (println "bb-sid-> " (:bb-sid @room) )
;          (println "cur-tk " cur-tk)
;          (re/bet room data )))
;
;
;    (go (<! timeout 6000)
;        (println "11")
;        (let [room    (hr/get-room room-id :texas)
;              cur-tk  (:cur-user-sid @room)
;              data    {:token cur-tk :select :fold :bet 0}]
;          (println "cur-tk " cur-tk)
;          (re/bet room data)
;          (hr/debug-room "aa" room-id :texas)))
;    (go (<! timeout 6100)
;        (println "22")
;        (let [room (hr/get-room room-id :texas)]
;          (re/remove-user :token2 (:room-id @room) :texas)
;          (hr/debug-room "aa1" room-id :texas)
;          ))
;    (go (<! (timeout 7000))
;        (println "33")
;        (println "remove user-> " :token-2)
;        (re/remove-user :token-2 room-id :texas))
;
;    (go (<! timeout 10000)
;        (println "44")
;          (let [aa1 (vita.db.game-db/get$ :aa1)
;                aa2 (vita.db.game-db/get$ :aa2) ]
;            (println "aa-1 db->" aa1)
;            (println "aa-2 db->" aa2)
;            (is (= (get-in aa1 [:user :money]) 99100))
;            (is (= (get-in aa2 [:user :money]) 10100))) ))
;  ))
;(bet)
