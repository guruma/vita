(ns test.sonaclo.crypto
  (:require [sonaclo.crypto :as cr :refer [create-tables decode encode]]))

(def user* (atom {:sock nil}))

; when user longins, cription tables is make by server.
; And it should be set in user with sock.
;; (defn init-tables []
;;   (swap! user* assoc :crypto (create-tables)))

;; (defn test-cript [s]
;;   (let [{:keys [base64-digits enc-table dec-table]} (:crypto @user*)]
;;     (= s (decode (encode s enc-table) dec-table))))

;; (init-tables)

;; (decode
;;  (encode "가나다라" (get-in @user* [:crypto :enc-table]))
;;   (get-in @user* [:crypto :dec-table]))

;; (test-cript "hello world!")
;; (test-cript "`!@   ~@#!#$@$%#^*&^(&()*(+)__*({}{|{|\"::<>?<>\"}})")
;; (test-cript "가나다라")
;; (test-cript "서울")
;; (test-cript "こんにちは")
;; (test-cript "你好")
;; (test-cript "Привет")
;; (test-cript "हाय")
;; (test-cript "서울 abcd 123ㅏㅕ4")
;; (test-cript (str {"a" 123}))
;; (test-cript (str {123 123}))
;; (test-cript (str {{:a 1} {:b 22}}))
;; (test-cript (str {:a 1 :b 3 :c {:fdaf 5}}))
;; (test-cript (str [1 2 3 4 5]))
;; (test-cript (str #{1 2 3 4 5}))
;; (test-cript (str '(1 2 3 4 5)))
