(ns vita.poker.calc.test-pots
  (:use [clojure.test])
  (:require [vita.poker.calc.pots :as pots :reload true]
            [vita.poker.calc.score :as score :reload true]
            [vita.dealer :as dealer :reload true] ))

; (require '[philos.debug :refer :all])

(defn- make-user-info
  [game-name [sid player-info] community-cards]
  (let [cards (get player-info 2)]
    {:sid   sid
     :cards cards
     :score (score/calc-score game-name cards community-cards)
     :bet   (get player-info 0) }))

(defn- get-fold-sids [players]
  (keep (fn [[sid player-info]]
          (if (= :fold (get player-info 1))
            sid))
        players))

(defn- mark-fold-sid
  [users fold-sids]
  (map (fn [user]
         (if ((set fold-sids) (get user :sid))
           (assoc user :cards nil)
           user))
        users))

(defn- sort-users-by-sid
  [users]
  (sort #(compare (get %1 :sid) (get %2 :sid)) users))

(defn- test-case
  ([game-name case]
   (test-case game-name case :high))
  ([game-name case mode]
   (let [community-cards (get case :community-cards)
         players         (get case :players)
         users           (map #(make-user-info game-name % community-cards) players)
         fold-sids       (get-fold-sids players)
         users'          (mark-fold-sid users fold-sids)]
     ;(dbg (sort-users-by-sid users') :pp)
     (dealer/calc-awards game-name users') )))


; ==== Texas holdem =====================
(def case-1
  {:players {"sid0" [20000 nil [:heart-7 :diamond-K]]
             "sid1" [20000 :fold [:heart-5 :diamond-10]]
             "sid2" [200 nil [:spade-K :diamond-Q]]
             "sid3" [20000 nil [:diamond-8 :heart-9]]
             "sid4" [20000 nil [:diamond-9 :diamond-3]]}
   :community-cards [:club-J :spade-10 :heart-6 :diamond-J :club-9] })

(def case-2
  {:players {"sid0" [1200 :fold [:heart-4 :diamond-3]]
             "sid1" [800 nil [:heart-6 :diamond-2]]
             "sid2" [500 nil [:spade-7 :diamond-8]]
             "sid3" [100 nil [:diamond-Q :heart-Q]]
             "sid4" [1400 nil [:diamond-9 :diamond-3]]}
   :community-cards [:club-5 :spade-5 :heart-5 :diamond-J :club-J] })

(def case-3
  {:players {"sid0" [1200 nil [:heart-3 :diamond-4]]
             "sid1" [800 nil [:heart-5 :diamond-6]]}
   :community-cards [:club-2 :spade-3 :heart-4 :diamond-7 :club-8] })

(def case-4
  {:players {"sid0" [1200 nil [:heart-5 :diamond-6]]
             "sid1" [800 nil [:heart-3 :diamond-4]]}
   :community-cards [:club-2 :spade-3 :heart-4 :diamond-7 :club-8] })

(def case-5
  {:players {"sid0" [1200 nil [:heart-5 :diamond-6]]
             "sid1" [800 nil [:club-5 :spade-6]]}
   :community-cards [:club-2 :spade-3 :heart-4 :diamond-7 :club-8] })

(def case-6
  {:players {"sid0" [1000 nil [:heart-9 :diamond-Q]]
             "sid1" [800 nil [:heart-10 :diamond-K]]
             "sid2" [400 nil [:heart-5 :diamond-6]]}
   :community-cards [:club-2 :spade-3 :heart-4 :diamond-7 :club-8] })

(def case-7
  {:players {"sid0" [1000 nil [:heart-9 :diamond-Q]]
             "sid1" [800 nil [:heart-5 :diamond-6]]
             "sid2" [400 nil [:heart-10 :diamond-K]]}
   :community-cards [:club-2 :spade-3 :heart-4 :diamond-7 :club-8] })

(def case-8
  {:players {"sid0" [1200 nil [:heart-5 :diamond-6]]
             "sid1" [800 nil [:heart-9 :diamond-10]]
             "sid2" [400 nil [:heart-7 :diamond-8]]}
   :community-cards [:club-2 :spade-3 :heart-4 :diamond-7 :club-8] })

(def case-9
  {:players {"sid0" [1200 nil [:diamond-5 :diamond-6]]
             "sid1" [800 nil [:spade-5 :heart-6]]
             "sid2" [400 nil [:heart-5 :club-6]]}
   :community-cards [:club-2 :spade-3 :heart-4 :diamond-7 :club-8] })

(def case-10
  {:players {"sid0" [1600 nil [:heart-9 :diamond-Q]]
             "sid1" [800 nil [:heart-10 :diamond-10]]
             "sid2" [400 nil [:spade-J :heart-J]]
             "sid3" [1400 nil [:heart-Q :diamond-K]]}
   :community-cards [:club-J :spade-10 :heart-6 :diamond-J :club-9] })

(def case-11
  {:players {"sid0" [1600 nil [:heart-9 :diamond-Q]]
             "sid1" [800 nil [:heart-10 :diamond-10]]
             "sid2" [400 nil [:heart-Q :diamond-K]]
             "sid3" [1400 nil [:spade-J :heart-J]]}
   :community-cards [:club-J :spade-10 :heart-6 :diamond-J :club-9] })

(def case-12
  {:players {"sid0" [1600 nil [:spade-J :heart-J]]
             "sid1" [800 nil [:heart-10 :diamond-10]]
             "sid2" [400 nil [:heart-Q :diamond-K]]
             "sid3" [1400 nil [:heart-9 :diamond-Q]]}
   :community-cards [:club-J :spade-10 :heart-6 :diamond-J :club-9] })

(def case-13
  {:players {"sid0" [1600 nil [:spade-2 :heart-3]]
             "sid1" [800 nil [:heart-2 :diamond-3]]
             "sid2" [400 nil [:club-2 :spade-3]]
             "sid3" [1400 nil [:diamond-2 :club-3]]}
   :community-cards [:club-J :spade-10 :heart-6 :diamond-J :club-9] })

(def case-14
  {:players {"sid0" [1600 nil [:spade-2 :heart-3]]
             "sid1" [800 :fold [:heart-J :diamond-3]]
             "sid2" [400 :fold [:club-2 :spade-J]]
             "sid3" [1400 nil [:diamond-2 :club-3]]}
   :community-cards [:club-J :spade-10 :heart-6 :diamond-J :club-9] })

(def case-15
  {:players {"sid0" [1600 nil [:heart-9 :diamond-Q]]
             "sid1" [800 nil [:heart-10 :diamond-10]]
             "sid2" [400 nil [:spade-J :heart-J]]
             "sid3" [1400 nil [:heart-Q :diamond-K]]
             "sid4" [1600 nil [:heart-8 :diamond-7]]}
   :community-cards [:club-J :spade-10 :heart-6 :diamond-J :club-9] })

(def case-16
  {:players {"sid0" [1600 nil [:heart-K :diamond-K]]
             "sid1" [800 nil [:heart-3 :diamond-4]]
             "sid2" [400 nil [:spade-A :heart-2]]
             "sid3" [1400 nil [:heart-5 :diamond-6]]
             "sid4" [1600 nil [:club-K :spade-K]]}
   :community-cards [:club-J :spade-10 :heart-6 :diamond-J :club-9]}
  )
(def case-17
  {:players {"sid0" [1600 nil [:heart-2 :diamond-3]]
             "sid1" [800 nil [:diamond-2 :spade-3]]
             "sid2" [400 nil [:spade-2 :club-3]]
             "sid3" [1400 :fold [:heart-5 :diamond-6]]
             "sid4" [1600 nil [:club-2 :heart-3]]}
   :community-cards [:club-J :spade-10 :heart-6 :diamond-J :club-9] })

(def case-18
  {:players {"sid0" [1600 nil [:heart-2 :diamond-3]]
             "sid1" [1600 nil [:diamond-2 :spade-3]]
             "sid2" [1600 nil [:spade-2 :club-3]]
             "sid3" [1600 nil [:heart-5 :diamond-6]]
             "sid4" [1600 nil [:club-2 :heart-3]]}
   :community-cards [:club-J :spade-10 :heart-6 :diamond-J :club-9] })

(def case-19
  {:players {"sid0" [1600000 nil [:heart-2 :diamond-3]]
             "sid1" [1600000 nil [:diamond-2 :spade-3]]
             "sid2" [1600000 nil [:spade-2 :club-3]]
             "sid3" [1600000 nil [:heart-5 :diamond-6]]
             "sid4" [1600000 nil [:club-2 :heart-3]]}
   :community-cards [:club-J :spade-10 :heart-6 :diamond-J :club-9] })

(def case-20
  {:players {"sid0" [1600 nil [:heart-8 :diamond-8]]
             "sid1" [800 nil [:heart-Q :diamond-Q]]
             "sid2" [400 nil [:spade-J :heart-J]]
             "sid3" [1400 nil [:spade-Q :club-Q]]
             "sid4" [1800 nil [:club-8 :spade-8]]}
   :community-cards [:club-J :spade-10 :heart-6 :diamond-J :club-9] })


; ==== Omaha =====================
(def case-21
  {:players {"sid0" [20000 nil [:heart-7 :diamond-K :heart-4 :heart-5]]
             "sid1" [20000 :fold [:club-5 :diamond-10 :heart-2 :heart-3]]
             "sid2" [200 nil [:spade-K :diamond-Q :spade-2 :spade-3]]
             "sid3" [20000 nil [:diamond-8 :heart-9 :diamond-2 :diamond-4]]
             "sid4" [20000 nil [:diamond-9 :spade-8 :club-2 :club-3]]}
   :community-cards [:club-J :spade-10 :heart-6 :diamond-J :club-9] })

(def case-22
  {:players {"sid0" [1200 :fold [:heart-4 :club-3 :club-4 :club-5]]
             "sid1" [800 nil [:heart-6 :club-J :spade-2 :spade-3]]
             "sid2" [500 nil [:spade-6 :spade-J :heart-2 :heart-3]]
             "sid3" [100 nil [:diamond-Q :heart-Q :diamond-2 :diamond-3]]
             "sid4" [1400 nil [:diamond-6 :heart-J :club-2 :heart-4]]}
   :community-cards [:club-5 :spade-5 :heart-5 :diamond-J :club-K] })

(def case-23
  {:players {"sid0" [1200 nil [:heart-3 :diamond-4 :club-K :spade-Q]]
             "sid1" [800 nil [:heart-5 :diamond-6 :club-9 :spade-10]]}
   :community-cards [:club-2 :spade-3 :heart-4 :diamond-7 :club-8] })

(def case-24
  {:players {"sid0" [1200 nil [:heart-5 :diamond-6 :club-9 :spade-10]]
             "sid1" [800 nil [:heart-3 :diamond-4 :club-K :spade-Q]]}
   :community-cards [:club-2 :spade-3 :heart-4 :diamond-7 :club-8] })

(def case-25
  {:players {"sid0" [1200 nil [:heart-5 :diamond-6 :club-9 :spade-10]]
             "sid1" [800 nil [:spade-5 :spade-6 :spade-9 :heart-10]]}
   :community-cards [:club-2 :spade-3 :heart-4 :diamond-7 :club-8] })

(def case-26
  {:players {"sid0" [1000 nil [:heart-2 :diamond-4 :club-K :spade-Q]]
             "sid1" [800 nil [:heart-3 :diamond-4 :club-K :spade-Q]]
             "sid2" [400 nil [:heart-5 :diamond-6 :club-9 :spade-10]]}
   :community-cards [:club-2 :spade-3 :heart-4 :diamond-7 :club-8] })

(def case-27
  {:players {"sid0" [1000 nil [:heart-2 :diamond-4 :club-K :spade-Q]]
             "sid1" [800 nil [:heart-5 :diamond-6 :club-9 :spade-10]]
             "sid2" [400 nil [:heart-3 :diamond-4 :club-K :spade-Q]]}
   :community-cards [:club-2 :spade-3 :heart-4 :diamond-7 :club-8] })

(def case-28
  {:players {"sid0" [1200 nil [:heart-5 :diamond-6 :club-9 :spade-10]]
             "sid1" [800 nil [:heart-2 :diamond-4 :club-K :spade-Q]]
             "sid2" [400 nil [:heart-3 :diamond-4 :club-K :spade-Q]]}
   :community-cards [:club-2 :spade-3 :heart-4 :diamond-7 :club-8] })

(def case-29
  {:players {"sid0" [1200 nil [:diamond-5 :diamond-6 :club-9 :spade-10]]
             "sid1" [800 nil [:spade-5 :heart-6 :diamond-9 :diamond-10]]
             "sid2" [400 nil [:heart-5 :club-6 :spade-9 :heart-10]]}
   :community-cards [:club-2 :spade-3 :heart-4 :diamond-7 :club-8] })

(def case-30
  {:players {"sid0" [1600 nil [:heart-9 :diamond-Q :club-2 :club-3]]
             "sid1" [800 nil [:heart-10 :diamond-10 :diamond-2 :diamond-3]]
             "sid2" [400 nil [:spade-J :heart-J :spade-2 :spade-3]]
             "sid3" [1400 nil [:heart-Q :diamond-K :heart-2 :heart-3]]}
   :community-cards [:club-J :spade-10 :heart-6 :diamond-J :club-9] })

(def case-31
  {:players {"sid0" [1600 nil [:heart-9 :diamond-Q :club-2 :club-3]]
             "sid1" [800 nil [:heart-10 :diamond-10 :diamond-2 :diamond-3]]
             "sid2" [400 nil [:heart-Q :diamond-K :heart-2 :heart-3]]
             "sid3" [1400 nil [:spade-J :heart-J :spade-2 :spade-3]]}
   :community-cards [:club-J :spade-10 :heart-6 :diamond-J :club-9] })

(def case-32
  {:players {"sid0" [1600 nil [:spade-J :heart-J :spade-2 :spade-3]]
             "sid1" [800 nil [:heart-10 :diamond-10 :diamond-2 :diamond-3]]
             "sid2" [400 nil [:heart-Q :diamond-K :heart-2 :heart-3]]
             "sid3" [1400 nil [:heart-9 :diamond-Q :club-2 :club-3]]}
   :community-cards [:club-J :spade-10 :heart-6 :diamond-J :club-9] })

(def case-33
  {:players {"sid0" [1600 nil [:heart-A :diamond-Q :club-K :spade-3]]
             "sid1" [800 nil [:diamond-A :club-Q :spade-K :heart-3]]
             "sid2" [400 nil [:club-A :spade-Q :heart-K :diamond-3]]
             "sid3" [1400 nil [:spade-A :heart-Q :diamond-K :club-3]]}
   :community-cards [:club-J :spade-10 :heart-6 :diamond-J :club-9] })

(def case-34
  {:players {"sid0" [1600 nil [:spade-J :heart-10 :spade-2 :spade-3]]
             "sid1" [800 nil [:heart-7 :diamond-10 :diamond-2 :diamond-3]]
             "sid2" [400 nil [:heart-Q :diamond-K :heart-2 :heart-3]]
             "sid3" [1400 nil [:heart-J :club-10 :spade-2 :spade-3]]}
   :community-cards [:club-J :spade-10 :heart-6 :diamond-J :club-9] })

(def case-35
  {:players {"sid0" [1600 nil [:heart-9 :diamond-Q :club-2 :club-3]]
             "sid1" [800 nil [:heart-10 :diamond-10 :diamond-2 :diamond-3]]
             "sid2" [400 nil [:spade-J :heart-J :spade-2 :spade-3]]
             "sid3" [1400 nil [:heart-Q :diamond-K :heart-2 :heart-3]]
             "sid4" [1600 nil [:heart-Q :diamond-8 :heart-2 :heart-3]]}
   :community-cards [:club-J :spade-10 :heart-6 :diamond-J :club-9] })

(def case-36
  {:players {"sid0" [1600 nil [:spade-J :heart-10 :spade-2 :spade-3]]
             "sid1" [800 nil [:heart-A :diamond-6 :diamond-2 :diamond-3]]
             "sid2" [400 nil [:spade-4 :club-6 :club-2 :club-3]]
             "sid3" [1400 nil [:heart-Q :diamond-K :heart-2 :heart-3]]
             "sid4" [1600 nil [:heart-J :diamond-10 :spade-A :heart-4]]}
   :community-cards [:club-J :spade-10 :heart-6 :diamond-J :club-9] })

(def case-37
  {:players {"sid0" [1600 nil [:heart-2 :diamond-3 :heart-7 :diamond-K]]
             "sid1" [800 nil [:diamond-2 :spade-3 :diamond-7 :spade-K]]
             "sid2" [400 nil [:spade-2 :club-3 :spade-7 :club-K]]
             "sid3" [1400 :fold [:heart-5 :diamond-6 :heart-10 :diamond-Q]]
             "sid4" [1600 nil [:club-2 :heart-3 :heart-7 :diamond-K]]}
   :community-cards [:club-J :spade-10 :heart-6 :diamond-J :club-9] })

(def case-38
  {:players {"sid0" [1600 nil [:heart-9 :diamond-Q :club-2 :club-3]]
             "sid1" [1600 nil [:heart-10 :diamond-10 :diamond-2 :diamond-3]]
             "sid2" [1600 nil [:heart-Q :diamond-K :heart-2 :heart-3]]
             "sid3" [1600 nil [:spade-J :heart-J :spade-2 :spade-3]]
             "sid4" [1600 nil [:heart-Q :diamond-8 :heart-2 :heart-3]]}
   :community-cards [:club-J :spade-10 :heart-6 :diamond-J :club-9] })

(def case-39
  {:players {"sid0" [1600000 nil [:heart-9 :diamond-Q :club-2 :club-3]]
             "sid1" [1600000 nil [:heart-10 :diamond-10 :diamond-2 :diamond-3]]
             "sid2" [1600000 nil [:heart-Q :diamond-K :heart-2 :heart-3]]
             "sid3" [1600000 nil [:spade-J :heart-J :spade-2 :spade-3]]
             "sid4" [1600000 nil [:heart-Q :diamond-8 :heart-2 :heart-3]]}
   :community-cards [:club-J :spade-10 :heart-6 :diamond-J :club-9] })

(def case-40
  {:players {"sid0" [1600 nil [:heart-8 :diamond-K :club-2 :club-3]]
             "sid1" [800 nil [:heart-10 :heart-9 :diamond-2 :diamond-3]]
             "sid2" [400 nil [:spade-J :heart-J :spade-2 :spade-3]]
             "sid3" [1400 nil [:diamond-10 :diamond-9 :diamond-2 :diamond-3]]
             "sid4" [1800 nil [:diamond-8 :club-K :heart-2 :heart-3]]}
   :community-cards [:club-J :spade-10 :heart-6 :diamond-J :club-9] })


; ==== Omaha hi-lo =====================
(def case-41
  {:players {"sid0" [20000 nil [:heart-2 :diamond-3 :heart-7 :heart-5]]
             "sid1" [20000 :fold [:diamond-5 :diamond-10 :heart-2 :heart-3]]
             "sid2" [200 nil [:spade-J :diamond-Q :spade-2 :spade-9]]
             "sid3" [20000 nil [:diamond-8 :heart-9 :diamond-2 :diamond-4]]
             "sid4" [20000 nil [:diamond-9 :spade-8 :club-2 :club-4]]}
   :community-cards [:club-J :spade-A :heart-6 :diamond-J :club-8] })

(def case-42
  {:players {"sid0" [1200 nil [:heart-A :club-K :club-4 :club-9]]
             "sid1" [800 nil [:heart-6 :club-J :spade-2 :spade-3]]
             "sid2" [500 nil [:spade-6 :spade-J :heart-2 :heart-3]]
             "sid3" [100 nil [:diamond-Q :heart-Q :diamond-2 :diamond-3]]
             "sid4" [1400 nil [:diamond-6 :heart-J :club-5 :club-3]]}
   :community-cards [:club-2 :spade-5 :heart-5 :diamond-7 :club-8] })

(def case-43
  {:players {"sid0" [1200 nil [:heart-3 :diamond-A :club-K :spade-J]]
             "sid1" [800 nil [:heart-K :diamond-4 :club-9 :spade-10]]}
   :community-cards [:club-J :spade-Q :heart-4 :diamond-7 :club-8] })

(def case-44
  {:players {"sid0" [1200 nil [:heart-5 :diamond-6 :club-9 :spade-10]]
             "sid1" [800 nil [:heart-3 :diamond-4 :club-K :spade-Q]]}
   :community-cards [:club-2 :spade-3 :heart-4 :diamond-7 :club-8] })

(def case-45
  {:players {"sid0" [1200 nil [:heart-5 :diamond-6 :club-9 :spade-10]]
             "sid1" [800 nil [:spade-5 :spade-6 :spade-9 :heart-10]]}
   :community-cards [:club-2 :spade-3 :heart-4 :diamond-7 :club-8] })\

(def case-46
  {:players {"sid0" [1000 nil [:heart-2 :heart-4 :diamond-K :heart-Q]]
             "sid1" [800 nil [:heart-3 :heart-5 :club-K :spade-Q]]
             "sid2" [400 nil [:heart-A :diamond-4 :club-9 :spade-10]]}
   :community-cards [:club-2 :spade-3 :heart-6 :diamond-7 :club-8] })

(def case-47
  {:players {"sid0" [1000 nil [:heart-A :diamond-2 :club-K :spade-Q]]
             "sid1" [800 nil [:heart-J :diamond-6 :club-9 :spade-10]]
             "sid2" [400 nil [:heart-3 :diamond-4 :club-K :spade-Q]]}
   :community-cards [:club-2 :spade-9 :heart-4 :diamond-7 :club-8] })

(def case-48
  {:players {"sid0" [1200 nil [:heart-5 :diamond-6 :club-9 :spade-10]]
             "sid1" [800 nil [:heart-2 :diamond-4 :club-K :spade-Q]]
             "sid2" [400 nil [:heart-3 :diamond-4 :club-K :spade-Q]]}
   :community-cards [:club-2 :spade-3 :heart-4 :diamond-7 :club-8] })

(def case-49
  {:players {"sid0" [1200 nil [:diamond-5 :diamond-6 :club-9 :spade-10]]
             "sid1" [800 nil [:spade-5 :heart-6 :diamond-9 :diamond-10]]
             "sid2" [400 nil [:heart-5 :club-6 :spade-9 :heart-10]]}
   :community-cards [:club-2 :spade-3 :heart-4 :diamond-7 :club-8] })

(def case-50
     {:players {"sid0" [1600 nil [:heart-5 :diamond-Q :club-2 :club-3]]
                "sid1" [800 nil [:heart-7 :diamond-7 :diamond-2 :diamond-3]]
                "sid2" [400 nil [:spade-J :heart-J :spade-2 :spade-3]]
                "sid3" [1400 nil [:heart-Q :diamond-K :heart-4 :heart-3]]}
      :community-cards [:club-J :spade-7 :heart-6 :diamond-J :club-5] })

(def case-51
  {:players {"sid0" [1600 nil [:heart-9 :diamond-Q :club-2 :club-3]]
             "sid1" [800 nil [:heart-10 :diamond-10 :diamond-2 :diamond-3]]
             "sid2" [400 nil [:heart-Q :diamond-K :heart-A :heart-3]]
             "sid3" [1400 nil [:spade-J :heart-J :spade-2 :spade-3]]}
   :community-cards [:club-J :spade-7 :heart-6 :diamond-J :club-8] })

(def case-54
  {:players {"sid0" [1600 nil [:spade-J :heart-8 :spade-2 :spade-3]]
             "sid1" [800 nil [:heart-7 :diamond-10 :diamond-2 :diamond-3]]
             "sid2" [400 nil [:heart-Q :diamond-K :heart-2 :heart-3]]
             "sid3" [1400 nil [:heart-J :club-8 :spade-2 :spade-3]]}
   :community-cards [:club-J :spade-8 :heart-6 :diamond-J :club-7] })

(def case-55
  {:players {"sid0" [1600 nil [:heart-9 :diamond-Q :club-2 :club-3]]
             "sid1" [800 nil [:heart-8 :diamond-8 :diamond-2 :diamond-4]]
             "sid2" [400 nil [:spade-J :heart-J :spade-2 :spade-4]]
             "sid3" [1400 nil [:heart-Q :diamond-K :heart-2 :heart-4]]
             "sid4" [1600 nil [:heart-Q :diamond-10 :heart-2 :heart-4]]}
   :community-cards [:club-J :spade-8 :heart-6 :diamond-J :club-7] })

(def case-57
  {:players {"sid0" [1600 nil [:heart-10 :diamond-9 :heart-5 :diamond-K]]
             "sid1" [800 nil [:diamond-10 :spade-9 :diamond-5 :spade-K]]
             "sid2" [400 nil [:spade-10 :club-9 :spade-5 :club-K]]
             "sid3" [1400 nil [:heart-2 :diamond-3 :heart-4 :diamond-Q]]
             "sid4" [1600 nil [:club-10 :heart-9 :heart-5 :diamond-K]]}
   :community-cards [:club-J :spade-7 :heart-6 :diamond-J :club-8] })

(def case-58
  {:players {"sid0" [1600 nil [:heart-9 :diamond-Q :club-2 :club-4]]
             "sid1" [1600 nil [:heart-10 :diamond-10 :diamond-2 :diamond-4]]
             "sid2" [1600 nil [:heart-Q :diamond-K :heart-2 :heart-4]]
             "sid3" [1600 nil [:spade-J :heart-J :spade-2 :spade-3]]
             "sid4" [1600 nil [:heart-Q :diamond-8 :heart-2 :heart-4]]}
   :community-cards [:club-J :spade-7 :heart-6 :diamond-J :club-8] })

(def case-60
  {:players {"sid0" [1600 nil [:heart-7 :diamond-K :club-2 :club-Q]]
             "sid1" [800 nil [:heart-10 :heart-8 :diamond-2 :diamond-Q]]
             "sid2" [400 nil [:spade-J :heart-J :spade-2 :spade-3]]
             "sid3" [1400 nil [:diamond-10 :diamond-8 :diamond-2 :diamond-Q]]
             "sid4" [1800 nil [:diamond-7 :club-K :heart-2 :heart-Q]]}
   :community-cards [:club-J :spade-7 :heart-6 :diamond-J :club-8] })


; case 1~20
(deftest test-texas
  (is (= {:overage nil,
          :high [[1000 ["sid2"]]
                 [79200 ["sid3" "sid4"]]],
          :low nil}
         (test-case :texas case-1) ))

  (is (= {:overage [200 "sid4"], 
          :high [[500 ["sid3"]]
                 [1600 ["sid1" "sid2" "sid4"]] 
                 [900 ["sid1" "sid4"]]
                 [800 ["sid4"]]],
          :low nil}
         (test-case :texas case-2) ))

  (is (= {:overage [400 "sid0"],
          :high [[1600 ["sid1"]]],
          :low nil}
         (test-case :texas case-3) ))

  (is (= {:overage [400 "sid0"],
          :high [[1600 ["sid0"]]],
          :low nil}
         (test-case :texas case-4) ))

  (is (= {:overage [400 "sid0"],
          :high [[1600 ["sid0" "sid1"]]]
          :low nil}
         (test-case :texas case-5) ))

  (is (= {:overage [200 "sid0"],
          :high [[1200 ["sid2"]]
                 [800 ["sid1"]]],
          :low nil}
         (test-case :texas case-6) ))

  (is (= {:overage [200 "sid0"],
          :high [[1200 ["sid1"]]
                 [800 ["sid1"]]],
          :low nil}
         (test-case :texas case-7) ))

  (is (= {:overage [400 "sid0"],
          :high [[1200 ["sid0"]] [800 ["sid0"]]],
          :low nil}
         (test-case :texas case-8) ))

  (is (= {:overage [400 "sid0"],
          :high [[1200 ["sid0" "sid1" "sid2"]]
                 [800 ["sid0" "sid1"]]],
          :low nil}
         (test-case :texas case-9) ))

  (is (= {:overage [200 "sid0"],
          :high [[1600 ["sid2"]]
                 [1200 ["sid1"]]
                 [1200 ["sid3"]]],
          :low nil}
         (test-case :texas case-10) ))

  (is (= {:overage [200 "sid0"],
          :high [[1600 ["sid3"]]
                 [1200 ["sid3"]]
                 [1200 ["sid3"]]],
          :low nil}
         (test-case :texas case-11) ))

  (is (= {:overage [200 "sid0"],
          :high [[1600 ["sid0"]]
                 [1200 ["sid0"]]
                 [1200 ["sid0"]]],
          :low nil}
         (test-case :texas case-12) ))

  (is (= {:overage [200 "sid0"],
          :high [[1600 ["sid0" "sid1" "sid2" "sid3"]]
                 [1200 ["sid0" "sid1" "sid3"]]
                 [1200 ["sid0" "sid3"]]],
          :low nil}
         (test-case :texas case-13) ))

  (is (=  {:overage [200 "sid0"],
           :high [[4000 ["sid0" "sid3"]]],
           :low nil}
         (test-case :texas case-14) ))

  (is (= {:overage nil,
          :high [[2000 ["sid2"]]
                 [1600 ["sid1"]]
                 [1800 ["sid3"]]
                 [400 ["sid4"]]],
          :low nil}
         (test-case :texas case-15) ))

  (is (= {:overage nil,
          :high [[2000 ["sid0" "sid4"]]
                 [1600 ["sid0" "sid4"]]
                 [1800 ["sid0" "sid4"]]
                 [400 ["sid0" "sid4"]]],
          :low nil}
         (test-case :texas case-16) ))

  (is (= {:overage nil,
          :high
          [[2000 ["sid0" "sid1" "sid2" "sid4"]]
           [1600 ["sid0" "sid1" "sid4"]]
           [2200 ["sid0" "sid4"]]],
          :low nil}
         (test-case :texas case-17) ))

  (is (= {:overage nil,
          :high [[8000 ["sid3"]]],
          :low nil}
         (test-case :texas case-18) ))

  (is (= {:overage nil,
          :high [[8000000 ["sid3"]]],
          :low nil}
         (test-case :texas case-19) ))

  (is (= {:overage [200 "sid4"],
          :high [[2000 ["sid2"]]
                 [1600 ["sid1" "sid3"]]
                 [1800 ["sid3"]]
                 [400 ["sid0" "sid4"]]],
          :low nil}
         (test-case :texas case-20) )))

; case 21~40
(deftest test-omaha
  (is (= {:overage nil,
          :high [[1000 ["sid2"]]
                 [79200 ["sid3" "sid4"]]],
          :low nil} 
         (test-case :omaha case-21) ))

  (is (= {:overage [200 "sid4"],
          :high
          [[500 ["sid3"]]
           [1600 ["sid1" "sid2" "sid4"]]
           [900 ["sid1" "sid4"]]
           [800 ["sid4"]]],
          :low nil} 
         (test-case :omaha case-22) ))

  (is (= {:overage [400 "sid0"],
          :high [[1600 ["sid1"]]],
          :low nil} 
         (test-case :omaha case-23) ))

  (is (= {:overage [400 "sid0"],
          :high [[1600 ["sid0"]]],
          :low nil}
         (test-case :omaha case-24) ))

  (is (= {:overage [400 "sid0"],
          :high [[1600 ["sid0" "sid1"]]],
          :low nil} 
         (test-case :omaha case-25) ))

  (is (= {:overage [200 "sid0"],
          :high [[1200 ["sid2"]]
                 [800 ["sid1"]]],
          :low nil}
         (test-case :omaha case-26) ))

  (is (= {:overage [200 "sid0"],
          :high [[1200 ["sid1"]]
                 [800 ["sid1"]]],
          :low nil} 
         (test-case :omaha case-27) ))

  (is (= {:overage [400 "sid0"],
          :high [[1200 ["sid0"]]
                 [800 ["sid0"]]],
          :low nil} 
         (test-case :omaha case-28) ))

  (is (= {:overage [400 "sid0"],
          :high [[1200 ["sid0" "sid1" "sid2"]]
                 [800 ["sid0" "sid1"]]],
          :low nil} 
         (test-case :omaha case-29) ))

  (is (= {:overage [200 "sid0"],
          :high [[1600 ["sid2"]]
                 [1200 ["sid1"]]
                 [1200 ["sid3"]]],
          :low nil} 
         (test-case :omaha case-30) ))

  (is (= {:overage [200 "sid0"],
          :high [[1600 ["sid3"]]
                 [1200 ["sid3"]]
                 [1200 ["sid3"]]],
          :low nil}
         (test-case :omaha case-31) ))

  (is (= {:overage [200 "sid0"],
          :high [[1600 ["sid0"]]
                 [1200 ["sid0"]]
                 [1200 ["sid0"]]],
          :low nil} 
         (test-case :omaha case-32) ))

  (is (= {:overage [200 "sid0"],
          :high
          [[1600 ["sid0" "sid1" "sid2" "sid3"]]
           [1200 ["sid0" "sid1" "sid3"]]
           [1200 ["sid0" "sid3"]]],
          :low nil}
         (test-case :omaha case-33) ))

  (is (= {:overage [200 "sid0"],
          :high
          [[1600 ["sid0" "sid3"]]
           [1200 ["sid0" "sid3"]]
           [1200 ["sid0" "sid3"]]],
          :low nil} 
         (test-case :omaha case-34) ))

  (is (= {:overage nil,
          :high [[2000 ["sid2"]]
                 [1600 ["sid1"]]
                 [1800 ["sid3"]]
                 [400 ["sid4"]]],
          :low nil}
         (test-case :omaha case-35) ))

  (is (= {:overage nil,
          :high
          [[2000 ["sid0" "sid4"]]
           [1600 ["sid0" "sid4"]]
           [1800 ["sid0" "sid4"]]
           [400 ["sid0" "sid4"]]],
          :low nil} 
         (test-case :omaha case-36) ))

  (is (= {:overage nil,
          :high
          [[2000 ["sid0" "sid1" "sid2" "sid4"]]
           [1600 ["sid0" "sid1" "sid4"]]
           [2200 ["sid0" "sid4"]]],
          :low nil} 
         (test-case :omaha case-37) ))

  (is (= {:overage nil,
          :high [[8000 ["sid3"]]],
          :low nil}
         (test-case :omaha case-38) ))

  (is (= {:overage nil,
          :high [[8000000 ["sid3"]]],
          :low nil} 
         (test-case :omaha case-39) ))

  (is (= {:overage [200 "sid4"],
          :high
          [[2000 ["sid2"]]
           [1600 ["sid1" "sid3"]]
           [1800 ["sid3"]]
           [400 ["sid0" "sid4"]]],
          :low nil} 
         (test-case :omaha case-40) )))


;; case 41~
(deftest test-omahahl
  (is (= {:overage nil,
          :high [[1000 ["sid2"]]
                 [79200 ["sid3" "sid4"]]],
          :low [[1000 ["sid0"]]
                [79200 ["sid0"]]]}
         (test-case :omahahl case-41 :low) ))

  (is (= {:overage [200 "sid4"],
          :high [[500 ["sid4"]]
                 [1600 ["sid4"]]
                 [900 ["sid4"]]
                 [800 ["sid4"]]],
          :low [[500 ["sid0"]]
                [1600 ["sid0"]]
                [900 ["sid0"]]
                [800 ["sid0"]]]}
         (test-case :omahahl case-42 :low) ))

  (is (= {:overage [400 "sid0"],
          :high [[1600 ["sid1"]]],
          :low [[1600 ["sid0"]]]}
         (test-case :omahahl case-43 :low) ))

  (is (= {:overage [400 "sid0"],
          :high [[1600 ["sid0"]]],
          :low [[1600 ["sid0"]]]}
         (test-case :omahahl case-44 :low) ))

  (is (= {:overage [400 "sid0"],
          :high [[1600 ["sid0" "sid1"]]],
          :low [[1600 ["sid0" "sid1"]]]}
         (test-case :omahahl case-45 :low) ))

  (is (= {:overage [200 "sid0"],
          :high [[1200 ["sid2"]]
                 [800 ["sid1"]]],
          :low [[1200 ["sid2"]]
                [800 ["sid0"]]]}
         (test-case :omahahl case-46 :low) ))

  (is (= {:overage [200 "sid0"],
          :high [[1200 ["sid1"]]
                 [800 ["sid1"]]],
          :low [[1200 ["sid0"]]
                [800 ["sid0"]]]}
         (test-case :omahahl case-47 :low) ))

  (is (= {:overage [400 "sid0"],
          :high [[1200 ["sid0"]]
                 [800 ["sid0"]]],
          :low [[1200 ["sid0"]]
                [800 ["sid0"]]]}
         (test-case :omahahl case-48 :low) ))

  (is (= {:overage [400 "sid0"],
          :high [[1200 ["sid0" "sid1" "sid2"]]
                 [800 ["sid0" "sid1"]]],
          :low [[1200 ["sid0" "sid1" "sid2"]]
                [800 ["sid0" "sid1"]]]}
         (test-case :omahahl case-49 :low) ))

  (is (= {:overage [200 "sid0"],
          :high [[1600 ["sid2"]]
                 [1200 ["sid1"]]
                 [1200 ["sid3"]]],
          :low [[1600 ["sid0" "sid1" "sid2"]]
                [1200 ["sid0" "sid1"]]
                [1200 ["sid0"]]]}
         (test-case :omahahl case-50 :low) ))

  (is (= {:overage [200 "sid0"],
          :high [[1600 ["sid3"]]
                 [1200 ["sid3"]]
                 [1200 ["sid3"]]],
          :low [[1600 ["sid2"]]
                [1200 ["sid0" "sid1" "sid3"]]
                [1200 ["sid0" "sid3"]]]}
         (test-case :omahahl case-51 :low) ))

  (is (= {:overage [200 "sid0"],
          :high [[1600 ["sid0" "sid3"]]
                 [1200 ["sid0" "sid3"]]
                 [1200 ["sid0" "sid3"]]],
          :low [[1600 ["sid0" "sid1" "sid2" "sid3"]]
                [1200 ["sid0" "sid1" "sid3"]]
                [1200 ["sid0" "sid3"]]]}
         (test-case :omahahl case-54 :low) ))

  (is (= {:overage nil,
          :high [[2000 ["sid2"]]
                 [1600 ["sid1"]]
                 [1800 ["sid3"]]
                 [400 ["sid4"]]],
          :low [[2000 ["sid0"]]
                [1600 ["sid0"]]
                [1800 ["sid0"]]
                [400 ["sid0"]]]}
         (test-case :omahahl case-55 :low) ))

  (is (= {:overage nil,
          :high [[2000 ["sid0" "sid1" "sid2" "sid4"]]
                 [1600 ["sid0" "sid1" "sid4"]]
                 [1800 ["sid0" "sid4"]]
                 [400 ["sid0" "sid4"]]],
          :low [[2000 ["sid3"]]
                [1600 ["sid3"]]
                [1800 ["sid3"]]
                [400 nil]]}
         (test-case :omahahl case-57 :low) ))

  (is (= {:overage nil,
          :high [[8000 ["sid3"]]],
          :low [[8000 ["sid3"]]]}
         (test-case :omahahl case-58 :low) ))

  (is (= {:overage [200 "sid4"],
          :high [[2000 ["sid2"]]
                 [1600 ["sid1" "sid3"]]
                 [1800 ["sid3"]]
                 [400 ["sid0" "sid4"]]],
          :low [[2000 ["sid2"]]
                [1600 nil]
                [1800 nil]
                [400 nil]]}
         (test-case :omahahl case-60 :low) )))


