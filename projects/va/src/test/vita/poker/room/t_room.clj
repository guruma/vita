(ns vita.poker.room.t-room
  #_(:use [midje.sweet]
        [vita.poker.room.room] ))

(require '[philos.debug :refer :all])

#_(against-background [(around :contents (let [room (create-room "first-room" :texas 100 nil)] ?form))]
  (facts "create-room"
    (:room-id room) => "first-room"
    (:game-name room) => :texas
    (:sb room) => 100
    (:bb room) => 200)

  (facts "join-user"
    (dosync
      (join-user room "my-social-id" "Bill Clinton" "https://apps.facebook.com/image/my-picture.png" "12345678" true))
    (let [users @(:users room)
          user (users "my-social-id")]
      (:name user) => "Bill Clinton" )))
