(ns vita.poker.room.t-user
  #_(:use midje.sweet
        vita.poker.room.user)
  #_(:require [vita.poker.room.room :as room :reload-all true]))

(require '[philos.debug :refer :all])

#_(fact "creates new user"
  (def room* (room/create-room "aaa" :texas 100 nil))
  (dosync
    (room/join-user room* "12345" "Michael Jackson" "https://abc/def/png" 456789 false))
  (:name (@(:users room*) "12345")) => "Michael Jackson")
