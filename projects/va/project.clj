(defproject va "0.1.0-SNAPSHOT"

  :description "Vita App Server"
  :min-lein-version "2.3.4"

  :dependencies
  [[org.clojure/clojure "1.6.0"]
   [org.clojure/math.combinatorics "0.0.8"]
   [org.clojure/core.async "0.1.338.0-5c5012-alpha"]
   [environ "0.5.0"]

   [org.clojure/data.generators "0.1.2"]

   ;; facebook용.
   ;; TODO(kep) 종속성제거.
   [lib-noir "0.8.4"]
   [hiccup "1.0.5"]
   [uri "1.1.0"]
   [clj-oauth2 "0.2.0"]
   [clj-http "0.9.2"]
   [pandect "0.3.4"]
   [org.clojure/data.json "0.2.5"]

   [ring/ring-jetty-adapter "1.3.0"]
   [clj-time "0.8.0"]

   ;; Encoding.
   [org.clojure/tools.reader "0.8.7"]

   ;; Log.
   [org.clojure/tools.logging "0.3.0"]
   [org.clojure/tools.cli "0.3.1"]
   [org.slf4j/slf4j-log4j12 "1.7.7"]
   [clj-logging-config "1.9.10"]
   [log4j/apache-log4j-extras "1.2.17"]
   [log4j/log4j "1.2.17" :exclusions [javax.mail/mail
                                      javax.jms/jms
                                      com.sun.jdmk/jmxtools
                                      com.sun.jmx/jmxri]]
   ;; for unit test
   [pjstadig/humane-test-output "0.6.0"]

   ;; nrepl
   [org.clojure/tools.nrepl "0.2.3"]
   [cider/cider-nrepl "0.8.0-SNAPSHOT"]

   ;; App Server.
   [ruiyun/tools.timer "1.0.1"]
   [prismatic/schema "0.2.4"]
   [slingshot "0.10.3"]
   [http-kit "2.1.18"]
   [ns-tracker "0.2.2"]


   ;; DB.
   [paddleguru/clutch "0.4.0"]
   [com.cemerick/url "0.1.1"]]


  :plugins
  [[lein-environ "0.5.0"]
   [cider/cider-nrepl "0.8.0-SNAPSHOT"]
   ;; for unit test
   [quickie "0.2.5"]]


  ;; paths.
  :source-paths ["src/main"]
  :test-paths ["src/test"]

  :main vita.core

  :profiles
  {:dev
   {:env {:va-config "../../dist/config/va.edn"}
    :dependencies [[org.clojure/test.check "0.5.8"]]
    :plugins [[jonase/eastwood "0.1.4"]]
    :jvm-opts ["-XX:-OmitStackTraceInFastThrow"]}


   :proxy
   {:env {:va-config "../../dist/config/proxy-va.edn"}
    :jvm-opts ["-XX:-OmitStackTraceInFastThrow"]}

   :qa
   {:env {:va-config "../../dist/config/qa1-va.edn"}
    :jvm-opts ["-XX:-OmitStackTraceInFastThrow"]}

   :live
   {:env {:va-config "../../dist/config/live1-va.edn"}}

   }

  :aliases
  {"proxy-va" ["with-profile" "proxy" "run"]}
  )
