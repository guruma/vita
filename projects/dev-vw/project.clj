(defproject dev-vw "0.1.0-SNAPSHOT"

  :min-lein-version "2.3.4"

  :dependencies [[org.clojure/clojure "1.6.0"]
                 [compojure "1.1.8"]]

  :plugins [[lein-ring "0.8.10"]]

  ;; paths.
  :source-paths ["src/main"]
  :resource-paths ["resources" "../vw/resources"]
  :clean-targets [:target-path "resources/public/js/game/"]

  :profiles
  {:dev
   {:ring {:handler dev-vw.handler/app
           :ssl? true
           :stacktraces? false
           :auto-reload? false
           :ssl-port 443
           :keystore "../../dist/certs/sonaclo.keystore"
           :key-password "gasanfreenote"
           :want-client-auth true
           :need-client-auth true
           }}

   :test-server
   {:ring {:handler dev-vw.handler/app
           :stacktraces? true
           :auto-reload? true

           :nrepl {:start? true}
           :port 3000
           }
    }
   }
  )
