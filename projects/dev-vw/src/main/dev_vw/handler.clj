(ns dev-vw.handler
  (:require
   [compojure.core :refer :all]
   [compojure.handler :as handler]
   [compojure.route :as route]
   [ring.util.response :as resp]
   [clojure.java.io :as io]))

(def login-page (io/resource "public/login.html"))
(def main-page (io/resource "public/main.html"))


(defn post-login [{:keys [params]}]
  (let [{:keys [id pass]} params]
    (resp/redirect (str "/vita-login/main.html" "?user-id=" id))))


(defroutes app-routes
  (GET "/" [] login-page)
  (POST "/login" req (post-login req))

  (POST "/vita-login/" [] main-page)
  (route/resources "/vita-login/")
  (route/not-found "Page not found"))


(def app
  (handler/site app-routes))
