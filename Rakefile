VITA_HOME = `git rev-parse --show-toplevel`.strip
CLI_HOME = "#{VITA_HOME}/projects/vc"
WEB_HOME = "#{VITA_HOME}/projects/vw"
DEV_WEB_HOME = "#{VITA_HOME}/projects/dev-vw"
APP_HOME = "#{VITA_HOME}/projects/va"
SHA_HOME = "#{VITA_HOME}/projects/vs"


JENKINS_SERVER_BRANCH = "master"
DEV_SERVER_BRANCH = "master"
QA_SERVER_BRANCH = "master"
# QA_SERVER_BRANCH = "http-kit"
LIVE1_SERVER_BRANCH = "live1"


task :default do
    sh "rake -T"
end


desc "[dev] Web Server"
task :dev_web do
    # ENSURE tmux server kill
    sh "tmux kill-server || true"

    # GIT.
    sh "git checkout -f #{DEV_SERVER_BRANCH}"
    sh "git clean -d -x -f"
    sh "git remote prune origin"
    sh "git pull"

    # cleanup
    [CLI_HOME, DEV_WEB_HOME, APP_HOME].each do |dir|
        Dir.chdir(dir) do
            sh "lein with-profile dev clean"
        end
    end

    # generate shared code
    Dir.chdir(SHA_HOME) do
        sh "lein cljx once"
    end

    # client build
    Dir.chdir(CLI_HOME) do
        sh "lein with-profile dev cljsbuild once"
    end

    # test-server launch
    sh "tmux new -d"
    sh "tmux new-window && tmux send-key -t 1 'cd #{DEV_WEB_HOME} && lein with-profile dev ring server-headless' Enter"
    sh "tmux new-window && tmux send-key -t 2 'cd #{APP_HOME} && lein with-profile dev run' Enter"
end


desc "[dev] App Server"
task :dev_app do
    # ENSURE tmux server kill
    sh "tmux kill-server || true"

    # GIT.
    sh "git checkout -f #{DEV_SERVER_BRANCH}"
    sh "git clean -d -x -f"
    sh "git remote prune origin"
    sh "git pull"

    # cleanup
    [CLI_HOME, WEB_HOME, APP_HOME].each do |dir|
        Dir.chdir(dir) do
            sh "lein with-profile dev clean"
        end
    end

    # generate shared code
    Dir.chdir(SHA_HOME) do
        sh "lein cljx once"
    end

    # launch
    sh "tmux new -d"
    sh "tmux new-window && tmux send-key -t 2 'cd #{APP_HOME} && lein with-profile dev run' Enter"
end


desc "[qa] Web Server"
task :qa_web do
    # ENSURE tmux server kill
    sh "tmux kill-server || true"

    # GIT.
    sh "git checkout -f #{QA_SERVER_BRANCH}"
    sh "git clean -d -x -f"
    sh "git remote prune origin"
    sh "git pull"

    # cleanup
    [CLI_HOME, WEB_HOME, APP_HOME].each do |dir|
        Dir.chdir(dir) do
            sh "lein with-profile qa clean"
        end
    end

    # generate shared code
    Dir.chdir(SHA_HOME) do
        sh "lein cljx once"
    end

    # client build
    Dir.chdir(CLI_HOME) do
        sh "lein with-profile qa cljsbuild once"
    end

    # test-server launch
    sh "tmux new -d"
    sh "tmux new-window && tmux send-key -t 1 'cd #{WEB_HOME} && lein with-profile qa ring server-headless' Enter"
end


desc "[qa] App Server"
task :qa_app do
    # ENSURE tmux server kill
    sh "tmux kill-server || true"

    # GIT.
    sh "git checkout -f #{QA_SERVER_BRANCH}"
    sh "git clean -d -x -f"
    sh "git remote prune origin"
    sh "git pull"

    # cleanup
    [CLI_HOME, WEB_HOME, APP_HOME].each do |dir|
        Dir.chdir(dir) do
            sh "lein with-profile qa clean"
        end
    end

    # generate shared code
    Dir.chdir(SHA_HOME) do
        sh "lein cljx once"
    end

    # apply log4j.
    cp "#{VITA_HOME}/dist/log4j.properties", "#{APP_HOME}/resources/log4j.properties"

    # launch
    sh "tmux new -d"
    sh "tmux new-window && tmux send-key -t 2 'cd #{APP_HOME} && lein with-profile qa run' Enter"
end


desc "[live] Web Server"
task :live_web do
    # ENSURE tmux server kill
    sh "tmux kill-server || true"

    # GIT.
    sh "git checkout -f #{LIVE1_SERVER_BRANCH}"
    sh "git clean -d -x -f"
    sh "git remote prune origin"
    sh "git pull"

    # cleanup
    [CLI_HOME, WEB_HOME, APP_HOME].each do |dir|
        Dir.chdir(dir) do
            sh "lein with-profile live clean"
        end
    end

    # generate shared code
    Dir.chdir(SHA_HOME) do
        sh "lein cljx once"
    end

    # client build
    Dir.chdir(CLI_HOME) do
        sh "lein with-profile live cljsbuild once"
    end

    # launch
    sh "tmux new -d"
    sh "tmux new-window && tmux send-key -t 1 'cd #{WEB_HOME} && lein with-profile live ring server-headless' Enter"
end


desc "[live] App Server"
task :live_app do
    # ENSURE tmux server kill
    sh "tmux kill-server || true"

    # GIT.
    sh "git checkout -f #{LIVE1_SERVER_BRANCH}"
    sh "git clean -d -x -f"
    sh "git remote prune origin"
    sh "git pull"

    # cleanup
    [CLI_HOME, WEB_HOME, APP_HOME].each do |dir|
        Dir.chdir(dir) do
            sh "lein with-profile live clean"
        end
    end

    # generate shared code
    Dir.chdir(SHA_HOME) do
        sh "lein cljx once"
    end

    # apply log4j.
    cp "#{VITA_HOME}/dist/log4j.properties", "#{APP_HOME}/resources/log4j.properties"

    # launch
    sh "tmux new -d"
    sh "tmux new-window && tmux send-key -t 2 'cd #{APP_HOME} && lein with-profile live run' Enter"
end


desc "[ETC] test all"
task :tester do
    # GIT.
    sh "git checkout -f #{JENKINS_SERVER_BRANCH}"
    sh "git clean -d -x -f"
    sh "git remote prune origin"
    sh "git pull"

    # generate shared code
    Dir.chdir(SHA_HOME) do
        sh "lein cljx once"
    end

    # App server Test.
    Dir.chdir(APP_HOME) do
        sh "lein do check, eastwood, test"
    end

    # Client Test.
    # Dir.chdir(CLI_HOME) do
    #     sh "lein with-profile test cljsbuild test unit-tests"
    # end
end


desc "[ETC] update documentation"
task :update_doc do
    # GIT.
    # sh "git checkout -f #{JENKINS_SERVER_BRANCH}"
    # sh "git clean -d -x -f"
    # sh "git remote prune origin"
    # sh "git pull"

    # sh "kill `ps aux | grep 'main doxer' | sed '/grep/d' | awk '{print $2}'` | true"
    # sh "BUILD_ID=dontKillMe lein with-profile doxer serve &"
end
