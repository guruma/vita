(defproject dev-vw "0.1.0-SNAPSHOT"

  :min-lein-version "2.3.4"

  :dependencies [[org.clojure/clojure "1.6.0"]
                 [org.clojure/math.combinatorics "0.0.7"]

                 ;; Web Server.
                 [compojure "1.1.6"]
                 [ring/ring-jetty-adapter "1.2.2"]
                 [lib-noir "0.8.3"]
                 [hiccup "1.0.5"]
                 [uri "1.1.0"]
                 [clj-time "0.7.0"]
                 [clj-oauth2 "0.2.0"]
                 [clj-http "0.9.1"]

                 ;; Encoding.
                 ;; [org.clojure/tools.reader "0.8.4"]
                 [pandect "0.3.2"]      ; checksum.
                 [fogus/ring-edn "0.2.0"]
                 [org.clojure/data.json "0.2.4"]
                 [cheshire "5.3.1"]

                 ;; Log.
                 [org.clojure/tools.logging "0.2.6"]
                 [org.slf4j/slf4j-log4j12 "1.7.7"]
                 [clj-logging-config "1.9.10"]
                 [log4j/apache-log4j-extras "1.2.17"]
                 [log4j/log4j "1.2.17" :exclusions [javax.mail/mail
                                                    javax.jms/jms
                                                    com.sun.jdmk/jmxtools
                                                    com.sun.jmx/jmxri]]

                 ;; DB.
                 [de.ubercode.clostache/clostache "1.3.1"]
                 [com.cemerick/url "0.1.1"]
                 ]

  :plugins [[lein-ring "0.8.10"]

            ;; for util.
            [lein-pdo "0.1.1"]
            [lein-libdir "0.1.1"]
            [lein-ancient "0.5.5"]

            ;; for flagger.
            [slothcfg "1.0.1"]

            ;; lint.
            [jonase/eastwood "0.1.2"]
            ]


  ;; paths.
  :source-paths ["src/main"]
  :test-paths ["src/test"]
  :resource-paths ["resources"]
  :clean-targets [:target-path "resources/public/js/game/"]
  :libdir-path "target/lib/"


  ;; for server.

  :profiles {:dev {:dependencies [[org.clojure/test.check "0.5.8"]]

                   :flag {:debug true
                          :level :internal
                          }

                   :ring {:handler vw.handler/app
                          :stacktraces? true
                          :auto-reload? true

                          :nrepl {:start? true}
                          :port 3000
                          }
                   }

             :test-server {:flag {:debug true
                                  :level :test-server
                                  }

                           :ring {:handler vw.handler/app
                                  :stacktraces? true
                                  :auto-reload? true

                                  :nrepl {:start? true}
                                  :port 3000
                                  }
                           }

             :amazon {:flag {:debug true
                             :level :amazon
                             }
                      :ring {:handler vw.handler/app
                             :nrepl {:start? true}
                             :ssl? true
                             :stacktraces? false
                             :auto-reload? false
                             :ssl-port 443
                             :keystore "../dist/certs/sonaclo.keystore"
                             :key-password "gasanfreenote"
                             :want-client-auth true
                             :need-client-auth true
                             }
                      }
             }
  )
