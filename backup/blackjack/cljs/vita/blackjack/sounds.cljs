(ns vita.blackjack.sounds
  "load sounds for game"
  (:require [media.sound :as snd]
            [philos.cljs.debug :refer-macros [dbg clog cl break]]))

(def sc* (snd/sound-context "sound/" ".mp3"))

(snd/def-sound sc* :click "common-click")
(snd/def-sound  sc* :blackjack "blackjack-blackjack")
(snd/def-sound  sc* :bet-please "blackjack-bet-please")
(snd/def-sound  sc* :bust "blackjack-bust")
(snd/def-sound  sc* :card-deal "common-card-deal")
(snd/def-sound  sc* :card-clean "common-card-clean")
(snd/def-sound  sc* :chip-drop "common-chip-drop")
(snd/def-sound  sc* :chip-stack "common-chip-stack")
(snd/def-sound  sc* :chip-return "common-chip-return")
(snd/def-sound  sc* :background1 "common-background1" {:loop true})
(snd/def-sound  sc* :background2 "common-background2" {:loop true})
(snd/def-sound  sc* :background3 "common-background3" {:loop true})

(defn background-music [play?]
  ((if play? snd/play snd/pause) sc* :background3))

(defn play [kwd]
  (snd/play sc* kwd))

(defn pause [kwd]
  (snd/pause sc* kwd))

(defn reset [kwd]
  (snd/reset sc* kwd))
