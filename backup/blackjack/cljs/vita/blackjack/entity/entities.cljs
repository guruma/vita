(ns vita.blackjack.entity.entities
  "entities"
  (:require [monet.timer :as mt]
            [vita.blackjack.entity.background :as background]
            [vita.blackjack.entity.user :as user]
            [vita.blackjack.entity.chip :as chip]
            [vita.blackjack.entity.card :as card]
            [vita.blackjack.entity.menu :as menu]
            [vita.blackjack.entity.panel :as panel]))



(def entities
  (concat [mt/timer]
          background/entities
          user/entities
          card/entities
          chip/entities ; 칩이 카드 위에 그려져야 한다.
          menu/entities
          panel/entities))
