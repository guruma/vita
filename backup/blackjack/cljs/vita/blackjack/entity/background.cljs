(ns vita.blackjack.entity.background
  "background"
  (:require [monet.canvas :as mc]
            [vita.blackjack.images :as image]))

(def entities [

   [:background
    {:img image/background :x 0 :y 0}
    nil ; update funtion
    (fn [ctx {:keys [img x y]}]
      (mc/draw-image ctx img x y ))]

])
