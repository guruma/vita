(ns vita.blackjack.entity.menu
  "menu"
  (:use-macros [philos.cljs.debug :only [clog break]])
  (:require [monet.canvas :as mc]
            [vita.blackjack.events :as event]
            [vita.blackjack.util :refer [dispatch state!]]
            [vita.blackjack.sounds]
            [vita.blackjack.sounds :as sound]
            [vita.blackjack.images :as image]))



(defn state->index [state]
  ({:norm 0 :over 1 :checked 1 :down 2 :disable 3} state))

(defn index-of-image [{:keys [state pressed]}]
  (state->index (if pressed :down state)))

(defn on-click [key {:keys [pressed] :as me}]
  (case key
    :btn-full-screen      nil
    :btn-background-music (if (not= nil pressed) (sound/background-music pressed))
    :btn-sound            nil
    :btn-info             nil
    nil))

(defn update-btn-by-mouse [key {:keys [state pressed x y w h] :as me}]
  (let [clicked? (event/click-in? me)
        state'  (cond
                 clicked? (do (on-click key me) state)
                 (and (= state :norm) (event/mouse-move-in? me))  :over
                 (and (= state :over) (event/mouse-move-out? me)) :norm
                 (and (= state :over) (event/mouse-down-in? me))  :down
                 (and (= state :down) (event/mouse-up-in?  me))   :over
                 (and (= state :down) (event/mouse-up-out? me))   :norm
                 :else state)
        pressed' (if (not= nil pressed)
                   (if clicked? (not pressed) pressed))]
    (assoc me :state state' :pressed pressed')))

(defn draw-btn [ctx {:keys [state pressed img x y w h] :as me}]
  (mc/draw-image ctx img (* w (index-of-image me)) 0 w h  x y w h))

(defn draw-text [ctx {:keys [txt x y] :as me}]
  (mc/draw-text ctx txt [x y]))

(def psk810-photo "https://fbcdn-profile-a.akamaihd.net/hprofile-ak-ash2/t1/c0.0.50.50/p50x50/1175664_10151919653717985_932849323_s.jpg")
(def anonymous-photo "https://fbcdn-profile-a.akamaihd.net/static-ak/rsrc.php/v2/yo/r/UlIqmHJn-SK.gif")
(def guruma-photo "https://fbcdn-profile-a.akamaihd.net/hprofile-ak-frc3/t1/c5.0.50.50/p50x50/1463505_1430269207203633_553985659_s.jpg")
(def photo guruma-photo)

(defn draw-photo [ctx {:keys [img x y]}]
  (mc/draw-image ctx img x y))

(def entities [

   [:btn-full-screen
    {:state :norm :img image/btn-window-bigger :img2 image/btn-window-smaller :x 550 :y 18 :w 32 :h 32}
    update-btn-by-mouse
    draw-btn]

   [:btn-background-music
    {:state :norm :img image/btn-background-music :pressed false :x 582 :y 18 :w 32 :h 32}
    update-btn-by-mouse
    draw-btn]

   [:btn-sound
    {:state :norm :img image/btn-sound :pressed false :x 614 :y 18 :w 32 :h 32}
    update-btn-by-mouse
    draw-btn]

   [:btn-info
    {:state :norm :img image/btn-info :x 646 :y 18 :w 32 :h 32}
    update-btn-by-mouse
    draw-btn]

   [:btn-buy-chip
    {:state :norm :img (:image-93 image/images*) :x 200 :y 10 :w 111 :h 46}
    update-btn-by-mouse
    draw-btn]

   [:user-name
    {:state :norm :txt "Hong Kil Dong" :x 100 :y 23 :w 111 :h 46}
    nil
    draw-text]

   [:my-money
    {:state :norm :txt "3,342,597" :x 130 :y 46 :w 111 :h 46}
    nil
    draw-text]

   [:photo
    {:state :norm  :img (image/load-image photo) :x 5 :y 5 :w 111 :h 46}
    nil
    draw-photo]

])
