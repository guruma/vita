(ns vita.blackjack.entity.chip
  "chip"
  (:require [monet.canvas :as mc]
            [monet.timer :as mt]
            [philos.cljs.debug :refer-macros [clog cl break dbg]]
            [vita.blackjack.images :as image :refer [images*]]
            [vita.blackjack.room :as r
             :refer [->coord ->offset coord+offset users-in move-delta-until keyword+
                     calc-speeds]]))



(def chips {:pink 1000 :blue 5000 :black 10000 :green 50000
            :orange 100000 :red 500000 :brown 1000000 :violet 5000000})


(def chip->img {:orange :image-1 :red :image-3 :blue :image-4 :pink
                :image-37 :green :image-19 :black :image-18
                :violet :image-81 :brown :image-83 })


; 36000 -> {:black 3 :blue 1 :pink 1}
(defn money->chips [amount]
  (loop [amount amount [[chip unit] & r] (sort-by second > chips) ret {}]
    (if chip
      (if (= 0 (quot amount unit))
        (recur amount r ret)
        (recur (rem amount unit) r (assoc ret chip (quot amount unit))))
      ret)))


; {:black 3 :blue 1 :pink 1} -> (:black :black :black :blue :pink)
(defn enumerate-chips [chips]
  (apply concat
    (map (fn [[c n]] (take n (repeat c))) chips)))


(defn update-chip
  [me state seat deck-kwd deck-cnt]
  (clog [state (:x me) (:dst-x me)] "update-chip" :o)
  (if (= :stop state)
    me
    (let [{:keys [a x y dst-a dst-x dst-y a-speed x-speed y-speed] :as moving-data}
                 (me (keyword+ seat deck-kwd))]
      (if (and (= x dst-x) (= y dst-y)) ; arrived?
        (do
          (if (= :double-down state)
            (r/set-double-down! seat deck-kwd)
            (r/set-betting-money! seat deck-kwd))   ; set betting-money to a user of seat.
          (dissoc me (keyword+ seat deck-kwd)))  ; me에서 에니메이션 드로잉 정보 제거.
        (let [x' (move-delta-until x dst-x (mt/dt) x-speed)
              y' (move-delta-until y dst-y (mt/dt) y-speed)
              a' (move-delta-until a dst-a (mt/dt) a-speed)]
          (assoc me (keyword+ seat deck-kwd) (assoc moving-data :x x' :y y' :a a')))))))

(defn chip-coord [seat-kwd deck-kwd deck-cnt offset]
 (-> (coord+offset seat-kwd offset)
     (r/+deck-offset deck-kwd deck-cnt)))

(defn start-chip-updating
  [me money state seat-kwd deck-kwd deck-cnt]
  (let [lapse-time (r/lapse-time* :chip)
        from (chip-coord seat-kwd deck-kwd deck-cnt :chip-throwing-from)
        to (chip-coord seat-kwd deck-kwd deck-cnt :chip)
        box (->coord :chip-box)
        [org-x org-y] (if (#{:betting :double-down} state) from to)
        [dst-x dst-y] ({:betting to :stop to :double-down to :lost box :won from } state)
        [org-a dst-a] [1.0 (if (= :lost state) 0.0 1.0)]
        [x-speed y-speed a-speed] (calc-speeds [dst-x dst-y dst-a] [org-x org-y org-a] lapse-time)]
    (clog [state org-y dst-y] "start-chip-updating" :o)
    (assoc me (keyword+ seat-kwd deck-kwd)
      {:seat-kwd seat-kwd :deck-cnt deck-cnt :deck-kwd deck-kwd :money money :x org-x :y org-y :a org-a :dst-x dst-x :dst-y dst-y :dst-a dst-a
       :x-speed x-speed :y-speed y-speed :a-speed a-speed :state state})))


(defn start-and-update-chip
  [state me [money seat deck-kwd deck-cnt]]
  (let [chip-me (me (keyword+ seat deck-kwd))]
    (clog [state money chip-me] "start-and-update-chip" :o)
    (if chip-me
      (if (and (= :stop (:state chip-me)) (not= :stop state))
        (start-chip-updating me money state seat deck-kwd deck-cnt)
        (update-chip me state seat deck-kwd deck-cnt))
      (start-chip-updating me money state seat deck-kwd deck-cnt))))


(defn update-chips-of-all [key me]
  (let [all (keep #(if (seq (second %)) %)
                  [[:stop    (r/money&seat-in @r/room*)]
                   [:betting (r/new-bets&seat-in @r/room*)]
                   [:lost    (r/lost-money&seat-in @r/room*)]
                   [:won     (r/won-money&seat-in @r/room*)]])]
    (if (< 1 (count all))
      (do (clog all "ERROR: chip multiple pred trues! check betting?, betted? win? losd?" :o)
           me)
      (let [[state lst] (first all)]
        (clog [state lst (r/users-in @r/room*)] "update-chips-of-all":o)
        (reduce (partial start-and-update-chip state) me lst)))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; update functions for double down animation.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defn update-chips-of-double-down [key me]
  (let [all (keep #(if (seq (second %)) %)
                   [[:double-down (r/double-down&seat-in @r/room*)]])]
    (let [[state lst] (first all)]
      (clog [all] "double-down" :o)
      (reduce (partial start-and-update-chip state) me lst))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; update functions for insurance animation
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defn start-chip-updating-insurance
  [me money state seat-kwd deck-kwd deck-cnt]
  (let [lapse-time (r/lapse-time* :chip)
        from (chip-coord seat-kwd deck-kwd deck-cnt :chip)
        ;to (chip-coord seat-kwd deck-kwd deck-cnt :chip)
        to (->coord (keyword (str "chip-insurance" (r/seat->num seat-kwd))))
        box (->coord :chip-box)
        [org-x org-y] (if (#{:betting :double-down} state) from to)
        [dst-x dst-y] ({:betting to :stop to :double-down to :lost box :won from } state)
        [org-a dst-a] [1.0 (if (= :lost state) 0.0 1.0)]
        [x-speed y-speed a-speed] (calc-speeds [dst-x dst-y dst-a] [org-x org-y org-a] lapse-time)]
    (clog [state org-y dst-y] "start-chip-updating" :o)
    (assoc me (keyword+ seat-kwd deck-kwd)
      {:seat-kwd seat-kwd :deck-cnt deck-cnt :deck-kwd deck-kwd :money money :x org-x :y org-y :a org-a :dst-x dst-x :dst-y dst-y :dst-a dst-a
       :x-speed x-speed :y-speed y-speed :a-speed a-speed :state state})))

(defn start-and-update-chip-insurance
  [state me [money seat deck-kwd deck-cnt]]
  (let [chip-me (me (keyword+ seat deck-kwd))]
    (clog [state money chip-me] "start-and-update-chip-insurance" :o)
    (if chip-me
      (if (and (= :stop (:state chip-me)) (not= :stop state))
        (start-chip-updating-insurance me money state seat deck-kwd deck-cnt)
        (update-chip me state seat deck-kwd deck-cnt))
      (start-chip-updating-insurance me money state seat deck-kwd deck-cnt))))

(defn update-chips-of-insurance [key me]
  (let [all (keep #(if (seq (second %)) %)
                   [[:insurance (r/insurance&seat-in @r/room*)]])]
    (let [[state lst] (first all)]
      (clog [all] "insurance" :o)
      (reduce (partial start-and-update-chip-insurance state) me lst))))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; draw functions
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defn draw-chips
  [ctx a x y money]
  (let [chips (money->chips money)
        chips (enumerate-chips chips)
        a (if (< 0.3 a) 1.0 (* 2 a))] ; hack(guruma). temporary easing effect to alpha.
    (doseq [[i chip] (map-indexed #(vector %1 %2) chips)]
      (-> ctx
        (mc/alpha a)
        (mc/draw-image ((chip->img chip) images*) (- x (* i 3)) (- y (* i 3)))
        (mc/alpha 1.0)))))


(defn draw-chips-of-all
  [ctx {:keys [x y] :as me}]
  (clog (:state me) "draw-chips-of-all" :o)
  (doseq [[_ {:keys [a x y money state seat-kwd deck-kwd deck-cnt]}] (r/select-keys-starting me :seat)]
    (let [money (r/betting-money-by seat-kwd deck-kwd)]
      (draw-chips ctx a x y money)
      (if (= :stop state)
        (let [[x y] (-> (coord+offset seat-kwd :betting-money)
                        (r/+deck-offset deck-kwd deck-cnt))]
        (mc/draw-text ctx (str "$" money) [x y] "10pt Arial"))))))


(def entities [

   ; 블랙잭 게임 칩 이동 에니메이션 : 베팅시 칩 던지기, 이길 때 칩 가져오기, 질 때 칩 딜러에게 주기.
   [:chip
    {}
    update-chips-of-all
    draw-chips-of-all]

   [:chip-double-down
    {}
    update-chips-of-double-down
    draw-chips-of-all]

   #_[:chip-insurance
    {}
    update-chips-of-insurance
    draw-chips-of-all]

])
