(ns vita.blackjack.entity.user
  "room"
  (:use-macros [philos.cljs.debug :only [clog]])
  (:require [monet.canvas :as mc]
            [monet.timer :as mt]
            [vita.blackjack.util :refer [state? get-entity]]
            [vita.blackjack.room :as r
             :refer [->coord ->size coord+offset keyword+ add-offset users-in get-next-timer-user]]
            [vita.blackjack.events :as event]
            [vita.blackjack.images :as image]
            [vita.blackjack.shared.cards :as card]
            [vita.blackjack.net :as net]
            [goog.string :as gstring]
            [goog.string.format :as gformat]
            [vita.blackjack.entity.panel :as panel]))


(def ^:private images* image/images)
(def ->image-count {:card-back 9 :timer 16})

(defn cycle-time [acc-t unit bound]
  (rem (quot acc-t unit) bound))


(defn next-seat-image-by [img state t]
  (if (= state :norm)
      (cycle-time t 1500 2)
      img))

(defn on-click-seat [me i]
  (net/send :seat {:seat i}))

(defn update-seat [{:keys [img t state] :as me} seat]
  (let [state'
          (cond
            (event/click-in? me) (do (on-click-seat me seat) state)
            (and (= state :norm) (event/mouse-move-in? me))  :over
            (and (= state :over) (event/mouse-move-out? me)) :norm
            (and (= state :over) (event/mouse-down-in? me))  :down
            (and (= state :down) (event/mouse-up-in?  me))   :over
            (and (= state :down) (event/mouse-up-out? me))   :norm
            :else state)
        t' (+ t (mt/dt))
        img (next-seat-image-by img state' t')]
    (assoc me :t t' :state state' :img img)))

(defn- toggle-seat [me seat]
  (update-in me [(keyword+ :seat seat)] update-seat seat))


(let [seat-images [(:image-76 images*) (:image-92 images*)]
      [w h] (->size :seat)]
  (defn draw-seat [ctx me {:keys [seat]}]
    (let [{:keys [state img x y]} ((keyword+ :seat seat) me)
          ox (* w ({:norm 0 :over 1 :down 2} state))]
      (mc/draw-image ctx (seat-images img) ox 0 w h  x y w h))))


(let [frame (:image-87 images*)
      state-of (fn [user]
                  (if (= (:state user) :none) ""
                    (name (:state user))))
      get-name (fn [user]
                 (.substr (:name user "no name") 0 8))]
  (defn draw-user [ctx me {:keys [seat] :as user}]
    (let [[x y] (->coord (keyword+ :seat seat))]
    (-> ctx
      (mc/draw-image-at frame (add-offset x y :frame))
      (mc/draw-image-at image/photo (add-offset x y :photo) (->size :photo))
      (mc/draw-text (r/get-user-state-str-for-display user) (add-offset x y :state))
      (mc/draw-text (get-name user) (add-offset x y :name))
      (mc/draw-text (str (:betting-money-result user 0)) (add-offset x y :result))))))


;;;;;;;;;;;;;;;;;;;;;;;;;;
; timer
;;;;;;;;;;;;;;;;;;;;;;;;;;


(defn update-timer [key {:keys [state t k] :as me}]
  (let [user (get-next-timer-user @r/room*)]
    (if-let [seat-num (:seat user)]
      (let [t (+ t (mt/dt))
            k (quot t 1000)]
        ;(clog [t k] :o)
        (if (< k (->image-count :timer)) ; time over
          (assoc me :state :drawing :t t :k k :seat (keyword+ :seat seat-num))
          (do
            (net/send :stand {:split-index (r/active-deck-keyword-of user)})
            (assoc me :state :none))))
      (assoc me :state :none :t 0 :k 0))))


(defn draw-timer [ctx {:keys [state img x y w h k seat] :as me}]
  ;(clog [k state] :o)
  (if (= :drawing state)
    (let [[x0 y0] (coord+offset seat :timer)
          [x1 y1] (coord+offset seat :timer-text)]
      (mc/draw-image ctx img (* w k) 0 w h x0 y0 w h)
      (mc/draw-text ctx (gstring/format "%2d" (- (->image-count :timer) (inc k))) [x1 y1]))))


(defn seat-properties [seat]
  (let [[x y] (->coord seat)
        [w h] (->size :seat)]
    {:state :norm :img 0 :t 0 :x x :y y :w w :h h}))


(def entities [

   [:users
    {:seat0 (seat-properties :seat0)
     :seat1 (seat-properties :seat1)
     :seat2 (seat-properties :seat2)
     :seat3 (seat-properties :seat3)
     :seat4 (seat-properties :seat4)}

    (fn [key me]
      ;(clog (:users @r/room*) :o)
      ;(clog @r/room* :o)
      (reduce (fn [me {:keys [state seat]}]
                (if (= :empty state)
                  (toggle-seat me seat)
                  me))
              me
              (users-in @r/room*)))

    (fn [ctx me]
      (doseq [[_ user] (:users @r/room*)]
        ;(clog @r/room* :o)
         (if (= :empty (:state user))
           (draw-seat ctx me user)
           (draw-user ctx me user))))]

   ; 원형 타이머 : 사용자가 게임 버튼(bet, hit, stand 등)을 누르기를 기다리는 타이머.
   [:timer
    {:state :none :img (:image-72 images*) :w 66 :h 66 :k 0 :t 0}
    update-timer
    draw-timer]

   [:my-id
    {:state :norm :x 10 :y 600}
    nil
    (fn [ctx {:keys [x y]}]
      (let [text (str "my-id: " (.substr (name @r/my-token*) 0 8))]
        (mc/draw-text ctx text [x y])))]

   [:room-id
    {:state :norm :x 10 :y 630}
    nil
    (fn [ctx {:keys [x y]}]
      (let [text (str "room-id: " (.substr (name (r/room-id)) 0 8))]
        (mc/draw-text ctx text [x y])))]

])
