(ns vita.blackjack.entity.panel
  "panel"
  (:use-macros [philos.cljs.debug :only [clog]])
  (:require [monet.canvas :as mc]
            [vita.blackjack.room :as r]
            [vita.blackjack.net :as net]
            [vita.blackjack.util :refer [dispatch state!]]
            [vita.blackjack.events :as event]
            [vita.blackjack.images :as image]))

(def ^:private images* image/images)

(defn state->index [state]
  ({:norm 0 :over 1 :checked 1 :down 2 :disable 3} state))

(defn on-click [key me]
  (case key
    :btn-bet   (net/send :bet)
    :btn-hit   (net/send :hit)
    :btn-stand (net/send :stand)
    :btn-split (net/send :split)
    :btn-double-down (net/send :double-down)))

(defn update-btn-by-mouse [key {:keys [state] :as me}]
  (let [state'
          (cond
            (event/click-in? me) (do (on-click key me) state)
            (and (= state :norm) (event/mouse-move-in? me))  :over
            (and (= state :over) (event/mouse-move-out? me)) :norm
            (and (= state :over) (event/mouse-down-in? me))  :down
            (and (= state :down) (event/mouse-up-in?  me))   :over
            (and (= state :down) (event/mouse-up-out? me))   :norm
            :else state)]
    (state! me state')))

(defn should-be-enable-btn? [btn]
  (cond
    (= :btn-bet btn) (r/waiting-user? (r/my-self))
    (#{:btn-stand :btn-hit } btn) (r/timer-user? (r/my-self))
    (#{:btn-split :btn-double-down} btn) (r/initial-two-cards-user? (r/my-self))))

(defn update-game-btn [key {:keys [state] :as me}]
  (if (should-be-enable-btn? key)
    (update-btn-by-mouse key (assoc me :state (if (= state :disable) :norm state)))
    (assoc me :state :disable)))

(defn adjust-money [money key]
  (let [min @r/min-betting*
        max @r/max-betting*
        money' (({:btn-min - :btn-max +} key) money 1000)]
    (cond
      (< money' min) min
      (> money' max) max
      :else money')))

(defn update-btn-+- [key {:keys [state] :as me}]
  (let [state'
          (cond
            (event/click-in? me) (do (swap! r/bet-money* adjust-money key) state)
            (and (= state :norm) (event/mouse-move-in? me))  :over
            (and (= state :over) (event/mouse-move-out? me)) :norm
            (and (= state :over) (event/mouse-down-in? me))  :down
            (and (= state :down) (event/mouse-up-in?  me))   :over
            (and (= state :down) (event/mouse-up-out? me))   :norm
            :else state)]
    (state! me state')))

(defn update-btn-check [key {:keys [state] :as me}]
  (let [state'
            (if (event/click-in? me)
                (do (dispatch :click key #{:user-panel})
                    (if (= state :norm) :checked :norm))
                state)]
    (state! me state')))

(defn draw-btn [ctx {:keys [state img x y w h]}]
  (mc/draw-image ctx img (* w (state->index state)) 0 w h  x y w h))

(defn draw-text [ctx me]
  (-> ctx
      (mc/fill-style "#FFF")
      (mc/font-style "18px Arial")
      (mc/text {:text (str "$" @r/bet-money*) :x 500 :y 558})))

(defn draw-ball [ctx {:keys [img x y w h]}]
  (mc/draw-image ctx img 0 0 46 42  x y 22 22))

(def entities [

   [:btn-bet
    {:state :norm :img (:image-31 images*) :x 290 :y 540 :w 108 :h 36}
    update-game-btn
    draw-btn]

   [:btn-hit
    {:state :norm :img (:image-86 images*) :x 264 :y 620 :w 152 :h 36}
    update-game-btn
    draw-btn]

   [:btn-stand
    {:state :norm :img (:image-91 images*) :x 424 :y 620 :w 152 :h 36}
    update-game-btn
    draw-btn]

   [:btn-double-down
    {:state :norm :img (:image-12 images*) :x 264 :y 660 :w 152 :h 36}
    update-game-btn
    draw-btn]

   [:btn-split
    {:state :norm :img (:image-85 images*) :x 424 :y 660 :w 152 :h 36}
    update-game-btn
    draw-btn]

   [:btn-min
    {:state :norm :img (:image-62 images*) :x 264 :y 580 :w 45 :h 33}
    update-btn-+-
    draw-btn]

   [:btn-max
    {:state :norm :img (:image-66 images*) :x 530 :y 580 :w 45 :h 33}
    update-btn-+-
    draw-btn]

   #_[:btn-minus
    {:state :norm :img (:image-62 images*) :x 264 :y 580 :w 30 :h 33}
    update-btn-by-mouse
    draw-btn]

   [:btn-plus
    {:state :norm :img (:image-39 images*) :x 500 :y 580 :w 30 :h 33}
    update-btn-by-mouse
    draw-btn]

   [:btn-check
    {:state :norm :img (:image-11 images*) :x 268 :y 540 :w 19 :h 22}
    update-btn-check
    draw-btn]

   [:betting-money
    {:state :norm :x 268 :y 540}
    nil
    draw-text]

   [:slide
    {:state :norm :img image/ball :x 350 :y 580 }
    nil
    draw-ball]

])
