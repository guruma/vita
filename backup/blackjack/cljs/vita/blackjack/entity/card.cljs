(ns vita.blackjack.entity.card
  "animation"
  (:use-macros [philos.cljs.debug :only [clog break]])
  (:require [monet.canvas :as mc]
            [monet.timer :as mt]
            [js.math :as math]
            [vita.blackjack.util :refer [state?]]
            [vita.blackjack.room :as r
               :refer [->coord ->offset ->size coord+offset keyword+ add-offset users-in calc-speeds
                       calc-speed move-delta-until lapse-time*]]
            [vita.blackjack.entity.chip :as chip]
            [vita.blackjack.net :as net]
            [vita.blackjack.images :as image :refer [images*]]
            [vita.blackjack.shared.cards :as card]))


;;; notes(guruma)
;;; must be read to understand this source code.
;
; entity : card, chip
;
;
; * use of name for parameters and vairalbes
;
;  t         {int}     : time from a start of a animation in ms.
;  x, y      {int}     : updated position of entity.
;  w, h      {int}     : width, hight of image of entity.
;  dst-x, dst-y
;            {int}     : destination of entity.
;  x-speed, y-speed
;            {dowble}  : speed of entity movement
;  deg       {int}     : angle of entity rotation during movement in degree.
;  d-speed   {double}  : speed of a angle change in degree/ms.
;  a         {double}  : alpha blending value to apply during entity movement.
;  a-speed   {double}  : speed of change of alpha value to apply to during entity movement.
;               (alpha value change : 1.0(start) -> 0.0(end))
;  lapse     {int}     : the lapse time during the moving of entity.
;  bet       {int}     : betting money.
;  seat      {keyword} : seat of whiche user bet. [:seat0, :seat1, ... seat5]
;  seat-num  {int}     : seat number of whiche user bet. [0 1 2 3 4 5]
;                        - dealer's seat-num is 5.
;  cards-coord-data
;            {map of card-data} : postion and speed of all cards.
;
;



(defn draw-card-back [ctx x y]
  (mc/draw-image ctx (:image-58 images*) x y)
  ctx)


;;;;;;;;;;;;;;;;;;;;;;;;;;
; card-delivery
;;;;;;;;;;;;;;;;;;;;;;;;;;

(def ->image-count {:card-back 9 :timer 16})

(defn card-delivery
  [dst-x dst-y lapse]
  (let [[x y] (->coord :card-box)
        [w h] (->size :card)
        initial-degree -50 ; the inclination of the card layed in the card box.
        [x-speed y-speed d-speed] (calc-speeds [dst-x dst-y 0] [x y initial-degree] lapse)]
  {:state :move :x x :y y :dst-x dst-x :dst-y dst-y :w w :h h :t 0 :k 0
   :x-speed x-speed :y-speed y-speed :d-speed d-speed :deg initial-degree}))

(defn update-card-moving
  [key {:keys [x y dst-x dst-y deg x-speed y-speed d-speed] :as me}]
  (let [x' (move-delta-until x dst-x (mt/dt) x-speed)
        y' (move-delta-until y dst-y (mt/dt) y-speed)
        deg' (move-delta-until deg 0 (mt/dt) d-speed)]
  (assoc me :x x' :y y' :deg deg')))

(defn update-card-reversion [key {:keys [k t] :as me}]
  (if (< k (->image-count :card-back))
    (let [t (+ t (mt/dt))
          k (quot t (lapse-time* :card-reversion-interval))]
      (assoc me :t t :k k))
    (do
      (when-let [[card seat] (r/next-new-card&seat-to-deliver @r/room*)]
        (r/add-new-card! seat card))
      (assoc me :state :none))))

(defn update-card-delivery [key {:keys [x y dst-x dst-y deg] :as me}]
  (if (and (= x dst-x) (= y dst-y) (= 0 deg))  ; card arrived?
    (update-card-reversion key me)
    (update-card-moving key me)))

(defn start-new-card-delivery-if-any [key me]
  (if-let [[card seat] (r/next-new-card&seat-to-deliver @r/room*)]
    (let [[x y] (if (= seat :seat5)
                  (->coord :dealer-cards)
                  (coord+offset seat :cards))
          lapse (lapse-time* (if (= seat :seat5) :dealer-cards seat))
          k (r/count-user's-cards seat)
          interval (->offset :card-interval)]
      (card-delivery (+ x (* k interval)) y lapse))
    me))

(defn update-card-delivery-by-room [key me]
  (if (state? me :move)
    (update-card-delivery key me)
    (start-new-card-delivery-if-any key me)))

(defn draw-card-rotated [ctx {:keys [x y w h deg] :as me}]
  (let [ox (+ x (/ w 2))
        oy (+ y (/ h w))]
    (-> ctx
        (mc/translate ox oy)  ; 좌표계 원점 이동
        (mc/rotate  deg)      ; 좌표계 원점을 중심으로 회전
        (draw-card-back 0 0)  ; 좌표계 원점에 그리기
        (mc/rotate  (- deg))  ; 좌표계 원점 회전 원복
        (mc/translate (- ox) (- oy))))) ; 좌표계 원점 이동 원복

(defn draw-card-reverted
  [ctx {:keys [k x y w h] :as me}]
  (mc/draw-image ctx image/card-back* (* w k) 0 w h x y w h))

(defn draw-card-delivery
  [ctx {:keys [x y dst-x dst-y deg] :as me}]
  (if (and (= x dst-x) (= y dst-y) (= 0 deg)) ; if card is arrived at the destination.
    (draw-card-reverted ctx me)
    (draw-card-rotated ctx me)))


;;;;;;;;;;;;;;;;;;;;;;;;;;
; cards-collecting
;;;;;;;;;;;;;;;;;;;;;;;;;;

(defn update-all-user's-cards-reversion [key {:keys [t] :as me}]
  (let [t (+ t (mt/dt))
        k (quot t (lapse-time* :card-back))]
    (assoc me :t t :k k)))

(defn move-card-for-collecting
  [{:keys [x y x-speed y-speed] :as card-data} {:keys [dst-x dst-y]}]
  (let [x' (move-delta-until x dst-x (mt/dt) x-speed)
        y' (move-delta-until y dst-y (mt/dt) y-speed)]
    (assoc card-data :x x' :y y')))

(defn update-all-user's-cards-moving
  [key {:keys [a a-speed cards-coord-data] :as me}]
  (let [a' (move-delta-until a 0 (mt/dt) a-speed)
        data' (map #(move-card-for-collecting % me) cards-coord-data)]
    (assoc me :a a' :cards-coord-data data')))

(defn assoc-speed [{:keys [x y] :as coord} {:keys [dst-x dst-y lapse]}]
  (let [[x-speed y-speed] (calc-speeds [dst-x dst-y] [x y] lapse)]
    (assoc coord :x-speed x-speed :y-speed y-speed)))

(defn start-all-user's-cards-collecting
  [key me]
  (clog me "start-all-user's-cards-collecting" :o)
  (if-let [coords (seq (r/coords-of-all-user's-cards-in @r/room*))]
    (assoc me :t 0 :k 0 :a 1.0 :cards-coord-data (map #(assoc-speed % me) coords))
    me))


(defn update-all-user's-cards-collecting
  [key {:keys [k] :as me}]
  (if (r/stage-clear?)
    (if (:cards-coord-data me)
      (if (< k (->image-count :card-back)) ; completed updating card reversion?
        (update-all-user's-cards-reversion key me)
        (do (r/clear-all-cards-in! @r/room*)
            (update-all-user's-cards-moving key me)))
      (start-all-user's-cards-collecting key me))
    me))

(defn draw-all-user's-cards-reversion
  [ctx {:keys [k cards-coord-data] :as me}]
  (let [[w h] (->size :card)]
    (doseq [{:keys [x y]} cards-coord-data]
      (clog [k x y w h] :o)
      ; draw from last image to first image.
      (mc/draw-image ctx image/card-back* (* w (- 8 k)) 0 w h x y w h))))

(defn draw-all-user's-cards-moving
  [ctx {:keys [a cards-coord-data] :as me}]
  (doseq [{:keys [x y]} cards-coord-data]
    (-> ctx
      (mc/alpha a)
      (draw-card-back x y)
      (mc/alpha 1.0))))

(defn draw-point [ctx x y point]
  (let [text (if (keyword? point) (name point) (str point))
        img (if (= "bust" text) (:image-10 images*) (:image-5 images*))]
    (-> ctx
      (mc/draw-image-at img (add-offset x y :card-point-image))
      (mc/draw-text text (add-offset x y :card-point-text)))))

(defn draw-card [ctx card x y]
  (let [[j i] (card/card->coord* card)
        [w h] (->size :card)]
    (mc/draw-image ctx (:image-82 images*) (* w i) (* h j) w h  x y w h))
  ctx)


(defn draw-cards [ctx me seat deck-idx deck-cnt {:keys [cards] :as deck}]
  (let [[x y] (-> (coord+offset (keyword+ :seat seat) :cards)
                  (r/+deck-offset deck-idx deck-cnt))
        dx (->offset :card-interval)]
    (if (< 1 (count cards))
      (draw-point ctx x y (:view-point deck)))
    (doseq [[n card] (keep-indexed #(vector %1 %2) cards)]
      (draw-card ctx card (+ x (* n dx)) y))))

(defn draw-decks [ctx me {:keys [seat] :as user}]
  (let [decks (r/decks-of user)
        cnt (count decks)]
    (doseq [[idx deck] (map-indexed #(vector %1 %2) decks)]
      (draw-cards ctx me seat idx cnt deck))))

(defn draw-all-user's-cards [ctx me]
  (doseq [user (users-in @r/room*)]
    (draw-decks ctx me user)))

(defn draw-dealer-card [ctx me]
  (let [cards (get-in @r/room* [:dealer :decks :deck0 :cards])]
    (if (and (< 1 (count cards)) (not ((set cards) :back)))
      (let [[x y] (->coord :dealer-cards-point)]
        (draw-point ctx x y (get-in @r/room* [:dealer :decks :deck0 :point]))))
    (doseq [[n card] (keep-indexed #(vector %1 %2) cards)]
      (let [[x y] (->coord :dealer-cards)
            dx (->offset :card-interval)]
        (if (= :back card)
          (draw-card-back ctx (+ x (* n dx)) y)
          (draw-card ctx card (+ x (* n dx)) y))))))

(defn draw-cards-collecting
  [ctx {:keys [k] :as me}]
  (draw-dealer-card ctx me)
  (draw-all-user's-cards ctx me)
  (if (r/stage-clear?)
      (if (< k (->image-count :card-back)) ; completed drawing card reversion?
        (draw-all-user's-cards-reversion ctx me)
        (draw-all-user's-cards-moving ctx me))))

(defn cards-collecting-propertis
  []
   (let [[x y] (->coord :cards-collection)
         [w h] (->size :cards)
         lapse (lapse-time* :cards-collection)
         a-speed (calc-speed 0 1.0 lapse)] ; alpha speed
     {:state :norm :dst-x x :dst-y y :w w :h h :lapse lapse :a-speed a-speed}))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; dealer-backcard-reversion
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defn update-dealer-backcard-reversion [key {:keys [state t k] :as me}]
  ;(if (r/all-users'-play-end?)
  (if (= :revert-hidden-card (get-in @r/room* [:dealer :state]))
    (let [t (+ t (mt/dt))
          k (quot t (lapse-time* :card-reversion-interval))]
      (if (< k (->image-count :card-back))
        (assoc me :t t :k k)
        (let [cards (get-in @r/room* [:dealer :the-cards])]
          (swap! r/room* update-in [:dealer] dissoc :the-cards)
          (swap! r/room* update-in [:dealer] dissoc :state)
          (swap! r/room* assoc-in [:dealer :decks :deck0 :cards] cards)
          (assoc me :t 0 :k 0))))
    me))

(let [img image/card-back*
      [w h] (->size :card)]
  (defn draw-dealer-backcard-reversion [ctx {:keys [state k x y]}]
    (if (= :revert-hidden-card (get-in @r/room* [:dealer :state]))
      (mc/draw-nth-image ctx img k x y w h))))

(defn dealer-backcard-reversion-properties []
  (let [dx (->offset :card-interval)
        [x y] (->coord :dealer-cards)]
    {:x (+ x dx) :y y }))


(defn merge-new-card [[k deck]]
   (if (:new-cards deck)
     (let [deck' (-> deck
              (assoc :cards (vec (concat (:new-cards deck) (:cards deck))))
              (dissoc :new-cards))]
       [k deck'])
    [k deck]))

(defn dummy-delivery [key me]
  (doseq [user (r/users-in @r/room*)]
    (let [token (:token user)
          new-decks (into {} (map merge-new-card (:decks user)))]
      ;(swap! r/room* [:users token] assoc :decks new-decks))))
      (swap! r/room* assoc-in [:users token :decks] new-decks)))
  ;(clog @r/room* :o)
  me)


(def entities [

   ; 모든 사용자의 게임이 끝난후 딜러카드의 백카드가 뒤집어져 보여지는 에니메이션.
   [:dealer-backcard-reversion
    (merge {:state :none :t 0 :k 0} (dealer-backcard-reversion-properties))
    update-dealer-backcard-reversion
    draw-dealer-backcard-reversion]

   ; 게임 종료후 카드 수거 에니메이션.
   [:cards-collecting
    (cards-collecting-propertis)
    update-all-user's-cards-collecting
    draw-cards-collecting]


   ; 게임 시작시 카드 배포 에니메이션.
   [:card-delivery
    (assoc (card-delivery 0 0 0) :state :none)
    ;dummy-delivery
    update-card-delivery-by-room
    draw-card-delivery]


])
