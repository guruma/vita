(ns vita.blackjack.room
  "room"
  (:use-macros [philos.cljs.debug :only [clog]]))


(def not-nil? (comp not nil?))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; room definition and room functions
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(def room* (atom {}))

(defn set-room [room]
  (reset! room* room))

(defn room-id [] (:room-id @room* :no-room-id))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; global variables for user
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(def my-token* (atom ""))

(def min-betting* (atom 5500))
(def max-betting* (atom 16500))
(def bet-money* (atom @min-betting*))

(defn set-bettings [room]
  (reset! min-betting* (:min-betting room 5500))
  (reset! max-betting* (:max-betting room 16500))
  room)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; lapse time constant definitions
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(def lapse-time*
  { :seat0 150
    :seat1 150
    :seat2 150
    :seat3 200
    :seat4 200
    :chip 200
    :dealer-cards 100
    :card-reversion-interval 30
    :cards-collection  300})


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; coordination constant definitions
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(def coords*
  {:coord  {:seat0 {:x 730 :y 380 }
            :seat1 {:x 550 :y 440 }
            :seat2 {:x 375 :y 460 }
            :seat3 {:x 200 :y 440 }
            :seat4 {:x 30  :y 380 }
            :dealer-cards {:x 375 :y 100 }
            :dealer-cards-point {:x 315 :y 130 }
            :card-box   {:x 580 :y 100}
            :chip-box   {:x 380 :y 50}
            :cards-collection   {:x 200 :y 90 }
            :chip-insurance0 {:x 550 :y 150}
            :chip-insurance1 {:x 480 :y 170}
            :chip-insurance2 {:x 395 :y 180}
            :chip-insurance3 {:x 310 :y 170}
            :chip-insurance4 {:x 240 :y 150}}
   :offset {:chip {:x 0 :y -215}
            :betting-money {:x 0 :y -170}
            :cards {:x 0 :y -130}
            :point {:x 0 :y -101}
            :timer {:x -29 :y -67}
            :timer-text {:x -4 :y -29}
            :frame {:x 0 :y -40}
            :state {:x 30 :y -23}
            :card-point-image {:x 5 :y -29}
            :card-point-text {:x 15 :y -11}
            :name {:x 9 :y -3}
            :photo {:x 9 :y 5}
            :chip-throwing-from {:x 20 :y 20}
            :result {:x 9 :y 63}
            :card-interval 11}
   :size   {:seat {:w 67 :h 71}
            :card {:w 39 :h 54}
            :photo {:w 50 :h 47}}
})

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; functions for coord and offset
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defn c-> [& args]
  (get-in coords* (vec args)))

(def xy (juxt :x :y))
(def wh (juxt :w :h))

(defn ->coord [ent]
  (xy (c-> :coord ent)))

(defn ->offset [ent]
  (if (= ent :card-interval)
    (c-> :offset ent)
    (xy (c-> :offset ent))))

(defn ->size [ent]
  (wh (c-> :size ent)))

(defn add-offset [x y ent]
  (vec (map + [x y] (->offset ent))))

(defn coord+offset [coord offset]
  (vec (map + (->coord coord) (->offset offset))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; deck의 좌표 계산
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; deck에 따른 offset 값을 리턴
; (:deck0) idx = 0, deck-cnt = 1 => offset-dx 0
; (:deck1) idx = 1, deck-cnt = 2 => offset-dx 30
(defn deck-offset [idx deck-cnt]
  (get-in [[0] [-30 30] [-45 -11 11]] [(dec deck-cnt) idx]))

(defn deck-kwd->num [kwd]
  ({:deck0 0 :deck1 1 :deck2 2} kwd))

(defn +deck-offset [[x y] idx cnt]
  (let [idx (if (keyword? idx) (deck-kwd->num idx) idx)
        dx (deck-offset idx cnt)]
    [(+ x dx) y]))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; keyword+
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defn keyword+ [& args]
  (let [args (map #(if (keyword? %) (name %) %) args)]
    (keyword (apply str args))))

(defn seat-keyword-of [user]
  (keyword+ :seat (:seat user)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; fill-empty-user
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defn- set-minus [a b]
  (reduce #(if (b %2) %1 (conj %1 %2)) [] a))

(defn empty-user-sat-in [seat-num]
  {:state :empty :token (keyword+ :seat seat-num) :seat seat-num :new-cards []})

(defn fill-empty-user [room]
  (let [users (vals (:users room))
        seats (set (map :seat users))
        empty-seats (set-minus #{0 1 2 3 4} seats)
        empty-users (map empty-user-sat-in empty-seats)]
    (reduce #(update-in %1 [:users] assoc (seat-keyword-of %2) %2)
            room
            empty-users)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; apis about room
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defn all-users'-play-end? []
  true)

(defn stage-clear? []
  (clog (:state @room*) "stage-clear?" :o)
  (= :clean (:state @room*)))

(defn dealer-in [room]
  (:dealer room))

(defn user-in-room? [token room]
  (not-nil? (some (set (keys (:users room))) [token])))

(defn my-self []
  (get-in @room* [:users @my-token*]))

(defn me-in-room? [room]
  (user-in-room? @my-token* room))

(defn users-in [room]
  (vals (:users room)))

(defn all-users-in [room]
  (conj (vec (users-in room)) (:dealer room)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; functions for deck of user
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defn active-deck? [deck]
  (#{:wait :bet :hit :split} (:state deck)))

(defn decks-of [user]
  (map second (sort-by key (:decks user))))

(defn active-deck-of [user]
  (some #(if (active-deck? %) %) (decks-of user)))

(defn last-deck-of [user]
  (second (last (:decks user))))

(defn active-deck-keyword-of [user]
  (some (fn [[k deck]] (if (active-deck? deck) k)) (:decks user)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; predicates for user
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defn empty-state? [state]
  (and (keyword? state) (= "empty" (.substring (name state) 0 5))))

(defn empty-user? [user]
  (empty-state? (:state user)))

(defn waiting-user? [user]
  (= :wait (:state (active-deck-of user))))

(defn have-only-two-cards? [user]
  (and (= 1 (count (:decks user)))
       (= 2 (count (get-in user [:decks :deck0 :cards])))))

(defn ready-user? [user]
  (< 1 (count (:cards (active-deck-of user)))))

(defn have-new-card? [user]
  (< 0 (count (:new-cards (active-deck-of user) 0))))

(defn terminated-user? [user]
  (nil? (active-deck-of user)))

(defn timer-user? [user]
  (and (not (empty-user? user))
       (ready-user? user)
       (not (have-new-card? user))
       (not (terminated-user? user))))

(defn initial-two-cards-user? [user]
  (and
    (not (terminated-user? user))
    (have-only-two-cards? user)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; get-user-state-for-display
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;
(defn get-user-state-for-display [user]
  (if-let [deck-state (:state (or (active-deck-of user) (last-deck-of user)))]
    deck-state
    (if-let [user-state (:state user)]
      user-state
      :nil-state)))

(defn get-user-state-str-for-display [user]
  (str (get-user-state-for-display user)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; get-next-timer-user
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defn sort-by-seat [users]
  (sort-by #(:seat %) users))

(defn get-next-timer-user [room]
  (if (ready-user? (:dealer room))
    (if-let [users (seq (filter timer-user? (users-in room)))]
      (first (sort-by-seat users)))))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; fill-empty-seat
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defn- fill-empty-seat [seat-card-pairs seat-num]
  (if ((set (map first seat-card-pairs)) seat-num) ; not empty-seat?
    seat-card-pairs
    (conj seat-card-pairs [seat-num []])))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; sall-users-new-cards-ordered-by-seat
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defn new-cards-of [user]
  (some :new-cards (decks-of user)))

(defn all-users-new-cards-ordered-by-seat [room]
  (let [seat-cards-pairs (map (juxt :seat #(new-cards-of %)) (all-users-in room))
        seat-cards-pairs' (reduce fill-empty-seat seat-cards-pairs [0 1 2 3 4])
        sorted (sort-by first seat-cards-pairs')]
    (vec (map second sorted))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; select-keys-starting
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defn startswith [k1 k2]
  (let [s1 (name k1)
        s2 (name k2)]
    (= s2 (.substring s1 0 (count s2)))))

;:seat0deck0 :seat
(defn select-keys-starting [m key-starting]
  (into {}
    (filter #(startswith (first %) key-starting)
            m)))

;
; 다음 배표할 카드를 찾는 전략:
; 다음 배포할 카드는 가장 많은 새 카드를 가진 유저중 seat-num이 가장 작은 유저의 첫번째 새 카드이다.
; 다음 배포할 카드는 가장 많은 새 카드를 가진 덱의 유저중 seat-num이 가장 작은 유저의 덱의 첫 새 카드이다.
;
; 1. 모든 유저의 새 카드 리스트를 구한다. (empty user는 empty vector 표시, seat 순서대로)
; 2. 각 유저의 새 카드의 갯수의 리스트를 구한다.
; 3. 각 유저의 새 카드의 갯수를 하나씩 줄인다. 최종적으로 리스트에는 0과 1만 남을 때까지.
;    (1이 남은 유저가 가장 많은 카드를 가진 유저다)
; 4. 위 리스트에서 1인 요소의 인덱스만 남긴다.
; 5. 다시 그 결과 리스트에서 첫 요소를 구한다.
;
; [[] [:heart-4 :spade-3] [] [:diamond-3 :spade-4] []]  -> new-cards-list, ordered by seat
; [0  2  0  2  0]                                       -> counts
; [0  1  0  1  0]                                       -> seq-of-one-zero
; [1 3]                                                 -> indexes of one
;
(defn next-new-card&seat-to-deliver [room]
  (let [new-cards-list (all-users-new-cards-ordered-by-seat room)
        counts (map count new-cards-list)
        seq-of-one-zero (if (some #(< 1 %) counts)
                          (map #(if (zero? %) % (dec %)) counts)
                          counts )
        indexes-of-one (keep-indexed #(if (= 1 %2) %1) seq-of-one-zero)
        seat-num-to-deliver (first indexes-of-one)]
    (if seat-num-to-deliver
        [(get-in new-cards-list [seat-num-to-deliver 0]) ; :heart-4
         (keyword+ :seat seat-num-to-deliver)])))        ; seat0

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; betting-money-by get-user-by-seat
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defn seat->num [seat]
  ({:seat0 0 :seat1 1 :seat2 2 :seat3 3 :seat4 4} seat))

(defn get-token&user-by-seat [seat]
  (let [seat-num (if (number? seat) seat (seat->num seat))]
    (some (fn [[k v]] (if (= seat-num (:seat v)) [k v])) (:users @room*))))

(defn get-user-by-seat [seat]
  (second (get-token&user-by-seat seat)))

(defn betting-money-by [seat-kwd deck-kwd]
  (get-in (get-user-by-seat seat-kwd) [:decks deck-kwd :betting-money]))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; add-new-card!
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defn- add-new-card-to-dealer! [card]
  (swap! room* update-in [:dealer :decks :deck0 :new-cards] #(vec (rest %)))
  (swap! room* update-in [:dealer :decks :deck0 :cards] #(vec (conj % card))))

(defn- add-new-card-to-user! [seat card]
  (let [[token user] (get-token&user-by-seat seat)
        deck (some #(if (:new-cards %) %) (decks-of user))
        deck-keyword (some (fn [[k v]] (if (:new-cards v) k)) (:decks user))
        cards' (vec (conj (:cards deck) card))
        new-cards' (vec (rest (:new-cards deck)))]
    (swap! room* assoc-in [:users token :decks deck-keyword :new-cards] new-cards')
    (swap! room* assoc-in [:users token :decks deck-keyword :cards] cards')))

(defn add-new-card! [seat card]
  (if (= seat :seat5)
    (add-new-card-to-dealer! card)
    (add-new-card-to-user! seat card)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; count-user's-cards
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defn count-user's-cards [seat]
  (let [user (if (= seat :seat5)
                (:dealer @room*)
                (get-user-by-seat seat))]
    (count (:cards user))))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; predicates for deck
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; ----------------------------
; 언제 칩이 이동할지를 판단하는 pred 함수들
; 게임 로직상 한 순간에는 한 pred 함수만 참이어야 한다.
; ----------------------------

(defn win? [deck]
  (and (< 0 (:betting-money deck 0))
       (= true (:win deck))))

(defn lose? [deck]
  (and (< 0 (:betting-money deck 0))
       (= false (:win deck))))

(defn betting? [deck]
  (< 0 (:new-betting-money deck 0)))

(defn betted? [deck]
  (and (< 0 (:betting-money deck 0))
       (not (contains? deck :new-betting-money))
       ;(not (contains? deck :win))))
       (not (win? deck))
       (not (lose? deck))))

; ----------------------------

(defn double-down? [deck]
  (= true (:double-down deck)))


(defn insurance? [deck]
  (clog deck "insurance?" :o)
  (< 0 (:insurance deck 0)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; chip movements
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; return [money seat deck-kwd deck-cnt]
(defn money&seat-of [pred user]
  (keep (fn [[k deck]]
          (if (pred deck)
            [(:betting-money deck)    ; money
             (seat-keyword-of user)   ; seat
             k                        ; deck-kwd
             (count (:decks user))])) ; deck-cnt
         (:decks user)))

(defn money&seat-in [room]
  (mapcat (partial money&seat-of betted?) (users-in room)))

(defn lost-money&seat-in [room]
  (mapcat (partial money&seat-of lose?) (users-in room)))

(defn won-money&seat-in [room]
  (mapcat (partial money&seat-of win?) (users-in room)))

(defn new-bets&seat-in
  "return a list of the new betting money with each seat in the room."
  [room]
  (mapcat (partial money&seat-of betting?) (users-in room)))

(defn double-down&seat-in [room]
  (mapcat (partial money&seat-of double-down?) (users-in room)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; set-betting-money!
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defn set-betting-money! [seat deck-kwd]
  (let [[token user] (get-token&user-by-seat seat)
        deck (get-in user [:decks deck-kwd])
        deck' (-> deck
                  ; :betting-money의 설정 방식
                  ;  - betting일 경우 :new-betting-money이 존재하므로 값을 설정.
                  ;  - lose나 win일 경우에는 :new-betting-money가 없기 때문에 디폴트로 0 을 설정.
                  (assoc :betting-money (:new-betting-money deck 0))
                  (dissoc :new-betting-money))
        user' (assoc-in user [:decks deck-kwd] deck')]
    (swap! room* assoc-in [:users token] user')))

(defn set-double-down! [seat deck-kwd]
  (let [[token user] (get-token&user-by-seat seat)
        deck  (-> (get-in user [:decks deck-kwd])
                  (dissoc :double-down)
                  (update-in [:betting-money] * 2))
        user' (assoc-in user [:decks deck-kwd] deck)]
    (swap! room* assoc-in [:users token] user')))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; coords-of-all-user's-cards-in
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defn coords-of-cards-by-deck
  "return coords [x y] of all cards of user"
  [user [deck-kwd deck]]
  (let [[x y] (if (= :seat5 (seat-keyword-of user))
                          (->coord :dealer-cards)
                          (-> (coord+offset (seat-keyword-of user) :cards)
                              (+deck-offset deck-kwd (count (:decks user)))))
        dx (->offset :card-interval)]
    (map-indexed (fn [n e] {:x (+ x (* dx n)) :y y}) (:cards deck))))

(defn coords-of-cards-by-user
  "return coords [x y] of all cards of user"
  [user]
  (mapcat (partial coords-of-cards-by-deck user) (:decks user)))

(defn coords-of-all-user's-cards-in [room]
  (apply concat
             (map coords-of-cards-by-user (all-users-in room))))


;;;;;;;;;;;;;;;;;;;;;;;;
;; kinetic
;;;;;;;;;;;;;;;;;;;;;;;;

(defn move-delta-until [a to dt speed]
  (let [a (+ a (* dt speed))]
    (if (< 0 speed) (min a to) (max a to))))

(defn calc-speed [dst org t]
  (/ (- dst org) t))

(defn calc-speeds [dst org t]
  (map #(calc-speed %1 %2 t) dst org))


;;;;;;;;;;;;;;;;;;;;;;;;
;; dealer-hidden-card-in
;;;;;;;;;;;;;;;;;;;;;;;;

(defn- dealer-hidden-card-in [new-room]
  (let [cur-cards (get-in @room* [:dealer :decks :deck0 :cards])
        new-cards (get-in new-room [:dealer :decks :deck0 :cards])]
    (if (and (= :back (second cur-cards))  ; hidden-card?
             (not= :back (second new-cards)))
      (let [dealer (:dealer new-room)
            dealer (update-in dealer [:decks :deck0 :cards] (comp vec butlast))
            dealer (assoc dealer :state :revert-hidden-card :the-cards new-cards)]
        (assoc new-room :dealer dealer))
      new-room)))

;;;;;;;;;;;;;;;;;;;;;;;;
;; bear-new-bets-in
;;;;;;;;;;;;;;;;;;;;;;;;

(defn betting-money [room token]
  (get-in room [:users token :betting-money] 0))

; f: [k v] -> v'
; map{} -> apply f -> map{}
(defn fmap
  "apply function f to each item of map m and return new map.
   f gets two parameters that are key and value of each item of m
   and returns new value."
  [m f]
  (into {} (for [[k v] m] [k (f k v)])))

(defn bear-new-bets-in [new-room]
  (update-in new-room [:users] fmap
    (fn [token new-user]
      (update-in new-user [:decks] fmap
         (fn [k new-deck]
            (let [old-betting-money (get-in (:users @room*) [token :decks k :betting-money] 0)
                  new-betting-money (:betting-money new-deck)]
              (if (< old-betting-money new-betting-money)
                (assoc new-deck :new-betting-money new-betting-money)
                new-deck)))))))


;;;;;;;;;;;;;;;;;;;;;;;;
;; bear-new-cards
;;;;;;;;;;;;;;;;;;;;;;;;

;; TODO: 이것을 fmap 을 사용하여 bear-new-bets-in 처럼 바꾸기

(defn- deck-minus [long-deck short-deck]
  (let [long-cards (:cards long-deck [])
        short-cards (:cards short-deck [])
        added-cards (vec (drop (count short-cards) long-cards))]
    [short-cards added-cards]))

(defn- bear-new-cards [old-decks new-decks [k new-deck]]
  (let [[old-cards added-cards] (deck-minus new-deck (k old-decks))]
    (if (seq added-cards)
      (-> new-decks
        (assoc-in [k :cards] old-cards)
        (assoc-in [k :new-cards] added-cards))
      new-decks)))

(defn- bear-new-cards-of [new-user old-user]
  (let [old-decks (:decks old-user)
        new-decks (:decks new-user)]
    (assoc new-user :decks
      (reduce #(bear-new-cards old-decks %1 %2) new-decks new-decks))))

(defn bear-new-cards-of-dealer [new-room]
  (let [old-dealer (:dealer @room*)
        new-dealer (:dealer new-room)]
    (assoc new-room :dealer
      (bear-new-cards-of new-dealer old-dealer))))

(defn bear-new-cards-in [new-room]
  (let [old-users (:users @room*)
        new-users (:users new-room)]
    (assoc new-room :users
      (reduce (fn [new-users [token new-user]]
                (assoc new-users token (bear-new-cards-of new-user (token old-users))))
                new-users
                new-users))))

(defn bear-all-new-cards-in [new-room]
  (-> new-room
      bear-new-cards-in
      bear-new-cards-of-dealer))

;;;;;;;;;;;;;;;;;;;;;;;;
;; clear-all-cards-in
;;;;;;;;;;;;;;;;;;;;;;;;

(defn clear-all-user's-cards-in [room]
  (update-in room [:users] fmap #(assoc %2 :decks [])))

(defn clear-dealer-cards-in [room]
  (update-in room [:dealer :decks :deck0 :cards] empty))

(defn clear-all-cards-in! [room]
  (reset! room*
      (-> room
          clear-dealer-cards-in
          clear-all-user's-cards-in)))
