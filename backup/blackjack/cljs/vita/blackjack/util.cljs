(ns vita.blackjack.util
  "utility functions"
  (:use-macros [philos.cljs.debug :only [clog cl]])
  (:use [jayq.core :only [$]])
  (:require [monet.canvas :as mc]))

(defn alert [str] (js/alert str))
(defn log [str] (.log js/console str))

(defn point-in-rect? [x y r]
  (and (< (:x r) x (+ (:x r) (:w r)))
       (< (:y r) y (+ (:y r) (:h r)))))

(defn bottom [me] (+ (:y me) (:h me)))
(defn right  [me] (+ (:x me) (:w me)))
(defn center-x  [me] (+ (:x me) (/ (:w me) 2)))
(defn center-y  [me] (+ (:y me) (/ (:h me) 2)))

(defn state? [me state]
  (= state (:state me)))

(defn state! [me state]
  (assoc me :state state))

(defn cyclex
  ([[a & r]] (conj (vec r) a))
  ([coll n]  (loop [coll coll i 0 n (rem (if (pos? n) n (- n)) (count coll))]
            (if (= i n) coll
              (recur (cyclex coll) (inc i) n)))))


;************************************************************
; utility functions for event
;************************************************************

(defn dispatch [event sender receivers]
  "@event {keyword} event name.
   @sender {keyword} generator of event as entity to be added
   @receivers {coll} collection of keyword, receivers for event.
   @return nil

   dispatch event from generator to each receiver
   the generator is added to a set for the event key in the value map fo receiver"

  (let [entities (:entities vita.blackjack.core/canvas)]
    (doseq [r receivers]
      (let [{:keys [value update draw] :as ent} (aget entities r)
            es (or (:event-senders value) (atom {}))
            senders (or (event @es) #{})
            _  (swap! es assoc event (conj senders sender))
            value (assoc value :event-senders es)]
        (aset entities r (assoc ent :value value))))))

(defn event?! [me event sender]
  "@me {map} the value of receiver entity
   @sender {keyword} a generator of event as entity to be queryed.
   @event {keyword} a event which sender send to receivers.
   @return {:keyword | nil} sender, as true or false"

  (if-let [es (:event-senders me)]
    (when-let [s (get-in @es [event sender])]
      (swap! es update-in [event] disj sender)
      s)))

(defn click-event? [me generator]
  (event?! me :click generator))

(defn- bingo-line [windows line]
  "@params {vector of vector} windows : window in the frame.
   @params {vector of int} line : line to be calculated against windows.
   @return {int | nil} : count of bingo in the line against windows,
                         or nil

   calculate the count of bingo in tne line against the windows.
   usage : (calc-bingo windows [1 1 2 1 2])"

   (let [line (map-indexed #(vector %1 %2) line)
         items (map #(get-in windows %) line)
         n (some #(if (apply = (take % items)) % nil) [5 4 3 2])
         bingo-item (first (take (or n 0) items))
         line (if (or (= :question bingo-item)
               (and  (= n 2) (#{:apple :lemon :grape :watermelon :cherry} bingo-item)))
               nil
               (take (or n 0) line))]
     (if (seq line)
       [bingo-item n line])))

(defn bingo-lines [windows lines]
  "@params {vector of vector} windows : windows in the frame.
   @params {vector of vector} lines : lines to be calculated against windows. ( frames.cljs에 정의된 *lines* )
   @return {map} : map of line number and bingo count in the windows.
                  {8 [:watermelon 3 ([0 0] [1 0] [2 1])], 13 [:apple 3 ([0 2] [1 1] [2 2])]} "

  (into {}
    (keep-indexed
      (fn [i line]
         (let [bingo (bingo-line windows line)]
           (if (seq bingo)
             [i bingo])))
      lines)))


;************************************************************
; utility functions for entity
;************************************************************

(defn get-entity [k]
  (mc/get-entity vita.blackjack.core/canvas k))

(defn entity-in
  ([k]
   ;(clog (:value (get-entity k)))
   (:value (get-entity k)))
  ([k k']   (k' (entity-in k))))

(defn frame-in [k]
  (entity-in :frame k))

(defn frame-state? [state]
  (= state (frame-in :state)))

(defn ->coords [k col]
  (apply concat
         (map-indexed
           (fn [i v]
             (map #(vector i %)
                  (keep-indexed #(if (= k %2) %1) v)))
           col)))

(defn gold7-occur []
  (let [reels (frame-in :reels)
        windows (frame-in :windows)]
    (for [[r w i] (map #(vector %1 %2 %3) reels windows (range 5))
             :when (and (state? r :stop) (:7gold (frequencies (take 3 w))))]
             i)))

(def item-7 {:gold :7gold :blue :7blue :red :7red})

(defn- ->7value [value]
  (vec (map #(if (% item-7) (% item-7) %) value)))

(defn convert-7value [value]
  "서버에서 받은 :gold -> :7gold로 변경한다.
  (convert-7value [[:gold] [:red] [:blue]])
  ==> [[:7gold] [:7red] [:7blue]]"
  (vec (map ->7value value)))




;************************************************************
; utility functions for drawing a number.
;************************************************************

(defn spin-btn-enable?[]
  (let [frame-state (frame-in :state)]
    (= frame-state :norm)) )

(defn- ->digit [n]
  (let [m {\0 0 \1 1 \2 2 \3 3 \4 4 \5 5 \6 6 \7 7 \8 8 \9 9 \, 10 \$ 11}]
  (map-indexed (fn [i n] [(m n) i])
    (flatten (interpose \, (partition-all 3 (reverse (str n))))))))

(defn draw-num [ctx {:keys [img num x y w h]}]
  (doseq [[n i] (->digit num)]
    (mc/draw-image ctx img (* n w) 0 w h  (- x (* i w)) y w h)))

(defn draw-money [ctx {:keys [img num x y w h]}]
  (let [max-i (count (->digit num))]
    (doseq [[n i] (->digit num)]
        (mc/draw-image ctx img (* n w) 0 w h  (- x (* i w)) y w h))
    ;; draws "$" symbol in front of num.
    (mc/draw-image ctx img (* 11 w) 0 w h  (- x (* max-i w) 7) y w h) ))
                              ;; "$" symbol is positioned in the 11th image.
















