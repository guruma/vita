(ns vita.blackjack.core
  (:use-macros [jayq.macros :only [ready]]
               [purnam.js :only [obj arr ? ?> ! !> f.n def.n do.n
                                 def* f*n def*n do*n]]
               [philos.cljs.debug :only [dbg clog]])
  (:use [jayq.core :only [$ bind]]
        [purnam.cljs :only [aget-in aget-in]])
  (:require [monet.canvas :as mc]
            [vita.blackjack.net :as net]
            [vita.blackjack.events :as event]
            [vita.blackjack.entity.entities :as entity]
            [vita.blackjack.sounds :as sound]
            [media.sound :as snd]
            [vita.sonaclo.shared.const.url :as SONACLO_URL]
            ))


(defn- bind-events []  ;; todo: refactoring.
  (ready
    (-> ($ :#vita-canvas) (bind :keydown event/on-keydown))
    (-> ($ :#vita-canvas) (bind :keyup event/on-keyup))
    (-> ($ :#vita-canvas) (bind :click event/on-click))
    (-> ($ :#vita-canvas) (bind :mousemove event/on-mouse-move))
    (-> ($ :#vita-canvas) (bind :mousedown event/on-mouse-down))
    (-> ($ :#vita-canvas) (bind :mouseup event/on-mouse-up))))

(defn- add-entities [entities canvas]
  (dorun (map #(apply (partial mc/add-entity canvas) %) entities)))

(defn- start-game []
  (sound/background-music true)
  (let [canvas (mc/init (.get ($ :#vita-canvas) 0) :2d)]
    (add-entities entity/entities canvas)
    (bind-events)))

(defn parse-query [url]
  (let [query (last (.split url "?"))
        pairs (.split query "&")]
    (into {}
      (map (fn [p] (let [[a b] (vec (.split p "="))] (vector (keyword a) b))) pairs))))

(def user-id* (atom "test-2"))

(defn set-user-id [url]
  (if-let [user-id (:user-id (parse-query url))]
    (reset! user-id* user-id)))

(defn ^:export main []
  (set-user-id (.-href (.-location js/document)))
  (net/connect SONACLO_URL/APP_BLACKJACK)
  (start-game))
