(ns vita.blackjack.images
  "load images for game"
  (:use-macros [vita.macros :only [def-image-loaded]]))

(defn load-image [path]
  (let [img (js/Image.)]
    (set! (.-src img) path)
    img))

(def background (load-image "img/blackjack/image 100.jpg"))
(def photo (load-image "img/blackjack/healing-girl.jpg"))
(def ball (load-image "img/blackjack/ball.png"))

;; menu images
(def btn-info (load-image "img/menu/btn-info.png"))
(def btn-sound (load-image "img/menu/btn-sound.png"))
(def btn-background-music (load-image "img/menu/btn-background-music.png"))
(def btn-window-bigger (load-image "img/menu/btn-window-bigger.png"))
(def btn-window-smaller (load-image "img/menu/btn-window-smaller.png"))

(defn keyword-image-name [n]
  (keyword (str "image-" n)))

(defn load-image-n [n]
  (load-image (str "img/blackjack/image " n ".png")))

(def images
  (loop [i 1 col {}]
    (cond
      (> i 105) col
      (#{100 95} i) (recur (inc i) col)
      :else (recur (inc i) (assoc col (keyword-image-name i) (load-image-n i))))))

(def images* images)

(def card-back* (:image-25 images))

