(ns vita.blackjack.net
  "net"
  (:use-macros [philos.cljs.debug :only [clog]])
  (:require [sonet.network :as net]
            [vita.blackjack.room :as r]))


(defn my-active-deck-keyword [room]
  (r/active-deck-keyword-of (get-in room [:users @r/my-token*])))

(defn process-data [cmd data]
  (-> data
      (#(if (= :login cmd)
          %
          (assoc % :token @r/my-token* :room-id (r/room-id))))
      (#(if (= :bet cmd)
          (assoc % :betting-money @r/bet-money*)
          (assoc % :split-index (my-active-deck-keyword @r/room*))))))

(defn send
  ([cmd] (send cmd {}))
  ([cmd data]
    (let [data' (process-data cmd data)]
      (net/send (str {:command cmd :data data'})))))

(defn- on-connect [obj]
  (.log js/console (str "on connect:" obj))
  (send :login {:user-id (keyword @vita.blackjack.core.user-id*)}))

(defn print-room [room]
  (let [k (r/lapse-time* :seat0)])
  (clog room :o)
  room)

(defn preprocess-room [room]
  (-> room
    r/bear-all-new-cards-in
    ;print-room
    r/bear-new-bets-in
    ;print-room
    r/dealer-hidden-card-in
    ;print-room
    (assoc-in [:dealer :seat] 5))) ; seat number of dealer is 5.

(defn fill-empty-user [room]
  (if (r/me-in-room? room)
    room
    (r/fill-empty-user room)))


(defn- on-receive [obj]
  ;(.log js/console obj)
  (let [{:keys [command data]} (cljs.reader/read-string (str (aget obj "data")))
        room' (case command
                :login (do (reset! r/my-token* (:token data))
                           (send :join))
                :join  ((comp fill-empty-user r/set-bettings) data)
                :seat  ((comp preprocess-room fill-empty-user) data)
                :bet   (preprocess-room data)
                :hit   (preprocess-room data)
                :double-down   (preprocess-room data)
                :stand (preprocess-room data)
                :calc  (preprocess-room data)
                :clean (preprocess-room data))]
    (clog command "on-receive" :o)
    (r/set-room room')))

(defn- on-close [obj]
  (.log js/console (str "close:" obj)))

(defn connect [url]
  (net/register on-connect on-receive on-close)
  (net/connect url))
