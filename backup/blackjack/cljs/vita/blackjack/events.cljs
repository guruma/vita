(ns vita.blackjack.events
  "events system"
  (:use [jayq.core :only [$]]
        [vita.blackjack.util :only [log point-in-rect?]]))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; events system
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(def events (atom {}))

(defn- event? [e] (@events e))
(defn- event-happen? [e] (@events e))
(defn- event-x? [e] ((event? e) 0))
(defn- event-y? [e] ((event? e) 1))
(defn- delete-event! [e] (swap! events dissoc e))

(defn- delegate-event [o]
  (let [type (keyword (.-type o))
        x (.-offsetX o)
        y (.-offsetY o)
        key (.-keyCode o)
        mouse-events #{:mousedown :mouseup}
        theother #(first (disj %1 %2))]
    (condp some [type]
      #{:keydown :keyup} (swap! events assoc type [key])
      mouse-events       (swap! events #(assoc (dissoc % (theother mouse-events type)) type [x y]))
      #{:mousemove :click} (swap! events assoc type [x y])
      nil)))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; event handlers
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defn on-click [e]
  (delegate-event e))

(defn on-mouse-move [e]
  (delegate-event e))


(defn on-mouse-down [e]
  #_(log (str " page-x: " (.-offsetX e)
              " page-y: " (.-offsetY e)
              " client-x: " (.-clientX e)
              " client-y: " (.-clientY e)
              " scrollTop: " (.scrollTop ($ :body))
              " scrollLeft: " (.scrollLeft ($ :body))
              " x: " (+ cx sl)
              " y: " (+ cy st)))
  (delegate-event e))

(defn on-mouse-up [e]
  (delegate-event e))

(defn on-keydown [e]
  (delegate-event e))

(defn on-keyup [e]
  (delegate-event e))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; event predicates for buttons
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


(defn- event-in-rect? [e r]
  (point-in-rect? (event-x? e) (event-y? e) r))

(defn- mouse-event-in? [e me]
  (if (event-happen? e)
    (when-let [r (event-in-rect? e me)]
      (delete-event! e)
      r)))

(defn- mouse-event-out? [e me]
  (if (event-happen? e)
    (when-let [r (not (event-in-rect? e me))]
      (delete-event! e)
      r)))

(defn mouse-move-in? [me]
  (mouse-event-in? :mousemove me))

(defn mouse-move-out? [me]
  (mouse-event-out? :mousemove me))

(defn mouse-down-in? [me]
  (mouse-event-in? :mousedown me))

(defn mouse-up-in? [me]
  (mouse-event-in? :mouseup me))

(defn mouse-up-out? [me]
  (mouse-event-out? :mouseup me))

(defn click-in? [me]
  (mouse-event-in? :click me))

(defn key? []
  (when-let [key (event? :keydown)]
    (delete-event! :keydown)
    (first key)))
