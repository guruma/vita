(ns vita.blackjack.sender
  "sender"
  (:require [vita.common.websocket :as ws]
            [vita.blackjack.util :as bj-util]
            [vita.session :as session]))


#_(def token-ws-map* (atom {})) ; user-id , ws
#_(def ws-token-map* (atom{}))  ; ws, user-id

(defn filter-seated-user
  "방에 seat 하지 않은 유저는 제외하고 client에 보내준다."
  [room]
  (let [users (:users room)
        filtered-users (into {}(remove (fn [[k v]] (= :none (:seat v) )) users) ) ]
    (assoc room :users filtered-users)))


(defn- select-user-id-from-room
  "return : ([user-id] [user-id])"
  [room]
  (let [users (:users room)]
    (map #(:token %) (vals users)) ))


(defn broadcast [user-ids data]
  (println "############### broadcast")

  (let [wss (map #((keyword %) (session/get-ws) ) user-ids)]
  ;(let [wss (map #((keyword %) @token-ws-map*) user-ids)]
    (loop [users-ws wss]
      (if (nil? users-ws)
        (println (count wss) "user web socket send complete")
         (do
          (let [user-ws (first users-ws)]
            (ws/send user-ws  data))
          (recur (next users-ws)) )))  ))


(defn send->user [token data]
  (let [user-ws   (session/get-ws token)]
  ;(let [user-ws   ((keyword token) @token-ws-map*)]
    (ws/send user-ws  (str data)) ))


(defn broadcast-room-user [command room]
  (let [data {:command command :data (filter-seated-user room)}
        user-ids (select-user-id-from-room room)]
    (bj-util/print-room command room)
    (broadcast user-ids (str data)) ))
