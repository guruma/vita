(ns vita.blackjack.room
  (:require [vita.blackjack.users :as user]
            [vita.blackjack.rooms :as rooms]
            [vita.blackjack.shared.cards :as cards]
            [vita.blackjack.sender :as sender]
            [vita.sonaclo.shared.util :as util]
            [vita.blackjack.util :as bj-util]
            [clojure.core.async :as async :refer [<! >! <!! >!! timeout chan alt! alts!! go close! thread]]
            [vita.blackjack.database.db-blackjack :as db]
            ))

;; room-state 변화
;; :init       : card 초기화 방
;; :wait       : channel를 대기한다는 의미.
;;               (:init일때만 :wait(channel)로 가능. send-clean-room&change-wait에서 :init로 상태변경)
;; :ch-timeout : :wait상태에서 일정 시간 후에 channel이 time 아웃이 된 상태
;;               :모든 유저들이 배팅을 하지 않았기에 이 상태에서 처음 betting한 유저만 게임 플레이된다.
;; :play       : 유저가 betting을 한 후의 상태
;; :clean      : 정산 후 client에 카드 애니매이션 시작하기 위한 신호
;; :init       : ...

;; user-state 변화
;; :none       : 단순 방의 입장
;; :wait       : 자리에 앉아 있는 상태
;; :in-play    : 카드 배팅하였을 때


;; deck-state (card-state)
;; :wait       : 초기화된 상태.
;; :bet :bust :blackjack 등등

(def cards* (apply concat cards/cards-board*))
(def room-list* (atom{}))
(def room*
   {:room-id :room-id
         :ch (chan)
         :state :init
         :cards  []
         :users  {}
         :dealer {:decks {:deck0 {:cards [] :point [] :state :wait}}}; :sate -> none or blackjack
         :min-betting 200
         :max-betting 10000})


;;(def user-card {:s0 {:cards [] :betting-money 0 :point 0 :state :bust} :s1 {} :s2 {}})
;; user-test code :state-> :none :bet :hit :stand

(def user  {:name "user-1"    :token :token-1 :seat :none  :state :none
                   :decks {}
                   :betting-money-result 0  :min-money 50000 :max-money 50000 :total-money 0 })

(def user-1      {:token-1 {:name "user-1"    :token :token-1 :seat :none :betting-money 0 :state :in-play
                            :decks {:deck0 {:cards [:diamond-K :spade-1] :betting-money 0 :point [10] :state :bust :cmd :none}
                                    :deck1 {:cards [:diamond-K :spade-2] :betting-money 0 :point [20] :state :bust}}
                           :betting-money-result -10000  :min-money 50000 :max-money 50000 :total-money 500000 } })
(def user-2      {:token-2 {:name "user-2"    :token :token-2 :seat :none :betting-money 0 :state :none
                            :decks {:deck0 {:cards [:diamond-K :spade-4] :betting-money 0 :point "bust" :state :bust :cmd :none}}
                           :betting-money-result -10000  :min-money 50000 :max-money 25000 :total-money 299990 } })

(def clear-stage-set #{:bust :blackjack :double-down :stand})
;; :wait => 방에 앉아 있는 값
;; :none => 방에 입장만 한 상태.
(def none-stage-set  #{:wait :none})
(def play-stage-set  #{:hit :wait :bet})





(declare blackjack?)
(declare <!user-split-card)
(declare calc-user-split-card-point)
(declare calc-user-money)
(declare calc-room)
(declare send-clean-room&change-wait)
(declare make-split-card)
(declare change-room-to-wait)
(declare hand-betting-card->room)
(declare exist-room?)
(declare calc-user-money)
(declare clear-playing-card?)
(declare calc-room)
(declare same-all-card-state?)
(declare check-blackjack)
(declare <!playing-game-user)
(declare calc-user-card-point)


;;;;;;;;;;;; 방의 정보 변경 관련
(def token-1-blackjack
              [:club-K     ])
               ;:spade-A   :spade-K :club-9  :diamond-10 :club-8
               ;:heart-Q    :diamond-9 :heart-A :heart-8 :diamond-7  :heart-4  :heart-3 :diamond-5
               ;:heart-7    :diamond-J :spade-6 :spade-3 :club-A     :spade-2])

(def all-bust [:club-K     :spade-K   :club-Q  :club-J  :heart-A    :diamond-J
               :heart-Q    :diamond-9 :heart-A :heart-8 :diamond-7  :heart-4  :heart-3 :diamond-5
               :heart-7    :diamond-J :spade-6 :spade-3 :club-A     :spade-2])

(def all-lose [:club-3     :spade-2   :club-2  :club-4  :heart-A    :diamond-J
               :heart-Q    :diamond-9 :heart-A :heart-8 :diamond-7  :heart-4  :heart-3 :diamond-5
               :heart-7    :diamond-J :spade-6 :spade-3 :club-A     :spade-2])

(def dealer-blackjack
              [:club-3     :spade-2   :club-2  :club-4  :heart-A    :diamond-1
               :heart-2    :diamond-K :heart-A :heart-8 :diamond-7  :heart-4  :heart-3 :diamond-5
               :heart-7    :diamond-J :spade-6 :spade-3 :club-A     :spade-2])


(def ace-split
              [:club-3     :spade-2   :club-A  :heart-A :club-2     :diamond-1
               :heart-2    :diamond-K :heart-A :heart-8 :diamond-7  :heart-4    :heart-3 :diamond-5
               :heart-7    :diamond-J :spade-6 :spade-3 :club-A     :spade-2])

(def split*3
              [:club-3     :spade-4   :club-2  :heart-2 :diamond-1  :spade-2
               :diamond-2  :diamond-K :heart-A :heart-8 :diamond-3  :heart-4    :heart-3 :diamond-5
               :heart-7    :diamond-J :spade-6 :spade-3 :club-A     :club-2])


(defn shuffle-cards []
  (let [cards (shuffle  cards*)]
    ;token-1-blackjack
    cards
  ))

(defn- <!room-cards
  ([room-id]  (:cards (room-id @room-list*)))
  ([room-id card-index] (get (<!room-cards room-id) card-index) ))


;;;;;;;;;;;;;; 방 생성 관련
(defn- init-room[]
  (let [room-id      (keyword (str "k" (java.util.UUID/randomUUID))) ]
        (merge room*  {:cards (shuffle-cards) :room-id room-id  :users {} }) ))


(defn- >!room-to-room-list [room]
  (swap! room-list* assoc  (:room-id room) room))


(defn- create-room
  "방을 생성한다."
  []
  (let [room  (init-room)]
    (>!room-to-room-list room)
    room))


;;;;;;;;;;;;;; client에 보낼 room정보
(defn- room->client
  "방에 있는 카드는 제외하고 client에 보내준다."
  [room]
  (let [room (dissoc room :cards :ch :betting-time)]
    room))





;;;;;;;;;;;;;; 방 입장 관련
(defn- <!join-possbile-room
  "5인 이하의 방을 가져온다.
  1. room-list의 room을 전수검사
  2. 여러개의 room 중에 첫번재 방을 반환한다
  3. 단 방이 없으면 새로운 방을 만들어서 반환한다."
  []
  (swap! room-list* dissoc nil)
  (let [rooms (filter #(< (count (:users %))  1) (vals @room-list* ))
        join-room-list (filter (fn [x] (= :init (:state x)) ) rooms) ]


    (if (empty? join-room-list)
      (do
        (println "create room")
        (create-room) )
      (do
        (println "return existed room room-id ->" (:room-id (first join-room-list)) )
        (first join-room-list)) )) )

(defn- <!room [room-id]
  (if (exist-room? room-id)
    (do
      (room-id @room-list*)))
  )


(defn- >!user-to-room
  "방에 유저가 입장한다."
  [user room-id]
  (if (exist-room? room-id )
    (swap! room-list* update-in [room-id :users] conj user ))
  (<!room room-id) )


;;;;;;;;;;;;;;; User 정보 변경 관련
(defn- change-user-value
  "chage-user-value :token :room-id :state :bet :seat 1"
  [token room-id & keys-data]
  (if (exist-room? room-id)
    (let [keys-data' (apply assoc{} (vec keys-data))]
      (swap! room-list* update-in [room-id :users token ]  merge keys-data'))))

(defn- change-user-state [token room-id state]
  (if (exist-room? room-id)
    (swap! room-list* update-in [room-id :users token ] assoc :state state)) )


(defn- update-user-money
  "총 금액, 현재 게임 중인 금액이 update 된다."
  [token room-id point]
  (if (exist-room? room-id)
    (swap! room-list* update-in [room-id :users token :betting-money-result] (fn[a]  (+ a point)))
    (swap! room-list* update-in [room-id :users token :total-money] (fn[a]  (+ a point)))) )


(defn- change-dealer-point [room-id point]
  (if (exist-room? room-id)
    (swap! room-list* update-in [room-id :dealer :decks :deck0] assoc :point point) ) )


(defn- change-dealer-state [room-id state]
  (if (exist-room? room-id)
    (swap! room-list* update-in [room-id :dealer :decks :deck0] assoc :state state) ))


;;;;;;;;;;;;;; 유저 관련..
(defn- none-state-user? [{:keys [state]}]
  (#{:none} state))

(defn- in-game-user?
  "유저의 state이다. card의 state는 아니다. 헛갈리지 말것."
  [{:keys [state]}]
  (#{:in-play} state))

(defn- wait-state-user? [{:keys [state]}]
  (#{:wait} state))

(defn- users-in-room
  "방에 있는 유저 목록을 불러온다."
  [room-id]
  (vals (:users (<!room room-id))))


(defn- waiting-user?
  "단순이 방에 seat한 유저인가... seat유저는 user state가 wait이다. 입장한 유저는 none"
  [user]
  (= :wait (:state user)))


(defn- <!state-user
  "state에 만족하는 유저를 return한다."
  [room-id state]
  (filter #(= (:state %) state) (users-in-room room-id)))


(defn- <!user [token room-id]
  (token (:users (<!room room-id)) ))


(defn- <!user-in-room [room-id]
   (vals (get-in @room-list* [room-id :users ])))

(defn <!playing-game-user
  "현재 게임중인 유저만 추출한다."
  [room-id]
  (let [users (users-in-room room-id)]
    (filter in-game-user? users) ))

(defn <!user-card-state [token room-id split-index]
  (if (exist-room? room-id)
    (get-in @room-list* [room-id :users token :decks split-index :state]) ))



(defn- user-all-bet?
  "모든 유저가 다 끝을 냈는가
   유저의 state값은 none(입장) wait(seat) in-play(betting)"
  [room-id]
  (let [users (users-in-room room-id)
        seat-user-count (count (filter #(and (not= (:seat %) :wait)
                                             (not= (:seat %) :none)) users))
        bet-user-count  (count (filter #(= (:state %) :in-play)  users))]
    (= seat-user-count bet-user-count) ))


(defn- <!seat-num-user [seat-num room-id]
  (let [users (<!user-in-room room-id)]
    (filter #(= seat-num (:seat %)) users )) )


(defn- clean-user [token room-id]
  (if (exist-room? room-id)
    (let [users (<!user token room-id)]
      (swap! room-list* update-in [room-id :users token :decks ] merge (make-split-card 0) )
      (change-user-state  token room-id :wait)) ))


;;;;;;;;;;;;;;;;; 방 관련
(defn- exist-seat-num? [room-id seat-num]
 (let [users (users-in-room  room-id )
       e (empty? (filter #(= seat-num (:seat %)) users) )]
     (if e false true) ))

(defn- exist-room? [room-id]
  (util/not-nil? (get-in @room-list* [room-id])))



(defn- change-room-state [room-id state]
  (if (exist-room? room-id)
    (swap! room-list* update-in [room-id] assoc :state state)))

(defn- change-room-value
  "change-room-value :room-id :state :bet :seat 1"
  [room-id & keys-data]
  (if (exist-room? room-id)
    (let [keys-data' (apply assoc{} (vec keys-data))]
      (swap! room-list* update-in [room-id]  merge keys-data'))))

(defn- change-room-wait-state
  "일정 시간(15초)동안 유저를 찾아 대기한 상태."
  [user-ch wait-limit-time room-id]
  (if (exist-room? room-id)
    (let [bet-completed (chan)]
      (go (loop [token (<! user-ch)]
            (if token ;; chanel date가 있을 경우 room의 user정보를 update한다.
              (do
                (change-user-state token room-id :in-play)
                (let [wait-user (<!state-user room-id :wait)]
                  (if (empty? wait-user)
                    (>! bet-completed 1) ))
                (recur (<! user-ch) ))  ; end do;;
              (>! bet-completed 0) )) )

      (go (let [ch-time-out (timeout wait-limit-time)
                [v ch] (alts! [ch-time-out bet-completed])]
            (if (or (= v 0 ) (nil? v))
              (change-room-state room-id :ch-timeout)
              (do
                (change-room-state room-id :play)
                (let [t (timeout 1000) ]
                  (go (<! t)
                      (hand-betting-card->room room-id)
                                        ;(calc-user-card-point room-id (into{} (<!state-user room-id :in-play)))
                      (sender/broadcast-room-user :bet (room->client (room-id @room-list*)) )
                      (check-blackjack room-id)
                      )) ))

            (close! user-ch)  ))  )) )

(defn- change-room-to-wait
  "room에 channel을 만든 후 유저 채널을 대기할 수 있는 상태로 변경한다."
  [room-id]
  (if (exist-room? room-id )
    (let [room-state (get-in @room-list* [room-id :state])]
      (if (= room-state :init )
        (do
          (if (exist-room? room-id)
            (swap! room-list* update-in [room-id] assoc :ch (chan 5) ))
          (change-room-state room-id :wait)
          (let [ch (get-in @room-list* [room-id :ch])]
            (change-room-wait-state ch 15000 room-id))) ))) )



(defn- playing-room?
  "play만 하는 방"
  [room-id]
  (let [user (users-in-room room-id)
        cnt (count (filter in-game-user? user))]
    (= cnt 0)))

(defn- empty-room?
  "사람 없는 방"
  [room-id]
  (= (count (<!user-in-room room-id)) 0) )

(defn- only-wait-user-room?
  "기다리는 유저만 있는 방"
  [room-id]
  (let [user (users-in-room room-id)
        cnt (count (filter wait-state-user? user))]
    (= cnt 0)))

(defn- only-none-user-room?
  "기다리는 유저만 있는 방"
  [room-id]
  (let [user (users-in-room room-id)
        cnt (count (filter none-state-user? user))]
    (= cnt 0)))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;; 카드 관련

(defn- change-user-card-value
  "change-user-card-value :token :room-id :deck0 :betting-money 100 :point 200"
  [token room-id split-index & keys-data]
  (if (exist-room? room-id)
    (let [keys-data' (apply assoc{} (vec keys-data))]
      (swap! room-list* update-in [room-id :users token :decks split-index]  merge keys-data') )))

(defn- remove-user-card-state [token room-id split-index key]
  (if (exist-room? room-id )
    (swap! room-list* update-in [room-id :users token :decks split-index]
           (fn [a] (dissoc a key) )) ))

(defn- update-user-card-betting-money [token room-id split-index  betting-money]
  (if (exist-room? room-id )
    (swap! room-list* update-in [room-id :users token :decks split-index :betting-money ]
           (fn [a] betting-money)) ) )


(defn <!user-split-card
  "input split-index -> :s0
   return {:cards [:heart-6], :betting-money 100, :point 0, :state :hit, :cmd :hit }"
  [token room-id split-index]
  (let [card-data (:decks (<!user token room-id))]
    (split-index card-data)) )

(defn <!user-card-point [token room-id split-index]
  (let [card-data (<!user-split-card token room-id split-index)]
    (:point card-data)))

(defn- clear-playing-card?
  "card의 state가 hit이 아니면 종료라는 의미가 된다."
  [room-id]
  (let [cards (map  #(:decks %)   (<!playing-game-user room-id))
        play-card (for [c cards ] ;play-stage-set(진행중인 state)- >hit, wait, bet
                    (filter #(play-stage-set (:state %) )(vals c) )) ]
    (every? empty? play-card) )) ;; hit, wait, bet가 없으면 게임 종료된 의미가 된다.


(defn- change-user-card-key-name
  "room에 속한 모든 유저의 카드 key의 이름을 변경한다.
   change-user-card-key-name room-kd :betting-money :betting
      {:cards [:diamond-K :spade-2], :betting-money 0, ~~}
   -> {:cards [:diamond-K :spade-2], :betting 0, ~~}"
  [room-id ori-key to-key]
  (let [user (<!user-in-room room-id)]
    (loop [u user]
      (if (nil? u)
        nil
        (do
          (loop [decks (:decks (first u))]
            (if (nil? decks)
              nil
              (do
                (let [split-card   (first decks) ;	split-card -> [:s1 {:cards [:diamond-K :spade-2], :betting-money 0, :point 0, :state :bust}]
                      split-index  (first split-card) ; :s1
                      card-data    (second split-card)] ; {:cards [:diamond-K :spade-2], :betting-money 0, ~~}
                  (if (exist-room? room-id)
                    (swap! room-list* update-in [room-id :users (:token (first u)) :decks split-index ]
                           (fn [a]
                             (if (contains? a ori-key)
                               (clojure.set/rename-keys a {ori-key to-key})) ))) )
                (recur (next decks)))))
          (recur (next u))))
      )) )


(defn- same-all-card-state?
  "게임중인 유저의 카드의 상태가 전무 state-set인가 여부
  input state-set :ex-> #{:bet :wait}
  (same-all-card-state? room-id :state #{:wait}
  (same-all-card-state? room-id #{:wait}"
  ([room-id state-set]
     ( same-all-card-state? room-id :state state-set) )
  ([room-id key state-set]
     (let [cards (map  #(:decks %)   (<!playing-game-user room-id))
           play-card (for [c cards ]    ; (() (:aa))
                       (filter #(state-set (key %) )(vals c) )) ]
       (every? not-empty play-card) ) ) )


(defn- hand-card-to-user
  "token -> keyword , room-id -> keyword, cards -> [], split-index -> :deck0 or :deck1.
  블랙잭 계산도 한다."
  [token room-id cards split-index]
  (let [vec-cards (if (vector? cards) ; :spade-A or [spade-A] 의 형태 input값이 입력된다.
                    cards
                    (vector cards))]
    (doseq [card vec-cards]
      (if (exist-room? room-id)
        (swap! room-list* update-in [room-id :users token :decks split-index :cards] conj card)) ) ))


(defn- hand-card-to-dealer
  "input vec-card [:spade-2 :spade-3]"
  [room-id vec-cards]
  (doseq [card vec-cards]
    (if ( exist-room? room-id )
      (swap! room-list* update-in [room-id :dealer :decks :deck0 :cards] conj card ))) )


(defn- drop-card-in-room [room-id end-index]
  (if (exist-room? room-id)
    (swap! room-list* update-in [room-id :cards] #(vec (drop end-index %)))))

(defn- make-split-card
  "새로운 index는 1 or 2 or 3 이다.
   새로운 카드 정보를 생성한다."
  [index]
  (hash-map (keyword (str "deck" index)) {:cards [] :betting-money 0 :point 0 :view-point 0 :state :wait :cmd :none :insurance 0} ))


(defn- split-ace-card [token room-id current-split-index next-split-index ]
      ;; Deck0에 카드 한 장만 추가된 room 정보전달
      ;; Deck0의 정산된 room 전달
      ;; Deck1에 카드 한 장만 추가된 room 정보전달
      ;; Deck1에 정산된 room 전달
  (let [t (timeout 1000) ] ;; Deck0에 카드 한 장만 추가된 room 정보전달
    (go (<! t)
        (hand-card-to-user token room-id (<!room-cards room-id 0 ) current-split-index) ;; 카드 한장 더 준다.
        (drop-card-in-room room-id 1)
        (sender/broadcast-room-user :split (room->client (<!room room-id)))))

  (let [t (timeout 2000) ] ;; Deck0의 정산된 room 전달
    (go (<! t) ;;Deck1의 :state가 :stand이면 카드점수계산이 되기 때문에 :bet 후 :stand로 변경한다.
        (calc-user-split-card-point token room-id current-split-index)
        (sender/broadcast-room-user :split (room->client (<!room room-id))) ))

  (let [t (timeout 3000) ] ;; Deck1에 카드 한 장만 추가된 room 정보전달
      (go (<! t)
          (hand-card-to-user token room-id (<!room-cards room-id 0 ) next-split-index)  ;; 카드 한 장 더.
          (drop-card-in-room room-id 1)
          (sender/broadcast-room-user :split (room->client (<!room room-id)))  ))
  (let [t (timeout 4000) ] ;; Deck1에 정산된 room 전달
      (go (<! t)
          (calc-user-split-card-point token room-id next-split-index)
          (sender/broadcast-room-user :split (room->client (<!room room-id)))
          (if (clear-playing-card?  room-id)
            (do
              (calc-room room-id)
              (send-clean-room&change-wait room-id 1500))))) )

(defn- >!split-card
  "split-index :deck0 or :deck1
   ace로 스플릿 할 경우 카드 1장만 받고 stand 된다."
  [token room-id current-split-index]
  (let [card                 (:decks (<!user token room-id ))
        split-card           (:cards (current-split-index card))
        betting-money        (:betting-money (current-split-index card))
        split-index          (inc (. Integer parseInt (subs (str current-split-index) 5 6))) ;:s0 -> 0 으로 나온다."
        next-split-cards     (make-split-card 1) ; {:deck0 {:cards [] :betting-money 0 :point 0 :state :wait :cmd :none  :insurance 0}
        next-split-index     (keyword (str "deck" split-index )) ; :deck1
        next-split-exist     (get-in @room-list* [room-id :users token :decks next-split-index]) ]

    (if (and (util/not-nil? next-split-exist) ;; :deck1을 :deck2로 이름변경한다.
             (exist-room? room-id) )
      (swap! room-list* update-in [room-id :users token :decks]
             (fn [a]
               (clojure.set/rename-keys
                a {next-split-index (keyword (str "deck" (inc split-index) ))})) ))

    (if (exist-room? room-id)
      (do
        (swap! room-list* update-in [room-id :users token :decks current-split-index  :cards]
               (fn [card] (pop split-card)))
        (swap! room-list* update-in [room-id :users token :decks]
               conj  next-split-cards) ;; split card {:s1 {:cards {} ~~}} 추가
        (swap! room-list* update-in [room-id :users token :decks next-split-index :cards]
               (fn [card] [(peek split-card)])) )) ;;[:spade-1 :spade-2] 추가.

    (update-user-money token room-id (- betting-money))
    (change-user-card-value token room-id next-split-index :betting-money betting-money) ;; 배팅금액
    (change-user-card-value token room-id next-split-index :state :bet)

    (if (cards/ace-card? (peek split-card)) ;; 카드 분배만 처리
      (do ;; client와 약속이다. stand이다.
        (change-user-card-value token room-id current-split-index :state :stand)
        (change-user-card-value token room-id next-split-index :state :stand))
      (do
        (hand-card-to-user token room-id (<!room-cards room-id 0 ) current-split-index) ;; 카드 한장 더 준다.
        (drop-card-in-room room-id 1)) )

    (if (cards/ace-card? (peek split-card)) ; 카드 client 전송 처리
      (split-ace-card token room-id current-split-index next-split-index)) ))


(defn- hand-betting-card->room
  "state가 :bet한 유저만 카드 준다.
   방에 Seat한 다음에 모든 유저들에게 카드를 전달한다."
  [room-id]
  (let [bet-user  (<!state-user room-id :in-play)
        room-card (<!room-cards room-id)]
    (loop [users bet-user card-index 1]
      (let [last-index   (* card-index 2) ;[0 2] [2 4] [4 6] 값대로 카드가 추출된다.
            first-index  (- last-index 2)
            extract-card (subvec room-card first-index last-index)] ; [:space-A :space-2]
        (if (empty? users)
          (do
            (hand-card-to-dealer room-id (conj [(first extract-card)] :back )) ;:back은 반드시 맨 나중으로 되엉 한다.
            (drop-card-in-room room-id (- (* 2 card-index) 1))) ; 딜러는 :space-a :back으로 전달.-> 카드 한장을 배포 안한다.-1을 한 이유

          (do
            (if (nil? (first users))
              (do
                (println "bug---bug---bug---bug---bug---bug---bug---bug---bug"
                         (println (bet-user)))
                (println "***bug***bug***bug***bug***bug***bug***bug***bug***bug")))
            (hand-card-to-user (:token (first users)) room-id extract-card :deck0) ; 2장의 카드를 유저의 split-index 0에 전달한다.
            (if (blackjack? extract-card)
              (change-user-card-value (:token (first users)) room-id :deck0 :temp-state :blackjack :view-point "BlackJack" ))
            (calc-user-card-point room-id  (first users))
            (recur (next users) (inc card-index))) ))) ))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;; 점수 계산
(defn- close-21
  "input point : [21] [12] or [22]
   (close-21 (apply vector (concat [133] [2])))"
  [point]
  (let [[a b] point]
    (if b ;; b에 데이터가 있으면
      (cond
       (and (<  21 a)  (<  21 b)) [a] ; 값을 리턴한다. 점수 계산할때는 bust는 제외된다.
       (and (>= 21 a)  (>= 21 b)) [(max a b)]
       (or  (<  21 a)  (<  21 b)) [(min a b)]
       :else [a] )                      ; [1] [1]일경우
      (if (< 21 a)
        [a]
        [a])) ))

;    (if b ;; 값이 2개인 경우
;      (if (< 21 a) a ;; 모두 21보다 큰 경우
;        (if (< 21 b) a ;; b 만 큰 경우
;          b))
;      a)



(defn- clean-room [room-id]
  (let [room-users (<!playing-game-user room-id)]
    (loop [users  room-users] ;  유저에게 카드를 돌린다.
      (if (empty? users)
        (do  ;; 딜러의 정보를 초기화, 카드 정보
          (if (exist-room? room-id)
            (swap! room-list* assoc-in [room-id :dealer :decks :deck0 :cards ] []))
          (change-dealer-point room-id 0)
          (change-dealer-state room-id :wait)
          (if (exist-room? room-id)
            (swap! room-list* assoc-in [room-id :cards] (shuffle-cards))))
        (do ;; 유저의 정보를 초기화
          (let [user (first users)
                token(:token user)]
            (if (exist-room? room-id)
              (swap! room-list* update-in [room-id :users token :decks ] merge (make-split-card 0) ))
            (change-user-state  token room-id :wait))
          (recur (next users)) ) ))
    (change-room-state room-id :init)
    (<!room room-id)))


(defn- send-clean-room&change-wait [room-id delay-time]
  (go (<! (timeout delay-time))
      (if (exist-room? room-id)
        (do
          (sender/broadcast-room-user :clean (room->client (clean-room room-id)))
          (change-room-to-wait room-id)  )) ))


(defn- calc-dealer-card
  "17점에 맞춰서 딜러의 카드를 분배한다.
   return value -> [[:spade-A :spade-2] 22]"
  [room-id]
  (if (exist-room? room-id)
    (swap!  room-list* update-in [room-id :dealer  :decks :deck0 :cards] (fn [card] (pop card))) ) ;:back 카드를 제거한다.
  (let [room (<!room room-id)
        dealer-card (get-in @room-list* [room-id :dealer :decks :deck0 :cards])
        point (first (cards/calc-dealer-point dealer-card))]
    (loop [x point card dealer-card ]
      (if (>= x  17)
        (do
          (if (blackjack? card)
            (change-dealer-state room-id :blackjack) )
          [card x] )
        (do
          (let [new-cards (conj card (<!room-cards room-id 0))
                new-point (first (cards/calc-dealer-point new-cards))]
            (drop-card-in-room room-id 1)
            (recur new-point new-cards) )) )) ))

(defn- blackjack?
  "input cards-> [:spade-1 :spade-1]
   split일경우 카드가 2장일 경우에 블랙잭이 아니다."
  [cards]
  (let [point (cards/calc-point cards)
        point1 (first (close-21 (apply vector (concat point))))]
    (if (and (= (count cards) 2)
             (= point1 21))
      true
      false) ))

(defn- check-blackjack [room-id]
  ;; 카드 배포 할 때 blackjack이 있으면
  ;; blackjack state는 시간 여유를 두고 :state를 정보를 보내준다.
  (if (same-all-card-state? room-id :temp-state #{:blackjack})
    (do
      (go (<! (timeout 1500))
          (change-user-card-key-name room-id :temp-state :state)
          (sender/broadcast-room-user :bet (room->client (<!room room-id))) )

      (go (<! (timeout 1500))
          (calc-user-money room-id)
          (send-clean-room&change-wait room-id 1000)) )) )


(defn- calc-user-split-card-point
  "카드 포인트 리턴한다."
  [token room-id  split-index]

  (let [user (<!user token room-id)
        user-cards (:decks user) ;{:s0 {:cards [:diamond-K :spade-2], :betting-money 0, :point 0, :state :bust} :s1{}
        split-card (split-index user-cards) ;{:cards [:heart-9 :club-5 :club-K], :betting-money 100, :point 0, :state :stand}
        cards (:cards split-card)       ;[:heart-9 :club-5 :club-K]
        point (cards/calc-point cards)
        point1 (first (close-21 (apply vector (concat point))))
        view-point (cards/point->string point)]

    (change-user-card-value token room-id split-index :point point1)

    (if (> point1 21 )
      (change-user-card-value token room-id split-index :state :bust :win false :view-point "bust")
      (change-user-card-value token room-id split-index :view-point view-point))

    (if (= :blackjack (:temp-state split-card)) ;; blackstate는 hand-betting-card->room에서 체크
      (change-user-card-value token room-id split-index :view-point "BlackJack"))  ))




(defn calc-user-card-point [room-id user]

  (loop [card (:decks user)]
    (if (nil? card)  ; {:s0 {:cards [:diamond-K :spade-2], :betting-money 0, :point 0, :state :bust} :s1{}
      (do
        (println "calculate users card point end. fn : calc-user-card-point"))
      (do
        (let [split-card   (first card)  ;	split-card -> [:s1 {:cards [:diamond-K :spade-2], :betting-money 0, :point 0, :state :bust}]
              split-index  (first split-card)]
          (calc-user-split-card-point (:token user) room-id split-index ))
        (recur (next card))) )))


(defn- calc-user-blackjack
  "블랙잭 계산이다."
  [token room-id betting-money]

  (let [corr-bet-money (int (* 2.5 betting-money))]
    (update-user-money token room-id corr-bet-money)
    (change-user-card-value token room-id :deck0 :win true )) )


(defn- calc-compare-card-point
  "split-card -> [:s1 {:cards [:diamond-K :spade-2], :betting-money 0, :point 0, :state :bust}].
  card-state가 :bust일 경우는 호출되지 않아 card-point가 22일 경우 고려되지 않는다."
  [token room-id user-split-card]
  (let [user-card-state         (:state (second user-split-card))
        user-card-point         (:point (second user-split-card))
        insurance-money         (:insurance (second user-split-card))
        split-index             (first user-split-card)
        betting-money           (:betting-money (second user-split-card))
        dealder-card-state      (get-in @room-list* [room-id :dealer :decks :deck0 :state])
        dealer-card-point       (get-in @room-list* [room-id :dealer :decks :deck0 :point])]

    (condp = dealder-card-state
      :blackjack      (do
                        (update-user-money token room-id (* 2 insurance-money))
                        (change-user-card-value token room-id split-index :win false))
      :bust           (do ;; 이 함수에서 bust인 유저는 없다. user는 stand or doubledown 이기때문에 유저승이다.sqrt
                        (update-user-money token room-id (* 2 betting-money))
                        (change-user-card-value token room-id split-index :win true))
      (do
        (cond
         (> user-card-point dealer-card-point) (do
                                                 (update-user-money token room-id (* 2 betting-money))
                                                 (change-user-card-value token room-id split-index :win true))
         (= user-card-point dealer-card-point) (do
                                                 (update-user-money token room-id betting-money)
                                                 (change-user-card-value token room-id split-index :win true))
         (< user-card-point dealer-card-point) (do ;돈 차감필요가 없다. 미리 betting할 때 차감했다.
                                                 (change-user-card-value token room-id split-index :win false)))) )))


(defn- calc-user-money
  "유저의 승패에 따른 돈 계산작업이다..
  calc-compare-card-point, calc-user-blackjack 과 같이 사용한다.
  유저 카드 state (blackjack, stand, doubledown)만 돈 게산한다."
  ([room-id]  (calc-user-money room-id (<!playing-game-user room-id) ))
  ([room-id in-users]
   (let [dealer-point (get-in @room-list* [room-id :dealer :decks :deck0 :point])]
     (loop [users in-users]
       (if (nil? users)
         (println "calced victory and defeat  end. fn : calc-user-money")
         (let [user (first users)]
           ;; 승리할 때는 betting 금액의 2배
           (loop [card (:decks user)] ; {:s0 {:cards [:club-7 ~],:betting-money 100, :point 0, :state :stand}, :s1 {~}}
             (if (nil? card)
               (do
                 (println "calc-user-money card end"))
               (do
                 (calc-user-card-point room-id user)
                 (let [split-card              (first card);split-card -> [:s1 {:cards [:diamond-K :spade-2], :betting-money 0, :point 0, :state :bust}]
                       user-card-state         (:state (second split-card))
                       user-card-point         (:point (second split-card))
                       betting-money           (:betting-money (second split-card))]
                   (condp = user-card-state
                     :blackjack  (calc-user-blackjack (:token user) room-id betting-money) ; 유저가 먼저 blackjack이면  승이다.
                     :stand      (calc-compare-card-point (:token user) room-id split-card)
                     :double-down (calc-compare-card-point (:token user) room-id split-card)
                     (println "don't need calc state " user-card-state))
                  (recur (next card))  ))))
          ( recur (next users))) )))) )



(defn- calc-user [room-id]
  (calc-user-money room-id))


(defn- calc-room [room-id]
  (let [calced-dealer-cards-data (calc-dealer-card room-id) ; [[:spade-3 :spade-2 :club-10] 20] ]
        calced-dealer-cards (vec (rest (first calced-dealer-cards-data)))
        calced-dealer-point (second calced-dealer-cards-data)]

    (go (loop [cards  calced-dealer-cards   t (timeout 1000) ]
        (if (not (seq cards))
            (do
              (let [dealer-card-cnt (count calced-dealer-cards)
                    stand-time-out  1000
                    clean-room-time-out (+ stand-time-out 3000)
                    init-time-out (+ clean-room-time-out 3000)]

                (go (<! (timeout stand-time-out))
                    (change-dealer-point room-id calced-dealer-point)
                    (if (> calced-dealer-point 21)
                      (change-dealer-state room-id :bust))
                    (calc-user room-id)
                    (sender/broadcast-room-user :stand (room->client (<!room room-id))) )

                (go (<! (timeout clean-room-time-out))
                    (change-room-state room-id :clean)
                    (sender/broadcast-room-user :stand (room->client (<!room room-id))) )

                (send-clean-room&change-wait room-id init-time-out)
                (println "finish up calc-room. fn : calc-room")) )

            (do (<! t) ;; 딜러카드 한장 한장씩 client에 보내준다.
              (if (exist-room? room-id)
                (do
                  (swap! room-list* update-in [room-id :dealer :decks :deck0 :cards] conj (first cards))
                  (swap! room-list* assoc-in  [room-id :dealer :decks :deck0 :point] (cards/calc-point (get-in @room-list* [room-id :dealer :decks :deck0 :cards]) ))
                  (sender/broadcast-room-user :calc (room->client (<!room room-id)))
                  (recur (next cards) (timeout 1000))) )) ))) ))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;; 외부 접근 가능 함수

;; 방 입장
(defn req-join-room
  "유저를 방에 입장 시킨다.
   유저-> :state :none 카드 -> :cards {:s0 {:cards [], :state :wait, :cmd :none"
  [user]
  (let [join-room (<!join-possbile-room)
        room      (>!user-to-room user (:room-id join-room))]
    (room->client room) ))


;; 좌석 입력
(defn req-seating-in-room
  "room의 좌석에 user를 위치시킨다.
   token  : keyword  room-id : keyword  position : 방에서의 좌석 위치
   seat을 하게 되면 유저의 :state는 wait가 된다. 단순이 방에 입장일 경우는 :none이다."
  [token room-id seat]
  (if (= false (exist-seat-num? room-id seat))
    (do
      (change-room-to-wait room-id)
      (change-user-value token room-id :state :wait :seat seat)))
  (room->client (room-id @room-list*)))


;; 방 퇴장
(defn req-remove-user [token room-id]
  (if (exist-room? room-id)
    (do
      (swap! room-list* update-in [room-id :users] dissoc  token) ) )


  (cond
   (= true (empty-room? room-id))     (do
                                        (swap! room-list* dissoc room-id))


   ;(or (= true (only-wait-user-room? room-id))
   ;    (= true (only-none-user-room? room-id)))  (do
   ;                                                (send-clean-room&change-wait room-id 0) )
   ))


;; 로그인
(defn req-login
  "token -> keyword"
  [token]
  (let [user' (assoc user :decks (make-split-card 0) :token token :name (str (name token) "-name"))]
    (assoc {} token user')
    ))


(defn- betting-time [room-id]
  (let [betting-gap 500
        before-betting-time (if-let [bt (get-in @room-list* [room-id :betting-time])]
                              bt
                              (System/currentTimeMillis))]
    (- betting-gap (- (System/currentTimeMillis) before-betting-time))) )


;; betting 하기...
(defn req-betting-money
  "betting 하게 되면 유저 state는 in-play상태
   split card의 state는 wait 상태로 변경
   split index의 default인 :s0로 betting이 된다."
  [token room-id betting-money]
  ;(println "*****************************************************")
  ;(println "room-state-->" (get-in @room-list* [room-id :state]))
  (if  (and (util/not-nil? (#{:wait} (<!user-card-state token room-id :deck0)))
            (or  (= :wait       (get-in @room-list* [room-id :state]))
                 (= :ch-timeout (get-in @room-list* [room-id :state])) ))
    (do
      (change-user-value token room-id :state :in-play)
      (change-user-card-value token room-id :deck0 :betting-money betting-money)
      (change-user-card-value token room-id :deck0 :state :bet)
      (change-user-card-value token room-id :deck0 :cmd   :bet)
      (update-user-money token room-id (- betting-money))
      (let [room-state (get-in @room-list* [room-id :state])]
         (if (= room-state :ch-timeout)
           (do
             ;ch-time의 경우는 모든 유저가 일정 시간동안 betting을 하지 않았다.
             ;그래서 room은 대기 중. 이 때 한 명이 betting하면 그 사람만 play 된다.
             ;1.배팅 머니 client전달.
             ;2.카드 정보 client 전달.
             (change-room-state room-id :play)
             (hand-betting-card->room room-id)
             (sender/broadcast-room-user :bet (room->client (<!room room-id)))
             (check-blackjack room-id) )

           (do
             ;1.배팅 머니 client에 전달(배팅머니 전달할 때 순차적으로 전달.)
             ;2. 카드 정보 전달
             ; 카드 정보는 채널에서 finish될 때 완료 됨.
             (let [time-out (betting-time room-id)]
               (change-room-value room-id :betting-time (System/currentTimeMillis))

               (go (<! (timeout time-out))
                 (sender/broadcast-room-user :bet (room->client (<!room room-id)) ))

               (let [ch (get-in @room-list* [room-id :ch])]
                 (go (>! ch token)))) )) ))

    (room->client (room-id @room-list*))))



;; 유저 카드를 더 받는다.
(defn req-hit
  "split-index :s0 or :s1 or :s2"
  [token room-id split-index]
  (if (util/not-nil? (#{:hit :bet} (<!user-card-state token room-id split-index)))
    (do
      (hand-card-to-user token room-id (<!room-cards room-id 0) split-index)
      (change-user-card-value token room-id split-index :state :hit)
      (change-user-card-value token room-id split-index :cmd   :hit)
      (drop-card-in-room room-id 1)
      (calc-user-split-card-point token room-id split-index)

      ;; 유저 DECK card의 state가 bust이면 dealer-card 정산 필요없다.
      (if (> (<!user-card-point token room-id split-index) 21)
        (do
                                        ;1. card 정보만 보내준다. (:state는 :bust의 이전 값. 여기서는 :hit가 된다.)
                                        ;2. :state 정보 보내준다.
                                        ;3. :win true/false 정보 보내준다.
                                        ;4. clean방 보내준다.
          (go (<! (timeout 1000))       ; 1번
              (change-user-card-value token room-id split-index :state :hit :win :none)
              (sender/broadcast-room-user :hit (room->client (<!room room-id))) )
          (go (<! (timeout 1500))       ; 2번
              (change-user-card-value token room-id split-index :state :bust)
              (sender/broadcast-room-user :hit (room->client (<!room room-id))) )

          (go (<! (timeout 2000))       ; 3번
              (change-user-card-value token room-id split-index :win false)
              (sender/broadcast-room-user :hit (room->client (<!room room-id))) )
          (if (same-all-card-state? room-id #{:bust})
            (go (<! (timeout 2500))     ; 4번
                (send-clean-room&change-wait room-id 500))) )

        (if (clear-playing-card?  room-id)
          (do
            (println "req-hit clean")
            (calc-room room-id))
          (room->client (room-id @room-list*)) )) )


    (room->client (room-id @room-list*)) ))



;; stand
(defn req-stand
  "split-index :deck0 or :deck1 or :deck2"
  [token room-id split-index]
  (if (util/not-nil? (#{:bet :hit} (<!user-card-state token room-id split-index) ))
    (do
      (change-user-card-value token room-id split-index :state :stand)
      (change-user-card-value token room-id split-index :cmd   :stand)
      (if (clear-playing-card?  room-id)
        (calc-room room-id)
        (room->client (<!room room-id)) ))
    (room->client (<!room room-id))))


;; split 요청
(defn req-split
  "split-index : deck0 or :deck1"
  [token room-id current-split-index]

  (let [state (#{:bet} (<!user-card-state token room-id current-split-index) )
        cnt (count (get-in @room-list* [room-id :users token :decks]) )]
    (if (and (util/not-nil? state) (< cnt 3))
      (>!split-card token room-id current-split-index)
      (do
        (if (same-all-card-state? room-id #{:bust})
          (calc-user-money room-id))

        (if (clear-playing-card?  room-id)
          (do
            (calc-room room-id))
          (room->client (room-id @room-list*))) )))

  (room->client (room-id @room-list*)))


;; 더블다운
(defn req-doubledown
  "배팅머니 *2가 된다. cards의 batting money 증가. 현재 게임 결과 머니 차감 :total-money 차감. "
  [token room-id split-index]
  ;; 1. :state :double-down :betting-money 그대로 :double-down true
  ;; 2. :state :double-down :betting-money *2배   :double-down true <- 삭제
  ;; 3. :state :double-down :betting-money *2배    카드 한장 더
  ;; 4. 유저 게임 점수 계산
  (if (util/not-nil? (#{:bet :hit} (<!user-card-state token room-id split-index) ))
    (do
      (go (<! (timeout 0))
          (change-user-card-value token room-id split-index
                                  :double-down true :state :double-down :cmd :double-down)
          (sender/broadcast-room-user :double-down (room->client (room-id @room-list*)) ))

      (go (<! (timeout 500))
          (let [split-card (<!user-split-card token room-id split-index) ;{:cards [:heart-6], :betting-money 100, :point 0, :state :hit, :cmd :hit }
                betting-money (:betting-money split-card)]
            (remove-user-card-state token room-id split-index :double-down)
            (update-user-money token room-id (- betting-money))
            (update-user-card-betting-money token room-id split-index (* betting-money 2) )
            (sender/broadcast-room-user :double-down (room->client (room-id @room-list*)) )) )

      (go (<! (timeout 1000))
          (hand-card-to-user token room-id (<!room-cards room-id 0) split-index)
          (drop-card-in-room room-id 1)
          (sender/broadcast-room-user :double-down (room->client (room-id @room-list*)) ))

      (go (<! (timeout 1500))
          (calc-user-split-card-point token room-id split-index)
          (if (clear-playing-card?  room-id)
            (calc-room room-id)
            (sender/broadcast-room-user :double-down (room->client (room-id @room-list*)) ) ) ))

    ;;잘못 입력이 들어오면 현재 방 정보만 보내준다.
    (room->client (room-id @room-list*))) )

;; 인슈어런스
(defn req-insurance [token room-id]
  (if (util/not-nil? (#{:bet :hit} (<!user-card-state token room-id :deck0) ))
    (let [betting-money (:betting-money (<!user-split-card token room-id :deck0))
         insurance-money (/ betting-money 2)]
     (change-user-card-value token room-id :deck0 :insurance insurance-money)
     (update-user-money token room-id (- insurance-money))) )
  (room->client (<!room room-id)))



;req-login [token]
;req-join-room [user]
;req-seating-in-room [token room-id seat]
;req-betting-money [token room-id betting-money]
;req-hit [token room-id split-index]
;req-stand [token room-id split-index]
;req-split [token room-id split-index]
;req-doubledown [token room-id split-index]

;req-remove-user [token room-id]
;req-hand-dealer-card [room-id] not use


 ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 테스트 코드


(comment



(def room-list* (atom{}))

@room-list*
(let [user (req-login :token-1)]
  (req-join-room  user))

(let [user (req-login :token-2)]
  (req-join-room  user))
(req-seating-in-room :token-1 (get (first  @room-list*) 0) 1)
(req-seating-in-room :token-2 (get (first  @room-list*) 0) 2)
(req-betting-money :token-1 (get (first  @room-list*) 0) 100)
(req-betting-money :token-2 (get (first  @room-list*) 0) 120)
@room-list*

  (Thread/sleep 1500)
  ;(req-split :token-1 (get (first  @room-list*) 0) :deck0 )

  (req-hit :token-1 (get (first  @room-list*) 0) :deck0)

(Thread/sleep 1500)
  (req-stand :token-1 (get (first @room-list*) 0) :deck0)
  (Thread/sleep 5000)
  (req-hit :token-2 (get (first  @room-list*) 0) :deck0)
  (req-stand :token-2 (get (first @room-list*) 0) :deck0)

)

(comment


  (defn- change-room-wait-state [user-ch wait-limit-time room-id]
  (let [bet-completed (chan)]
    (go (loop [token (<! user-ch)]
          (println "token" token)
         (if token  ;; chanel date가 있을 경우 room의 user정보를 update한다.
           (do
             (change-user-state token room-id :in-play)
             (let [wait-user (<!state-user room-id :wait)]
               (if (empty? wait-user)
                 (do
                   (println "complete")
                   (>! bet-completed room-id) ) ))
             (recur (<! user-ch) ))
           (do
             (println "timeout")
             (>! bet-completed room-id) )) ))
    (go (let [ch-time-out (timeout wait-limit-time)
              [v ch] (alts! [ch-time-out bet-completed])]
           (println "finish room-id -> " room-id )
          (close! user-ch) ))  ))

(defn- change-room-to-wait [room-id]
  (if (exist-room? room-id )
  (swap! room-list* update-in [room-id] assoc :ch (chan 5) )
  (let [ch (get-in @room-list* [room-id :ch])]
    (change-room-wait-state ch 10000 room-id)  )) )



(defn- ch-bet[token room-id]
    (let [ch (get-in @room-list* [room-id :ch])]
      (go (>! ch token))  )
  )
     (change-user-state :token-1 (get (first  @room-list*) 0) :wait)
 (change-user-state :token-2 (get (first  @room-list*) 0) :wait)


(wait-room-bet1 (get(first @room-list*)0))
(ch-bet :token-1 (get(first @room-list*)0))
(ch-bet :token-2 (get(first @room-list*)0))

  )

(comment
  (do
    (let [ch-net (:ch @room)
          ch-net (chan 2)]
    (wait  ch-net  5000 2)
    (<!! (timeout 1000))
    (go (>! ch-net :token-2))
    (go (>! ch-net :token-1)) ) )
)
(comment   ;; simple
(def room-list* (atom{}))
@room-list*
(let [user (req-login :token-1)]
  (req-join-room  user))

(let [user (req-login :token-2)]
  (req-join-room  user))

(req-seating-in-room :token-1 (get (first  @room-list*) 0) 1)
(req-seating-in-room :token-2 (get (first  @room-list*) 0) 2)
(req-betting-money :token-1 (get (first  @room-list*) 0) 100)
(req-betting-money :token-2 (get (first  @room-list*) 0) 200)
(req-split :token-2 (get (first @room-list*)0) :deck0)

(req-insurance :token-2 (get (first  @room-list*) 0))
(req-doubledown :token-2 (get (first  @room-list*) 0) :deck0)
(Thread/sleep 1500)
(req-hit :token-1 (get (first  @room-list*) 0) :deck0)
(req-hit :token-2 (get (first  @room-list*) 0) :deck0)


(req-stand :token-1 (get(first @room-list*) 0) :deck0)
(req-stand :token-2 (get (first @room-list*) 0) :deck0)

)
