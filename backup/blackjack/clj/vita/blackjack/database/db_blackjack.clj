(ns vita.blackjack.database.db-blackjack
  ;(:use [philos.debug])
  ;(:use [clojure.tools.logging ])
  (:require [cemerick.url :as urls]
            [com.ashafa.clutch :as cloudant]
            [vita.blackjack.users :as user]
            [vita.sonaclo.shared.util :as util])
 ; (:require  [vita.log :as log]  )
  (:import [java.net.URL]))



(defonce db (assoc (urls/url "https://vitatest.cloudant.com/" "blackjack")
                    :username "vitatest"
                    :password "vitatest0987")) ;; reg-email :sonaclo@weebinar.com


(defn- exist-user?
  "input token : keyword"
  [token]
  (util/not-nil? (cloudant/get-document db token)))


(defn insert-blackjack-user
  "input token : keyword"
  [token]
  (let [new-token (if-let [t token]
                    t
                    (util/generate-user-token))
        k-token (keyword new-token)
        document {:_id k-token :token k-token :total-money 0} ]
    (cloudant/put-document db document)) )



(defn get-blackjack-user
  "input token : keyword"
  [token]
  (if (exist-user? token) ;; db_user와 연동되면 이 소소 필요할 수도 필요없을 수 도 있다.
    (cloudant/get-document db token)
    (insert-blackjack-user token) )  )


(defn remove-blackjack-user
  "input token : keyword"
  [token]
  (->> (cloudant/get-document db token)
       (cloudant/delete-document db))
  )


(defn get-token
  "input user-id : string.
   임시코드"
  [user-id]
  (let [db-doc (get-blackjack-user (keyword user-id))]
    (println db-doc)
    (keyword (:token db-doc) )))



(defn update-user-total-money [token money]
  (let [db-doc (cloudant/get-document db token)
        total-money (if-let [t (:total-money db-doc)]
                      t
                      0)
        rev     (:_rev db-doc)
        new-total-money (+ total-money money)]
        (cloudant/update-document db {:_id token :_rev rev :total-money new-total-money}) ))


(comment
  (get-user (get-token "11"))
  (cloudant/get-document db "token-1")


  (cloudant/put-document db {:_id "token-1" :user-id "user_id"})
  (cloudant/put-document db {:_id "token-2" :token "token2"})
  (cloudant/put-document db {:_id "token-3" :userid "userid"})
  (cloudant/put-document db {:_id "token-4" :user_id "userid"})  ;; <-- keyword에 - 는 안 된다.
  (cloudant/put-document db {:_id "token-5" :user_id "user-id"})

  (cloudant/get-view db "views" "user-id" {:key "user_id"} )
  (cloudant/get-view db "views" "userid" {:key "userid"} )
  (cloudant/get-view db "views" "user_id" {:key "aa"} )
  (cloudant/get-view db "views" "token" {:key  "token2"} )

  (cloudant/get-document db "eb8e819eb0d10fe1ee32e792bc55032b")
  (cloudant/get-view db "views"  "token" {:key (name :u457fd805-aba8-45f6-ab47-27ec4668c0da)} )

  (defn get-user-by-token [token]
  (into {} (cloudant/get-document db  token)))



  (defn get-user-by-user-id [user-id]
  (let [db-doc (cloudant/get-view db "views" "user_id" {:key (name user-id)})
        db-doc (into{} db-doc)]
    (:value db-doc ) ))

  (defn get-token [user-id]
    (let [document (get-user-by-user-id user-id)]
      (if (nil? document)
        (let [token        (keyword (user/add-user user-id) )
              new-document (cloudant/put-document db {:_id token :token token :user_id user-id})]
          (keyword (:token new-document)) )
        (keyword (:token document))) ))


)
