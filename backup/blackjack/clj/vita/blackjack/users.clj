(ns vita.blackjack.users
  "users")

(def tokens* (atom {}))

(defn add-user [id]
  (let [token (str "u" (java.util.UUID/randomUUID))]
    (swap! tokens* assoc token id)
    token))

(defn get-user-id [key]
  (@tokens* key))





