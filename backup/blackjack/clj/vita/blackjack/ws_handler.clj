(ns vita.blackjack.ws-handler
  (:use [clojure.tools.logging ])
  (:require [vita.common.websocket :as ws]
            [vita.blackjack.users :as user]
            [vita.blackjack.util :as bj-util]
            [vita.blackjack.sender :as sender :refer [filter-seated-user]]
            [vita.blackjack.room :as room]
            [vita.db.game-db :as db]
            [vita.sonaclo.shared.util :as util]
            [vita.session   :as session]
            ))

(def user-room* (atom{})) ; user-id, room

(defn connect-handler [ws]
  (println "")
  (println "**---***---*****---***---***")
  (println "new connection occure")
  (println "**---***---*****---***---***")
  (warn  "**---***---*****---***---***\n new connection ")

)
(defn do-command [{:keys [command] :as data}]

  (condp = (:command data)
    :login              (do
                          (let [token (:token (:data data))]
                            (sender/send->user token data) ))

    :join               (do
                            (let [token (:token (:data data))
                                  ;user-id (user/get-user-id token)
                                  ;user (assoc{} token (room/req-login token)) ; user -> {:user-id { user-info }}
                                  user (room/req-login token ) ; user -> {:user-id { user-info }}
                                  room (room/req-join-room  user )
                                  data {:command :join :data (filter-seated-user room)}]
                              #_(swap! user-room* assoc token (:room-id room))
                              (session/add-room-id token (:room-id room))
                              (println "token--> " token "data--> " data)
                              (sender/send->user token data)
                              #_(sender/broadcast-room-user :join room) ))

    :seat               (do ;(defn req-seating-in-room[user-id room-id seat]
                            (let [client-data (:data data)
                                  room (room/req-seating-in-room (:token client-data) (:room-id client-data) (:seat client-data))
                                  data {:command :seat :data (filter-seated-user room)}]
                              #_(sender/send->user (:token client-data) data)
                              (sender/broadcast-room-user :seat room) ))

    :bet                (do ;(defn req-betting-money [user-id room-id betting-money]
                          (let [client-data (:data data)
                                room (room/req-betting-money (:token client-data) (:room-id client-data) (:betting-money client-data))]
                            (if (map? room)
                              (sender/broadcast-room-user :bet room))))

    :hit                (do ;req-hit [user-id room-id]
                          (let [client-data (:data data)
                                room (room/req-hit (:token client-data) (:room-id client-data) (:split-index client-data) )]
                            (if (map? room)
                              (sender/broadcast-room-user :hit room))) )

    :stand              (do ;req-stand [user-id room-id]
                          (let [client-data (:data data)
                                room (room/req-stand (:token client-data) (:room-id client-data) (:split-index client-data) )]
                            (if (map? room )
                              (sender/broadcast-room-user :stand room))))

    :double-down         (do ;(defn req-doubledown [token room-id current-split-index]
                          (let [client-data (:data data)
                                room (room/req-doubledown (:token client-data) (:room-id client-data) (:split-index client-data))]
                            (if (map? room)
                              (sender/broadcast-room-user :double-down room))))
    :split             (do
                         (let [client-data (:data data)
                                room (room/req-split (:token client-data) (:room-id client-data) (:split-index client-data))]
                           (if (map? room )
                             (bj-util/print-room command room))))
     (do
       (let [user-id   (:user-id (:data data) )]
         (sender/send->user user-id "invalid command") )) ))


(defn receive-handler [user-ws data] ;; 실제 로직 처리..
  (println "blackjack ws-> " user-ws)
  (println "receive data==>" data)
  (println "")
  (let [user-id (keyword (:user-id (:data data)))
        token (keyword (:token (:data data)))]
    (cond
      (and user-id token)
        (do
            (println "user-id data and token data is full")
            (ws/send user-ws "user-id is null -> sample : {:command :login :data {:user-id :id}}") )

      (and (nil? user-id) (nil? token))
        (do
            (println "user-id data and token data is empty")
            (ws/send user-ws "user-id is null -> sample : {:command :login :data {:user-id :id}}") )

     (and user-id (nil? token))
        (do
          (let [token (keyword (str "u" (java.util.UUID/randomUUID)) )
                data  {:command :login :data {:token token}}]
            (println "user-id ->>>>" user-id)
            (println "token ->>>>" token)
            (session/add-tk-sid token user-id)
            (session/add-ws-tk user-ws token)
            (do-command data) ))

      (and (nil? user-id) token)
        (let [user-id  (user/get-user-id token)
              new-data (assoc-in data [:data :token] token)]

              (session/add-ws-tk user-ws token)
              (do-command new-data) ) )

           ))




(defn close-handler [ws] ;; room user 삭제 등등.
  (println "#################################")

 #_(debug "websocket close.\n token " (@ws-token-map* ws))

  (let [token (session/get-token ws )
        room-id (session/get-rid token)]
    (println token "websocket closed")
    (println "delete connection token =>" token)
    #_(debug "delete .\n token " token)
    (room/req-remove-user token  room-id)
    (session/remove-session ws)
    #_(swap! token-ws-map* dissoc token)
    #_(swap! ws-token-map* dissoc ws)
    #_(swap! user-room* dissoc token) ))


























