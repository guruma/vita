(ns vita.blackjack.rooms
  "rooms")


(def rooms* (atom {}))


(defn room>! [id room]
  (swap! rooms* assoc id room))

(defn room<! [id]
  (swap! rooms* dissoc id))

(defn get-room [id]
  (@rooms* id))

