(ns vita.blackjack.shared.cards
  "cards")

;; deprecate.
;; trump.clj로 이동
(def cards-board* [
  [:club-A :club-2 :club-3 :club-4 :club-5 :club-6 :club-7 :club-8 :club-9 :club-10 :club-J :club-Q :club-K]
  [:heart-A :heart-2 :heart-3 :heart-4 :heart-5 :heart-6 :heart-7 :heart-8 :heart-9 :heart-10 :heart-J :heart-Q :heart-K]
  [:diamond-A :diamond-2 :diamond-3 :diamond-4 :diamond-5 :diamond-6 :diamond-7 :diamond-8 :diamond-9 :diamond-10 :diamond-J :diamond-Q :diamond-K ]
  [:spade-A :spade-2 :spade-3 :spade-4 :spade-5 :spade-6 :spade-7 :spade-8 :spade-9 :spade-10 :spade-J :spade-Q :spade-K]
])

(def card->coord*
  (into {}
    (apply concat
      (keep-indexed (fn [i v]
                      (keep-indexed (fn [j card] [card [i j]]) v))
                    cards-board*))))


(def ^:private points {:A 1 :2 2 :3 3 :4 4 :5 5 :6 6 :7 7 :8 8 :9 9 :10 10 :J 10 :Q 10 :K 10})

(defn take-point-part [card]
  (keyword (second (.split (str card) "-"))))

(defn take-point [card]
  (let [part-card (take-point-part card)]
    (part-card points))
  )
(defn calc-point [cards]
  (let [p-keys (map take-point-part cards)
        point (apply + (map #(points % 0) p-keys))]
    (if (nil? ((set p-keys) :A))
      [point]
      [point (+ point 10)])))

(defn ace-card?
  "input card -> [:card] 형식"
  [card]

  (let [a-card (if (keyword? card)
                 [card]
                 card)]
    (keyword? (some  #{:club-A :heart-A :diamond-A :spade-A} a-card) ) ))


(defn calc-dealer-point [cards]
  (let [p-keys (map take-point-part cards)
        point (apply + (map #(points % 0) p-keys))]
    (if (nil? ((set p-keys) :A))
      [point]
      [(+ point 10)]))) ;;dealer에서 A는 11로 고정이다. {:A 1}로 이미 +1이 되어있다.


(defn point->string [point]
  (let [[a b] point]
    (if b
      (if (< 21 a)
          "bust"
        (if (< 21 b)
          (str a)
          (str a "/" b)))
      (if (< 21 a)
          "bust"
          (str a)))))
