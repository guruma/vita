# SAMPLE DOCUMENT

```javascript
{
  "_id": ":fbid-1",
  "_rev": "4-5e6b4467282b1ef58d1f6af718ec2a3f",

  "social_id": (string) facebook ID.

  "user": {
    "money": (integer) 현재 유저가 보유한 돈.
  },

  "game": {
    "blackjack": {

    },

    "slotmachine": {
      "bet-amount": (integer) 베팅 최소 액.
      "freespin-cnt": (integer) 현재 유저의 프리스핀 갯수.
    }
  },
}
```
