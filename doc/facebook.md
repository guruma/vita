# 페이스북 앱 설정

### ![페이스북 게임앱 설정 과정](https://fbcdn-dragon-a.akamaihd.net/hphotos-ak-prn1/t39.2178-6/851560_304559776343153_962033601_n.png)

# Login

* [Facebook Login](https://developers.facebook.com/docs/facebook-login)

* [Facebook Login for the Web Using the JavaScript SDK](https://developers.facebook.com/docs/facebook-login/login-flow-for-web/)

* [Login for Games on Facebook](https://developers.facebook.com/docs/facebook-login/using-login-with-games)

## Game Login 과정

 1. Checking Login Status
   1. 유저가 게임 앱 링크를 클릭하면 Canvas URL로 post 요청이 들어온다.
     1. 이 요청에는 signed_request 파라미터가 있는데, HMAC으로 사인되고 Base64url로 인코딩되어 있다.
     2. 서버가 이 signed_request를 파싱하면 다음과 같이 나온다.
       1. 앱에 로그인한 유저의 경우
       ```javascript
       {
         "oauth_token": "{user-access-token}",
         "algorithm": "HMAC-SHA256",
         "expires": 1291840400,
         "issued_at": 1291836800,
         "user_id": "218471"
       }
       ```
       2. 앱에 로그인하지 않은 유저의 경우
       ```javascript
       {
         "algorithm": "HMAC-SHA256",
         "issued_at": 1396877961,
         "user": {:country "kr",
         "locale": "ko_KR",
         "age": {:min 21}}}
       ```
     3. 유저의 로그인 상태 확인은 user_id와 oauth_token 필드의 존재 여부를 확인한다.
     4. 유저의 상태가 확인이 됐으면 그에 따른 시나리오는 다음과 같다.
       1. 유저가 페이스북과 게임에 로그인 된 상태. 로그인 된 상태로 게임에 진입.
         1. 이 경우에는 위의 '앱에 로그인한 유저의 경우'에 해당되며 oauth_token과 user_id 필드가 존재한다.
       2. 유저가 페이스북에만 로그인 된 상태. 페이스북 로그인 다이알로그로 리다이렉션.
         1. 이 경우에는 위의 '앱에 로그인하지 않은 유저의 경우'에 해당되면 user_id와 oauth_token 필드가 없다.
         2. 그런데 이 과정은 Canvas 앱에 대해서 약간의 변경의 소지가 있다.
       3. 유저가 페이스북에도 로그인 되지 않은 상태. 페이스북 로그인 다이알로그로 리다이렉션.
 2. Logging People in
   1. 페이스북에는 로그인하였으나 게임 엡에는 로그인 하지 않은 경우 로그인 다이알로그로 리다이렉션한다.
   2. 하지만 게임은 iframe 안에 있기 때문에 302 리다이렉션으로는 로그인 다이알로그로 리다이렉션 되지 않는다. 이를 위해서는 top(_top)을 지정해주어야 한다.

   ```clojure
   {:status 200
    :headers {"Content-Type" "text/html"}
    :body "<html><body><script type=\"text/javascript\"> window.parent.top.location.href=\""
        url-to-facebook-login-dialog-with-your-auth
        "\";</script></body></html>"}
   ```
   3. 정상적으로 처리되었다면 페이스북 로그인 다이알로그가 나타난다. 유저는 취소하거나 게임 앱에 로그인하게 된다. 그 결과는 로그인 다이알로그에 리다이렉션시 보냈던 redirect_uri(이것은 게임앱의 Canvas URL로 지정하는 것을 권장)에 지정된 곳으로 보내게 된다. 이 때는 물론 signed_request는 access token을 포함하게 된다.
     1. 이 부분에서 약간의 문제가 있다. 일단 페북 로그인 다이알로그에서 게임앱으로 리다이렉션 될 때 바로 signed_request로 오지 않는다. 실제로는 request method는 get이 고 url 파라미터에는 code와 state가 붙어 온다.
     2. 따라서 다시 한 번 code를 access-token과 교환해주는 작업을 통해 access-token을 구해야 한다.
     3. 그 후 다시 게임앱으로 리다이렉션을 해주어야 하는데 처음 페북 로그인 다이알로그에 리다이렉션 했던 것과 같은 방식으로 해야 한다. 즉 <script> parent.top.locale = url </script>
     4. 이 때 사용하는 url은 canvas app설정시 설정해준 canvas page에 적인 url로 해주면 된다.
   4. 정상적으로 처리되지 않은 경우 get 메소드로 다음과 같은 URL 인코드 파라미터가 온다.
   {"error" "access_denied",
   "error_code" "200",
   "error_description" "Permissions error",
   "error_reason" "user_denied",
   "state" "07cdd8bb-b7e4-4682-8f52-11e32ab7756e"}

 3. Confirming Signature and Identity
 4. Further Steps

# Game

* [Games Overview](https://developers.facebook.com/docs/games/overview) : Quickly integrate Facebook Games APIs

* [Canvas Game](https://developers.facebook.com/docs/games/canvas) :

* [Canvas Games Tutorial](https://developers.facebook.com/docs/games/canvas/canvas-tutorial)

* [Games Developer Center](https://developers.facebook.com/docs/games)

### Canvas Page

페이스북 앱은 Canvas Page에 로드되는 웹앱이다. 유저가 앱을 클릭하면 페이스북은 Canvas Page의 iframe에 Canvas URL을 로드한다. Canvas URL은 앱의 기능을 구현한 서버의 HTML, CSS, JavaScript로 구성된다.

# OAuth

## OAuth2

* [OAuth 2.0 protocol: Bearer Tokens draft-ietf-oauth-v2-bearer-08](http://tools.ietf.org/html/draft-ietf-oauth-v2-bearer-08)

OAuth는 리소스에 대한 접근과 그 접근에 대한 권한의 획득을 분리하는 방식이다. 즉 리소스 저장 서버로부터 직접 인증을 받는 대신, 권한부여 서버(리소스 저장 서버가 승인한)를 통해 리소스 접근에 대한 허가를 득하고, 리소스가 저장 서버는 허가를 득한 유저의 요청만을 처리한다.

허가를 득하는 것은 권한부여 서버로부터 access token을 받는 것인데, 이것은 "해당 클라이언트에만 부여된 접근 권한을 나타내는 문자열이다." 이것은 리소스에 대한 임시 접근 허가증 정도로 이해하면 된다.

* [clj-oauth2](https://github.com/DerGuteMoritz/clj-oauth2)

# Graph API

* [The Graph API](https://developers.facebook.com/docs/graph-api)
* [Quickstart for Graph API](https://developers.facebook.com/docs/graph-api/quickstart/)
* [Graph API Explorer](https://developers.facebook.com/tools/explorer)
* [clj-facebook-graph](https://github.com/maxweber/clj-facebook-graph)
