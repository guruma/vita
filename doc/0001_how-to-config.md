# 설정 파일 정보.

* 서버구동 및 클라이언트 컴파일시, 뭔가 자주 변경되는 것(port 등...)을, 소스 수정없이 반영시키고 싶었다.
* 환경변수 `VC_CONFIG`, `VA_CONFIG`를 이용.


## Example Code.
```clojure
(:require vita.config.macros :as cm)

;; :level 영향.
(cm/LEVEL
    :dev (println "dev")
    :test1 (println "test1")
    :live1 (println "live"))


;; :debug 영향.
(cm/DEBUG
    (println "dev"))
```


## ex) VC_CONFIG

```clojure
{:level :internal
 :debug true

 :api {:facebook {:appId "651425301560327"
                  :status true
                  :cookie true
                  :xfbml true}}

 :va {:url "wss://localhost:8080"}
 }
```

# ex) VA_CONFIG (WIP)

```clojure
{:level 코드 분기 레벨 ( :internal=>:dev | :test-server=> :test1 | :amazon => :live1)
 :debug true

 :host "127.0.0.1"
 :port 8080

 :db {:username "vitatest"
      :password "vitatest0987"

      ;; Cloudant Reg-Email :sonaclo@weebinar.com
      :connect {:game "https://vitatest.cloudant.com/game"
                :rank "https://vitatest.cloudant.com/rank"
                :log "https://vitatest.cloudant.com/log"
                }
      }

 :ws {:ssl true
      :key-store-path "../../dist/certs/sonaclo.keystore"
      :key-store-password "gasanfreenote"
      }

 :npc {:logout-probility 10 ; Round 종료 후 방을 나갈 확률
        :room-pool-cnt 3 ;"초기 방 생성 개수"
        :join-room-probility 20 ;"NPC가 방에 들어갈 확률"
        :win-probility 50 ; "NPC가 승자일 확률"
        :join-period-time 1200000 ;"NPC가 방을 검색해서들어가는 시간(주기)" (* 1000 60 20)
      }
 }
```
