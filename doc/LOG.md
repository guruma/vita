* npc 디버그로그 다 지우기.

* OBT때는, LEVEL=DEBUG
* OBT이후, LEVEL=WARN

* 로그 포맷?


(debug sid username "description")
(warn sid username "description")
(error e sid username "description")

sid username이 없는 경우,
(warn "[none]" "description")
(error e "[none]" "description")


# TRACE
- 개발자용. 커밋하지마셈.


# DEBUG
- 개발용


# INFO
- 일단 사용하지 마셈.

# WARN
- 데이터 검증용.

# ERROR
- Exception
- DB Error
- 진짜 크리티컬한것.
