
# 코딩 규칙.
* 파일 저장 포맷은 UTF8 with no BOM으로 한다.

* 심볼명은 숫자로 시작하지 않는다.

    7-gold => X
    gold-7 => O

* 파일내 전역변수는 뒤에 별(*)이 붙는 식으로 작명한다.

  something*

* public 함수 및 변수에는 docstring을 작성한다.

    "function description
    
     #argument {type}: description
     
     #return {type}: description"

* clojure-style-guide를 되도록이면 준수하도록 한다.

  https://github.com/bbatsov/clojure-style-guide

* 상태관련
 - 진행중: 진행형(~ing)
 - 진행완료: 과거완료(p.p)

* 주석
 - NOTE, TODO, FIXME, HACK

     ex)
     TODO(kep) 나중에 할 일 임.
     
* docstring
 - 함수 설명, 함수 인자, 함수 결과값에 대해서 (외부 공개용)...
 - 개발자 내부 참고용은 docstring에 넣지말고, 인수 바로 뒤에다가...
