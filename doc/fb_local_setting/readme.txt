페이스북 설정

;;-----------------------------------------

1. 페이스북에 개발자로 앱을 만든다.

2. 설정 -> + Add Platform 버튼으로 App on Facebook 을 추가한다.

3. 보안 캠버스 url 칸에,
   웹서버 주소를 넣는다. (https://localhost/vita-login/)


로컬 edn 설정

;;-----------------------------------------

4. vc, vw 의 appId 와 app-secret-code 를 바꾼다.

5. vc, vw, va 를 :live 로 바꾼다.


로컬 nginx 설정

;;-----------------------------------------

6. 첨부한 nginx.conf 로 덮어씌운다.

7. sudo nginx (443 포트 사용은 sudo 권한 필요)

  (참고: 1. 끄려면 sudo nginx -s quit
        2. 켤때 dist/certs 관련 에러가 나오면 dist/certs/* 파일을 /opt/certs 로 복사한다)
