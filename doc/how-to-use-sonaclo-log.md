sonaclo.log 
===========

sonaclo.log 함수 사용법
---------------

```
;;;; Web Client --> Log Server (log.sonaclo.com)
(ns log-example
  (:require [sonaclo.log :as log :refer-macros [dlog ulog]]))

;; lc* 내의 :app_name을 :texas로, :user_id를 :user-123으로 미리 설정한다.  
(def lc* (log/log-context :texas :user-123))

;; dlog 함수는 debugging 목적의 log를 Log Server로 전송한다. 
(dlog lc* {:cmd :login :data "This is login command."})
       ;; --------------------------------------------
       ;; log message

;; ulog 함수는 user action 관련 log를 Log Server로 전송한다. 
(ulog lc* {:action {:click :spin-button}})
       ;; ----------------------------
       ;; log message
```

```
;;;; Game Server --> Cloudant db server
(ns log-example
  (:require [sonaclo.log :as log :refer [dlog]]))

;; lc* 내의 :app_name을 :texas로, :source를 :game-server로 미리 설정한다.  
(def lc* (log/log-context :texas :game-server))

;; dlog 함수는 debugging 목적의 log를 Cloudant db Server로 전송한다. 
(dlog lc* :user-123  {:aaa 100})
        ;; --------  ---------
        ;; user id   log message

;; ulog 함수는 제공되지 않는다.
```


log server  (log.sonaclo.com) 기동 
----------------------------------

```
philos@sun:~$ ssh -i ~/.ssh/sona_ubun_key.pem ubuntu@log.sonaclo.com
Welcome to Ubuntu 14.04 LTS (GNU/Linux 3.13.0-24-generic x86_64)

 * Documentation:  https://help.ubuntu.com/

  System information as of Wed Apr 30 10:26:08 UTC 2014

  System load:  0.0               Processes:           68
  Usage of /:   21.8% of 7.75GB   Users logged in:     0
  Memory usage: 14%               IP address for eth0: 172.31.30.135
  Swap usage:   0%

  Graph this data and manage this system at:
    https://landscape.canonical.com/

  Get cloud support with Ubuntu Advantage Cloud Guest:
    http://www.ubuntu.com/business/services/cloud


Last login: Wed Apr 30 10:26:09 2014 from 121.67.181.103

ubuntu@ip-172-31-30-135:~$ pwd
/home/ubuntu

ubuntu@ip-172-31-30-135:~$ cd work/log

ubuntu@ip-172-31-30-135:~/work/log$ sudo lein run
WARNING: You're currently running as root; probably by accident.
Press control-C to abort or Enter to continue as root.
Set LEIN_ROOT to disable this warning.

Http server on 0.0.0.0:80
Started clojure verticle: SonacloLogServerStart.clj 
Succeeded in deploying verticle

```
