# How To Run


## 폴더 설명.
* va : vita app server
  - 웹소켓을 이용한 어플리케이션 서버.

* vw : vita web server
  - 파일을 호스팅하는 웹서버.

* vc : vita client
  - 클라이언트.

* vs : vita shared files
 - 클라, 서버 공통코드를 모아두는 곳.
 - 개발 편의를 위해, cljx로 생성된 파일 역시 버전관리에 포함시킨다.

* dev-vw : vita dev web server
 - 테섭에 특화된 서버(login페이지 등등..)



## 기본.
### 빌드
```bash
[va]$ lein run
[vc]$ lein cljsbuild once dev
[dev-vw]$ sudo lein ring server
```
### 접속
1. https://localhost/ 페이지에서 무시하고 실행하기로 인증받음
2. file:///Users/manmyung/project/vita/projects/vw/resources/public/main.html?user-id=abcd 페이지(path는 사용자 따라 수정필요)에서 게임실행. 이때 abcd 는 임의로 변경가능

### 문제해결
서버접속이 안되는 경우, dist/config/vc.edn 코드에서 :level, :va 항목을 다음처럼 수정

	:level :internal
	:va {:url "wss://localhost:8080"}


## 내부 개발 서버 연동 방법.
client만 빌드하면 된다.
[vc]lein cljsbuild once dev

*자동으로 내부 개발 서버에 붙게 된다.
*내부 개발 서버에 연동된것인지 확인하는 방법
** vita/dist/config/vc.edn 파일 중에
** :va {:url "ws://192.168.0.7:8080"}


## 심화.
### 빌드
```bash
[va]$ VA_CONFIG=~/config/va.edn lein run
[vc]$ VC_CONFIG=~/config/vc.edn lein cljsbuild auto repl
[vc]$ lein repl
```
### 접속
http://localhost:8888/?user-id=p1

### 사용방법 예제
```
repl=> (cljs!)
<< started Weasel server on ws://0.0.0.0:9001 >>
Type `:cljs/quit` to stop the ClojureScript REPL
nil
;이때 http://localhost:8888/?user-id=p1 에 접속
cljs.user=> (ns vita.scene.slotmachine.core)
nil
vita.scene.slotmachine.core=> (vita.resource.sound/sound-off)
nil
vita.scene.slotmachine.core=>
```
## 공통파일 생성.

```bash
[vs]$ lein cljx once
```

## 클라이언트에서 Breakpoint 이용하여 디버깅 하려면
### 빌드
projects/vc/project.clj
```
:output-dir "../vw/resources/public/js/game/out"
:optimizations :none
:source-map true
```

projects/vw/resources/public/main.html
```
<script src="js/game/out/goog/base.js" type="text/javascript"></script>
...
<script type="text/javascript">goog.require("vita.core");</script>

```
### Breakpoint 세팅방법
1. 크롬 개발자 도구 Sources 탭의 js/game/out 밑에서 원하는 cljs 연다
2. 가운데 창에서 line number 클릭하여 Breakpoint 세팅
