require 'fileutils'

puts "packaging start"

GITROOT = `git rev-parse --show-toplevel`.strip
FileUtils.chdir(File.join(GITROOT, 'target'))

MAIN_CLJ = <<TT
(ns verticle-main (:require vita.core)) (vita.core/s-start)
TT

MOD_JSON = <<TT
{"licenses":[null],"homepage":null,"description":null,"main":"main.clj"}
TT


File.open('main.clj', 'w') do |out|
    out << MAIN_CLJ
end


File.open('mod.json', 'w') do |out|
    out << MOD_JSON
end

puts `cp vita-0.1.0-SNAPSHOT.jar lib/`
puts `cp -R ../dist/ dist/`
puts `zip -r vita-0.1.0-SNAPSHOT.zip mod.json main.clj lib/ dist/`

puts "package end"
