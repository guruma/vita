(defproject vita-dummy "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.6.0"]
                 [http.async.client "0.5.2"]
                 [org.clojure/core.async "0.1.301.0-deb34a-alpha"]
                 [org.clojure/tools.reader "0.8.4"]
                 [org.clojure/tools.logging "0.2.6"]
                 [org.slf4j/slf4j-log4j12 "1.7.7"]
                 ]
  :main vita-dummy.core)
