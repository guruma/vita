(ns vita-dummy.client
  (:require [vita-dummy.client.vita-dummy :as vita-dummy]
            ))

(defn make-dummy [base-url]
  (vita-dummy/->VitaDummy base-url nil nil))
