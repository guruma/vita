(ns vita-dummy.client.protocols)


(defprotocol IClient
  (connect! [this url])
  (send! [this msg])
  (close! [this]))


(defprotocol IMainLobby
  (join-lobby [this social_id])
  )


(defprotocol ISlotmachine
  (join-slotmachine [this bet-amount])
  (spin-slotmachine [this freespin-count bet-line-count per-line-bet chip])
  )
