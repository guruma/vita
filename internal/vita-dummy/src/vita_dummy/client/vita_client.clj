(ns vita-dummy.client.vita-client
  (:require [vita-dummy.client.protocols :as p]
            [clojure.tools.reader.edn :as edn]))


(defrecord VitaClient [client]
  p/IClient
  (connect! [this url]
    (p/connect! client url)
    this)

  (close! [this]
    (p/close! client)
    this)

  (send! [this msg]
    (->> (str msg)
         (p/send! client)
         (:val)
         ;; (map char)
         ;; (apply str)
         (edn/read-string))))
