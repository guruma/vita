(ns vita-dummy.client.vita-dummy
  (:require [vita-dummy.client.protocols :as p]
            [vita-dummy.client.client :as client]
            [vita-dummy.client.vita-client :as vita-client]
            ))

(defn- make-client [url]
  (-> (client/->Client nil nil nil)
      (vita-client/->VitaClient)
      (p/connect! url)))



(deftype VitaDummy [base-url ^:volatile-mutable client ^:volatile-mutable token]
  p/IMainLobby
  (join-lobby [this social_id]
    (set! client (-> (str base-url "/game/main-lobby")
                     (make-client)))
    (let [ret (-> client
                  (p/send! {:cmd :JOIN-LOBBY, :data {:social_id social_id}, :token ""}))]
      (set! token (get-in ret [:data :token]))
      ret))

  p/ISlotmachine
  (join-slotmachine [this bet-amount]
    (set! client (-> (str base-url "/game/slotmachine")
                     (make-client)))
    (-> client
        (p/send! {:cmd :JOIN-SLOTMACHINE, :data {:bet-amount bet-amount}, :token token})
        )
    )
  (spin-slotmachine [this freespin-count bet-line-count per-line-bet chip]
    (-> client
        (p/send! {:cmd :SPIN-SLOTMACHINE, :data {:freespin-count freespin-count,
                                                 :bet-line-count bet-line-count,
                                                 :per-line-bet per-line-bet
                                                 :chip chip}
                  :token token})))
  )
