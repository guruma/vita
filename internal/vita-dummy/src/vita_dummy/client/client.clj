(ns vita-dummy.client.client
  (:require [vita-dummy.client.protocols :as p]
            [http.async.client :as h]
            [clojure.core.async :as async :refer [>!! thread >! <!! chan put! timeout alts! alts!! go]]
            ))


(deftype Client [^:volatile-mutable client
                 ^:volatile-mutable channel
                 ^:volatile-mutable connection]
  p/IClient
  (connect! [this url]
    (set! client (h/create-client))
    (set! channel (chan))

    (let [con (promise)]
      (h/websocket client url
                   :text (fn [conn text]
                           (thread (>!! channel {:type :text :val text}))
                           )

                   ;; :byte (fn [conn byte]
                   ;;         (put! channel {:type :byte :val byte}))

                   :error (fn [conn error]
                            (thread (>!! channel {:type :error :val error}))
                            )

                   :close (fn [con status]
                            (thread (>!! channel {:type :close :val status}))
                            )

                   :open (fn [conn]
                           (deliver con conn)
                           (thread (>!! channel {:type :open :val nil}))
                           ))
      (set! connection @con))

    (let [[v c] (alts!! [channel (timeout 15000)])]
      (if v
        v
        (throw (Throwable. "connect timeout"))
        )))

  (close! [this]
    (h/close client))

  (send! [this msg]

    (go (h/send connection :text msg))

    (let [[v c] (alts!! [channel (timeout 300000)])]
      (if v
        v
        (throw (Throwable. "recv timeout"))
        ))))
