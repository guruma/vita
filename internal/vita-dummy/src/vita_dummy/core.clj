(ns vita-dummy.core
  (:gen-class)
  (:require [vita-dummy.client :as client]
            [vita-dummy.client.protocols :as p]
            [clojure.core.async :as async :refer [<! >! <!! timeout chan alt! go put! go-loop]]
            [clojure.tools.reader.edn :as edn]
            [clojure.tools.logging :refer [debug error]]
            ))


(defn test-worker [worker-id base-url slotmachine]
  (go
    (try
      (when-let [c (client/make-dummy base-url)]

        (when (p/join-lobby c worker-id)
          (debug "logined:" worker-id)

          (loop [cnt
                 (:spin-count slotmachine)

                 {:keys [freespin-count bet-line-count per-line-bet chip]}
                 {:chip (get-in  (p/join-slotmachine c 200) [:data :chip])
                  :freespin-count 0
                  :bet-line-count 20
                  :per-line-bet 200}]

            (when (and (pos? cnt)
                       (> chip (* bet-line-count per-line-bet)))

              (when-let [ret (p/spin-slotmachine c freespin-count bet-line-count per-line-bet chip)]

                (debug "worker" worker-id "ret:" ret)

                (let [{:keys [chip freespin-count]} (:data ret)]
                  (recur (dec cnt) {:chip chip
                                    :freespin-count freespin-count
                                    :bet-line-count bet-line-count
                                    :per-line-bet per-line-bet})))))
          )
        )
      (catch Exception e
        (error e "death:" worker-id)
        )
      )
    )
  )


(defn simulate [config]
  (let [{:keys [prefix from to slotmachine base-url]} config
        ids (map #(str prefix %) (range from (inc to)))]

    (let [workers (doall (map (fn [i] (test-worker i base-url slotmachine)) ids))]
      (<!! (go-loop [[f & rst] workers]
             (when f
               (<! f)
               (recur rst)))))))


(defn -main []
  (debug "start")
  (debug "time: " (with-out-str (time (simulate (edn/read-string (slurp "config.edn"))))))
  (debug "end")
  (System/exit 0)
  )
