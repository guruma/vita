(defproject sonaclo/log "0.1.0-SNAPSHOT"
  :description "Sonaclo Log Library"
  :dependencies [[org.clojure/clojure "1.5.1"]
                 [org.clojure/clojurescript "0.0-2202"]
                 [io.vertx/clojure-api "1.0.1"]
                 [jayq "2.5.0"]
                 [clj-time "0.7.0"]
                 [com.cemerick/url "0.1.1"]
                 [paddleguru/clutch "0.4.0"]]
  :source-paths ["src/clj"]
  :plugins [[lein-cljsbuild "1.0.3"]]
  :main sonaclo.log.server
  :cljsbuild {:builds {:dev
                       {:source-paths ["src/cljs"]
                        :compiler {:output-to "resources/public/js/app.js"
                                   :optimizations :whitespace
                                   ;:source-map true
                                   :pretty-print true} }}})
         
