(ns philos.debug
  (:require (clojure [string :as str]
                     [pprint :as pp] )))

(defmacro dbg [x & more]
  `(binding [*print-length* 100]
     (let [[return# more#] [~x (vector ~@more)]
           n#   (some #(if (number? %) %) more#)
           msg# (some #(if (string? %) %) more#)
           pp#  (some #{:pp} more#)
           test# (let [v# (map-indexed vector more#)
                       i# (some #(if (= (get % 1) :if)
                                  (inc (get % 0)))
                           v#)
                       t# (get-in (vec v#) [i# 1])]
                  t#)
           return# (or (and n# (take n# return#))
                       return#)
           pprint# (str/trim (with-out-str (pp/pprint return#)))
           format# (str ">> dbg"
                        (and msg# (str " <" msg# ">"))
                        ": " '~x
                        (or (and pp# "%n =>%n%s")
                            "%n => %s")
                        " <<%n")]
       (when (or (nil? test#) test#)
         (printf format# pprint#)
         (flush))
       return#) ))

;; for debugging dbg macro internally
(defmacro dbg_ [x & more]
  `(let [[return# more#] [~x (vector ~@more)]]
     return#))
