(ns sonaclo.log
  "game server --> log db server"
  (:require [com.ashafa.clutch :as clutch]
            [cemerick.url :as url]
            [clj-time.core :as time]
            [clj-time.coerce :as coerce]
            [philos.debug :refer [dbg]] ))

;; remote cloudant log db
(def ^:private db*
  (atom (assoc (url/url "https://vitatest.cloudant.com/" "log")
               :username "vitatest"
               :password "vitatest0987") ))

(defn set-db
  "Sets db* to db."
  [db]
  (reset! db* db))


(defn log-context
  [app-name source]
  {:app_name app-name
   :source   source})

(defn- current-time
  "#return {int}"
  []
  (-> (time/now) coerce/to-long))

(defn- send-log
  "Sends data to log db server."
  [data]
  (clutch/put-document @db* data))


(defn dlog
  "log for debugging"
  [lc user-id message]
  (let [data (into lc {:log_type :debug
                       :user_id  user-id
                       :time     (current-time)
                       :message  message} )]
    (send-log data) ))

(comment
  (def lc* (log-context :omaha :game-server))
  (dlog lc* :user-1234 {:aaa 2030})
) 
