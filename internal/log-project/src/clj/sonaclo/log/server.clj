(ns sonaclo.log.server
  "web client --> log server --> log db server"
  (:require [vertx.http :as http]
            [vertx.http.websocket :as ws]
            [vertx.stream :as stream]
            [vertx.embed :as embed]
            [com.ashafa.clutch :as clutch]
            [cemerick.url :as url])
  (:import java.util.concurrent.CountDownLatch)
  (:gen-class))

(use 'philos.debug)

(def ^:private host* (atom "localhost"))
(def ^:private port* (atom 8080))

;; remote cloudant log db
(def db* 
  (atom (assoc (url/url "https://vitatest.cloudant.com/" "log")
               :username "vitatest"
               :password "vitatest0987")))

(defn set-db
  "Sets db* to db."
  [db]
  (reset! db* db))


(defn relay-log
  "Relays log message from web client to log db server."
  [message]
  (clutch/put-document @db* message))

(defn- start
  []
  (-> (http/server)
      (http/on-request
        (fn [req]
          (when (= "/" (.path req))
            (http/send-file (http/server-response req)
                            "resources/public/index.html") )))
      (ws/on-websocket
        (fn [ws]
          (if (= "/log" (.path ws))
            (stream/on-data ws (fn [data]
                                 (relay-log (read-string (str data))) ))
            (.reject ws) )))
      (http/listen @port* @host*))
  (println (format "Http server on %s:%s" @host* @port*)))

(defn run-vertx
  []
  (embed/set-vertx! (embed/vertx))
  (start))

(run-vertx)

(defn -main
  [& args]
  (embed/set-vertx! (embed/vertx))
  (let [latch (CountDownLatch. 1)]
    (start)
    (.await latch)))
