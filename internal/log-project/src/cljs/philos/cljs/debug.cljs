(ns philos.cljs.debug)

;;--------------
;; caching 관련
;;--------------

(def ^:private prev-returns* (atom {}))

;; @args {str}: the key of prev-returns* map
;; @return {str}: the value of prev-returns* map
(defn changed? [args return]
  (if-not (contains? @prev-returns* args)
    (swap! prev-returns* assoc args ""))

  (and (not= return (@prev-returns* args))
       (swap! prev-returns* assoc args return) ))


;;----------------
;; css 관련 함수들
;;----------------

(def ^:private css*
  (atom {:error   "color: red"
         :warn    "color: #ff00ff"
         :info    "color: blue" 
         :debug   "color: green"
         :default "color: black"
         :title   "color: #8b008b"
         :form    "background: #ffc125; color: black"} ))

;; <sample>
;; key  => :css
;; args => [:css :error :if (+ 2 3)]
;; indexed-args => ([0 :css] [1 :error] [2 :if] [3 5])
;; index => 1
;; return => :error
(defn key->value
  [key args]
  (let [indexed-args (map-indexed vector args)
        index        (some #(if (= (get % 1) key)
                              (inc (get % 0)))
                           indexed-args)]
    (get-in (vec indexed-args) [index 1]) ))

(defn set-css [new-css]
  (swap! css* merge new-css))

(defn css-value [key args]
  (let [value (key->value key args)]
    (cond
      (keyword? value)
      (cond
        (#{:error :e} value)
        (:error @css*)

        (#{:warn :w} value)
        (:warn @css*)

        (#{:info :i} value)
        (:info @css*)

        (#{:debug :d} value)
        (:debug @css*)

        (#{:default} value)
        (:default @css*)

        :else
        (value @css*))

      (string? value)
      value )))


;;------------
;; print 관련
;;------------

(defn print-log [header msg-css result js return] 
  (if js
    (if msg-css
      (do
        (.group js/console header (:title @css*) (:default @css*)
                                  msg-css        (:default @css*)
                                  (:form @css*)  (:default @css*))
        (.log js/console result return)
        (.groupEnd js/console))
      (do
        (.group js/console header (:title @css*) (:default @css*)
                                  (:form @css*)  (:default @css*))
        (.log js/console result return)
        (.groupEnd js/console) ))
    (if msg-css
      (do
        (.group js/console header (:title @css*) (:default @css*)
                                  msg-css        (:default @css*)
                                  (:form @css*)  (:default @css*))
        (.log js/console result)
        (.groupEnd js/console))
      (do
        (.group js/console header (:title @css*) (:default @css*)
                                  (:form @css*)  (:default @css*))
        (.log js/console result)
        (.groupEnd js/console) ))))


 
