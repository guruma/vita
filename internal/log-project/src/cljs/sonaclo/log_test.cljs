(ns sonaclo.log-test
  "web client --> log server"
  (:use-macros [jayq.macros :only [ready]])
  (:require [philos.cljs.debug :refer-macros [dbg clog cl break]]
            [jayq.core :as jq :refer [$]]
            [sonaclo.log :as log] ))

(def lc* (log/log-context :texas :user-id))
(log/set-url lc* "ws://localhost:8080/log")

(defn start
  []
  (jq/on ($ :#send-button) "click" #(log/dlog lc* (jq/val ($ :#message)))))

(defn ^:export main []
  (ready
    (start) ))
