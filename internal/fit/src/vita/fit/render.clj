(ns vita.fit.render
  (:require [vita.fit.split :as split :refer [split-wiki]]
            [vita.fit.test :as fit :refer [test-wiki-item]]))


(defn map-tag [tag xs]
  (map (fn [x] [tag x]) xs))

(defn render-table-row [items]
  [:tr [:td {:colspan 2} items]])

(def result-color
  {:success "green" :fail "red" :actual "gray"})

(defn render-bottom [[f & r :as items]]
  (if (keyword? f)
    [:tr {:bgcolor (f result-color)} (map-tag :td r)]
    [:tr (map-tag :td items)]))


(defn tested-table? [rows]
  (keyword? (first (last rows))))

(defn render-table [rows]
  [:table {:border 1 :width 500}
   (let [[rows' result] (if (tested-table? rows)
                          (split-with #(string? (first %)) rows)
                          ((juxt butlast #(vector (last %))) rows))]
     (concat
      (map render-table-row rows')
      (map render-bottom result)))])

(defn render-title [item]
  [:h1 (first item)])

(defn render-list [item]
  [:h1 (first item)])

(defn render-item [{:keys [type item]}]
  (case type
    :table (render-table item)
    :title (render-title item)
    :list  (render-list item)
    ""))

(defn render-wiki [wiki]
  (->> (split-wiki wiki)
       (map render-item)
       (interpose [:br])))

(defn render-tested-wiki [wiki]
  (->> (split-wiki wiki)
       (map test-wiki-item)
       (map render-item)
       (interpose [:br])))
