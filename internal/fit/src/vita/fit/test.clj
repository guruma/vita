(ns vita.fit.test
  (:require [vita.fit.split :as split :refer [split-wiki]]
            [vita.poker-score :as poker :refer [winners winnners-for-texas winnners-for-omaha]]))


(defn split [s]
  (clojure.string/split s #"\s+"))

(defn str->vector [[hand-str]]
  (if (seq hand-str)
    (->> (split hand-str)
         (map keyword)
         vec)))

(defn row->score [[rank points]]
  (if-let [rank' (seq rank)]
    (let [points' (->> (split points)
                       (remove empty?)
                       (map read-string))]
      [(keyword (clojure.string/lower-case rank)) points'])))


(defn score->row [[rank points]]
  [(name rank)
   (->> points
        (map str)
        (interpose " ")
        (apply str))])

(defn calc-score-of-poker [card-rows]
  (let [[rank indexes] (winners (map str->vector card-rows))]
    [rank (map inc indexes)]))

(defn calc-score-of-texas [[community-card-row & hole-card-rows]]
  (let [community-card (str->vector community-card-row)
        hole-cards (map str->vector hole-card-rows)
        [rank indexes] (winnners-for-texas community-card hole-cards)]
    [rank (map inc indexes)]))

(defn calc-score-of-omaha [[community-card-row & hole-card-rows]]
  (let [community-card (str->vector community-card-row)
        hole-cards (map str->vector hole-card-rows)
        [rank indexes] (winnners-for-omaha community-card hole-cards)]
    [rank (map inc indexes)]))

(defn calc-score [game card-rows]
  (case game
    "poker" (calc-score-of-poker card-rows)
    "texas" (calc-score-of-texas card-rows)
    "omaha" (calc-score-of-omaha card-rows)
    []))

(defn game-name [[title]]
  (-> title (.split ":")
      first
      clojure.string/trim
      clojure.string/lower-case))

(defn split-last [n col]
  (split-at (- (count col) n) col))

(defn determine-by-score [expected-score actual-score]
  (if (= actual-score expected-score)
    :success
    :fail))

(defn result-row [result expected-row]
  [(cons result expected-row)])

(defn actual-row [result actual-score]
  (if (= result :fail)
    [(cons :actual (score->row actual-score))]))

(defn test-table [[title & r]]
  (let [[card-rows [expected-row]] (split-last 1 r)
        actual-score (calc-score (game-name title) card-rows)
        result (determine-by-score (row->score expected-row) actual-score)]
    (concat
     [title]
     card-rows
     (result-row result expected-row)
     (actual-row result actual-score))))

(defn test-wiki-item [item]
  (if (= :table (:type item))
    (update-in item [:item] test-table)
    item))


