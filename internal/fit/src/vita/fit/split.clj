(ns vita.fit.split
  (:require [clojure.string :as string :refer [trim split-lines blank?] ]))


(defn table-line? [line]
  (.startsWith line "|"))

(defn title-line? [line]
  (.startsWith line "#"))

(defn list-line? [line]
  (.startsWith line "*"))

(defmacro casep [param & rst]
  `(condp (fn [pred# _#] (pred# ~param)) nil
     ~@rst))

(defn line-type [line]
  (casep line
         title-line?       :title
         table-line?       :table
         list-line?        :list
         :none))

(defn- wiki-line? [line]
  (not= :none (line-type line)))

(defn split-in-lines [txt]
  (map trim (split-lines txt)))

(defn same-type-line? [line1 line2]
  (if (and line1 line2)
    (let [[t1 t2] (map line-type [line1 line2])]
      (if (= t1 t2 :none)
        false
        (= (line-type line1) (line-type line2))))))

(defn- conj-if [pred item coll]
  (if (pred item) (conj coll item) coll))

(defn split-table-line [txt]
  (->> (.split txt "\\|")
       (remove blank?)
       (map trim)))

(defn split-item [type line]
  (if (= :table type)
    (split-table-line line)
    line))

(defn split-wiki [txt]
  (loop [[line & r] (split-in-lines txt) item {} items []]
    (if line
      (if (wiki-line? line)
        (let [type (line-type line)
              sl (split-item type line)]
          (if (empty? item)
            (recur r {:type type :item [sl]} items)
            (if (= type (:type item))
              (recur r (update-in item [:item] conj sl) items)
              (recur r {:type type :item [sl]} (conj items item)))))
                                        ;(recur r nil (conj items item)))
        (recur r {} (conj-if (complement empty?) item items)))
      (conj items item))))
