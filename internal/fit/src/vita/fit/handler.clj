(ns vita.fit.handler
  (:use compojure.core)
  (:require [compojure.handler :as handler]
            [compojure.route :as route]
            [ring.util.response :as res]
            [vita.fit.render :as render :refer [render-wiki render-tested-wiki]]
            [hiccup.core :refer :all]
            [hiccup.form :as form]
            [hiccup.element :refer [link-to]]
            [hiccup.page :refer [html5]]))

;; url root : http:localhost:3000/fit
;; resource root : resources/public/fit


(defn read-wiki [file-name]
  (slurp file-name))

(def wiki-root-path* "internal/fit")
(defn url->path [url] (str wiki-root-path* "/" url ".txt"))

(def root-url* "/data")
(defn wiki-url [url] (str root-url* "/" url))
(def main-url* (wiki-url "main"))

(defn main-page []
  (html5
   [:div
    (link-to (wiki-url "poker") "poker")
     "&nbsp"
    (link-to (wiki-url "texas") "texas")
     "&nbsp"
    (link-to (wiki-url "omaha") "omaha")
    ]))

(defn edit-wiki [url wiki]
  (html5
   (form/form-to [:post (str "/" url)]
                 (form/text-area {:rows 20 :cols 40} "wiki" wiki)
                 [:br]
                 (form/submit-button "Save"))))

(defn test-wiki [wiki]
  (html5
   (render-tested-wiki wiki)))

(defn fit-page [render-fn wiki link]
  (html5
   [:div
    (link-to (str link "/edit") "edit")
     "&nbsp"
    (link-to (str link "/test") "test")
    [:br]
     (render-fn wiki)]))

(defn show-wiki [url file]
  (let [url' (str url "/" file)
        wiki (read-wiki (url->path url'))]
  (fit-page render-wiki wiki file)))

(defn process-wiki [url file]
  (case file
    "edit" (edit-wiki url (read-wiki (url->path url)))
    "test" (test-wiki (read-wiki (url->path url)))
    (show-wiki url file)))

(defn set-wiki [url file wiki]
  (spit (url->path (str url "/" file)) wiki))

(defroutes app-routes
  (GET "/" [] (res/redirect  main-url*))
  (GET "/fit" [] (res/redirect main-url*))
  (GET "/fit/" [] (res/redirect main-url*))
  (GET main-url* [] (main-page))
  (POST "/*/:file" {{url :* file :file wiki :wiki} :params}
        (set-wiki url file wiki)
        (res/redirect main-url*))
  (GET "/*/:file" {{url :* file :file} :params}
       (process-wiki url file))
  (route/resources "/")
  (route/not-found "Not Found"))

(def app
  (handler/site app-routes))
