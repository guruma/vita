(ns vita.npc.npc-test
  (:require [vita.npc.core :as npc :reload true]
            [vita.npc.player :as player :reload true]))

(use 'philos.debug)

(defn simulate [game-name]
  (def nc* (npc/npc-context game-name))
  (npc/set-rate nc* 100)
  (npc/set-candidate nc* 3)
  (dotimes [n 5]
    (npc/add-player nc* (inc n)))
  (npc/request-cards nc*))

(dbg (simulate :texas))
; => {:winner {:high [3]},
;     :community-cards [:club-9 :diamond-Q :heart-4 :heart-A :club-6],
;     :players
;     {5
;      {:personal-cards [:club-4 :club-A],
;       :score
;       {:high
;        {:category :two-pair,
;         :ranks [:Q :Q :9 :9 :A],
;         :cards [:spade-Q :diamond-Q :spade-9 :club-9 :heart-A]}}},
;      4
;      {:personal-cards [:club-7 :diamond-6],
;       :score
;       {:high
;        {:category :one-pair,
;         :ranks [:6 :6 :A :Q :9],
;         :cards [:diamond-6 :club-6 :heart-A :diamond-Q :club-9]}}},
;      3
;      {:personal-cards [:spade-Q :spade-9],
;       :score
;       {:high
;        {:category :two-pair,
;         :ranks [:A :A :4 :4 :Q],
;         :cards [:club-A :heart-A :club-4 :heart-4 :diamond-Q]}}},
;      2
;      {:personal-cards [:diamond-10 :spade-10],
;       :score
;       {:high
;        {:category :one-pair,
;         :ranks [:10 :10 :A :Q :9],
;         :cards [:diamond-10 :spade-10 :heart-A :diamond-Q :club-9]}}},
;      1
;      {:personal-cards [:spade-3 :spade-2],
;       :score
;       {:high
;        {:category :high-card,
;         :ranks [:A :Q :9 :6 :4],
;         :cards [:heart-A :diamond-Q :club-9 :club-6 :heart-4]}}}}} <<

(dbg (simulate :omaha))
; => {:winner {:high [3]},
;     :community-cards [:club-3 :heart-10 :diamond-6 :club-Q :club-5],
;     :players
;     {5
;      {:personal-cards [:spade-6 :club-10 :diamond-K :heart-4],
;       :score
;       {:high
;        {:category :two-pair,
;         :ranks [:10 :10 :6 :6 :Q],
;         :cards [:club-10 :heart-10 :spade-6 :diamond-6 :club-Q]}}},
;      4
;      {:personal-cards [:heart-9 :spade-8 :diamond-8 :heart-2],
;       :score
;       {:high
;        {:category :one-pair,
;         :ranks [:8 :8 :Q :10 :6],
;         :cards [:spade-8 :diamond-8 :club-Q :heart-10 :diamond-6]}}},
;      3
;      {:personal-cards [:club-J :club-9 :diamond-9 :club-8],
;       :score
;       {:high
;        {:category :flush,
;         :ranks [:Q :J :9 :5 :3],
;         :cards [:club-Q :club-J :club-9 :club-5 :club-3]}}},
;      2
;      {:personal-cards [:spade-K :heart-Q :club-4 :heart-7],
;       :score
;       {:high
;        {:category :straight,
;         :ranks [:7 :6 :5 :4 :3],
;         :cards [:heart-7 :diamond-6 :club-5 :club-4 :club-3]}}},
;      1
;      {:personal-cards [:diamond-A :diamond-10 :heart-K :spade-3],
;       :score
;       {:high
;        {:category :two-pair,
;         :ranks [:10 :10 :3 :3 :Q],
;         :cards [:diamond-10 :heart-10 :spade-3 :club-3 :club-Q]}}}}} <<

(dbg (simulate :omaha-hi-lo))
; => {:winner {:high [3], :low [5 4]},
;     :community-cards [:spade-7 :diamond-3 :heart-2 :club-7 :spade-10],
;     :players
;     {5
;      {:personal-cards [:spade-4 :heart-5 :heart-A :heart-3],
;       :score
;       {:high
;        {:category :two-pair,
;         :cards [:spade-7 :club-7 :heart-3 :diamond-3 :heart-A]},
;        :low
;        {:category :low-card,
;         :cards [:spade-7 :spade-4 :diamond-3 :heart-2 :heart-A]}}},
;      4
;      {:personal-cards [:diamond-4 :spade-A :club-6 :club-10],
;       :score
;       {:high
;        {:category :two-pair,
;         :cards [:club-10 :spade-10 :spade-7 :club-7 :spade-A]},
;        :low
;        {:category :low-card,
;         :cards [:spade-7 :diamond-4 :diamond-3 :heart-2 :spade-A]}}},
;      3
;      {:personal-cards [:heart-4 :diamond-5 :spade-9 :spade-Q],
;       :score
;       {:high
;        {:category :full-house,
;         :cards [:heart-7 :spade-7 :club-7 :heart-10 :spade-10]},
;        :low nil}},
;      2
;      {:personal-cards [:spade-3 :club-A :diamond-2 :club-3],
;       :score
;       {:high
;        {:category :full-house,
;         :cards [:spade-3 :club-3 :diamond-3 :spade-7 :club-7]},
;        :low nil}},
;      1
;      {:personal-cards [:diamond-A :heart-10 :heart-Q :heart-7],
;       :score
;       {:high
;        {:category :one-pair,
;         :cards [:spade-7 :club-7 :spade-Q :spade-10 :spade-9]},
;        :low
;        {:category :low-card,
;         :cards [:spade-7 :diamond-5 :heart-4 :diamond-3 :heart-2]}}}}} <<

;; 승자로 내정된 사람이 게임을 중단한 경우, 승자 재계산 절차
(npc/remove-player nc* 3)

;; :high mode 재계산
(npc/sort-players nc* :high)
(dbg (npc/calc-winner nc* :high))
; => [2]

;; :low mode 재계산
(npc/sort-players nc* :low)
(dbg (npc/calc-winner nc* :low))
; => [5 4]


(defn m-test
  []
  (npc/set-rate nc* 100)
  (npc/set-candidate nc* 3)
  (dotimes [n 5]
    (npc/add-player nc* (inc n)))
  (dbg (npc/request-cards nc*))

  (dbg (player/bet nc* 3 :preflop))
  (dbg (player/bet nc* 3 :flop))
  (dbg (player/bet nc* 3 :turn))
  (dbg (player/bet nc* 3 :river))

  (dbg (player/bet nc* 1 :preflop))
  (dbg (player/bet nc* 1 :flop))
  (dbg (player/bet nc* 1 :turn))
  (dbg (player/bet nc* 1 :river))

  (dbg (player/check nc* 3 :preflop))
  (dbg (player/check nc* 3 :flop))
  (dbg (player/check nc* 3 :turn))
  (dbg (player/check nc* 3 :river))

  (dbg (player/check nc* 1 :preflop))
  (dbg (player/check nc* 1 :flop))
  (dbg (player/check nc* 1 :turn))
  (dbg (player/check nc* 1 :river)))
