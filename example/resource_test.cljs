(ns vita.resource-test
  (:require [resource.images :as img]
            [resource.audios :as aud]
            [media.sound :as snd]
            [philos.cljs.debug :refer-macros [dbg clog cl break]] ))

;;;; loading 상황 처리 관련
(def load-count (atom 0))
(def load-total (atom 0))

(defn on-load
  []
  (swap! load-count inc)

  ;; 이곳에 로딩 상황을 시각적으로 보여주는 루틴을 대신 추가한다. 
  (clog [@load-count @load-total] "in on-load")) 


;;;; audio data 추가하는 예
(def ac* (aud/audio-context on-load))
(defn audio
  [k]
  (aud/audio ac* k))

(aud/add-audio ac* :click-1 "sound/common/click-1.wav")
;; 엘리먼트의 atrubute를 추가하고 싶은 경우
(snd/set-props (audio :click-1) {:loop true})

(aud/add-audio ac* :click-2 "sound/common/click-2.wav")

(aud/add-dom-audio ac* :bgm-main "bgm-main")
(aud/add-dom-audio ac* :bgm-slotmachine "bgm-slotmachine")


;; image data 추가
(def ic* (img/image-context on-load))
(defn image
  [k]
  (img/image ic* k))

(img/add-image ic* :b-call "img/poker/b_call.png")
(img/add-image ic* :bet-plus "img/poker/bet_plus.png")


;;;; 모든 리소스를 로드
(defn preload
  []
  (reset! load-total (+ (img/count-images ic*) 
                        (aud/count-audios ac*))) 
  (aud/preload ac*)
  (img/preload ic*))


;;;; 실제 사용 예
;; (snd/play (audio :click-1))
;; (canvas/draw-image (image :b-call) ...)



             
