(ns vita.devtool.admin
  (:import [java.net.URL])
  (:require [clostache.parser :as clostache]
            [ring.util.response :as response]
            [cemerick.url :as urls]
            [com.ashafa.clutch :as cloudant]
            [vita.sonaclo.shared.util :as util]
            ))


(defonce db* (assoc (urls/url "https://vitatest.cloudant.com/" "game")
               :username "vitatest"
               :password "vitatest0987"))


(def ^:const DEFAULT_USER_DATA
  {:_id nil
   :social_id nil
   :user {:money 9900000
          :avatar_url nil}
   :game {:blackjack {:level 0}
          :slotmachine {:bet-amount 200
                        :freespin-cnt 0}}})


(defn- get-url [req]
  (str (:server-name req) ":" (:server-port req) (:uri req)))


;; GET.
(defn admin-get [req]
  (clostache/render-resource "public/admin.html" {:url (get-url req)}))


;; POST.
(defn admin-add-user [id pwd]
  (let [doc (util/combine DEFAULT_USER_DATA
                          {:_id (str (keyword id))
                           :social_id id
                           :pass pwd
                           :user {:avatar_url (str "avatar-" id)}})]

    (try
      (cloudant/put-document db* doc)
      (response/response (str "[done] add user: " doc))

      (catch Exception e
        (response/response (str "[fail] add user: " doc))))))


(defn admin-clear-db []
  (let [all-docs (cloudant/all-documents db*)
        doc-infos (map (fn [x]
                         {:_id (:id x) :_rev (get-in x [:value :rev])})
                       all-docs)]
    (doseq [doc doc-infos]
      (when-not (re-find #"^_design/*" (:_id doc))
        (cloudant/delete-document db* doc))))
  (response/response "[done] clear db"))
